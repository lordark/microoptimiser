const express = require('express');
const {json} = require('body-parser');
const cors = require('cors');

const app = express();
app.use(json());
app.use(cors());

app.get('/',(req,res)=>{
    res.send("Hi There Marketing")
})


app.listen(4002,()=>{
    console.log("Successfully running on port 4002 !! ");
})