if(window.location.search.includes("?inactive=true")){
  sessionStorage.setItem("status","User_not_found")
  window.location.href="/"
}

$(document).ready(function () {
    var whatTop;
    if ($('.whatMakesNav').length > 0) {
      whatTop = $('.whatMakesNav').offset().top - 79;
  
      $(".whatMakesNav .nav-link").on('click', function (event) {
  
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
  
          // Store hash
          var hash = this.hash;
  
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function () {
  
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });
    }
  
    $(window).scroll(function () {
      var wScrollTop = $(window).scrollTop();
  
      if (wScrollTop > 56) {
        $('header >.navbar').addClass('shadow');
      }
      else {
        $('header >.navbar').removeClass('shadow');
      }
      if (whatTop) {
        if (wScrollTop > whatTop) {
          $('.whatMakesNav').addClass('stick');
        }
        else {
          $('.whatMakesNav').removeClass('stick');
        }
      }
    });
    $(document).off('click', '.btnFreeTrial');
    $(document).on('click', '.btnFreeTrial', function () {
      $(this).closest('.form-tryFreeRegistration').find('.pending-field').removeClass('pending-field');
      let valid = true;
      $(this).closest('.form-tryFreeRegistration').find('[data-required]').each(function () {
        if ($(this).val() == "") {
          $(this).addClass('pending-field');
          if (valid) {
            valid = false;
            if (this.nodeName.toLowerCase() != "select")
              $(this).focus();
          }
        }
      });
      if (valid) {
        if ($(document).find('.chkTermAndCondition').is(':checked') == false) {
          swal("Please accept the term and conditions.", { "icon": "info" });
          return false;
        }
        return true;
      } else
        return false;
    })
    $(document).off('click', '.btnSubmitRequestForDemo');
    $(document).on('click', '.btnSubmitRequestForDemo', function () {
      $(this).closest('.form-RequestDemo').find('.pending-field').removeClass('pending-field');
      let valid = true;
      $(this).closest('.form-RequestDemo').find('[data-required]').each(function () {
        if ($(this).val() == "") {
          $(this).addClass('pending-field');
          if (valid) {
            valid = false;
            if (this.nodeName.toLowerCase() != "select")
              $(this).focus();
          }
        }
      });
      if (valid) {
        if ($(document).find('.chkTermAndCondition').is(':checked') == false) {
          swal("Please accept the term and conditions.", { "icon": "info" });
          return false;
        }
        return true;
      } else
        return false;
    })
    $(document).off('click', '.btnSubmitContactUs');
    $(document).on('click', '.btnSubmitContactUs', function () {
      $(this).closest('.form-ContactUs').find('.pending-field').removeClass('pending-field');
      let valid = true;
      $(this).closest('.form-ContactUs').find('[data-required]').each(function () {
        if ($(this).val() == "") {
          $(this).addClass('pending-field');
          if (valid) {
            valid = false;
            if (this.nodeName.toLowerCase() != "select")
              $(this).focus();
          }
        }
      });
      if (valid)
        return true;
      else
        return false;
    })
  });