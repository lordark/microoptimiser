const _contact = {};

_contact.emailPopupHTML = (`<div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5 class="font-weight-bold text-primary">Domains</h5>
                                <div class="form-group vld-parent custom-column-div">
                                    <select class="domainDropdown form-control form-control-sm" />
                                </div>
                            </div>
                        </div>
                        <div class="row frmUnsubscribeEmail">
                            <div class="error-container vld-parent"></div>
                            <div class="col-sm-12 mt-4 subscribe-unsubscribe-details">
                                <div class="table-responsive tblSubscription">
                                    <table class="table table-bordered w-100">
                                        <thead class="w-100">
                                            <tr class="head-row w-100">
                                                <th>Marketing Categories</th>
                                                <th>Subscribed</th>
                                                <th>Unsubscribed</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <nav class="mt-3 float-right subscription-email-paging">
                                    <div class="paging">
                                        <ul class="pagination"></ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>`);

//Events Page: ContactDetail
_contact.InitContactDetail = function (page) {
    let contactID = page._id;
    _common.GetStaticPage('/page/contact/DetailRightSection.html', function (res) {
        let rightSection = page.ObjectPage.find('.object-detail-right').html(res);
        //Get Activity Chart
        _chart.ActivityCountMonthWise('Contact', contactID, 'past', rightSection.find('.activity-chart .card-body .tab-pane[display-type="past"]')[0]);
        rightSection.find('.activity-chart .card-header .nav-item').on('click', function (e) {
            e.stopPropagation();
            let displayType = $(this).attr('display-type');
            rightSection.find('.activity-chart .card-header .nav-item').removeClass('active');
            $(this).addClass('active');
            rightSection.find('.activity-chart .card-body .tab-pane').addClass('d-none');
            rightSection.find('.activity-chart .card-body .tab-pane[display-type="' + displayType + '"]').removeClass('d-none');
            _chart.ActivityCountMonthWise('Contact', contactID, displayType, rightSection.find('.activity-chart .card-body .tab-pane[display-type="' + displayType + '"]')[0]);
        })
    });

    _contact.CheckSubscription(page);

    let emailSubscribePage = page.ObjectPage;
    let contactOwner = page.Data.OwnerID;
    let emailPopup = _common.DynamicPopUp({ title: '<b>' + page.Data["FirstName"] + ' '+ page.Data["LastName"] + '</b>', body: _contact.emailPopupHTML });

    emailSubscribePage.find('.btnEmailSubsShow').unbind('click');
    emailSubscribePage.find('.btnEmailSubsShow').on('click', function(){
        let emailID = $(this).parent().find('.email').text();
        _contact.ShowDomainList(emailPopup, emailID, contactOwner, page);

        emailPopup.find('.domainDropdown').unbind('change');
        emailPopup.find('.domainDropdown').on('change', function () {
            selectedDomainId = $(this).val();
            _contact.DisplaySubscribeUnsubscribeCategories(emailPopup, emailID, selectedDomainId, contactOwner);
        });
    });
}

_contact.ReloadContactDetail = function (page) {
    _contact.CheckSubscription(page);

    let emailSubscribePage = page.ObjectPage;
    let contactOwner = page.Data.OwnerID;
    let emailPopup = _common.DynamicPopUp({ title: '<b>' + page.Data["FirstName"] + ' '+ page.Data["LastName"] + '</b>', body: _contact.emailPopupHTML });

    emailSubscribePage.find('.btnEmailSubsShow').unbind('click');
    emailSubscribePage.find('.btnEmailSubsShow').on('click', function(){
        let emailID = $(this).parent().find('.email').text();
        _contact.ShowDomainList(emailPopup, emailID, contactOwner, page);

        emailPopup.find('.domainDropdown').unbind('change');
        emailPopup.find('.domainDropdown').on('change', function () {
            selectedDomainId = $(this).val();
            _contact.DisplaySubscribeUnsubscribeCategories(emailPopup, emailID, selectedDomainId, contactOwner);
        });
    });
}

_contact.ShowDomainList = function (popup, emailID, ContactOwner, page) {
    let selectedEmailId = emailID;
    let emailPopup = popup;
    let contactOwner = ContactOwner;
    _common.FetchApiByParam({
        url: "api/admin/marketingdomaindml",
        type: "post",
        data: { Operation: "select" }
    }, function (result) {
        if (result.message == "success") {
            let mktDomains = result.data;
            _common.GetRegisteredEmail().then(function (data) {
                if (data && data.length > 0) {
                    let domainList = "";
                    let domainAccess = [];
                    for (let i = 0; i < data.length; i++) {
                        let allowedUsers = data[i].AllowedUsers;
                        if (allowedUsers && allowedUsers.length > 0) {
                            for (let j=0; j < allowedUsers.length; j++) {
                                if (allowedUsers[j] == _initData.User._id)
                                    domainAccess.push(data[i].DomainId);
                            }
                        }
                    }
                    domainAccess = domainAccess.filter((value, index, self) => self.indexOf(value) === index );
                    for (let i = 0; i < mktDomains.length; i++) {
                        for (let j=0; j < domainAccess.length; j++) {
                            if (domainAccess[j] == mktDomains[i]._id)
                                domainList += `<option value="${mktDomains[i]._id}">${mktDomains[i].DomainName.trim()}</option>`;
                        }
                    }
                    emailPopup.find('.domainDropdown').html('').append(domainList);
                    let selectedDomainId = emailPopup.find('.domainDropdown').val();
                    _contact.DisplaySubscribeUnsubscribeCategories(emailPopup, selectedEmailId, selectedDomainId, contactOwner);
                    emailPopup.find('.modal-title').html('<b>' + selectedEmailId + '</b>');
                    if (contactOwner != _initData.User._id)
                        emailPopup.find('.modal-footer').html('').append('<button type="button" class="btn btn-primary pl-3 pr-3" data-dismiss="modal">OK</button>');
                    emailPopup.modal("show");
                } else {
                    swal('No campaign scheduled', {icon: 'info'});
                }
            }, function (result) {
                _common.Loading(false);
            });

            emailPopup.find('.btnSave').unbind('click');
            emailPopup.find('.btnSave').on('click', function () {
                let obj = {};
                obj.EmailID = emailID
                obj.CampaignSubsCategory = [];
                obj.CampaignUnsubsCategory = [];
                emailPopup.find('.tblSubscription tbody [type="checkbox"]').each(function (i, v) {
                    if ($(this).prop('checked') && $(this).closest('td').hasClass('unsubscription'))
                        obj.CampaignUnsubsCategory.push($(this).attr('data-id'));
                    else if ($(this).prop('checked') && $(this).closest('td').hasClass('subscription'))
                        obj.CampaignSubsCategory.push($(this).attr('data-id'));
                });
                obj.DomainId = emailPopup.find('.domainDropdown').val();
                obj.LastModifiedDate = new Date();
                _contact.updateUnsubscribeData(obj, emailPopup, page);
            });
        }
    });
}

_contact.DisplaySubscribeUnsubscribeCategories = function (popup, emailID, domainID, ContactOwner) {
    let sdata = { Email: emailID, DomainId: domainID };
    let emailPopup = popup;
    let selectedDomainId = domainID;
    let contactOwner = ContactOwner;
    _common.ContainerLoading(true, emailPopup.find('.tblSubscription'));
    _common.FetchApiByParam({
        url: "api/common/getemailwiseunsubscribedata",
        type: "post",
        data: sdata
    }, function (result) {
        if (result.message == "success") {
            // let TotalCategoryCount = 0;
            if (result.data && result.data.length > 0) {
                let campaignCategoryList = result.data;
                if (selectedDomainId != undefined)
                    campaignCategoryList = campaignCategoryList.filter(p => p.DomainID == selectedDomainId);
                // if (campaignCategoryList && campaignCategoryList.length > 0) {
                //     TotalCategoryCount = campaignCategoryList.length;
                // }
                emailPopup.find('.tblSubscription tbody tr').remove();
                for (let i = 0; i < campaignCategoryList.length; i++) {
                    let data = campaignCategoryList[i];
                    let tr = "<tr>";
                    tr += "<td>" + data.Value + "</td>";
                    if (contactOwner != _initData.User._id) {
                        if (!data.checked) {
                            tr += '<td class="subscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"><img src="/images/checked.svg" /></div></td>';
                            tr += '<td class="unsubscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"><img src="/images/unchecked.svg" /></div</td>';
                        } else {
                            tr += '<td class="subscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"><img src="/images/unchecked.svg" /></td>';
                            tr += '<td class="unsubscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"><img src="/images/checked.svg" /></td>';
                        }
                    } else {
                        if (!data.checked) {
                            tr += `<td class="subscription">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="chkSelect${data.Key} custom-control-input" checked data-id="${data.Key}" id="chkIsSubscribe1${data.Key}" disabled>
                                        <label class="custom-control-label" for="chkIsSubscribe1${data.Key}"></label>
                                    </div></td>`;
                            tr += `<td class="unsubscription">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="chkSelect${data.Key} custom-control-input" data-id="${data.Key}" id="chkIsSubscribe2${data.Key}">
                                        <label class="custom-control-label" for="chkIsSubscribe2${data.Key}"></label>
                                    </div></td>`;
                        } else {
                            tr += `<td class="subscription">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="chkSelect${data.Key} custom-control-input" data-id="${data.Key}" id="chkIsSubscribe3${data.Key}" disabled>
                                        <label class="custom-control-label" for="chkIsSubscribe3${data.Key}"></label>
                                    </div></td>`;
                            tr += `<td class="unsubscription">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="chkSelect${data.Key} custom-control-input" checked data-id="${data.Key}" id="chkIsSubscribe4${data.Key}">
                                        <label class="custom-control-label" for="chkIsSubscribe4${data.Key}"></label>
                                    </div></td>`;
                        }
                    }
                    tr += '</tr>';
                    emailPopup.find('.tblSubscription tbody').append(tr);

                    _common.ContainerLoading(false, emailPopup.find('.tblSubscription'));
                    
                    emailPopup.find(".tblSubscription tbody").on('change', '.chkSelect' + data.Key, function() {
                        emailPopup.find(".tblSubscription tbody .chkSelect" + data.Key).prop('checked',false);
                        $(this).prop('checked',true);
                    });
                }
                //modalUser.find('.total-count').text((TotalUserCount > 0 ? TotalUserCount + " Items" : "No data found"));
                //let pager = _common.PagerService(TotalEmailCount, page.PageIndex, page.PageSize);
                //_common.BuildPaging(pager, page.PageContainer.find('.restrict-email-paging'));
            } else {
                swal('Data not found', { "icon": "error" });
            }
        } else {
            swal(result.message, { "icon": "error" });
        }
    });
}

_contact.CheckSubscription = function (page) {
    if (page.ObjectPage && page.PageData && page.PageData.ObjectSchema && page.PageData.ObjectSchema.Fields){
        let pageContainer =  page.ObjectPage;
        let fields = page.PageData.ObjectSchema.Fields
        for (let i=0; i<fields.length; i++){
            let fld = fields[i];
            if (fld.UIDataType == "email") {
                let fldName = fld.Name;
                let emailID = page.Data[fldName];
                if (emailID == undefined || emailID == '') { continue; }
                //Append Envope HTML
                if (pageContainer.find('[data-field=' + fldName + '] .email-section .btnEmailSubsShow').length == 0)
                    pageContainer.find('[data-field=' + fldName + '] .email-section').append('<span class="btnEmailSubsShow btn pt-0 pb-1"><i class="fas fa-envelope" style="font-size:14px;"></i></span>');
                // Get unsubscribe data
                let sdata = { Email: emailID };
                _common.FetchApiByParam({
                    url: "api/common/getemailwiseunsubscribedata",
                    type: "post",
                    data: sdata
                }, function (result) {
                    if (result.message == "success") {
                        let unsubscribeAll = true;
                        let filterUnsubsData = [];
                        if (result.data && result.data.length > 0) {
                            _common.GetRegisteredEmail().then(function (data) {
                                if (data && data.length > 0) {
                                    let domainList = "";
                                    let domainAccess = [];
                                    for (let i = 0; i < data.length; i++) {
                                        let allowedUsers = data[i].AllowedUsers;
                                        if (allowedUsers && allowedUsers.length > 0) {
                                            for (let j=0; j < allowedUsers.length; j++) {
                                                if (allowedUsers[j] == _initData.User._id)
                                                    domainAccess.push(data[i].DomainId);
                                            }
                                        }
                                    }
                                    domainAccess = domainAccess.filter((value, index, self) => self.indexOf(value) === index );
                                    for (let i = 0; i < domainAccess.length; i++) {
                                        filterUnsubsData.push(...result.data.filter(p => p.DomainID == domainAccess[i]));
                                    }
                                    for (let i=0; i<filterUnsubsData.length; i++) {
                                        if (!filterUnsubsData[i].checked) {
                                            pageContainer.find('[data-field=' + fldName + ']').find('.btnEmailSubsShow').css('color', 'green');
                                            unsubscribeAll = false; break;
                                        }
                                    }
                                    if (unsubscribeAll)
                                        pageContainer.find('[data-field=' + fldName + ']').find('.btnEmailSubsShow').css('color', 'red');                        
                                }
                            }, function (result) {
                                _common.Loading(false);
                            });
                        } else {
                            console.log('Unsubscribe data not found');
                        }
                    } else {
                        swal(result.message, { "icon": "error" });
                    }
                });
            }
        }
    }
}

_contact.updateUnsubscribeData = function (data, popup, page) {
    let emailPopup = popup;
    _common.Loading(true);
    _common.FetchApiByParam({
        url: "api/common/updateunsubscribedata",
        type: "post",
        data: { data: data }
    }, function (result) {
        if (result.message == "success") {
            emailPopup.modal("hide");
            swal(result.data, {icon: 'success'});
            _contact.CheckSubscription(page);
            _common.Loading(false);
        } else {
            swal('Something went wrong.', {icon: 'error'});
            _common.Loading(false);
         }
    });
}

//Events Page: ContactDetail
_contact.ContactFormValidation = function (page) {
    if (page._id) {
        let reportsToCtrl = page.PageContainer.find('[data-field="ReportsTo"]');
        let reportsToField = reportsToCtrl[0].field;
        let reportsToID = _controls.GetCtrlValue({ field: reportsToField, container: page.PageContainer });
        if (page._id == reportsToID) {
            _common.ValidateAddMsg(reportsToCtrl, 'A contact must report to a different contact.');
            return false;
        }
    }
    return true;
}


//Events Page: ContactDetail
_contact.ContactAccountChange = function (result) {
    if (result.data) {
        $.ajax({
            url: '/api/contact/accountchange',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(result.data),
            success: function (res) { },
        });
    }
}

_contact.ContactAddEvent = function (self) {
    let childForm = self.ParamData.childContainor
    if(childForm && (self.PageContainer.attr('data-uid') == "ContactDetail:secondary-form")){
        // if(sdata.fields.AccountID == null){
        //     childForm.find('.btnSaveObject').trigger('click')
        // }
        // else 
        if(childForm[0].PageForm){
            childForm.modal('hide');
            setTimeout(function(){ childForm[0].PageForm.ParamData.onSave() }, 1000);
        }
    }
}