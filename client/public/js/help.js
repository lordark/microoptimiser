const _help = {};

_help.HelpDocumentInit = function (page) {
  _common.FetchApiByParam(
    {
      url: "/api/help/gethelpdocumentlist",
    },
    function (result) {
      if (result.message === "success") {
        if (result.data && result.data.length > 0) {
          for (let i = 0; i < result.data.length; i++) {
            let documentBody = $(
              '<div class="col-sm-6 col-md-4 col-lg-3 mt-3 document link-page" page-name="HelpDocumentDetail"><div class="card help-topic-card"><a href="#" class="card-body d-flex"><span><i class="document-icon"></i></span><span class="topic-text"><span class="document-title"></span><span class="small d-block p document-subtitle"></span></span></a></div></div>'
            );
            let item = result.data[i];
            documentBody.find(".document-title").html(item.Title);
            documentBody.find(".document-subtitle").html(item.Subtitle);
            documentBody.find(".document-icon").addClass(item.Icon);
            documentBody.attr("data-id", item._id);
            page.PageContainer.find(".help-documents").append(documentBody);
          }
          page.PageContainer.on("keyup", "#help_input_1", function () {
            var value = $(this).val().toLowerCase();
            page.PageContainer.find(".help-detail-body *").filter(function () {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
          });
        }
      }
    }
  );
};

_help.HelpDocumentDetailInit = function (page) {
  let helpID = page.param._id;
  let container = page.PageContainer;
  _common.FetchApiByParam(
    {
      url: "/api/help/gethelpdocumentdetail",
      data: { id: helpID },
    },
    function (result) {
      if (result.message === "success") {
        if (result.data && result.data !== null) {
          //sidebar
          container.find(".help-side-title").html(result.data.Title);
          for (let i = 0; i < result.data.Topics.length; i++) {
            container
              .find(".help-main-topic")
              .append(
                `<li id="help_${result.data.Topics[i].UID}" class="help-sub-topic caret caret-down"><a href="#${result.data.Topics[i].UID}">${result.data.Topics[i].Topic}</a></li>`
              );
            if (result.data.Topics[i].Topics.length !== 0) {
              for (let j = 0; j < result.data.Topics[i].Topics.length; j++) {
                container
                  .find(`#help_${result.data.Topics[i].UID}`)
                  .append(
                    `<li id="help_child${result.data.Topics[i].Topics[j].UID}" class="nested"><a href="#${result.data.Topics[i].Topics[j].UID}">${result.data.Topics[i].Topics[j].Topic}</a></li>`
                  );
                if (result.data.Topics[i].Topics[j].Topics.length !== 0) {
                  for (
                    let k = 0;
                    k < result.data.Topics[i].Topics[j].Topics.length;
                    k++
                  ) {
                    container
                      .find(`#help_child${result.data.Topics[i].Topics[j].UID}`)
                      .append(
                        `<li class="nested"><a href="#${result.data.Topics[i].Topics[j].Topics[k].UID}">${result.data.Topics[i].Topics[j].Topics[k].Topic}</a></li>`
                      );
                  }
                }
              }
            }
          }
          //mainpage
          container.find(".help-detail-title").html(result.data.Title);
          for (let i = 0; i < result.data.Topics.length; i++) {
            container
              .find(".help-detail-body")
              .append(
                `<section><h3 id="${result.data.Topics[i].UID}">${result.data.Topics[i].Topic}</h3><p>${result.data.Topics[i].Description}</p><ul></ul></section>`
              );
            for (let j = 0; j < result.data.Topics[i].Topics.length; j++) {
              container
                .find(".help-detail-body")
                .append(
                  `<section class="help-detail-child"><h3 id="${result.data.Topics[i].Topics[j].UID}">${result.data.Topics[i].Topics[j].Topic}</h3><p>${result.data.Topics[i].Topics[j].Description}</p></section>`
                );
              for (
                let k = 0;
                k < result.data.Topics[i].Topics[j].Topics.length;
                k++
              ) {
                container
                  .find(".help-detail-body")
                  .append(
                    `<section class="help-detail-child"><h3 id="${result.data.Topics[i].Topics[j].Topics[k].UID}">${result.data.Topics[i].Topics[j].Topics[k].Topic}</h3><p>${result.data.Topics[i].Topics[j].Topics[k].Description}</p></section>`
                  );
              }
            }
          }
          container.on("click", ".caret", function () {
            $(this).toggleClass("caret-down");
            $(this).find(".nested").toggle();
          });

          container.on("keyup", "#help_detail_input_1", function () {
            var value = $(this).val().toLowerCase();
            container.find(".help-detail-body *").filter(function () {
              if ($(this).text().toLowerCase().indexOf(value) > 0) {
                var content = $(this).text();
                console.log(value);
                content = content.replace(
                  value,
                  '<span class="search-found">' + value + "</span>"
                );
                container.find(".help-detail-body *").html(content);
              }
            });
          });
        }
      }
    }
  );
};

//sidebar
// container.find(".help-side-title").html(result.data.Title);
// const traverse = (data) => {
//   if (data && data.UID != undefined) {
//     if (Array.isArray(data.Topics)) {
//       container
//         .find(".help-main-topic")
//         .append(
//           `<li id="help_${data.UID}" class="help-sub-topic caret caret-down"><a href="#${data.UID}">${data.Topic}</a></li>`
//         );
//     } else {
//       container
//         .find(`.help-sub-topic`)
//         .append(
//           `<li class="nested"><a href="#${data.UID}">${data.Topic}</a></li>`
//         );
//     }
//   }
//   if (Array.isArray(data.Topics)) {
//     let childAmt = data.Topics.length;

//     for (let i = 0; i < childAmt; i++) {
//       traverse(data.Topics[i]);
//     }
//   }

//   return container;
// };

// traverse(result.data);
