const GoogleMaps = {};

GoogleMaps.GetLatLongByAddress = function (options) {
    let address = "";
    if (options) {
        if (options.address)
            address += options.address + ", ";
        if (options.postalcode)
            address += options.postalcode + ", ";
        if (options.city)
            address += options.city + ", ";
        if (options.state)
            address += options.state + ", ";
        if (options.country)
            address += options.country + ", ";

        address = address.substring(0, address.lastIndexOf(','));
        options.search_address = address;
    }
    options.status = "ok";
    options.IsMatchFound = true;
    if (options.lat && options.lng) {
        options.zoomLevel = 16;
        if (options.IsLocateOnMap == true)
            GoogleMaps.LocateOnMap(options)
    } else {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //center = results[0].geometry.location;
                options.lat = results[0].geometry.location.lat();
                options.lng = results[0].geometry.location.lng();
                options.zoomLevel = 16;
                options.status = "ok";
                options.IsMatchFound = true;
            }
            else {
                options.status = "failed";
                options.IsMatchFound = false;
                options.zoomLevel = 11;
            }
            if (options.IsLocateOnMap == true)
                GoogleMaps.LocateOnMap(options)
        });
    }
}

GoogleMaps.LocateOnMap = function (options) {
    var map = null,
        markerA = null,
        infowindow = null,
        modalMap = null,
        lat = (options.lat || null),
        lng = (options.lng || null);

    let popUpObj = { title: "Show on Map", body: '<div><input type="text" id="txtSearch" autocomplete="false" placeholder="Search Box" style="padding:4px 10px; height:20px;width:250px;box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);display:none;"/><div style="width:100%;height:400px" id="map-canvas"></div></div>' }
    if (options.ClickRef) {
        popUpObj.ClickRef = options.ClickRef
    }
    modalMap = _common.DynamicPopUp(popUpObj);

    modalMap.modal('show');

    //var message = "<h3 style='margin-top: 0;font-size: 15px;padding: 0;width:auto;margin: 0;display:inline-block'>Test Location</h3><p style='margin-top:5px;'>Test Address</p>";

    options.lat = lat = (lat || -33.8688)
    options.lng = lng = (lng || 151.2195);

    var pointDest = new google.maps.LatLng(lat, lng);

    var mapOptions = {
        center: { lat: lat, lng: lng },
        zoom: options.zoomLevel,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: true,
        fullscreenControl: true,
        gestureHandling: 'greedy'
    };
    if (options.IsRemoveBtnsaveButton == true) {
        modalMap.find('.modal-footer .btnSave').hide();
    }
    setTimeout(function () {
        map = new google.maps.Map(modalMap.find('#map-canvas')[0], mapOptions);
        markerA = new google.maps.Marker({
            position: pointDest,
            //icon: '/public/images/bg.png',
            map: map,
            draggable: true
        });

        // infowindow = new google.maps.InfoWindow({
        //     content: message
        // })
        // google.maps.event.addDomListener(markerA, 'click', function () {
        //     infowindow.open(map, markerA);
        // });
        if (options.ShowSearchBox == true) {
            modalMap.find('#txtSearch').show();
            var input = modalMap.find('#txtSearch')[0];
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }

                var place = places[0];
                if (markerA) {
                    markerA.setMap(null);
                }
                var bounds = new google.maps.LatLngBounds();
                markerA = new google.maps.Marker({
                    map: map,
                    title: place.name,
                    draggable: true,
                    position: place.geometry.location
                });
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
                map.fitBounds(bounds);
                options.IsNewAddress = true;
                options.NewAddress = GoogleMaps.ParseGoogleAddress(place);

                //google.maps.event.trigger(map, 'resize');
                //map.setCenter(new google.maps.LatLng(lat, lng));
            });
        } else {
            modalMap.find('#txtSearch').hide();
        }
        google.maps.event.addListener(markerA, 'dragend', function (event) {
            options.lat = this.getPosition().lat();
            options.lng = this.getPosition().lng();
        });
        /*  google.maps.event.addListenerOnce(map, 'idle', function() {
             google.maps.event.trigger(map, 'resize');        
         }); */
    }, 1000);



    modalMap.find('.btnSave').click(function () {
        if (options.CallingControl) {
            if (options.IsNewAddress == true && options.NewAddress) {
                options.CallingControl.attr('data-lat', options.NewAddress.lat);
                options.CallingControl.attr('data-lng', options.NewAddress.lng);
                let parentForm = options.CallingControl.closest('.page-form.show');
                locationFields = options.CallingControl[0].field.Schema.LocationFileds;

                if (parentForm.find('[data-field="' + locationFields.Address + '"]')) {
                    parentForm.find('[data-field="' + locationFields.Address + '"]').val(options.NewAddress.address);
                }
                if (parentForm.find('[data-field="' + locationFields.City + '"]'))
                    if (parentForm.find('[data-field="' + locationFields.City + '"]').children('.lookup-item').contents().length() > 0)
                        options.city = parentForm.find('[data-field="' + locationFields.City + '"]').children('.lookup-item').contents()[0].data;

                if (parentForm.find('[data-field="' + locationFields.State + '"]')) {
                    parentForm.find('[data-field="' + locationFields.State + '"]').val(options.NewAddress.state);
                }
                if (parentForm.find('[data-field="' + locationFields.Country + '"]')) {
                    parentForm.find('[data-field="' + locationFields.Country + '"]').val(options.NewAddress.country);
                }
                if (parentForm.find('[data-field="' + locationFields.PostalCode + '"]')) {
                    parentForm.find('[data-field="' + locationFields.PostalCode + '"]').val(options.NewAddress.postalcode);
                }

            } else {
                options.CallingControl.attr('data-lat', options.lat);
                options.CallingControl.attr('data-lng', options.lng);
            }
        }
        modalMap.modal('hide');
    });
    if (options.IsLocateOnMap == true && options.IsMatchFound == false) {
        swal('Unable to locate the given address, Please use map for the same', { icon: "info", });
    }
}
GoogleMaps.ParseGoogleAddress = function (googleAddress) {
    let NewAddress = { venuename: "", address: "", city: "", country: "", state: "", postalcode: "", AddressFound: false };
    if (googleAddress) {
        if (googleAddress.address_components && googleAddress.address_components.length > 0) {
            NewAddress.AddressFound = true;
            let AddComp = googleAddress.address_components;
            let venuename = AddComp.filter(x => (x.types.indexOf("premise") >= 0))
            if (venuename && venuename.length == 0) {
                venuename = AddComp.filter(x => (x.types.indexOf("route") >= 0))
            }
            if (venuename && venuename.length) {
                for (let i = 0; i < venuename.length; i++) {
                    NewAddress.venuename += venuename[i].long_name + ', ';
                }
                NewAddress.venuename = NewAddress.venuename.substring(0, NewAddress.venuename.lastIndexOf(", "));
            }
            let address = AddComp.filter(x => (x.types.indexOf("street_number") >= 0 || x.types.indexOf("route") >= 0
                || x.types.indexOf("sublocality") >= 0 || x.types.indexOf("sublocality_level_1") >= 0) || x.types.indexOf("postal_town") >= 0)
            if (address && address.length) {
                for (let i = 0; i < address.length; i++) {
                    NewAddress.address += address[i].long_name + ', ';
                }
                NewAddress.address = NewAddress.address.substring(0, NewAddress.address.lastIndexOf(", "));
            }
            let city = AddComp.filter(x => (x.types.indexOf("administrative_area_level_2") >= 0));
            if (city && city.length) {
                for (let i = 0; i < city.length; i++) {
                    NewAddress.city += city[i].long_name + ', ';
                }
                NewAddress.city = NewAddress.city.substring(0, NewAddress.city.lastIndexOf(", "));
            }
            let state = AddComp.filter(x => x.types.indexOf("administrative_area_level_1") >= 0)
            if (state && state.length) {
                for (let i = 0; i < state.length; i++) {
                    NewAddress.state += state[i].long_name + ', ';
                }
                NewAddress.state = NewAddress.state.substring(0, NewAddress.state.lastIndexOf(", "));
            }
            let country = AddComp.filter(x => x.types.indexOf("country") >= 0)
            if (country && country.length) {
                for (let i = 0; i < country.length; i++) {
                    NewAddress.country += country[i].short_name + ', ';
                }
                NewAddress.country = NewAddress.country.substring(0, NewAddress.country.lastIndexOf(", "));
            }
            let postalcode = AddComp.filter(x => x.types.indexOf("postal_code") >= 0)
            if (postalcode && postalcode.length) {
                for (let i = 0; i < postalcode.length; i++) {
                    NewAddress.postalcode += postalcode[i].long_name + ', ';
                }
                NewAddress.postalcode = NewAddress.postalcode.substring(0, NewAddress.postalcode.lastIndexOf(", "));
            }
            if (googleAddress.geometry) {
                NewAddress.lat = googleAddress.geometry.location.lat();
                NewAddress.lng = googleAddress.geometry.location.lng();
            }
        }
    }
    return NewAddress;
}


/*This Function Not In Use*/
GoogleMaps.GetAddressByLatLong = function (options) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'location': { lat: options.NewAddress.lat, lng: options.NewAddress.lng } }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            // options.map.setZoom(11);
            // var marker = new google.maps.Marker({
            //     position: latlng,
            //     map: options.map
            // });
            // infowindow.setContent(results[0].formatted_address);
            // infowindow.open(map, marker);

            //alert(results[0].formatted_address);
            let resultCountry = results.filter(x => x.types.indexOf("country") >= 0)
            if (resultCountry && resultCountry.length > 0) {
                if (resultCountry[0].address_components && resultCountry[0].address_components.length > 0) {
                    NewAddress.Country = resultCountry[0].address_components[0];
                }
            }
            let resultPostalCode = results.filter(x => x.types.indexOf("postal_code") >= 0)
            if (resultPostalCode && resultPostalCode.length > 0) {
                if (resultPostalCode[0].address_components && resultPostalCode[0].address_components.length > 0) {
                    NewAddress.PinCode = resultPostalCode[0].address_components[0].long_name;
                }
            }
            let resultStateCode = results.filter(x => x.types.indexOf("administrative_area_level_1") >= 0)
            if (resultStateCode && resultStateCode.length > 0) {
                if (resultStateCode[0].address_components && resultStateCode[0].address_components.length > 0) {
                    NewAddress.State = resultStateCode[0].address_components[0].long_name;
                }
            }
            NewAddress.Address = "";
            NewAddress.State = "";
            NewAddress.City = "";
        } else {
            NewAddress.Address = "";
            NewAddress.State = "";
            NewAddress.Country = "";
            NewAddress.PinCode = "";
            NewAddress.City = "";
        }
    });
}
GoogleMaps.GoogleAutoComplete = function (options) {
    var modalAddress = null,
        lat = (options.lat || null),
        lng = (options.lng || null),
        autocomplete = null;
    let currentOptionsId = Date.parse(new Date());
    let body = `<div style="min-height:300px" class="searchOptionsContainer">
                    <div class="custom-control custom-radio custom-control-inline mt-2 search-type">
                        <input type="radio" name="search-type" id="${currentOptionsId}" class="custom-control-input search-google" checked>
                        <label class="custom-control-label" for="${currentOptionsId}">Search On Google</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline mt-2 search-type">
                        <input type="radio" name="search-type" id="${currentOptionsId + 1}" class="custom-control-input search-msp">
                        <label class="custom-control-label" for="${currentOptionsId + 1}">Search On ${_initData.Company.Name}</label>
                    </div>
                    <div class="row mt-4">
                        <div class="form-group col-sm-12 google-searchbox-container">
                            <input type="text" class="msp-ctrl form-control form-control-sm" autocomplete="false" placeholder="Search Box">
                        </div>
                        <div class="form-group col-sm-12 msp-searchbox-container">
                        </div>
                    </div>
                </div>`;
    let popUpObj = { title: 'Search Address', body: body, size: 'modal-sm' };

    //To enable the button when model is closed
    if (options.ClickRef) {
        popUpObj["ClickRef"] = options.ClickRef
    }

    modalAddress = _common.DynamicPopUp(popUpObj);
    let fieldMSP = { 'Name': 'AddressCollection', 'DisplayName': 'Search', 'ObjectName': 'CommonObjectSchema', 'HasAddButton': false };
    fieldMSP.Schema = { 'UIDataType': 'lookup' };
    let fieldMSPCtrl = _controls.BuildCtrl({ field: fieldMSP });
    modalAddress.find('.msp-searchbox-container').append(fieldMSPCtrl);
    modalAddress.find('.modal-footer .btnSave').remove();
    modalAddress.modal('show');

    let field = options.CallingControl[0].field;
    if (field && field.SearchAddress) {
        if (field.SearchAddress.DefaultSource) {
            if (field.SearchAddress.DefaultSource == "db") {
                modalAddress.find('.search-msp').prop('checked', true);
                modalAddress.find('.google-searchbox-container').addClass('d-none');
                modalAddress.find('.msp-searchbox-container').removeClass('d-none');
                modalAddress.find('.msp-searchbox-container input').focus();
            }
            else if (field.SearchAddress.SearchOptions) {
                modalAddress.find('.search-google').prop('checked', true);
                modalAddress.find('.msp-searchbox-container').addClass('d-none');
                modalAddress.find('.google-searchbox-container').removeClass('d-none');
                modalAddress.find('.google-searchbox-container input').focus();
            }
        }
        if (field.SearchAddress.RemoveOption) {
            modalAddress.find('.search-type').addClass('d-none');
            if (field.SearchAddress.RemoveOption == "db") {
                modalAddress.find('.msp-searchbox-container').addClass('d-none');
                modalAddress.find('.google-searchbox-container input').focus();
            } else if (field.SearchAddress.RemoveOption == "google") {
                modalAddress.find('.google-searchbox-container').addClass('d-none');
                modalAddress.find('.msp-searchbox-container input').focus();
            }
        }
    }
    modalAddress.on('change', 'input[name="search-type"]', function (e) {
        let checkBoxContainer = $(this).closest('.searchOptionsContainer');
        if (checkBoxContainer.find('.search-google').is(':checked')) {
            checkBoxContainer.find('.msp-searchbox-container').addClass('d-none');
            checkBoxContainer.find('.google-searchbox-container').removeClass('d-none');
            checkBoxContainer.find('.google-searchbox-container input').focus();
        }
        else if (checkBoxContainer.find('.search-msp').is(':checked')) {
            checkBoxContainer.find('.google-searchbox-container').addClass('d-none');
            checkBoxContainer.find('.msp-searchbox-container').removeClass('d-none');
            checkBoxContainer.find('.msp-searchbox-container input').focus();
        }
    })

    fieldMSPCtrl.on('click', '.msp-lookup .item', function (e) {
        e.stopPropagation();
        let data = this.item.LookupData;
        let cityHTML = '';
        if (data.City) {
            cityHTML = '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' + data.City + '"><span class="city-name">' + data.CityDetail.Name + '</span><i class="fas fa-times"></i></div>';
        }
        let NewAddress = { venuename: data.AccountDetail.Name, address: data.Address, cityHTML: cityHTML, country: data.Country, state: data.State, postalcode: data.PostalCode, AddressFound: true };
        if (data.SearchAddress && data.SearchAddress.lat && data.SearchAddress.lng) {
            NewAddress.lat = data.SearchAddress.lat;
            NewAddress.lng = data.SearchAddress.lng;
        }
        options.NewAddress = NewAddress;
        if (options.callback) {
            options.callback(options);
        }
        modalAddress.modal('hide');
    })

    options.lat = lat = (lat || -33.8688);
    options.lng = lng = (lng || 151.2195);

    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    autocomplete = new google.maps.places.Autocomplete(modalAddress.find('.google-searchbox-container input')[0], { types: ['geocode'] });

    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
    autocomplete.setFields(['address_component', 'geometry']);

    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener('place_changed', function () {
        // Get the place details from the autocomplete object.
        let place = autocomplete.getPlace();
        options.NewAddress = GoogleMaps.ParseGoogleAddress(place);
        if (options.callback) {
            options.callback(options);
        }
        modalAddress.modal('hide');

    });
}
//Events Page: AccountDetail (All Pages where Address Control Added)
GoogleMaps.AddressFieldsChangeEvent = function (searchCtrl) {
    if (searchCtrl) {
        let field = searchCtrl[0].field;
        if (field && field.Schema && field.Schema.LocationFileds) {
            let parentForm = searchCtrl.closest('.page-form.show');
            if (parentForm) {
                for (let keyField in field.Schema.LocationFileds) {
                    if (keyField) {
                        let addressField = parentForm.find('[data-field="' + keyField + '"]:visible');
                        if (addressField) {
                            if (addressField.hasClass('msp-ctrl-lookup') || addressField.hasClass('msp-ctrl-multilookup')) {
                                addressField.on('click', ".fa-times", function () {
                                    searchCtrl.attr("data-lat", "").attr("data-lng", "");
                                });
                                addressField.parent().on('click', ".msp-lookup .item, .msp-multilookup .item", function () {
                                    searchCtrl.attr("data-lat", "").attr("data-lng", "");
                                });
                            }
                            else {
                                addressField.change(function () {
                                    searchCtrl.attr("data-lat", "").attr("data-lng", "");
                                });
                            }
                        }
                    }
                }
            }
        }
    }
}

