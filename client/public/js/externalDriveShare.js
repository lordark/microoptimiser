const _driveExternalShare = {};
const container = $(".drive-share");
const hash = window.location.pathname.split("/").pop();

_driveExternalShare.GetFileDetails = function () {
    let tid = parseInt(localStorage.getItem("tid"));
    if (isNaN(tid)) {
        localStorage.setItem("tid", 0);
        tid = 1;
    } else {
        if (tid > 9999) {
            tid = 0;
        }
        localStorage.setItem("tid", tid + 1)
    }

    $.ajax({
        url: `/noauth/api/driveshare/fileDetails/${hash}/${tid}`,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            if (result.message == "success") {
                _driveExternalShare.BindData(result.data)
            } else if (result.data) {
                //Error
                _driveExternalShare.BindError(result.data, { TemplateContent : result.TemplateContent})
            }
        },
        error: function (e) {
            //Error
            _driveExternalShare.BindError(e.data, { TemplateContent })
        }
    })
}

_driveExternalShare.SendOTPMail = function (email, done) {
    $.ajax({
        url: `/noauth/api/driveshare/sendotp/${email}/${hash}`,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            if (result.message == "success") {
                done(result.data)
            } else {
                if (result.data) {
                    //Error
                    _driveExternalShare.BindError(result.data)
                }
            }
        },
        error: function (e) {
            //Error
            _driveExternalShare.BindError()
        }
    })
}

_driveExternalShare.VerifyOTP = function (otp, done) {
    $.ajax({
        url: `/noauth/api/driveshare/verify/${hash}/${_driveExternalShare.Email}/${otp}`,
        type: "GET",
        contentType: "application/json",
        success: function (result) {
            if (result.message !== "success") {
                _driveExternalShare.BindError(result.data)
            } else {
                done(result.data)
            }
        },
        error: function (e) {
            //Error
            _driveExternalShare.BindError()
        }
    })
}

_driveExternalShare.BindData = function (data) {

    _driveExternalShare.commonBinding(data)
    container.find(".file-name").html(data.FileName)
    container.find(".owner-name").html(data.OwnerData.FirstName + " " + data.OwnerData.LastName)
    container.find(".owner-email").html(data.OwnerData.Email);
    let expDateContent = data.FormattedSentence ? "| " + data.FormattedSentence : ""
    container.find(".sharing-content").html(`${_driveExternalShare.getFileSize(data.FileSize)} in total ${expDateContent}`)

}
_driveExternalShare.commonBinding = function (data) {
    container.find(".sharing-loader").addClass("d-none");
    container.find(".main-sections").removeClass("d-none");
    container.find(".file-details").removeClass("d-none");
    container.find(".dynamic-content").html(data.TemplateContent || _driveExternalShare.DEFAULT_CONTENT)

}

_driveExternalShare.BindError = function (errorCode, data) {
    container.find(".sharing-loader").addClass("d-none");

    switch (errorCode) {
        case "Not_Authorized_Email":
            container.find(".unauthorized-error").text("Your are not authorized to download this file")
            break;
        case "OTP_Not_Matched":
            container.find(".invalid-otp").text("OTP doesn't match")
            break;
        case "OTP_Timeout":
            container.find(".unauthorized-error").text("OTP has expired. Please try again.");
            container.find(".email-container").addClass("active");
            container.find(".otp-container").removeClass("active");
            container.find(".gen-otp").attr("disabled", false);
            container.find(".otp-field").val("");
            break;
        case "Link_Expired":
            _driveExternalShare.commonBinding(data)
            container.find(".file-details").addClass("d-none");
            container.find("#report-mail").modal("hide");
            container.find(".error-sharing").removeClass("d-none");
            container.find(".error-sharing").html(`
            <div class="text-center">
                <p class="display-4 text-danger" >Link Expired!</p> 
                <p class="text-secondary h3">Please ask the admin to regenerate the link or reshare the file.</p>
            </div>
            `)
            break;
        case "Invalid_Hash":
            _driveExternalShare.commonBinding(data)
            container.find(".file-details").addClass("d-none");
            container.find(".error-sharing").removeClass("d-none")
            container.find(".error-sharing").html(`
                <div class="text-center">
                    <p class="display-4 text-danger" >Invalid Link!</p> 
                    <p class="text-secondary h3">Link is not valid. Please check the link.</p>
                </div>
                `)
            break;
        default:
            break;
    }
    // container.find(".error-sharing").removeClass("d-none");

}

_driveExternalShare.getFileSize = function (bytes = 0, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

_driveExternalShare.BindEvents = function () {
    //Email field
    container.find(".external-email").keyup(function () {
        let value = $(this).val();
        container.find(".unauthorized-error").text("")
        if (_driveExternalShare.isValidEmailAddress(value)) {
            container.find(".gen-otp").attr("disabled", false)
        } else {
            container.find(".gen-otp").attr("disabled", true)
        }
    })

    //OTP field
    container.find(".otp-field").keyup(function () {
        let value = $(this).val();
        container.find(".invalid-otp").text("");
        if (value.length == 6) {
            container.find(".verify-btn").attr("disabled", false)
        } else {
            container.find(".verify-btn").attr("disabled", true)
        }
    })

    container.find(".verify-btn").click(function () {
        container.find(".invalid-otp").text("");
        container.find(".mail-send-again").text("");
        let otp = container.find(".otp-field").val();
        $(this).attr("disabled", true);
        _driveExternalShare.VerifyOTP(otp, function (data) {
            let dA = document.createElement("a");
            dA.href = `/noauth/api/driveshare/download/${data}`;
            dA.click()
            container.find("#report-mail").modal("hide");
            container.find(".dwnld-btn").attr("disabled", true);
            container.find(".download-success").removeClass("d-none")
            container.find("")
        })
    })

    //otp button
    container.find(".gen-otp").click(function () {
        $(this).attr("disabled", true)
        let email = container.find(".external-email").val();
        if (!_driveExternalShare.isValidEmailAddress(email)) {
            return
        }

        _driveExternalShare.SendOTPMail(email, () => {
            _driveExternalShare.Email = email;
            container.find(".email-container").removeClass("active");

            container.find(".external-email").html(email)
            container.find(".otp-container").addClass("active")
        })


    })

    container.find(".otp-again").click(function (e) {
        e.preventDefault()
        container.find(".invalid-otp").text("");
        container.find(".mail-send-again").text("");
        _driveExternalShare.SendOTPMail(_driveExternalShare.Email, () => {
            container.find(".mail-send-again").text("mail sent successfully.")
        })
    })
}

_driveExternalShare.isValidEmailAddress = function (emailAddress) {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(String(emailAddress).toLowerCase());

}

$(
    function () {
        _driveExternalShare.GetFileDetails();
        _driveExternalShare.BindEvents();

    }
)

_driveExternalShare.DEFAULT_CONTENT = `<h1 class="text-light mt-4 pt-4 ">
Business software that doesn't require the hard sell
</h1>

<p class="pt-4 text-light lead" style="text-align: justify;">Optimiser is a single subscription platform, providing customer
engagement and sales tools, marketing automation, data storage, file sharing and much more all in
one place. The platform helps to increase the bottom line by providing effective communication and
focus among users, as well as greater supervision and traking. Shoot and enquiry now to get your
free demo of the platform. </p>
	`