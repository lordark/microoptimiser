const _account = {};

//Events Page: AccountDetail
_account.InitAccountDetail = function (page) {
    let accountID = page._id;
    _common.GetStaticPage('/page/account/DetailRightSection.html', function (res) {
        let rightSection = page.ObjectPage.find('.object-detail-right').html(res);

        //Get Subsidiaries
        let childAccountObj = { pageName: 'AccountList' };
        childAccountObj.match = { 'ParentAccountID': _controls.ToEJSON('objectid', accountID) };
        childAccountObj.callback = function (result) {
            if (result.data.length > 0) {
                let container = '<ul>';
                for (let i = 0; i < result.data.length; i++) {
                    container += '<li><a href="#">' + result.data[i].Name + '</a></li>';
                }
                container += '</ul>';
                rightSection.find('.subsidiaries').removeClass('d-none').find('.card-body').html(container);
            }
        }
        _common.GetObjectList(childAccountObj);

        //Get Activity Chart
        _chart.ActivityCountMonthWise('Account', accountID, 'past', rightSection.find('.activity-chart .card-body .tab-pane[display-type="past"]')[0]);
        rightSection.find('.activity-chart .card-header .nav-item').on('click', function (e) {
            e.stopPropagation();
            let displayType = $(this).attr('display-type');
            rightSection.find('.activity-chart .card-header .nav-item').removeClass('active');
            $(this).addClass('active');
            rightSection.find('.activity-chart .card-body .tab-pane').addClass('d-none');
            rightSection.find('.activity-chart .card-body .tab-pane[display-type="' + displayType + '"]').removeClass('d-none');
            _chart.ActivityCountMonthWise('Account', accountID, displayType, rightSection.find('.activity-chart .card-body .tab-pane[display-type="' + displayType + '"]')[0]);
        })

    });
}

//Events Page: AccountContactRelationshipList>PageButtons>AddContact
_account.AddContactForm = function (page) {
    let param = { pageName: 'ContactDetail' };
    let gridObj = page.CurrentClickElement.closest('.msp-data-grid')[0].grid;
    let accountID = page.CurrentClickElement.closest('.object-detail')[0].obj.Data._id;
    let pageDetailData = page.param.PageDetail.Data;
    let phone = pageDetailData.Phone;
    param.MatchFields = [{ Name: 'AccountID', Value: accountID }, { Name: 'Phone', Value: phone }];
    param.onSave = function () {
        gridObj.ReloadData();
    }
    new PageForm(param);
}

_account.AddFreeTextArea = function (result, callback) {
    if (result.content != undefined) {
        $.ajax({
            url: '/api/account/addfreetextarea',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(result),
            success: function (res) {
                if (res.message == 'success') {
                    callback()
                }
            },
        });
    }
}

_account.GetFreeTextArea = function (accountID, callback) {
    if (accountID != undefined) {
        $.ajax({
            url: '/api/account/getfreetextarea',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({ sdata: accountID }),
            success: function (res) {
                callback(res);
            },
        });
    }
}
