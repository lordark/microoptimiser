const notification = {};
notification.ReminderArray = {};
$(function() {


    $(document).on('click', '.notification-div .notification-item', function() {
        let sdata = {};
        sdata.Objectname = $(this).data('objectname');
        sdata.Objectid = $(this).data('objectid');
        sdata.NotiId = $(this).data('id');
        sdata.onClick = function(record) {
            if (record.message == "success") {
                switch (sdata.Objectname) {
                    case "DriveStorageUpgrade":
                        new Page({ "pageName": "DriveSettings" });
                        break;
                    case "Activity":
                        new Page({"pageName":"Calendar"});
                        let activity = record.data;
                        let param = {};
                        param._id = activity._id;
                        let userAttendeeData = activity.AttendeesData.find(attendee => attendee.Id == _initData.User._id);
                        if (userAttendeeData && activity.IsActive) {
                            if (userAttendeeData.Status == "pending")
                                param.pending = true;
                            _calendar.ReadOnlyActivityForm(param);
                        }
                        break;
                        case "DriveShare":
                            //If drive is already active then click on button to open share
                            if($('#page-nav .nav-item[data-uid="MyDrive"]').length==0){
                                window.location.hash="share"
                            }else{
                                $('[data-viewtype="shareWithMe"]').click()
                            }
                            new Page({ "pageName": "MyDrive" });
                           break;
                    default:
                        break;
                }

            }

        }
        notification.GetNotificationDetailsByID(sdata);

        notification.GetUnseenNotifications(function(result) {
            $('.notification-div').find('.notification-item').remove();
            notification.UpdateNotificationList(result);
        });
    })

    $(".dropdown #notificationDropdown").on("click", function() {
        $("#notificationDropdown i.fa-bell").removeClass("bell-swing")
    })
    setInterval(() => {
        let momentCurrentTime = moment(new Date());
        for (var i = notification.ReminderArray.length - 1; i >= 0; i--) {
            let reminder = notification.ReminderArray[i];
            let eventTime = moment(reminder.EventStartTime);
            if (eventTime >= momentCurrentTime) {
                let reminderTime = eventTime.subtract(reminder.Number, reminder.Parameter);
                if (reminderTime <= momentCurrentTime && reminderTime > momentCurrentTime.subtract(2, 'm')) {
                    let message = notification.GetReminderMessage(reminder);
                    if (reminder.Notification) {
                        alert(message);
                    }
                    if (reminder.Mail) {
                        notification.sendReminderMail(reminder);
                    }
                    notification.ReminderArray.splice(i, 1);
                }
            }
        }
    }, 60000);

    setTimeout(() => {
        notification.GetUnseenNotifications(function(result) {
            $('.notification-div').find('.notification-item').remove();
            notification.UpdateNotificationList(result);
        });
    }, 3000);
})

notification.GetReminderMessage = function(reminder) {

    let message = '';
    message = reminder.Number + ' ';
    let parameter = '';
    switch (reminder.Parameter) {
        case 'm':
            parameter = 'minute(s)'
            break;
        case 'h':
            parameter = 'hour(s)'
            break;
        case 'd':
            parameter = 'day(s)'
            break;
        case 'w':
            parameter = 'minute(s)'
            break;
    }
    message += parameter + ' left before ' + reminder.ObjectName;

    if (reminder.ObjectName == "Activity") {
        message += ' "' + reminder.ObjectData.Subject + '"\n';
    }
    let eventTime = moment(reminder.EventStartTime).tz(_initData.User.MySettings.Timezone);
    message += reminder.ObjectName + ' is scheduled to be on ' + eventTime.toString();

    return message;

}



notification.sendReminderMail = function(reminder, callback) {
    let sdata = {};
    sdata.Subject = "Reminder for " + reminder.ObjectName + " " + reminder.ObjectData.Subject;
    sdata.Html = notification.GetReminderMessage(reminder);
    sdata.User = reminder.CreatedBy;

    $.ajax({
        url: '/api/event/sendnotificationmail',
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(sdata),
        success: function(result) {
            //console.log(result);
        },
        error: function(err) {
            console.log(err);
        }
    })

}

notification.GetNotificationIcon = function(objectName) {
  
    let iClass = ''
    switch (objectName) {
        case "Activity":
            iClass = '<i class="fas fa-calendar-alt text-white"></i>';
            break;
        case "Drive":
        case "DriveStorageUpgrade":
            iClass = '<i class="fas fa-folder-open text-white"></i>';
            break;
        case "DriveShare":
            iClass = '<i class="fas fa-file text-white"></i>';
            break;
        default:
            break;
    }

    return iClass;
}

notification.UpdateNotificationList = function(notifications) {
    $(".notification-div .no-new-noti").remove()
    if (notifications.length > 0) {
        if (_initData.User) {
            if (!_initData.User.MySettings)
                return;
        } else {
            return;
        }
        $('#notificationDropdown .badge-counter').html(notifications.length);

        for (let i = 0; i < notifications.length; i++) {
            let notifTime = moment(notifications[i].CreatedDate).tz(_initData.User.MySettings.Timezone).format('MMMM Do YYYY')
            let nhtm = $('<a class="dropdown-item d-flex align-items-center notification-item" href="#" data-objectname="' + notifications[i].EventObjectName + '" data-objectid="' + notifications[i].EventID + '" data-id="' + notifications[i]._id + '"></a>');
            if (notifications[i].EventObjectName == "Activity") {
                nhtm.addClass("activity-notification");
            }
            nhtm.append('<div class="mr-3"><div class="icon-circle bg-primary">' + notification.GetNotificationIcon(notifications[i].EventObjectName) + '</div></div>');
            nhtm.append('<div><div class="small text-gray-500">' + notifTime + '</div><span class="font-weight-bold small">' + notifications[i].Message + '</span></div>')
            $(nhtm).insertBefore('.notification-div .openAllNotificationLink');
        }
    } else {
        $('#notificationDropdown .badge-counter').html('');
        let nhtm = '<div class="dropdown-item text-center small text-gray-500 no-new-noti">No New Notification</div>';
        $(nhtm).insertBefore('.notification-div .openAllNotificationLink');
    }

}

notification.UpdateReminderArray = function() {
    notification.GetReminderList(function(result) {
        if (result.message == "success") {
            notification.ReminderArray = result.resultArray;
        }
    })
}

notification.SetNotifications = function(sdata) {
    $.ajax({
        url: "/api/common/setnotifications",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(sdata),
        success: function(result) {
            //console.log(result);
        },
        error: function(err) {
            console.log(err);
        }
    })
}

// notification.SetNotifications({
//     Message:'new noti',
//     EventName:'Activity',
//     EventId:{$oid:'5d821ce4cf0c48663be69947'},
//     Users:[{
//         UserId:{:'5d821ce4cf0c48663be69947'},
//         IsSeen:false
//     }]
// })

notification.resetActivitiesAfterReschedult = function(data) {
    $.ajax({
        url: "/api/common/resetActivityAfterReschedule",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(data),
        success: function(result) {
            //console.log(result);
        },
        error: function(err) {
            console.log(err);
        }
    })
}

notification.GetParticipantForNotification = function(data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "/api/common/getParticipantForNotifications",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function(result) {
                resolve(result)
            },
            error: function(err) {
                resolve(null)
            }
        })
    })
}

notification.GetUnseenNotifications = function(callback) {
    $.ajax({
        url: '/api/common/getunseenusernotifications',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        success: function(res) {
            callback(res);
        },
    })
}

notification.GetNotificationDetailsByID = function(sdata, callback) {
    $.ajax({
        url: '/api/common/getnotificationdetailsbyid',
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(sdata),
        contentType: 'application/json',
        success: function(res) {
            if (sdata.onClick != undefined)
                sdata.onClick(res);
            //callback(res);
        }
    })
}

notification.GetReminderList = function(callback) {
    $.ajax({
        url: '/api/event/getreminderlist',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        success: function(res) {
            callback(res);
        }
    })
}