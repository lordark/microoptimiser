const _store = {};
_store.AppDetailInit = function (page) {
  page.PageContainer.find('.disabled-config').click(function () {
    swal({
      title: "Are you sure?",
      text: "Do you want to disabled your opera integration!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          swal("Your integration has been disabled successfully!", {
            icon: "success",
          });
        } else {
          swal("Your integration setting is safe!");
        }
      });
  })
}

_store.LoadApps = function (page) {
  _store.GetAppList(page);

  // page.PageContainer.find('.add-now').click(function(){    
  //   new Page({ 'pageName': 'StoreDetail', 'AppID': '1' });
  // });

  page.PageContainer.on('click', '.add-now', function () {
    new Page({ 'pageName': 'AppDetail', 'AppID': $(this).closest('.card').attr('data-appid') });
  });


}

_store.GetAppList = function (page) {
  _common.FetchApiByParam({
    url: "/api/store/getapplist"
  }, function (result) {
    if (result && result.status == "ok") {
      if (result.data) {
        let itemHtml = '';
        for (let i = 0; i < result.data.length; i++) {
          let item = result.data[i];
          itemHtml += '<div class="card" data-appid="' + item._id + '">';
          itemHtml += '<a href="#" class="card-img-top d-block" >';
          itemHtml += '<img src="' + item.IconLink + '" height="80" alt="Card image cap">';
          itemHtml += '</a>';

          itemHtml += '<div class="card-body">';
          for (let j = 0; j < item.Industry.length; j++) {
            itemHtml += '<p class="btn btn-sm btn-outline-light mb-1">' + item.Industry[j] + '</p>';
          }
          itemHtml += '<a class="card-title h5 d-block">' + item.DisplayName + '</a>';

          itemHtml += '<p class="card-text">' + item.Summary + '</p>';
          itemHtml += '</div>';
          itemHtml += '<div class="card-footer bg-white border-0 d-flex">';
          itemHtml += '<div>';
          itemHtml += '<a href="#" class="btn btn-sm btn-primary add-now">Add Now</a>';
          itemHtml += '</div>';
          if (item.Trial.TrialAvailable) {
            itemHtml += '<div class="ml-auto text-right">';
            itemHtml += '<p class="text-primary font-weight-bold mb-0">Free ' + item.Trial.TrialValue + ' ' + item.Trial.TrialUnit + ' trial period</p>';
            itemHtml += '</div>';
          }
          itemHtml += '</div>';
          itemHtml += '</div>';

          page.PageContainer.find('.card-deck').append(itemHtml);

        }
      } else {
        swal(result.message, { icon: "info" });
      }
    }
  });

  page.PageContainer.find('.backup-data').click(function () {
    new Page({ 'pageName': 'BackupDetail', 'AppID': '2' });
  })

}

_store.InitStoreDetail = function (page) {
  let sdata = {}
  sdata.appID = _controls.ToEJSON("objectid", page.param.AppID);
  _common.FetchApiByParam({
    url: "/api/store/getappdetail",
    data: sdata,
  }, function (result) {
    debugger
    if (result && result.status == "ok") {
      if (result.data) {
        let item = result.data[0];
        page.PageContainer.find(".object-picture").attr('src', item.IconLink);
        page.PageContainer.find(".object-name").html(item.DisplayName);
        for (let i = 0; i < item.Industry.length; i++) {
          page.PageContainer.find(".object-industry").append(item.Industry[i] + " | ");
        }
        page.PageContainer.find(".object-industry").append(item.ProductCompany);
        if (item.Trial.TrialAvailable) {
          page.PageContainer.find(".page-top-button .btn-edit-object").html('Start free ' + item.Trial.TrialValue + ' ' + item.Trial.TrialUnit + ' trial period');
        } else {

        }
        page.PageContainer.find(".page-top-button .text-secondary").html(item.BasicPlan);
        page.PageContainer.find(".row .tiles").addClass("d-none");
        page.PageContainer.find(".object-detail-right .collapse-head").html(item.ObjectRightHead);
        page.PageContainer.find(".object-detail-right .card-body").html(item.ObjectRightBody);
      }
    }
  });
}
