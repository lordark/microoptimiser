
const _emailsync = {};

//Events Page: EmailSyncDetail
_emailsync.InitEmailSyncDetail = function (page) {

    let mailID = page.param.record._id;
    let container = page.PageContainer;
    _common.Loading(true)
    GetEmailContent(mailID).then(({ data, message }) => {
        if (data == "error") {
            _common.Loading(false)
            swal(message, { icon: "error" });
        } else {
            let res = data
          _page.nav.find('.nav-item.active[data-uid="' + _page.nav.find('.nav-item.active').attr('data-uid') + '"] .title').text(res.Subject);
            let EmaiAddressContainer = container.find('.emailaddress');
            EmaiAddressContainer.find('.from').text(res.From);
            let ToArray = res.To.split(',')
            for(let i=0 ; i < ToArray.length ; ++i ){
                EmaiAddressContainer.find('.to').append(`<span class="d-inline-block">${ToArray[i]} ${(ToArray.length-1  == i)? '' : ','} </span> `);
            }
            EmaiAddressContainer.find('.emaildate').text(moment(res.Date).tz(_initData.User.MySettings.Timezone).format('hh:mm A, DD MMM YYYY').toUpperCase());
            container.find('.subject h2').text(res.Subject);
            if (res.BCC != null || res.CC != null) {
                let CCBCC = container.find('.CCBCC');
                CCBCC.removeClass('d-none');
                if (res.CC != null) {
                    let CCcontainer = CCBCC.find('.CC').removeClass('d-none');
                    let cc = res.CC.split(',');
                    for(let j=0; j< cc.length ; ++j){
                        CCcontainer.append(`<span class="d-inline-block">${cc[j]} ${(cc.length - 1  == j)? '' : ',' } </span>`)
                    }
                }
                if (res.BCC != null) {
                    let bcc = res.BCC.split(',');
                    let BCCcontainer = CCBCC.find('.BCC').removeClass('d-none');

                    for(let j=0; j< bcc.length ; ++j){
                        BCCcontainer.append(`<span class="d-inline-block">${bcc[j]} ${(bcc.length - 1  == j)? '' : ',' } </span>`)
                    }

                }
            }
            var iframe =container.find('.mail-body-frame')[0];
            iframeWindow = iframe.contentWindow || ( iframe.contentDocument.document || iframe.contentDocument);
            iframeWindow.document.open();
            iframeWindow.document.write(res.Html);
            iframeWindow.document.close();
            $(iframeWindow.document).find('a').on('click',(e)=>{
                event.preventDefault();
                if( e.currentTarget && 'href' in e.currentTarget && !e.currentTarget.href.includes('mailto:')){
                    window.open(e.currentTarget.href)
                }else if ( e.currentTarget && 'href' in e.currentTarget && e.currentTarget.href.includes('mailto:') ) {
                 const removedMailto = e.currentTarget.href.split(':')[1];
                 const MailToEmailId = removedMailto.split('?')[0];   


                }

            });
            iframe.style.height=iframeWindow.document.body.scrollHeight +'px';
            let AttachmentContainer = container.find('.mail-attachment');
 
            if (res.AllAttachments.length > 0) {
                let FileNumber = res.AllAttachments.length
                let FileCount = FileNumber.toString() + ((FileNumber > 1) ? ' Files' : '  File');
                AttachmentContainer.removeClass('d-none')
                if (FileNumber > 1) {
                    AttachmentContainer.find('.AttachmentCount').text(FileCount)
                    AttachmentContainer.find('p').append(`<a href=/api/emailsync/downloadall/${mailID} target="_blank" >Download all</a>`)
                }
                if(FileNumber == 1){
                    AttachmentContainer.find('.AttachmentCount').addClass('d-none')
                }else{
                    AttachmentContainer.find('.AttachmentCount').removeClass('d-none')
                }

                for (let i = 0; i < res.AllAttachments.length; ++i) {
                    AttachmentContainer.find('.fileName').append(`<li> <a href=${res.AllAttachments[i].Attachment}><i class="fa fa-link"></i>${res.AllAttachments[i].FileName}</a> </li>`)
                }
            }


            container.find('.printemail').off('click').on('click', function () {
                window.print();
            })
            _common.Loading(false)
        }
    }).catch((error) => {
        _common.Loading(false)
    })
}
function print_specific_div_content(container) {
    var win = window.open('', '', 'left=0,top=0,width=552,height=477,toolbar=0,scrollbars=0,status =0');
    var handler = function () {
        win.print();
        win.close();
    };
    if (win.addEventListener)
        win.addEventListener('load', handler);
    else if (win.attachEvent)
        win.attachEvent('onload', handler);

    var content = "<html>";
    content += container;
    content += "</body>";
    content += "</html>";
    win.document.write(content);
    win.document.close();
}
function GetEmailContent(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/emailsync/getemail/${id}`,
            type: 'GET',
            contentType: "application/json",
            success: function (result) {
                resolve(result)
            },
            error: function (result) {
                reject(result)
            }
        });



    })

}
