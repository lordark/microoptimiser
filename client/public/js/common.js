const _common = {};
$(function () {
    $.fn.inlineStyle = function (prop) {
        var styles = this.attr("style"),
            value;
        styles && styles.split(";").forEach(function (e) {
            var style = e.split(":");
            if ($.trim(style[0]) === prop) {
                value = style[1];
            }
        });
        return value;
    };
})



_common.GetObjectData = function (param) {
    let sdata = { 'pagename': param.pageName, '_id': param._id };
    $.ajax({
        url: '/api/common/getobjectdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
        error: function (res) {
            param.callback(res.responseJSON);
        }
    });
}

_common.AddObjectData = function (sdata, callback) {
    $.ajax({
        url: '/api/common/addobjectdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            callback(res);
        },
    });
}

_common.CompareStringValue = function (a, b) {
    if (a.trim().toLowerCase() === b.trim().toLowerCase())
        return true;
    else
        return false;
}

_common.UpdateObjectData = function (sdata, callback) {
    $.ajax({
        url: '/api/common/updateobjectdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            callback(res);
        },
    });
}
_common.UpdateEmbededObjectData = function (sdata, callback) {
    $.ajax({
        url: '/api/common/updateembededobjectdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            callback(res);
        },
    });
}
_common.UpdateGridData = function (sdata, callback) {
    $.ajax({
        url: '/api/common/updategriddata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            callback(res);
        },
    });
}

_common.DeleteMultipleObjectData = function (param) {
    let sdata = { 'pageName': param.pageName };
    sdata.ids = param.ids;
    $.ajax({
        url: '/api/common/deletemultipleobjectdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
    });
}

_common.GetGridData = function (param) {
    let sdata = { 'pagename': param.pageName };
    sdata.limit = (param.pageSize == undefined) ? 20 : param.pageSize;
    sdata.skip = (param.currentPage == undefined || param.currentPage == 1) ? 0 : (param.currentPage - 1) * sdata.limit;
    sdata.sort = param.sort;
    sdata.headerFilters = param.headerFilters;
    sdata.mustMatch = param.mustMatch;
    if (param.listFilter != undefined)
        sdata.listFilter = param.listFilter.Name;
    $.ajax({
        url: '/api/common/getgriddata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
        error: function (err) {
            param.callback(err.responseJSON);
        }
    });
}

_common.GetObjectList = function (param) {
    let sdata = { 'pagename': param.pageName };
    sdata.sort = param.sort;
    sdata.match = param.match;
    $.ajax({
        url: '/api/common/getobjectlist',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
    });
}

_common.GetInitData = function (callback) {
    $.ajax({
        url: '/api/common/getinitdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            callback(res);
        },
    });
}

_common.GetLookupList = function (param) {
    let sdata = { 'objectname': param.objectName, 'fieldname': param.fieldName, 'text': param.text != undefined ? param.text.trim() : "", 'multilookupfield': param.multilookupfield };
    if (param.Collections) { sdata.Collections = param.Collections.slice() };
    sdata.match = param.match;
    sdata.limit = param.limit;

    let url = '/api/common/getlookuplist';
    if (sdata.text.charAt(0) == "#" && param.multilookupfield) {
        sdata.text = sdata.text.substr(1);
        url = '/api/tag/gettaglookuplist';
    }
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
    });
};

_common.GetLookupData = function (object, field, id, callback) {
    $.ajax({
        url: '/api/common/getlookupdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ 'objectname': object, 'fieldname': field, '_id': id }),
        success: function (res) {
            callback(res.data);
        },
    });
};

_common.GetCollectionLookupData = function (lookupObject, lookupFields, arrIDs, callback) {
    $.ajax({
        url: '/api/common/getcollectionlookupdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ 'lookupobject': lookupObject, "lookupfields": lookupFields, 'arrids': arrIDs }),
        success: function (res) {
            callback(res.data);
        },
    });
};

_common.ObjectCount = function (param) {
    let sdata = { 'name': param.name };
    sdata.match = param.match;
    $.ajax({
        url: '/api/common/objectcount',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
    });
}
_common.CountUniqueFields = function (param) {
    let sdata = { 'name': param.name };
    sdata.match = param.match;
    let count = 0;
    $.ajax({
        url: '/api/common/countuniquefields',
        async: false,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            count = res;
        },
    });
    return count;
}

_common.GetStaticPage = function (url, callback) {
    $.ajax({
        url: url,
        success: function (res) {
            callback(res);
        },
    });
}

_common.GetPageDetail = function (pageName, callback) {
    let sdata = { 'pagename': pageName };
    $.ajax({
        url: '/api/common/getpagedetail',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            if (res.Groups != undefined) {
                res.Groups.sort(function (a, b) { return a.Sequence - b.Sequence });
            }
            if (res.Fields != undefined)
                res.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
            callback(res);
        },
    });
}

_common.GetPageDetailWithUserPage = function (pageName, callback) {
    let sdata = { 'pagename': pageName };
    $.ajax({
        url: '/api/common/getpagedetailwithuserpage',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            if (res.PageData.Groups != undefined) {
                res.PageData.Groups.sort(function (a, b) { return a.Sequence - b.Sequence });
            }
            if (res.PageData.Fields != undefined)
                res.PageData.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
            callback(res);
        },
    });
}

_common.FetchApiByParam = function (param, callback) {
    if (param.type == undefined)
        param.type = 'post';

    $.ajax({
        url: param.url,
        type: param.type,
        contentType: 'application/json',
        data: JSON.stringify(param.data),
        headers: param.headers,
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            callback(err);
        }
    })
}
_common.b64toBlob = function (b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}

_common.UploadFiles = function (param, callback) {

    $.ajax({
        url: param.url,
        type: param.type,
        contentType: 'application/json',
        data: JSON.stringify(param.data),
        cache: false,
        method: 'POST',
        headers: {
            'socket': sessionStorage.getItem('socket-id')
        },

        success: function (res) {
            callback(res);
        },
        error: function (err) {
            callback(err);
        }
    })
}

_common.Resize = function (src, width, ext, callback) {
    if (!ext) {
        ext = "jpg";
    }
    let img = document.createElement('img');
    img.onload = function () {
        let oc = document.createElement('canvas');
        let ctx = oc.getContext('2d');
        let scale = width / img.width;
        oc.width = img.width * scale;
        oc.height = img.height * scale;
        ctx.drawImage(img, 0, 0, oc.width, oc.height);
        //ctx.drawImage(img, 0, 0);
        // ctx.drawImage(oc, 0, 0, oc.width*0.5, oc.height*0.5);
        //ctx.drawImage(oc, 0, 0, oc.width, oc.height,);
        //ctx.drawImage(oc, 0, 0, oc.width*0.5, oc.height*0.5,0,0,oc.width,oc.height);
        let imageData = ctx.getImageData(0, 0, oc.width, oc.height);

        let pixels = imageData.data;
        let alpha = 0.2;
        let alpha1 = 1 - alpha;

        // Pick the best array length
        let rl = Math.round(ctx.canvas.width * 3.73);
        let randoms = new Array(rl);

        // Pre-calculate random pixels
        for (let i = 0; i < rl; i++) {
            randoms[i] = Math.random() * alpha + alpha1;
        }

        // Apply random pixels
        for (let i = 0, il = pixels.length; i < il; i += 4) {
            pixels[i] = (pixels[i] * randoms[i % rl]) | 0;
        }

        ctx.putImageData(imageData, 0, 0);
        callback(oc.toDataURL("image/" + ext));
    }
    img.src = src;
}

_common.FetchXMLHttpRequestByParam = function (obj, callback) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function () {
        callback(xhr.response);
    };
    xhr.open('GET', obj.url);
    xhr.responseType = obj.responseType;  //blob
    xhr.send();
}

_common.GetObjectSchemaDetail = function (param) {
    let sdata = { 'objectname': param.objectName };
    sdata.fields = param.fields;
    $.ajax({
        url: '/api/common/getobjectschemadetail',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
    });
}

_common.SaveListViewFilter = function (param) {
    let sdata = { 'pagename': param.pageName, 'actiontype': param.actionType, 'filter': param.filter, };
    $.ajax({
        url: '/api/common/savelistviewfilter',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
    });
}

_common.GetPageDetailFromInitData = function (name) {
    for (let i = 0; i < _initData.Page.length; i++) {
        if (_initData.Page[i].PageName == name) {
            return _initData.Page[i];
        }
    }
    return false;
}

_common.GetListSchemaDataFromInitData = function (schemaName) {
    let result = [];
    for (let i = 0; i < _initData.ListSchema.length; i++) {
        if (_initData.ListSchema[i].Name == schemaName) {
            result = _initData.ListSchema[i].Data.filter(x => x.IsActive == true);
            break;
        }
    }
    return result;
}

_common.GetGMTTimezone = function (listArr, key) {
    let result = '';
    if (listArr.length) {
        for (let i = 0; i < listArr.length; i++) {
            if (listArr[i]['Key'] == key)
                result = listArr[i]['Value'];
        }
    }
    let returnData = result.trim()
    return returnData.substr(returnData.indexOf("(") + 1, returnData.indexOf(")") - 1);
}

_common.GetActivityTypeIcon = function (listArr, key) {
    let result = '';
    if (listArr.length) {
        for (let i = 0; i < listArr.length; i++) {
            if (listArr[i]['Key'] == key)
                result = listArr[i]['Icon'];
        }
    }
    return result;
}

_common.GetCompanyCurrencySymbol = function () {
    //Default British Pound
    let result = '£';
    let currency = _initData.Company.Setting.Currency;
    let currencyList = _common.GetListSchemaDataFromInitData('Currency');
    if (currencyList != undefined && currency != undefined) {
        result = currencyList.find(x => x.Key == currency).Symbol;
    }
    return result;
}

_common.GetDBListSchemaData = function (fieldSchema) {
    let result = [];
    let dataObj = _DBListSchemaData[fieldSchema.LookupObject];
    if (dataObj) {
        result = dataObj;
    }
    else {
        let options = { "LookupObject": fieldSchema.LookupObject, "LookupFields": fieldSchema.LookupFields, 'MatchCondition': fieldSchema.MatchCondition };
        result = _common.GetCollectionDataForSelect(options);
        _DBListSchemaData[fieldSchema.LookupObject] = result;
    }
    return result;
}
_common.ResetDBListSchemaData = function (objectName) {
    if (_DBListSchemaData[objectName]) {
        delete _DBListSchemaData[objectName];
    }
}

_common.ConvertDateByTimezone = function (panel, val, format) {
    //incomplete
    let timeZone = _initData.User.MySettings.Timezone;
    let el = panel.find('select.timezone');
    if (el.length > 0) {
        timeZone = el.val();
    }
    return moment.tz(val, format, timeZone).format();
}

_common.GetObjectByKeyValueFromList = function (list, key, val) {
    if (list && list.length > 0) {
        for (let i = 0; i < list.length; i++) {
            if (list[i][key] == val)
                return list[i]
        }
    }
    return undefined;
}

_common.FormatForApiName = function (string) {
    let result = string.replace(/[^\A-Za-z0-9_\s]/gi, '').trim().replace(/\s+/g, "_");
    if (!isNaN(result.charAt(0))) {
        result = 'X' + result;
    }

    return result;
}

_common.isValidUrl = function (url) {
    var myVariable = url;
    if (/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/i.test(myVariable)) {
        return true;
    } else {
        return false;
    }
}

//Validation part start
_common.Validate = function (panel) {
    let isValid = true;
    panel.find('.vld-message').remove();
    panel.find('.has-error').removeClass('has-error');

    panel.find('input[type="text"],input[type="password"],textarea').each(function () {
        $(this).val($.trim($(this).val().replace(/\'/g, "’")));
    });

    panel.find('.msp-ctrl[data-required]:visible,.msp-ctrl-email:visible').each(function () {
        let el = $(this);
        let nodeName = this.nodeName.toLowerCase();
        if (el.attr('data-required') != undefined) {
            if (nodeName == 'input' || nodeName == 'textarea') {
                if (el.attr('type') == 'text' || el.attr('type') == 'password' || nodeName == 'textarea') {
                    if (el.val() == '') {
                        isValid = false;
                        _common.ValidateAddMsg(el, el.attr('title') + ' is required.');
                    }
                }
            }
            else if (nodeName == 'select') {
                if (el.find('option:selected') == undefined || el.find('option:selected').val() == "" || el.find('option:selected').val() == el.attr('data-reqval')) {
                    isValid = false;
                    _common.ValidateAddMsg(el, el.attr('title') + ' is required.');
                }
            }
            else if ((el.hasClass('msp-ctrl-lookup') || el.hasClass('msp-ctrl-multilookup')) && el.find('.lookup-item').length == 0) {
                isValid = false;
                _common.ValidateAddMsg(el, el.attr('title') + ' is required.');
            }
            else if (el.hasClass('msp-ctrl-texteditor')) {
                let text = el.find('.texteditor').html();
                if (text == '' || text == '<p><br></p>') {
                    isValid = false;
                    _common.ValidateAddMsg(el, el.attr('title') + ' is required.');
                }
            }
            else if (el.hasClass('msp-ctrl-file') && el.closest('form').find('.div-file-uploading .file-data').length == 0) {
                isValid = false;
                _common.ValidateAddMsg(el, el.attr('title') + ' is required.');
            }
            else if (el.hasClass('msp-ctrl-location') && (el.attr('data-lat') == "" || !el.attr('data-lng') == "")) {
                isValid = false;
                _common.ValidateAddMsg(el, el.attr('title') + ' is required.');
            }
        }
        if (el.hasClass('msp-ctrl-email') == true && el.val() != '') {
            if (_common.isValidEmailAddress(el.val()) == false) {
                isValid = false;
                _common.ValidateAddMsg(el, 'Enter valid email.');
            }
        }
    });

    return isValid;
}

_common.isValidEmailAddress = function (emailAddress) {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(String(emailAddress).toLowerCase());

}
_common.isValidCommaSepratedEmailAddress = function (emailAddress) {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailAddress.length > 0) {
        let email = emailAddress.split(',');
        for (let i = 0; i < email.length; i++) {
            if (email[i] != undefined && email[i] != '') {
                if (!pattern.test(String(email[i].trim()).toLowerCase())) {
                    return false;
                }
            }
        }
    } else {
        return false;
    }
    return true;
}

_common.ValidateAddMsg = function (el, message) {
    if (!el.closest('.vld-parent').hasClass('has-error'))
        el.closest('.vld-parent').addClass('has-error').append('<p class="help-block vld-message">' + message + '</p>');
}
_common.RemoveValidation = function (panel) {
    panel.find('.has-error').removeClass('has-error');
    panel.find('.vld-message').remove();
}
_common.ConfirmDelete = function (param) {

    if (param === undefined) {
        param = {};
        param.title = 'Are you sure?';
        param.message = 'Once deleted, you will not be able to recover this!';
    }
    else if (param.title === undefined)
        param.title = 'Are you sure?';
    else if (param.message === undefined)
        param.message = 'Once deleted, you will not be able to recover this!';

    return swal({
        title: param.title,
        text: param.message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
}

_common.PagerService = function (totalItems, currentPage, pageSize) {
    // default to first page
    currentPage = currentPage || 1;

    // default page size is 10
    pageSize = pageSize || 10;

    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    let startPage, endPage;
    if (totalPages <= 10) {
        // less than 10 total pages so show all
        startPage = 1;
        endPage = totalPages;
    } else {
        // more than 10 total pages so calculate start and end pages
        if (currentPage <= 6) {
            startPage = 1;
            endPage = 10;
        } else if (currentPage + 4 >= totalPages) {
            startPage = totalPages - 9;
            endPage = totalPages;
        } else {
            startPage = currentPage - 5;
            endPage = currentPage + 4;
        }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    //let pages = _.range(startPage, endPage + 1);
    let pages = [];
    for (let i = startPage; i <= endPage; i++) {
        pages.push(i);
    }

    // return object with all pager properties required by the view
    return {
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages
    };

}

_common.BuildPaging = function (pager, container) {
    let pagingHTML = '<ul class="pagination">';
    if (pager.pages.length > 1) {
        pagingHTML += '<li pid="1" class="page-item' + (pager.currentPage === 1 ? ' disabled' : '') + '"><a href="#" class="page-link">First</a></li>';
        pagingHTML += '<li pid="' + (pager.currentPage - 1) + '" class="page-item' + (pager.currentPage === 1 ? ' disabled' : '') + '"><a href="#" class="page-link">Previous</a></li>';
        for (let i = 0; i < pager.pages.length; i++) {
            let page = pager.pages[i];
            pagingHTML += '<li pid="' + page + '" class="page-item' + (pager.currentPage === page ? ' active' : '') + '""><a href="#" class="page-link">' + page + '</a></li>';
        }
        pagingHTML += '<li pid="' + (pager.currentPage + 1) + '" class="page-item' + (pager.currentPage === pager.totalPages ? ' disabled' : '') + '"><a href="#" class="page-link">Next</a></li>';
        pagingHTML += '<li pid="' + pager.totalPages + '" class="page-item' + (pager.currentPage === pager.totalPages ? ' disabled' : '') + '"><a href="#" class="page-link">Last</a></li>';
    }
    pagingHTML += '</ul>';
    container.html(pagingHTML).find('.pagination')[0].pager = pager;
}

_common.Loading = function (show) {
    if (show)
        _page.mainLoading.addClass('active');
    else
        _page.mainLoading.removeClass('active');
}
_common.ContainerLoading = function (show, container) {
    if (show)
        container.append('<div class="loading"><img src="/images/loader-sm.gif" /></div>');
    else
        container.find('div.loading').remove();
}

_common.UploadFormFile = function (element, objectProps, inputfile, onUpload) {
    const { ObjectName, ObjectFieldName, ObjectDocumentID, isActive } = objectProps
    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    // progressbar.show();
                    element.find('.uploading-progress-bar').attr('value', percentComplete);
                    // if (percentComplete === 100) {
                    //     //element.find('.uploading-progress-bar').hide();
                    // }
                }
            }, false);

            return xhr;
        },
        url: "/api/drive/uploadfile",
        method: "POST",
        contentType: 'application/json',
        data: inputfile,
        cache: false,
        headers: {
            'objectname': ObjectName,
            'confirmattachment': true,
            'objectFieldName': ObjectFieldName,
            'objectdocumentid': ObjectDocumentID,
            'isactive': isActive,
            'filetype': 'file',
            'socket': sessionStorage.getItem('socket-id')
        },
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.message == "success") {
                onUpload(res.fileid);
            }
        },
        error: function (err) {
            onUpload(err);
        }
    })


}

_common.AfterSaveDriveUpdate = function (objectid, fileids, type) {
    let sdata = {}
    sdata.objectdata = objectid;
    sdata.filedata = fileids;
    sdata.type = type
    $.ajax({
        url: "/api/drive/afteruploaddriveupdate",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(sdata),
        success: function (result) {
            console.log(result);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

_common.SendMailWithIcs = function (sdata) {
    let eventObject = {};
    console.log(sdata);
    alert('event called');
}

_common.Addhttp = function (url) {
    if (!/^(f|ht)tps?:\/\//i.test(url)) {
        url = "http://" + url;
    }
    return url;
}

_common.DynamicPopUp = function (param) {
    let saveButtonName = param.saveButtonName || 'Save';
    let cancelButtonName = param.cancelButtonName || 'Cancel';
    let leftSideButtonName = param.leftSideButtonName;
    let leftButton = leftSideButtonName ? `<button type="button" class="btn btn-outline-primary left-button mr-auto btn-sm" >${leftSideButtonName}</button>` : ``
    let modal = $('<div class="modal fade create-link-modal"  data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<h5 class="modal-title">' + param.title + '</h5>' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +
        '<div class="modal-body">' + param.body + '</div>' +
        '<div class="modal-footer">' +
        leftButton +
        '<button type="button" class="btn btn-light" data-dismiss="modal">' + cancelButtonName + '</button>' +
        '<button type="button" class="btn btn-primary btnSave">' + saveButtonName + '</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>');
    if (param.size !== undefined) {
        modal.find('.modal-dialog').removeClass('modal-lg').addClass(param.size);
    }
    if (param.DistroyOnHide) {
        modal.on('hidden.bs.modal', function (e) {
            modal.remove();
        })
    }
    //Pass Reference of button to enable after popup is closed
    if (param.ClickRef) {
        modal.on('hidden.bs.modal', function (e) {
            $(param.ClickRef).removeAttr("disabled")
            modal.remove();
        })
    }

    if (param.ClearDriveDataOnClose) {
        _mydrive.ListViewContextSelection = [];
    }

    return modal;
}

_common.LoadEditor = function (container) {
    let editorBody = '<div class="msp-ctrl msp-ctrl-texteditor">\
    '+ _constantClient.EDITOR_TOOL + '\
    <div class="texteditor"  style="margin-top: 0px;  resize: vertical;overflow: auto; margin-bottom: 0px; height: 140px;border:1px solid #d1d3e2">\
    </div>\
    </div>';
    container.append(editorBody);
}
_common.GetMapKey = function () {
    return "AIzaSyDD0FJbCAEc4efoWxHd_ZtYkW4BkrPiNs4";
}
_common.GetDefaultLatLng = function () {
    return { lat: 51.507351, lng: -0.127758 }
}
_common.GetCitySearch = function (options) {
}

_common.ExpendGrid = function (page) {
    page.Grid.find('.grid-table tbody tr td:nth-child(2) .grid-cell').append('<i class="fa fa-angle-down row-icon"></i>').addClass('pr-15');
    page.Grid.find('.grid-table tbody tr td:nth-child(2) .grid-cell .row-icon').off('click');
    page.Grid.find('.grid-table tbody tr td:nth-child(2) .grid-cell .row-icon').on('click', function () {
        let tr = $(this).closest('tr')
        if (tr.hasClass('open-row') == false) {
            tr.addClass('open-row');
            $(this).removeClass('fa-angle-down').addClass('fa-angle-up');
            let newRow = $('<tr class="open-row-child"><td colspan="' + tr.find('td').length + '"><div class="open-row-result"></div></td></tr>');
            let obj = { pageName: page.PageData.ExpandPage };
            obj.match = {};
            if (page.PageData.PageName == 'AccountBranchList')
                obj.match['Branch'] = { $elemMatch: { $eq: _controls.ToEJSON('objectid', tr.data('id')) } }
            else
                obj.match[page.PageData.ObjectName + 'ID'] = _controls.ToEJSON('objectid', tr.data('id'));
            obj.callback = function (result) {
                if (result.data.length > 0) {
                    if (result.pagedata.Groups != undefined) {
                        for (let i = 0; i < result.pagedata.Groups.length; i++) {
                            for (let j = 0; j < result.pagedata.Groups[i].Fields.length; j++) {
                                let grpFld = result.pagedata.Groups[i].Fields[j];
                                grpFld.GroupName = result.pagedata.Groups[i].Name;
                                result.pagedata.Fields.push(grpFld);
                            }
                        }
                    }
                    result.pagedata.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
                    new PageTable(result.data, result.pagedata, newRow.find('.open-row-result'));
                }
                else
                    newRow.find('td').html('No record found.')
            }
            _common.GetObjectList(obj);
            tr.after(newRow);
        }
        else {
            tr.removeClass('open-row');
            $(this).removeClass('fa-angle-up').addClass('fa-angle-down');
            tr.next('tr.open-row-child').remove();
        }

    });
}

_common.isEmptyObj = function (object) {
    for (let key in object) {
        if (object.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

_common.GetObjectByString = function (o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    let a = s.split('.');
    for (let i = 0, n = a.length; i < n; ++i) {
        let k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return '';
        }
    }
    return o;
}

_common.CheckAddressFields = function (addressArray) {

    if (addressArray.length > 0) {
        for (let i = 0; i < addressArray.length; i++) {
            const addressElement = addressArray[i];
            if (addressElement == "" || addressElement == undefined)
                continue;
            else
                return true;
        }
        return false;
    }
}

_common.EmailAccountDefaultSettings = function () {
    return new Promise(function (resolve, reject) {
        _common.FetchApiByParam({
            url: "/api/common/getemailclient",
            type: "post"
        }, function (result) {
            if (result && result.message == "success") {
                resolve(result);
            } else {
                reject({ "message": "No Data Found" });
            }
        });
    });
}

_common.ValidateDomainName = function (domainName) {
    if (!domainName)
        return false;
    else {
        let matches = domainName.match(/^(?!(https:\/\/|http:\/\/|www\.|mailto:|smtp:|ftp:\/\/|ftps:\/\/))(((([a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z0-9\-]{0,86}[a-zA-Z0-9]))\.(([a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z0-9\-]{0,73}[a-zA-Z0-9]))\.(([a-zA-Z0-9]{2,12}\.[a-zA-Z0-9]{2,12})|([a-zA-Z0-9]{2,25})))|((([a-zA-Z0-9])|([a-zA-Z0-9][a-zA-Z0-9\-]{0,162}[a-zA-Z0-9]))\.(([a-zA-Z0-9]{2,12}\.[a-zA-Z0-9]{2,12})|([a-zA-Z0-9]{2,25}))))$/g);
        if (matches == null)
            return false;
        else
            return true;
    }
}
/*
Note: 
listType: CampaignCategory , 
keyType: key type of list (Auto | Manual), 
keyDataType: key data type (Number | String); in case of String key data will be key but after 
replacing space with underscore., 
key: name of key, 
value: value of key, 
operation: insert, update, delete, checkduplicate
callBack: it gives true or false in callback. however callback is not mandatory.
*/
_common.ListSchemaDML = function (listType, keyType, keyDataType, key, value, operation, domainID) {
    let lsDMLPromise = new Promise(function (success, failure) {
        let sdata = { Operation: operation, ListType: listType, KeyType: keyType, KeyDataType: keyDataType, DomainID : domainID };
        if (operation == "insert") {
            if (keyType == "Auto") {
                sdata.Item = {
                    Value: value,
                    DomainID : domainID
                }
            } else {
                sdata.Item = {
                    Key: key,
                    Value: value,
                    DomainID : domainID
                }
            }
            sdata.Item.CreatedBy = _controls.ToEJSON("objectid", _initData.User._id);
            sdata.Item.CreatedDate = _controls.ToEJSON("datetime", new Date());
            _common.FetchApiByParam({
                url: "/api/common/listschemadml",
                type: "post",
                data: sdata
            }, function (result) {
                if (result && result.message == "success") {
                    success();
                } else {
                    failure();
                }
            });
        }
        else if (operation == "update" || operation == "delete" || operation == "checkduplicate") {
            sdata.Item = {
                Key: key,
                Value: value,
                DomainID : domainID
            }
            _common.FetchApiByParam({
                url: "/api/common/listschemadml",
                type: "post",
                data: sdata
            }, function (result) {
                if (result && result.message == "success") {
                    success();
                } else {
                    failure();
                }
            });
        }
    });
    return lsDMLPromise;
}

_common.GetRegisteredEmail = function () {
    let domainPromise = new Promise(function (success, failure) {
        _common.FetchApiByParam({
            url: "api/common/getregisteredemails",
            type: "post",
            data: { 'currentUser': _initData.User._id }
        }, function (result) {
            if (result.message == "success") {
                success(result.data);
            } else {
                failure(result);
            }
        });
    })
    return domainPromise;
}

_common.GetListSchema = function (listName) {
    let listPromise = new Promise(function (success, failure) {
        let sdata = {
            ListName: listName
        }
        _common.FetchApiByParam({
            url: "api/common/getListSchema",
            type: "post",
            data: sdata
        }, function (result) {
            if (result.message == "success") {
                if (result.data)
                    success(result.data);
                else
                    success([]);
            } else {
                failure(result);
            }
        });
    });
    return listPromise;
}

_common.GetDatesByCode = function (code, currentDate, num, format) {
    let result = { fromDate: '', toDate: '' };
    let date = new Date(currentDate);
    let dayInMillisec = 86400000;
    let nwDate, firstDate, lastDate;
    switch (code) {
        case 'Today':
            result.fromDate = date.setHours(0, 0, 0, 0);
            result.toDate = date.setHours(23, 59, 59, 999);
            break;
        case 'Yesterday':
            date.setDate(date.getDate() - 1);
            result.fromDate = date.setHours(0, 0, 0, 0);
            result.toDate = date.setHours(23, 59, 59, 999);
            break;
        case 'Tomorrow':
            date.setDate(date.getDate() + 1);
            result.fromDate = date.setHours(0, 0, 0, 0);
            result.toDate = date.setHours(23, 59, 59, 999);
            break;
        case 'ThisWeek':
            nwDate = new Date(Date.parse(date) - (dayInMillisec * (date.getDay() - 1)));
            result.fromDate = nwDate.setHours(0, 0, 0, 0);
            result.toDate = new Date(Date.parse(nwDate) + (dayInMillisec * 6)).setHours(23, 59, 59, 999);
            break;
        case 'LastWeek':
            nwDate = new Date(Date.parse(date) - (dayInMillisec * 6));
            nwDate = new Date(Date.parse(nwDate) - (dayInMillisec * (nwDate.getDay() - 1)));
            result.fromDate = nwDate.setHours(0, 0, 0, 0);
            result.toDate = new Date(Date.parse(nwDate) + (dayInMillisec * 6)).setHours(23, 59, 59, 999);
            break;
        case 'NextWeek':
            nwDate = new Date(Date.parse(date) + (dayInMillisec * 6));
            nwDate = new Date(Date.parse(nwDate) - (dayInMillisec * (nwDate.getDay() - 1)));
            result.fromDate = nwDate.setHours(0, 0, 0, 0);
            result.toDate = new Date(Date.parse(nwDate) + (dayInMillisec * 6)).setHours(23, 59, 59, 999);
            break;
        case 'Last-N-Days':
            nwDate = new Date(Date.parse(date) - (dayInMillisec * num));
            result.fromDate = nwDate.setHours(0, 0, 0, 0);
            result.toDate = new Date(Date.parse(date) - dayInMillisec).setHours(23, 59, 59, 999);
            break;
        case 'Next-N-Days':
            nwDate = new Date(Date.parse(date) + dayInMillisec);
            result.fromDate = nwDate.setHours(0, 0, 0, 0);
            result.toDate = new Date(Date.parse(nwDate) + (dayInMillisec * (num - 1))).setHours(23, 59, 59, 999);
            break;
        case 'ThisMonth':
            firstDate = new Date(date.getFullYear(), date.getMonth(), 01);
            lastDate = new Date(firstDate);
            lastDate.setMonth(lastDate.getMonth() + 1)
            lastDate = new Date(Date.parse(lastDate) - dayInMillisec);
            result.fromDate = firstDate.setHours(0, 0, 0, 0);
            result.toDate = lastDate.setHours(23, 59, 59, 999);
            break;
        case 'LastMonth':
            date.setMonth(date.getMonth() - 1);
            firstDate = new Date(date.getFullYear(), date.getMonth(), 01);
            lastDate = new Date(firstDate);
            lastDate.setMonth(lastDate.getMonth() + 1)
            lastDate = new Date(Date.parse(lastDate) - dayInMillisec);
            result.fromDate = firstDate.setHours(0, 0, 0, 0);
            result.toDate = lastDate.setHours(23, 59, 59, 999);
            break;
        case 'NextMonth':
            date.setMonth(date.getMonth() + 1);
            firstDate = new Date(date.getFullYear(), date.getMonth(), 01);
            lastDate = new Date(firstDate);
            lastDate.setMonth(lastDate.getMonth() + 1)
            lastDate = new Date(Date.parse(lastDate) - dayInMillisec);
            result.fromDate = firstDate.setHours(0, 0, 0, 0);
            result.toDate = lastDate.setHours(23, 59, 59, 999);
            break;
        case 'Before-N-MonthFistDayToCurrentDate':
            nwDate = new Date(date);
            nwDate.setMonth(nwDate.getMonth() - num);
            result.fromDate = new Date(nwDate.getFullYear(), nwDate.getMonth(), 01).setHours(0, 0, 0, 0);
            result.toDate = date.setHours(23, 59, 59, 999);
            break;
        case 'After-N-MonthLastDayToCurrentDate':
            nwDate = new Date(date);
            nwDate.setMonth(nwDate.getMonth() + num);
            result.fromDate = date.setHours(0, 0, 0, 0);
            result.toDate = new Date(nwDate.getFullYear(), nwDate.getMonth() + 1, 0).setHours(23, 59, 59, 999);
            break;
        case 'ThisYear':
            firstDate = new Date(date.getFullYear(), 00, 01);
            lastDate = new Date(date.getFullYear() + 1, 00, 01);
            lastDate = new Date(Date.parse(lastDate) - dayInMillisec);
            result.fromDate = firstDate.setHours(0, 0, 0, 0);
            result.toDate = lastDate.setHours(23, 59, 59, 999);
            break;
        case 'LastYear':
            firstDate = new Date(date.getFullYear() - 1, 00, 01);
            lastDate = new Date(date.getFullYear(), 00, 01);
            lastDate = new Date(Date.parse(lastDate) - dayInMillisec);
            result.fromDate = firstDate.setHours(0, 0, 0, 0);
            result.toDate = lastDate.setHours(23, 59, 59, 999);
            break;
        case 'NextYear':
            firstDate = new Date(date.getFullYear() + 1, 00, 01);
            lastDate = new Date(date.getFullYear() + 2, 00, 01);
            lastDate = new Date(Date.parse(lastDate) - dayInMillisec);
            result.fromDate = firstDate.setHours(0, 0, 0, 0);
            result.toDate = lastDate.setHours(23, 59, 59, 999);
            break;
        default:
            break;
    }
    if (format) {
        result.fromDate = moment(result.fromDate).format(format);
        result.toDate = moment(result.toDate).format(format)
    }

    return result;
}

_common.ShowContextMenu = function (e, parentOffset, contextMenu) {
    if ($('.context-menu:visible').length > 0) {
        $('.context-menu:visible').hide();
    }
    if ($('.dropdown-menu.show').length > 0) {
        $('.dropdown-menu.show').removeClass('show');
    }
    let top = e.pageY - parentOffset.top;
    let left = e.pageX - parentOffset.left;
    if ($(window).height() <= (e.pageY + contextMenu.outerHeight())) {
        top = top - contextMenu.outerHeight();
    }

    contextMenu.css({ display: "block", left: left, top: top });
}

_common.loadDeveloperEditor = function (page) {
    var textArea = page.PageContainer.find('[data-field="Content"]');
    page.PageContainer.find('.modal-content').addClass('full-screen');
    page.PageContainer.find('.modal-footer').prepend('<button type="button" class="btn btn-light btnSaveOnly">Save</button>');
    page.PageContainer.find('.modal-footer').prepend('<button type="button" class="btn btn-light btn-format">Format</button>');
    textArea.height('100%');
    setTimeout(function () {
        var editor = CodeMirror.fromTextArea(textArea[0], {
            lineNumbers: true,
            lineWrapping: true,
            mode: "javascript",
            viewportMargin: Infinity
        });

        editor.setSize('100%', $(window).height() - 280)

        page.PageContainer.find('.btn-format').click(function () {
            editor.save();
            CodeMirror.commands["selectAll"](editor);
            var range = getSelectedRange();
            editor.autoFormatRange(range.from, range.to);
        });
        page.PageContainer.find('.btnSaveOnly').click(function () {
            page.PageContainer.find('.btnSaveObject').attr('data-exit', 'false');
            page.PageContainer.find('.btnSaveObject').click();
            setTimeout(function () {
                page.PageContainer.find('.btnSaveObject').attr('data-exit', 'true');
            }, 2000)

        })
        page.PageContainer.find('.btnSaveObject').html('Save and Exit');
        page.PageContainer.find('.btnSaveObject')[0].onSave = function () {
            editor.save();
        }

    }, 1000)

}
_common.FromTimeToTimeValid = function (fromTimeCtrl, toTimeCtrl, changeType) {
    let startDateTime = '2001-01-01 ' + fromTimeCtrl.attr('data-time');
    let endDateTime = '2001-01-01 ' + toTimeCtrl.attr('data-time');

    if (changeType == "FromTime") {
        let fromTime = Number(fromTimeCtrl.attr('data-time').replace(':', ''));
        if (fromTime != undefined && fromTime >= 2300) {
            endDateTime = moment(endDateTime).hour(23);
            endDateTime.minute(45);
        }
        else if (fromTimeCtrl.attr('data-time')) {
            if (moment(startDateTime).isSameOrAfter(endDateTime)) {
                endDateTime = moment(startDateTime).add('minute', 60);
            }
        }
        toTimeCtrl.val(moment(endDateTime).format(_initData.Company.Setting.CalendarTimeFormat))
            .attr('data-time', moment(endDateTime).format('HH:mm'));
    }
    else if (changeType == "ToTime") {
        let toTime = Number(toTimeCtrl.attr('data-time').replace(':', ''));
        if (toTime != undefined && (toTime <= 100 || toTime == 0)) {
            startDateTime = moment(startDateTime).hour(0);
            startDateTime.minute(0);
        }
        else if (moment(endDateTime).isSameOrBefore(startDateTime)) {
            startDateTime = moment(endDateTime).subtract('minute', 60);
        }
        fromTimeCtrl.val(moment(startDateTime).format(_initData.Company.Setting.CalendarTimeFormat))
            .attr('data-time', moment(startDateTime).format('HH:mm'));
    }
}
_common.GetCollectionDataForSelect = function (options) {
    let sdata = { "LookupObject": options.LookupObject, "LookupFields": options.LookupFields, "MatchCondition": options.MatchCondition };
    let resultReturn = [];
    $.ajax({
        url: "/api/common/getlistforselect",
        type: "post",
        async: false,
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (result) {
            if (result.message == "success" && result.data) {
                resultReturn = result.data;
            }
        },
        error: function (err) {
        }
    });
    return resultReturn;

}
_common.DisableField = function (ctrl, UIDataType) {
    switch (UIDataType) {
        case "texteditor":
            // ctrl = $('<div title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl ' + cls + '" ' + attr + req + '>\
            // '+ _constantClient.EDITOR_TOOL + '\
            // <div class="texteditor" contenteditable="true" style="margin-top: 0px;  resize: vertical;\
            // overflow: auto; margin-bottom: 0px; height: 70px;border:1px solid #d1d3e2"></div>\
            // </div>');
            break;
        case "multiselect":
            if (ctrl)
                ctrl.multiselect('disable');
            break;
        case "lookup":
            ctrl.find('.msp-ctrl').attr('contenteditable', false).addClass('disabled-lookup');
            ctrl.find('.fas.fa-times').remove()
            ctrl.find('.lookup-display-record').removeClass('lookup-display-record').addClass('lookup-display-disabled-record');
            break;
        case "dropdown":
            ctrl.find(".msp-ctrl").prop('disabled', true);
            break;
        case "multilookup":
            //ctrl = $('<div class="input-group input-group-sm multilookup"><div class="input-group-prepend"><div class="btn btn-outline-primary btn-sm"><div class="pt-1"><i class="fas fa-search"></i></div></div></div><div contenteditable="true" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl trigger-popup form-control form-control-sm ' + cls + '" ' + req + '><p contenteditable="true" class="trigger-popup"></p></div></div>');
            ctrl.find('.msp-ctrl').attr('contenteditable', false).addClass('disabled-lookup');
            ctrl.find('.fas.fa-times').remove()
            ctrl.find('.lookup-display-record').removeClass('lookup-display-record').addClass('lookup-display-disabled-record');
            break;

        case "file":
            //ctrl = $('<form enctype="multipart/form-data">  <label title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + req + '><input type="file" name="file" style="visibility:hidden" ></label><div class="div-file-uploading "></div></form>');
            break;
        case "location":
            // ctrl = $('<div data-lat="" data-lng="" title="Locate On Map" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl msp-ctrl-location" ' + req + '>\
            // <button class="btn btn-sm btn-outline-primary btnLocateOnMap">Locate On Map</button>\
            // <button class="btn btn-sm btn-outline-info btnSearchAddress">Search Address</button>\
            // </div></div>');
            break;
        case "image":
            // ctrl = $('<div class="edi-bx msp-ctrl msp-ctrl-image profile-pic" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" data-height="' + field.Height + '" data-width="' + field.Width + '" style="width:150px;">\
            //          <form enctype="multipart/form-data"> \
            //          <input type="file" name="file" style="visibility:hidden" >\
            //         <img src="/images/bg.png" class="rounded" style="width:150px; height:120px;" onerror="this.src=\'images/bg.png\';"/>\
            //         <div class="profile-img-edit">\
            //         <i class="fa fa-edit msp-ctrl-image-edit btn btn-sm btn-primary" aria-hidden="true" title="edit"></i>\
            //         <i class="fa fa-trash msp-ctrl-image-remove btn btn-sm btn-danger" aria-hidden="true" title="remove"></i>\
            //         </div>\
            //         <form>\
            //         </div>');
            break;
        case "checkbox":
            ctrl.find('input').prop('disabled', true).closest('.lblChk').addClass('disabled-checkbox');
        case "date":
        case "datetime":
        case "time":
        case "checkbox":
        case "multiselect":
            ctrl.find('input,select,button').prop('disabled', true);
            break;
        default:
            ctrl.prop('disabled', true);
            break;

    }

    return ctrl;
}

_common.EnableField = function (ctrl, UIDataType) {
    switch (UIDataType) {
        case "texteditor":
            // ctrl = $('<div title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl ' + cls + '" ' + attr + req + '>\
            // '+ _constantClient.EDITOR_TOOL + '\
            // <div class="texteditor" contenteditable="true" style="margin-top: 0px;  resize: vertical;\
            // overflow: auto; margin-bottom: 0px; height: 70px;border:1px solid #d1d3e2"></div>\
            // </div>');
            break;
        case "multiselect":
            if (ctrl)
                ctrl.multiselect('enable');
            break;
        case "lookup":
            ctrl.find('.msp-ctrl').attr('contenteditable', true).removeClass('disabled-lookup');
            //ctrl.find('.fas.fa-times').remove()
            ctrl.find('.lookup-display-disabled-record').removeClass('lookup-display-disabled-record').addClass('lookup-display-record');
            break;
        case "multilookup":
            //ctrl = $('<div class="input-group input-group-sm multilookup"><div class="input-group-prepend"><div class="btn btn-outline-primary btn-sm"><div class="pt-1"><i class="fas fa-search"></i></div></div></div><div contenteditable="true" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl trigger-popup form-control form-control-sm ' + cls + '" ' + req + '><p contenteditable="true" class="trigger-popup"></p></div></div>');
            break;

        case "file":
            //ctrl = $('<form enctype="multipart/form-data">  <label title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + req + '><input type="file" name="file" style="visibility:hidden" ></label><div class="div-file-uploading "></div></form>');
            break;
        case "location":
            // ctrl = $('<div data-lat="" data-lng="" title="Locate On Map" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl msp-ctrl-location" ' + req + '>\
            // <button class="btn btn-sm btn-outline-primary btnLocateOnMap">Locate On Map</button>\
            // <button class="btn btn-sm btn-outline-info btnSearchAddress">Search Address</button>\
            // </div></div>');
            break;
        case "image":
            // ctrl = $('<div class="edi-bx msp-ctrl msp-ctrl-image profile-pic" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" data-height="' + field.Height + '" data-width="' + field.Width + '" style="width:150px;">\
            //          <form enctype="multipart/form-data"> \
            //          <input type="file" name="file" style="visibility:hidden" >\
            //         <img src="/images/bg.png" class="rounded" style="width:150px; height:120px;" onerror="this.src=\'images/bg.png\';"/>\
            //         <div class="profile-img-edit">\
            //         <i class="fa fa-edit msp-ctrl-image-edit btn btn-sm btn-primary" aria-hidden="true" title="edit"></i>\
            //         <i class="fa fa-trash msp-ctrl-image-remove btn btn-sm btn-danger" aria-hidden="true" title="remove"></i>\
            //         </div>\
            //         <form>\
            //         </div>');
            break;
        case "checkbox":
            ctrl.find('input').prop('disabled', false).closest('.lblChk').removeClass('disabled-checkbox');
        case "date":
        case "datetime":
        case "time":
        case "checkbox":
        case "multiselect":
            ctrl.find('input,select,button').prop('disabled', false);
            break;
        default:
            ctrl.prop('disabled', false);
            break;

    }

    return ctrl;
}

_common.GetLocationFromBrowser = function () {
    return new Promise(function (resolve, reject) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function (position) {
                    if (position && position.coords) {
                        resolve({
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        })
                    } else {
                        resolve("401")
                    }
                },
                function (error) {
                    resolve("401")
                });
        }
    })
}
_common.ServiceSubscribeStatus = function (servicePageName) {
    let result = { status: false };
    result.status = _userprofile.HasModuleAccess({ pageData: _common.GetPageDetailFromInitData(servicePageName) });
    switch (servicePageName) {
        case "IntegrateEmailClient":
            if (result.status && _initData.Company.Setting
                && _initData.Company.Setting.EmailIntegration
                && _initData.Company.Setting.EmailIntegration.EmailIntegrationEnable == true) {
                result.enabled = true;
            } else
                result.enabled = false;
            break;
    }
    return result;
}

_common.GetImportObjectDataID = function (param) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/common/getimportobjectdataID',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(param),
            success: function (res) {
                resolve(res);
            },
            error: function (err) {
                resolve(null)
            }
        });
    })
};


_common.BeforeContextMenuOpenEvent = function () {
    $('body').find("#main-header #userDropdown .img-profile").removeClass("img-profile-glow");
}



_common.GetObjectDataByID = function (param) {

    $.ajax({
        url: '/api/common/getobjectdatabyid',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(param),
        success: function (res) {
            param.callback(res);
        },
        error: function (res) {
            param.callback(res);
        }
    });
}

_common.GetGridDataWithoutLimit = function (param) {
    let sdata = { 'pagename': param.pageName };
    sdata.headerFilters = param.headerFilters;
    sdata.mustMatch = param.mustMatch;
    if (param.listFilter != undefined)
        sdata.listFilter = param.listFilter.Name;
    $.ajax({
        url: '/api/common/getgriddatawithoutlimit',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
        error: function (err) {
            param.callback(err.responseJSON);
        }
    });
}

_common.checkOnline = function (status) {
    if (status == "online") {
        sessionStorage.setItem("Conected", true);
        $(document).find('.no-internet').addClass('d-none');
    } else {
        let drivePage = $('.page-content[data-uid="MyDrive"]');
        _drive.ForceCancelAllUploads(drivePage);
        sessionStorage.setItem("Conected", false);
        $(document).find('.no-internet').removeClass('d-none')
    }

}

_common.doOnlineCheck = function () {
    $.ajax({
        url: '/api/drive/checkinternet',
        type: "GET",
        success: function () {
            _common.checkOnline("online");
        },
        error: function () {
            _common.checkOnline("offline");
        }
    });
}
/**
 * 
 * @param {companyId: "Company Id",
 *  access:"public"/"private",
 * dataID:"objectid of image"} param 
 * 
 * Hamza: 17July2020 : Function return the Src Url of the image from CDN
 */
_common.GetImageSourceURL = function (param) {

    let src = '';
    let access
    let companyID = _initData.Company._id
    if (param.companyID != undefined)
        companyID = param.companyID
    if (param.access != undefined)
        access = param.access
    else
        access = 'private'

    if (param.dataID)
        src = _constantClient.CDN + '/' + access + '/' + companyID + '/' + param.dataID;
    else
        src = 'images/nouser.jpg';

    return src;
}

//To stop checking net=> localStorage.setItem("checknet",false)
// checkInternetConnection = localStorage.getItem("checknet") == null ? true : false
// setInterval(() => {
//     if (checkInternetConnection) {
//         _common.doOnlineCheck()
//     }
// }, 5000);
