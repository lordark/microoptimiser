const _pipeline = {};

_pipeline.OnChangeStatusField = function (ctrl) {
    let field = ctrl[0].field;
    let changeFieldName = field.ChangeField;
    if (changeFieldName) {
        let changeFildCtrl;

        //check if gird else page
        if (ctrl.parent().hasClass('cell-edit'))
            changeFildCtrl = ctrl.closest('tr').find('[data-field="' + changeFieldName + '"]');
        else
            changeFildCtrl = ctrl.closest('.page-form').find('[data-field="' + changeFieldName + '"]');

        if (changeFildCtrl != undefined && changeFildCtrl.length > 0) {
            let listArr = _common.GetListSchemaDataFromInitData(field.Schema.ListSchemaName);
            let val = ctrl.find('[data-field="'+field.Name+'"]').val();
            let obj = _common.GetObjectByKeyValueFromList(listArr, 'Key', val);
            if (obj == undefined)
                changeFildCtrl.val('');
            else
                changeFildCtrl.val(obj.Probability);
            changeFildCtrl.trigger('change');
        }
    }
}