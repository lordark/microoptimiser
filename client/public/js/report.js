const _report = {};

_report.BuildFolderList = function (ul, callback) {
    _common.Loading(true);
    _common.FetchApiByParam({ url: "/api/report/getfolderlist", }, function (result) {
        if (result.FolderList) {
            ul.html('');
            if (result.UserFolderData) {
                for (let i = 0; i < result.UserFolderData.length; i++) {
                    let item = result.UserFolderData[i];
                    let folder = result.FolderList.find(x => x._id == item.FolderID);
                    if (folder) {
                        if (item.Sequence !== undefined)
                            folder.Sequence = item.Sequence;
                        if (item.IsDefault) {
                            folder.IsDefault = item.IsDefault;
                            let otherDefaultfolder = result.FolderList.find(x => x._id != item.FolderID && x.IsDefault == true);
                            if (otherDefaultfolder)
                                otherDefaultfolder.IsDefault = false;
                        }
                    }
                }
            }
            result.FolderList.sort(function (a, b) { return a.Sequence - b.Sequence });
            for (let i = 0; i < result.FolderList.length; i++) {
                let item = result.FolderList[i];
                let li = $('<li><a href="#"><i class="fas folder-icon"></i> <span class="name"></span></a><i class="fas fa-thumbtack fa-rotate-90 btn btn-set-default" data-toggle="tooltip" title="Make it default folder"></i><div class="btn-group"><button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button><div class="dropdown-menu text-wrap-dropdown shadow btn-menu"></div></div></li>');
                li.find('.name').html(item.Name);

                if (item.IsSystemDefine) {
                    li.find('.btn-menu').append('<a class="dropdown-item btn-pin-top">Pin to top</a> <a class="dropdown-item btn-access" data-toggle="modal" data-target="#folder-access">Access</a>');
                }
                else {
                    li.find('.btn-menu').append('<a class="dropdown-item btn-pin-top">Pin to top</a> <a class="dropdown-item btn-rename">Rename</a> <a class="dropdown-item btn-share" data-toggle="modal" data-target="#folder-access">Share</a> <a class="dropdown-item btn-delete-folder">Delete</a>');
                    li.addClass('custom')
                }

                if (item.IsDefault) {
                    li.addClass('active');
                    li.find('.folder-icon').addClass('fa-folder-open');
                    li.find('.btn-set-default').removeClass('fa-rotate-90').attr('title', 'This is default folder');
                }
                else {
                    li.find('.folder-icon').addClass('fa-folder')
                }
                li[0].item = item;

                ul.append(li);

            }
        }
        if (callback)
            callback(result.FolderList);
        _common.Loading(false);
    });
}

_report.LoadReportListPage = function (folder, container) {
    let obj = { 'PageContainer': container, 'Data': { PageName: 'ReportList' } };
    obj.MatchFields = [{ 'Name': 'ReportFolder', Value: folder._id, 'UIDataType': 'objectid' }];
    obj.callback = function (gridPage) {
        gridPage.PageContainer.find('.page-name').html(folder.Name);
    }
    new DataGrid(obj);
}

_report.LoadIndexPage = function (page) {
    let ulFolderList = page.PageContainer.find('ul.report-folder');
    _report.BuildFolderList(ulFolderList, function () {
        let folder = ulFolderList.find('li.active')[0].item;
        _report.LoadReportListPage(folder, page.PageContainer.find('.report-list'));
    });

    page.PageContainer.find('.txt-search-folder').keyup(function () {
        let val = $(this).val();
        if (val == undefined || val == '') {
            ulFolderList.find('li').removeClass('d-none');
            return false;
        }
        val = val.toLowerCase();
        ulFolderList.find('li').each(function () {
            let li = $(this);
            let name = li.find('.name').text().toLowerCase();
            if (name.includes(val)) {
                li.addClass("text-found");
                li.removeClass("d-none");
            }
            else {
                li.removeClass("text-found");
                li.addClass("d-none");
            }
        })
    });

    page.PageContainer.find('.btn-add-folder').click(function () {
        let formModal = _common.DynamicPopUp({ title: 'Add Folder', size: 'modal-sm', body: '<div class="row"><div class="form-group col-sm-12 vld-parent "><label class="small">Folder Name</label><input type="text" title="Folder Name" data-field="Name" data-ui="text" class="msp-ctrl form-control form-control-sm msp-ctrl-text" data-required=""></div></div>' });
        formModal.modal('show');
        formModal.find('.btnSave').click(function () {
            if (_common.Validate(formModal)) {
                _common.Loading(true);
                let sdata = { name: formModal.find('[data-field="Name"]').val(), sequence: page.PageContainer.find('ul.report-folder li').length + 1 };
                _common.FetchApiByParam({ url: "/api/report/addfolder", data: sdata }, function (result) {
                    _common.Loading(false);
                    if (result.message == 'success') {
                        formModal.modal('hide');
                        _report.BuildFolderList(ulFolderList);
                        swal('Folder added successfully', { icon: 'success' });
                    }
                    else {
                        swal(result.message, { icon: "info", });
                    }
                })
            }
        });
    });

    ulFolderList.on('click', '.btn-delete-folder', function (e) {
        e.stopPropagation();
        let item = $(this).closest('li')[0].item;
        _common.ConfirmDelete().then((confirm) => {
            if (confirm) {
                _common.Loading(true);
                let sdata = { 'id': item._id };
                _common.FetchApiByParam({ url: "/api/report/deletefolder", data: sdata }, function (result) {
                    _common.Loading(false);
                    if (result.message == 'success') {
                        _report.BuildFolderList(ulFolderList);
                        swal('Folder deleted successfully', { icon: 'success' });
                    }
                    else {
                        swal(result.message, { icon: "info", });
                    }
                })
            }
        });
    });

    ulFolderList.on('click', '.btn-rename', function (e) {
        e.stopPropagation();
        let item = $(this).closest('li')[0].item;
        let formModal = _common.DynamicPopUp({ title: 'Rename Folder : ' + item.Name, size: 'modal-sm', body: '<div class="row"><div class="form-group col-sm-12 vld-parent "><label class="small">Folder Name</label><input type="text" title="Folder Name" data-field="Name" data-ui="text" class="msp-ctrl form-control form-control-sm msp-ctrl-text" data-required=""></div></div>' });
        formModal.modal('show');
        formModal.find('[data-field="Name"]').val(item.Name);
        formModal.find('.btnSave').click(function () {
            if (_common.Validate(formModal)) {
                _common.Loading(true);
                let sdata = { name: formModal.find('[data-field="Name"]').val(), 'id': item._id };
                _common.FetchApiByParam({ url: "/api/report/renamefolder", data: sdata }, function (result) {
                    _common.Loading(false);
                    if (result.message == 'success') {
                        formModal.modal('hide');
                        _report.BuildFolderList(ulFolderList);
                        swal('Folder renamed successfully', { icon: 'success' });
                    }
                    else {
                        swal(result.message, { icon: "info", });
                    }
                })
            }
        });
    });

    ulFolderList.on('click', '.btn-pin-top', function (e) {
        e.stopPropagation();
        _common.Loading(true);
        let li = $(this).closest('li');
        li.parent().prepend(li);
        let sdata = [];
        let i = 1;
        ulFolderList.find('li').each(function () {
            let item = $(this)[0].item;
            sdata.push({ id: item._id, sequence: i });
            i++;
        });
        _common.FetchApiByParam({ url: "/api/report/pintopfolder", data: sdata }, function (result) {
            _common.Loading(false);
            if (result.message == 'success') {
                swal('Updated successfully', { icon: 'success' });
            }
            else {
                swal(result.message, { icon: "info", });
            }
        })
    });

    ulFolderList.on('click', '.btn-set-default.fa-rotate-90', function (e) {
        e.stopPropagation();
        let item = $(this).closest('li')[0].item;
        _common.Loading(true);
        let sdata = { 'id': item._id };
        _common.FetchApiByParam({ url: "/api/report/setdefaultfolder", data: sdata }, function (result) {
            _common.Loading(false);
            if (result.message == 'success') {
                _report.BuildFolderList(ulFolderList);
                swal('Updated default folder successfully', { icon: 'success' });
            }
            else {
                swal(result.message, { icon: "info", });
            }
        })
    });

    ulFolderList.on('click', 'li', function (e) {
        e.stopPropagation();
        let li = $(this);
        if (li.hasClass('active'))
            return false;

        let item = li[0].item;
        ulFolderList.find('li.active').removeClass('active');
        li.addClass('active');
        _report.LoadReportListPage(item, page.PageContainer.find('.report-list'));
    });

}