const _desktopNotification = {};
let isSubscribed = false;
let swRegistration = null;

//SECTION  SERVICE WORKERS
//STUB Check for service workers
if ('serviceWorker' in navigator && 'PushManager' in window) {
    navigator.serviceWorker.register('js/service_worker.js')
        .then(function(swReg) {
            swRegistration = swReg;
            swRegistration.pushManager.getSubscription()
                .then(function(subscription) {
                    // console.log(subscription)
                    isSubscribed = !(subscription === null);

                    if (isSubscribed) {
                        _desktopNotification.SaveSubscriptiontoDb(subscription);
                    } else {
                        swRegistration.pushManager.subscribe({
                                userVisibleOnly: true,
                                applicationServerKey: _desktopNotification.UrlB64ToUint8Array(_constantClient.NOTIFICATION_KEY)
                            })
                            .then(function(subscription) {
                                // console.log(subscription);
                                _desktopNotification.SaveSubscriptiontoDb(subscription);
                                isSubscribed = true;
                            })
                            .catch(function(err) {
                                //NOTE Failed to subscribe
                            })
                    }
                })
        })
        .catch(function(error) {
            // console.error('Service Worker Error', error);
        });
} else {
    console.warn('Push messaging is not supported');
}



//STUB  Url Encription
_desktopNotification.UrlB64ToUint8Array = function(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

//STUB Save the browser endpoint to db
_desktopNotification.SaveSubscriptiontoDb = function(subs) {
    subs.expiration = 12 * 60 * 60
    $.ajax({
        url: '/api/push_notification/subscribe',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(subs),
        success: function(res) {},
    });
}


//NOTE check aid is present in query param or not
// 1. if url like Calendar:id -> open calendar page then open popup
// 2. if url like MyDrive -> open drive
_desktopNotification.checkDesktopNotification = function() {
    if (location.search.includes('?aid')) {
        let query = location.search.split("=")[1]
        let parts = query.split(':');
        let page = parts[0];
        let pendingStatus = parts[2] == 't' ? false : true
        $(`[menu-data=${page}]`).click()

        
        window.history.replaceState({}, document.title, "/#");
        switch (page) {
            case 'Calendar':
                if (parts[1]) {
                    params = { _id: parts[1], pending: pendingStatus }
                    _calendar.ReadOnlyActivityForm(params);
                }
                break;
            case 'DriveStorageUpgrade':
                new Page({ "pageName": "DriveSettings" });
                break;
            case 'DriveShare':
                new Page({"pageName":"MyDrive"});
                window.location.hash="share"

        }

    }
}

// function CheckDesktopNotification() {
//     if (location.search.includes('?aid')) {
//         let sdata = {};
//         sdata.Objectname = 'Activity';
//         sdata.Objectid = location.search.split(":")[1];
//         notification.GetNotificationDetailsByObjectID(sdata, function (record) {
//             if (record.message == "success") {
//                 let activity = record.data;
//                 let param = {};
//                 param._id = activity._id;
//                 param.pending = true;
//                 // for (let j = 0; j < activity.UserParticipantsData.length; j++) {
//                 //     if (activity.UserParticipantsData[j].UserID == _initData.User._id)
//                 //         if (activity.UserParticipantsData[j].Status == "pending")
//                 //             param.pending = true;
//                 // }
//                 new ReadOnlyActivityForm(param);
//             }
//         })

//     }
// }