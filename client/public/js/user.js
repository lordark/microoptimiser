const _user = {};
//_user.SuspendedUsers = [];
_user.UserDetailInit = function (page) {
  console.log(page);
};
_user.Login = function (email, password) {
  let sdata = { Email: email, Password: password };
  $.ajax({
    url: "/noauth/api/user/login",
    type: "post",
    dataType: "json",
    contentType: "application/json",
    data: JSON.stringify(sdata),
    success: function (res) {
      if (res.message == "success") location.reload();
      else {
        $(".printError").text(res.message);
        $(".NotValid").removeClass("d-none");
        $(".txtEmail").val(null);
        $(".txtPassword").val(null);
      }
    },
  });
};

_user.Logout = function () {
  $.ajax({
    url: "/api/user/logout",
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (res) {
      if (res.message == "success") location.reload();
      else swal(res.message, { icon: "error" });
    },
  });
};

_user.ResetPassword = function (data, cb) {
  $.ajax({
    url: "/api/user/resetpassword",
    type: "post",
    dataType: "json",
    contentType: "application/json",
    data: JSON.stringify(data),
    success: function (res) {
      if (res.message == "success") cb(res);
      else swal(res.message, { icon: "error" });
    },
  });
};

_user.GetLoginUserData = function (callback) {
  $.ajax({
    url: "/api/user/getloginuserdata",
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (res) {
      _initData.User = res;
      if (callback != undefined) callback(res);
    },
  });
};
_user.UpdateUserPageSetting = function (page, callback) {
  $.ajax({
    url: "/api/user/updateuserpagesetting",
    type: "post",
    dataType: "json",
    contentType: "application/json",
    data: JSON.stringify(page),
    success: function (res) {
      if (callback != undefined) callback(res);
    },
  });
};

_user.BindUserEvents = function () {
  let navBar = $("#main-header .navbar");
  navBar.off("click", ".btnUserProfile");
  navBar.on("click", ".btnUserProfile", function () {
    new Page({ pageName: "LoginUserDetail", _id: _initData.User._id });
  });
  navBar.off("click", ".btnMySetting");
  navBar.on("click", ".btnMySetting", function () {
    new Page({ pageName: "UserSettings", _id: _initData.User._id });
  });
  navBar.off("click", ".btnTag");
  navBar.on("click", ".btnTag", function () {
    if (_initData.ProfileAuth.IsAdmin) {
      new Page({ pageName: "AdminTagList" });
      // new Page({ 'pageName': 'AdminTagList', 'OwnerID': _initData.User._id });
    } else {
      let MatchFields = [
        { Name: "OwnerID", UIDataType: "objectid", Value: _initData.User._id },
      ];
      new Page({ pageName: "TagList", MatchFields: MatchFields });
    }
  });
  navBar.off("click", ".btnStoreMenu");
  navBar.on("click", ".btnStoreMenu", function () {
    new Page({ pageName: "Store", _id: _initData.User._id });
  });

  if (
    !_initData.User.OnBoardingProcessComplete ||
    _initData.User.OnBoardingProcessComplete == false
  ) {
    _user.InitUserSetup();
  } else {
    let emailServiceStatus = _common.ServiceSubscribeStatus(
      "IntegrateEmailClient"
    );
    if (
      emailServiceStatus.status == true &&
      emailServiceStatus.enabled == true
    ) {
      _user.TakeUserEmailPassword();
    }
  }
};

//Events Page: UserSettings
_user.loadmysettings = function (page) {
  let mySettingPage = page.PageContainer;
  _user.GetLoginUserData(function (resultLoginUserData) {
    let UserSettings = _initData.User.MySettings || {};
    let CalendarRowsList = _initData.ListSchema.find(
      (x) => x.Name == "ActivityType"
    );
    if (CalendarRowsList) {
      CalendarRowsList.Data.sort(function (a, b) {
        return a.Value - b.Value;
      });
      let calRowOptions = '<option value="0"></option>';
      for (let i = 0; i < CalendarRowsList.Data.length; i++) {
        calRowOptions +=
          "<option value='" +
          CalendarRowsList.Data[i].Key +
          "'>" +
          CalendarRowsList.Data[i].Value +
          "</option>";
      }
      mySettingPage.find(".ddlCalendarDoubleClick").append(calRowOptions);
    }
    let GridRowsList = _initData.ListSchema.find((x) => x.Name == "GridRows");
    if (GridRowsList) {
      GridRowsList.Data.sort(function (a, b) {
        return a.Value - b.Value;
      });
      let gridRowOptions = '<option value="0"></option>';
      for (let i = 0; i < GridRowsList.Data.length; i++) {
        gridRowOptions +=
          "<option value='" +
          GridRowsList.Data[i].Key +
          "'>" +
          GridRowsList.Data[i].Value +
          "</option>";
      }
      mySettingPage.find(".ddlUserGridRows").append(gridRowOptions);
    }
    let UserMenu = _initData.ProfileAuth.UserMenu;
    if (UserMenu) {
      UserMenu.sort(function (a, b) {
        let aName = a.DisplayName.toLowerCase();
        let bName = b.DisplayName.toLowerCase();
        return aName < bName ? -1 : aName > bName ? 1 : 0;
      });
      let userMenuOptions = '<option value=""></option>';
      for (let i = 0; i < UserMenu.length; i++) {
        userMenuOptions +=
          "<option value='" +
          UserMenu[i].PageName +
          "'>" +
          UserMenu[i].DisplayName +
          "</option>";
      }
      mySettingPage.find(".ddlUserDefaultTab").append(userMenuOptions);
    }
    let fieldTimeCtrl_Start = { Name: "StartTime", DisplayName: "Start Time" };
    fieldTimeCtrl_Start.Schema = { UIDataType: "time" };
    let timerCtrlStartTime = _controls.BuildCtrl({
      field: fieldTimeCtrl_Start,
    });
    page.PageContainer.find(".user-working-days")
      .find(".starttime")
      .append(timerCtrlStartTime);

    let fieldTimerCtrl_End = { Name: "EndTime", DisplayName: "End Time" };
    fieldTimerCtrl_End.Schema = { UIDataType: "time" };
    let timerCtrlEndTime = _controls.BuildCtrl({ field: fieldTimerCtrl_End });
    page.PageContainer.find(".user-working-days")
      .find(".endtime")
      .append(timerCtrlEndTime);

    mySettingPage
      .find(".ddlUserTimezone")
      .append(
        _controls.BuildSelectOptions(
          _common.GetListSchemaDataFromInitData("Timezone"),
          "Key",
          "Value"
        )
      );
    if (UserSettings.Timezone) {
      mySettingPage.find(".ddlUserTimezone").val(UserSettings.Timezone);
    }

    if (UserSettings.GridRows)
      mySettingPage.find(".ddlUserGridRows").val(UserSettings.GridRows);
    if (UserSettings.DefaultTab)
      mySettingPage.find(".ddlUserDefaultTab").val(UserSettings.DefaultTab);
    if (UserSettings.CalendarDoubleClick)
      mySettingPage
        .find(".ddlCalendarDoubleClick")
        .val(UserSettings.CalendarDoubleClick);
    if (UserSettings.Signature) {
      if (UserSettings.Signature.indexOf("@") >= 0) {
        _user.GetUserSignatureFromDefaultSignature(
          UserSettings.Signature,
          _initData.User,
          function (signature) {
            mySettingPage
              .find(".txtUserSignature .texteditor")
              .html(signature || "");
          }
        );
      } else {
        mySettingPage
          .find(".txtUserSignature .texteditor")
          .html(UserSettings.Signature);
      }
    } else if (_initData.Company.Setting.CompanyDefaultSignature) {
      if (_initData.Company.Setting.CompanyDefaultSignature.indexOf("@") >= 0) {
        _user.GetUserSignatureFromDefaultSignature(
          _initData.Company.Setting.CompanyDefaultSignature,
          _initData.User,
          function (signature) {
            mySettingPage
              .find(".txtUserSignature .texteditor")
              .html(signature || "");
          }
        );
      } else {
        mySettingPage
          .find(".txtUserSignature .texteditor")
          .html(_initData.Company.Setting.CompanyDefaultSignature);
      }
    }
    if (UserSettings.WorkingDays) {
      for (let i = 0; i < UserSettings.WorkingDays.length; i++) {
        let wDay = UserSettings.WorkingDays[i];
        let wDayContainer = page.PageContainer.find(
          '.user-working-days .chkDay[data-id="' + wDay.WorkingDay + '"]'
        );
        if (wDayContainer) {
          let wDayParent = wDayContainer.closest(".daytiming");
          wDayParent.find(".chkDay").attr("checked", "checked");
          let startTime = moment(wDay.StartTime, "HH:mm").format(
            _initData.Company.Setting.CalendarTimeFormat
          );
          let endTime = moment(wDay.Endtime, "HH:mm").format(
            _initData.Company.Setting.CalendarTimeFormat
          );
          wDayParent
            .find('.starttime input[type="text"].msp-ctrl-time')
            .val(startTime)
            .attr("data-time", wDay.StartTime);
          wDayParent
            .find('.endtime input[type="text"].msp-ctrl-time')
            .val(endTime)
            .attr("data-time", wDay.Endtime);
        }
      }
    } else {
      let companyWorkingTimings = _initData.Company.Setting.CompanyWorkingDays;
      if (companyWorkingTimings) {
        let companyStartTime = _initData.Company.Setting.WorkingHrsStart;
        let companyEndTime = _initData.Company.Setting.WorkingHrsEnd;
        let companyStartTimeDisplay = "";
        let companyEndTimeDisplay = "";
        if (companyStartTime)
          companyStartTimeDisplay = moment(companyStartTime, "HH:mm").format(
            _initData.Company.Setting.CalendarTimeFormat
          );
        if (companyEndTime)
          companyEndTimeDisplay = moment(companyEndTime, "HH:mm").format(
            _initData.Company.Setting.CalendarTimeFormat
          );
        for (let i = 0; i < companyWorkingTimings.length; i++) {
          let wDayContainer = page.PageContainer.find(
            '.user-working-days .chkDay[data-id="' +
              companyWorkingTimings[i] +
              '"]'
          );
          if (wDayContainer) {
            let wDayParent = wDayContainer.closest(".daytiming");
            wDayParent.find(".chkDay").attr("checked", "checked");
            if (companyStartTime) {
              wDayParent
                .find('.starttime input[type="text"].msp-ctrl-time')
                .val(companyStartTimeDisplay)
                .attr("data-time", companyStartTime);
            }
            if (companyEndTime) {
              wDayParent
                .find('.endtime input[type="text"].msp-ctrl-time')
                .val(companyEndTimeDisplay)
                .attr("data-time", companyEndTime);
            }
          }
        }
      }
    }
    page.PageContainer.find(".user-working-days .chkDay").each(function () {
      if (!$(this).is(":checked")) {
        _common.DisableField($(this).parent().next(), "time");
      } else {
        $(this)
          .parent()
          .next()
          .find(".msp-ctrl-time")
          .attr("data-required", true);
      }
    });
    page.PageContainer.find(".user-working-days .chkDay").click(function () {
      $(this).parent().next().find("input").val("").attr("data-time", "00:00");
      if ($(this).is(":checked")) {
        _common.EnableField($(this).parent().next(), "time");
        $(this)
          .parent()
          .next()
          .find(".msp-ctrl-time")
          .attr("data-required", true);
      } else {
        _common.DisableField($(this).parent().next(), "time");
        $(this)
          .parent()
          .next()
          .find(".msp-ctrl-time")
          .removeAttr("data-required");
      }
    });
    page.PageContainer.on(
      "change",
      '.user-working-days .starttime input[data-field="StartTime"]',
      function () {
        let row = $(this).closest(".daytiming");
        _common.FromTimeToTimeValid(
          row.find('.starttime input[data-field="StartTime"]'),
          row.find('.endtime input[data-field="EndTime"]'),
          "FromTime"
        );
      }
    );
    page.PageContainer.on(
      "change",
      '.user-working-days .endtime input[data-field="EndTime"]',
      function () {
        let row = $(this).closest(".daytiming");
        _common.FromTimeToTimeValid(
          row.find('.starttime input[data-field="StartTime"]'),
          row.find('.endtime input[data-field="EndTime"]'),
          "ToTime"
        );
      }
    );

    if (mySettingPage.find(".txtUserSignature").length > 0) {
      page.PageContainer.find(".txtUserSignature").prepend(
        _constantClient.EDITOR_TOOL
      );
    }

    mySettingPage.find(".btnSaveMySettings").click(function () {
      if (_common.Validate(mySettingPage)) {
        if (
          mySettingPage.find(".user-working-days .chkDay:checked").length == 0
        ) {
          _common.ValidateAddMsg(
            mySettingPage.find(".error-container"),
            "Please select atleast one working day."
          );
          return;
        }
        let UserSettings = {};
        UserSettings.GridRows = mySettingPage.find(".ddlUserGridRows").val();
        UserSettings.DefaultTab = mySettingPage
          .find(".ddlUserDefaultTab")
          .val();
        UserSettings.CalendarDoubleClick = mySettingPage
          .find(".ddlCalendarDoubleClick")
          .val();
        UserSettings.Signature = mySettingPage
          .find(".txtUserSignature .texteditor")
          .html();
        UserSettings.Timezone = mySettingPage.find(".ddlUserTimezone").val();
        UserSettings.WorkingDays = [];
        mySettingPage.find(".user-working-days .daytiming").each(function () {
          if ($(this).find(".chkDay").is(":checked")) {
            let WorkingDayObj = {};
            WorkingDayObj.WorkingDay = $(this).find(".chkDay").attr("data-id");
            WorkingDayObj.StartTime = $(this)
              .find('.starttime input[type="text"].msp-ctrl-time')
              .attr("data-time");
            WorkingDayObj.Endtime = $(this)
              .find('.endtime input[type="text"].msp-ctrl-time')
              .attr("data-time");
            UserSettings.WorkingDays.push(WorkingDayObj);
          }
        });
        UserSettings.CalendarAllowedUsers = _controls.ToEJSON(
          "multilookup",
          page.PageContainer.find(".ddlAllowedUsersList").val()
        );
        UserSettings.CalendarDeniedUsers = _controls.ToEJSON(
          "multilookup",
          page.PageContainer.find(".ddlDeniedUsersList").val()
        );

        _user.savemysettings(mySettingPage, UserSettings);
      }
    });
    mySettingPage.find(".btnCancel").click(function () {
      $(document)
        .find("#main-header #page-nav .nav-item.active .tab-close")
        .trigger("click");
    });
    mySettingPage.find(".btnResetUserSignature").click(function () {
      if (_initData.Company.Setting.CompanyDefaultSignature.indexOf("@") >= 0) {
        _user.GetUserSignatureFromDefaultSignature(
          _initData.Company.Setting.CompanyDefaultSignature,
          _initData.User,
          function (signature) {
            mySettingPage
              .find(".txtUserSignature .texteditor")
              .html(signature || "");
          }
        );
      } else {
        mySettingPage
          .find(".txtUserSignature .texteditor")
          .html(_initData.Company.Setting.CompanyDefaultSignature);
      }
    });
    page.PageContainer.find(".user-working-days .starttime input").on(
      "change",
      function () {
        let chkMonday = $(this).closest(".daytiming").find(".chkDay");
        if (chkMonday.attr("id") === "chkWDMonday") {
          let val = $(this).val();
          let dataTime = $(this).attr("data-time");
          page.PageContainer.find(".user-working-days .starttime input").each(
            function () {
              let chkDay = $(this).closest(".daytiming").find(".chkDay");
              if (
                chkDay.attr("id") !== "chkWDSunday" &&
                chkDay.attr("id") !== "chkWDSaturday" &&
                chkDay.is(":checked")
              ) {
                $(this).val(val).attr("data-time", dataTime);
              }
            }
          );
        }
      }
    );
    page.PageContainer.find(".user-working-days .endtime input").on(
      "change",
      function () {
        let chkMonday = $(this).closest(".daytiming").find(".chkDay");
        if (chkMonday.attr("id") === "chkWDMonday") {
          let val = $(this).val();
          let dataTime = $(this).attr("data-time");
          page.PageContainer.find(".user-working-days .endtime input").each(
            function () {
              let chkDay = $(this).closest(".daytiming").find(".chkDay");
              if (
                chkDay.attr("id") !== "chkWDSunday" &&
                chkDay.attr("id") !== "chkWDSaturday" &&
                chkDay.is(":checked")
              ) {
                $(this).val(val).attr("data-time", dataTime);
              }
            }
          );
        }
      }
    );
    _user.GetUsersList(function (result) {
      if (result.status == "ok") {
        let usersList = result.data;
        let AllowedUsers = [],
          DeniedUsers = [];
        if (_initData.User.MySettings) {
          AllowedUsers = _initData.User.MySettings.CalendarAllowedUsers || [];
          DeniedUsers = _initData.User.MySettings.CalendarDeniedUsers || [];
        }
        let option = "";
        for (let i = 0; i < usersList.length; i++) {
          if (usersList[i]._id.toString() == _initData.User._id.toString()) {
            continue;
          }
          option +=
            '<option value="' +
            usersList[i]._id +
            '">' +
            usersList[i].FirstName +
            " " +
            usersList[i].LastName +
            "</option>";
        }
        page.PageContainer.find(".ddlAllowedUsersList")
          .append(option)
          .multiselect({
            nonSelectedText: "",
            onChange: function (currentOption, isSelected) {
              let userid = currentOption.val();
              let userContainer = page.PageContainer.find(
                '.users-denied input[type="checkbox"][value="' + userid + '"]'
              );
              if (isSelected == true) {
                userContainer.prop("disabled", true);
                userContainer.closest("li").addClass("disabled");
              } else {
                userContainer.prop("disabled", false);
                userContainer.closest("li").removeClass("disabled");
              }
            },
          });
        page.PageContainer.find(".ddlDeniedUsersList")
          .append(option)
          .multiselect({
            nonSelectedText: "",
            onChange: function (currentOption, isSelected) {
              let userid = currentOption.val();
              let userContainer = page.PageContainer.find(
                '.users-allowed input[type="checkbox"][value="' + userid + '"]'
              );
              if (isSelected == true) {
                userContainer.prop("disabled", true);
                userContainer.closest("li").addClass("disabled");
              } else {
                userContainer.prop("disabled", false);
                userContainer.closest("li").removeClass("disabled");
              }
            },
          });
        if (
          _initData.Company.Setting &&
          _initData.Company.Setting.AllowOverride == false
        ) {
          _common.DisableField(
            page.PageContainer.find(".ddlAllowedUsersList"),
            "multiselect"
          );
          _common.DisableField(
            page.PageContainer.find(".ddlDeniedUsersList"),
            "multiselect"
          );
        }
        page.PageContainer.find(".ddlAllowedUsersList").multiselect(
          "select",
          AllowedUsers,
          true
        );
        page.PageContainer.find(".ddlDeniedUsersList").multiselect(
          "select",
          DeniedUsers,
          true
        );
      }
    });
  });
};
_user.savemysettings = function (mySettinsPage, settingsObj) {
  _common.FetchApiByParam(
    {
      url: "/api/user/savemysettings",
      type: "post",
      contentType: "application/json",
      dataType: "JSON",
      data: settingsObj,
    },
    function (result) {
      if (result && result.message == "success") {
        swal("Settings has been saved successfully", { icon: "success" });
        _user.GetLoginUserData();
      } else {
        swal(result.message, { icon: "error" });
      }
    }
  );
};
//Events Page: UserDetail
_user.AfterUpdateProfile = function (page) {
  if (page.Data._id == _initData.User._id) {
    _user.GetLoginUserData(function () {
      _user.SetUserTopNavBar();
    });
  }
  _user.UserSyncWithMaster(page, "update");
  //_user.GetSuspendedUsersList();
  _user.UserLicenseConsumeCalculate();
  if (
    page.ResultData &&
    page.ResultData.message == "success" &&
    page.ResultData.data
  ) {
    if (
      page.ResultData.data.UserStatus &&
      page.ResultData.data.UserStatus == _constantClient.USER_STATUS.SUSPEND
    ) {
      let signoutOptions = {
        UserID: page.Data._id.toString(),
        Description: "On Suspend of user",
      };
      _user.UserSignoutFromAllDevices(signoutOptions);
    } else if (
      page.ResultData.data.Email &&
      page.ResultData.data.Email != page.Data.Email
    ) {
      let signoutOptions = {
        UserID: page.Data._id.toString(),
        Description: "On change of email address",
        RemovedBy: _initData.User._id,
      };
      _user.UserSignoutFromAllDevices(signoutOptions);
    }
  }
};
//Events Page: LoginUserDetail
_user.AfterUpdateMyProfile = function (page) {
  if (page.Data._id == _initData.User._id) {
    _user.GetLoginUserData(function () {
      _user.SetUserTopNavBar();
    });
  }
  _user.UserSyncWithMaster(page, "update");
};

//Events Page: UserDetail
_user.AfterCreateProfile = function (page) {
  const {
    FirstName,
    Email,
    LastName,
    Phone,
    Mobile,
    UserStatus,
    _id,
  } = page.ResultData.data;
  _user.UserSyncWithMaster(page, "create");
  //_user.GetSuspendedUsersList();
  _user.UserLicenseConsumeCalculate();
  _user.UpdateNewUserMySettings(page);
  _common.FetchApiByParam(
    {
      url: "/api/user/sendemailtosetpassword",
      type: "post",
      contentType: "application/json",
      dataType: "JSON",
      data: {
        email: Email,
        lastname: LastName,
        phone: Phone,
        mobile: Mobile,
        firstname: FirstName,
        userstatus: UserStatus,
        _id: _id,
      },
    },
    function (result) {
      if (result && result.message == "sucess") {
        swal(
          "An Email has been sent to the given email id, please check the same.",
          { icon: "success" }
        );
      } else {
        swal("Couldn’t connect with the server, Please try after sometime", {
          icon: "error",
        });
      }
    }
  );
};

//Events Page: UserList
_user.InitGridPage = function (gridPage) {
  let licenceSection = $(
    '<div class="license-sec position-absolute">\
    <div class="d-inline-block  mr-2 ml-2  align-items-center license-issued">\
                <div class="col-auto">\
                    <div class="progress-bar-per" class="file-loader inactive">\
                        <svg id="svgLicenseIssued" viewbox="0 0 100 100">\
                            <circle cx="50" cy="50" r="45" fill="#b3b3b3" />\
                            <path class="pathcircle" fill="none" stroke-linecap="round" stroke-width="12"\
                                stroke="#fff" stroke-dasharray="250,0"\
                                d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80">\
                            </path>\
                        </svg>\
                        <span class="lblLicenseCount count" font-size="20">0</span>\
                    </div>\
                </div>\
                 <div class="col filename small text-secondary">Issued License</div>\
            </div>\
    <div class="d-inline-block  mr-2 ml-2  align-items-center license-consumed">\
                <div class="col-auto">\
                <div class="progress-bar-per" class="file-loader inactive">\
                    <svg id="svgLicenseConsumed" viewbox="0 0 100 100">\
                        <circle cx="50" cy="50" r="45" fill="#f4f4f4" />\
                        <path class="pathcircle" fill="none" stroke-linecap="round" stroke-width="12"\
                            stroke="#fff" stroke-dasharray="0,250"\
                            d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80">\
                        </path>\
                    </svg>\
                    <span class="lblLicenseConsumedCount count" font-size="20">0</span>\
                </div>\
            </div>\
            <div class="col filename small text-secondary">Used License</div>\
        </div>\
    '
  );

  gridPage.Grid.find(".grid-header .page-title-info").append(licenceSection);
  _user.RefreshLicenseInformation();
  gridPage.Grid.find(".grid-body .gird-button-list .btnDelete").remove();
};

_user.SetUserTopNavBar = function () {
  let navBar = $("#main-header .navbar");
  if (_initData.User.ProfilePicture)
    navBar
      .find(".img-profile")
      .attr(
        "src",
        _constantClient.CDN +
          "/public/" +
          _initData.Company._id +
          "/" +
          _initData.User.ProfilePicture
      );
  else navBar.find(".img-profile").attr("src", "images/nouser.jpg");

  navBar
    .find("#userDropdown span.user-name")
    .html(_initData.User.FirstName + " " + _initData.User.LastName);
};
_user.RefreshLicenseInformation = function () {
  let userPage = $("#page-body .UserList");
  if (userPage) {
    let licenseSection = userPage.find(".license-sec");
    if (licenseSection) {
      let license = _initData.Company.Setting.NoOfUsersLicense || 0;
      let licenseConsumed = _initData.Company.Setting.NoOfLicenseConsumed || 0;
      if (license > 0) {
        let percent = (licenseConsumed / license) * 100;
        let decvalue = 250 - parseInt(2.5 * percent);
        let incvalue = parseInt(parseInt(2.5 * percent));
        licenseSection
          .find("#svgLicenseConsumed .pathcircle")
          .attr("stroke-dasharray", [incvalue, decvalue].join(","));
        licenseSection
          .find(".lblLicenseCount")
          .text(_initData.Company.Setting.NoOfUsersLicense || 0);
        licenseSection
          .find(".lblLicenseConsumedCount")
          .text(_initData.Company.Setting.NoOfLicenseConsumed || 0);
      }
    }
  }
  //new Page({ 'pageName': 'UserDetail' });
};
_user.GetUsersList = function (callback) {
  $.ajax({
    url: "/api/user/getuserslist",
    type: "post",
    dataType: "json",
    contentType: "application/json",
    success: function (res) {
      //_initData.User = res;
      if (callback != undefined) callback(res);
    },
  });
};
_user.GetUsersDetails = function (data, callback) {
  let sdata = { userId: data.userId };
  _common.FetchApiByParam(
    {
      url: "/api/user/getuserdetails",
      type: "post",
      contentType: "application/json",
      dataType: "JSON",
      data: sdata,
    },
    function (result) {
      if (callback) {
        callback(result);
      }
    }
  );
};
_user.GetSubordinateDetails = function (data, callback) {
  let sdata = { userId: data.userId, fullSubTree: data.fullSubTree || false };
  _common.FetchApiByParam(
    {
      url: "/api/user/getchildusersdetails",
      type: "post",
      contentType: "application/json",
      dataType: "JSON",
      data: sdata,
    },
    function (result) {
      if (callback) {
        callback(result);
      }
    }
  );
};
_user.GetUserSignatureFromDefaultSignature = function (
  signature,
  userData,
  callback
) {
  let actualSignature = signature;
  _user.GetUsersDetails({ userId: userData._id }, function (result) {
    if (result && result.message == "success") {
      for (let userField in result.data) {
        if (result.data.hasOwnProperty(userField)) {
          if (actualSignature.indexOf("@" + userField) >= 0) {
            actualSignature = actualSignature.replace(
              "@" + userField,
              result.data[userField] || ""
            );
          }
        }
      }
    }
    //This code was written for data field whose values not exists in db.
    // if (actualSignature.indexOf("##") >= 0) {
    //     actualSignature = actualSignature.replace(/##[\w|\d]+##/gi, '')
    // }
    if (callback) {
      callback(actualSignature);
    }
  });
};
_user.InitUserSetup = function () {
  let url = "/page/user/usersetup.html";
  _common.GetStaticPage(url, function (pageHTML) {
    let emailIntegrationSubscribe = false;
    let loginUser = _initData.User;
    let companySettings = _initData.Company.Setting;
    let userSetupPage = _common.DynamicPopUp({
      title: "Welcome!!!",
      body: pageHTML,
    });
    userSetupPage.modal({
      keyboard: false,
    });
    emailIntegrationSubscribe = _userprofile.HasModuleAccess({
      pageData: _common.GetPageDetailFromInitData("IntegrateEmailClient"),
    });

    if (
      emailIntegrationSubscribe == false ||
      !companySettings ||
      (companySettings && !companySettings.EmailIntegration) ||
      (companySettings &&
        companySettings.EmailIntegration &&
        companySettings.EmailIntegration.EmailIntegrationEnable != true)
    ) {
      userSetupPage.find(".btnStartOnboarding").attr("data-slide-to", 3);
      userSetupPage.find("li#slide-email-pass").hide();
      userSetupPage.find("li#slide-email-sign").hide();
    }
    userSetupPage.find(".modal-header, .modal-footer").remove();

    userSetupPage.find("#onboard").carousel({
      interval: false,
      keyboard: false,
    });
    userSetupPage
      .find(".userName")
      .text(loginUser.FirstName + " " + loginUser.LastName);
    userSetupPage.find(".lblUserEmail").text(loginUser.Email);
    if (loginUser.ProfilePicture) {
      userSetupPage
        .find(".msp-ctrl-image")
        .attr("data-imageid", loginUser.ProfilePicture);
      userSetupPage
        .find(".msp-ctrl-image img")
        .attr(
          "src",
          _constantClient.CDN +
            "/public/" +
            _initData.Company._id +
            "/" +
            loginUser.ProfilePicture
        );
    }
    if (emailIntegrationSubscribe == true) {
      if (companySettings.EmailIntegration) {
        //if (companySettings.EmailIntegration.IsEmailPasswordMandatory == true) {
        userSetupPage.find(".txtEmailPassword").attr("data-required", "");
        userSetupPage.find(".btnSkipEmailPassword").addClass("d-none");
        //} else {
        //  userSetupPage.find('.btnSkipEmailPassword').attr('data-slide-to', 2);
        // }
      }
    }

    _common.LoadEditor(userSetupPage.find(".emailsignature"));

    if (_initData.Company.Setting.CompanyDefaultSignature) {
      if (_initData.Company.Setting.CompanyDefaultSignature.indexOf("@") >= 0) {
        _user.GetUserSignatureFromDefaultSignature(
          _initData.Company.Setting.CompanyDefaultSignature,
          _initData.User,
          function (signature) {
            userSetupPage
              .find(".emailsignature .texteditor")
              .html(signature || "");
            userSetupPage.find(".btnSaveEmailSign").removeAttr("disabled");
          }
        );
      } else {
        userSetupPage
          .find(".emailsignature .texteditor")
          .html(_initData.Company.Setting.CompanyDefaultSignature);
      }
    }

    userSetupPage
      .find(".texteditor")
      .css({ color: "black", "text-align": "left" });
    userSetupPage
      .find(".ddlUserTimezone")
      .append(
        _controls.BuildSelectOptions(
          _common.GetListSchemaDataFromInitData("Timezone"),
          "Key",
          "Value"
        )
      );

    let fieldTimeCtrl_Start = { Name: "StartTime", DisplayName: "Start Time" };
    fieldTimeCtrl_Start.Schema = { UIDataType: "time" };
    let timerCtrlStartTime = _controls.BuildCtrl({
      field: fieldTimeCtrl_Start,
    });
    userSetupPage
      .find(".user-working-days")
      .find(".starttime")
      .append(timerCtrlStartTime);

    let fieldTimerCtrl_End = { Name: "EndTime", DisplayName: "End Time" };
    fieldTimerCtrl_End.Schema = { UIDataType: "time" };
    let timerCtrlEndTime = _controls.BuildCtrl({ field: fieldTimerCtrl_End });
    userSetupPage
      .find(".user-working-days")
      .find(".endtime")
      .append(timerCtrlEndTime);

    let companyWorkingDays = _initData.Company.Setting.CompanyWorkingDays;
    if (companyWorkingDays) {
      let companyStartTime = _initData.Company.Setting.WorkingHrsStart;
      let companyEndTime = _initData.Company.Setting.WorkingHrsEnd;
      let companyStartTimeDisplay = "";
      let companyEndTimeDisplay = "";
      if (companyStartTime)
        companyStartTimeDisplay = moment(companyStartTime, "HH:mm").format(
          _initData.Company.Setting.CalendarTimeFormat
        );
      if (companyEndTime)
        companyEndTimeDisplay = moment(companyEndTime, "HH:mm").format(
          _initData.Company.Setting.CalendarTimeFormat
        );
      for (let i = 0; i < companyWorkingDays.length; i++) {
        let wDayContainer = userSetupPage.find(
          '.user-working-days .chkDay[data-id="' + companyWorkingDays[i] + '"]'
        );
        if (wDayContainer) {
          let wDayParent = wDayContainer.closest(".daytiming");
          wDayParent.find(".chkDay").attr("checked", "checked");
          if (companyStartTime) {
            wDayParent
              .find('.starttime input[type="text"].msp-ctrl-time')
              .val(companyStartTimeDisplay)
              .attr("data-time", companyStartTime);
          }
          if (companyEndTime) {
            wDayParent
              .find('.endtime input[type="text"].msp-ctrl-time')
              .val(companyEndTimeDisplay)
              .attr("data-time", companyEndTime);
          }
        }
      }
    }
    userSetupPage.find(".user-working-days .chkDay").each(function () {
      if (!$(this).is(":checked")) {
        _common.DisableField($(this).parent().next(), "time");
      } else {
        $(this)
          .parent()
          .next()
          .find(".starttime,.endtime")
          .addClass("vld-parent");
        $(this)
          .parent()
          .next()
          .find(".msp-ctrl-time")
          .attr("data-required", true);
      }
    });
    userSetupPage.find(".user-working-days .chkDay").click(function () {
      $(this).parent().next().find("input").val("").attr("data-time", "00:00");
      if ($(this).is(":checked")) {
        _common.EnableField($(this).parent().next(), "time");
        $(this)
          .parent()
          .next()
          .find(".starttime,.endtime")
          .addClass("vld-parent");
        $(this)
          .parent()
          .next()
          .find(".msp-ctrl-time")
          .attr("data-required", true);
      } else {
        _common.DisableField($(this).parent().next(), "time");
        $(this)
          .parent()
          .next()
          .find(".starttime,.endtime")
          .removeClass("vld-parent");
        $(this)
          .parent()
          .next()
          .find(".msp-ctrl-time")
          .removeAttr("data-required");
      }
    });
    userSetupPage.find(".msp-ctrl-image").click(function () {
      let imageBox = {};
      imageBox.ImageContainer = $(this).closest(".msp-ctrl-image");
      imageBox.Height =
        Number(imageBox.ImageContainer.attr("data-height")) || 0;
      imageBox.Width = Number(imageBox.ImageContainer.attr("data-width")) || 0;
      imageBox.CallingControl = $(this).closest(".msp-ctrl-image");
      IBP.ImageEditor(imageBox);
    });
    try {
      let userTimezone = moment.tz.guess();
      if (userTimezone) {
        userSetupPage.find(".ddlUserTimezone").val(userTimezone);
      }
    } catch (ex) {}
    userSetupPage.find(".txtEmailPassword").keydown(function (e) {
      if (e.keyCode == 13) {
        if (
          userSetupPage.find(".txtEmailPassword").attr("data-verified") == ""
        ) {
          userSetupPage.find(".imgVerifyPassword").removeClass("d-none");
          userSetupPage.VerifyEmailPassword();
        }
      }
    });
    userSetupPage.find(".txtEmailPassword").keyup(function (e) {
      if (e.keyCode != 13) {
        userSetupPage
          .find(".carousel-indicators li.active")
          .nextAll()
          .removeAttr("data-slide-to");
        if ($(this).val().trim().length > 0) {
          $(this)
            .closest(".carousel-item")
            .find(".btnSave")
            .removeAttr("disabled");
        } else {
          $(this)
            .closest(".carousel-item")
            .find(".btnSave")
            .attr("disabled", "disabled");
        }

        userSetupPage.find(".imgVerifyPassword").addClass("d-none");
        userSetupPage.find(".imgPasswordVerified").addClass("d-none");
        userSetupPage.find(".imgPasswordInvalid").addClass("d-none");
        userSetupPage.find(".txtEmailPassword").attr("data-verified", "");
        userSetupPage.find(".btnVerifyEmailPassword").attr("data-slide-to", 1);
      }
    });
    userSetupPage.find(".emailsignature .texteditor").keyup(function () {
      if ($(this).text().trim().length > 0) {
        $(this)
          .closest(".carousel-item")
          .find(".btnSave")
          .removeAttr("disabled");
      } else {
        $(this)
          .closest(".carousel-item")
          .find(".btnSave")
          .attr("disabled", "disabled");
      }
    });
    userSetupPage.on(
      "change",
      '.user-working-days .starttime input[data-field="StartTime"]',
      function () {
        let row = $(this).closest(".daytiming");
        _common.FromTimeToTimeValid(
          row.find('.starttime input[data-field="StartTime"]'),
          row.find('.endtime input[data-field="EndTime"]'),
          "FromTime"
        );
      }
    );
    userSetupPage.on(
      "change",
      '.user-working-days .endtime input[data-field="EndTime"]',
      function () {
        let row = $(this).closest(".daytiming");
        _common.FromTimeToTimeValid(
          row.find('.starttime input[data-field="StartTime"]'),
          row.find('.endtime input[data-field="EndTime"]'),
          "ToTime"
        );
      }
    );
    userSetupPage.find(".btnSubmitAndFinish").click(function () {
      let skipToFinish = $(this).attr("data-skip");
      if (skipToFinish == undefined) {
        if (
          userSetupPage.find(".msp-ctrl-image").attr("data-imageid") ==
          undefined
        ) {
          alert(
            "Please upload your profile picture or skip to finish the setup"
          );
          return false;
        }
      }
      if (
        emailIntegrationSubscribe == true &&
        companySettings.EmailIntegration &&
        companySettings.EmailIntegration.EmailIntegrationEnable == true &&
        userSetupPage.find(".txtEmailPassword[data-required]").length > 0 &&
        userSetupPage.find(".txtEmailPassword").attr("data-verified") != "done"
      ) {
        userSetupPage.find("#onboard").carousel(1);
        return;
      }
      if (_common.Validate(userSetupPage)) {
        let MySettings = {};
        if (userSetupPage.find(".slide-page-email-pass[data-skip]").length == 1)
          MySettings.IsEmailPasswordSkipped = true;
        else {
          MySettings.IsEmailPasswordSkipped = false;
          MySettings.EmailPassword = userSetupPage
            .find(".txtEmailPassword")
            .val();
          MySettings.IsValidEmailPassword =
            userSetupPage.find(".txtEmailPassword").attr("data-verified") ==
            "done"
              ? true
              : false;
        }
        if (userSetupPage.find(".slide-page-email-sign[data-skip]").length == 1)
          MySettings.IsEmailSignatureSkipped = true;
        else {
          MySettings.IsEmailSignatureSkipped = false;
          MySettings.Signature = userSetupPage
            .find(".emailsignature")
            .find(".texteditor")
            .html();
        }
        MySettings.Timezone = userSetupPage.find(".ddlUserTimezone").val();
        MySettings.WorkingDays = [];
        userSetupPage.find(".user-working-days .daytiming").each(function () {
          if ($(this).find(".chkDay").is(":checked")) {
            let WorkingDayObj = {};
            WorkingDayObj.WorkingDay = $(this).find(".chkDay").attr("data-id");
            WorkingDayObj.StartTime = $(this)
              .find('.starttime input[type="text"].msp-ctrl-time')
              .attr("data-time");
            WorkingDayObj.Endtime = $(this)
              .find('.endtime input[type="text"].msp-ctrl-time')
              .attr("data-time");
            MySettings.WorkingDays.push(WorkingDayObj);
          }
        });
        let sdata = {
          UserID: loginUser._id,
          MySettings: MySettings,
          EmailIntegrationSubscribed: emailIntegrationSubscribe,
        };
        if (skipToFinish == undefined) {
          sdata["ProfilePicture"] =
            userSetupPage.find(".msp-ctrl-image").attr("data-imageid") || "";
        }
        _common.FetchApiByParam(
          {
            url: "/api/user/saveuseronboardingdata",
            type: "post",
            contentType: "application/json",
            dataType: "JSON",
            data: sdata,
          },
          function (result) {
            if (result && result.message == "success") {
              swal({
                title: "You've made it!",
                text: "Your account has been updated successfully",
                icon: "success",
                button: "Ok",
              }).then((diagResult) => {
                location.reload(true);
              });
            } else {
              swal(result.message, { icon: "error" });
            }
          }
        );
      }
    });
    userSetupPage.find(".btnVerifyEmailPassword").click(function () {
      $(this).closest(".carousel-item").removeAttr("data-skip");
      if (userSetupPage.find(".txtEmailPassword").attr("data-verified") == "") {
        userSetupPage.find(".imgVerifyPassword").removeClass("d-none");
        userSetupPage.find(".imgPasswordVerified").addClass("d-none");
        userSetupPage.find(".imgPasswordInvalid").addClass("d-none");
        userSetupPage.VerifyEmailPassword();
        userSetupPage
          .find(".carousel-indicators")
          .find("#slide-email-pass")
          .attr("data-slide-to", "1");
      }
    });
    userSetupPage.find(".btnSkipEmailPassword").click(function () {
      $(this).closest(".carousel-item").attr("data-skip", "true");
      userSetupPage
        .find(".carousel-indicators")
        .find("#slide-email-pass")
        .attr("data-slide-to", "1");
    });

    userSetupPage.find(".btnSkipEmailSign").click(function () {
      $(this).closest(".carousel-item").attr("data-skip", "true");
      userSetupPage
        .find(".carousel-indicators")
        .find("#slide-email-sign")
        .attr("data-slide-to", "2");
    });
    userSetupPage.find(".btnSaveEmailSign").click(function () {
      $(this).closest(".carousel-item").removeAttr("data-skip");
      userSetupPage
        .find(".carousel-indicators")
        .find("#slide-email-sign")
        .attr("data-slide-to", "2");
    });
    userSetupPage.find(".btnSaveTimezone").click(function () {
      userSetupPage
        .find(".carousel-indicators")
        .find("#slide-timezone")
        .attr("data-slide-to", "3");
    });
    userSetupPage.find(".btnSaveWorkingdays").click(function () {
      if (_common.Validate(userSetupPage.find(".user-working-days"))) {
        if (
          userSetupPage.find(".user-working-days .chkDay:checked").length == 0
        ) {
          _common.ValidateAddMsg(
            userSetupPage.find(".user-working-days .error-container"),
            "Please select atleast one working day."
          );
          return;
        }
        userSetupPage
          .find(".carousel-indicators")
          .find("#slide-working-hours")
          .attr("data-slide-to", "4");
        userSetupPage
          .find(".carousel-indicators")
          .find("#slide-final")
          .attr("data-slide-to", "5");
        userSetupPage.find("#onboard").carousel("next");
      } else {
        userSetupPage
          .find(".carousel-indicators")
          .find("#slide-final")
          .removeAttr("data-slide-to");
      }
    });
    userSetupPage
      .find(".user-working-days .starttime input")
      .on("change", function () {
        let chkMonday = $(this).closest(".daytiming").find(".chkDay");
        if (chkMonday.attr("id") === "chkWDMonday") {
          let val = $(this).val();
          let dataTime = $(this).attr("data-time");
          userSetupPage
            .find(".user-working-days .starttime input")
            .each(function () {
              let chkDay = $(this).closest(".daytiming").find(".chkDay");
              if (
                chkDay.attr("id") !== "chkWDSunday" &&
                chkDay.attr("id") !== "chkWDSaturday" &&
                chkDay.is(":checked")
              ) {
                $(this).val(val).attr("data-time", dataTime);
              }
            });
        }
      });
    userSetupPage
      .find(".user-working-days .endtime input")
      .on("change", function () {
        let chkMonday = $(this).closest(".daytiming").find(".chkDay");
        if (chkMonday.attr("id") === "chkWDMonday") {
          let val = $(this).val();
          let dataTime = $(this).attr("data-time");
          userSetupPage
            .find(".user-working-days .endtime input")
            .each(function () {
              let chkDay = $(this).closest(".daytiming").find(".chkDay");
              if (
                chkDay.attr("id") !== "chkWDSunday" &&
                chkDay.attr("id") !== "chkWDSaturday" &&
                chkDay.is(":checked")
              ) {
                $(this).val(val).attr("data-time", dataTime);
              }
            });
        }
      });
    userSetupPage.VerifyEmailPassword = function () {
      let sdata = {
        UserEmailId: loginUser.Email,
        EmailPassword: userSetupPage.find(".txtEmailPassword").val(),
      };
      _common.FetchApiByParam(
        {
          url: "/api/user/verifyemailpassword",
          type: "post",
          contentType: "application/json",
          dataType: "JSON",
          data: sdata,
        },
        function (result) {
          if (result && result.message == "success") {
            userSetupPage
              .find(".txtEmailPassword")
              .attr("data-verified", "done");
            userSetupPage.find(".imgVerifyPassword").addClass("d-none");
            userSetupPage.find(".imgPasswordVerified").removeClass("d-none");
            userSetupPage.find(".imgPasswordInvalid").addClass("d-none");
            userSetupPage
              .find(".btnVerifyEmailPassword")
              .attr("data-slide-to", 2);
            userSetupPage.find("#onboard").carousel(2);
          } else {
            userSetupPage.find(".txtEmailPassword").attr("data-verified", "");
            userSetupPage.find(".imgVerifyPassword").addClass("d-none");
            userSetupPage.find(".imgPasswordVerified").addClass("d-none");
            userSetupPage.find(".imgPasswordInvalid").removeClass("d-none");
            userSetupPage
              .find(".btnVerifyEmailPassword")
              .attr("data-slide-to", 1);
          }
        }
      );
    };
  });
};
_user.UpdateCompanySignatureToUser = function (options) {
  let userAutocompleteBody = _user.UserLookupCtrl({
    AutocompleteType: "MultiLookupUser",
    UIDataType: "multilookup",
    IsRequired: true,
    DisplayName: "User",
  });

  let modalUpdateSignature = options.Page.PageContainer.find(
    "#modalSignatureUpdate"
  );
  // if (modalUpdateSignature.find('.user-lookup-ctrl [data-ui="multilookup"]').length > 0) {
  //     modalUpdateSignature.find('.user-lookup-ctrl>div, [data-ui="multilookup"]').remove()
  // }
  if (
    modalUpdateSignature.find('.user-lookup-ctrl [data-ui="multilookup"]')
      .length == 0
  ) {
    modalUpdateSignature.find(".user-lookup-ctrl").append(userAutocompleteBody);
  }
  modalUpdateSignature.modal({
    backdrop: "static",
    keyboard: false,
    show: true,
  });
  modalUpdateSignature.find(".modal-footer .btnCancel").click(function () {
    if (options.callback) {
      if (options.Page && options.Page.DefaultSignatureModified) {
        options.Page.DefaultSignatureModified = false;
      }
      options.callback({ message: "cancelled" });
    }
    modalUpdateSignature.modal("hide");
  });

  modalUpdateSignature
    .find('.sign-update-options input[type="radio"]')
    .click(function () {
      if (modalUpdateSignature.find(".optSelectedUsers").is(":checked")) {
        modalUpdateSignature.find(".selectuser").removeClass("d-none");
      } else {
        modalUpdateSignature.find(".selectuser").addClass("d-none");
        modalUpdateSignature
          .find(".user-lookup-ctrl .msp-ctrl-multilookup .lookup-item")
          .remove();
      }
    });

  modalUpdateSignature.find(".btnSave").click(function () {
    if (modalUpdateSignature.find(".optNobody").is(":checked")) {
      if (options.callback) {
        if (options.Page && options.Page.DefaultSignatureModified) {
          options.Page.DefaultSignatureModified = false;
        }
        options.callback({ message: "success" });
      }
      // modalUpdateSignature.find('.optNobody').prop('checked', 'checked');
    } else if (modalUpdateSignature.find(".optAllUsers").is(":checked")) {
      _user.GetUsersList(function (result) {
        if (result.message == "success") {
          let counter = 0;
          for (let i = 0; i < result.data.length; i++) {
            let userData = result.data[i];
            _user.GetUserSignatureFromDefaultSignature(
              options.companySignature,
              userData,
              function (newSignature) {
                counter += 1;
                let userOptions = {
                  UserID: userData._id,
                  UserSignature: newSignature || "",
                };
                userOptions.callback = function (updateStatus) {
                  counter -= 1;
                  if (updateStatus && updateStatus.message != "success")
                    options.FailureCount = (options.FailureCount || 0) + 1;
                  if (counter == 0) {
                    if (options.callback) {
                      if ((options.FailureCount || 0) > 0)
                        options.callback({ message: "failed" });
                      else {
                        if (
                          options.Page &&
                          options.Page.DefaultSignatureModified
                        ) {
                          options.Page.DefaultSignatureModified = false;
                        }
                        options.callback({ message: "success" });
                      }
                    }
                  }
                };
                _user.UpdateUserSettings(userOptions);
              }
            );
          }
        }
      });
      // modalUpdateSignature.find('.optAllUsers').prop('checked', 'checked');
    } else if (modalUpdateSignature.find(".optSelectedUsers").is(":checked")) {
      let field = {
        Name: "MultiLookupUser",
        DisplayName: "User",
        ObjectName: "CommonObjectSchema",
      };
      field.Schema = { UIDataType: "multilookup" };
      let selectedUsers = _controls.GetCtrlValue({
        field: field,
        container: modalUpdateSignature,
        isEjsonFormat: true,
      });
      if (!selectedUsers || selectedUsers.length == 0) {
        swal("Please select the user", { icon: "info" });
        return;
      }
      let counter = 0;
      for (let i = 0; i < selectedUsers.length; i++) {
        let userData = { _id: selectedUsers[i]["$oid"] };
        _user.GetUserSignatureFromDefaultSignature(
          options.companySignature,
          userData,
          function (newSignature) {
            counter += 1;
            let userDataNew = { userId: userData._id };
            _user.GetUsersDetails(userDataNew, function (result) {
              if (result && result.message == "success") {
                let userDataUpdate = {
                  UserID: result.data._id,
                  UserSignature: newSignature || "",
                };
                userDataUpdate.callback = function (updateStatus) {
                  counter -= 1;
                  if (updateStatus && updateStatus.message != "success")
                    options.FailureCount = (options.FailureCount || 0) + 1;
                  if (counter == 0) {
                    if (options.callback) {
                      if ((options.FailureCount || 0) > 0)
                        options.callback({ message: "failed" });
                      else {
                        if (
                          options.Page &&
                          options.Page.DefaultSignatureModified
                        ) {
                          options.Page.DefaultSignatureModified = false;
                        }
                        options.callback({ message: "success" });
                      }
                    }
                  }
                };
                _user.UpdateUserSettings(userDataUpdate);
              }
            });
          }
        );
      }
      // modalUpdateSignature.find('.optSelectedUsers').prop('checked', 'checked');
    }
    modalUpdateSignature.find(".optNobody").prop("checked", "checked");
    modalUpdateSignature.modal("hide");
    modalUpdateSignature.find(".btnSave").unbind("click");
  });
  if (!modalUpdateSignature.find(".optSelectedUsers").is(":checked")) {
    modalUpdateSignature.find(".selectuser").addClass("d-none");
  }
};
_user.UpdateUserSettings = function (options) {
  let sdata = { UserID: options.UserID, UserSignature: options.UserSignature };
  _common.FetchApiByParam(
    {
      url: "/api/user/saveusersignature",
      type: "post",
      contentType: "application/json",
      dataType: "JSON",
      data: sdata,
    },
    function (result) {
      if (result && result.message == "success") {
        if (options.callback) {
          options.callback(result);
        } else {
          swal("Signature has been saved successfully", { icon: "success" });
        }
      } else {
        if (options.callback) {
          options.callback(result);
        } else {
          swal("Unable to save. the Signature, Please try Again", {
            icon: "error",
          });
        }
      }
    }
  );
};
_user.UserLookupCtrl = function (options) {
  let field = {
    Name: options.AutocompleteType,
    ObjectName: "CommonObjectSchema",
    HasAddButton: options.HasAddButton || false,
  };
  field.Schema = {
    UIDataType: options.UIDataType,
    DisplayName: options.DisplayName || "User",
    IsRequired: options.IsRequired || false,
  };
  let fieldCtrl = _controls.BuildCtrl({ field: field });
  return fieldCtrl;
};
_user.UserSyncWithMaster = function (pageObj, operation) {
  if (
    pageObj &&
    pageObj.ResultData &&
    pageObj.ResultData.message == "success" &&
    pageObj.ResultData.data
  ) {
    let sdata = {
      UserID: pageObj.ResultData.data._id,
      CompanyID: _initData.Company._id,
      DMLOperation: operation,
    };
    _common.FetchApiByParam(
      {
        url: "/api/user/syncuserwithmaster",
        type: "post",
        contentType: "application/json",
        dataType: "JSON",
        data: sdata,
      },
      function (result) {
        if (result.message == "success") {
          //swal('User sync done with master database');
        }
      }
    );
  }
};

_user.UpdateNewUserMySettings = function (pageObj) {
  let DefaultTimeZone = _initData.Company.Setting.Timezone;
  let UserID = pageObj.ResultData.data._id;
  let WorkingDays = [];
  let defaultSpace = _initData.User.MySettings.DriveSettings
    ? _initData.User.MySettings.DriveSettings.TotalSpace
    : "10737418240";

  let DriveSettings = {
    CanShare: true,
    TotalSpace: defaultSpace,
    SpaceUsed: 0,
    NeedApproval: true,
    Unit: "byte",
  };
  _initData.Company.Setting.CompanyWorkingDays.forEach((day) => {
    WorkingDays.push({
      WorkingDay: day,
      StartTime: _initData.Company.Setting.WorkingHrsStart,
      Endtime: _initData.Company.Setting.WorkingHrsEnd,
    });
  });
  $.ajax({
    url: "/api/user/UpdateNewUserMySettings",
    type: "post",
    dataType: "json",
    contentType: "application/json",
    data: JSON.stringify({
      DefaultTimeZone,
      WorkingDays,
      UserID,
      DriveSettings,
    }),
    success: function (res) {
      if (res.message == "success") {
      }
    },
  });
};

// _user.GetSuspendedUsersList = function () {
//     _common.FetchApiByParam({
//         url: "/api/user/getsuspendeduserslist"
//         , type: "post"
//         , contentType: 'application/json'
//         , dataType: 'JSON'
//         , data: {}
//     }, function (result) {
//         if (result && result.message == "success") {
//             _user.SuspendedUsers = result.data;
//         } else {
//             _user.SuspendedUsers = [];
//         }
//     });
// }
_user.ReplaceUserManage = function () {
  $.ajax({
    url: "page/user/userreplace.html",
    success: function (res) {
      let modalRUBody = $("<div>" + res + "</div>")
        .find("#content")
        .html();
      let modalRU = _common.DynamicPopUp({
        title: "Replace User",
        body: modalRUBody,
        size: "modal-sm",
      });
      modalRU.modal("show");
      modalRU.addClass("modal-replace-user");
      let fromUserAutocomplete = _user.UserLookupCtrl({
        AutocompleteType: "SingleLookupUser",
        UIDataType: "lookup",
        IsRequired: true,
        HasAddButton: false,
        DisplayName: "From User",
      });
      let toUserAutocomplete = _user.UserLookupCtrl({
        AutocompleteType: "SingleLookupUser",
        UIDataType: "lookup",
        IsRequired: true,
        HasAddButton: false,
        DisplayName: "To User",
      });

      modalRU.find(".from-user").append(fromUserAutocomplete);
      modalRU.find(".to-user").append(toUserAutocomplete);

      let userLookupField = {
        ObjectName: "CommonObjectSchema",
        Name: "SingleLookupUser",
        HasAddButton: false,
        Schema: {
          LookupObject: "User",
          UIDataType: "lookup",
          LookupFields: ["FirstName", "LastName"],
          IsRequired: true,
        },
      };

      modalRU.find(".to-user").find("[data-field]")[0].field = userLookupField;

      modalRU.GetUserDataCounts = function () {
        modalRU.find(".moudules-list .modules").html("");

        let selectedUsers = _controls.GetCtrlValue({
          field: userLookupField,
          container: modalRU.find(".from-user"),
          isEjsonFormat: true,
        });
        if (!selectedUsers || selectedUsers.length == 0) {
          swal('"From" user required', { icon: "info" });
          return;
        }

        let sdata = { UserID: selectedUsers["$oid"], DataCount: true };
        _common.FetchApiByParam(
          {
            url: "/api/user/getuseralldatacount",
            type: "post",
            data: sdata,
          },
          function (result) {
            if (result && result.message == "success") {
              let userDataArray = [];
              for (let i = 0; i < result.data.length; i++) {
                let moduleData = result.data[i];
                let userModuleData = '<li class="pt-1">';
                userModuleData +=
                  '<span class="large badge badge-secondary mr-2 mt-1 mw-30 lblDataCount">' +
                  moduleData.TotalCount +
                  "</span>";
                userModuleData +=
                  (moduleData.Module == "Activity" ? "Upcoming " : " ") +
                  moduleData.Module +
                  " will be replaced.";
                userModuleData += "</li>";
                userDataArray.push(userModuleData);
              }
              userDataArray.sort();
              let userPermissions = "";
              for (let i = 0; i < userDataArray.length; i++) {
                userPermissions += userDataArray[i];
              }

              if (result.data.length == 0)
                userPermissions = "<li class='pt-1'>No Data Found</li>";
              modalRU.find(".moudules-list .modules").html(userPermissions);
            }
            _common.ContainerLoading(false, modalRU.find(".moudules-list"));
          }
        );
      };
      modalRU.find(".btnSave").click(function () {
        if (_common.Validate(modalRU)) {
          _common
            .ConfirmDelete({
              title: "Replace User Confirmation",
              message:
                "All data will be transferred to the selected user, are you sure?",
            })
            .then((confirm) => {
              if (confirm) {
                let fromUser = _controls.GetCtrlValue({
                  field: userLookupField,
                  container: modalRU.find(".from-user"),
                  isEjsonFormat: true,
                });
                let toUser = _controls.GetCtrlValue({
                  field: userLookupField,
                  container: modalRU.find(".to-user"),
                  isEjsonFormat: true,
                });
                if (!fromUser || fromUser.length == 0) {
                  swal('"From" user required', { icon: "info" });
                  return;
                }
                if (!toUser || toUser.length == 0) {
                  swal('"To"  user required', { icon: "info" });
                  return;
                }
                if (fromUser == toUser) {
                  swal("'From' user can't be same as 'To' user", {
                    icon: "info",
                  });
                  return;
                }
                let sdata = {
                  FromUserID: fromUser["$oid"],
                  ToUserID: toUser["$oid"],
                  DateTime: new Date(),
                };
                _common.FetchApiByParam(
                  {
                    url: "/api/user/replaceuser",
                    type: "post",
                    data: sdata,
                  },
                  function (result) {
                    _common.ContainerLoading(
                      false,
                      modalRU.find(".moudules-list .modules")
                    );
                    swal("Data has been updated sucessfully", {
                      icon: "success",
                    });
                    modalRU.parent().find(".UserList")[0].grid.ReloadData();
                    modalRU.modal("hide");
                    _user.UserLicenseConsumeCalculate();
                  }
                );
              }
            });
        }
      });
      modalRU.find(".from-user").on("click", ".fa-times", function () {
        modalRU.find(".moudules-list .modules").html("");
      });

      modalRU.find(".from-user").on("click", ".msp-lookup .item", function () {
        setTimeout(function () {
          let fromUser = _controls.GetCtrlValue({
            field: userLookupField,
            container: modalRU.find(".from-user"),
            isEjsonFormat: true,
          });
          let toUser = _controls.GetCtrlValue({
            field: userLookupField,
            container: modalRU.find(".to-user"),
            isEjsonFormat: true,
          });

          let flag = true;
          if (fromUser && toUser) {
            if (fromUser.$oid == toUser.$oid) {
              swal("'From' user can't be same as 'To' user", { icon: "info" });
              modalRU.find(".from-user").find(".lookup-item[data-id]").remove();
              modalRU
                .find(".from-user .msp-ctrl-lookup")
                .attr("contenteditable", "true");
              flag = false;
            }
          }
          if (fromUser && flag == true) {
            _common.ContainerLoading(true, modalRU.find(".moudules-list"));
            modalRU.GetUserDataCounts();
          }
        }, 300);
      });
      modalRU.find(".to-user").on("click", ".msp-lookup .item", function () {
        setTimeout(function () {
          let fromUser = _controls.GetCtrlValue({
            field: userLookupField,
            container: modalRU.find(".from-user"),
            isEjsonFormat: true,
          });
          let toUser = _controls.GetCtrlValue({
            field: userLookupField,
            container: modalRU.find(".to-user"),
            isEjsonFormat: true,
          });
          if (fromUser && toUser) {
            if (fromUser.$oid == toUser.$oid) {
              swal("'From' user can't be same as 'To' user", { icon: "info" });
              modalRU.find(".to-user").find(".lookup-item[data-id]").remove();
              modalRU
                .find(".to-user .msp-ctrl-lookup")
                .attr("contenteditable", "true");
            }
          }
        }, 200);
      });
    },
    error: function (res) {
      swal(_constantClient.ERROR_MSG, { icon: "error" });
    },
  });
};
_user.CheckUserLicense = async function (userPageObj) {
  return new Promise(function (resolve, reject) {
    _common.FetchApiByParam(
      {
        url: "/api/user/CheckUserLicense",
        type: "post",
        contentType: "application/json",
        dataType: "JSON",
        data: {},
      },
      function (result) {
        if (result) {
          resolve(result);
        } else {
          reject({ message: "error" });
        }
      }
    );
  });
};
_user.UserLicenseConsumeCalculate = function () {
  _common.FetchApiByParam(
    {
      url: "/api/user/UserLicenseConsumeCalculate",
      type: "post",
      contentType: "application/json",
      dataType: "JSON",
      data: {},
    },
    function (result) {
      if (result && result.message == "success" && result.data) {
        _initData.Company.Setting.NoOfUsersLicense =
          result.data.NoOfUsersLicense;
        _initData.Company.Setting.NoOfLicenseConsumed =
          result.data.NoOfLicenseConsumed;
        _user.RefreshLicenseInformation();
      } else {
        swal("Details not found", { icon: "info" });
      }
    }
  );
};
_user.UserSignoutFromAllDevices = function (options) {
  _common.FetchApiByParam(
    {
      url: "/api/user/signoutuseralldevices",
      type: "post",
      contentType: "application/json",
      dataType: "JSON",
      data: options,
    },
    function (result) {}
  );
};
_user.TakeUserEmailPassword = function (forceChangePassword) {
  if (
    (_initData.User.MySettings &&
      (_initData.User.MySettings.IsValidEmailPassword || false) == false) ||
    forceChangePassword == true
  ) {
    let url = "/page/user/useremailpassword.html";
    _common.GetStaticPage(url, function (pageHTML) {
      let emailPasswordValidationBody = $("<div>" + pageHTML + "</div>");
      var modalEmailPass = _common.DynamicPopUp({
        title: "Email Password Varification Mandatory",
        body: emailPasswordValidationBody.find("#content").html(),
      });
      modalEmailPass.modal({
        keyboard: false,
      });
      modalEmailPass.find(".modal-header, .modal-footer").remove();
      if (
        _initData.Company &&
        _initData.Company.Setting &&
        _initData.Company.Setting.EmailIntegration &&
        _initData.Company.Setting.EmailIntegration.EmailAccountName
      )
        modalEmailPass
          .find(".lblEmailAccountName")
          .text(
            _initData.Company.Setting.EmailIntegration.EmailAccountName.toUpperCase()
          );

      modalEmailPass.find(".lblUserEmail").text(_initData.User.Email);
      modalEmailPass.find(".txtEmailPassword").keyup(function () {
        if ($(this).val().trim().length > 0) {
          modalEmailPass.find(".btnSave").removeAttr("disabled");
        } else {
          modalEmailPass.find(".btnSave").attr("disabled", "disabled");
        }
      });
      modalEmailPass.find(".btnSave").click(function () {
        modalEmailPass.find(".imgVerifyPassword").removeClass("d-none");
        let sdata = {
          UserEmailId: _initData.User.Email,
          EmailPassword: modalEmailPass.find(".txtEmailPassword").val(),
        };
        _common.FetchApiByParam(
          {
            url: "/api/user/verifyemailpassword",
            type: "post",
            contentType: "application/json",
            dataType: "JSON",
            data: sdata,
          },
          function (result) {
            modalEmailPass.find(".imgVerifyPassword").addClass("d-none");
            if (result && result.message == "success") {
              let emailPasswordObj = {};
              emailPasswordObj.EmailPassword = modalEmailPass
                .find(".txtEmailPassword")
                .val();
              emailPasswordObj.IsValidEmailPassword = true;
              _common.FetchApiByParam(
                {
                  url: "/api/user/savemyemailpassword",
                  type: "post",
                  contentType: "application/json",
                  dataType: "JSON",
                  data: emailPasswordObj,
                },
                function (result) {
                  if (result && result.message == "success") {
                    modalEmailPass.modal("hide");
                    swal("Password has been saved successfully", {
                      icon: "success",
                    });
                    _user.GetLoginUserData();
                  } else {
                    swal("Unable to save the password. Please try again", {
                      icon: "error",
                    });
                  }
                }
              );
            } else {
              swal(
                "Unable to Integrate, Please contact system administrator.",
                { icon: "error" }
              );
            }
          }
        );
      });
    });
  } else {
    _common.FetchApiByParam(
      {
        url: "/api/user/verifyloginuseremailpassword",
        type: "post",
        contentType: "application/json",
        dataType: "JSON",
        data: {},
      },
      function (result) {
        if (result && result.message == "failed") {
          _user.TakeUserEmailPassword(true);
        }
      }
    );
  }
};
_user.GetCookies = function (cname) {
  let name = cname + "=";
  let ca = document.cookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};
_user.SetLoginUserName = function () {
  if (document.cookie && document.cookie.includes("user_email=")) {
    let UserName = _user.GetCookies("user_email");
    $(".txtPassword").click();

    $(".txtEmail").click().val(UserName);
  }
  $(".txtEmail").focus();

  $(".txtEmail,.txtPassword").keypress(function (e) {
    if (e.keyCode == 13) $(".btnLogin").click();
  });

  $(".btnLogin").click(function () {
    _user.Login($(".txtEmail").val(), $(".txtPassword").val());
  });
};

//Events Page : UserDetail
_user.EmailAvailabilityCheck = function (page) {
  page.PageContainer.find('[data-field="Email"]').on("change", function () {
    if (!_common.isValidEmailAddress($(this).val())) return;
    _user.CheckDuplicateEmailInMaster(page);
  });
};

_user.CheckDuplicateEmailInMaster = function (page) {
  let emailField = page.PageContainer.find('[data-field="Email"]');
  let emailID = emailField.val();
  if (page._id != undefined) {
    if (page.Data.Email == emailID) return true;
  }
  let isValid = true;
  let param = { Email: emailID };
  $.ajax({
    url: "/api/user/checkduplicateemailmaster",
    async: false,
    type: "post",
    contentType: "application/json",
    data: JSON.stringify(param),
    success: function (result) {
      if (result && result.message == "ExistInSameComapny") {
        let errorMsg = "Email already exists.";
        _common.ValidateAddMsg(emailField, errorMsg);
        isValid = false;
      } else if (result && result.message == "ExistInDiffComapny") {
        let errorMsg = "Email already exists in other company.";
        _common.ValidateAddMsg(emailField, errorMsg);
        isValid = false;
      }
    },
    error: function (err) {
      console.log(err);
    },
  });
  return isValid;
};

_user.checkUserLoginStatus = function () {
  let status = sessionStorage.getItem("status");
  console.log(status);
  if (status) {
    let msg = "";
    switch (status) {
      case "User_not_found":
        msg =
          "Your access has been disabled. Please contact System Administrator for more information.";
        break;
      case "Session_expire":
        msg = "Your session has expired. Please login again.";
        break;
    }
    sessionStorage.removeItem("status");
    if (!msg) return;
    $(".printError").text(msg);
    $(".NotValid").removeClass("d-none");
  }
};

_user.IsPasswordValid = function (password) {
  return /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(
    password
  );
};
_user.ResetUserPassword = function (page) {
  let container = $(page.PageContainer);

  container.find(".view-hidepassword").on("click", function () {
    if ($(this).hasClass("fas fa-eye")) {
      $(this).removeClass("fas fa-eye").addClass("fas fa-eye-slash");
      container.find(".password").attr("type", "password");
    } else {
      container.find(".password").attr("type", "text");
      $(this).removeClass("fas fa-eye-slash").addClass("fas fa-eye");
    }
  });

  container.find(".password, .confirmpassword").on("change", function (event) {
    let password = container.find(".password").val();
    let confirmpassword = container.find(".confirmpassword").val();

    if (!_user.IsPasswordValid(password) || password.length < 7) {
      container.find(".new-password-invalid").removeClass("d-none");
    }

    if (confirmpassword !== password && confirmpassword.length > 0) {
      container.find(".confirm-password-match").removeClass("d-none");
    }
  });

  container.find(".password, .confirmpassword").on("input", function () {
    let password = container.find(".password").val();
    let confirmpassword = container.find(".confirmpassword").val();
    if (password == confirmpassword) {
      container.find(".confirm-password-match").addClass("d-none");
    }
    if (_user.IsPasswordValid(password)) {
      container.find(".new-password-invalid").addClass("d-none");
    }
    if (password) {
      container.find(".current-password-required").addClass("d-none");
    } else {
      container.find(".current-password-required").addClass("d-none");
    }
  });

  container.find(".current-password").on("input", function () {
    container.find(".current-password-required").addClass("d-none");
  });

  container.find(".resetpassword").on("click", function () {
    let password = container.find(".password").val();
    let confirmpassword = container.find(".confirmpassword").val();
    let currentPassword = container.find(".current-password").val();
    if (!currentPassword || currentPassword == "") {
      container.find(".current-password-required").removeClass("d-none");
      return;
    }
    if (!_user.IsPasswordValid(password) || password !== confirmpassword) {
      return;
    }
    let userGMTTimezone = _common.GetGMTTimezone(
      _common.GetListSchemaDataFromInitData("Timezone"),
      _initData.User.MySettings.Timezone
    );
    let UserTimezone = _initData.User.MySettings.Timezone;
    _user.ResetPassword(
      {
        CurrentPassword: currentPassword,
        NewPassword: password,
        Timezone: userGMTTimezone,
        UserTimezone: UserTimezone,
      },
      function (res) {
        if (res.error) {
          swal(res.error, { icon: "error" });
        } else {
          swal("Password updated successfully", { icon: "success" }).then(
            (done) => {
              document.cookie.split(";").forEach(function (c) {
                document.cookie = c
                  .replace(/^ +/, "")
                  .replace(
                    /=.*/,
                    "=;expires=" + new Date().toUTCString() + ";path=/"
                  );
              });
              window.location.href = "/";
            }
          );
        }

        container.find(".password").val("");
        container.find(".confirmpassword").val("");
        container.find(".current-password").val("");
      }
    );
  });
};
