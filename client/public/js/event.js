const eventobj = {};

eventobj.SendMailWithIcs = function (sdata, eventType, receiversIds) {
    let activityData = Object.assign({}, sdata.ResultData);
    let eventObject = {};
    eventObject.start = moment.utc(moment.tz(activityData.StartDate, activityData.Timezone)).format('YYYY-M-D-H-m').split("-");
    eventObject.end = moment.utc(moment.tz(activityData.EndDate, activityData.Timezone)).format('YYYY-M-D-H-m').split("-");
    eventObject.startInputType = 'utc';
    eventObject.endInputType = 'utc';
    if ((eventType == 'adminDelete' || eventType == 'contactRemoved' || eventType == 'userRemoved') && eventType != undefined) {
        eventObject.method = 'CANCEL';
        eventObject.status = 'CANCELLED';
        eventObject.sequence = activityData.Sequence + 1;
    } else {
        eventObject.method = 'REQUEST';
        eventObject.status = 'CONFIRMED';
        eventObject.sequence = activityData.Sequence;
    }
    eventObject.title = activityData.ActivityType + " : '" + activityData.Subject + "'";
    eventObject.description = (activityData.IsRecurrence ? "Invited to attend the Recurrence " : "Invited to attend the ") + activityData.ActivityType + " '" + activityData.Subject + "'";
    eventObject.uid = "<<<" + activityData._id + "--" + _initData.Company._id + ">>>";
    eventObject.organizer = { name: _initData.User.FirstName + ' ' + _initData.User.LastName, email: _constantClient.ActivityNotificationEmail };
    eventObject.location = eventobj.CreateAddressString(activityData);

    let attendeesContacts = [];
    if (activityData.Contacts != undefined) {
        for (let i = 0; i < activityData.Contacts.length; i++) {
            attendeesContacts.push(_controls.ToEJSON("objectid", activityData.Contacts[i]));
        }
    }
    activityData.eventObj = eventObject;
    activityData.attendeesContacts = attendeesContacts;

    activityData.UserName = _initData.User.FirstName + ' ' + _initData.User.LastName;
    activityData.UserEmail = _initData.User.Email;
    activityData.eventType = eventType;
    activityData.receiversIds = receiversIds;//NOTE to send mail to specific users
    console.log(activityData);
    $.ajax({
        url: 'api/event/sendmailwithics',
        data: JSON.stringify(activityData),
        type: 'post',
        contentType: 'application/json',
        success: function (result) {
            console.log(result);
            //onFetch(result);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

eventobj.SetActivityNotification = function (activityId, type, receiversIds) {
    _calendar.GetActivityCreaterAndCurrentUser(activityId, (data) => {
        let { creater, loggedinUser, otherUsers, activityData } = data;
        let createrId = { UserID: _controls.ToEJSON("objectid", creater.Id), IsSeen: false };
        let loggedinUserId = { UserID: _controls.ToEJSON("objectid", loggedinUser.Id), IsSeen: false };
        let otherUserIds = otherUsers.filter(u => u.Type == 'user').map(u => { return { IsSeen: false, UserID: _controls.ToEJSON("objectid", u.Id) } })
        notificationObj = {
            EventObjectName: 'Activity',
            EventID: { $oid: activityData._id },
            Users: []
        }
        switch (type) {
            case 'newActivity':
                if (receiversIds) {
                    receiversIds.forEach(
                        usr => {
                            notificationObj.Users.push(otherUserIds.find(u => u.UserID.$oid == usr))
                        }
                    )
                } else {
                    notificationObj.Users = [...otherUserIds]
                }


                if (activityData.IsRecurrence) {
                    notificationObj.Message = `${loggedinUser.Name} has invited you to join a new Recurrence ${activityData.ActivityType} '${activityData.Subject}'`;
                } else {
                    notificationObj.Message = `${loggedinUser.Name} has invited you to join a new ${activityData.ActivityType} '${activityData.Subject}'`;
                }
                notification.SetNotifications(notificationObj)
                break;

            // case 'newRecurrenceActivity':
            //     notificationObj.Users = [...otherUserIds]
            // notificationObj.Message = `${loggedinUser.Name} has invited you to join a new Recurrence ${activityData.ActivityType} '${activityData.Subject}'`
            //     notification.SetNotifications(notificationObj)
            //     break;

            case 'accepted':
                notificationObj.Users = [createrId];
                notificationObj.Message = `${loggedinUser.Name} has accepted your invitation for the ${activityData.ActivityType} '${activityData.Subject}'`;
                notification.SetNotifications(notificationObj)

                // notificationObj.Users = [loggedinUserId];
                // notificationObj.Message = `You have accepted an invitation  from  ${creater.Name}  for ${activityData.ActivityType}.`;
                // notification.SetNotifications(notificationObj)
                break;
            case 'rejected':
                notificationObj.Users = [createrId];
                notificationObj.Message = `${loggedinUser.Name} has rejected your invitation for the ${activityData.ActivityType} '${activityData.Subject}'`;
                notification.SetNotifications(notificationObj)

                // notificationObj.Users = [loggedinUserId];
                // notificationObj.Message = `You have rejected an invitation from  ${creater.Name}  for ${activityData.ActivityType}.`;
                // notification.SetNotifications(notificationObj)
                break;
            case 'reschedule':
                notificationObj.Users = [...otherUserIds];
                notificationObj.Message = `${creater.Name} has rescheduled the ${activityData.ActivityType} '${activityData.Subject}'`;
                notification.SetNotifications(notificationObj)
                break;

            case 'adminDelete':
                notificationObj.Users = [...otherUserIds];
                notificationObj.Message = `${creater.Name} has deleted the ${activityData.ActivityType} '${activityData.Subject}'`;
                notification.SetNotifications(notificationObj);
                break;

            case 'attendeeDelete':
                notificationObj.Users = [createrId];
                notificationObj.Message = `${loggedinUser.Name} has rejected your ${activityData.ActivityType} '${activityData.Subject}'`;
                notification.SetNotifications(notificationObj);
                break;

            case 'userRemoved':
                notificationObj.Users = receiversIds.map(u => {
                    return {
                        IsSeen: false, UserID: _controls.ToEJSON("objectid", u)
                    }
                });
                notificationObj.Message = `${loggedinUser.Name} has removed your from the ${activityData.ActivityType} '${activityData.Subject}'`;
                notification.SetNotifications(notificationObj);
                break;

            case 'attendeeAddedByAttendee':
                notificationObj.Users = [createrId]
                notificationObj.Message = `${loggedinUser.Name} has add new attendee to ${activityData.ActivityType} '${activityData.Subject}'`;
                notification.SetNotifications(notificationObj);
                break;

        }

    })


}


eventobj.SendMailToUserParticipants = function (activityId, type, receiversIds) {

    $.ajax({
        url: 'api/event/sendmailtouserparticipants',
        data: JSON.stringify({ activityId, type, receiversIds }),
        type: 'post',
        contentType: 'application/json',
        success: function (result) {
            console.log(result);
            //onFetch(result);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

eventobj.GenericPushNoitification = function (id, type, receiversIds) {
    $.ajax({
        url: 'api/event/genericPushNotification',
        type: 'post',
        data: JSON.stringify({ id, type, receiversIds }),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            // callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}


// const event1 = {
//     start: moment(eventObj.start).format('YYYY-M-D-H-m').split("-"),
//     duration: { hours: 6, minutes: 30 },
//     method: 'REQUEST',
//     status: 'CONFIRMED',
//     title: 'Saturday Office',
//     description: 'Invited to attend office on saturday',
//     location: 'Innovanda , WTT , Noida Sector-16 ,UP,India',
//     url: 'http://www.innovanda.com/',
//     organizer: { name: 'Hamza', email: 'hamza@innovanda.com' },
//     attendees: [
//         { name: 'Hamza Khan', email: 'hamza@innovanda.com', rsvp: true, role: 'REQ-PARTICIPANT' }
//     ]
// }
eventobj.CreateAddressString = function (activityData) {
    let address = "";
    if(activityData.VenueName != undefined){
        address += activityData.VenueName + ', ';
    }
    if(activityData.Address != undefined){
        address += activityData.Address + ', ';
    }
    if(activityData.City_LookupData != undefined){
        address += activityData.City_LookupData.Name + ', ';
    }
    if(activityData.State != undefined){
        address += activityData.State + ', ';
    }
    if(activityData.Country_LookupData != undefined){
        address += activityData.Country_LookupData.Value;
    }
    if(activityData.PostalCode != undefined){
        address += ', ' + activityData.PostalCode ;
    }
    return address;
}


eventobj.ExecuteCommand = function(pageDetail){
    if(pageDetail.Data == null){
        return false;
    } 
    if(pageDetail.Data.Queries == null || pageDetail.Data.Queries.length < 1){
        alert('No queries found');
        return false;
    }
    else{
        eventobj.ExecuteFormModal(pageDetail,function(){
            
        });
        let queries = pageDetail.Data.Queries;
        
    }
}

eventobj.ExecuteFormModal = function (page) {
    console.log(page)
    let formModal = _common.DynamicPopUp({ title: "Execute", body: "" });

    formModal.find(".btnSave").html("Execute").removeClass("btnSave").addClass("btnExecute");

    let DatabaseFieldSchema = {
        Name: "Database",
        UIDataType: "multilookup", DisplayName: "Databases",
        LookupObject: "Database",
        LookupFields: [
            "Name"
        ]
    };

    let EnvironmentFieldSchema = {
        Name: "Environment",
        UIDataType: "dropdown", DisplayName: "Environment",
        ListSchemaName: "EnvironmentList"
    };

    let DatabaseFieldObj = {
        Schema: DatabaseFieldSchema,
        ColumnWidth: 8,
        Name: "Database",
        Attribute: 'data-tagObject=\"Database\"',
        Match: '{\"TagType\": \"Database\"}',
        ObjectName: "Database",
        IsRequired: true
    }

    let EnvironmentFieldObj = {
        Schema: EnvironmentFieldSchema,
        ColumnWidth: 4,
        Name: "Environment",
        ObjectName: "Environment",
        IsRequired: true
    }

    let dbparams = {};
    let envparams = {};
    let Data = page.Data;
    let frm = formModal.find('.modal-body').append('<div class="row"></div>').find('.row');
    // params.data = entityData; //To pass data in multilookup
    dbparams.field = DatabaseFieldObj;
    dbparams.container = frm;
    dbparams.data = Data;
    _page.BuildField(dbparams);

    envparams.field = EnvironmentFieldObj;
    envparams.container = frm;
    _page.BuildField(envparams);


    let override = '';
    let overrideId = "override" + (new Date()).getMilliseconds();
    let appendId = "append" + (new Date()).getMilliseconds();
    
    eventobj.SortQueriesBySequence(Data.Queries);
    let querylist = $('<div class = "query-list"><h6 class="font-weight-bold border-bottom pb-2">Queries<span class="clearfix"></span></h6></div>');
    formModal.find('.modal-body').append(querylist); 
    for (let i = 0; i < Data.Queries.length; i++) {
        const query = Data.Queries[i];
        let queryElm = $(`<label class="lblChk query">
        <input type="checkbox" class="event-filter-item chkHide" data-key="${query._id}" checked="">
        <span class="checkmark"></span>${query.Query}</label>`);  
        formModal.find('.query-list').append(queryElm);  
    }

    formModal.find('.btnExecute').click(function () {
        let sdata = {};
        // sdata.objectName = page.objectName;
        // sdata.records = page.ids;
        dbparams.isEjsonFormat = true;
        envparams.isEjsonFormat = true;
        sdata.databases = _controls.GetCtrlValue(dbparams);
        sdata.environment = _controls.GetCtrlValue(envparams);
        if (sdata.databases == null) {
            alert('Please select atleast one Database')
        }
        else if (sdata.environment == null) {
            alert('Please select an Environment')
        }
        else {
            formModal.find('.btnExecute').attr('disabled',true);
            for (let i = 0; i < Data.Queries.length; i++) {
                const query = Data.Queries[i];
                sdata.query= query.Query;
                sdata.queryId = query._id;
                eventobj.ExecuteQuery(sdata,function(res){
                    console.log(res);
                });
            }
            // _common.Loading(true);
            // _tag.AddTagToObject(sdata, function (result) {
            //     formModal.modal('hide');
            //     page.callback(result);
            // })
        }
    });

    formModal.modal('show')

}

eventobj.ExecuteQuery = function(params,callback){
    $.ajax({
        url: 'api/event/executequery',
        type: 'post',
        data: JSON.stringify(params),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
             callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

eventobj.SortQueriesBySequence = function (queries) {
    if (queries != undefined && queries.length > 0) {
        function compare(a, b) {
            const genreA = moment(a.Sequence);
            const genreB = moment(b.Sequence);

            let comparison = 0;
            if (genreA > genreB) {
                comparison = 1;
            } else if (genreA < genreB) {
                comparison = -1;
            }
            return comparison;
        }
        queries.sort(compare);
    }
}