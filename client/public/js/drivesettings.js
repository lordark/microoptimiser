_driveSettings = {};

_driveSettings.InitDriveSettings = (page) => {
     _driveSettings.allUpdates = {};
    _driveSettings.GetMyDriveDetails(1, 10).then(({ data }) => {
        new _driveSettings.DriveSettings(page, data, 1, 10)
    })

    // let updateBtn = this.PageContainer.find('.updatebtn');
    page.PageContainer.on('click', '.updatebtn .btnCancelUpdate', function() {
        _driveSettings.allUpdates = {}
         page.PageContainer.find('.updatebtn').addClass('d-none');
         page.PageContainer.find('.msp-user-filter-ctrl').val('')
         page.PageContainer.find('.msp-user-type-filter-ctrl').val('');
         page.DriveSetting.ReloadData(page.DriveSetting.PageNumber, page.DriveSetting.PageSize,{});
    })
    page.PageContainer.on('click', '.updatebtn .btnSaveAllUpdated', function() {
        _common.ContainerLoading(true, page.PageContainer.find('.Drive-Space-Permission'));
        let UpdateArray = Object.values(_driveSettings.allUpdates)
        _driveSettings.UpdateUserSettings(UpdateArray).then((res) => {
        page.PageContainer.find('.updatebtn').addClass('d-none');
            if( res.statuscode ==  414 ){
                swal(res.message, { icon: "error" });
            }else{
             swal(res.message, { icon: "success" });
            }
            _driveSettings.allUpdates = {};    
            page.DriveSetting.ReloadData(page.DriveSetting.PageNumber, page.DriveSetting.PageSize,page.DriveSetting.SearchObject);
        }).finally(()=>{
            _common.ContainerLoading(false, page.PageContainer.find('.Drive-Space-Permission'));
        })

    })
    page.PageContainer.on('click', '.paging .pagination .page-item', function(e) {

        let pid = parseInt($(this).closest("li").attr('pid'));
        let pager = $(this).closest('.pagination')[0].pager;
        page.PageContainer.find('.updatebtn').addClass('d-none');
        if (pid < 1 || pid > pager.totalPages || pid == pager.currentPage) {
            return false;
        }
        page.DriveSetting.PageNumber = pid;
        page.DriveSetting.ReloadData(page.DriveSetting.PageNumber, page.DriveSetting.PageSize, {});
    });

    page.PageContainer.on('keyup','.msp-user-filter-ctrl', function(e)  {
        let SearchObject = { 'filter': { key: 'FullName', value: e.target.value } };
        if (e.target.value.length > 2) {
            page.DriveSetting.ReloadData(page.DriveSetting.PageNumber, page.DriveSetting.PageSize, SearchObject)
        } else if (e.target.value.length == 0) {
            page.DriveSetting.ReloadData(page.DriveSetting.PageNumber, page.DriveSetting.PageSize, SearchObject)
        }
    });

    page.PageContainer.on('keyup', '.msp-user-type-filter-ctrl',(e) => {
        let SearchObject = { 'filter': { key: 'Profile', value: e.target.value } };
        if (e.target.value.length > 2) {
            page.DriveSetting.ReloadData(page.DriveSetting.PageNumber, page.DriveSetting.PageSize, SearchObject)
        } else if (e.target.value.length == 0) {
            page.DriveSetting.ReloadData(page.DriveSetting.PageNumber, page.DriveSetting.PageSize, {})
        }
    });

    page.PageContainer.on('click', '.canShare',(e) => {
        page.PageContainer.find('.updatebtn').removeClass('d-none')
        let UserId = $(e.target).attr('data-userid');
        let CheckStatus = $(e.target).prop('checked');
        (CheckStatus === true) ? $(e.target).attr('checked', false): $(e.target).attr('checked', true);
        let updateObj = { "UserID": UserId, "CanShare": (CheckStatus === true) ? true : false };
        _driveSettings.FilterUpdateObject(updateObj, _driveSettings.allUpdates)

    });

    page.PageContainer.on('change','.spacecap' ,(e) => {
        console.log("vdf;jviou798897")
        page.PageContainer.find('.updatebtn').removeClass('d-none')
        let UserId = $(e.target).attr('data-userid');
        let SelectedValue = $(e.target).val();
        //  let UpdateObj = { "UserID": UserId.toString() ,"key":"TotalSpace", "value": _driveSettings.ConvertGBtoByte(parseInt(SelectedValue)) }
        let updateObj = { "UserID": UserId, "TotalSpace": _driveSettings.ConvertGBtoByte(parseInt(SelectedValue)) }
        _driveSettings.FilterUpdateObject(updateObj, _driveSettings.allUpdates)
        //  _driveSettings.UpdateUserSettings(UpdateObj).then((res)=>{
        //     swal(res.message, { icon: "info" });
        // })
    })


    page.PageContainer.on('click','.needApproval', (e) => {
         page.PageContainer.find('.updatebtn').removeClass('d-none')
        let UserId = $(e.target).attr('data-userid');
        let CheckStatus = $(e.target).prop('checked');
        (CheckStatus === true) ? $(e.target).attr('checked', false): $(e.target).attr('checked', true);
        let UpdateObj = { "UserID": UserId, "NeedApproval": (CheckStatus === true) ? true : false }
        _driveSettings.FilterUpdateObject(UpdateObj, _driveSettings.allUpdates)
            // _driveSettings.UpdateUserSettings(UpdateObj).then((res)=>{
            //     swal(res.message, { icon: "info" });
            // })
    });   
}

_driveSettings.isInt = (n) => {
    return n % 1 === 0;
}




_driveSettings.ConvertSize = function(bytes) {
    let Size
    switch (true) {
        case bytes < (1024 * 1024):
            Size = `${(bytes / 1024).toFixed(2)} Kb`
            break;
        case bytes >= (1024 * 1024) && bytes < (1024 * 1024 * 1024):
            Size = `${(bytes / (1024 * 1024)).toFixed(2)} Mb`
            break;
        default:
            Size = `${(bytes / (1024 * 1024 * 1024)).toFixed(2)} Gb`
            break;

    }

    return Size;
}

_driveSettings.ConvertBytetoGB = (byte) => {
    let gb;
    (_driveSettings.isInt((byte / 1073741824))) ? gb = (byte / 1073741824): gb = (byte / 1073741824).toFixed(2);
    return gb
}
_driveSettings.ConvertGBtoByte = (gb) => {
    return (gb * 1073741824)
}

_driveSettings.UpdateUserSettings = (ToUpdate) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: 'api/drive/updatedrivesettings',
            type: 'POST',
            data: JSON.stringify({ UserData: ToUpdate }),
            contentType: 'application/json',
            success: function(res) {
                resolve(res);
            },
            error: function(err) {
                reject(err)
            }
        })
    }).catch((err) => {
        swal(message, { icon: "error", });
    });
}


_driveSettings.GetMyDriveDetails = (currentPage, pageSize, { filter } = {}) => {
    return new Promise((resolve, reject) => {

        $.ajax({
            url: 'api/drive/drivesettings',
            type: 'POST',
            data: JSON.stringify({ 'pageNo': currentPage, 'size': pageSize, filter }),
            contentType: 'application/json',
            success: function(res) {
                resolve(res);
            },
            error: function(err) {
                reject(err)
            }
        })
    }).catch((err) => {
        swal(err.message, { icon: "error", });
    });
}


_driveSettings.FilterUpdateObject = function(updateObj, allUpdates) {

    if (!allUpdates[updateObj.UserID]) {
        allUpdates[updateObj.UserID] = updateObj
    } else {
        let AllUpdatesKeys = Object.keys(allUpdates[updateObj.UserID]);
        let UpdateObjKeys = Object.keys(updateObj)

        for (let j = 0; j < AllUpdatesKeys.length; j++) {
            for (let m = 0; m < UpdateObjKeys.length; m++) {
                if (AllUpdatesKeys[j] == UpdateObjKeys[m]) {
                    allUpdates[updateObj.UserID][UpdateObjKeys[m]] = updateObj[UpdateObjKeys[m]]
                } else if (!AllUpdatesKeys.find(o => o == UpdateObjKeys[m])) {
                    allUpdates[updateObj.UserID][UpdateObjKeys[m]] = updateObj[UpdateObjKeys[m]]

                }
            }
        }

    }
    return allUpdates

}


_driveSettings.DriveSettings = function(page, data, pageNumber, pageSize) {
    this.PageContainer = page.PageContainer;
    this.Data = data;
    this.PageNumber = pageNumber;
    this.PageSize = pageSize
    this.TotalCount = data.TotalCount;
    page.DriveSetting = this;
    this.ReloadData = (pageNumber, pageSize = 10, searchObject) => {
        this.SearchObject = searchObject
        _common.ContainerLoading(true, page.PageContainer.find('.Drive-Space-Permission'));
        _driveSettings.GetMyDriveDetails(pageNumber, pageSize, searchObject).then(({ data }) => {
            this.Data = data;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize
            this.TotalCount = data.TotalCount;
            this.DataBind();
        })


    };

    this.BildSpaceCapOptions = (UserSpace, CompanySpace) => {
        let options = '';
        if (UserSpace === CompanySpace) {
            options = `<option selected> Unlimited</option>`
        } else {
            options = `<option >Unlimited</option>`
        }
        for (let i = 0; i < CompanySpace - 1; i++) {
            if (UserSpace === (i + 1)) {
                options = options + `<option selected > ${ i + 1}</option>`
            } else {
                options = options + `<option > ${ i + 1}</option>`
            }
        }

        return options
    };

    this.DataBind = () => {

        let UserDetailContainer = this.PageContainer.find('.user-details');
        let ProgressBar = this.PageContainer.find('.progress-bar');
        let SpaceStatus = this.PageContainer.find('.space-status')
            //to set value to default
        SpaceStatus.text(`0 GB of 0 GB used`)
        ProgressBar.css("width", 0 + '%');
        UserDetailContainer.html('')

        let TotalSpace = _driveSettings.ConvertSize(this.Data.CompanyDetails.DriveSetting.TotalSpace);
        let SpaceOccupied = _driveSettings.ConvertSize(this.Data.CompanyDetails.DriveSetting.SpaceUsed);
        let DriveStatus = `${SpaceOccupied}  of ${TotalSpace} used`
        let SpaceOccupiedPercent = ((this.Data.CompanyDetails.DriveSetting.SpaceUsed) / (this.Data.CompanyDetails.DriveSetting.TotalSpace) * 100).toFixed(2);
        ProgressBar.css("width", SpaceOccupiedPercent + '%');
        SpaceStatus.text(DriveStatus)
        for (let j = 0; j < this.Data.UserDetails.length; j++) {
            let UserDetails = `  <tr>
                                 <td>
                                     <div>${this.Data.UserDetails[j].FullName}</div>
                                 </td>
                                 <td>
                                    <div>
                                         ${this.Data.UserDetails[j].Profile}
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        ${_driveSettings.ConvertSize(this.Data.UserDetails[j].DriveSettings.SpaceUsed)} 
                                    </div>
                               </td>
                               <td>
                                <div>
                                    <select data-userid=${this.Data.UserDetails[j]._id} class="form-control spacecap form-control-sm mt-1 mb-1">
                                       ${this.BildSpaceCapOptions(_driveSettings.ConvertBytetoGB(this.Data.UserDetails[j].DriveSettings.TotalSpace),_driveSettings.ConvertBytetoGB(this.Data.CompanyDetails.DriveSetting.TotalSpace))}
                                    </select>
                                </div>
                             </td>
                             <td >
                               <div class="justify-content-center"><label class="lblChk"><input type="checkbox" data-userid=${this.Data.UserDetails[j]._id}  ${this.Data.UserDetails[j].DriveSettings.CanShare ?  'checked' : '' } class="chkSelect canShare"><span class="checkmark" ></span></label></div>
                            </td>
                            <td class="d-none">
                                <div><label class="lblChk"><input type="checkbox"  data-userid=${this.Data.UserDetails[j]._id} ${this.Data.UserDetails[j].DriveSettings.NeedApproval ? 'checked' : '' } class="chkSelect needApproval"><span class="checkmark"   ></span></label></div>
                            </td>
                        </tr>`

            UserDetailContainer.append(UserDetails)
        }
        let pager = _common.PagerService(this.TotalCount, this.PageNumber, this.PageSize);
        _common.BuildPaging(pager, this.PageContainer.find('.paging'));
        _common.ContainerLoading(false, page.PageContainer.find('.Drive-Space-Permission'));
    };
    this.Init = () => {
        _common.ContainerLoading(true, page.PageContainer.find('.Drive-Space-Permission'));
        this.DataBind();
    }

    this.Init();


}