const _setup = {};

_setup.PageInitObjectList = function (page) {
    _common.FetchApiByParam({ 'url': '/api/setup/getobjectschemalist' }, function (result) {
        for (let i = 0; i < result.data.length; i++) {
            let obj = result.data[i];
            let tr = '<tr data-id="' + obj._id + '"><td>' + obj.Name + '</td><td>' + obj.DisplayName + '</td><td><button class="btn btn-sm btn-primary btn-fields">Fields</button></td></tr>';
            page.PageContainer.find('table').append(tr);
        }
    });

    page.PageContainer.find('table').on('click', '.btn-fields', function () {
        let _id = $(this).closest('tr').attr('data-id');
        new Page({ 'pageName': 'SetupObjectFieldList', '_id': _id });
    });
}

_setup.PageInitObjectFieldList = function (page) {
    _common.FetchApiByParam({ 'url': '/api/setup/getobjectschemadetail', 'data': { '_id': page.param._id } }, function (result) {
        page.PageContainer.find('.object-name').html(result.data.Name);
        if (result.data.Fields.length > 0) {
            result.data.Fields.sort(function (a, b) {
                let x = a.Name.toLowerCase();
                let y = b.Name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });

            for (let i = 0; i < result.data.Fields.length; i++) {
                let obj = result.data.Fields[i];
                let tr = '<tr data-id="' + obj.Name + '"><td>' + obj.Name + '</td><td>' + obj.DisplayName + '</td><td>' + obj.UIDataType + '</td><td><button class="btn btn-sm btn-primary btn-access">Access</button></td></tr>';
                page.PageContainer.find('table').append(tr);
            }

            page.PageContainer.find('table').on('click', '.btn-access', function () {
                let fieldName = $(this).closest('tr').attr('data-id');
                let formModal = _common.DynamicPopUp({ title: fieldName + ' - Set Field Level Security', body: '' });
                formModal.modal('show');
                formModal.find('.btnSave').remove();

                let table = $('<table class="table table-bordered"><thead><tr><th>Profile</th><th>Access</th><th>Read-Only</th></tr></thead><tbody></tbody></table>');
                _common.FetchApiByParam({ 'url': '/api/setup/getprofilefieldlist', 'data': { 'objectname': result.data.Name } }, function (data) {

                    for (let i = 0; i < data.ProfileList.length; i++) {
                        let profileObj = data.ProfileList[i];
                        let access = '<input type="checkbox" class="chk-access" checked \>';
                        let readonly = '<input type="checkbox" class="chk-readonly" \>';
                        if (data.ProfileObjectList) {
                            let profileDataObj = data.ProfileObjectList.find(x => x.UserProfileID == profileObj._id);
                            if (profileDataObj) {
                                let fldObj = profileDataObj.Fields.find(x => x.Name == fieldName);
                                if (fldObj) {
                                    if (fldObj.Access === false) {
                                        access = '<input type="checkbox" class="chk-access"  \>';
                                    }
                                    if (fldObj.ReadOnly === true) {
                                        readonly = '<input type="checkbox" class="chk-access" checked  \>';
                                    }
                                }
                            }
                        }

                        let tr = '<tr data-id="' + profileObj._id + '"><td>' + profileObj.Name + '</td><td>' + access + '</td><td>' + readonly + '</td></tr>';
                        table.find('tbody').append(tr);
                    }
                    formModal.find('.modal-body').append(table);
                });

            });
        }

    });

}