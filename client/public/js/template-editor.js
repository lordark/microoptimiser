$(function () {
    $(document).on({ mouseleave: function (e) { $("#img-template-edit").remove(); } }, "#img-template-edit");
    $(document).on({ mouseleave: function (e) { $("#img-template-resolution").remove(); } }, "#img-template-resolution");
})
var _templateEditor = {};
_templateEditor.MARKETING_TEMPLATE_SIDEBAR_ICON = '<div class="block-row-template-edit form-control form-control-sm h-auto p-1">\
<i class="fa fa-arrows-alt btn btn-sm btn-secondary" title="Move Snippet"></i>\
<i class="fa fa-code btn btn-sm btn-secondary mt-1" title="Edit Html Code"></i>\
<i class="fa fa-clone btn btn-sm btn-secondary mt-1" title="Duplicate Snippet"></i>\
<i class="fa fa-trash-alt btn btn-sm btn-danger mt-1" title="Delete Snippet"></i></div>';

_templateEditor.MARKETING_TEMPLATE_SIDEBAR_ICON_ONLY_MOVE = '<div class="block-row-template-edit form-control form-control-sm h-auto p-1">\
<i class="fa fa-arrows-alt btn btn-sm btn-secondary" title="Move Snippet">\
</i><i class="fa fa-location-arrow btn btn-sm btn-secondary mt-1" title="Select Section"></i></div>';

_templateEditor.PREVIEW_EMAIL = `<div id="divMailPreview" style="position:fixed;z-index:9999;left:0;right:0;top:0;bottom:0;background:#fff;display: none;">
<div class="bg-white p-2 shadow fixed-top text-center" style="min-height: 2.9rem;">
    <div class="rd-tool" style="display:inline-block;margin:auto;">
        <label class="btn btn-outline-primary btn-sm mb-0 active">
            <input type="radio" class="rdDesktop" name="EmailPreview"
                style="visibility:hidden; position:absolute;">
            <i class="fa fa-desktop"></i> Desktop
        </label>
        <label class="btn btn-outline-primary btn-sm mb-0">
            <input type="radio" class="rdMobile" name="EmailPreview"
                style="visibility:hidden; position:absolute;">
            <i class="fa fa-mobile"></i> Mobile
        </label>
        <label class="btn btn-outline-primary btn-sm mb-0">
            <input type="radio" class="rdHtml" name="EmailPreview"
                style="visibility:hidden; position:absolute;">
            <i class="fa fa-code"></i> Html
        </label>
        <label class="btn btn-outline-primary btn-sm mb-0">
            <input type="radio" class="rdSendMail" name="SendMail"
                style="visibility:hidden; position:absolute;">
            <i class="fa fa-envelope"></i> Send Test Mail
        </label>               
        <button class="btnPrint btn btn-sm btn-secondary"><i class="fa fa-print" aria-hidden="true"></i>
            Print</button>
    </div><span class="btnClose btn btn-danger btn-sm float-right"><i class="fa fa-times"></i> Close</span>
</div>
<div class="d-flex bg-light mt-5 h-100">
    <textarea class="txtHtml" style="width:80%;margin-left: auto;margin-right: auto;"></textarea>
    <iframe name="webView" class="webView border-0"
        seamless
        style="margin:0 auto;width:100%; border:solid 0px #c0c0c0" />
        <div class="divTestMail p-3 bg-white" style="width:600px; margin: 0 auto;">
            <div class="row">
                <div class="form-group col-sm-6 vld-parent "><label class="small">Email From</label>
                    <div class="ctrl-container"><select title="Email From"
                            class="ddlMailFrom form-control form-control-sm msp-ctrl-dropdown">

                        </select></div>
                </div>
                <div class="form-group col-sm-6 vld-parent "><label class="small">Email To</label><input type="text"
                        title="Mail To" class="txtMailTo form-control form-control-sm msp-ctrl-text"></div>
                <div class="form-group col-sm-12 vld-parent "><label class="small">Subject</label><input type="text"
                        title="Mail To" class="txtSubject form-control form-control-sm msp-ctrl-text"></div>
                       <div class="form-group col-sm-12"><button class="btn btn-primary btnSend" >Send <i class="fas fa-envelope"></i></button></div> 

            </div>
        </div>
    
</div>
</div>`;

_templateEditor.MAP_HTML = '<div class="form-group col-sm-12 p-0">\
  <input type="text" class="txtSearch msp-ctrl form-control form-control-sm msp-ctrl-text"  placeholder="Search Box"/>\
  <div style="width:100%;height:360px; position:relative" class="map"></div></div>';

_templateEditor.BindColorPicker = function (param) {
    param.el.ColorPicker({
        onShow: function (colpkr) {
            if ($(colpkr).find('.colorpicker_blank').length == 0) {
                let blankBtn = $('<div class="colorpicker_blank"><i class="fa fa-ban"></i></div>')
                blankBtn.insertBefore($(colpkr).find('.colorpicker_submit'));
                blankBtn.click(function () {
                    param.onChange('transparent');
                    param.el.css('background-color', 'transparent')
                })
            }
            $(colpkr).fadeIn(500);
            return false;
        },
        onHide: function (colpkr) {
            $(colpkr).fadeOut(500);
            return false;
        },
        onChange: function (hsb, hex, rgb) {
            if (this.is(":visible")) {
                param.onChange('#' + hex);
                param.el.css('background-color', '#' + hex)
            }
        },
        onSubmit: function (hsb, hex, rgb, el) {
            param.onChange('#' + hex);
            param.el.css('background-color', '#' + hex);
            $(el).ColorPickerHide();
        }
    });
}
_templateEditor.RenderEmailHTML = function (container) {
    let tmpVar = $('<div>' + container.find('body').html() + '</div>');
    let align = container.find('.marketing-editing-block').attr('align');
    let twidth = container.find('.marketing-editing-block').attr('width');
    if (align == null) { align = 'center' }
    let fontFamily = container.find('.marketing-editing-block').css('font-family');
    let fontSize = container.find('.marketing-editing-block').css('font-size');
    let fontColor = container.find('.marketing-editing-block').css('color');
    let lineHeight = container.find('.marketing-editing-block')[0].style.lineHeight;
    tmpVar.find('.block-col').each(function () {
        let pBlock = $(this).closest('.block-item');
        let pfontFamily = fontFamily;
        let pfontSize = fontSize;
        let pfontColor = fontColor;
        let plineHeight = lineHeight;

        if (pBlock.css('font-family')) {
            pfontFamily = pBlock.css('font-family');
        }
        if (pBlock.css('font-size')) {
            pfontSize = pBlock.css('font-size');
        }
        if (pBlock.css('color')) {
            pfontColor = pBlock.css('color');
        }
        if (pBlock.css('line-height')) {
            plineHeight = pBlock.css('line-height');
        }

        if (!$(this).css('font-family')) {
            $(this).css('font-family', pfontFamily);
        }
        if (!$(this).css('font-size')) {
            $(this).css('font-size', pfontSize);
        }
        if (!$(this).css('color')) {
            $(this).css('color', pfontColor);
        }
        if (!$(this).css('line-height')) {
            $(this).css('line-height', plineHeight);
        }
    });

    tmpVar.find('p,h1,h2,h3,h4,h5,h6').each(function () {
        if (($(this).css('font-family') == undefined) || ($(this).css('font-family') == "")) {
            $(this).css({ "font-family": $(this).closest('.block-col').css('font-family') });
        }
        if (($(this).css('font-size') == undefined) || ($(this).css('font-size') == "")) {

            if ($(this)[0].nodeName == 'P') {
                $(this).css({ "font-size": $(this).closest('.block-col').css('font-size') });
            }
            if ($(this)[0].nodeName == 'H1') {
                $(this).css({ "font-size": '30px', 'margin-top': '0px', 'margin-bottom': '0px' });
            }
            else if ($(this)[0].nodeName == 'H2') {
                $(this).css({ "font-size": '24', 'margin-top': '0px', 'margin-bottom': '0px' });
            }
            if ($(this)[0].nodeName == 'H3') {
                $(this).css({ "font-size": '18', 'margin-top': '0px', 'margin-bottom': '0px' });
            }
            if ($(this)[0].nodeName == 'H4') {
                $(this).css({ "font-size": '16', 'margin-top': '0px', 'margin-bottom': '0px' });
            }
            if ($(this)[0].nodeName == 'H5') {
                $(this).css({ "font-size": '12', 'margin-top': '0px', 'margin-bottom': '0px' });
            }
            if ($(this)[0].nodeName == 'H6') {
                $(this).css({ "font-size": '10', 'margin-top': '0px', 'margin-bottom': '0px' });
            }
        }
    });

    tmpVar.find('.block-row-template-edit ').remove();
    tmpVar.find('.block-item.remove').remove();
    tmpVar.find('.block-item-edt .edit').each(function () {
        let htm = _templateEditor.GetFinalHtml($(this));
        $(this).closest('.block-col').html(htm);
    });
    let tmpHTMVar = $('<div><table cellspacing="0" cellpadding="0" width="' + twidth + '" align="center"></table></div>');
    let tmpTable = tmpHTMVar.find('table');
    tmpVar.find('.block-item-edt').each(function () {
        let tdStyle = $(this).parent().attr('style');
        if (tdStyle == undefined)
            tdStyle = $(this).attr('style');
        tmpTable.append('<tr align="center"><td width="' + twidth + '" style="' + tdStyle + '">' + $(this).html() + "</td></tr>");
    });
    tmpVar = tmpHTMVar;
    tmpVar.find('table').each(function () {
        $(this).attr('cellspacing', '0').attr('cellpadding', '0').attr('border-collapse', 'collapse');
    });
    tmpVar.find('.tmp-btn-txt').each(function () {
        let btn = $(this).parent();
        btn.html($(this).val());
    });

    tmpVar.find('p,h1,h2,h3,h4,h5,h6').each(function () {
        let el = $(this);
        let style = el.attr('style');
        if (style == undefined) {
            style = '';
        }
        el.getCssValue = function (style, rp) {
            var val = el.css(style);
            if (val == '') {
                val = rp;
            }
            return val;
        }
        if (style.indexOf('padding-left') == -1) {

            style += 'padding-left:' + el.getCssValue('padding-left', 0) + ';';
        }
        if (style.indexOf('padding-right') == -1) {
            style += 'padding-right:' + el.getCssValue('padding-right', 0) + ';';
        }
        if (style.indexOf('padding-top') == -1) {
            style += 'padding-top:' + el.getCssValue('padding-top', 0) + ';';
        }
        if (style.indexOf('padding-bottom') == -1) {
            style += 'padding-bottom:' + el.getCssValue('padding-bottom', 0) + ';';
        }
        if (style.indexOf('margin-right') == -1) {
            style += 'margin-right:' + el.getCssValue('margin-right', 0) + ';';
        }
        if (style.indexOf('margin-left') == -1) {
            style += 'margin-left:' + el.getCssValue('margin-left', 0) + ';';
        }
        if (style.indexOf('margin-top') == -1) {
            style += 'margin-top:' + el.getCssValue('margin-top', '10px') + ';';
        }
        if (style.indexOf('margin-bottom') == -1) {
            style += 'margin-bottom:' + el.getCssValue('margin-bottom', '10px') + ';';
        }
        el.attr('style', style);
    });
    tmpVar.find('a').each(function () {
        if (!$(this).css('color')) {
            let color = $(this).closest('span').css('color');
            if (color) {
                $(this).css('color', color);
            }
        }
    })
    width = '';
    divStart = '';
    divEnd = '';
    let stylePageBody = (container.find('body').attr('style') == undefined) ? "" : container.find('body').attr('style');
    stylePageBody += ';vertical-align:top;'
    let fhtml = $('<div><table cellspacing="0" cellpadding="0" width="100%" height="100%" align="center"><tr align="center"><td  class="template-page-body">' + '<table 	style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" 	align="' + align + '" class="template-mail-body" cellspacing="0" cellpadding="0" width="' + twidth + '"><tr><td class="main-template-container" align="center" width="' + twidth + '">' + tmpVar.html() + '</td></tr></table>' + '</td></tr></table></div>');
    fhtml.find('.main-template-container')[0].style.cssText = container.find('.marketing-editing-block')[0].style.cssText;
    fhtml.find('table [contenteditable="true"]').attr('contenteditable', false);
    let bWidth = container.find('.marketing-editing-block').attr('width');
    let sbWebPage = '';
    sbWebPage += "<!DOCTYPE html><html><head><title></title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
    sbWebPage += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
    sbWebPage += "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />";
    sbWebPage += "<style type=\"text/css\">";
    sbWebPage += "body{margin:0}";
    sbWebPage += ".template-page-body{vertical-align:top;}";
    sbWebPage += '@media only screen and (max-width: 480px) { [width="' + bWidth + '"] {width:100% !important}';
    sbWebPage += ".template-page-body{width:100% !important;vertical-align:top;}";
    sbWebPage += ".template-page-body > center{width:100% !important}";
    sbWebPage += ".template-mail-body{width:100% !important}";
    sbWebPage += ".td-col{display:block !important;width:100% !important;box-sizing: border-box;}";
    sbWebPage += ".block-col{display:block !important;width:100% !important;box-sizing: border-box;}";
    sbWebPage += ".youtube-block,.img{height:auto !important;width:100% !important;box-sizing: border-box !important}";
    sbWebPage += "}";
    sbWebPage += "</style></head>";
    return sbWebPage + '<body style="' + stylePageBody + '">' + fhtml.html() + '</body>';
}
_templateEditor.ShowPreview = function (container) {
    let block = _templateEditor.RenderEmailHTML(container);
    let height = $(window).height() - 50;
    let modal =$( _templateEditor.PREVIEW_EMAIL);   
    modal.appendTo($('body'));
    modal.show();
    let iframe = modal.find('.webView')[0];
    $(iframe).show().css('width', '100% ', 'important').height(height);
    modal.find('.txtHtml').hide();
    modal.find('.divTestMail').hide();
    let listMails = _initData.ListSchema.find(x => x.Name == "MarketingRegisteredEmail").Data;
    let stMailOptions = '<option value="0" selected>Select</option>';
    for (let i = 0; i < listMails.length; i++) {
        stMailOptions += '<option value="' + listMails[i].Key + '">' + listMails[i].Value + '</option>'
    }
    modal.find('.ddlMailFrom').append(stMailOptions);
    modal.find('.btnClose').click(function () {
        modal.remove();
    });
    modal.find('.btnPrint').click(function () {
        window.frames["webView"].focus();
        window.frames["webView"].print();
    })
    modal.find('.rd-tool input').click(function () {
        modal.find('.rd-tool label').removeClass('active');
        if ($(this).attr('class') == "rdSendMail") {
            $(this).parent().addClass('active');
            modal.find('.CodeMirror').hide();
            modal.find('.webView').hide();
            modal.find('.divTestMail').show();
        }
        else if ($(this).attr('class') == "rdMobile") {
            $(this).parent().addClass('active');
            modal.find('.CodeMirror').hide();
            modal.find('.divTestMail').hide();
            modal.find('.webView').show();
            modal.find('.webView').width(320);
            modal.find('.webView').height(480);
            modal.find('.webView').parent().addClass('mobile-view');
            modal.find('.txtHtml').hide();
            _templateEditor.SetIframe(modal.find('.webView')[0], block);
        } else if ($(this).attr('class') == "rdDesktop") {
            $(this).parent().addClass('active');
            modal.find('.CodeMirror').hide();
            modal.find('.divTestMail').hide();
            modal.find('.webView').show().width('100%', 'important');
            modal.find('.webView').height(height);
            modal.find('.webView').parent().removeClass('mobile-view');
            modal.find('.txtHtml').hide();
            _templateEditor.SetIframe(modal.find('.webView')[0], block);
        } else {
            $(this).parent().addClass('active');
            modal.find('.txtHtml').css('height', height);
            modal.find('.webView').hide();
            modal.find('.divTestMail').hide();
            modal.find('.webView').parent().removeClass('mobile-view');
            modal.find('.txtHtml').text(block);
            if (modal.find('.CodeMirror').length > 0) {
                modal.find('.CodeMirror').show();
            } else {
                modal.find('.txtHtml').show();
                let editor = CodeMirror.fromTextArea(modal.find('.txtHtml')[0], {
                    lineNumbers: true,
                    mode: "text/html",
                    gutters: ["CodeMirror-lint-markers"],
                    lint: true
                });
            }
        }
    });
    modal.find('.btnSend').click(function () {
        let data = {};
        if (modal.find(".ddlMailFrom").val() == "0") {
            swal("Please select Email From", { icon: "info" });
            return;
        }
        if (modal.find(".txtMailTo").val() == "0") {
            swal("Please enter Email To", { icon: "info" });
            return;
        }
        if (!_common.isValidCommaSepratedEmailAddress(modal.find(".txtMailTo").val())) {
            swal('Please enter  valid Email To', { icon: "info" });
            return false;
        }
        if ($.trim(modal.find(".txtSubject").val()) == "") {
            swal("Please enter Subject", { icon: "info" });
            return;
        }
        data.SenderEmail = modal.find(".ddlMailFrom").val();
        data.Subject = modal.find(".txtSubject").val();
        data.TemplateEmailContent = block;
        data.mailID = modal.find(".txtMailTo").val();
        let param = { 'url': '/api/marketing/SendTestMail', 'type': 'post', 'contentType': 'application/json' };
        param.data = data;
        _common.FetchApiByParam(param, function (res) {
            swal(res.msg, { icon: "info" });
        });
    })

    modal.find('.rdDesktop').prop('checked', 'checked').click();
}

_templateEditor.SetIframe = function (iframe, content) {
    iframe = (iframe.contentWindow) ? iframe.contentWindow : (iframe.contentDocument.document) ? iframe.contentDocument.document : iframe.contentDocument;
    iframe.document.open();
    iframe.document.write('<html>');
    iframe.document.write('<head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head>');
    iframe.document.write(content);
    iframe.document.write('</html>');
    iframe.document.close();
    $(iframe.document).find('body').on("click", "a", function (e) {
        if ($(this).hasClass("unsubscribe-link")) {
            e.preventDefault();
            return
        }
        if ($(this).attr('href').includes("http")) {
            e.preventDefault();
            window.open($(this).attr('href'), "_blank");
        }
        iframe.document.bindevent = true;
    });

}

_templateEditor.GetFinalHtml = function (el) {
    if (el.find('.edit').length > 0) {
        return _templateEditor.GetFinalHtml(el.find('.edit').first());
    } else {
        return el.html();
    }
}

function TemplateEditor(param, campaignData) {
    let self = this;
    this.Zoom = 100;
    this.Content = param.content;
    this.param = param;
    this.undostack = new Array();
    this.redostack = new Array();
    let DynamicField;
    this.undostack.push = function () {
        if (this.length >= 200) {
            this.shift();
        }
        return Array.prototype.push.apply(this, arguments);
    }
    this.redostack.push = function () {
        if (this.length >= 200) {
            this.shift();
        }
        return Array.prototype.push.apply(this, arguments);
    }
    this.beforechange = function () {
        let c = {};
        let tmp = $('<div>' + self.EditBlock.find('.marketing-editing-block')[0].outerHTML + '</div>');
        tmp.find('.ui-droppable').remove();
        tmp.find('.ui-draggable-dragging').remove();
        tmp.find('#img-template-edit').remove();
        tmp.find('#img-template-resolution').remove();
        c.htm = tmp.html();
        c.style = self.EditBlock.find('body').attr('style')
        if (self.undostack.length > 0) {
            let p = self.undostack[self.undostack.length - 1];
            if (p) {
                if (p.htm == c.htm && p.style == c.style) {
                    return;
                }
            }
            self.undostack.push(c);
        } else {
            self.undostack.push(c);
        }
    }
    this.InitTemplate = function () {
        $('.colorpicker').remove();
        this.Container.on("click", ".nav-item a", function () {
            if ($(this).hasClass("dropdown-toggle")) { return; }
            else {
                let target = $(this).find('i[data-target]').attr('data-target');
                $(this).closest('.nav').find('.nav-link').removeClass('active');
                $(this).addClass('active');
                self.Container.find('.block').removeClass('active');
                self.Container.find('.block[name="' + target + '"]').addClass('active');
            }
        });
        this.Container.find('.nav .nav-link').each(function () {
            let attrname = $(this).find('[data-target]').attr('data-target');
            if (attrname != undefined) {
                let url = '/page/marketing/' + attrname + ".html";
                _common.GetStaticPage(url, function (pageHTML) {
                    let HtmlContent = pageHTML.replace(new RegExp('##URL##', 'g'), window.location.origin);
                    let blocks = $('<div></div>').html(HtmlContent);
                    let blockItems = self.Container.find('.block[name="' + attrname + '"]');
                    if (blocks.find('#DynamicFieldsBlock').length == 0) {
                        blocks.find('.block-item').each(function () {
                            let className = $(this).attr('class');//.split(/\s+/)[1];
                            let item = $('<div class="block-item ' + className + ' ui-draggable">' + $(this).html() + '</div>');
                            item[0].content = $(this).html();
                            blockItems.append(item);
                            self.BindDragEvent(blockItems);
                        });
                    } else {
                        let table = blocks.find('#DynamicFieldsBlock');
                        if (campaignData && campaignData.DynamicField) {
                            let dynamicFields = campaignData.DynamicField;
                            for (let i=0; i<dynamicFields.length; i++) {
                                if (dynamicFields[i].key) {
                                    let tr = '<tr>';
                                    tr += '<td style="padding:5px;" class="cellDisp" index="' + i + '"><a class="dropdown-item status-dropdown" href="#">' + '##' + dynamicFields[i].key + '##'  + '</a></td>';
                                    tr += '</tr>';
                                    table.find('tbody').append(tr);
                                }
                            }
                            blockItems.append(table);
                            blockItems.on('mousedown', 'a', function (e) {
                                if (self.cursorPosition) {
                                    let insertText = ' ' + $(this).text() + ' ';
                                    self.EditBlock[0].execCommand('insertText', false, insertText);
                                } else {
                                    swal('Please put cursor at some position', {icon: 'info'});
                                }
                            });
                        }
                    }
                })
            }
        });
        this.Container.find('.btn-zoom').css('display', 'none');
        this.Container.find('.btn-zoom').on('click', function () {
            let block = self.EditBlock.find('.marketing-editing-block')
            if ($(this).hasClass('plus') && self.Zoom < 150)
                self.Zoom += 5;
            else if ($(this).hasClass('minus') && self.Zoom > 25)
                self.Zoom -= 5;
            else if ($(this).hasClass('reset'))
                self.Zoom = 100;

            block.css('zoom', self.Zoom + '%');
            self.Container.find('.marketing-reset').html('Reset(' + self.Zoom + '%)');
        });
        //-------------------Editor Events start-----------------------------//
        this.Container.find('.marketing-css-block .ddlValign').change(function () {
            self.beforechange();
            var el = self.GetSelectedElement();
            el.attr('valign', $(this).val());
        });
        this.Container.find('.marketing-css-block .ddlLineHeight').on('change', function () {
            self.beforechange();
            var el = self.GetSelectedElement();
            el.css('line-height', $(this).val());
        });

        this.Container.find('.marketing-css-block').find('.ddlBorderWidth,.ddlBorderStyle').change(function () {
            self.beforechange();
            let element = self.GetSelectedElement();
            let color = self.Container.find('.lblBorderColor').attr('data-color');
            let width = self.Container.find('.ddlBorderWidth').val() + 'px';
            let style = self.Container.find('.ddlBorderStyle').val();
            if (element.hasClass('template-section-block')) {
                element = element.find('td').first();
            }
            element.css({
                "border-color": "#" + color,
                "border-width": width,
                "border-style": style
            });
        });
        this.Container.find('.marketing-css-block').find('.padding-area input').change(function () {
            self.beforechange();
            let paddingBlock = self.GetSelectedElement();
            if ($(this).hasClass('lft-box')) {
                paddingBlock.css({ 'padding-left': $(this).val() + 'px' });
            }
            if ($(this).hasClass('rgt-box')) {
                paddingBlock.css({ 'padding-right': $(this).val() + 'px' });
            }
            if ($(this).hasClass('top-box')) {
                paddingBlock.css({ 'padding-top': $(this).val() + 'px' });
            }
            if ($(this).hasClass('bot-box')) {
                paddingBlock.css({ 'padding-bottom': $(this).val() + 'px' });
            }
        });
        this.Container.find('.btn-list').on('click', function () {
            self.beforechange();
            let eventValue = $(this).attr('value');
            self.Container.find('.marketing-css-block .btn-list').removeClass('btn-info');
            $(this).addClass('btn-info');
            self.EditBlock[0].execCommand(eventValue, false, null);
        });
        this.Container.find('.btn-align').on('click', function () {
            self.beforechange();
            let eventValue = $(this).attr('value');
            self.Container.find('.marketing-css-block .btn-align').removeClass('btn-info');
            $(this).addClass('btn-info');
            var el = self.GetSelectedElement();
            if (el.isBlock) {
                if (self.CheckSelection()) {
                    self.EditBlock[0].execCommand(eventValue, false, null);
                    return;
                }
            }
            let cssVal = $(this).attr('cssval');
            el.css('text-align', cssVal);
        });
        this.Container.find('.marketing-css-block').find('.btn-style').click(function () {
            self.beforechange();
            let eventValue = $(this).attr('value');
            $(this).addClass('btn-info');
            var el = self.GetSelectedElement();
            if (el.isBlock) {
                if (self.CheckSelection()) {
                    self.EditBlock[0].execCommand(eventValue, false, null);
                    return;
                }
            }
            // switch(eventValue) {
            //     case 'bold':
            //         el.css('font-weight')=='bold'? el.css('font-weight','normal'): el.css('font-weight','bold')
            //         break;
            //     case 'italic':
            //         el.css('font-style')=='italic'? el.css('font-style','normal'): el.css('font-style','italic')
            //         break;
            //     case 'underline':
            //          el.css('text-decoration')=='underline'? el.css('text-decoration','none'): el.css('text-decoration','underline')
            //         break;
            // }
        });

        this.Container.find('.fa-undo').click(function () {
            if (self.undostack.length > 0) {
                let c = self.undostack.pop();
                self.redostack.push(c);
                let body = self.EditBlock.find('body');
                body.attr('style', c.style);
                body.find('.marketing-editing-block').replaceWith(c.htm);
                self.EditBlock.find('.marketing-editing-block').sortable({
                    revert: true,
                    handle: ".fa-arrows-alt",
                    axis: "y",
                    scroll: true
                });
            }
        })
        this.Container.find('.fa-redo').click(function () {
            if (self.redostack.length > 0) {
                let c = self.redostack.pop();
                let body = self.EditBlock.find('body');
                body.html(c.htm).attr('style', c.style);
                self.EditBlock.find('.ui-droppable').remove();
                self.EditBlock.find('.ui-draggable-dragging').remove();
                self.EditBlock.find('#img-template-edit').remove();
                self.EditBlock.find('#img-template-resolution').remove();
                self.EditBlock.find('.marketing-editing-block').sortable({
                    revert: true,
                    handle: ".fa-arrows-alt",
                    axis: "y",
                    scroll: true
                });
            }
        });
        this.Container.find('.btn-create-link').on('click', function () {
            self.OpenLinkDialouge();
        });
        this.Container.find('.marketing-css-block').find('.txtSize').change(function () {
            self.beforechange();
            let fontSize = $(this).val();
            var el = self.GetSelectedElement();
            if (el.isBlock) {
                if (self.CheckSelection()) {
                    self.EditBlock[0].execCommand('styleWithCSS', false, false);
                    self.EditBlock[0].execCommand("fontSize", false, "7");
                    self.EditBlock.find('.marketing-editing-block font[size="7"]').removeAttr('size').css('font-size', fontSize + 'px');
                }
                else {
                    el.css("font-size", fontSize + 'px');
                }
            } else {
                el.css("font-size", fontSize + 'px');
            }
        });
        this.Container.find('.marketing-css-block .txtThickness').change(function () {
            self.beforechange();
            let height = $(this).val();
            var el = self.GetSelectedElement();
            el.attr("height", height);
        })

        this.Container.find('.marketing-css-block').find('.ddlFont').change(function () {
            self.beforechange();
            let fontFamily = $(this).val();
            var el = self.GetSelectedElement();
            if (el.isBlock) {
                if (self.CheckSelection()) {
                    self.EditBlock[0].execCommand('styleWithCSS', false, true);
                    self.EditBlock[0].execCommand("fontName", false, fontFamily);
                }
                else {
                    el.css("font-family", fontFamily);
                }
            } else {
                el.css("font-family", fontFamily);
            }

        });

        this.Container.find('.marketing-css-block').find('.ddlEmailAlign').change(function () {
            self.beforechange();
            let margin = { left: '0', center: '0 auto', right: '0 0 0 auto' }
            self.EditBlock.find('.marketing-editing-block').css('margin', margin[$(this).val()]).attr('align', $(this).val());

        });
        this.Container.find('.marketing-css-block').find('.ddlEmailWidthType,.txtEmailWidth').change(function () {
            self.beforechange();
            let block = $(this).parent();
            if (block.find('.ddlEmailWidthType').val() == 'px') {
                block.find('.txtEmailWidth').attr('max', '1024');
                block.find('.txtEmailWidth').attr('min', '550');
                if (block.find('.txtEmailWidth').val() < 550) {
                    block.find('.txtEmailWidth').val(600)
                }
            } else {
                block.find('.txtEmailWidth').attr('max', '100');
                block.find('.txtEmailWidth').attr('min', '50');
                if (block.find('.txtEmailWidth').val() > 100) {
                    block.find('.txtEmailWidth').val(100)
                }
            }
            let width = block.find('.txtEmailWidth').val() + block.find('.ddlEmailWidthType').val();
            self.EditBlock.find('.marketing-editing-block').css('width', width).attr('width', width);
        });

        //To show the suggestion from mailing list when @ in typed
        //CampaignData is empty when create template and exist when scheduling
        if (campaignData) {
            _campaign.LoadDynamicField(campaignData.data.ContactListID.$oid).then(data => {
                DynamicField = data;
                self.EditBlock.find('.marketing-editing-block').on('keyup', '.block-col', function (e) {
                    if (campaignData && DynamicField != undefined)
                        editor.EditorKeyUp(this, e, DynamicField);
                })
                self.EditBlock.find('.marketing-editing-block').on('keydown', '.block-col', function (e) {
                    if (campaignData && DynamicField != undefined)
                        editor.EditorKeyDown(this, e, DynamicField);
                })
            });
        }
        //-------------------Editor Events end-----------------------------//

        _templateEditor.BindColorPicker({
            el: this.Container.find('.marketing-css-block').find('.lblBackColor'),
            onChange: function (color) {
                self.beforechange();
                var el = self.GetSelectedElement();
                if (el.isBlock) {
                    if (self.CheckSelection()) {
                        self.EditBlock[0].execCommand('styleWithCSS', false, true);
                        self.EditBlock[0].execCommand("BackColor", false, color);
                    }
                    else {
                        el.css("background-color", color);
                    }
                } else {
                    el.css("background-color", color);
                }
            }
        });

        _templateEditor.BindColorPicker({
            el: this.Container.find('.marketing-css-block').find('.lblForeColor'),
            onChange: function (color) {
                self.beforechange();
                var el = self.GetSelectedElement();
                if (el.isBlock) {
                    if (self.CheckSelection()) {
                        self.EditBlock[0].execCommand('styleWithCSS', false, true);
                        self.EditBlock[0].execCommand("foreColor", false, color);
                    }
                    else {
                        el.css("color", color);
                    }
                } else {
                    el.css("color", color);
                }
            }
        });

        _templateEditor.BindColorPicker({
            el: this.Container.find('.marketing-css-block').find('.lblBorderColor'),
            onChange: function (color) {
                self.beforechange();
                var el = self.GetSelectedElement();
                _templateEditor.LeftBlockContent.find('.lblBorderColor').attr('data-color', color);
                let width = _templateEditor.LeftBlockContent.find('.ddlBorderWidth').val() + 'px';
                let style = _templateEditor.LeftBlockContent.find('.ddlBorderStyle').val();
                if (el.hasClass('template-section-block')) {
                    el = element.find('td').first();
                }
                el.css({
                    "border-color": color,
                    "border-width": width,
                    "border-style": style
                });
            }
        });

        this.Container.find('.marketing-editing-block').find('.block-row-template-edit').remove();
        this.Container.find('.marketing-editing-block .active').removeClass('active');
        this.Container.find('.marketing-editing-block').find('.db-tool-tip').remove();
        this.Container.find('.marketing-editing-block').attr('style', $(this.Content).attr('style'));
        if (this.param.pageStyle)
            self.EditBlock.find('.marketing-full-edit').attr('style', this.param.pageStyle);

        _templateEditor.LeftBlockContent = this.Container.find('.marketing-css-block');
        this.GetSocialIconSettingPage();
        this.Container.find('.ddlStyleElement').on('change', function () {
            let el;
            if ($(this).val() != 'Section') {
                if ($(this).val() == 'PageBody') {
                    el = self.EditBlock.find('body');
                } else {
                    el = self.EditBlock.find('.marketing-editing-block');
                }
                self.ReadElementStyle(el);
            } else {
                el = self.EditBlock.find('.marketing-editing-block .block-item:first').find('td').first();
                if (el.length > 0)
                    self.ReadElementStyle(el);
            }
            self.Container.find('.marketing-css-block').attr('data-block', $(this).val());
        });
        this.Container.find('.chkUnderline').click(function () {
            self.EditBlock[0].execCommand("unlink", false, false);
        });
        this.Container.find('.marketing-editing-block-container .btn-preview').on('click', function () {
            $("#img-template-edit").remove();
            _templateEditor.ShowPreview(self.EditBlock);
        });
        if (self.param && self.param.templateId != 'ManualHTMLCode')
            this.BindImageEvents();
        if (this.param.topButtons) {
            for (let i = 0; i < this.param.topButtons.length; i++) {
                let obj = this.param.topButtons[i];
                let button = $(obj.html);
                this.Container.find('.custom-buttons').append(button);
                button.click(function () {
                    self.GetLatestContent();
                    obj.onClick(this, self);
                });
            }
        }
        this.Container.find('.marketing-editor-body').css('zoom', '100%', 'important');
        self.EditBlock.on('click', 'a', function (e) {
            e.preventDefault();
        })
        _userprofile.Auth({ pageData: { IsPermissionRequired: true, ObjectName: "MarketingCampaign" }, container: this.Container })
    }

    this.GetLatestContent = function () {
        this.Content = self.EditBlock.find('body')[0].outerHTML;
        this.PageStyle = self.EditBlock.attr('style');
        this.TemplateID = this.param.templateId;
    }

    this.BindImageEvents = function () {
        self.EditBlock.on({
            mouseenter: function () {
                if ($(this).hasClass('sicon') && $(this).attr('data-name') != 'Custom') {
                    if (!this.onImageChange) {
                        this.onImageChange = function (obj) {
                            $(obj).attr('data-name', 'Custom');
                        }
                    }
                }
                let width = $(this).width();
                let height = $(this).height();
                let imgTEdit = self.EditBlock.find('#img-template-edit');
                let imgTR = self.EditBlock.find('#img-template-resolution');
                if (imgTEdit.length == 0) {
                    self.EditBlock.find('body').append('<div id="img-template-edit"></div>');
                    imgTEdit = self.EditBlock.find('#img-template-edit');
                    imgTEdit.on("click", ".fa,.fab,.fas", function (e) {
                        e.stopPropagation();
                        if ($(this).hasClass('fa-share-alt-square')) {
                            self.SocialIconSetting();
                        }
                        else if ($(this).hasClass('fa-link')) {
                            self.ButtonSetting();
                        }
                        else if ($(this).hasClass('fa-map-marker')) {
                            self.MapSetting();
                        }
                        else if ($(this).hasClass('fa-youtube')) {
                            self.YouTubeSetting();
                        }
                        else if ($(this).hasClass('fa-camera')) {
                            self.ImageUpload();
                        }
                        else {
                            self.ImageSetting();
                        }
                    });
                }
                if ($(this).hasClass('social-icons')) {
                    imgTEdit.html('<i class="fa fa-share-alt-square btn btn-sm btn-primary" title="Edit Social Icon Settings"></i>');
                }
                else if ($(this).hasClass('template-button')) {
                    imgTEdit.html('<i class="fa fa-link" title="Edit button settings"></i>');
                }
                else if ($(this).hasClass('map-block')) {
                    imgTEdit.html('<i class="fas fa-map-marker btn btn-primary btn-sm" title="Edit map"></i>');
                }
                else if ($(this).hasClass('youtube-block')) {
                    imgTEdit.html('<i class="fab fa-youtube btn btn-sm btn-primary" title="Import URL"></i><i class="fa fa-camera btn btn-sm btn-primary mt-1" title="Add an Image"></i><i class="fa fa-cog btn btn-sm btn-primary mt-1" title="Image Settings"></i>');
                } else {
                    if (imgTR.length == 0) {
                        self.EditBlock.find('body').append('<div id="img-template-resolution"></div>');
                        imgTR = self.EditBlock.find('#img-template-resolution');
                    }
                    imgTEdit.html('<i class="fa fa-camera btn btn-sm btn-primary" title="Add an Image"></i><i class="fa fa-cog btn btn-sm btn-primary mt-1" title="Image Settings"></i>');
                    imgTR.html('<div class="p-2 bg-secondary rounded"><span title="Image Size">Image Size:<span><br/>' + width + 'x' + height + '</div>');
                }

                imgTEdit.css('left', $(this).offset().left);
                imgTEdit.css('top', $(this).offset().top);
                imgTEdit[0].el = $(this);
                imgTR.css('left', $(this).offset().left + $(this).width() - 84 - $(this).closest('td').css('padding-right').replace('px', ''));
                let ofsTop = 0;
                if (width < 120) {
                    ofsTop = 55;
                    imgTR.addClass('ttp');
                } else {
                    imgTR.removeClass('ttp');
                }
                imgTR.css('top', $(this).offset().top - ofsTop);
            },
            mouseleave: function (e) {
                if ($(this).hasClass('sicon')) {
                    return;
                }
                let imgTEdit = self.EditBlock.find('#img-template-edit');
                if (imgTEdit.length > 0) {
                    let left = $(this).offset().left;
                    let top = $(this).offset().top;
                    let ht = $(this).height();
                    let wt = $(this).width();
                    if (e.pageX < left + wt && e.pageX > left && e.pageY < top + ht && e.pageY > top) {
                    } else {
                        let imgTR = self.EditBlock.find('#img-template-resolution');
                        if (imgTR.length > 0) {
                            let left = imgTR.position().left;
                            let top = imgTR.position().top;
                            let ht = imgTR.height() + 9;
                            let wt = imgTR.width() + 9;
                            if (e.pageX < left + wt && e.pageX > left && e.pageY < top + ht && e.pageY > top) {
                            }
                            else {
                                imgTR.remove();
                                imgTEdit.remove();
                            }
                        }
                    }
                }
            }
        }, "img,.template-button,td.social-icons");
    }

    this.GetSocialIconSettingPage = function () {
        $.ajax({
            url: '/page/marketing/SocialIconSetting.html',
            type: "GET",
            cache: false,
            contentType: "text/html",
            success: function (result) {
                self.SocialDiag = '<div>' + $(result).find('#content').html() + '</div>';
            }
        });
    }

    this.GetSocialIconImagePath = function (name, style, type) {
        var src = name;
        if (src != 'Custom') {
            if (style == 'round') {
                src = src + '-ru';
            }
            if (type == 'grayscale') {
                src = src + '-b';
            }
        }
        src = window.location.origin + '/images/marketing-template-image/social-icons/' + src + '.png';
        return src;
    }

    this.YouTubeSetting = function () {
        let block = self.EditBlock.find('#img-template-edit')[0].el;
        self.EditBlock.find('#img-template-edit').remove();
        self.EditBlock.find('#img-template-resolution').remove();
        let body = '<div class="form-group"><label class="small">Enter URL</label><input type="text" class="txtURL form-control form-control-sm" style="max-width:500px;"/></div>';
        let formModal = _common.DynamicPopUp({ title: 'Enter Youtube URL', body: body, 'DistroyOnHide': true });
        formModal.find('.txtURL').val(block.parent().attr('href'));
        formModal.modal('show');
        formModal.find('.btnSave').on('click', function () {
            let url = formModal.find('.txtURL').val();
            if (url === null) {
                alert('Youtube link can not be blank.'); return;
            }
            let vid;
            let results;
            if (url.indexOf('http') != 0) {
                url = 'http://' + url;
            }
            results = url.match("[\?&]v=([^&#]*)");
            if (results === null) {
                alert('Please enter a valid youtube link.'); return;
            }
            vid = (results === null) ? url : results[1];
            block.parent().attr('href', url);
            let imgURL = window.location.origin + '/noauth/api/utility/youtubeimage/' + vid + '.jpg';
            block.attr('src', imgURL);
            formModal.modal('hide');
        });
    }

    this.SocialIconSetting = function () {
        let block = self.EditBlock.find('#img-template-edit')[0].el;
        self.EditBlock.find('#img-template-edit').remove();
        self.EditBlock.find('#img-template-resolution').remove();
        let popup = _common.DynamicPopUp({ title: 'Social Icon Settings', body: self.SocialDiag, 'DistroyOnHide': true });
        let align = block.attr('align');
        let style = block.attr('data-style');
        let type = block.attr('data-type');
        let spacing = block.attr('data-spacing');
        if (align == undefined) {
            align = 'left';
        }
        if (style == undefined) {
            style = 'round';
        }
        if (type == undefined) {
            type = 'colour';
        }
        if (spacing == undefined) {
            spacing = "20";
        }
        popup.find('.ddlAlign').val(align);
        popup.find('.ddlStyle').val(style);
        popup.find('.ddlType').val(type);
        popup.find('.txtSpacing').val(spacing)
        let tbl = popup.find('.tbl-icons')
        block.find('img').each(function () {
            let op = {};
            op.type = $(this).attr('data-name');
            op.url = $(this).parent().attr('href');
            op.imgurl = $(this).attr('src');
            let tr = self.AddSocialItem(op);
            tr[0].imgHeight = $(this).height();
            tr[0].imgWidth = $(this).width();
            tbl.append(tr);
        });
        popup.find('.ddlStyle,.ddlType').change(function () {
            popup.find('.tbl-icons .row').each(function () {
                var newStyle = popup.find('.ddlStyle').val();
                var newType = popup.find('.ddlType').val();
                var iconType = $(this).find('.ddlSType').val();
                var src = self.GetSocialIconImagePath(iconType, newStyle, newType);
                $(this).find('.img').html('<img width="34" src="' + src + '"/>');
            });
        });
        popup.on('change', '.ddlSType', function () {
            var newStyle = popup.find('.ddlStyle').val();
            var newType = popup.find('.ddlType').val();
            var iconType = $(this).val();
            var src = self.GetSocialIconImagePath(iconType, newStyle, newType);
            $(this).closest('.row').find('.img').html('<img height="34" width="34" src="' + src + '"/>');
        });

        popup.find('.btnAdd').on('click', function () {
            let row = self.AddSocialItem();
            tbl.append(row);
        });
        popup.on('click', '.btnDelete', function () {
            if ($(this).closest('.tbl-icons').find('.row').length > 1) {
                $(this).closest('.row').remove();
            } else {
                swal("Cannot remove last item.", { icon: "info", });
            }
        });
        tbl.sortable({
            placeholder: "ui-state-highlight"
        });
        popup.find('.btnSave').on('click', function () {
            let Isvalidate = true;
            popup.find('.tbl-icons .row').each(function () {
                if ($(this).find('.txtURL').val() == "") {
                    alert('Please enter the url.');
                    Isvalidate = false;
                    return false;
                }
                else if (_common.isValidUrl($(this).find('.txtURL').val()) == false) {
                    alert('Please enter a valid url.');
                    Isvalidate = false;
                    return false;
                }
            });
            if (!Isvalidate) return;
            let socialData = {};
            socialData.align = block.attr('align');
            socialData.htm = block.html();
            socialData.dataStyle = block.attr('data-style');
            socialData.dataType = block.attr('data-type');
            socialData.dataSpacing = block.attr('data-spacing');
            block.attr('align', popup.find('.ddlAlign').val());
            block.html('');
            let newStyle = popup.find('.ddlStyle').val();
            let newType = popup.find('.ddlType').val();
            let newSpacing = popup.find('.txtSpacing').val();
            let tdfill = parseInt(parseInt(newSpacing) / 2);
            block.attr('data-style', newStyle);
            block.attr('data-type', newType);
            block.attr('data-spacing', newSpacing);
            let tmplt = '<td width="' + tdfill + '"></td>';
            tmplt += '<td align="center" style="padding-top: 10px; padding-bottom: 10px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">'
            tmplt += '##icon##</td>';
            tmplt += '<td width="' + tdfill + '"></td>';
            let tbl = $('<table cellspacing="0" cellpadding="0"><tr></tr></table>');
            tbl.attr('align', popup.find('.ddlAlign').val());
            let tr = tbl.find('tr');
            popup.find('.tbl-icons .row').each(function () {
                let newName = $(this).find('.ddlSType').val();
                let newSrc = $(this).find('img').attr('src');
                let imgHeight = 34;
                let imgWidth = 34;
                if ($(this)[0].imgHeight) {
                    imgHeight = $(this)[0].imgHeight;
                }
                if ($(this)[0].imgWidth) {
                    imgHeight = $(this)[0].imgWidth;
                }
                let newIcon = '<a href="' + $(this).find('.txtURL').val() + '" target="_blank"><img height="' + imgHeight + '"  width="' + imgWidth + '" data-name="' + newName + '" class="sicon" src="' + newSrc + '" /></a>';
                let tblnew = tmplt.replace('##icon##', newIcon);
                tr.append(tblnew);
            });
            block.append(tbl);
            popup.modal('hide');
        });
        popup.modal('show');
    }

    this.AddSocialItem = function (op) {
        let tr = `  <div class="row mb-2">
        <div class="col-auto pr-0">
            <div class="btn btn-sm">
                <i class="fas fa-grip-horizontal"></i>
            </div>
        </div>
        <div class="col-auto pr-0 img">
           
        </div>
        <div class="col-4">
            <select class="ddlSType form-control form-control-sm"></select>
        </div>
        <div class="col-4">
            <input type="text" class="txtURL form-control-sm form-control">
        </div>
        <div class="col-auto">
            <button class="btnDelete btn btn-light btn-sm">
                <i class="fa fa-trash"></i>
            </button>
        </div>
    </div>`;
        tr = $(tr);
        let items = ['Facebook', 'Twitter', 'LinkedIN', 'Instagram', 'Youtube', 'Pinterest', 'gplus', 'Tumblr', 'Custom'];
        let ddl = tr.find('.ddlSType')
        for (let i = 0; i < items.length; i++) {
            ddl.append('<option value="' + items[i] + '">' + items[i] + '</option>');
        }
        if (op) {
            ddl.val(op.type);
            tr.find('.txtURL').val(op.url);
            tr.find('.img').append('<img width="34" src="' + op.imgurl + '"/>')
        } else {
            ddl.val('Custom');
            let src = self.GetSocialIconImagePath('Custom');
            tr.find('.img').html('<img  width="34" src="' + src + '"/>');
        }
        return tr;
    }

    this.ButtonSetting = function () {
        let block = self.EditBlock.find('#img-template-edit')[0].el;
        self.EditBlock.find('#img-template-edit').remove();
        self.EditBlock.find('#img-template-resolution').remove();
        let formModal = _common.DynamicPopUp({ title: 'Button Settings', body:self.ModalButtonSetting, 'DistroyOnHide': true });
       
       // let formModal = self.Container.find('.create-link-modal');
        formModal.modal('show');
        formModal.find('.dvLinkEmail').hide();
        formModal.find('.txtText').val(block.find('a').text());
        formModal[0].block = block;
        if (!formModal[0].EventBind) {
            formModal.find('.modal-body .ddlLinkType').on('change', function () {
                if ($(this).val() == 'url') {
                    formModal.find('.dvLinkURL').show();
                    formModal.find('.dvLinkEmail').hide();
                } else {
                    formModal.find('.dvLinkURL').hide();
                    formModal.find('.dvLinkEmail').show();
                }
            })
            formModal.find('.btnSave').on('click', function () {
                let block = formModal[0].block;
                var lnkHref = '';
                if (formModal.find('.ddlLinkType').val() == "email") {
                    lnkHref = 'mailto:' + formModal.find('.txtLinkEmailAddress').val() + '?';
                    lnkHref += 'subject=' + (formModal.find('.txtLinkMessageSubject').val() || '') + '&';
                    // change me 
                    lnkHref += 'body=' + (formModal.find('.txtLinkMessageBody').val().replace(/\r\n|\r|\n/g, "%0D%0A") || '');
                    block.find('a').attr('href', lnkHref);
                }
                else {
                    lnkHref = formModal.find('.txtURL').val();
                    if (lnkHref.indexOf('http') != 0) {
                        lnkHref = 'http://' + lnkHref
                    }
                    if (_common.isValidUrl(lnkHref) == true) {
                        block.find('a').attr('href', lnkHref);
                    }
                    else {
                        alert("Please enter a valid url.");
                        return;
                    }
                }
                block.find('a').html(formModal.find('.txtText').val());
                block.closest('table').attr('align', formModal.find('.ddlAlign').val());
                formModal.modal('hide');
            })
            formModal.find('.btnClose').on('click', function () {
                if (block.find('a').attr('href') == '#') {
                    block.find('a').removeAttr('href');
                }
            });
            formModal[0].EventBind = true;
        }
        let align = block.closest('table').attr('align');
        if (!align) { align = 'left' }
        formModal.find('.ddlAlign').val(align);
        var linkHrefShow = block.find('a').attr('href');
        if (linkHrefShow && linkHrefShow.startsWith("mailto:")) {
            var lnkArr = linkHrefShow.split('?subject=');
            if (lnkArr.length == 2) {
                formModal.find('.txtLinkEmailAddress').val(lnkArr[0].replace('mailto:', ''));
                var lnkSubArr = lnkArr[1].split('&body=');

                if (lnkSubArr.length == 2) {
                    formModal.find('.txtLinkMessageSubject').val(lnkSubArr[0]);
                    formModal.find('.txtLinkMessageBody').val(lnkSubArr[1].toString().replace(/%0D%0A/gi, "\n"));
                }
                else {
                    var lnkSubArr = lnkArr[1].split('&body=');
                    if (lnkSubArr.length == 2) {
                        formModal.find('.txtLinkMessageSubject').val(lnkSubArr[0]);
                        formModal.find('.txtLinkMessageBody').val(lnkSubArr[1].toString().replace(/%0D%0A/gi, "\n"));
                    }
                }
            }
            formModal.find('.ddlLinkType').val('email');
        } else {
            if (block.find('a').attr('href') == "#") {
                formModal.find('.txtURL').val("");
            }
            else {
                formModal.find('.txtURL').val(block.find('a').attr('href'));
            }
        }
        formModal.find('.txtText').val(block.find('a').html().trim());
        formModal.find('.ddlLinkType').change()

        formModal.modal('show');
    }

    this.MapSetting = function () {
        let imgBlock = self.EditBlock.find('#img-template-edit')[0].el;
        self.EditBlock.find('#img-template-edit').remove();
        self.EditBlock.find('#img-template-resolution').remove();
        let modalMap = _common.DynamicPopUp({ title: 'Mark Location', body: _templateEditor.MAP_HTML, 'DistroyOnHide': true });
        modalMap.modal('show');
        modalMap.find('.btnSave').on('click', function () {
            let key = _common.GetMapKey();
            let src = "https://maps.googleapis.com/maps/api/staticmap?center=##lat##,##lng##&zoom=##zoom##&size=##width##x##height##&maptype=roadmap&markers=color:red%7Clabel:A%7C##mlat##,##mlng##&key=" + key;
            src = src.replace('##lat##', modalMap[0].lat);
            src = src.replace('##lng##', modalMap[0].lng);
            src = src.replace('##mlat##', modalMap[0].lat);
            src = src.replace('##mlng##', modalMap[0].lng);
            src = src.replace('##zoom##', modalMap[0].map.getZoom());
            src = src.replace('##width##', imgBlock.width());
            src = src.replace('##height##', imgBlock.height());
            imgBlock.attr('src', src);
            imgBlock.attr('data-lat', modalMap[0].lat);
            imgBlock.attr('data-lng', modalMap[0].lng);
            imgBlock.attr('data-zoom', modalMap[0].map.getZoom());
            let mapLink;
            if (imgBlock.parent().hasClass('map-link')) {
                mapLink = imgBlock.parent();
            }
            else {
                mapLink = $('<a class="map-link" target="_blank"></a>');
                mapLink.insertAfter(imgBlock);
                imgBlock.appendTo(mapLink);
            }

            // .gm-svpc{
            //     display: none !important;
            // }
            console.log('https://www.google.com/maps/search/?api=1&query=' + modalMap[0].lat + ',' + modalMap[0].lng, "=================>>>>>")
            mapLink.attr('href', 'https://www.google.com/maps/search/?api=1&query=' + modalMap[0].lat + ',' + modalMap[0].lng);
            modalMap.modal('hide');
        })
        setTimeout(function () {
            // search box events 
            let addSearchBox = (modalMap, map, marker) => {
                var input = modalMap.find('.txtSearch')[0];
                var searchBox = new google.maps.places.SearchBox(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.addListener('bounds_changed', function () {
                    searchBox.setBounds(map.getBounds());
                });

                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', () => {
                    var places = searchBox.getPlaces();
                    if (places.length == 0) {
                        return;
                    }
                    var place = places[0];
                    if (marker) {
                        marker.setMap(null);
                    }
                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();

                    marker = new google.maps.Marker({
                        map: map,
                        title: place.name,
                        draggable: true,
                        position: place.geometry.location
                    });
                    addMarkerEvent(marker, map);
                    modalMap[0].HasPlace = true;
                    modalMap[0].lat = place.geometry.location.lat();
                    modalMap[0].lng = place.geometry.location.lng();
                    modalMap[0].map = map;
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                    map.fitBounds(bounds);
                    google.maps.event.addListener(marker, 'dragend', function (event) {
                        modalMap[0].lat = this.getPosition().lat();
                        modalMap[0].lng = this.getPosition().lng();
                        modalMap[0].map = map;
                        modalMap[0].HasPlace = true;
                    });
                });
            }

            //marker drag event
            let addMarkerEvent = (marker, map) => {
                marker.addListener('dragend', function () {
                    map.setCenter(new google.maps.LatLng(marker.position.lat(), marker.position.lng()));
                    modalMap[0].lat = marker.position.lat();
                    modalMap[0].lng = marker.position.lng();
                    modalMap[0].map = map;
                    modalMap[0].HasPlace = true;
                });
            }

            let map;
            let marker;
            //map in edit mode
            if (imgBlock.attr('data-lat')) {
                map = new google.maps.Map(modalMap.find('.map')[0], {
                    center: { lat: parseFloat(imgBlock.attr('data-lat')), lng: parseFloat(imgBlock.attr('data-lng')) },
                    zoom: parseFloat(imgBlock.attr('data-zoom')),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    streetViewControl: false,
                    draggable: true
                });
                marker = new google.maps.Marker({
                    map: map,
                    title: 'Location',
                    draggable: true,
                    position: { lat: parseFloat(imgBlock.attr('data-lat')), lng: parseFloat(imgBlock.attr('data-lng')) }
                });
                addMarkerEvent(marker, map);
                modalMap[0].HasPlace = true;
                modalMap[0].map = map;
                modalMap[0].lat = imgBlock.attr('data-lat');
                modalMap[0].lng = imgBlock.attr('data-lng');
                addSearchBox(modalMap, map, marker);
            } else {

                //new map
                let positionObj = { lat: -33.8688, lng: 151.2195 };
                let zoom = 1;
                _common.GetLocationFromBrowser().then(
                    data => {
                        if (data != 401) {
                            positionObj = data;
                            zoom = 4;
                        }
                        map = new google.maps.Map(modalMap.find('.map')[0], {
                            center: positionObj,
                            zoom: zoom,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        marker = new google.maps.Marker({
                            map: map,
                            title: 'Location',
                            draggable: true,
                            position: positionObj
                        });
                        modalMap[0].HasPlace = true;
                        modalMap[0].map = map;
                        modalMap[0].lat = positionObj.lat;
                        modalMap[0].lng = positionObj.lng;
                        addMarkerEvent(marker, map);
                        addSearchBox(modalMap, map, marker);
                    }
                )
            }
        }, 1000)
    }

    this.ImageSetting = function () {
        let block = self.EditBlock.find('#img-template-edit')[0].el;
        self.EditBlock.find('#img-template-edit').remove();
        self.EditBlock.find('#img-template-resolution').remove();

      //  let formModal = self.Container.find('.modal-image-property');
      let formModal = _common.DynamicPopUp({ title: 'Image Settings', body:self.ModalImageSetting, 'DistroyOnHide': true });
        formModal.modal('show');
        formModal[0].block = block;
        if (!formModal[0].EventBind) {
            formModal.find('.btnSave').on('click', function () {
                let block = formModal[0].block;
                block.css("width", "");
                block.css("height", "");
                block.attr('height', formModal.find('.txtHeight').val());
                block.attr('width', formModal.find('.txtWidth').val());
                if (formModal.find('.chkResponsive').is(':checked')) {
                    block.addClass('img');
                } else {
                    block.removeClass('img');
                }
                if (formModal.find('.chkRatio').is(':checked')) {
                    block.removeClass('nmtrt');
                } else {
                    block.addClass('nmtrt');
                }

                if (formModal.find('.chkHasLink').is(':checked')) {
                    if (formModal.find('.txtURL').val() != "") {
                        if (_common.isValidUrl(formModal.find('.txtURL').val()) == true) {
                            if (block.parent().is('a')) {
                                block.parent().attr('href', formModal.find('.txtURL').val());
                            } else {
                                let alink = $('<a/>').insertAfter(block);
                                block.appendTo(alink);
                                alink.attr('href', formModal.find('.txtURL').val());
                                alink.attr('target', "_blank");
                            }
                        }
                        else {
                            alert('Please enter a valid url.');
                            return;
                        }
                    }
                    else {
                        alert("Please enter a url.");
                        return;
                    }
                }
                block.attr('data-style', formModal.find('.ddlStyle').val());
                if (formModal.find('.ddlStyle').val() == 1) {
                    block.css('border-radius', '0');
                }
                if (formModal.find('.ddlStyle').val() == 2) {
                    block.css('border-radius', '10px');
                }
                if (formModal.find('.ddlStyle').val() == 3) {
                    block.css('border-radius', '300px');
                    let height = parseInt(formModal.find('.txtHeight').val());
                    let width = parseInt(formModal.find('.txtWidth').val());
                    if (height < width) {
                        block.attr('width', formModal.find('.txtHeight').val());
                    } else {
                        block.height('height', formModal.find('.txtWidth').val());
                    }
                }
                block.closest('td').attr('align', formModal.find('.ddlAlign').val());
                block.attr('align', formModal.find('.ddlAlign').val());

                formModal.modal('hide');
            });
            formModal.find('.chkHasLink').click(function () {
                if ($(this).is(':checked')) {
                    formModal.find('.tmp-link').show();
                } else {
                    formModal.find('.tmp-link').hide();
                }
            });
            formModal.find('.txtHeight').change(function () {
                if (formModal.find('.chkRatio').is(':checked')) {
                    var width = parseInt(formModal.find('.txtHeight').val()) / formModal.find('.chkRatio')[0].ratio;
                    if (width > 600) {
                        let height = 600 * formModal.find('.chkRatio')[0].ratio;
                        formModal.find('.txtWidth').val('600');
                        formModal.find('.txtHeight').val(Math.ceil(height));
                    } else {
                        formModal.find('.txtWidth').val(Math.ceil(width));
                    }
                }
            });
            formModal.find('.txtWidth').change(function () {
                var width = parseInt(formModal.find('.txtWidth').val());
                if (width > 600) {
                    width = 600;
                    formModal.find('.txtWidth').val('600');
                }
                if (formModal.find('.chkRatio').is(':checked')) {
                    let height = width * formModal.find('.chkRatio')[0].ratio;
                    formModal.find('.txtHeight').val(Math.ceil(height));
                }
            });
            formModal[0].EventBind = true;
        }

        if (block.parent().is('a')) {
            formModal.find('.tmp-link').show();
            formModal.find('.txtURL').val(block.parent().attr('href'));
            formModal.find('.chkHasLink').prop('checked', true);
            formModal.find('.tmp-link').show();
        } else {
            formModal.find('.chkHasLink').prop('checked', false);
            formModal.find('.txtURL').val("");
            formModal.find('.tmp-link').hide();
        }
        formModal.find('.txtHeight').val(block[0].height);
        formModal.find('.txtWidth').val(block[0].width);
        formModal.find('.chkRatio')[0].ratio = block[0].height / block[0].width;
        if (block.hasClass('img')) {
            formModal.find('.chkResponsive').prop('checked', true);
        }else{
            formModal.find('.chkResponsive').prop('checked', false);
        }
        if (!block.hasClass('nmtrt')) {
            formModal.find('.chkRatio').prop('checked', true);
        }else{
            formModal.find('.chkRatio').prop('checked', false);
        }
        if (block.attr('data-style')) {
            formModal.find('.ddlStyle').val(block.closest('td').attr('data-style'));
        }
        if (block.closest('td').attr('align')) {
            formModal.find('.ddlAlign').val(block.closest('td').attr('align'));
        } else {
            formModal.find('.ddlAlign').val('left');
        }
      
    }

    this.ImageUpload = function () {
        let imgBlock = self.EditBlock.find('#img-template-edit')[0].el;
        //Attach height and width to imageBlock if missing
        if (!imgBlock.attr("height")) {
            imgBlock.attr("height", Math.round(imgBlock.height()))
        }
        if (!imgBlock.attr("width")) {
            imgBlock.attr("width", Math.round(imgBlock.width()))
        }
        self.EditBlock.find('#img-template-edit').remove();
        self.EditBlock.find('#img-template-resolution').remove();
        gallery.MediaGallery(imgBlock);
    }

    this.OpenLinkDialouge = function (link) {
        if (link) {
            self.EditLink(link);
            return;
        }
        let range = self.EditBlock[0].getSelection().getRangeAt(0);

        if (range.collapsed) {
            alert('Please get selection of words from which you want to create a link.');
            return;
        }
        let parentElement = range.commonAncestorContainer;
        if (parentElement.parentNode.hasAttribute('href')) {
            self.EditLink($(parentElement.parentNode));
            return;
        }
        self.AddLink();

    }
    this.AddLink = function () {
        let formModal = this.SetupLinkModal('Create Link', function (linkValue) {
            self.EditBlock[0].execCommand('createlink', false, linkValue);
        });
        formModal.find('.gp-underline').hide();
    }
    this.EditLink = function (link) {
        let formModal = this.SetupLinkModal('Link Properties', function (linkValue, formModal) {
            link.attr('href', linkValue);
            if (formModal.find('.isUnderline').is(':checked')) {
                link.css('text-decoration', 'underline');
            } else {
                link.css('text-decoration', 'none');
            }
        });
        formModal.find('.gp-underline').show();
        let textDecoration = link.css('text-decoration');
        if (textDecoration == null || textDecoration.indexOf('underline') > -1) {
            formModal.find('.isUnderline').prop('checked', true);
        }
        let hrefData = link.attr('href');
        if (hrefData.indexOf('mailto') != -1) {
            formModal.find('.modal-body .ddlLinkType').val('email');
            formModal.find('.dvLinkURL').hide();
            formModal.find('.dvLinkEmail').show();
            var lnkArr = hrefData.split('?subject=');
            if (lnkArr.length == 2) {
                formModal.find('.txtLinkEmailAddress').val(lnkArr[0].replace('mailto:', ''));
                var lnkSubArr = lnkArr[1].split('&body=');

                if (lnkSubArr.length == 2) {
                    formModal.find('.txtLinkMessageSubject').val(lnkSubArr[0]);
                    formModal.find('.txtLinkMessageBody').val(lnkSubArr[1].toString().replace(/%0D%0A/gi, "\n"));
                }
                else {
                    var lnkSubArr = lnkArr[1].split('&body=');
                    if (lnkSubArr.length == 2) {
                        formModal.find('.txtLinkMessageSubject').val(lnkSubArr[0]);
                        formModal.find('.txtLinkMessageBody').val(lnkSubArr[1].toString().replace(/%0D%0A/gi, "\n"));
                    }
                }
            }           
        } else {
            formModal.find('.dvLinkURL').show();
            formModal.find('.dvLinkEmail').hide();
            formModal.find('.modal-body .ddlLinkType').val('url');
            let url = window.location.href.substring(0, window.location.href.length - 1);
            hrefData = hrefData.replace(new RegExp(url, 'g'), '');
            formModal.find('.txtURL').val(hrefData);
        }
    }
    this.SetupLinkModal = function (title, onSave) {
       // let formModal = self.Container.find('.modal-link-property');
       let formModal = _common.DynamicPopUp({ title: 'Link Properties', body:self.ModalLinkSetting, 'DistroyOnHide': true });
        formModal.modal('show');
        formModal.find('.dvLinkEmail').hide();
        formModal.find('.dvBtnText').hide();
        formModal[0].onSave = onSave;
        if (!formModal[0].EventBind) {
            formModal.find('.modal-body .ddlLinkType').on('change', function () {
                if ($(this).val() == 'url') {
                    formModal.find('.dvLinkURL').show();
                    formModal.find('.dvLinkEmail').hide();
                } else {
                    formModal.find('.dvLinkURL').hide();
                    formModal.find('.dvLinkEmail').show();
                }
            });
            formModal.find('.btnSave').click(function () {
                let linkValue = '';
                let flag = 0;
                if (formModal.find('.ddlLinkType').val() == 'email') {
                    flag = 1;
                    let msgmail = formModal.find('.txtLinkEmailAddress').val();
                    let msgSubject = formModal.find('.txtLinkMessageSubject').val();
                    let msgAddress = (formModal.find('.txtLinkMessageBody').val().toString().replace(/\r\n|\r|\n/g, "%0D%0A") || '');

                    if (msgSubject.indexOf('&') != -1 || msgAddress.indexOf('&') != -1) {
                        swal("special char(&,?) not allowed");
                        return false;
                    }
                    if (!_common.isValidEmailAddress(msgmail)) {
                        swal("Please enter valid mail address", { icon: "info", });
                        return false;
                    }
                    linkValue = 'mailto:' + msgmail + '?subject=' + msgSubject + '&body=' + msgAddress;
                } else {
                    linkValue = $.trim(formModal.find('.txtURL').val());
                    if (!(linkValue.indexOf('http://') == 0 || linkValue.indexOf('https://') == 0)) {
                        linkValue = 'http://' + linkValue;
                    }
                }
                if (flag == 0 && (!_common.isValidUrl(linkValue) || linkValue.length == 0 || !linkValue.includes("http"))) {
                    swal(_constantClient.URL_INVALID_MSG, { icon: "warning" });
                    return false;
                } else {
                    formModal[0].onSave(linkValue, formModal);
                    formModal.modal('hide');
                }

            });
            formModal[0].EventBind = true;
        }
        return formModal;
    }

    this.GetSelectedElement = function () {
        let el = self.EditBlock.find('.marketing-editing-block .active');
        if (el.length == 0) {
            if (this.Container.find('.ddlStyleElement').val() == 'PageBody') {
                el = self.EditBlock.find('body');
            }
            else if (this.Container.find('.ddlStyleElement').val() == 'EmailBody') {
                el = self.EditBlock.find('.marketing-editing-block');
            }
        }
        else {
            el.isBlock = true;
            this.Container.find('.ddlStyleElement').val("Section");
        }
        return el;
    }
    this.CheckSelection = function () {
        var selectionWords;
        if (!self.EditBlock[0].getSelection().isCollapsed)
            selectionWords = self.EditBlock[0].getSelection().getRangeAt(0);
        else
            selectionWords = undefined;
        if (selectionWords != undefined && !selectionWords.collapsed) {
            return true;
        }
        return false;
    }

    this.BindDragEvent = function (block) {
        block.find('.block-item').draggable({
            scroll: true,
            iframeFix: true,
            iframeOffset: $('#iframeEdit').offset(),
            iframeScroll: true,
            refreshPositions: true,
            helper: function (event) {
                self.Container.find('.marketing-template-editor-body').append('<div style="padding:10px;font-weight:bold;color:#222;border:dashed 1px #777;width:210px;z-index:9999999" class="drag-helper"><i class="fa fa-list-alt" aria-hidden="true"></i> Drop this snap to template editor.</div>');
                // let offset = $('#iframeEdit').offset();
                // let scrollTop = self.EditBlock.scrollTop();
                // let top = offset.top; 
                // if (scrollTop > 0) {
                //     let offsetGap = 0;  
                //     self.EditBlock.find('.block-item').each(function () {
                //         if ($(this).offset().top + $(this).height() < scrollTop + $(window).height()+offsetGap) {
                //             offsetGap += 28;                            
                //         }
                //     })
                //     top = -1 * self.EditBlock.scrollTop()-offsetGap;
                //     setTimeout(function () {                       
                //         self.EditBlock.scrollTop(scrollTop + offsetGap);
                //     }, 200);
                // }
                // $(this).draggable("option", "cursorAt", { left: offset.left, top: top });
                // return self.Container.find('.drag-helper').css({ marginTop: top, marginLeft: offset.left }).show();
                return self.Container.find('.drag-helper').show();
            },
            start: function (event, ui) {
                self.EditBlock.find('.marketing-editing-block .template-block-droppable').remove();
                self.EditBlock.find('.marketing-editing-block .block-item').each(function () {
                    if ($(this).hasClass("ui-draggable-dragging")) {
                        return;
                    }
                    if (!($(this).hasClass('remove'))) {
                        $('<div class="template-block-droppable" style="text-align:center">\
                                <span class="ui-drop">Drop here</span>\
                            </div>').insertBefore($(this));
                    }
                })
                self.EditBlock.find('.marketing-editing-block').append('<div class="template-block-droppable" style="text-align:center"> <span class="ui-drop">Drop here</span></div>');
                self.BindDropEvent('.block-item');
            },
            stop: function (event, ui) {
                self.EditBlock.find('.marketing-editing-block .template-block-droppable').remove();
            }
        })
    }
    this.BindAllEvents = function () {
        self.EditBlock.on('dblclick', 'a', function () {
            self.OpenLinkDialouge($(this));
        })
        self.EditBlock.find('.marketing-editing-block').sortable({
            revert: true,
            handle: ".fa-arrows-alt",
            axis: "y",
            scroll: true
        });
        self.EditBlock.on('click', function () { $('.op-color-picker').ColorPickerHide(); });
        self.EditBlock.on('focus', '[contenteditable]', function () {
            this.beforeHTM = self.EditBlock.find('.marketing-editing-block')[0].outerHTML;
            this.beforBlock = $(this).html();
            self.cursorPosition = true;
        }).on('blur paste', '[contenteditable]', function () {
            self.beforechange();
            self.cursorPosition = false;
        });
        self.EditBlock.on('click', '.block-row-template-edit .fa', function (e) {
            e.stopPropagation();
        });
        self.EditBlock.on('click', '.block-item', function (e) {
            e.stopPropagation();
            $('.op-color-picker').ColorPickerHide();
            if ($(this).find('.block-row-template-edit').length == 0) {
                if ($(this).find('.unsubscribe-page').length > 0)
                    $(this).append(_templateEditor.MARKETING_TEMPLATE_SIDEBAR_ICON_ONLY_MOVE);
                else
                    $(this).append(_templateEditor.MARKETING_TEMPLATE_SIDEBAR_ICON);
            }
            $(this).find('.block-row-template-edit').show();
        });
        self.EditBlock.on('click', '.block-row-template-edit .fa.fa-clone', function (e) {
            e.stopPropagation();
            if ($(this).attr('data-disable') == undefined) {  //To disable double click
                let btn = $(this);
                btn.attr('data-disable', true);
                setTimeout(function () { btn.removeAttr('data-disable'); }, 400);
                self.beforechange();
                let block = $(this).closest('.block-item');
                let copyItem = $('<div class="block-item">' + block.html() + '</div>')
                if (block.attr('style')) {
                    copyItem.attr('style', block.attr('style'));
                }
                copyItem.find('.block-row-template-edit').remove();
                copyItem.insertAfter(block);
            }
        });

        self.EditBlock.on('click', '.block-row-template-edit .fa.fa-trash-alt', function (e) {
            e.stopPropagation();
            let block = $(this).closest('.block-item');
            _common.ConfirmDelete({ message: 'Are you sure,you want to delete this snippet?' }).then((confirm) => {
                if (confirm) {
                    self.beforechange();
                    block.remove();
                }
            });
        })

        self.EditBlock.on('click', '.block-row-template-edit .fa-arrows-alt', function () {
            let block = $(this).closest('.block-item');
            self.ReadElementStyle(block);
        });

        self.EditBlock.on('click', '.block-row-template-edit .fa.fa-code', function (e) {
            e.stopPropagation();
            let blockData = $(this).closest('.block-item').find('.block-item-edt');
            let body = $('<div><textarea class="txtEdit" style="width:100%;height:400px"></textarea></div>');
            let popup = _common.DynamicPopUp({ title: 'HTML Code', body: body.html(), 'DistroyOnHide': true });
            popup.find('.txtEdit').val(blockData.html());
            setTimeout(function () {
                var editor = CodeMirror.fromTextArea(popup.find('.txtEdit')[0], {
                    lineNumbers: true,
                    mode: "text/html",
                    gutters: ["CodeMirror-lint-markers"],
                    lint: true
                });
                popup.find('.btnSave').on('click', function () {
                    self.beforechange();
                    editor.save();
                    blockData.html('');
                    blockData.html(popup.find('.txtEdit').val());
                    popup.modal('hide');
                })
            }, 1000);
            popup.modal('show');
        })
        self.BindEditor();

    }

    this.BindDropEvent = function (acceptClass) {
        self.EditBlock.find('.marketing-editing-block .template-block-droppable').droppable({
            accept: acceptClass,
            activeClass: "ui-state-active",
            hoverClass: "ui-state-hover",
            tolerance: "touch",
            drop: function (event, ui) {
                if (!(ui.draggable.next()[0] == this || ui.draggable.prev()[0] == this)) {
                    if( self.EditBlock.find('.marketing-editing-block .template-block-droppable.ui-state-hover').length>0 && $(this).hasClass('ui-state-hover')==false){
                        return;
                    }
                    self.beforechange();
                    let tempBlock;
                    if (ui.draggable.hasClass('block-item')) {
                        tempBlock = $('<div class="block-item">' + ui.draggable.html() + '</div>');
                        tempBlock.attr('style', ui.draggable.attr('style'));
                        if (ui.draggable.hasClass('rspv')) {
                            tempBlock.addClass('rspv');
                        }
                        tempBlock.insertBefore($(this));
                        // Comment Manish 23Mar20  self.BindBlockEvent(tempBlock);
                    }
                    // else {
                    //     ui.draggable.parent().insertBefore($(this));
                    // }
                }              
            }
        })
    }

    this.BindEditor = function () {
        self.EditBlock.on('click', 'td.block-col', function () {
            $(this).attr('contenteditable', 'true');
            $(this).find('.template-button').each(function () {
                $(this).closest('table').attr('contenteditable', false);
            })
        })
        self.EditBlock.on('click', '.template-button,td.block-col,.td-col,.block-hr,.block-item-edt', function (e) {
            e.stopPropagation();
            $('.op-color-picker').ColorPickerHide();
            let block = $(this).closest('.block-item');
            if (block.find('.block-row-template-edit').length == 0) {
                if ($(this).find('.unsubscribe-page').length > 0)
                    block.append(_templateEditor.MARKETING_TEMPLATE_SIDEBAR_ICON_ONLY_MOVE);
                else
                    block.append(_templateEditor.MARKETING_TEMPLATE_SIDEBAR_ICON);
            }
            block.find('.block-row-template-edit').show();
            self.Container.find('.marketing-css-block .te-bold, .te-italic, .te-underline, .te-justifyLeft, .te-justifyRight, .te-justifyFull, .te-justifyCenter, .te-subscript, .te-superscript, .te-redo, .te-undo, .te-insertUnorderedList, .te-insertOrderedList').removeClass('btn-info');
            if ($(this).hasClass('template-button') || $(this).hasClass('block-hr')) {
                self.ReadElementStyle($(this));
                return;
            }
            let sel = self.EditBlock[0].getSelection();
            if (sel.isCollapsed) {
                self.ReadElementStyle($(this));
                return;
            }
            let selectedStr = sel.getRangeAt(0);
            let getTag = selectedStr.commonAncestorContainer.parentNode;
            let td = $(getTag).closest('.td-col');
            if (td.length == 0) {
                td = $(getTag).closest('.block-col');
            }
            if (td[0] === this) {

                let strTxtAlign = $(getTag).css("text-align");
                //Bold, Italic, Underline code
                if ($(getTag).closest('b').length > 0)
                    self.Container.find('.marketing-css-block .te-bold').addClass('btn-info');
                if ($(getTag).closest('i').length > 0)
                    self.Container.find('.marketing-css-block .te-italic').addClass('btn-info');
                if ($(getTag).closest('u').length > 0)
                    self.Container.find('.marketing-css-block .te-underline').addClass('btn-info');
                //Alignment code
                if (strTxtAlign == 'left')
                    self.Container.find('.marketing-css-block .te-justifyLeft').addClass('btn-info');
                if (strTxtAlign == 'right')
                    self.Container.find('.marketing-css-block .te-justifyRight').addClass('btn-info');
                if (strTxtAlign == 'justify')
                    self.Container.find('.marketing-css-block .te-justifyFull').addClass('btn-info');
                if (strTxtAlign == 'center')
                    self.Container.find('.marketing-css-block .te-justifyCenter').addClass('btn-info');

                //order-unorder list code
                if ($(getTag).closest('ul').length > 0)
                    self.Container.find('.marketing-css-block .te-insertUnorderedList').addClass('btn-info');
                if ($(getTag).closest('ol').length > 0)
                    self.Container.find('.marketing-css-block .te-insertOrderedList').addClass('btn-info');

                //sub-superscript code
                if ($(getTag).closest('sub').length > 0)
                    self.Container.find('.marketing-css-block .te-subscript').addClass('btn-info');
                if ($(getTag).closest('sup').length > 0)
                    self.Container.find('.marketing-css-block .te-superscript').addClass('btn-info');
                self.ReadElementStyle($(getTag), true);
            } else {
                self.ReadElementStyle($(this));
            }
        });
    }

    this.ReadElementStyle = function (currentBlock, normalElement) {
        self.EditBlock.find('.marketing-editing-block .active').removeClass('active');
        this.Container.find('.marketing-css-block').show();
        let section = "section";
        if (currentBlock.hasClass('template-button')) { section = "button" }
        if (currentBlock.hasClass('marketing-editing-block')) { section = "email" }
        if (currentBlock[0] && currentBlock[0].tagName == 'BODY') { section = "body" }
        if (currentBlock.hasClass('block-hr')) { section = "divider" }
        this.Container.find('.marketing-css-block').find('[data-section]').hide();
        this.Container.find('.marketing-css-block').find('[data-section*="' + section + '"]').show();
        self.EditBlock.find('.block-row-template-edit').hide();
        let activeBlock = currentBlock;
        if (normalElement) {
            activeBlock = currentBlock.closest('.td-col');
            if (activeBlock.length == 0) { activeBlock = currentBlock.closest('.block-col'); }
        }
        if (!(currentBlock.hasClass('marketing-editing-block'))) {
            activeBlock.addClass('active');
            activeBlock.closest('.block-item').find('.block-row-template-edit').show();
            self.Container.find('.marketing-css-block').attr('data-block', 'Section');
        }
        if (!(section == 'body' || section == 'email')) {
            this.Container.find('.ddlStyleElement').val('Section');
        }
        this.SetSelectionCSS(currentBlock, normalElement);
    }
    this.SetSelectionCSS = function (currentBlock, normalElement) {
        let styles = ['font-size', 'font-family', 'background-color', 'text-align', 'color', 'line-height', 'height'];
        let stylesBlock = ['border-color', 'border-width', 'border-style', 'padding-left', 'padding-right', 'padding-top', 'padding-bottom'];
        this.ResetToolBox();
        for (let i = 0; i < styles.length; i++) {
            let cStyle = currentBlock.css(styles[i]);
            if (styles[i] == 'line-height') {
                cStyle = parseFloat(currentBlock.css('line-height').replace('px', '')) / parseFloat(currentBlock.css('font-size').replace('px', ''));
            }
            this.SetToolBoxElementValue(styles[i], cStyle);
        }
        let activeBlock = currentBlock;
        if (normalElement) {
            activeBlock = currentBlock.closest('.td-col');
            if (activeBlock.length == 0) { activeBlock = currentBlock.closest('.block-col'); }
        }
        for (let i = 0; i < stylesBlock.length; i++) {
            let cStyle = activeBlock.css(stylesBlock[i]);
            this.SetToolBoxElementValue(stylesBlock[i], cStyle);
        }
        if (currentBlock.hasClass('block-hr')) {
            this.Container.find('.marketing-css-block .txtThickness').val(currentBlock.attr('height'));
        }
    }

    this.ResetToolBox = function () {
        this.Container.find('.marketing-css-block .ddlLineHeight').val("1.2");
        this.Container.find('.marketing-css-block .lblBorderColor').css('background-color', 'transparent').data('color', '#000000').trigger('getcolor');
        this.Container.find('.marketing-css-block .lblBackColor').css('background-color', 'transparent').data('color', '#ffffff').trigger('getcolor');
        this.Container.find('.marketing-css-block .lblForeColor').css('background-color', 'transparent').data('color', '#000000').trigger('getcolor');
        this.Container.find('.marketing-css-block .ddlBorderWidth').val(0);
        this.Container.find('.marketing-css-block .ddlBorderStyle').val("none");
        this.Container.find('.marketing-css-block .ddlFont').val("Century Gothic");
        this.Container.find('.marketing-css-block .txtSize').val(16)
        this.Container.find('.marketing-css-block .lft-box').val(0);
        this.Container.find('.marketing-css-block .top-box').val(0);
        this.Container.find('.marketing-css-block .rgt-box').val(0);
        this.Container.find('.marketing-css-block .bot-box').val(0);
    }
    this.SetToolBoxElementValue = function (attrValue, val) {
        if (!attrValue && !val) return;
        let value = val;
        if (typeof (val) == 'string') {
            value = val.trim();
        }
        switch (attrValue.trim()) {
            case 'line-height':
                this.Container.find('.marketing-css-block .ddlLineHeight').val(parseFloat(value));
                break;
            case 'border-color':
                let colorBorderValue = self.rgb2hex(value);
                this.Container.find('.marketing-css-block .lblBorderColor').css('background-color', colorBorderValue).data('color', colorBorderValue).trigger('getcolor');
                break;
            case 'border-width':
                this.Container.find('.marketing-css-block .ddlBorderWidth').val(parseInt(value));
                break;
            case 'border-style':
                this.Container.find('.marketing-css-block .ddlBorderStyle').val(value);
                break;
            case 'background-color':
                let colorBackValue = self.rgb2hex(value);

                this.Container.find('.marketing-css-block .lblBackColor').css('background-color', colorBackValue).data('color', colorBackValue).trigger('getcolor');
                break;
            case 'color':
                let colorForeValue = self.rgb2hex(value);
                this.Container.find('.marketing-css-block .lblForeColor').css('background-color', colorForeValue).data('color', colorForeValue).trigger('getcolor');
                break;
            case 'font-family':
                this.Container.find('.marketing-css-block .ddlFont').val(value.replace(/['"]+/g, ''));
                break;
            case 'font-size':
                this.Container.find('.marketing-css-block .txtSize').val(parseInt(value.replace('px', '')));
                break;
            case 'padding-left':
                this.Container.find('.marketing-css-block .lft-box').val(parseInt(value));
                break;
            case 'padding-right':
                this.Container.find('.marketing-css-block .rgt-box').val(parseInt(value));
                break;
            case 'padding-top':
                this.Container.find('.marketing-css-block .top-box').val(parseInt(value));
                break;
            case 'padding-bottom':
                this.Container.find('.marketing-css-block .bot-box').val(parseInt(value));
                break;
            case 'text-align':
                this.Container.find('.marketing-css-block .btn-align[cssval="' + value + '"]').addClass('btn-info')
                break;
        }
    }

    this.rgb2hex = function (orig) {
        var a, isPercent,
            rgb = orig.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
            alpha = (rgb && rgb[4] || "").trim(),
            hex = rgb ?
                (rgb[1] | 1 << 8).toString(16).slice(1) +
                (rgb[2] | 1 << 8).toString(16).slice(1) +
                (rgb[3] | 1 << 8).toString(16).slice(1) : orig;

        if (alpha !== "") {
            a = alpha;
        } else {
            a = 01;
        }
        // multiply before convert to HEX
        a = ((a * 255) | 1 << 8).toString(16).slice(1)
        hex = hex + a;
        return '#' + hex;
    }
    this.WriteBody = function (htm) {
        let doc = this.EditBlock[0];
        doc.open();
        doc.write(this.EditBlockHead);
        doc.write(htm);
        doc.close();
        self.EditBlock.find('.ui-droppable').remove();
        self.EditBlock.find('.ui-draggable-dragging').remove();
        self.EditBlock.find('#img-template-edit').remove();
        self.EditBlock.find('#img-template-resolution').remove();
        

    }
    _common.GetStaticPage('/page/marketing/templateEditor.html', function (pageHTML) {
        self.Container = $(pageHTML);
        self.ModalButtonSetting=self.Container.find('.modal-button-setting').html();
        self.ModalLinkSetting= self.Container.find('.modal-link-setting').html();
        self.ModalImageSetting= self.Container.find('.modal-image-setting').html();
        self.Container.find('.modal-body').remove();
        $('body').append(self.Container);
        $('body').addClass('fixedbody')
        let iframe = self.Container.find('#iframeEdit')[0];
       
        iframe.onload = function () {

            let doc = iframe.contentDocument || iframe.contentWindow.document;
            let head = $(doc).find('head')[0].outerHTML;
            self.EditBlock = $(doc);
            self.EditBlockHead = head;
            self.WriteBody(self.Content);
           
            self.EditBlock.find('.active').removeClass('active');
            self.EditBlock.find('.block-row-template-edit').remove();
            let width = self.EditBlock.find('.marketing-editing-block').attr('width');
            let align = self.EditBlock.find('.marketing-editing-block').attr('align');
            if (width) {
                let type = 'px';
                if (width.indexOf('%') > -1) { type = '%' }
                let nwidth = width.replace(type, '');
                self.Container.find('.marketing-css-block').find('.ddlEmailWidthType').val(type);
                self.Container.find('.marketing-css-block').find('.txtEmailWidth').val(nwidth);
            }
            if (align) {
                self.Container.find('.marketing-css-block').find('.ddlEmailAlign').val(align)
            }
            self.InitTemplate();
            self.BindAllEvents();
            self.Container.find('.marketing-css-block').find('.ddlStyleElement').change();
        };
    })
}