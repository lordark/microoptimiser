﻿const _page = {};
$(function () {
  _page.nav = $("#page-nav");
  _page.header = $("#main-header");
  _page.body = $("#page-body");
  _page.menu = $("#page-menu");
  _page.mainLoading = $("#main-loading");

  //Main Menu
  _page.menu.on("click", ".nav-item", function () {
    _page.menu.find(".nav-item").removeClass("active");
    $(this).addClass("active");
    $("#sidebarToggleTop").removeClass("cross");
    _page.menu.removeClass("slide");
    new Page({ pageName: $(this).attr("menu-data") });
  });
  _page.body.on("click", ".link-page", function () {
    let param = { pageName: $(this).attr("page-name") };
    if ($(this).attr("data-id") != undefined) {
      param._id = $(this).attr("data-id");
    }
    new Page(param);
  });
  _page.header.on("click", ".link-page", function () {
    new Page({ pageName: $(this).attr("page-name") });
  });

  //Nevigation
  _page.nav.on("click", ".nav-item", function () {
    _page.nav
      .find(".nav-item.active")[0]
      .Page.PageContainer.trigger("pageinactive");
    _page.nav.find(".nav-item").removeClass("active");
    $(this).addClass("active");
    _page.body.find(".page-content").removeClass("active");
    this.Page.PageContainer.addClass("active");
    let uid = $(this).attr("data-uid");
    if (_page.menu.find('.nav-item[menu-data="' + uid + '"]').length == 1) {
      _page.menu.find(".nav-item[menu-data]").removeClass("active");
      _page.menu.find('.nav-item[menu-data="' + uid + '"]').addClass("active");
    }
    this.Page.PageContainer.trigger("pageactive");
  });

  //Nevigation Close
  _page.nav.on("click", ".nav-item .tab-close", function (e) {
    e.stopPropagation();
    let nav = $(this).closest(".nav-item");
    if (nav.hasClass("active") && _page.nav.find(".nav-item").length > 1) {
      let snav = nav.prev();
      if (snav.length == 0) snav = nav.next();

      snav.addClass("active");
      snav[0].Page.PageContainer.addClass("active");
      if (nav[0].Page.param.pageName == "CreateCampaign") {
        _campaign.campaignData = {};
        _campaign.BasicPageData = {};
      }
    }
    nav[0].Page.PageContainer.trigger("pageinactive");
    nav[0].Page.PageContainer.remove();
    nav.remove();
  });

  //Nevigation Close
  _page.body.on("click", ".collapse-expend .collapse-head", function (e) {
    e.stopPropagation();
    $(this)
      .closest(".collapse-expend")
      .find(".collapse-body")
      .toggleClass("show");
    $(this).toggleClass("collapsed");
  });

  //Nevigation Close
  _page.body.on("click", ".page-open", function (e) {
    e.stopPropagation();
    let param = {
      pageName: $(this).attr("data-page-name"),
      pageType: $(this).attr("data-page-type"),
    };
    if ($(this).attr("data-id") != undefined)
      param._id = $(this).attr("data-id");
    if (this.onSave != undefined) param.onSave = this.onSave;
    if (this.MatchFields != undefined) {
      param.MatchFields = this.MatchFields;
    }

    new Page(param);
  });

  //Remove validation error
  $(document).on("keypress focus click", ".has-error .msp-ctrl", function () {
    $(this)
      .closest(".vld-parent")
      .removeClass("has-error")
      .find(".vld-message")
      .remove();
  });

  $(document).on(
    "keypress focus click",
    ".has-error button.multiselect",
    function () {
      $(this)
        .closest(".vld-parent")
        .removeClass("has-error")
        .find(".vld-message")
        .remove();
    }
  );
});

_page.GetFieldDisplayName = function (field) {
  return field.DisplayName ? field.DisplayName : field.Schema.DisplayName;
};

_page.BuildPage = function (container, pageData, data, readOnly) {
  if (pageData.Fields != undefined && pageData.Fields.length > 0) {
    let frm = container.append('<div class="row"></div>').find(".row");
    for (let i = 0; i < pageData.Fields.length; i++) {
      let field = pageData.Fields[i];
      if (field.Schema == undefined) continue;

      _page.BuildField({
        field: field,
        container: frm,
        data: data,
        readOnly: readOnly,
        pageData: pageData,
      });
    }
  }

  if (pageData.Groups != undefined) {
    for (let g = 0; g < pageData.Groups.length; g++) {
      let group = pageData.Groups[g];
      let pageGroup = $(
        '<div><h6 class="mb-2 mt-3">' +
          group.DisplayName +
          '</h6><div class="row page-group"></div></div>'
      );
      for (let i = 0; i < group.Fields.length; i++) {
        let field = group.Fields[i];
        if (field.Schema == undefined) continue;

        _page.BuildField({
          field: field,
          container: pageGroup.find(".page-group"),
          data: data,
          readOnly: readOnly,
          pageData: this.PageData,
          groupData: group,
        });
      }
      formModal.find(".modal-body").append(pageGroup);
    }
  }
};

_page.FieldResetBlank = function (fieldName, container) {
  let ctrl = container.find('[data-field="' + fieldName + '"]');
  if (ctrl && ctrl.length) {
    let field = ctrl[0].field;
    if (
      field.Schema.UIDataType == "lookup" ||
      field.Schema.UIDataType == "multilookup"
    ) {
      ctrl.find(".lookup-item").remove();
    } else ctrl.val("");
  }
};

_page.BuildField = function (params) {
  let field = params.field;
  let container = params.container;
  let data = params.data;
  let readOnly = params.readOnly != undefined ? params.readOnly : false;
  let removeEmpty =
    params.removeEmpty != undefined ? params.removeEmpty : false;
  let pageData = params.pageData;
  let groupData = params.groupData;

  let ctrl;
  let val;
  let isForEdit = data && data._id ? true : false;
  // added by Manish 18 oct 19 for nested lookup collection
  if (pageData != undefined && pageData.Collections) {
    field = $.extend({}, field);
    field.Collections = pageData.Collections.slice();
    let pField = $.extend({}, field);
    pField.Fields = undefined;
    pField.Collections = undefined;
    if (pField.Schema) {
      pField.Schema = undefined;
    }
    field.Collections.push(pField);
    field.ObjectName = pageData.ObjectName;
  }
  if (data != undefined) {
    ctrl = _controls.BuildCtrlWithValue({
      field: field,
      data: data,
      pageData: pageData,
      groupData: groupData,
      isForEdit: isForEdit,
    });
    val = _controls.GetDisplayValue({ field: field, data: data });
  } else
    ctrl = _controls.BuildCtrl({
      field: field,
      pageData: pageData,
      groupData: groupData,
      isForEdit: isForEdit,
    });

  if (readOnly) {
    ctrl = _common.DisableField(ctrl, field.Schema.UIDataType);
  }

  let col = 6;
  if (field.ColumnWidth != undefined) col = field.ColumnWidth;
  else if (
    groupData != undefined &&
    groupData != null &&
    groupData.DefaultColumnWidth != undefined
  )
    col = groupData.DefaultColumnWidth;
  else if (pageData != undefined && pageData.DefaultColumnWidth != undefined)
    col = pageData.DefaultColumnWidth;

  let isHidden = field.IsHidden ? "field-hidden d-none" : "";
  if (removeEmpty && !val) isHidden = "field-hidden d-none";
  let fieldDisplayName = _page.GetFieldDisplayName(field);
  let fieldContainer = $(
    '<div class="form-group col-sm-' +
      col +
      " vld-parent " +
      isHidden +
      '"><label class="small">' +
      fieldDisplayName +
      "</label></div>"
  );
  if (field.Schema.UIDataType == "collection") {
    fieldContainer = $(
      '<div class="form-group col-sm-' +
        col +
        " vld-parent " +
        isHidden +
        ' pt-4"><h6 class="mb-2 mt-3 pb-4">' +
        fieldDisplayName +
        '<span class="clearfix"></span></h6></div>'
    );
    if (field.IsHideBox) fieldContainer.find("h6").remove();
  }
  if (field.Schema.UIDataType == "checkbox") {
    fieldContainer.find("label").append(ctrl).addClass("parent-lblChk");
  } else fieldContainer.append(ctrl);

  if (field.Schema.IsCurrency == true)
    fieldContainer
      .append(ctrl)
      .find("label")
      .append(" (" + _common.GetCompanyCurrencySymbol() + ")");
  else if (field.Schema.IsPercent == true)
    fieldContainer.append(ctrl).find("label").append(" (%)");

  if (field.ResetFields !== undefined && field.ResetFields.length > 0) {
    if (
      field.Schema.UIDataType == "lookup" ||
      field.Schema.UIDataType == "multilookup"
    ) {
      ctrl.on("click", ".lookup-item .fa-times", function () {
        for (let i = 0; i < field.ResetFields.length; i++) {
          _page.FieldResetBlank(
            field.ResetFields[i],
            container.closest(".page-form")
          );
        }
      });
    }
  }

  container.append(fieldContainer);
};

_page.CheckFieldValidation = function (field, container, objectName, objectID) {
  if ((field.IsUnique || field.Schema.IsUnique) && objectName) {
    let ctrl = container.find('.msp-ctrl[data-field="' + field.Name + '"]');
    let value = _controls.GetCtrlValue({
      field: field,
      container: container,
      isEjsonFormat: true,
    });
    let extraFields = "";
    if (
      field.IsRequired ||
      field.Schema.IsRequired ||
      (field.IsRequired || field.Schema.IsRequired) == undefined
    ) {
      if (value == undefined || value == null || value == "") {
        _common.ValidateAddMsg(ctrl, "Unique field can not be blank.");
      }
    }
    let param = { name: objectName };
    param.match = [];
    if (field.UniqueFieldCombination || field.Schema.UniqueFieldCombination) {
      let uniqueCheck =
        field.UniqueFieldCombination || field.Schema.UniqueFieldCombination;
      for (let i = 0; i < uniqueCheck.length; i++) {
        let tempObj = {};
        let fldName = uniqueCheck[i];
        if (field.Schema.CaseSensitive !== undefined)
          tempObj[fldName] = {
            UniqueString: {
              Value: value,
              CaseSensitive: field.Schema.CaseSensitive,
            },
          };
        else tempObj[fldName] = value;
        param.match.push(tempObj);
      }
    } else {
      let tempObj = {};
      if (field.Schema.CaseSensitive !== undefined)
        tempObj[field.Name] = {
          UniqueString: {
            Value: value,
            CaseSensitive: field.Schema.CaseSensitive,
          },
        };
      else tempObj[field.Name] = value;
      param.match.push(tempObj);
    }
    if (field.UniqueFields != undefined) {
      for (let i = 0; i < field.UniqueFields.length; i++) {
        let fldName = field.UniqueFields[i];
        let fldObj = container.find('[data-field="' + fldName + '"]')[0].field;
        if (
          field.UniqueFieldCombination ||
          field.Schema.UniqueFieldCombination
        ) {
          let uniqueCheck =
            field.UniqueFieldCombination || field.Schema.UniqueFieldCombination;
          for (let i = 0; i < uniqueCheck.length; i++) {
            if (fldObj.Schema.CaseSensitive !== undefined)
              param.match[i][fldObj.Name] = {
                UniqueString: {
                  Value: _controls.GetCtrlValue({
                    field: fldObj,
                    container: container,
                    isEjsonFormat: true,
                  }),
                  CaseSensitive: fldObj.Schema.CaseSensitive,
                },
              };
            else
              param.match[i][fldObj.Name] = _controls.GetCtrlValue({
                field: fldObj,
                container: container,
                isEjsonFormat: true,
              });
            extraFields += _page.GetFieldDisplayName(fldObj) + ", ";
          }
        } else {
          if (fldObj.Schema.CaseSensitive !== undefined)
            param.match[0][fldObj.Name] = {
              UniqueString: {
                Value: _controls.GetCtrlValue({
                  field: fldObj,
                  container: container,
                  isEjsonFormat: true,
                }),
                CaseSensitive: fldObj.Schema.CaseSensitive,
              },
            };
          else
            param.match[0][fldObj.Name] = _controls.GetCtrlValue({
              field: fldObj,
              container: container,
              isEjsonFormat: true,
            });
          extraFields += _page.GetFieldDisplayName(fldObj) + ", ";
        }
      }
    }
    if (objectID != undefined) {
      if (field.UniqueFieldCombination || field.Schema.UniqueFieldCombination) {
        let uniqueCheck =
          field.UniqueFieldCombination || field.Schema.UniqueFieldCombination;
        for (let i = 0; i < uniqueCheck.length; i++) {
          param.match[i]["_id"] = {
            $ne: _controls.ToEJSON("objectid", objectID),
          };
        }
      } else {
        param.match[0]["_id"] = {
          $ne: _controls.ToEJSON("objectid", objectID),
        };
      }
    }
    let count = _common.CountUniqueFields(param);
    if (count > 0) {
      let errorMsg = ctrl.attr("title") + " already exists.";
      if (extraFields != "") {
        extraFields = extraFields.slice(0, -2);
        errorMsg =
          ctrl.attr("title") + " already exists with " + extraFields + ".";
      }
      _common.ValidateAddMsg(ctrl, errorMsg);
    }
  }

  //CheckCompareValidation
  if (field.Schema.CompareValidation) {
    let ctrl = container.find('.msp-ctrl[data-field="' + field.Name + '"]');
    let value = _controls.GetCtrlValue({ field: field, container: container });
    value = _controls.ParseValue(field.Schema.UIDataType, value);
    for (let i = 0; i < field.Schema.CompareValidation.length; i++) {
      let obj = field.Schema.CompareValidation[i];
      let valueToCompare;
      let errorMsg = "";
      let errorEnding = "";
      if (obj.ValueType == "Field") {
        let ctrlCompare = container.find(
          '.msp-ctrl[data-field="' + obj.Value + '"]'
        );
        if (ctrlCompare.length > 0) {
          let fieldCompare = ctrlCompare[0].field;
          valueToCompare = _controls.GetCtrlValue({
            field: fieldCompare,
            container: container,
          });
          valueToCompare = _controls.ParseValue(
            fieldCompare.Schema.UIDataType,
            valueToCompare
          );
          errorEnding = _page.GetFieldDisplayName(fieldCompare);
        }
      } else if (obj.ValueType == "Static") {
        valueToCompare = obj.Value;
        errorEnding = obj.Value;
      } else if (obj.ValueType == "Function") {
        //not impletmented
      }
      if (value && valueToCompare) {
        switch (obj.Operator) {
          case "GREATER_OR_EQUAL":
            if (!(value >= valueToCompare)) {
              errorMsg =
                ctrl.attr("title") +
                " must be greater or equal to " +
                errorEnding;
            }
            break;
          case "GREATER_THAN":
            if (!(value > valueToCompare)) {
              errorMsg =
                ctrl.attr("title") + " must be greater than " + errorEnding;
            }
          case "LESS_OR_EQUAL":
            if (!(value <= valueToCompare)) {
              errorMsg =
                ctrl.attr("title") + " must be less or equal to " + errorEnding;
            }
            break;
          case "LESS_THAN":
            if (!(value < valueToCompare)) {
              errorMsg =
                ctrl.attr("title") + " must be less than " + errorEnding;
            }
        }
        if (errorMsg) {
          _common.ValidateAddMsg(ctrl, errorMsg);
        }
      }
    }
  }
};

_page.LoadJsonData = function (container, pageData, jsonObj) {
  if (_common.Validate(container)) {
    if (pageData.Fields != undefined) {
      for (let i = 0; i < pageData.Fields.length; i++) {
        let field = pageData.Fields[i];
        if (field.Schema == undefined) continue;

        let val = _controls.GetCtrlValue({
          field: field,
          container: container,
          isEjsonFormat: true,
        });
        _page.CheckFieldValidation(
          field,
          container,
          pageData.ObjectName,
          jsonObj._id
        );
        jsonObj[field.Name] = val;
      }
    }

    if (pageData.Groups != undefined) {
      for (let g = 0; g < pageData.Groups.length; g++) {
        let group = pageData.Groups[g];

        for (let i = 0; i < group.Fields.length; i++) {
          let field = group.Fields[i];
          if (field.Schema == undefined) continue;

          let val = _controls.GetCtrlValue({
            field: field,
            container: container,
            isEjsonFormat: true,
          });
          _page.CheckFieldValidation(
            field,
            container,
            pageData.ObjectName,
            jsonObj._id
          );
          jsonObj[field.Name] = val;
        }
      }
    }

    if (container.find(".has-error").length > 0) {
      container.find(".has-error:first")[0].scrollIntoView({
        behavior: "smooth",
        block: "end",
        inline: "nearest",
      });
      return false;
    }
  } else {
    container
      .find(".has-error:first")[0]
      .scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
    return false;
  }
  return true;
};

_page.ClosePage = function (identifer) {
  let nav = _page.nav.find('.nav-item[data-uid="' + identifer + '"]');
  nav[0].Page.PageContainer.remove();
  nav.remove();
};

_page.OpenGridPage = function (identifer) {
  let snav = _page.nav.find('.nav-item[data-uid="' + identifer + '"]');
  if (snav.length > 0) {
    let gridObj = snav[0].Page.PageContainer.find(".msp-data-grid")[0].grid;
    gridObj.RefereshGrid();
    new Page({ pageName: "MarketingCampaignList" });
  }
};

_page.GetCollectionData = function (field, fieldRecord, onComplete) {
  let lookupFields = field.Schema.Fields.filter(
    (x) =>
      x.UIDataType == "lookup" ||
      x.UIDataType == "file" ||
      x.UIDataType == "dropdown"
  );
  if (lookupFields != null && lookupFields.length > 0) {
    for (let i = 0; i < lookupFields.length; i++) {
      let arrIDs = [];
      for (let j = 0; j < fieldRecord.length; j++) {
        if (fieldRecord[j][lookupFields[i].Name] != null) {
          arrIDs.push({ $oid: fieldRecord[j][lookupFields[i].Name] });
        }
      }
      if (arrIDs.length > 0) {
        let lookupObject = lookupFields[i].LookupObject;
        let lookupFieldsArr = lookupFields[i].LookupFields;
        if (lookupFields[i].UIDataType == "file") {
          lookupObject = "Drive";
          lookupFieldsArr = ["FileName"];
        }
        if (lookupFields[i].UIDataType != "dropdown") {
          _common.GetCollectionLookupData(
            lookupObject,
            lookupFieldsArr,
            arrIDs,
            function (data) {
              if (data) {
                for (let j = 0; j < fieldRecord.length; j++) {
                  fieldRecord[j][
                    lookupFields[i].Name + _constantClient.LookupAlias
                  ] = data.find(
                    (x) => x._id == fieldRecord[j][lookupFields[i].Name]
                  );
                }
              }
              onComplete();
            }
          );
        } else {
          for (let j = 0; j < fieldRecord.length; j++) {
            fieldRecord[j][
              lookupFields[i].Name + _constantClient.LookupAlias
            ] = _common
              .GetListSchemaDataFromInitData(lookupFields[i].ListSchemaName)
              .find((x) => x.Key == fieldRecord[j][lookupFields[i].Name]);
          }
        }
      }
    }
  } else {
    onComplete();
  }
};

function Page(param) {
  let self = this;
  this.param = param;
  this.Data = _common.GetPageDetailFromInitData(param.pageName);
  this.NewPage = false;

  this.Build = function () {
    if (this.Data.Type == "Form" || param.pageType == "Form") {
      new PageForm(param);
      return;
    }
    if (param.NewPage) this.NewPage = param.NewPage;
    if (param.PageTitle == undefined) param.PageTitle = this.Data.DisplayName;

    _page.body.find(".page-content").removeClass("active");
    let identifer = this.Data.PageName;
    if (param._id != undefined) identifer += ":" + param._id;
    let snav = _page.nav.find('.nav-item[data-uid="' + identifer + '"]');
    if (snav.length > 0 && !this.NewPage) {
      _page.nav.find(".nav-item").removeClass("active");
      snav.addClass("active");
      snav[0].Page.PageContainer.addClass("active");
      return;
    }

    if (param.MatchFields != undefined) this.MatchFields = param.MatchFields;
    if (param.HeaderFilters != undefined)
      this.HeaderFilters = param.HeaderFilters;

    let nav = $(
      '<span class="nav-item active" data-uid="' +
        identifer +
        '"><span class="title">' +
        param.PageTitle +
        '</span><span class="tab-close">X</span></span>'
    );
    let pageContainer = $(
      '<div class="page-content active" data-uid="' + identifer + '"></div>'
    );
    _page.nav.find(".nav-item").removeClass("active");
    nav.appendTo(_page.nav);
    pageContainer.appendTo(_page.content);
    this.Nav = nav;
    this.PageContainer = pageContainer;

    switch (this.Data.Type) {
      case "ObjectGrid":
        new DataGrid(this);
        break;
      case "ObjectDetail":
        new PageDetail(this, param._id);
        break;
      case "Static":
        new StaticPage(this);
        break;
      case "Calendar":
        new Calendar(this);
        break;
    }
    nav[0].Page = this;
    pageContainer[0].Page = this;
    pageContainer.appendTo(_page.body);
  };

  if (this.Data.RedirectPage) {
    if (param._id) {
      _common.GetObjectData({
        pageName: param.pageName,
        _id: param._id,
        callback: function (result) {
          if (result.message == "success") {
            for (let i = 0; i < self.Data.RedirectPage.length; i++) {
              let objPage = self.Data.RedirectPage[i];
              let isValid = true;
              for (let j = 0; j < objPage.Conditions.length; j++) {
                let objCondition = objPage.Conditions[j];
                let val = result.data[objCondition.FieldName];
                if (objCondition.Value != val) {
                  isValid = false;
                  break;
                }
              }
              if (isValid) {
                param.pageName = objPage.PageName;
                new Page(param);
                break;
              }
            }
          }
        },
      });
    }
  } else {
    let hasPermission = _userprofile.Auth({ pageData: this.Data });
    if (hasPermission) {
      this.Build();
    }
  }
}

function PageDetail(param, _id) {
  let self = this;
  this.param = param;
  this._id = _id;
  this.Data = {};
  this.PageData = undefined;
  this.ObjectPage = undefined;

  this.Build = function () {
    let title = "",
      headerImage;
    let titleArray = [];
    if (this.PageData.Fields != undefined && this.PageData.Fields.length > 0) {
      let groupRow = $('<div class="row"></div>');
      for (let i = 0; i < this.PageData.Fields.length; i++) {
        let field = this.PageData.Fields[i];
        let col = 6;
        if (field.ColumnWidth != undefined) col = field.ColumnWidth;
        else if (this.PageData.DefaultColumnWidth != undefined)
          col = this.PageData.DefaultColumnWidth;
        if (field.Schema == undefined || field.IsHidden == true) continue;

        let val = _controls.GetDisplayValue({ field: field, data: this.Data });
        if (field.IsPageTitle) {
          title += val + " ";
          titleArray.push(val);
        }
        if (field.IsPrimaryImage == true) {
          let imgCtrl = $(val);
          if (imgCtrl) headerImage = imgCtrl.find("img").attr("src");
          continue;
        }

        let fieldDisplayName = _page.GetFieldDisplayName(field);
        let fieldCell = $(
          '<div class="form-group col-sm-' +
            col +
            '" data-field="' +
            field.Name +
            '"><label class="small mb-0">' +
            fieldDisplayName +
            '</label><div class="text-gray-900"></div></div>'
        );
        if (field.Schema.UIDataType == "collection") {
          let cparam = {};
          cparam.field = field;
          cparam.fieldRecord = this.Data[field.Name];
          cparam.field.Idetifier = { _id: { $oid: this.Data._id } };
          field.ObjectName = this.PageData.ObjectName;
          cparam.MainData = this.Data;
          fieldCell = $(
            '<div class="form-group col-sm-' +
              col +
              '" data-field="' +
              field.Name +
              '"><h6 class="font-weight-bold border-bottom pb-2">' +
              fieldDisplayName +
              '<span class="clearfix"></span></h6><div class="text-gray-900"></div></div>'
          );
          fieldCell
            .find(".text-gray-900")
            .append(self.RecBuildCollection(cparam));
        }
        fieldCell.find(".text-gray-900").append(val);
        groupRow.append(fieldCell);
      }
      this.ObjectPage.find(".page-detail").append(groupRow);
    }
    if (this.PageData.Groups != undefined && this.PageData.Groups.length > 0) {
      for (let g = 0; g < this.PageData.Groups.length; g++) {
        let group = this.PageData.Groups[g];
        let groupRow = $('<div class="row"></div>');
        for (let i = 0; i < group.Fields.length; i++) {
          let field = group.Fields[i];
          let col = 6;
          if (field.ColumnWidth != undefined) col = field.ColumnWidth;
          else if (group.DefaultColumnWidth != undefined)
            col = group.DefaultColumnWidth;
          else if (this.PageData.DefaultColumnWidth != undefined)
            col = this.PageData.DefaultColumnWidth;
          if (field.Schema == undefined || field.IsHidden == true) continue;

          let val = _controls.GetDisplayValue({
            field: field,
            data: this.Data,
          });

          if (
            field.Schema.UIDataType == "location" &&
            (val == "" ||
              (field.LocateOnMap && field.LocateOnMap.IsRemove === true))
          ) {
            continue;
          }

          if (field.IsPageTitle) {
            title += val + " ";
            titleArray.push(val);
          }
          if (field.IsPrimaryImage == true) {
            let imgCtrl = $(val);
            if (imgCtrl) headerImage = imgCtrl.find("img").attr("src");
            continue;
          }

          let fieldDisplayName = _page.GetFieldDisplayName(field);
          let fieldCell = $(
            '<div class="form-group col-sm-' +
              col +
              '" data-field="' +
              field.Name +
              '"><label class="small mb-0">' +
              fieldDisplayName +
              '</label><div class="text-gray-900"></div></div>'
          );
          // if (field.Schema.IsLabelHidden == true) {
          //     fieldCell.find('label').remove();
          // }
          if (field.Schema.UIDataType == "collection") {
            let cparam = {};
            cparam.field = field;
            cparam.fieldRecord = this.Data[field.Name];
            cparam.field.Idetifier = { _id: { $oid: this.Data._id } };
            field.ObjectName = this.PageData.ObjectName;
            cparam.MainData = this.Data;
            fieldCell = $(
              '<div class="form-group col-sm-' +
                col +
                '" data-field="' +
                field.Name +
                '"><h6 class="font-weight-bold border-bottom pb-2">' +
                fieldDisplayName +
                '<span class="clearfix"></span></h6><div class="text-gray-900 pt-2"></div></div>'
            );
            if (field.IsHideBox) {
              fieldCell.find("h6").remove();
            }
            if (cparam.fieldRecord != undefined) {
              fieldCell
                .find(".text-gray-900")
                .append(self.RecBuildCollection(cparam));
            }
          }
          fieldCell.find(".text-gray-900").append(val);
          groupRow.append(fieldCell);
        }
        let pageGroup = $(
          '<div class="mb-3 detail-sec"><h6 class="font-weight-bold border-bottom pb-2">' +
            group.DisplayName +
            '<span class="clearfix"></span></h6></div>'
        );
        if (group.IsHideBox) {
          pageGroup.find("h6").remove();
        }
        pageGroup.append(groupRow);
        this.ObjectPage.find(".page-detail").append(pageGroup);
      }
    }

    if (this.PageData.PageTitleFormula != undefined) {
      let formula = this.PageData.PageTitleFormula.toString();
      for (let j = 0; j < titleArray.length; j++) {
        formula = formula.replace("{{" + j + "}}", titleArray[j]);
      }
      title = formula;
    }
    this.ObjectPage.find(".object-name").html(title);
    if (this.param.Nav) {
      this.param.Nav.find(".title").html(title);
    }
    if (headerImage) {
      this.ObjectPage.find(".object-picture").off("error");
      this.ObjectPage.find(".object-picture").on("error", function () {
        this.src = "images/nouser.jpg";
      });
      this.ObjectPage.find(".object-picture")
        .attr("src", headerImage)
        .removeClass("d-none");
    } else if (this.ObjectPage.find(".object-picture:visible").length > 0) {
      this.ObjectPage.find(".object-picture")
        .attr("src", "images/nouser.jpg")
        .addClass("d-none");
      if (
        this.PageData.ObjectName != undefined &&
        this.PageData.ObjectName == "User"
      )
        this.ObjectPage.find(".object-picture").removeClass("d-none");
    } else if (
      this.PageData.ObjectName != undefined &&
      this.PageData.ObjectName == "User"
    ) {
      this.ObjectPage.find(".object-picture").removeClass("d-none");
    }
  };

  this.ReloadData = function () {
    _common.Loading(true);
    _common.GetObjectData({
      pageName: this.PageData.PageName,
      _id: this._id,
      callback: function (result) {
        if (result.message == "success") {
          self.Data = result.data;
          self.ObjectPage.find(".page-detail").html("");
          self.Build();
          if (
            self.PageData.Events != undefined &&
            self.PageData.Events.AfterReloadData
          )
            eval(self.PageData.Events.AfterReloadData);
          _common.Loading(false);
        } else if (result.message == "deleted") {
          _common.Loading(false);
          self.ObjectPage.html(
            '<h1 class="info">This record has been deleted.</h1>'
          );
        } else {
          self.ObjectPage.html('<h1 class="info">' + result.message + "</h1>");
          _common.Loading(false);
        }
      },
    });
  };

  this.BulidTab = function () {
    if (this.PageData.Tabs != undefined) {
      this.ObjectPage.find(".main-panel .nav-tabs").append(
        '<a class="nav-item nav-link active" data-pagename="detail">Details</a>'
      );
      let tabPane =
        '<div class="tab-pane fade show active" data-pagename="detail"><div class="p-3 page-detail"></div></div>';
      for (let i = 0; i < this.PageData.Tabs.length; i++) {
        let tab = this.PageData.Tabs[i];
        let nav = $(
          '<a class="nav-item nav-link" data-pagename="' +
            tab.PageName +
            '">' +
            tab.DisplayName +
            "</a>"
        );
        nav[0].TabData = tab;
        this.ObjectPage.find(".main-panel .nav-tabs").append(nav);
        tabPane +=
          '<div class="tab-pane fade p-3" data-pagename="' +
          tab.PageName +
          '">' +
          tab.DisplayName +
          "</div>";
      }
      this.ObjectPage.find(".main-panel .tab-content").append(tabPane);

      this.ObjectPage.find(".main-panel .nav-tabs .nav-item").click(
        function () {
          self.ObjectPage.find(".main-panel .nav-tabs .nav-item").removeClass(
            "active"
          );
          self.ObjectPage.find(
            ".main-panel .tab-content .tab-pane"
          ).removeClass("active show");
          $(this).addClass("active");
          $(this)
            .closest(".main-panel")
            .find('.tab-pane[data-pagename="' + $(this).data("pagename") + '"]')
            .addClass("active show");
          new PageTab(self, $(this));
        }
      );
    } else {
      this.ObjectPage.find(".main-panel").html(
        '<div class="p-3 page-detail"></div>'
      );
    }
  };
  this.RecBuildCollection = function (param) {
    let field = param.field;
    let fieldRecord = param.fieldRecord;
    let mainData = param.MainData;
    field._id = fieldRecord._id;
    if (param.level == undefined) {
      param.level = 1;
    }
    if (field.Filter == undefined) {
      field.Filter = [];
    }
    if (field.ObjectChain == undefined) {
      field.ObjectChain = field.Name + ".$[el" + param.level + "]";
    }
    if (field.Collections == undefined) {
      let col = $.extend({}, field.Schema);
      col.Fields = undefined;
      field.Collections = [col];
    }
    let result = $(
      '<div class="msp-view-collection"><div><button class="btn btn-sm btn-outline-primary mb-2 btn-add-' +
        param.level +
        '" data-restriction="Create">Add ' +
        field.Schema.DisplayName +
        "</button></div>"
    );
    let table = $('<table class="table table-bordered"></table>');
    let trHead = $("<tr></tr>");
    for (let i = 0; i < field.Schema.Fields.length; i++) {
      trHead.append("<th>" + field.Schema.Fields[i].DisplayName + "</th>");
    }
    trHead.append("<td></td>");
    table.append(trHead);
    _page.GetCollectionData(field, fieldRecord, function () {
      for (let i = 0; i < fieldRecord.length; i++) {
        let tr = $("<tr></tr>");
        fieldRecord[i].Idetifier = $.extend({}, field.Idetifier);
        fieldRecord[i].Filter = field.Filter.slice();
        fieldRecord[i].Filter.push({
          ["el" + param.level + "._id"]: { $oid: fieldRecord[i]._id },
        });
        for (let j = 0; j < field.Schema.Fields.length; j++) {
          var childField = $.extend({}, field.Schema.Fields[j]);
          childField.Schema = field.Schema.Fields[j];
          childField.Parent = field;
          childField.ObjectChain =
            field.ObjectChain +
            "." +
            childField.Name +
            ".$[el" +
            (param.level + 1) +
            "]";
          childField.Filter = fieldRecord[i].Filter;
          childField.ObjectName = field.ObjectName;
          childField.Idetifier = fieldRecord[i].Idetifier;
          childField.Collections = field.Collections.slice();

          if (childField.Schema.UIDataType == "collection") {
            let cparam = {};
            cparam.field = childField;
            cparam.fieldRecord = fieldRecord[i][childField.Name];
            cparam.level = param.level + 1;
            cparam.MainData = mainData;
            childField.Collections.push($.extend({}, childField.Schema));
            $("<td></td>").append(self.RecBuildCollection(cparam)).appendTo(tr);
          } else {
            let style = "";
            if (childField.Width) {
              style =
                "style='width:" +
                childField.Width +
                ";overflow: hidden; text-overflow: ellipsis;white-space: nowrap;'";
            }
            tr.append(
              "<td><div " +
                style +
                ">" +
                _controls.GetDisplayValue({
                  field: childField,
                  data: fieldRecord[i],
                }) +
                "</div></td>"
            );
          }
        }
        let tdButtons = $(
          '<td><button class="btn btn-sm btn-outline-primary m-1 btn-edit-' +
            param.level +
            '" data-restriction="Modify"><i class="fa fa-edit"></i></button><button class="btn-delete-' +
            param.level +
            ' btn btn-sm btn-outline-danger ml-1 hide-edit" data-restriction="Modify"><i class="fas fa-trash-alt"></i></button></td>'
        );
        tdButtons.find(".btn-edit-" + param.level)[0].data = fieldRecord[i];
        tdButtons.find(".btn-delete-" + param.level)[0].data = fieldRecord[i];
        tr.append(tdButtons);
        table.append(tr);
      }
    });

    result.append(table);
    result.find(".btn-add-" + param.level).click(function () {
      let param = {};
      let addField = $.extend({}, field);
      for (let i = 0; i < addField.Schema.Fields.length; i++) {
        addField.Schema.Fields[i].Schema = $.extend(
          {},
          addField.Schema.Fields[i]
        );
      }
      param.PageData = {};
      param.Operation = "add";
      param.PageData.ObjectName = field.ObjectName;
      if (field.Parent == undefined) {
        param.PageData.ObjectChain = field.Name;
      } else {
        param.PageData.ObjectChain =
          field.Parent.ObjectChain + "." + field.Name;
      }
      param.PageData.Idetifier = field.Idetifier;
      param.PageData.Filter = field.Filter;
      param.PageData.Fields = addField.Schema.Fields;
      param.PageData.DisplayName = field.Schema.DisplayName;
      param.PageData.Collections = field.Collections;
      param.PageData.Events = field.Schema.Events;
      param.PageData.FieldName = field.Name; //To Be Verified
      param.PageData.MainData = mainData;
      param.onSave = function () {
        self.ReloadData();
      };
      PageForm(param);
    });
    result.on("click", ".btn-edit-" + param.level, function () {
      let param = {};
      let addField = $.extend({}, field);
      for (let i = 0; i < addField.Schema.Fields.length; i++) {
        addField.Schema.Fields[i].Schema = $.extend(
          {},
          addField.Schema.Fields[i]
        );
      }
      param.PageData = {};
      param.Operation = "edit";
      param.PageData.ObjectName = field.ObjectName;
      param.PageData.ObjectChain = field.ObjectChain;
      param.PageData.Idetifier = this.data.Idetifier;
      param.PageData.Filter = this.data.Filter;
      param.PageData._id = this.data._id;
      param.PageData.Collections = field.Collections;
      param.PageData.Fields = addField.Schema.Fields;
      param.PageData.DisplayName = field.Schema.DisplayName;
      param.PageData.Events = field.Schema.Events;
      param.Data = this.data;
      param.PageData.MainData = mainData;
      param.PageData.FieldName = field.Name; //To Be Verified
      param.RemoveMinimize = true;
      param.onSave = function () {
        self.ReloadData();
      };
      PageForm(param);
    });
    result.on("click", ".btn-delete-" + param.level, function () {
      let sdata = { objectname: field.ObjectName };
      sdata.operation = "del";
      sdata.idetifier = field.Idetifier;
      sdata.FieldName = field.Name; //To Be Verified
      sdata.filter = field.Filter;

      if (field.Parent == undefined) {
        sdata.objectChain = field.Name;
      } else {
        sdata.objectChain = field.Parent.ObjectChain + "." + field.Name;
      }
      sdata._id = this.data._id;
      swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover data!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((deleteResult) => {
        if (deleteResult) {
          //Delete parent field using Custom.js
          // if (field.Schema.Events != undefined && field.Schema.Events.BeforeCollectionDelete != undefined) {
          //     eval(field.Schema.Events.BeforeCollectionDelete);
          //     if (result[0].ParentFields) {
          //         sdata.ParentFields = result[0].ParentFields;
          //     }
          // }
          _common.UpdateEmbededObjectData(sdata, function (result) {
            if (result.message == "success") {
              self.ReloadData();
              swal("Record deleted successfully.", { icon: "success" });
            } else {
              swal(result.message, { icon: "error" });
            }
          });
        }
      });
    });
    return result;
  };

  _common.Loading(true);
  _common.GetStaticPage("/page/detail.html", function (pageresult) {
    self.ObjectPage = $(pageresult);
    self.ObjectPage[0].obj = self;
    param.PageContainer.html("").append(self.ObjectPage);
    _common.GetObjectData({
      pageName: param.Data.PageName,
      _id: _id,
      callback: function (result) {
        if (result.message == "success") {
          if (result.pagedata.Fields != undefined)
            result.pagedata.Fields.sort(function (a, b) {
              return a.Sequence - b.Sequence;
            });
          if (result.pagedata.Groups != undefined)
            result.pagedata.Groups.sort(function (a, b) {
              return a.Sequence - b.Sequence;
            });
          self.PageData = result.pagedata;
          self.Data = result.data;
          self.BulidTab();
          self.Build();

          if (self.PageData.Events != undefined && self.PageData.Events.Init)
            eval(self.PageData.Events.Init);

          if (self.PageData.PageButtons != undefined) {
            for (let i = 0; i < self.PageData.PageButtons.length; i++) {
              let obj = self.PageData.PageButtons[i];
              let hasfn =
                obj.Function != undefined ? "data-fn=" + obj.Function : "";
              let btn =
                '<button class="' +
                obj.Class +
                '" ' +
                hasfn +
                ">" +
                obj.DisplayName +
                "</button>";
              self.ObjectPage.find(".page-top-button").prepend(btn);
            }
            self.ObjectPage.find(".page-top-button button[data-fn]").click(
              function () {
                eval($(this).data("fn"));
              }
            );
          }

          if (self.PageData.Tiles != undefined) {
            for (let i = 0; i < self.PageData.Tiles.length; i++) {
              let obj = self.PageData.Tiles[i];
              let tilesContainer = $(
                '<div class="col-xl-3 col-md-6 mb-2" snippet-name="' +
                  obj.Name +
                  '"><div class="card border-left-' +
                  obj.Class +
                  ' h-100"><div class="card-body"><div class="row no-gutters align-items-center"><div class="col mr-2"><div class="text-xs font-weight-bold text-' +
                  obj.Class +
                  ' text-uppercase mb-1">' +
                  obj.DisplayName +
                  '</div><div class="h5 mb-0 font-weight-bold text-gray-800 result"></div></div><div class="col-auto"><i class="' +
                  obj.Icon +
                  ' fa-2x text-gray-300"></i></div></div></div></div></div>'
              );
              tilesContainer[0].obj = obj;
              tilesContainer[0].ObjectID = self._id;
              self.ObjectPage.find(".tiles").append(tilesContainer);
              if (obj.Type == "GetCount") {
                let param = { name: obj.Name };
                param.match = {};
                if (obj.MatchFields) {
                  for (let i = 0; i < obj.MatchFields.length; i++) {
                    let fld = obj.MatchFields[i];
                    let val;
                    if (fld.ValueFrom != undefined) {
                      val = self.Data[fld.ValueFrom];
                    } else if (fld.Value != undefined && fld.Value != "") {
                      val = fld.Value;
                    }
                    if (fld.UIDataType != undefined) {
                      val = _controls.ToEJSON(fld.UIDataType, val);
                    }
                    param.match[fld.Name] = val;
                  }
                }

                if (["Document", "Note"].includes(obj.Name)) {
                  param.match["ObjectSchemaName"] = self.PageData.ObjectName;
                }
                param.callback = function (result) {
                  tilesContainer.find(".result").html(result);
                };
                _common.ObjectCount(param);
              }
            }
          }

          self.ObjectPage.find(".btn-edit-object").click(function () {
            let param = {};
            param.pageName = self.PageData.PageName;
            param._id = _id;
            param.onSave = function () {
              self.ReloadData();
            };
            new PageForm(param);
          });
          _common.Loading(false);
          _userprofile.Auth({
            pageData: self.PageData,
            container: self.ObjectPage,
          });
        } else if (result.message == "deleted") {
          _common.Loading(false);
          self.ObjectPage.html(
            '<h1 class="info">This record has been deleted.</h1>'
          );
        } else {
          self.ObjectPage.html('<h1 class="info">' + result.message + "</h1>");
          _common.Loading(false);
        }
      },
    });
  });
}

function StaticPage(page) {
  let url = "/page/" + page.Data.PageName + ".html";
  if (page.Data.PageURL != undefined) url = page.Data.PageURL;

  _common.GetStaticPage(url, function (pageHTML) {
    page.PageContainer.html(pageHTML);
    _userprofile.Auth({ pageData: page.Data, container: page.PageContainer });
    if (page.Data.Events != undefined && page.Data.Events.Init)
      eval(page.Data.Events.Init);
  });
}

function PageTab(pageDetailObj, tab) {
  let pageName = tab.data("pagename");
  if (tab[0].PageTab != undefined || pageName == "detail") {
    return;
  }

  this.TabData = tab[0].TabData;
  this.PageDetail = pageDetailObj;
  if (
    this.TabData != undefined &&
    this.TabData.MatchFields != undefined &&
    this.TabData.MatchFields.length > 0
  ) {
    this.MatchFields = [];
    for (let i = 0; i < this.TabData.MatchFields.length; i++) {
      let fld = this.TabData.MatchFields[i];
      if (fld.ValueFrom != undefined) {
        fld["Value"] = pageDetailObj.Data[fld.ValueFrom];
        this.MatchFields.push(fld);
      } else if (fld.Value != undefined) this.MatchFields.push(fld);
    }
  }

  this.Build = function () {
    this.Data = _common.GetPageDetailFromInitData(pageName);
    let pageContainer = pageDetailObj.ObjectPage.find(
      '.main-panel .tab-content .tab-pane[data-pagename="' + pageName + '"]'
    ).html("");
    this.Tab = tab;
    this.PageContainer = pageContainer;
    switch (this.Data.Type) {
      case "ObjectGrid":
        new DataGrid(this);
        break;
      case "ObjectDetail":
        new PageDetail(this, pageDetailObj._id);
        break;
      case "Static":
        new StaticPage(this);
        break;
    }
    tab[0].PageTab = this;
    pageContainer[0].PageTab = this;
  };
  this.Build();
}

function PageForm(param) {
  let self = this;
  this.PageName = param.pageName;
  this._id = param._id;
  this.PageData = undefined;
  this.Data = undefined;
  this.OnSave = param.onSave;
  this.ParamData = param;
  this.AllFields = [];

  let identifer = this.PageName;
  let minimiseHtml =
    '<button class="btn btn-sm bg-white ml-auto mr-0 modal-minimise" data-toggle="tooltip" data-placement="bottom" title="Minimize" value="Minimize"><i class="fas fa-minus"></i></button>';
  if (param.RemoveMinimize) {
    minimiseHtml = "";
  }
  if (param._id != undefined) identifer += ":" + param._id;
  if (param.formClass != undefined) identifer += ":" + param.formClass;
  let snav = $('.modal.page-form[data-uid="' + identifer + '"]');
  if (snav.length > 0) {
    if (snav.hasClass("minimized")) {
      snav[0].minSnippit.remove();
      snav.removeClass("minimized");
      snav.modal("show");
      return;
    } else {
      snav.remove();
    }
  }

  _common.Loading(true);
  let formModal = $(
    '<div  class="modal fade page-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true" data-uid="' +
      identifer +
      '">' +
      '<div class="modal-dialog modal-lg" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<h5 class="modal-title"></h5>' +
      minimiseHtml +
      '<button type="button" class="close ml-n1" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
      "</div>" +
      '<div class="modal-body"></div>' +
      '<div class="modal-footer">' +
      '<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>' +
      '<button type="button" class="btn btn-primary btnSaveObject">Save</button>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>"
  );
  formModal.modal("show");
  formModal.on("hidden.bs.modal", function () {
    if (!formModal.hasClass("minimized")) formModal.remove();
  });

  if (param.formClass != undefined) {
    formModal.addClass(param.formClass);
    if (param.formClass.indexOf("secondary-form") >= 0) {
      formModal.find(".modal-header .modal-minimise").remove();
    }
  }

  if (this._id != undefined) {
    formModal.addClass("edit");
    formModal.find(".btnSaveObject").attr("data-restriction", "Modify");
  } else {
    formModal.addClass("add");
    formModal.find(".btnSaveObject").attr("data-restriction", "Create");
  }

  formModal[0].PageForm = self;
  this.PageContainer = formModal;

  this.Build = function () {
    let title = "";
    if (this.PageData.Fields != undefined && this.PageData.Fields.length > 0) {
      this.PageData.Fields.sort(function (a, b) {
        return a.Sequence - b.Sequence;
      });
      let frm = formModal
        .find(".modal-body")
        .append('<div class="row"></div>')
        .find(".row");
      for (let i = 0; i < this.PageData.Fields.length; i++) {
        let field = this.PageData.Fields[i];
        this.AllFields.push(field);
        if (field.Schema == undefined || field.Schema.UIDataType == "formula")
          continue;
        if (this._id != undefined && field.Schema.UIDataType == "collection")
          continue;
        if (this._id == undefined && field.Schema.UIDataType == "autoincrement")
          continue;

        let hideON = field.Schema.HideON;
        if (field.HideON != undefined) hideON = field.HideON;
        if (hideON !== undefined) {
          if (
            hideON == "both" ||
            (formModal.hasClass("edit") && hideON == "edit") ||
            (formModal.hasClass("add") && hideON == "add")
          )
            continue;
        }

        let readOnly = param.readOnly;
        if (this.ProfileData && this.ProfileData.Fields) {
          let fldObj = this.ProfileData.Fields.find(
            (x) => x.Name == field.Name
          );
          if (fldObj && fldObj.ReadOnly) readOnly = true;
        }
        _page.BuildField({
          field: field,
          container: frm,
          data: this.Data,
          readOnly: readOnly,
          pageData: this.PageData,
        });
        if (field.IsPageTitle) {
          let val = _controls.GetDisplayValue({
            field: field,
            data: this.Data,
          });
          title += val + " ";
        }
      }
    }

    if (this.PageData.Groups != undefined) {
      for (let g = 0; g < this.PageData.Groups.length; g++) {
        let group = this.PageData.Groups[g];
        group.Fields.sort(function (a, b) {
          return a.Sequence - b.Sequence;
        });
        let pageGroup = $(
          '<div><h6 class="mb-2 mt-3">' +
            group.DisplayName +
            '</h6><div class="row page-group"></div></div>'
        );
        if (group.IsHideBox) {
          pageGroup.find("h6").remove();
        }
        for (let i = 0; i < group.Fields.length; i++) {
          let field = group.Fields[i];
          this.AllFields.push(field);
          if (field.Schema == undefined || field.Schema.UIDataType == "formula")
            continue;
          if (this._id != undefined && field.Schema.UIDataType == "collection")
            continue;
          if (
            this._id == undefined &&
            field.Schema.UIDataType == "autoincrement"
          )
            continue;

          let readOnly = param.readOnly;
          if (this.ProfileData && this.ProfileData.Fields) {
            let fldObj = this.ProfileData.Fields.find(
              (x) => x.Name == field.Name
            );
            if (fldObj && fldObj.ReadOnly) readOnly = true;
          }
          _page.BuildField({
            field: field,
            container: pageGroup.find(".page-group"),
            data: this.Data,
            readOnly: readOnly,
            pageData: this.PageData,
            groupData: group,
          });
          if (field.IsPageTitle) {
            let val = _controls.GetDisplayValue({
              field: field,
              data: this.Data,
            });
            title += val + " ";
          }
        }
        formModal.find(".modal-body").append(pageGroup);
      }
    }
    if (
      param.MatchFields == undefined &&
      this.PageData.MatchFields != undefined
    )
      param.MatchFields = this.PageData.MatchFields;
    else if (
      param.MatchFields != undefined &&
      this.PageData.MatchFields != undefined
    )
      param.MatchFields = param.MatchFields.concat(this.PageData.MatchFields);

    if (this._id == undefined && param.MatchFields != undefined) {
      for (let i = 0; i < param.MatchFields.length; i++) {
        let matchfld = param.MatchFields[i];
        let ctrl = formModal.find('[data-field="' + matchfld.Name + '"]');
        if (ctrl.length > 0) {
          let field = ctrl[0].field;
          if (field.Schema.UIDataType == "lookup") {
            _common.GetLookupData(
              field.ObjectName,
              field.Name,
              matchfld.Value,
              function (data) {
                let cHtm =
                  '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' +
                  data._id +
                  '">' +
                  data.name +
                  '<i class="fas fa-times"></i></div>';
                ctrl.html(cHtm);
              }
            );
          } else ctrl.val(matchfld.Value);
        }
        if (matchfld.Name == "ObjectSchemaName")
          self.ObjectValidate = matchfld.Value;
      }
    }

    if (self.PageData.FormButtons != undefined) {
      for (let i = 0; i < self.PageData.FormButtons.length; i++) {
        let obj = self.PageData.FormButtons[i];
        let hasfn = obj.Function != undefined ? "data-fn=" + obj.Function : "";
        let btn =
          '<button class="' +
          obj.Class +
          '" ' +
          hasfn +
          ">" +
          obj.DisplayName +
          "</button>";
        if (obj.DisplayName == "Delete") {
          if (this._id != undefined)
            self.PageContainer.find(".modal-footer").append(btn);
        } else self.PageContainer.find(".modal-footer").append(btn);
      }
      self.PageContainer.find(".modal-footer button[data-fn]").click(
        function () {
          eval($(this).data("fn"));
        }
      );
    }

    if (param.PageTitle != undefined) {
      formModal.find(".modal-title").html(param.PageTitle);
    } else {
      if (this.Data == undefined) {
        formModal.find(".modal-title").html("New " + this.PageData.DisplayName);
        let ctrlOwner = formModal.find('[data-field="OwnerID"]');
        let ownerSchema =
          this.PageData.ObjectSchema && this.PageData.ObjectSchema.Fields
            ? this.PageData.ObjectSchema.Fields.filter(
                (p) => p.Name == "OwnerID"
              )
            : undefined;
        let isOwnerEdit =
          ownerSchema && ownerSchema[0] && ownerSchema[0].IsOwnerEdit
            ? ownerSchema[0].IsOwnerEdit
            : false;
        if (
          ctrlOwner != undefined &&
          isOwnerEdit != undefined &&
          !isOwnerEdit
        ) {
          ctrlOwner
            .closest(".form-group")
            .append(
              "<div>" +
                _initData.User.FirstName +
                " " +
                _initData.User.LastName +
                "</div>"
            );
          ctrlOwner.closest(".input-group").remove();
        }
      } else {
        formModal.find(".modal-title").html("Edit " + title);
      }
    }

    if (
      self.PageData.Events != undefined &&
      self.PageData.Events.InitForm != undefined
    ) {
      eval(self.PageData.Events.InitForm);
    }

    if (param.readOnly != undefined && param.readOnly == true) {
      formModal.find(".modal-footer .btnSaveObject").remove();
    }

    if (this.PageData.ReadOnly) {
      formModal.find(".modal-footer").remove();
      formModal.find(".modal-title").html("Details of " + title);
    }

    if (self.ObjectValidate != undefined) {
      let pageData = {
        ObjectName: self.ObjectValidate,
        PageName: self.PageData.PageName,
      };
      _userprofile.Auth({ pageData: pageData, container: formModal });
    } else _userprofile.Auth({ pageData: self.PageData, container: formModal });
  };
  // For Embedded Forms Start Manish 10 Oct 19
  if (param.PageData) {
    self.PageData = param.PageData;
    if (param.Data) {
      self.Data = param.Data;
    }
    self.Build();
    _common.Loading(false);
    formModal.find(".btnSaveObject").click(function () {
      let btnSave = $(this);
      if (formModal.find("[data-field]").length == 0) return false;
      if (this.onSave) {
        this.onSave();
      }
      _common.Loading(false);
      if (_common.Validate(formModal)) {
        let sdata = { objectname: self.PageData.ObjectName };
        sdata.operation = param.Operation;
        sdata.idetifier = PageData.Idetifier;
        sdata.objectChain = PageData.ObjectChain;
        sdata.FieldName = PageData.FieldName; //To be verified
        sdata.filter = PageData.Filter;
        sdata.fields = {};
        if (PageData._id != undefined) sdata._id = PageData._id;

        if (
          self.PageData.Events != undefined &&
          self.PageData.Events.FormValidation != undefined
        ) {
          if (eval(self.PageData.Events.FormValidation) == false) {
            if (self.PageContainer.find(".has-error").length > 0) {
              self.PageContainer.find(".has-error:first")[0].scrollIntoView({
                behavior: "smooth",
                block: "end",
                inline: "nearest",
              });
            }
            _common.Loading(false);
            return false;
          }
        }
        if (self.PageData.Fields != undefined) {
          for (let i = 0; i < self.PageData.Fields.length; i++) {
            let field = self.PageData.Fields[i];
            if (field.Schema == undefined) continue;

            let val = _controls.GetCtrlValue({
              field: field,
              container: formModal,
              isEjsonFormat: true,
            });
            _page.CheckFieldValidation(
              field,
              formModal,
              self.PageData.ObjectName,
              sdata._id
            );
            sdata.fields[field.Name] = val;
          }
        }
        //Update parent field using Custom.js
        // if (self.PageData.Events != undefined && self.PageData.Events.BeforeCollectionUpdate != undefined) {
        //     eval(self.PageData.Events.BeforeCollectionUpdate);
        //     if (btnSave[0].ParentFields) {
        //         sdata.ParentFields = btnSave[0].ParentFields;
        //     }
        // }
        //if(sdata._id !=undefined){
        _common.UpdateEmbededObjectData(sdata, function (result) {
          if (result.message == "success") {
            _common.Loading(false);
            if (btnSave.attr("data-exit") == "false") {
              return;
            }
            formModal.modal("hide");
            if (param.onSave) {
              param.onSave();
            }
            swal("Record saved successfully.", { icon: "success" });
          } else {
            _common.Loading(false);
            swal(result.message, { icon: "error" });
          }
        });
        // }
      }
    });
    return;
  }
  // For Embedded Forms End Manish 10 Oct 19
  if (this._id != undefined) {
    _common.GetObjectData({
      pageName: this.PageName,
      _id: this._id,
      callback: function (result) {
        if (result.message == "success") {
          self.Data = result.data;
          if (self.Data["ObjectSchemaName"] !== undefined)
            self.ObjectValidate = self.Data["ObjectSchemaName"];

          if (result.pagedata.Groups != undefined) {
            result.pagedata.Groups.sort(function (a, b) {
              return a.Sequence - b.Sequence;
            });
            //result.pagedata.Groups.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
          }
          if (result.pagedata.Fields != undefined)
            result.pagedata.Fields.sort(function (a, b) {
              return a.Sequence - b.Sequence;
            });

          self.PageData = result.pagedata;
          self.ProfileData = result.profiledata;
          self.Build();
          _common.Loading(false);
        } else if (result.message == "deleted") {
          _common.Loading(false);
          formModal.find(".modal-header .modal-minimise").remove();
          formModal
            .find(".modal-header .modal-title")
            .html("<b>Oops..</b> Missing Record");
          formModal
            .find(".modal-body")
            .html(
              '<h5 class="info">This record does not exist or has been deleted.</h5>'
            );
          formModal
            .find(".modal-footer")
            .html(
              '<button type="button" data-dismiss="modal" class="btn btn-primary">Ok</button>'
            );
        } else {
          formModal
            .find(".modal-body")
            .html('<h1 class="info">' + result.message + "</h1>");
          formModal.find(".modal-footer").remove();
          _common.Loading(false);
        }
      },
    });
  } else {
    _common.GetPageDetail(this.PageName, function (result) {
      if (result.Groups != undefined) {
        result.Groups.sort(function (a, b) {
          return a.Sequence - b.Sequence;
        });
        //result.Groups.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
      }
      if (result.Fields != undefined)
        result.Fields.sort(function (a, b) {
          return a.Sequence - b.Sequence;
        });

      if (param.Data) {
        self.Data = param.Data;
      }
      self.PageData = result;
      self.Build();
      _common.Loading(false);
    });
  }

  formModal.find(".btnSaveObject").click(async function () {
    let fileArray = [];
    _common.Loading(true);
    if (_common.Validate(formModal)) {
      if (self.PageData.ObjectName && self.PageData.ObjectName == "User") {
        if (!_user.CheckDuplicateEmailInMaster(self)) {
          _common.Loading(false);
          return;
        }
      }
      let sdata = { pageName: self.PageData.PageName, fields: {} };
      if (self._id != undefined) sdata._id = self._id;

      if (
        self.PageData.Events != undefined &&
        self.PageData.Events.FormValidation != undefined
      ) {
        if (eval(self.PageData.Events.FormValidation) == false) {
          if (self.PageContainer.find(".has-error").length > 0) {
            self.PageContainer.find(".has-error:first")[0].scrollIntoView({
              behavior: "smooth",
              block: "end",
              inline: "nearest",
            });
          }
          _common.Loading(false);
          return false;
        }
      }
      if (self.PageData.Fields != undefined) {
        for (let i = 0; i < self.PageData.Fields.length; i++) {
          let field = self.PageData.Fields[i];
          if (field.Schema == undefined) continue;
          if (self._id != undefined && field.Schema.UIDataType == "collection")
            continue;

          let val = _controls.GetCtrlValue({
            field: field,
            container: formModal,
            isEjsonFormat: true,
          });
          if (field.Schema.UIDataType == "file" && val != null) {
            fileArray.push(val);
          }
          _page.CheckFieldValidation(
            field,
            formModal,
            self.PageData.ObjectName,
            sdata._id
          );
          sdata.fields[field.Name] = val;
        }
      }

      if (self.PageData.Groups != undefined) {
        for (let g = 0; g < self.PageData.Groups.length; g++) {
          let group = self.PageData.Groups[g];

          for (let i = 0; i < group.Fields.length; i++) {
            let field = group.Fields[i];
            if (field.Schema == undefined) continue;
            if (
              self._id != undefined &&
              field.Schema.UIDataType == "collection"
            )
              continue;

            let val = _controls.GetCtrlValue({
              field: field,
              container: formModal,
              isEjsonFormat: true,
            });
            if (field.Schema.UIDataType == "file" && val != null) {
              fileArray.push(val);
            }
            _page.CheckFieldValidation(
              field,
              formModal,
              self.PageData.ObjectName,
              sdata._id
            );
            sdata.fields[field.Name] = val;
          }
        }
      }
      if (self.PageContainer.find(".has-error").length > 0) {
        _common.Loading(false);
        self.PageContainer.find(".has-error:first")[0].scrollIntoView({
          behavior: "smooth",
          block: "end",
          inline: "nearest",
        });
        return false;
      }
      sdata.Files = fileArray;
      if (self._id != undefined) {
        _common.UpdateObjectData(sdata, function (result) {
          if (result.message == "success") {
            self.ResultData = result;
            if (
              self.PageData.Events != undefined &&
              self.PageData.Events.Update
            ) {
              eval(self.PageData.Events.Update);
            }

            let fileids = [];
            formModal.find(".msp-ctrl-file, .msp-ctrl-image").each(function () {
              let fieldProperties = $(this)[0].field;
              let fieldProp = {};
              fieldProp.ObjectName = fieldProperties.ObjectName;
              fieldProp.ObjectFieldName = fieldProperties.Name;
              if ($(this).hasClass("msp-ctrl-image"))
                fieldProp.ObjectDocumentID = $(this).attr("data-imageid") || "";
              else
                fieldProp.ObjectDocumentID =
                  $(this).next().find(".file-data").data("fileid") || "";
              fileids.push(fieldProp);
            });
            if (fileids.length > 0) {
              _common.AfterSaveDriveUpdate(sdata._id, fileids, "Update");
            }

            _common.Loading(false);
            formModal.modal("hide");
            swal("Record saved successfully.", { icon: "success" });
            if (self.OnSave != undefined) {
              self.OnSave(self);
            }
          } else {
            _common.Loading(false);
            swal(result.message, { icon: "error" });
          }
        });
      } else {
        if (
          self.PageData.Events != undefined &&
          self.PageData.Events.BeforeAdd
        ) {
          try {
            let tempResponse = {};
            tempResponse = await eval(self.PageData.Events.BeforeAdd);
            if (
              tempResponse &&
              tempResponse.message &&
              tempResponse.message != "success"
            ) {
              _common.Loading(false);
              swal(tempResponse.message, { icon: "info" });
              return;
            }
          } catch (ex) {
            console.log(ex);
          }
        }
        // Add parent field using Custom.js
        // if (self.PageData.Events != undefined && self.PageData.Events.BeforeCollectionAdd != undefined) {
        //     eval(self.PageData.Events.BeforeCollectionAdd);
        // }
        _common.AddObjectData(sdata, function (result) {
          if (result.message == "success") {
            self.ResultData = result;
            if (self.PageData.Events != undefined && self.PageData.Events.Add) {
              eval(self.PageData.Events.Add);
            }

            let fileids = [];
            formModal.find(".msp-ctrl-file, .msp-ctrl-image").each(function () {
              let fieldProperties = $(this)[0].field;
              let fieldProp = {};
              fieldProp.ObjectName = fieldProperties.ObjectName;
              fieldProp.ObjectFieldName = fieldProperties.Name;
              if ($(this).hasClass("msp-ctrl-image"))
                fieldProp.ObjectDocumentID = $(this).attr("data-imageid") || "";
              else
                fieldProp.ObjectDocumentID =
                  $(this).next().find(".file-data").data("fileid") || "";
              fileids.push(fieldProp);
            });
            if (fileids.length > 0) {
              _common.AfterSaveDriveUpdate(result.data._id, fileids, "Add");
            }

            _common.Loading(false);
            formModal.modal("hide");
            swal("Record added successfully.", { icon: "success" });
            if (self.OnSave != undefined) {
              self.OnSave(self);
            }
          } else {
            _common.Loading(false);
            swal(result.message, { icon: "error" });
          }
        });
      }
    } else {
      _common.Loading(false);
      self.PageContainer.find(".has-error:first")[0].scrollIntoView({
        behavior: "smooth",
        block: "end",
        inline: "nearest",
      });
    }
  });
}

function PageTable(data, pageData, container) {
  let self = this;
  this.Data = data;
  this.PageData = pageData;
  this.Table = $(
    '<table class="table table-bordered page-table"><thead></thead><tbody></tbody></table>'
  );
  this.BuildHead = function () {
    let row = "<tr>";
    for (let i = 0; i < this.PageData.Fields.length; i++) {
      let field = this.PageData.Fields[i];
      if (field.Schema == undefined || field.IsHide) {
        continue;
      }
      row +=
        '<th data-field="' +
        field.Name +
        '">' +
        _page.GetFieldDisplayName(field) +
        "</th>";
    }
    row += "</tr>";
    this.Table.find("thead").html(row);
  };

  this.BuildBody = function () {
    if (this.Data.length > 0) {
      for (let i = 0; i < this.Data.length; i++) {
        let record = this.Data[i];
        let row = $("<tr></tr>");
        row[0].record = record;
        for (let c = 0; c < this.PageData.Fields.length; c++) {
          let field = this.PageData.Fields[c];
          if (field.Schema == undefined || field.IsHide) {
            continue;
          }

          let val = _controls.GetDisplayValue({ field: field, data: record });
          let td = $(
            '<td data-field="' +
              field.Name +
              '" data-type="' +
              field.Schema.UIDataType +
              '"><div class="cell-disp">' +
              val +
              "</div></td>"
          );
          td[0].field = field;
          if (field.Schema.UIDataType == "file") {
            let filedatactrl = ctrl.find(".div-file-uploading .file-data");
            if (filedatactrl.length > 0) {
              td.find(".cell-disp").append(
                '<a  href="/api/drive/downloadfilelocal/' +
                  filedatactrl.data("fileid") +
                  "&" +
                  filedatactrl.data("filename") +
                  '"><i class="fa fa-download"></i></a>'
              );
            }
          }
          row.append(td);
        }
        this.Table.find("tbody").append(row);
      }
    }
  };

  this.BuildHead();
  this.BuildBody();
  container.html("").append(this.Table);
  this.Table.find("tbody tr").dblclick(function (e) {
    e.stopPropagation();
    if (self.PageData.ChildPage != undefined)
      new Page({
        pageName: self.PageData.ChildPage,
        _id: $(this)[0].record._id,
      });
  });
}

function DirectPage(param) {
  let self = this;
  this.param = param;

  this.Build = function () {
    _page.body.find(".page-content").removeClass("active");
    let identifer = this.param.PageName;
    if (param._id != undefined) identifer += ":" + param._id;
    let snav = _page.nav.find('.nav-item[data-uid="' + identifer + '"]');
    if (snav.length > 0) {
      _page.nav.find(".nav-item").removeClass("active");
      snav.addClass("active");
      snav[0].Page.PageContainer.addClass("active");
      return;
    }

    let nav = $(
      '<span class="nav-item active" data-uid="' +
        identifer +
        '"><span class="title">' +
        this.param.PageTabName +
        '</span><span class="tab-close">X</span></span>'
    );
    let pageContainer = $(
      '<div class="page-content active" data-uid="' + identifer + '"></div>'
    );
    _page.nav.find(".nav-item").removeClass("active");
    nav.appendTo(_page.nav);
    pageContainer.appendTo(_page.content);
    this.Nav = nav;
    this.PageContainer = pageContainer;
    nav[0].Page = this;
    pageContainer[0].Page = this;
    pageContainer.appendTo(_page.body);
  };

  this.Build();
}
