
const gallery = {};
gallery.PageSize = 10;
gallery.MARKETING_MEDIA_LIBRARY_HTML = '<div class="m_lib">\
<div class="m_lib_hdr bg-white shadow row p-3"><h2 class="text-primary mb-0">Content Studio</h2>\
<div class="ml-auto">\
<span class="btnDir btn btn-outline-primary mr-2" data-restriction="Create" style="position:relative">New Folder</span>\
<span class="btnUpload btn btn-primary mr-2" data-restriction="Create" style="position:relative">Upload<form action="/" method="post" enctype="multipart/form-data">\
<input name="file"  class="flUpload" type="file" style="position:absolute;top: 0;left: 0; bottom: 0; right: 0; width: 100%;opacity: 0; cursor: pointer; z-index: 99;height: 100%"/></form></span>\
<span class="btnUploadURL btn btn-primary" style="position:relative; display:none;"><i class="fa fa-cloud-download"></i> Import from URL</span> <span class="btnClose btn btn-danger"><i class="fa fa-times"></i></span></div></div>\
<div class="m_lib_cnt row"><ul class="left-panel-tree nav flex-column flex-nowrap col pr-0"></ul><div class="right-panel-image col"></div>\
</div>\
</div>';

gallery.MARKETING_FOLDER_POPUP = '<div class="add_folder form-group">\
<label>Name </label>\
<input type="text" placeholder="Enter Folder Name" class="txtDir vdSpace form-control"/>\
</div>';

gallery.ImageToolBox = '<div class="img-tool"><a class="img" href="" target="_blank" style="border:none;">\
<i class="fa fa-search btn btn-sm btn-primary mb-1" title="Preview"></i></a> \
<i class="fa fa-check-square apply-image btn btn-sm btn-secondary mb-1" title="Select Image"></i> \
<i title="Edit" class="fa fa-edit edit-image btn btn-sm btn-secondary mb-1" data-restriction="Modify"></i> \
<i class="fa fa-trash del-image btn btn-sm btn-secondary mb-1" data-restriction="Delete" title="Delete Image" thumbImageID="" imgExt="" isParent="" data-id=""></i>\
</div>';

gallery.MediaGallery = function (obj) {
    gallery.prvObj = obj;
    let mediadModal = $(gallery.MARKETING_MEDIA_LIBRARY_HTML);
    _userprofile.Auth({ pageData: { "PageName": "CreateCampaign", "ObjectName": "MarketingCampaign" }, container: mediadModal });
    let ht = $(window).height() - 100;
    // mediadModal.find('.left-panel-tree').height(ht);
    mediadModal.find('.right-panel-image').height(ht);
    mediadModal.appendTo($('body'));
    mediadModal.find('.btnClose').on('click', function () {
        mediadModal.remove();
    })
    mediadModal.find('.btnDir').on('click', function () {
        if ($(this).find('Div').length == 0)
            gallery.CreateFolder(mediadModal);
    })
    mediadModal.find('.left-panel-tree').on('click', '.dir', function () {
        mediadModal.find('.left-panel-tree .fa.fa-trash.del-folder').remove();
        mediadModal.find('.left-panel-tree .ui-active').removeClass('ui-active');
        $(this).addClass('ui-active');
        if ($(this).attr('isDefault') != 1) {
            $(this).append('<i class="fa fa-trash del-folder ml-auto" title="Delete Folder" data-restriction="Delete" style="color:#FFFFFF; float:right;" data-id="1"></i>');
        }
        mediadModal.find('.del-folder').off('click');
        mediadModal.find('.del-folder').on('click', function () {
            let currentObj = $(this).parent('.dir').attr('data-dir');
            _common.ConfirmDelete({ message: 'are you sure to delete this folder.' }).then((confirm) => {
                if (confirm) {
                    let param = { 'url': '/api/marketing/DeleteMediaLibraryFolder', 'type': 'post', 'contentType': 'application/json' };
                    param.data = { '_id': currentObj };
                    _common.FetchApiByParam(param, function (result) {
                        if (result.statuscode == 200) {
                            swal('Folder deleted successfully.', { icon: "info", });
                            gallery.GetGalleryFolderList(mediadModal);
                        } else {
                            swal('Folder not deleted.', { icon: "info", });
                        }
                    })
                }
            })
        })
        mediadModal.folderID = mediadModal.find('.left-panel-tree .ui-active').attr('data-dir');
        if (mediadModal[mediadModal.folderID] == undefined)
            mediadModal[mediadModal.folderID] = {};
        _userprofile.Auth({ pageData: { "PageName": "CreateCampaign", "ObjectName": "MarketingCampaign" }, container: $(this) });
        gallery.InitializegalleryPaging(mediadModal);
    })
    gallery.GetGalleryFolderList(mediadModal);
    mediadModal.find('.m_lib_hdr').on('click', '.flUpload', function () {
        $(this).val('');
        //  gallery.UploadImageInFolder(mediadModal);
    })
    mediadModal.find('.m_lib_hdr').on('change', '.flUpload', function () {
        gallery.UploadImageInFolder(mediadModal);
    })
}

gallery.UploadImageInFolder = function (gallaryDiv) {
    let file = gallaryDiv.find('.flUpload')[0].files[0];
    if ((/\.(gif|jpg|jpeg|png)$/i).test(file.name)) {
        // let formData = new FormData(gallaryDiv.find('form')[0]);
        gallaryDiv.folderID = gallaryDiv.find('.left-panel-tree .ui-active').attr('data-dir');
        let reader = new FileReader();
        reader.onload = function (e) {
            let param = { 'url': '/api/drive/uploadfile', 'type': 'post', 'contentType': 'application/json' };
            let data = e.target.result;

            param.data = {
                'filename': file.name, 'size': file.size,
                'type': file.type, 'objectname': 'MediaImages',
                'FolderId': gallaryDiv.folderID, 'confirmAttachment': true,
                'data': data, 'galleryFolderID': gallaryDiv.folderID,
                'filetype': 'public'
            };
            _common.Loading(true)
            _common.UploadFiles(param, function (res) {
                if (res.fileid) {
                    let obj = {
                        fileName: file.name, fileSize: file.size,
                        fileType: file.type, imageID: res.fileid,
                        base64string: data,
                        folderID: gallaryDiv.folderID, ext: file.name.substr(file.name.lastIndexOf('.') + 1),
                        dimensions: res.dimensions
                    };

                    gallery.InsertThumbnails(obj, gallaryDiv, function (res) {
                        _common.Loading(false);
                        swal('Image uploaded successfully.', { icon: "info", });
                        gallery.InitializegalleryPaging(gallaryDiv);

                    })
                }
            })
        }
        reader.readAsDataURL(file);
    } else {
        swal("Please upload only image file of format .gif, .jpg, .jpeg or .png", { icon: "info", });
    }
}
gallery.InitializegalleryPaging = function (gallaryDiv) {
    if (gallaryDiv[gallaryDiv.folderID] == undefined)
        gallaryDiv[gallaryDiv.folderID] = {};
    gallaryDiv[gallaryDiv.folderID].PageSize = gallery.PageSize;
    gallaryDiv[gallaryDiv.folderID].PageIndex = gallaryDiv[gallaryDiv.folderID].PageIndex == undefined ? 1 : gallaryDiv[gallaryDiv.folderID].PageIndex;
    gallaryDiv[gallaryDiv.folderID].TotalCount = gallaryDiv[gallaryDiv.folderID].TotalCount == undefined ? 1 : gallaryDiv[gallaryDiv.folderID].TotalCount;
    gallery.GetImageList(gallaryDiv);
}

gallery.GetImageList = function (gallaryDiv) {
    _common.ContainerLoading(true, gallaryDiv.find('.right-panel-image.col'));
    let param = { 'url': '/api/marketing/GetImagesFolderwise', 'type': 'post', 'contentType': 'application/json' };
    param.data = { 'FolderID': gallaryDiv.folderID, 'pageIndex': gallaryDiv[gallaryDiv.folderID].PageIndex, 'pageSize': gallaryDiv[gallaryDiv.folderID].PageSize };
    let rightPanel = gallaryDiv.find('.right-panel-image');
    _common.FetchApiByParam(param, function (result) {
        if (result.statuscode == 200) {
            let imageDiv = $('<div></div>');
            let folderName = gallaryDiv.find('.dir.ui-active').text();
            let header = '<h5 class="img-lst-hdr text-primary font-weight-bold mt-3 mb-1">' + folderName + '</h5><div class="clearfix"></div>';
            let imageArray = [];
            if (result.data && result.data.data && result.data.data.length > 0) {
                imageArray = result.data.data;
                gallaryDiv[gallaryDiv.folderID].TotalCount = result.data.total;
            }
            gallery.DisplayImageList(gallaryDiv, rightPanel, imageArray, header);
            rightPanel.append(imageDiv);
            _common.ContainerLoading(false, gallaryDiv.find('.right-panel-image.col'));
        } else {
            swal(result.message, { icon: "info", });
        }
    })
}
gallery.OpenModalForSize = function (cb) {
    let body = `
    <p>Enter the width and height of image</p>
    <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Width (px)</label>
      <input type="number" class="form-control crop-width" value="300" placeholder="Width" min="1" max="600">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Height (px)</label>
      <input type="number" class="form-control crop-height" value="300" placeholder="Height" min="1" max="2000">
    </div>
  </div>
    `
    let formModal = _common.DynamicPopUp({
        saveButtonName: "Ok",
        title: "Crop Size",
        body: body
    })
    formModal.find(".modal-lg").removeClass("modal-lg").addClass("modal-sm")
    formModal.modal("show");
    formModal.find(".btnSave").click(function () {
        let obj = {
            width: formModal.find(".crop-width").val(),
            height: formModal.find(".crop-height").val()
        }
        if( obj.width<1){
            swal("Image width must be greater than 1px",{icon:"error"});
            return
        }
        if( obj.height<1){
            swal(`Image height must be greater than 1px`,{icon:"error"});
            return
        }
        cb(obj);
        formModal.modal("hide");
    })
}
gallery.DisplayImageList = function (gallaryDiv, panel, imgList, header) {
    gallaryDiv.folderID = gallaryDiv.find('.left-panel-tree .ui-active').attr('data-dir');
    panel.html(header);
    if (imgList) {
        let divCover = $('<div class="row"></div>');
        let toolBox = $(gallery.ImageToolBox);
        _userprofile.Auth({ pageData: { "PageName": "CreateCampaign", "ObjectName": "MarketingCampaign" }, container: toolBox });
        for (let i = 0; i < imgList.length; i++) {
            let imageDimension = '';
            if (imgList[i].dimensions) {
                imageDimension = ' data-image-height="' + imgList[i].dimensions.height + '" data-image-width="' + imgList[i].dimensions.width + '"';
            }
            let originalImageUrl = _constantClient.CDN + '/public/' + _initData.Company._id + '/' + imgList[i].ParentImageID;
            let thumbnailUrl = _constantClient.CDN + '/public/' + _initData.Company._id + '/' + imgList[i].ThumbImageID;
            //console.log(originalImageUrl,"===",thumbnailUrl)
            let galleryThumb = $('<div' + imageDimension + ' class="div-img-list img-ver ml-3 mr-3 card mb-3" data-url="' + originalImageUrl + '"></div>');
            galleryThumb[0].data = imgList[i];
            galleryThumb.append('<div class="thumb_img rounded" style="background:url( ' + thumbnailUrl + ') no-repeat"></div>');
            let toolDiv = $('<div class="toolDiv"></div>');
            if (imgList[i].ImageArray && imgList[i].ImageArray.length > 0) {
                toolDiv.append('<div class="ct-image btn btn-sm btn-danger mb-1" data-toggle="tooltip" data-placement="right" title="Edited Files" data-id="' + imgList[i].ParentImageID + '">' + (imgList[i].ImageArray.length) + '</div>');
            }
            let fSize = Math.round(imgList[i].FileSize / 1024);
            let unit='KB';
            if(fSize>1024){
                fSize = Math.round((fSize / 1024)*10)/10 ;
                unit='MB'
            }
            fSize=fSize+unit;
            let fDimension='';
            if(imgList[i].dimensions){
                fDimension='<div class="sm-txt small pull-right">'+imgList[i].dimensions.width+' X '+ imgList[i].dimensions.height+'</div>';
            }  
            galleryThumb.append('<div class="position-absolute p-2 rounded-bottom bg-white w-100" style="bottom:0;"><h5 class="img_name mb-0">' + imgList[i].FileName + '</h5><div class="sm-txt small">(' + fSize + ')</div>'+fDimension+'</div>');

            let toolBoxDiv = $(toolBox[0].outerHTML);
            toolBoxDiv.find('.img').attr('href', originalImageUrl).attr('data-url', ` ${window.location.origin}/api/drive/dbimages/${imgList[i].ParentImageID}`);
            if (toolBoxDiv.find('.del-image').length > 0) {
                toolBoxDiv.find('.del-image').attr('thumbImageID', imgList[i].ThumbImageID);
                toolBoxDiv.find('.del-image').attr('imgExt', imgList[i].Ext);
                toolBoxDiv.find('.del-image').attr('isParent', imgList[i].IsParent);
                toolBoxDiv.find('.del-image').attr('data-id', imgList[i].ParentImageID);
            }
            toolDiv.append(toolBoxDiv);
            // toolDiv.append('<div class="img-tool">                < a href = "/api/drive/dbimages/' + imgList[i].ParentImageID + '.' + imgList[i].Ext + '" target = "_blank" style = "border:none;" >
            //     <i class="fa fa-search btn btn-sm btn-primary mb-1" title="Preview"></i></a >
            // <i class="fa fa-check-square apply-image btn btn-sm btn-secondary mb-1" title="Select Image"></i>
            // <i title="Edit" class="fa fa-edit edit-image btn btn-sm btn-secondary mb-1" data-restriction="Modify"></i>
            // <i class="fa fa-trash del-image btn btn-sm btn-secondary mb-1" data-restriction="Delete" title="Delete Image" thumbImageID="' + imgList[i].ThumbImageID + '"
            //     imgExt="' + imgList[i].Ext + '" isParent="' + imgList[i].IsParent + '" data-id="' + imgList[i].ParentImageID + '"></i></div > ');
            divCover.append(galleryThumb);
            galleryThumb.append(toolDiv);
            panel.append(divCover);
            if (gallery.prvObj == undefined) {
                panel.find('.apply-image').hide();
            }
        }
        //paging start
        panel.append('<nav class="mt-3 float-right hide-edit gallery-image-paging"></nav>');
        let pager = _common.PagerService(gallaryDiv[gallaryDiv.folderID].TotalCount, gallaryDiv[gallaryDiv.folderID].PageIndex, gallaryDiv[gallaryDiv.folderID].PageSize);
        _common.BuildPaging(pager, panel.find('.gallery-image-paging'));
        panel.off("click", '.gallery-image-paging .pagination .page-item');
        panel.on("click", '.gallery-image-paging .pagination .page-item', function () {
            let pid = parseInt($(this).attr('pid'));
            let pager = $(this).closest('.pagination')[0].pager;
            if (pid < 1 || pid > pager.totalPages || pid == pager.currentPage) {
                return false;
            }
            gallaryDiv[gallaryDiv.folderID].PageIndex = pid;
            gallery.GetImageList(gallaryDiv);
        });
        //paging end
    }

    panel.off("click", '.lnkBack');
    panel.find('.lnkBack').click(function () {
        gallaryDiv.find('.gallery-image-paging').show();
        gallery.InitializegalleryPaging(gallaryDiv);
    });

    panel.off("click", '.ct-image');
    panel.find('.ct-image').on('click', function () {
        let parentImageID = $(this).attr('data-id');
        let rightPanel = gallaryDiv.find('.right-panel-image');
        let param = { 'url': '/api/marketing/GetCropImageList', 'type': 'post', 'contentType': 'application/json' };
        param.data = { 'parentImageID': parentImageID, 'folderID': gallaryDiv.folderID };
        _common.FetchApiByParam(param, function (result) {
            if (result.statuscode === 200) {
                var header = '<div class="img-lst-hdr"><span class="lnkBack btn pl-0"><i class="fas fa-chevron-left"></i> Back</span></divf><div class="clearfix"></div>';
                if (result.data) {
                    gallery.DisplayImageList(gallaryDiv, rightPanel, result.data, header);
                    gallaryDiv.find('.gallery-image-paging').hide();
                }
                else {
                    swal(result.message, { icon: "info", });
                }
                rightPanel.find('.edit-image').remove();
            }
        })
    })

    panel.off("click", '.del-image');
    panel.find('.del-image').on('click', function () {
        let btnDelete = $(this);
        _common.ConfirmDelete({ message: 'Are you sure,you want to delete this image?' }).then((confirm) => {
            if (confirm) {
                let param = { 'url': '/api/marketing/DeleteImageFromFolder', 'type': 'post', 'contentType': 'application/json' };
                param.data = {
                    'parentImageID': btnDelete.attr('data-id'), 'folderID': gallaryDiv.folderID,
                    'thumbImageID': btnDelete.attr('thumbImageID'), 'isParent': btnDelete.attr('isParent'),
                };
                _common.FetchApiByParam(param, function (result) {
                    if (result.statuscode === 200) {
                        swal(result.message, { icon: "info", });
                        gallery.InitializegalleryPaging(gallaryDiv);
                    }
                    else {
                        swal(result.message, { icon: "info", });
                    }
                })
            }
        });


    })

    panel.off("click", '.edit-image');
    panel.find('.edit-image').on('click', function () {
        params = {
            width: 300,
            height: 300
        }
        if (gallery.prvObj == undefined) {
            gallery.OpenModalForSize((sizeObj) => {
                params.width = sizeObj.width;
                params.height = sizeObj.height;
                panel.StartEditing(params, $(this))
            })
        } else {
            params.width = gallery.prvObj == undefined ? '600' : gallery.prvObj.attr('width');
            params.height = gallery.prvObj == undefined ? '300' : gallery.prvObj.attr('height');
            panel.StartEditing(params, $(this))
        }
    })

    panel.StartEditing = function (params, ref) {
        _common.Loading(true);
        let parentImageID = ref.parent().find('.del-image').attr('data-id');
        let url = ref.parent('.img-tool').find('a').attr('data-url');
        let ext = url.substr(url.lastIndexOf('.') + 1);
        let fileName = ref.closest('.div-img-list').find('.img_name').text();// ref.parent().parent().find('.img_name').text();
        let obj = { responseType: 'blob', url: url };
        let imgExt = ref.parent().find('.del-image').attr('imgext'); //$(this).parent().find('.del-image').attr('imgExt');
        _common.FetchXMLHttpRequestByParam(obj, function (res) {
            let reader = new FileReader();
            reader.onloadend = function () {
                let data = reader.result;
                let objData = {
                    Width: params.width,
                    Height: params.height,
                    FileName: fileName,
                    galleryFolderID: gallaryDiv.folderID,
                    parentImageID: parentImageID
                };
                objData.OriginalImage = new Image();
                objData.callback = function (galleryRes) {
                    if (galleryRes) {
                        let obj = { responseType: 'blob', url: '/api/drive/dbimages/' + galleryRes + "." + imgExt };

                        _common.FetchXMLHttpRequestByParam(obj, function (resCropImage) {
                            let readerCrop = new FileReader();
                            readerCrop.onloadend = function () {
                                let dataCropImage = readerCrop.result;
                                let base64string = dataCropImage.split(";base64,")[1];
                                let obj = {
                                    fileName: fileName, fileSize: base64string.length,
                                    fileType: "image/" + ext, parentImageID: parentImageID,
                                    ImageID: galleryRes,
                                    base64string: dataCropImage,
                                    folderID: gallaryDiv.folderID, ext: ext,
                                    galleryFolderID: gallaryDiv.folderID,
                                    parentImageID: parentImageID
                                };
                                gallery.InsertCropImageThumbnail(obj, function (res) {
                                    gallery.InitializegalleryPaging(gallaryDiv);
                                })
                            }
                            readerCrop.readAsDataURL(resCropImage);
                        })
                    }
                    else {
                        if (galleryRes != "")
                            swal('something went wrong', { icon: "info", });
                    }
                }
                objData.OriginalImage.onload = function (e) {
                    const stringLength = data.length - data.split(';base64')[0].length;
                    const sizeInBytes = 4 * Math.ceil((stringLength / 3)) * 0.5624896334383812;
                    objData.FileSize = sizeInBytes
                    IBP.CustomizeImage(objData);
                }
                objData.OriginalImage.src = data;
                _common.Loading(false);
            }
            reader.readAsDataURL(res);
        })
    }

    panel.off("click", '.apply-image');
    panel.on('click', '.apply-image', function () {
        let width = parseFloat($(this).closest('.div-img-list').attr('data-image-width'));
        let imgURL = $(this).closest('.div-img-list').attr('data-url');
        let td = gallery.prvObj.closest('td');
        gallery.prvObj.attr('width', (isNaN(width) ? td.width() : (width > td.width() ? td.width() : width)));
        if (isNaN(width)) {
            gallery.prvObj.attr('height', td.height());
        } else {
            gallery.prvObj.removeAttr('height');
        }
        gallery.prvObj.css('margin', '0');
        gallery.prvObj.attr('src', imgURL);
        if( gallery.prvObj[0].onImageChange){
            gallery.prvObj[0].onImageChange(gallery.prvObj[0]);
        }
        gallaryDiv.remove();
    });
}



gallery.CreateFolder = function (gallaryDiv) {
    let folderPopup = _common.DynamicPopUp({ title: 'Create Folder', body: gallery.MARKETING_FOLDER_POPUP, DistroyOnHide: true });
    folderPopup.find('.modal-dialog').removeClass('modal-lg');
    folderPopup.modal('show');

    folderPopup.find('.btnSave').click(function (e) {
        let param = { 'url': '/api/marketing/CheckMediaLibraryFolderExists', 'type': 'post', 'contentType': 'application/json' };
        let folderValue = folderPopup.find('.txtDir').val();
        if (folderValue == '' || folderValue.trim().length == 0) {
            swal(_constantClient.FOLDER_NAME_EMPTY_MSG, { icon: "info", });
            return false;
        }
        param.data = { 'FolderName': folderValue };
        _common.FetchApiByParam(param, function (result) {
            if (result) {
                swal(_constantClient.FOLDER_NAME_EXISTS_MSG, { icon: "info", });
                return false;
            } else {
                param = { 'url': '/api/marketing/InsertMediaLibraryFolder', 'type': 'post', 'contentType': 'application/json' };
                param.data = { 'FolderName': folderPopup.find('.txtDir').val().trim() }
                _common.FetchApiByParam(param, function (result) {
                    if (result.statuscode == 200) {
                        folderPopup.modal('hide');
                        swal(result.message, { icon: "success", });
                        gallery.GetGalleryFolderList(gallaryDiv);

                    } else {
                        swal(result.message, { icon: "info", });
                    }
                });
            }
        });
    });
}

gallery.GetGalleryFolderList = function (gallaryDiv) {
    let leftPanel = gallaryDiv.find('.left-panel-tree');
    let param = { 'url': '/api/marketing/GetMediaLibraryFolderList', 'type': 'post', 'contentType': 'application/json' };
    param.data = {};
    _common.FetchApiByParam(param, function (result) {
        leftPanel.html('');
        let len = result.length;
        if (len > 0) {
            let isActive = true;
            let isActiveClass = '';
            for (let i = 0; i < len; i++) {
                isActiveClass = isActive == true ? 'ui-active' : ''
                leftPanel.append('<li class="nav-item"><a class="dir nav-link ' + isActiveClass + '" data-dir="' + result[i]._id + '" isDefault="' + result[i].IsDefault + '"><i class="fa fa-folder"></i> <span  class="folder-name">' + result[i].FolderName + '</span> </a></li>');
                isActive = false
            }
        }
        gallaryDiv.folderID = gallaryDiv.find('.left-panel-tree .ui-active').attr('data-dir');
        gallaryDiv[gallaryDiv.folderID] = {};
        gallaryDiv[gallaryDiv.folderID].PageIndex = 1;
        gallaryDiv[gallaryDiv.folderID].PageSize = gallery.PageSize;
        gallery.InitializegalleryPaging(gallaryDiv);
    })
}



gallery.InsertThumbnails = function (obj, gallaryDiv, callback) {
    let param = { 'url': '/api/drive/uploadfile', 'type': 'post', 'contentType': 'application/json' };
    _common.Resize(obj.base64string, 300, obj.ext, function (resizeResponse) {
        if (resizeResponse != undefined) {
            param.data = {
                'filename': obj.fileName, 'size': obj.fileSize,
                'type': obj.fileType, 'objectname': 'MediaImages',
                'confirmAttachment': true,
                'data': resizeResponse, 'parentImageID': obj.imageID,
                'galleryFolderID': obj.folderID,
                'filetype': 'thumbnail'
            };
            _common.UploadFiles(param, function (res) {
                if (res.fileid) {
                    let paramNew = { 'url': '/api/marketing/InsertImageInLibraryFolder', 'type': 'post', 'contentType': 'application/json' };

                    paramNew.data = {
                        'fileName': obj.fileName, 'fileSize': obj.fileSize, "parentImageID": obj.imageID,
                        'thumbImageID': res.fileid, 'folderID': obj.folderID, 'imagExt': obj.ext,
                        'dimensions': obj.dimensions
                    };
                    _common.FetchApiByParam(paramNew, function (res) {
                        if (res.statuscode == 200) {
                            callback(res);
                        }
                        else {
                            swal(res.message, { icon: "info", });
                        }
                    });
                }
                return false;
            })
        }
    })
}

gallery.InsertCropImageThumbnail = function (obj, fnCallback) {
    _common.Resize(obj.base64string, 300, obj.ext, function (callback) {
        if (callback != undefined) {
            let param = { 'url': '/api/drive/uploadfile', 'type': 'post', 'contentType': 'application/json' };
            param.data = {
                'filename': obj.fileName, 'size': obj.fileSize,
                'type': "image/" + obj.ext, 'objectname': 'MediaImages',
                'confirmAttachment': true,
                'data': callback.split(";base64,")[1],
                'parentImageID': obj.parentImageID,
                'galleryFolderID': obj.folderID,
                'filetype': 'thumbnail'
            };
            param.headers = {
                'socket': sessionStorage.getItem('socket-id')
            }
            _common.FetchApiByParam(param, function (res) {
                if (res.fileid) {
                    let paramNew = { 'url': '/api/marketing/InsertCropImageInFolder', 'type': 'post', 'contentType': 'application/json' };
                    paramNew.data = {
                        'dimensions':res.dimensions,
                        'mainParentImageID': obj.parentImageID, 'fileName': obj.fileName,
                        'fileSize': obj.fileSize, "parentImageID": obj.ImageID, 'thumbImageID': res.fileid, 'folderID': obj.folderID, 'imagExt': obj.ext
                    };
                    _common.FetchApiByParam(paramNew, function (res) {
                        if (res) {
                            fnCallback(res);
                        }
                    });
                }
            })
        }
    })
}

