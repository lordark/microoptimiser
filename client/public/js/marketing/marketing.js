const _marketing = {};
_marketing.PageSize = 20;
const _campaign = {};
_marketing.COMBINE_LIST_POPUP = '<div class="row">\
<div class="form-group col-sm-12">\
    <label class="radio-inline">\
        <input type="radio" class="radio" value="combine" name="optradio" checked>Combine\
    </label>\
    <label class="radio-inline" style="margin-left:50px;">\
        <input type="radio" class="radio" value="create" name="optradio">Create New List from Selected List\
    </label>\
</div>\
<div class="form-group col-sm-6 vld-parent">\
    <label class="small">Select Primary List</label>\
    <select title="List" class="form-control form-control-sm ddl-list msp-ctrl" data-required><option></option></select>\
</div>\
<div class="form-group col-sm-6 create-section d-none vld-parent">\
    <label class="small">Enter list name</label>\
    <input type="text" title="List name" class="msp-ctrl form-control form-control-sm txtListName msp-ctrl" data-required />\
</div>\
<div class="form-group col-sm-6 create-section d-none">\
    <label class="small parent-lblChk lbl-selected-list">Delete Selected List (<div style="display:inline-block;"></div>)<label class="lblChk"><input type="checkbox" class="msp-ctrl"><span class="checkmark"></span></label></label>\
</div>\
<div class="form-group col-sm-12 tblMapping">\
</div>\
</div>';

_marketing.TemplateToolbox = '<li class="col mb-4 pl-3 pr-3" templateblockid="" templateblockgroup="">\
<a href="#" class="marketing-select position-relative card">\
<span style="position:absolute;top: 8px;right: 8px;">\
<span class="marketing-preview rounded btn btn-sm btn-primary" Title="Template Preview"> <i class="fas fa-search"></i></span>\
<span class="template-clone btn btn-sm btn-secondary" Title="Duplicate" data-restriction="Create"><i class="fas fa-clone"></i></span>\
<span class="template-delete btn btn-sm btn-secondary" Title="Delete" data-restriction="Delete"><i class="fas fa-trash"></i></span>\
</span>\
</a>\
<h4 class="text-center mt-2 title template-block-name"></h4><p class="text-center text-secondary sub-title"><span>Last edited: </span> <span class="last-modify-date"></span> <br/><span class="last-modify-by"></span></p> \
</li>';

_marketing.emailPopupHTML = (`<div class="row">
                                <div class="col-sm-12 mt-1 subscribe-unsubscribe-details">
                                    <div class="table-responsive tblSubscription">
                                        <table class="table table-bordered w-100">
                                            <thead class="w-100">
                                                <tr class="head-row w-100">
                                                    <th>Marketing Categories</th>
                                                    <th>Subscribed</th>
                                                    <th>Unsubscribed</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <nav class="float-right subscription-email-paging">
                                        <div class="paging">
                                            <ul class="pagination"></ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>`);

_marketing.OpenTemplateListPage = function (page) {
    new Page({ 'pageName': 'TemplateList' });
}

_marketing.InitTemplateListPage = function (page) {
    page.PageContainer.find('.btnAddTemplate').on('click', function () {
        new Page({ 'pageName': 'CreateTemplate' });
    })
}

_marketing.InitTemplateCreate = function (page) {
    if (page.GroupID == undefined) {
        page.GroupID = 1;
    }
    _marketing.InitializeTemplateWithPaging(page);
    page.PageContainer.find('.list-inline .switch-category').on('click', function () {
        $('.list-inline .switch-category').removeClass('active');
        $(this).toggleClass('active');
        page.GroupID = $(this).attr('groupid');
        page.OrderByShort = -1;
        _marketing.InitializeTemplateWithPaging(page);
    })
}

_marketing.InitShowSavedTemplate = function (page) {
    page.GroupID = 2;
    _marketing.InitializeTemplateWithPaging(page);
    page.PageContainer.find('.btnShowTemplateList').on('click', function () {
        new Page({ 'pageName': 'CreateTemplate' });
    })
    page.PageContainer.find('.get-gallary').on('click', function () {
        gallery.MediaGallery();
    })

    page.PageContainer.find('.btn-template-search').on('click', function () {
        page.OrderByShort = page.PageContainer.find('.template-order-sorting').val();;
        page.TemplateSearch = page.PageContainer.find('.template-search').val();;
        _marketing.InitializeTemplateWithPaging(page);
    })

    page.PageContainer.find('.template-order-sorting').on('change', function () {
        page.OrderByShort = page.PageContainer.find('.template-order-sorting').val();
        page.TemplateSearch = page.PageContainer.find('.template-search').val();;
        _marketing.InitializeTemplateWithPaging(page);
    })
}

_marketing.InitializeTemplateWithPaging = function (page) {
    let statusTemplate = page.GroupID == 1 ? 'pagerBasic' : page.GroupID == 2 ? 'pageSaved' : 'htmlCode';
    if (page[statusTemplate] == undefined)
        page[statusTemplate] = {};
    page[statusTemplate].PageSize = _marketing.PageSize;
    page[statusTemplate].PageIndex = page[statusTemplate].PageIndex == undefined ? 1 : page[statusTemplate].PageIndex;
    page[statusTemplate].TotalCount = page[statusTemplate].TotalCount == undefined ? 1 : page[statusTemplate].TotalCount;
    _marketing.GetTemplateList(page);
}

_marketing.GetTemplateList = function (page) {
    if (!page.OrderByShort) {
        page.OrderByShort = page.PageContainer.find('.template-order-sorting').val();
    }
    let toolBox = $(_marketing.TemplateToolbox);
    _userprofile.Auth({ pageData: page.Data, container: toolBox });
    // _common.ContainerLoading(true, page.PageContainer.find('.TemplateSavedBasic'));
    let statusTemplate = page.GroupID == 1 ? 'pagerBasic' : page.GroupID == 2 ? 'pageSaved' : 'htmlCode';
    let param = { 'url': 'api/marketing/GetTemplateList', 'type': 'post', 'contentType': 'application/json' };
    param.data = {
        'groupID': page.GroupID,
        'pageSize': page[statusTemplate].PageSize,
        'pageIndex': page[statusTemplate].PageIndex,
        'sort': page.OrderByShort,
        'templateSearch': page.TemplateSearch
    };
    _common.Loading(true);
    _common.FetchApiByParam(param, function (result) {
        if (result.statuscode == 200) {
            _common.Loading(false);
            let res = result.data[0].data;
            let imgSrc = '';
            page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').html('');
            if (res && res.length > 0) {
                for (let i = 0; i < res.length; i++) {
                    let date = moment(new Date(res[i].ModifiedDate)).format('YYYY MMM D');
                    let thumbnail = (res[i].Thumbnail.includes('.')) ? res[i].Thumbnail.split('.')[0] : res[i].Thumbnail;
                    imgSrc = page.GroupID == 1 ? _constantClient.TEMPLATE_PUBLIC_IMAGE_URL + res[i].Thumbnail : _constantClient.TEMPLATE_SERVER_IMAGE_URL + 'public/' + _initData.Company._id + '/' + thumbnail;
                    let li = $(toolBox[0].outerHTML);
                    li.attr('templateblockid', res[i]._id);
                    li.attr('templateblockgroup', res[i].GroupID);
                    li.find('.marketing-select').css({ 'background': 'url(' + imgSrc + ')' });

                    li.find('.template-block-name').html(res[i].TemplateBlockName);
                    li.find('.last-modify-date').html(date);
                    li.find('.last-modify-by').html(res[i].LastModifiedBy);

                    // let li = $('<li class="col mb-4 pl-3 pr-3" templateblockid="' + res[i]._id + '" "\
                    // templateblockgroup="'+ res[i].GroupID + '"><a href=# class="marketing-select position-relative card" style="background:url(' + imgSrc + ') no-repeat;">\
                    // <span class="marketing-preview rounded btn btn-sm btn-primary" Title="Template Preview"> <i class="fas fa-search"></i></span><span class="template-clone btn btn-sm btn-secondary" Title="Duplicate" data-restriction="Create">\
                    // <i class="fas fa-clone"></i></span><span class="template-delete btn btn-sm btn-secondary" Title="Delete" data-restriction="Delete"><i class="fas fa-trash"></i></span></a>\
                    // <h4 class="text-center mt-2 title">' + res[i].TemplateBlockName + '</h4><p class="text-center text-secondary sub-title"><span>Last edited: </span> <span>' + date + ' </span> <br/><span>' + res[i].LastModifiedBy + '</span></p> \
                    // </li>');
                    li[0].item = res[i];
                    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').append(li);
                    if (page.GroupID == 1)
                        page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list .template-clone,.template-delete').hide();
                }
                let statusTemplate = page.GroupID == 1 ? 'pagerBasic' : page.GroupID == 2 ? 'pageSaved' : 'htmlCode';
                if (!_common.isEmptyObj(result.data[0]))
                    page[statusTemplate].TotalCount = (result.data[0].total[0] && result.data[0].total[0].count > 0) ? result.data[0].total[0].count : 0;
                page[statusTemplate].PageSize = page[statusTemplate] == undefined ? 3 : page[statusTemplate].PageSize;
                _marketing.BindTemplateButtonEvent(page, page.GroupID, res);
                _marketing.Pagination(page);
                _common.ContainerLoading(false, page.PageContainer.find('.TemplateSavedBasic'));
            } else if (page.GroupID != 1 && page.GroupID != 2) {
                page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list')
                    .html('<li class="col mb-4 pl-3 pr-3" templateblockgroup="3">\
                                <a href="#" class="marketing-select position-relative card" style="background: url(&quot;../images/marketing-template-image/htmlcode.jpg&quot;);"></a>\
                                <h4 class="text-center mt-2 title template-block-name">Paste in code</h4>\
                                <p class="text-center text-secondary sub-title">\
                                    <span>Create a campaign by pasting your custom coded design.</span>\
                                </p>\
                            </li>');
                page.PageContainer.find('.TemplateSavedBasic .pagination').html('');
            }
        }
        else {
            _common.Loading(false);
            swal(result.message, { icon: "info" });
        }
        page.PageContainer.find('.TemplateSavedBasic').off("click", page.EditClassName);

        page.PageContainer.find('.TemplateSavedBasic').on("click", '.marketing-select', function () {
            if (_initData.ProfileAuth && _initData.ProfileAuth.Permissions
                && _initData.ProfileAuth.Permissions.find(x => x.ModuleID == "marketing")
                && (_initData.ProfileAuth.Permissions.find(x => x.ModuleID == "marketing").Operations.indexOf("Create") >= 0 || _initData.ProfileAuth.Permissions.find(x => x.ModuleID == "marketing").Operations.indexOf("Modify") >= 0)) {
                let item = $(this).closest('li')[0].item;
                let url = window.location.origin;
                if (item != undefined && (page.GroupID == 1 || page.GroupID == 2)) {
                    item.TemplateBlockContent = item.TemplateBlockContent.replace(new RegExp('##URL##', 'g'), url);
                    let param = {
                        content: item.TemplateBlockContent,
                        GroupID: item.GroupID,
                        templateName: item.TemplateBlockName,
                        thumbnailName: item.Thumbnail,
                        pageStyle: item.PageStyle,
                        templateId: item._id,
                    };
                    _marketing.OpenEditor(page, param);
                } else {
                    let body = $('<div><textarea class="txtEdit" style="width:100%;height:400px"></textarea></div>');
                    let addHtmlPopup = _common.DynamicPopUp({ title: 'Add HTML Code', body: body.html() });
                    setTimeout(function () {
                        var editor = CodeMirror.fromTextArea(addHtmlPopup.find('.txtEdit')[0], {
                            lineNumbers: true,
                            mode: "text/html",
                            gutters: ["CodeMirror-lint-markers"],
                            lint: true
                        });
                        addHtmlPopup.find('.btnSave').on('click', function () {
                            editor.save();
                            let htmlBody = $('<div>' + addHtmlPopup.find('.txtEdit').val() + '</div>');
                            if (htmlBody.find('a.unsubscribe-link').length == 0) {
                                swal('To add unsubscribe link in your template, so you need to add "unsubscribe-link" class in your html Anchor tag.', { icon: 'info' });
                                return false;
                            }
                            param.templateId = 'ManualHTMLCode';
                            param.content = addHtmlPopup.find('.txtEdit').val();
                            _marketing.OpenEditor(page, param);
                            addHtmlPopup.modal('hide');
                        });
                    }, 200);
                    addHtmlPopup.modal('show');
                }
            } else {
                swal('You are not authorised to access this page.', { icon: 'info' });
            }
        })
    })
}

_marketing.Pagination = function (page) {
    let statusTemplate = page.GroupID == 1 ? 'pagerBasic' : page.GroupID == 2 ? 'pageSaved' : 'htmlCode';
    let pager = _common.PagerService(page[statusTemplate].TotalCount, page[statusTemplate].PageIndex, page[statusTemplate].PageSize);
    _common.BuildPaging(pager, page.PageContainer.find('.template-list-paging'));
    page.PageContainer.off("click", '.template-list-paging .pagination .page-item');
    page.PageContainer.on("click", '.template-list-paging .pagination .page-item', function () {
        let pid = parseInt($(this).attr('pid'));
        let pager = $(this).closest('.pagination')[0].pager
        if (pid < 1 || pid > pager.totalPages || pid == pager.currentPage) {
            return false;
        }
        let statusTemplate = page.GroupID == 1 ? 'pagerBasic' : page.GroupID == 2 ? 'pageSaved' : 'htmlCode';
        page[statusTemplate] = {};
        page[statusTemplate].PageSize = _marketing.PageSize;
        page[statusTemplate].PageIndex = pid;
        page[statusTemplate].TotalCount = pager.totalPages;
        _marketing.GetTemplateList(page);
    });
}

_marketing.OpenEditor = function (page, param) {
    param.topButtons = [];
    param.IsDynamicField = false;
    let campaignData = page.param.campaignData;

    if (_common.isEmptyObj(campaignData) && param.EditUnsubsPage) {
        param.topButtons.push({
            'html': '<button class="btn btn-primary btnSaveTemplate save mr-2" data-toggle="tooltip" title="" data-restriction="Modify" data-original-title="Save">Save</button>',
            'onClick': function (e, data) {
                param.content = data.EditBlock;
                param.actionName = "SaveUnsubscribePage";
                param.closeEditor = { 'BtnSave': $(e), 'FixedBody': $('body').removeClass('fixedbody') };
                _marketing.ValidateTemplate(page, param);
            }
        });
    } else if (_common.isEmptyObj(campaignData) || _page.nav.find('.nav-item.active').attr('data-uid') == 'CreateTemplate' ||
        _page.nav.find('.nav-item.active').attr('data-uid') == 'TemplateList') {
        let isShow = param.GroupID == 1 ? 'd-none' : '';
        param.topButtons.push({
            'html': '<button class="btn btn-primary btnSaveTemplate save mr-2 ' + isShow + '" data-toggle="tooltip" title="" data-restriction="Modify" data-original-title="Save">Save</button>',
            'onClick': function (e, data) {
                param.content = data.Content;
                param.pageStyle = data.PageStyle;
                param.actionName = "Save";
                param.popUpName = "Save Template";
                _marketing.ValidateTemplate(page, param);
            }
        });
        param.topButtons.push({
            'html': '<button class="btn btn-primary   btnSaveTemplate save-as" data-toggle="tooltip" data-restriction="Create" title="Save As">Save As</button>',
            'onClick': function (e, data) {
                param.content = data.Content;
                param.pageStyle = data.PageStyle;
                param.actionName = "Save As";
                param.popUpName = "Save Template";
                _marketing.ValidateTemplate(page, param);
            }
        });
    }

    if (!_common.isEmptyObj(campaignData) && campaignData.hasOwnProperty('data') && _page.nav.find('.nav-item.active').attr('data-uid') != 'CreateTemplate'
        && _page.nav.find('.nav-item.active').attr('data-uid') != 'TemplateList') {
        param.IsDynamicField = true;
        page.PageContainer.find('.campaign .nav-item .nav-link').removeClass('active');
        page.PageContainer.find('.campaign .nav-item .nav-link[name="editor"]').addClass('active');
        page.PageContainer.find('containerTabtab-pane').removeClass('active');
        page.PageContainer.find('.containerTab.tab-pane[name="editor"]').addClass('active');
        page.PageContainer.find('.campaign-container .containerTab.tab-pane.active .template-Bottam').addClass('d-none');
        param.topButtons.push({
            'html': ' <button class="btn btn-outline-primary change-template mr-2">Change Template</button>',
            'onClick': function (e, data) {
                $(e).closest('.marketing-template-editor').remove();
                $('body').removeClass('fixedbody');
                _marketing.InitTemplateCreate(page);
            }
        });
        param.topButtons.push({
            'html': '<span class="btn btn-outline-primary float-left"><i class="fas fa-arrow-left next-schedule"></i> Back </span>',
            'onClick': function (e, data) {
                _campaign.LoadCampaignValue(page, 'template', data);
                $(e).closest('.marketing-template-editor').remove();
                $('body').removeClass('fixedbody');
                page.PageContainer.find('.campaign .nav-item .nav-link').removeClass('active');
                page.PageContainer.find('.campaign .nav-item .nav-link[name="basic"]').addClass('active');
                page.PageContainer.find('.containerTab.tab-pane').removeClass('active');
                page.PageContainer.find('.containerTab.tab-pane[name="basic"]').addClass('active');
                page.PageContainer.find('.campaign-container .containerTab.tab-pane .template-Bottam').removeClass('d-none');

                page.PageContainer.find('.campaign .nav-item a[name="basic"]')
                    .closest('li')
                    .removeClass('completed')
                    .addClass('selected');
                page.PageContainer.find('.campaign .nav-item a[name="template"]')
                    .closest('li')
                    .removeClass('completed')
                    .removeClass('selected');
            }
        });

        param.topButtons.push({
            'html': ' <span class="btn btn-primary float-right"> Next <i class="fas fa-arrow-right next-schedule"></i></span>',
            'onClick': function (e, data) {
                _campaign.LoadCampaignValue(page, 'template', data);
                setTimeout(function () {
                    if ($(page.param.campaignData.data.TemplateBlockContent).find('.unsubscribe-link').length == 0) {
                        _common.Loading(false);
                        swal('To move on the next step, you need to add unsubscribe link in your template that you are using to send campaign.', { icon: 'info' });
                    } else {
                        $(e).closest('.marketing-template-editor').remove();
                        $('body').removeClass('fixedbody');
                        page.PageContainer.find('.campaign .nav-item .nav-link').removeClass('active');
                        page.PageContainer.find('.campaign .nav-item .nav-link[name="schedule"]').addClass('active');
                        page.PageContainer.find('.containerTab.tab-pane').removeClass('active');
                        page.PageContainer.find('.containerTab.tab-pane[name="schedule"]').addClass('active');
                        page.PageContainer.find('.campaign-container .containerTab.tab-pane.active .btnNext').removeClass('d-none');
                        page.PageContainer.find('.campaign .nav-item a[name="template"]')
                            .closest('li')
                            .removeClass('selected')
                            .addClass('completed');
                        page.PageContainer.find('.campaign .nav-item a[name="schedule"]')
                            .closest('li')
                            .addClass('selected');
                    }
                }, 400);
            }
        });
    } else {
        param.topButtons.push({
            'html': ' <span class="btn btn-danger btn-sm close-editor"><i class="far fa-times close-marketing"></i></span>',
            'onClick': function (e, data) {
                let currentPage = _page.body.find('.page-content[data-uid=' + _page.nav.find('.nav-item.active').attr('data-uid') + ']');
                if (currentPage.find('.switch-category[groupid="1"]') != undefined && currentPage.find('.switch-category[groupid="' + page.GroupID + '"]').length > 0)
                    _page.body.find('.page-content[data-uid=' + _page.nav.find('.nav-item.active').attr('data-uid') + '] .switch-category[groupid="' + page.GroupID + '"]').trigger("click");
                else
                    currentPage.find('.btn-template-search').trigger("click");

                $(e).closest('.marketing-template-editor').remove();
                $('body').removeClass('fixedbody');
            }
        });
    }
    new TemplateEditor(param, campaignData);
}

_marketing.BindTemplateButtonEvent = function (page, groupID, templateData) {
    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').off('click', '.marketing-preview');
    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').
        on('click', '.marketing-preview', function (e) {
            e.stopPropagation();
            let id = $(this).closest('li').attr('templateblockid');
            let data = templateData.filter(p => p._id == id)[0];
            var iframe = document.createElement('iframe');
            document.body.appendChild(iframe);
            let doc = iframe.contentDocument || iframe.contentWindow.document;
            doc.open();
            doc.write(data.TemplateBlockContent);
            doc.close();
            _templateEditor.ShowPreview($(doc));
            $(iframe).remove();
            // data.TemplateBlockContent = data.TemplateBlockContent.replace(new RegExp('##URL##', 'g'), window.location.origin);
            //  _marketing.ShowTemplatePreview(data.TemplateBlockContent, data.PageStyle);
        })
    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').off('click', '.template-clone');
    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').
        on('click', '.template-clone', function (e) {
            e.stopPropagation();
            let id = $(this).closest('li').attr('templateblockid');
            let data = templateData.filter(p => p._id == id)[0];
            let param = {
                'templateName': data.TemplateBlockName,
                'content': data.TemplateBlockContent,
                'pageStyle': data.PageStyle,
                'IsUpdate': false,
                'popUpName': 'Clone Template',
                'actionName': 'clone'
            };
            _marketing.ValidateTemplate(page, param);
        })
    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').off('click', '.template-delete');
    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').
        on('click', '.template-delete', function (e) {
            e.stopPropagation();
            let id = $(this).closest('li').attr('templateblockid');
            if (id == undefined || id == '') {
                swal('Something went wrong', { icon: "info" });
                return;
            }
            let url = { 'url': '/api/marketing/DeleteTemplate', 'type': 'post', 'contentType': 'application/json' };
            url.data = { 'templateID': id };
            _common.ConfirmDelete({ message: 'Once deleted, you will not be able to recover this template!' }).then((confirm) => {
                if (confirm) {
                    _common.FetchApiByParam(url, function (res) {
                        if (res.statuscode == 200) {
                            page.GroupID = 2;
                            _marketing.InitializeTemplateWithPaging(page);
                            swal(res.message, { icon: "success" });
                        }
                    })
                }
            })
        })
}

_marketing.ShowTemplatePreview = function (content, pageStyle) {
    let container = $('<div class="marketing-editing-block-container"><div class="marketing-full-edit"></div></div>');
    container.find('.marketing-full-edit').append(content);
    container.find('.marketing-full-edit').attr('style', pageStyle);
    _templateEditor.ShowPreview(container);
}


_marketing.ValidateTemplate = function (page, param) {

    if (param.EditUnsubsPage && param.actionName == 'SaveUnsubscribePage') {
        _common.Loading(true);
        _marketing.SaveUnsubscribePageTemplate(page, param, function (res) {
            if (res.statuscode === 200) {
                param.closeEditor.BtnSave.closest('.marketing-template-editor').remove();
                $('body').removeClass('fixedbody');
                let templateId = null;
                _admin.GetUnsubscribePageIdsfromCompany().then(function (unsubscribeIDs) {
                    if (unsubscribeIDs && unsubscribeIDs.length > 0) {
                        templateId = unsubscribeIDs;
                    }
                    _admin.GetUnsubscribePage(templateId, param.SelectedDomainID).then(
                        template => {
                            page.PageContainer.find('.unsubscribePageTemplate').html(template.TemplateShow);
                            page.PageContainer.find('.unsubscribePageTemplate').find('[contenteditable="true"]').attr('contenteditable', 'false');
                            page.PageContainer.find('.unsubscribePageTemplate').on('click', 'a', function (e) {
                                e.preventDefault();
                            });
                            page.PageContainer.find('.unsubscribePageTemplate').find('button, textarea').attr('disabled', true);
                            page.PageContainer.find('.btnPreviewUnsubscribePage').unbind('click');
                            page.PageContainer.find('.btnPreviewUnsubscribePage').click(function () {
                                window.open('noauth/api/mail/emailsubscription?cid=' + _initData.Company._id.toString() + '&did=' + param.SelectedDomainID.toString() + '&mode=preview', "_blank");
                            });
                            page.PageContainer.find('.btnEditUnsubscribePage').unbind('click');
                            page.PageContainer.find('.btnEditUnsubscribePage').click(function () {
                                let param = page.param;
                                param.EditUnsubsPage = true;
                                param.content = template.TemplateEdit;
                                _marketing.OpenEditor(page, param);
                            });
                            page.PageContainer.GetMarketingCampaignCategoryList(param.SelectedDomainID);
                        }
                    );
                });
                swal(res.message, { icon: "success" });
                _common.Loading(false);
            } else {
                _common.Loading(false);
                swal(res.message, { icon: "info" });
            }
        });
    } else if (page.GroupID == 2 && param.actionName == 'Save') {
        _common.Loading(true);
        param.IsUpdate = true;

        _marketing.SaveTemplate(page, param, function (res) {
            if (res.statuscode === 200) {
                _common.Loading(false);
                swal(res.message, { icon: "success" });
            } else {
                _common.Loading(false);
                swal(res.message, { icon: "info" });
            }
        });
    } else {
        let tempHtml = '<div class="form-group mb-3 mt-3"><label>Template Name</label><input type="text" class="txtTemplateName form-control" maxlength="50"  style="padding:6px;font-size: 12px;"></div>';
        let saveTemplatePopup = _common.DynamicPopUp({ title: param.popUpName, body: tempHtml });
        saveTemplatePopup.find('.modal-dialog').addClass('modal-sm')
        if (param.actionName == 'clone')
            saveTemplatePopup.find('.txtTemplateName').val(param.templateName);
        saveTemplatePopup.modal('show');

        saveTemplatePopup.modal().find('.modal-dialog').removeClass('modal-lg');
        saveTemplatePopup.find('.btnSave').on('click', function () {
            _common.Loading(true);
            let templateName = saveTemplatePopup.find('.txtTemplateName').val();
            if (!templateName) {
                _common.Loading(false);
                swal('Please enter tempalate name.', { icon: "info" });
                return false;
            }
            let url = { 'url': '/api/marketing/CheckTemplateNameExist', 'type': 'post', 'contentType': 'application/json' };
            url.data = { 'TemplateBlockName': saveTemplatePopup.find('.txtTemplateName').val() }
            _common.FetchApiByParam(url, function (res) {
                if (res.statuscode == 200) {
                    if (res.data.isTemplateExists === false) {
                        param.templateName = templateName;
                        param.IsUpdate = false;
                        _marketing.SaveTemplate(page, param, function (res) {
                            if (res.statuscode === 200) {
                                _common.Loading(false);
                                swal(res.message, { icon: "success" });
                                if (page.GroupID != 3) { page.GroupID = 2; }
                                page.PrvTemplateID = res.data.insertedId;
                                _marketing.InitializeTemplateWithPaging(page);
                            } else {
                                _common.Loading(false);
                                swal(res.message, { icon: "info" });
                            }
                        });
                        saveTemplatePopup.modal('hide');
                    }
                    else {
                        _common.Loading(false);
                        swal(res.message, { icon: "info" });
                    }
                }
                else {
                    _common.Loading(false);
                    swal(res.message, { icon: "info" });
                }
            })
        })
    }
}

_marketing.SaveTemplate = function (page, param, callback) {
    let url = { 'url': '/api/marketing/InsertTemplatePage', 'type': 'post', 'contentType': 'application/json' };

    url.data = {
        'TemplateBlockName': param.templateName,
        'TemplateBlockContent': param.content, 'GroupID': 2,
        'IsActive': true, 'PageStyle': param.pageStyle,
        'IsUpdate': param.IsUpdate
    };
    _marketing.InsertThumbNail(page, param, function (documentID) {
        url.data.Thumbnail = documentID + '.png';
        _common.FetchApiByParam(url, function (res) {
            callback(res);
        })
    })
}

_marketing.SaveUnsubscribePageTemplate = function (page, param, callback) {
    let url = { 'url': '/api/admin/marketingSaveUnsubscribePage', 'type': 'post', 'contentType': 'application/json' };
    let TemplateEmailContent = _templateEditor.RenderEmailHTML(param.content);
    let mainTable = $('<div>' + TemplateEmailContent + '</div>').find('table')[0];
    $(mainTable).attr({ 'style': param.content.find('body').attr('style'), 'height': '' });

    TemplateEmailContent = '<title class="d-none">' + _initData.Company.Name + ' | Marketing Email Prefrences</title>' + $(mainTable)[0].outerHTML;
    url.data = { 'UnsubscribePageBody': TemplateEmailContent, 'EditBlockContent': param.content.find('body')[0].outerHTML, 'DomainId': param.SelectedDomainID };
    _common.FetchApiByParam(url, function (res) {
        callback(res);
    });
}

_marketing.InsertThumbNail = function (page, param, callbackThumb) {
    _marketing.CreateSnapShotOfTemplate(param, function (base64String) {
        _common.Resize(base64String, 300, 'jpeg', function (callback) {
            let url = { 'url': '/api/drive/uploadfile', 'type': 'post', 'contentType': 'application/json' };
            let data = callback.split(";base64,")[1];
            let fileSize = Math.round(data.length / 1024);

            url.data = {
                'fileName': param.templateName, 'size': fileSize,
                'type': "image/jpeg", 'objectname': 'MediaImages',
                'confirmAttachment': true,
                'filetype': 'thumbnail',
                'data': data
            };
            _common.UploadFiles(url, function (res) {
                if (res.fileid) {
                    callbackThumb(res.fileid);
                }
            })
        })
    });
}
_marketing.CreateSnap = function (iframedoc, iframe, responceCallback) {
    html2canvas(iframedoc, {
        useCORS: true,
        height: $(iframedoc).height(),
        windowHeight: $(iframedoc).height(),
    }).then(canvas => {
        iframe.remove();
        responceCallback(canvas.toDataURL('image/jpeg', 0.5));

    });

}
_marketing.CreateSnapShotOfTemplate = function (container, responceCallback) {

    let iframe = $('<iframe/>');
    let imagesLoaded = 0;
    $('body').append(iframe);

    let htm = container.content;
    htm = htm.replace(new RegExp('http://img.youtube.com/vi/', 'g'), window.location.origin + '/api/drive/youtubeimage/');
    htm = htm.replace(new RegExp(_constantClient.CDN + '/public/' + _initData.Company._id, 'g'), window.location.origin + '/api/drive/dbimages');
    let doc = iframe[0].contentDocument || iframe[0].contentWindow.document;
    doc.open();
    doc.write(htm);
    doc.close();
    let iframedoc = iframe[0].contentDocument.documentElement || iframe[0].contentWindow.document.body;
    let allImages = $(iframedoc).find('img').length;

    if ((container.GroupID == 1 || container.GroupID == 2) && allImages > 0) {
        $(iframedoc).find('img').on('load', function (event) {
            imagesLoaded++;
            if (imagesLoaded == allImages) {
                _marketing.CreateSnap(iframedoc, iframe, responceCallback)
            }
        });
    } else {
        setTimeout(function () {
            _marketing.CreateSnap(iframedoc, iframe, responceCallback)
        }, 400);
    }
}

//**************************************************** */Marketing List Code Start**********************************************//
var _marktingRecipientsList = {};
var _marketingBounceEmailList = {};

_marktingRecipientsList.OpenPage = function (page) {
    new Page({ 'pageName': 'MarketingRecipientsList' });
}

_marktingRecipientsList.Init = function (page) {
    _marktingRecipientsList.GridPage = page;
    page.marktingRecipientsList = {};
    let block = ('<div class="row">\
	              <div class="col-12">\
                    <div class="form-group vld-parent">\
                           <label data-lng="UploadExcelFile" class="loaded small">Enter List Name</label>\
                           <input type="text" title="List Name" class="txtListName loaded form-control form-control-sm msp-ctrl" placeholder="List Name" data-required />\
                           </div>\
                    <div class="form-group vld-parent"><label  class="small">Upload CSV File</label>\
                      <div class="custom-file">\
                        <label class="form-control form-control-sm custom-file-label mb-4 browselabel h-auto">\
                        <input type="file" accept=".csv" title="file" class="fileUploaded d-none " data-required name="fileUploaded" />\
                        <label class="lblFileUpload"><span></span><i class="fas fa-times pl-2 removefile d-none"></i></label> \
                      </div><div class="mt-3 mb-3"><button type="button" class="btn btn-primary btn-sm contact-upload d-inline-block">Upload</button></div>\
                    </div>\
                    <div class="form-group  vld-parent custom-column-div">\
                       <label class="small">Select Email Field</label>  <select class="ddlField form-control form-control-sm" />\
                    </div>\
                    <div class="form-group  vld-parent duplicate-email">\
                        <label class="small lblTitle">Duplicate Email List</label>\
                        <div class="list-group"> <ul class="ul-duplicate-email pl-0"></ul></div>\
                        <label class="small text-danger lblError">Please remove dulicate email in your list or continue.</label>\
                    </div>\
                    <div class="form-group  vld-parent invalid-email">\
                        <label class="small lblTitle">Invalid Email List</label>\
                        <div class="list-group"> <ul class="ul-invalid-email pl-0"></ul></div>\
                        <label class="small text-danger lblError">Please correct invalid email in your list or continue.</label>\
                    </div>\
                  <hr/ class="mb-3 d-none"><div class="text-center mt-n5 d-none"><span class="d-inline-block bg-white p-4 ">OR</span></div><h6 class="d-none">Import list from our dynamic database</h6><button class="btn btn-outline-primary btn-sm d-none">Select from database</button></div>\
                  </div>');
    let popup = _common.DynamicPopUp({ title: 'Import Marketing Recipients List', body: block });
    popup.find('.modal-dialog').removeClass('modal-lg');
    popup.find('.modal-dialog').addClass('modal-mg');
    popup.find('.modal-footer').append('<button type="button" class="btn btn-primary btnContinue" style="display: none;">Continue</button>');
    popup.find('.modal-footer, .custom-column-div,.duplicate-email,.invalid-email').hide();

    popup.find('.fileUploaded').on('change', function () {
        let files = $(this)[0].files;
        popup.find('.modal-footer .btnContinue').hide();
        popup.find('.modal-footer .btnSave').show();
        if (files.length > 0) {
            if (popup.find('.lblFileUpload').length == 0) {
                popup.find('.custom-file .browselabel').removeClass('h-100').addClass('h-auto').append(' <label class="lblFileUpload"><span></span><i class="fas fa-times pl-2 removefile d-none"></i></label>')
            }
        } else if (files.length == 0) {
            popup.find('.browselabel').addClass('custom-file-label')
        }
        popup.find('.lblFileUpload span').html($(this).val());
        popup.find('.modal-footer, .custom-column-div, .duplicate-email, .invalid-email').hide();
    })

    popup.find('.contact-upload').on('click', function () {
        if (_common.Validate(popup)) {
            let url = { 'url': '/api/marketing/CheckMarketingRecipientsListNameExist', 'type': 'post', 'contentType': 'application/json' };
            url.data = { 'RecipientsListName': popup.find('.txtListName').val() }
            _common.FetchApiByParam(url, function (res) {
                if (res.data.isRecipientsListExists == false) {
                    page.marktingRecipientsList.FileName = popup.find('.txtListName').val();
                    _marktingRecipientsList.UploadFile(page, popup);
                } else {
                    swal(res.message, { icon: "info" });
                }
            });
        }
    })
    popup.modal("show");
}

_marktingRecipientsList.UploadFile = function (page, popup) {
    let fileUpload = popup.find(".fileUploaded")[0];
    if (fileUpload.files.length == 0) {
        swal("Please select csv file", { icon: "info" });
        return false;
    } else if (fileUpload.files.length == 1) {
        let separatedfileType = fileUpload.files[0].name.split('.');
        let fileType = separatedfileType[separatedfileType.length - 1];
        if (fileType != 'csv') {
            swal("Please upload a CSV file.", { icon: "info" });
            return false;
        }
    }
    let regex = /(\,|\r?\n|\r|^)(?:"([^"]*(?:""[^"]*)*)"|([^"\,\r\n]*))/;
    if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            let reader = new FileReader();
            reader.onload = function (e) {
                let rows = e.target.result.split("\n");
                page.marktingRecipientsList.CsvData = rows;
                _marktingRecipientsList.MapCSVColumn(page, popup);
            }
            reader.readAsText(fileUpload.files[0]);
        }
        else {
            swal("This browser does not support HTML5.", { icon: "info" });
        }
    } else {
        swal("Please upload a valid CSV file.", { icon: "info" });
    }
}

_marktingRecipientsList.MapCSVColumn = function (page, popup) {
    page.marktingRecipientsList.CSVColumn = [];
    let recipientsListLimit = _initData.Company.Setting.MarketingSetting.RecipientsListLimit;
    if (page.marktingRecipientsList.CsvData && page.marktingRecipientsList.CsvData.length > 0) {
        let chkArray = page.marktingRecipientsList.CsvData[0].split(",");
        chkArray = chkArray.filter(p => p.trim() != "");
        popup.find('.duplicate-email,.invalid-email').hide();
        popup.find('.ddlField,.list-group .ul-duplicate-email,.list-group .ul-invalid-email').html('');
        if (chkArray.length > recipientsListLimit.Columns) {
            swal(`No of columns can not be more than ${recipientsListLimit.Columns} in the CSV file.`, { icon: "info" });
            return;
        }
        if (page.marktingRecipientsList.CsvData.filter(p => p.trim() != "").length - 1 > recipientsListLimit.Rows) {
            swal(`No of rows can not be more than ${recipientsListLimit.Rows} in the CSV file.`, { icon: "info" });
            return;
        }
        let isDuplicateColumn = false;
        if (chkArray.map(p => (p.indexOf('.') !== -1 || p.indexOf('"') !== -1)).find(p => p == true)) {
            swal('Please remove dot(.) or double quotes(") from column name.', { icon: "info" });
            return;
        }
        for (let k = 0; k < chkArray.length; k++) {
            let colValue = chkArray[k];
            if (popup.find('.ddlField option[value="' + colValue.trim() + '"]').length == 1) {
                popup.find('.list-group .ul-duplicate-email').append($('<li class="list-group-item p-2 small">' + colValue.trim() + '</li>'))
                isDuplicateColumn = true;
            }
            popup.find('.ddlField').append('<option  value="' + colValue.trim() + '" > ' + colValue.trim() + "</option>");
            page.marktingRecipientsList.CSVColumn.push({ 'ColumnName': colValue.toString().trim(), 'Index': k });
        }
        popup.find('.modal-footer').show();
        popup.find('.custom-column-div').show();
        popup.find('.btnSave').css('enabled', 'enabled');
        popup.find('.btnSave').on('click', function () {
            page.marktingRecipientsList.EmailFieldName = popup.find('.ddlField').val();
            swal({
                title: "Are you sure?",
                text: 'your email field is ' + page.marktingRecipientsList.EmailFieldName,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((confirm) => {
                if (confirm) {
                    _common.Loading(true);
                    setTimeout(function () {
                        _marktingRecipientsList.ImportCSVData(page, popup, function (res) {
                            if (res.statuscode == 200) {
                                swal(res.message, { icon: "success" });
                                popup.modal("hide");
                                _marktingRecipientsList.GridPage.ReloadData();
                                _common.Loading(false);
                            } else if (res.statuscode == 422) {
                                _common.Loading(false);
                            } else {
                                swal(res.message, { icon: "info" });
                                _common.Loading(false);
                            }
                        });
                    }, 200);
                }
            });
        });
        if (isDuplicateColumn) {
            popup.find('.duplicate-email').show();
            popup.find('.duplicate-email .lblTitle').text('Duplicate Column');
            popup.find('.duplicate-email .lblError').text('Please remove duplicate column in your list.');
            popup.find('.btnSave').attr('disabled', 'true');
        }
        popup.modal("show");
    }
}

_marktingRecipientsList.ImportCSVData = function (page, popup, callback) {
    let csvData = page.marktingRecipientsList.CsvData;
    let length = csvData.length;
    let csvJSON = [];
    let validCsvJSON = [];
    let invalidCsvJSON = [];
    let duplicateCsvJSON = [];
    let columns = page.marktingRecipientsList.CSVColumn;
    let rowNo = 1;
    let isEmail = true;
    let invalidEmailCount = 0;
    for (let i = 1; i < length; i++) {
        // let dataRow = csvData[i].length > 0 ? csvData[i].match(/((?:"(?:"{2}|,|\n|[^"]*)+")|(?:[^,"\n]+))/g) : [];
        let tempRow = csvData[i].length > 0 ? ImportData.CSVRowtoArray(csvData[i]) : [];
        // dataRow = dataRow.filter(p => p.trim() != "");
        dataRow = []
        tempRow.forEach(p => {
            if (p == "") {
                dataRow.push("")
            } else {
                dataRow.push(p.trim())
            }
        });
        if (dataRow.length == 0) {
            continue;
        }
        let jsonObj = {};
        jsonObj["RowNo"] = rowNo;
        let isBlank = false;
        for (let dr = 0; dr < dataRow.length; dr++) {
            if (_common.isEmptyObj(columns[dr])) {
                isBlank = true;
            } else {
                jsonObj[columns[dr].ColumnName] = dataRow[dr];
            }
        }
        if (jsonObj && !isBlank) {
            rowNo = rowNo + 1;
            jsonObj["PrimaryKey"] = page.marktingRecipientsList.EmailFieldName;
            let emailField = jsonObj[page.marktingRecipientsList.EmailFieldName.toString()];
            jsonObj['IsValidEmail'] = isEmail = _common.isValidEmailAddress(emailField.trim()) == true ? true : false;
            if (!isEmail)
                invalidEmailCount++;
            csvJSON.push(jsonObj);
        }
    }
    if (invalidEmailCount == csvJSON.length) {
        swal("Please select valid email field.", { icon: "error" });
        popup.find('.invalid-email, .duplicate-email').hide();
        _common.Loading(false);
        return false;
    }
    popup.find('.ul-duplicate-email,.ul-invalid-email').html('');
    let isDuplicateEmailExists = false;
    let isInvalidEmailExists = false;
    for (let i = 0; i < csvJSON.length; i++) {
        let len = csvJSON.filter(p => p[page.marktingRecipientsList.EmailFieldName.toString()] == csvJSON[i][page.marktingRecipientsList.EmailFieldName.toString()]).length;
        if (len > 1 && csvJSON[i].IsValidEmail) {
            isDuplicateEmailExists = true;
            if (popup.find('.ul-duplicate-email li:contains(' + csvJSON[i][page.marktingRecipientsList.EmailFieldName.toString()].trim() + ')').length == 0)
                popup.find('.ul-duplicate-email')
                    .append($('<li class="list-group-item d-flex justify-content-between align-items-center p-2 small" >' + csvJSON[i][page.marktingRecipientsList.EmailFieldName.toString()].trim() + ' <span class="badge badge-primary badge-pill">' + len + '</span></li>'));
        } else if (!csvJSON[i].IsValidEmail) {
            isInvalidEmailExists = true;
            if (popup.find('.ul-invalid-email li:contains(' + csvJSON[i][page.marktingRecipientsList.EmailFieldName.toString()].trim() + ')').length == 0)
                popup.find('.ul-invalid-email')
                    .append($('<li class="list-group-item d-flex justify-content-between align-items-center p-2 small" >' + csvJSON[i][page.marktingRecipientsList.EmailFieldName.toString()].trim() + ' <span class="badge badge-danger badge-pill">' + len + '</span></li>'));
        }
    }
    if (isDuplicateEmailExists) {
        popup.find('.modal-footer .btnContinue').show();
        popup.find('.modal-footer .btnSave').hide();
        popup.find('.duplicate-email').show();
        if (isInvalidEmailExists)
            popup.find('.invalid-email').show();
        callback({ 'statuscode': 422 });
    } else if (isInvalidEmailExists) {
        popup.find('.modal-footer .btnContinue').show();
        popup.find('.modal-footer .btnSave').hide();
        popup.find('.invalid-email').show();
        callback({ 'statuscode': 422 });
    } else {
        let newJson = {};
        newJson = { 'Name': page.marktingRecipientsList.FileName, 'FileData': csvJSON };
        let paramNew = { 'url': '/api/marketing/InsertMarketingRecipientsList', 'type': 'post', 'contentType': 'application/json' };
        paramNew.data = newJson;
        paramNew.data["CreatedBy"] = _initData.User.FirstName + " " + _initData.User.LastName;
        paramNew.data["ColumnList"] = newJson.FileData[0];
        _common.FetchApiByParam(paramNew, function (res) {
            callback(res);
        });
    }
    popup.find('.custom-column-div').on('change', function () {
        popup.find('.modal-footer .btnContinue').hide();
        popup.find('.modal-footer .btnSave').show();
        popup.find('.duplicate-email, .invalid-email').hide();
    });
    popup.find('.btnContinue').on('click', function () {
        swal({
            title: "If you wish to continue",
            text: "duplicate & invalid emails will be removed from the list",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((confirm) => {
            if (confirm) {
                _common.Loading(true);
                setTimeout(function () {
                    let newJson = {};
                    for (let i = 0; i < csvJSON.length; i++) {
                        let chkDuplicate = csvJSON.filter(p => p[page.marktingRecipientsList.EmailFieldName.toString()] == csvJSON[i][page.marktingRecipientsList.EmailFieldName.toString()]);
                        if (chkDuplicate.length > 1 && csvJSON[i].IsValidEmail) {
                            if (duplicateCsvJSON.filter(p => p[page.marktingRecipientsList.EmailFieldName.toString()] == chkDuplicate[0][page.marktingRecipientsList.EmailFieldName.toString()]).length == 0) {
                                validCsvJSON.push(chkDuplicate[0]);
                                for (let j = 1; j < chkDuplicate.length; j++) {
                                    duplicateCsvJSON.push(chkDuplicate[j]);
                                }
                            }
                        } else if (!csvJSON[i].IsValidEmail) {
                            invalidCsvJSON.push(csvJSON[i]);
                        } else {
                            validCsvJSON.push(csvJSON[i]);
                        }
                    }
                    newJson = { 'Name': page.marktingRecipientsList.FileName, 'FileData': validCsvJSON, 'InvalidEmailFileData': invalidCsvJSON };
                    let paramNew = { 'url': '/api/marketing/InsertMarketingRecipientsList', 'type': 'post', 'contentType': 'application/json' };
                    paramNew.data = newJson;
                    paramNew.data["CreatedBy"] = _initData.User.FirstName + " " + _initData.User.LastName;
                    paramNew.data["ColumnList"] = newJson.FileData[0];
                    _common.FetchApiByParam(paramNew, function (res) {
                        callback(res);
                    });
                }, 200);
            }
        });
    });
}

//Event :  Init  - PageName : MarketingRecipientsDetail
_marktingRecipientsList.LoadRecipientDetails = function (page) {
    _common.ContainerLoading(true, page.PageContainer.find('.recipients-container'));
    let arrayContactListID = [{ '_id': page.param._id }];
    _marktingRecipientsList.GetRecipientsList(arrayContactListID, function (res) {
        if (res.statuscode == 200) {
            if (res) {
                page.RecipientsList = res.data[0].ColumnList;
                page.data = res.data[0];
            }
            _marktingRecipientsList.CreateRecipientDetailsTable(page);
            _common.ContainerLoading(false, page.PageContainer.find('.recipients-container'));
        }
        else {
            swal(res.message, { icon: "info" });
            _common.ContainerLoading(false, page.PageContainer.find('.recipients-container'));
        }
    })
}

_marktingRecipientsList.GetRecipientsList = function (listArray, callback) {
    let paramNew = { 'url': '/api/marketing/FetchRecipientsList', 'type': 'post', 'contentType': 'application/json' };
    paramNew.data = [];
    paramNew.data = listArray;
    _common.FetchApiByParam(paramNew, function (res) {
        callback(res);
    });
}

_marktingRecipientsList.GetRecipientsData = function (slf, callback) {
    let paramNew = { 'url': '/api/marketing/GetRecipientsData', 'type': 'post', 'contentType': 'application/json' };
    paramNew.data = {};
    paramNew.data = { 'recipientDetailID': slf.param._id, 'HeaderFilters': slf.HeaderFilters };
    _common.FetchApiByParam(paramNew, function (res) {
        callback(res);
    });
}

_marktingRecipientsList.CreateRecipientDetailsTable = function (page) {
    let data = page.data.FileData;
    if (data.length < 10000)
        page.PageContainer.find('.btnInsert').show();
    else
        page.PageContainer.find('.btnInsert').hide();
    if (page.data.InvalidEmailFileData && page.data.InvalidEmailFileData.length != 0)
        page.PageContainer.find('.btnShowInvalidEmail').show();
    else if (page.data.InvalidEmailFileData && page.data.InvalidEmailFileData.length == 0)
        page.PageContainer.find('.btnShowInvalidEmail').hide();
    page.PageContainer.find('.total-count').text('0 items');
    _page.nav.find('.nav-item.active[data-uid="' + _page.nav.find('.nav-item.active').attr('data-uid') + '"] .title').text(page.data.Name);
    page.PageContainer.find('.file-name').text('');
    page.PageContainer.find('.file-name').text('Recipients Detail for ' + page.data.Name);
    page.PageContainer.find('.total-count').text(data.length + ' items');
    //page.RecipientsArray = data.sort((a, b) => (a.RowNo < b.RowNo) ? 1 : (b.RowNo < a.RowNo) ? -1 : 0);
    page.RecipientsArray = data;
    let length = data.length;
    let pagingIndex = length / (Number(_initData.User.MySettings.GridRows) || 10);
    pagingIndex = parseInt(pagingIndex) < pagingIndex ? parseInt(pagingIndex) + 1 : parseInt(pagingIndex);
    page.RecipientsArray.pager = { index: 1, pageSize: pagingIndex, totalPage: length };
    _marktingRecipientsList.FillGridData(page);
    page.PageContainer.find('.txtSearchInTable').unbind('keyup')
    page.PageContainer.find('.txtSearchInTable').keyup(function () {
        var searchTerm = $(this).val();
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

        $.extend($.expr[':'], {
            'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        page.PageContainer.find(".recipients-table  .data-row").not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'false');
            $(this).find(".cellDisp").not(":containsi('" + searchSplit + "')").each(function (e) {
                $('.cellDisp').removeClass('text-found-marketing-contact');
            });
        });

        page.PageContainer.find(".recipients-table  .data-row:containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'true');
            $(this).find(".cellDisp:containsi('" + searchSplit + "')").each(function (e) {
                if (searchSplit != '')
                    $(this).addClass('text-found-marketing-contact');
                else
                    $('.cellDisp').removeClass('text-found-marketing-contact');
            });
        });

        var jobCount = page.PageContainer.find('.recipients-table .data-row[visible="true"]').length;
        page.PageContainer.find('.total-count').text(jobCount + ' item');

        if (jobCount == '0') { page.PageContainer.find('.no-result').show(); }
        else { page.PageContainer.find('.no-result').hide(); }
    });

    page.PageContainer.find('.btnInsert').unbind('click');
    page.PageContainer.find('.btnInsert').on('click', function () {
        let createRecipients = '<div class="row">';
        let max = Math.max.apply(Math, page.RecipientsArray.map(function (o) { return o.RowNo + 1; }));
        let rowCount = max == -Infinity ? 1 : max;
        let singleObj = page.RecipientsList;
        for (k in singleObj) {
            if (singleObj.hasOwnProperty(k) && k != "RowNo" && k != "PrimaryKey" && k != "IsValidEmail") {
                if (_common.CompareStringValue(k, singleObj.PrimaryKey)) {
                    createRecipients += '<div class="form-group col-sm-6 vld-parent ">\
                   <label class="small">'+ k + '</label>\
                   <input type="text" RowNo="'+ rowCount + '" title="' + k + '" data-field="' + singleObj.PrimaryKey + '" class="form-control form-control-sm msp-ctrl-email email-field" placeholder="Enter ' + k + '"  data-required  data-field="' + k + '">\
                   \</div>'
                } else {
                    createRecipients += '<div class="form-group col-sm-6">\
                    <label class="small">'+ k + '</label>\
                    <input type="text" RowNo="'+ rowCount + '" title="' + k + '" class="form-control form-control-sm msp-ctrl" placeholder="Enter ' + k + '"  data-field="' + k + '">\
                    \</div>'
                }
            }
        }
        createRecipients += '</div>';
        let createRecipientsPopup = _common.DynamicPopUp({ title: 'Add Recipients', body: createRecipients });
        createRecipientsPopup.find('.btnSave').on('click', function () {
            if (_common.Validate(createRecipientsPopup)) {
                _common.Loading(true);
                let rowJson = {};
                rowJson["RowNo"] = rowCount;
                createRecipientsPopup.find('.modal-body input').each(function () {
                    if ($(this).hasClass('email-field')) {
                        rowJson['IsValidEmail'] = _common.isValidEmailAddress($(this).val().trim()) ? true : false;
                        rowJson['PrimaryKey'] = $(this).attr('data-field');
                    }
                    rowJson[$(this).attr('data-field')] = $(this).val();
                })
                let obj = { mode: 'insert', recipientDetailID: page.param._id, rowJson: rowJson };
                _marktingRecipientsList.InsertRecipientsRecord(page, obj, function (res) {
                    if (res.statuscode == 200) {
                        createRecipientsPopup.modal('hide');
                        _marktingRecipientsList.CreateRecipientDetailsTable(page);
                        swal(res.message, { icon: res.status });
                        _common.Loading(false);
                    }
                    else {
                        swal(res.message, { icon: "info" });
                        _common.Loading(false);
                    }
                });
            }
        })
        createRecipientsPopup.modal('show');
    });

    page.PageContainer.find('.btnDelete').unbind('click');
    page.PageContainer.find('.btnDelete').on('click', function () {
        let array = [];
        page.PageContainer.find('.recipients-table tr td .chkRecipients:checked').each(function () {
            array.push(parseInt($(this).attr('index')));
        })
        if (array.length == 0) {
            swal('Please select record from grid.', { icon: "info" });
            return false;
        }
        _common.ConfirmDelete({ message: 'If you delete this it will be permanently removed for all users.' }).then((confirm) => {
            if (confirm) {
                _common.Loading(true);
                let rowJson = {};
                let obj = { mode: 'delete', recipientDetailID: page.param._id, rowJson: rowJson };
                obj.RecipientsIndexList = array;
                _marktingRecipientsList.InsertRecipientsRecord(page, obj, function (res) {
                    if (res.statuscode == 200) {
                        _marktingRecipientsList.CreateRecipientDetailsTable(page);
                        swal(res.message, { icon: res.status });
                        _common.Loading(false);
                    }
                    else {
                        swal(res.message, { icon: "info" });
                        _common.Loading(false);
                    }
                });
            }
        });
    });

    // Refresh grid
    page.PageContainer.find('.btnRefresh').unbind('click');
    page.PageContainer.find('.btnRefresh').on('click', function () {
        _marktingRecipientsList.LoadRecipientDetails(page);
    });

    // Search grid
    page.PageContainer.find('.gird-button-list .btnSearch').unbind('click');
    page.PageContainer.find('.gird-button-list .btnSearch').click(function () {
        _common.ContainerLoading(true, page.PageContainer.find('.recipients-container'));
        let self = page;
        page.PageContainer.find('.txtSearchInGrid').val('');
        $(this).closest('.recipients-container').find('.grid-table .chkSelectAll, .grid-table .chkSelect').prop('checked', false);
        if ($(this).closest('.grid-body').find('.select-all-grid').length != 0)
            $(this).closest('.grid-body').find('.select-all-grid').remove();
        self.HeaderFilters = [];
        let trFilter = page.PageContainer.find('.filter-row');
        trFilter.find('th [data-field]').each(function () {
            let fieldName = $(this).closest('th').attr('data-field');
            let val = $(this).val();
            if (val !== undefined && val !== null && val.length > 0) {
                self.HeaderFilters.push({ 'Name': fieldName, 'Value': val });
            }
        });
        _marktingRecipientsList.GetRecipientsData(self, function (res) {
            if (res.statuscode == 200) {
                page.RecipientsArray = res.data;
                page.PageContainer.find('.total-count').text(res.data.length + ' items');
                let length = res.data.length;
                let pagingIndex = length / (Number(_initData.User.MySettings.GridRows) || 10);
                pagingIndex = parseInt(pagingIndex) < pagingIndex ? parseInt(pagingIndex) + 1 : parseInt(pagingIndex);
                page.RecipientsArray.pager = { index: 1, pageSize: pagingIndex, totalPage: length };
                _marktingRecipientsList.AppendRow(page);
                _marktingRecipientsList.Paging(page);
                _common.ContainerLoading(false, page.PageContainer.find('.recipients-container'));
            } else {
                swal('Something went wrong while searching records.', { icon: "error" });
                _common.ContainerLoading(false, page.PageContainer.find('.recipients-container'));
            }
        });
    });

    page.PageContainer.find('.recipients-table thead').unbind('keypress');
    page.PageContainer.find('.recipients-table thead').on("keypress", '.filter-row .msp-ctrl', function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $(this).closest('.recipients-container').find('.gird-button-list .btnSearch').trigger('click');
        }
    });

    page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').unbind('change');
    page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').on('change', function () {
        page.PageContainer.find('.recipients-table tr  .chkRecipients').closest('.data-row').removeClass('selected');
        page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').closest('.head-row').removeClass('selected');
        if ($(this).prop('checked')) {
            page.PageContainer.find('.recipients-table tr  .chkRecipients').prop('checked', true);
            page.PageContainer.find('.recipients-table tr  .chkRecipients:checked').closest('.data-row').addClass('selected');
            page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').closest('.head-row').addClass('selected');
        }
        else {
            page.PageContainer.find('.recipients-table tr  .chkRecipients').prop('checked', false);
        }
    });

    page.PageContainer.find('.recipients-table .data-row .chkRecipients').unbind('change');
    page.PageContainer.find('.recipients-table .data-row .chkRecipients').on('change', function () {
        $(this).closest('.data-row').removeClass('selected');
        page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').prop('checked', false);
        page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').closest('.head-row').removeClass('selected');
        if ($(this).prop('checked')) {
            page.PageContainer.find('.recipients-table .data-row .chkRecipients:checked').closest('.data-row').addClass('selected');
            if (page.PageContainer.find('.recipients-table .data-row .chkRecipients:not(:checked)').length == 0) {
                page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').prop('checked', true);
                page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients:checked').closest('.head-row').addClass('selected');
            }
        }
    });

    page.PageContainer.find('.btnShowInvalidEmail').unbind('click');
    page.PageContainer.find('.btnShowInvalidEmail').on('click', function () {
        new Page({ 'pageName': 'MarketingInvalidEmailRecipientsDetail', '_id': page.param._id });
    });

    _marktingRecipientsList.InsertRecipientsRecord = function (page, data, callback) {
        let paramNew = { 'url': '/api/marketing/UpdateMarketingRecipientsList', 'type': 'post', 'contentType': 'application/json' };
        paramNew.data = {};
        paramNew.data.recipientDetailID = data.recipientDetailID;
        paramNew.data.mode = data.mode;

        if (data.mode == 'update') {
            data.rowJson["PrimaryKey"] = page.pkValue;
            let chkDuplicate = page.RecipientsArray.filter(p => p[page.pkValue] == data.rowJson[page.pkValue]);
            if (chkDuplicate.filter(p => p.RowNo != data.rowJson["RowNo"]).length > 0) {
                return callback({ statuscode: 500, message: 'Mail ID already exists.' });
            }
            page.RecipientsArray = page.RecipientsArray.filter(p => p.RowNo != data.rowJson["RowNo"]);
            page.RecipientsArray.push(data.rowJson);
            page.RecipientsArray = page.RecipientsArray.sort((a, b) => (a.RowNo > b.RowNo) ? 1 : (b.RowNo > a.RowNo) ? -1 : 0);
            page.data.FileData = page.RecipientsArray;
            paramNew.data.UpdateData = data.rowJson;
        } else if (data.mode == 'delete') {
            for (let i = 0; i < data.RecipientsIndexList.length; i++) {
                page.RecipientsArray = page.RecipientsArray.filter(p => p.RowNo != data.RecipientsIndexList[i]);
                page.data.FileData = page.RecipientsArray;
            }
            paramNew.data.DeleteData = data.RecipientsIndexList;
        } else if (data.mode == 'invalidDataMove') {
            data.rowJson["PrimaryKey"] = page.pkValue;
            if (page.data.FileData.filter(p => p[page.pkValue] == data.rowJson[page.pkValue]).length > 0) {
                return callback({ statuscode: 500, message: 'Mail ID already exists.' });
            }
            paramNew.data.AddData = data.rowJson;
            page.data.InvalidEmailFileData = page.data.InvalidEmailFileData.filter(p => p.RowNo != data.rowJson["RowNo"]);
            paramNew.data.invalidRecipientsList = page.data.InvalidEmailFileData;
        } else if (data.mode == 'invalidDataDelete') {
            for (let i = 0; i < data.RecipientsIndexList.length; i++) {
                page.RecipientsArray = page.RecipientsArray.filter(p => p.RowNo != data.RecipientsIndexList[i]);
                page.data.InvalidEmailFileData = page.RecipientsArray;
            }
            paramNew.data.invalidRecipientsList = page.data.InvalidEmailFileData;
        } else {
            data.rowJson["PrimaryKey"] = page.pkValue;
            if (page.RecipientsArray.filter(p => p[page.pkValue] == data.rowJson[page.pkValue]).length > 0) {
                return callback({ statuscode: 500, message: 'Mail ID already exists.' });
            }
            page.RecipientsArray.unshift(data.rowJson);
            page.data.FileData = page.RecipientsArray;
            paramNew.data.AddData = data.rowJson;
        }
        _common.FetchApiByParam(paramNew, function (res) {
            callback(res);
        });
    }
}

_marktingRecipientsList.LoadInvalidEmailRecipientDetails = function (page) {
    _common.ContainerLoading(true, page.PageContainer.find('.recipients-container'));
    let arrayContactListID = [{ '_id': page.param._id }];
    page.PageContainer.find('.row').addClass('mb-2');
    page.PageContainer.find('.btnRefresh').hide();
    page.PageContainer.find('.total-count').text('0 items');
    page.PageContainer.find('.file-name').text('');
    _marktingRecipientsList.GetRecipientsList(arrayContactListID, function (res) {
        if (res.statuscode == 200) {
            if (res) {
                page.RecipientsList = res.data[0].ColumnList;
                page.data = res.data[0];
            }
            _marktingRecipientsList.CreateInvalidEmailRecipientDetailsTable(page);
            _common.ContainerLoading(false, page.PageContainer.find('.recipients-container'));
        }
        else {
            swal(res.message, { icon: "info" });
            _common.ContainerLoading(false, page.PageContainer.find('.recipients-container'));
        }
    })
}

_marktingRecipientsList.CreateInvalidEmailRecipientDetailsTable = function (page) {
    let data = page.data.InvalidEmailFileData;
    _page.nav.find('.nav-item.active[data-uid="' + _page.nav.find('.nav-item.active').attr('data-uid') + '"] .title').text(page.data.Name + ': Invalid Emails');
    page.PageContainer.find('.file-name').text('Recipients Detail of Invalid Emails for ' + page.data.Name);
    page.PageContainer.find('.total-count').text(data.length + ' items');
    page.RecipientsArray = data;
    let length = data.length;
    let pagingIndex = length / (Number(_initData.User.MySettings.GridRows) || 10);
    pagingIndex = parseInt(pagingIndex) < pagingIndex ? parseInt(pagingIndex) + 1 : parseInt(pagingIndex);
    page.RecipientsArray.pager = { index: 1, pageSize: pagingIndex, totalPage: length };
    _marktingRecipientsList.FillGridData(page);
    page.PageContainer.find('.txtSearchInTable').unbind('keyup')
    page.PageContainer.find('.txtSearchInTable').keyup(function () {
        var searchTerm = $(this).val();
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

        $.extend($.expr[':'], {
            'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        page.PageContainer.find(".recipients-table  .data-row").not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'false');
            $(this).find(".cellDisp").not(":containsi('" + searchSplit + "')").each(function (e) {
                $('.cellDisp').removeClass('text-found-marketing-contact');
            });
        });

        page.PageContainer.find(".recipients-table  .data-row:containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'true');
            $(this).find(".cellDisp:containsi('" + searchSplit + "')").each(function (e) {
                if (searchSplit != '')
                    $(this).addClass('text-found-marketing-contact');
                else
                    $('.cellDisp').removeClass('text-found-marketing-contact');
            });
        });

        var jobCount = page.PageContainer.find('.recipients-table .data-row[visible="true"]').length;
        page.PageContainer.find('.total-count').text(jobCount + ' item');

        if (jobCount == '0') { page.PageContainer.find('.no-result').show(); }
        else { page.PageContainer.find('.no-result').hide(); }
    });

    page.PageContainer.find('.btnDelete').unbind('click');
    page.PageContainer.find('.btnDelete').on('click', function () {
        let array = [];
        page.PageContainer.find('.recipients-table tr td .chkRecipients:checked').each(function () {
            array.push(parseInt($(this).attr('index')));
        })
        if (array.length == 0) {
            swal('Please select record from grid.', { icon: "info" });
            return false;
        }
        _common.ConfirmDelete({ message: 'If you delete this it will be permanently removed for all users.' }).then((confirm) => {
            if (confirm) {
                _common.Loading(true);
                let rowJson = {};
                let obj = { mode: 'invalidDataDelete', recipientDetailID: page.param._id, rowJson: rowJson };
                obj.RecipientsIndexList = array;
                _marktingRecipientsList.InsertRecipientsRecord(page, obj, function (res) {
                    if (res.statuscode == 200) {
                        _marktingRecipientsList.CreateInvalidEmailRecipientDetailsTable(page);
                        swal(res.message, { icon: res.status });
                        _common.Loading(false);
                    }
                    else {
                        swal(res.message, { icon: "info" });
                        _common.Loading(false);
                    }
                });
            }
        });
    });

    page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').unbind('change');
    page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').on('change', function () {
        page.PageContainer.find('.recipients-table tr  .chkRecipients').closest('.data-row').removeClass('selected');
        page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').closest('.head-row').removeClass('selected');
        if ($(this).prop('checked')) {
            page.PageContainer.find('.recipients-table tr  .chkRecipients').prop('checked', true);
            page.PageContainer.find('.recipients-table tr  .chkRecipients:checked').closest('.data-row').addClass('selected');
            page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').closest('.head-row').addClass('selected');
        }
        else {
            page.PageContainer.find('.recipients-table tr  .chkRecipients').prop('checked', false);
        }
    });

    page.PageContainer.find('.recipients-table .data-row .chkRecipients').unbind('change');
    page.PageContainer.find('.recipients-table .data-row .chkRecipients').on('change', function () {
        $(this).closest('.data-row').removeClass('selected');
        page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').prop('checked', false);
        page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').closest('.head-row').removeClass('selected');
        if ($(this).prop('checked')) {
            page.PageContainer.find('.recipients-table .data-row .chkRecipients:checked').closest('.data-row').addClass('selected');
            if (page.PageContainer.find('.recipients-table .data-row .chkRecipients:not(:checked)').length == 0) {
                page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients').prop('checked', true);
                page.PageContainer.find('.recipients-table thead tr th .chkAllRecipients:checked').closest('.head-row').addClass('selected');
            }
        }
    });
}

_marktingRecipientsList.FillGridData = function (page) {
    page.PageContainer.find('.recipients-table').html('');
    let singleObj = page.data.ColumnList;
    page.pkValue = singleObj["PrimaryKey"];
    let tblHeader = '<thead ><tr class="head-row">';
    let tblHeaderFilter = '<tr class="filter-row hide-edit"><th></th>'
    let isCheckbox = false;
    for (k in singleObj) {
        if (singleObj.hasOwnProperty(k) && k != "RowNo" && k != "PrimaryKey" && k != "IsValidEmail") {
            if (!isCheckbox) {
                tblHeader += "<th width='20px;'><div><label class='lblChk'><input type='checkbox' index='0' class='chkAllRecipients'><span class='checkmark'></span></label></div></th>";
                isCheckbox = true;
            }
            if (_common.CompareStringValue(k, page.pkValue)) {
                tblHeader += `<th style='width:auto;' class='thPrimaryKey'>${k}</th>`;
            } else {
                tblHeader += `<th style='width:auto;' class="${k}">${k}</th>`;
            }
            tblHeaderFilter += `<th data-field="${k}" class=''><div><input type='text' data-field="${k}" data-ui='text' class='msp-ctrl form-control form-control-sm msp-filter-ctrl msp-ctrl-text'></div></th>`;
        }
    }
    tblHeader += '</tr>' + tblHeaderFilter + '</thead>';
    page.PageContainer.find('.recipients-table').append(tblHeader);
    _marktingRecipientsList.AppendRow(page);
    _marktingRecipientsList.Paging(page);
}

_marktingRecipientsList.Paging = function (page) {
    let pager = page.RecipientsArray.pager;
    let pagingHTML = '<li class="page-item first " pid="1"><a href="#" class="page-link">First</a></li>';
    pagingHTML += '<li class="page-item prev" pid="' + (pager.index - 1) + '"><a href="#" class="page-link">Previous</a></li>';
    let startIndex = pager.index;
    if (startIndex <= 0) {
        return;
    }
    // let pageSize = 0;
    // let totalSize =  page.totalPage / 10;
    // let EndPoint = (page.totalPage % 10 == 0) ? '' : '';
    // if (pager.pageSize >= (pager.index + 10)) {

    // pageSize = pager.index + 10;
    // }

    // for (let i = startIndex; i <= pageSize; i++) {
    //     let activeCls = (i == pager.index) ? 'active' : '';
    //     pagingHTML += '<li class="page-item num ' + activeCls + '" pid="' + i + '"><a href="#" class="page-link">' + i + '</a></li>';
    // }
    // pagingHTML += '<li class="page-item next" pid="' + (pager.index + 1) + '"><a href="#" class="page-link">Next</a></li>';
    // pagingHTML += '<li class="page-item last" pid="' + (pager.pageSize) + '"><a href="#" class="page-link">Last</a></li>';
    // console.log(pager, pagingHTML, pager.pageSize, (pager.index + 10), "Sdcscsdlcjhhvuwhuerhuiew======>>")
    let pageStatus = _common.PagerService(pager.totalPage, pager.index, (Number(_initData.User.MySettings.GridRows) || 10));
    _common.BuildPaging(pageStatus, page.PageContainer.find('.pagination'));
    // $(`.pagination .page-item[pid=${page.index}]`).addClass('active')
    // page.PageContainer.find('.pagination').html($(pagingHTML));


    page.PageContainer.find('.pagination .page-item').on("click", function () {
        // console.log($(this), "=======>><><<")
        // page.PageContainer.find('.pagination').removeClass('active')
        // $(this).addClass('active')
        // $('p[MyTag="Sara"]')
        let pid = parseInt($(this).attr('pid'));
        if (pid > pager.pageSize) {
            return false;
        }
        page.RecipientsArray.pager.index = pid;
        // let pagingHTML = '';
        // for (let i = startIndex; i <= pageSize; i++) {
        //     let activeCls = (i == pager.index) ? 'active' : '';
        //     pagingHTML += '<li class="page-item num ' + activeCls + '" pid="' + i + '"><a href="#" class="page-link">' + i + '</a></li>';
        // }
        // page.PageContainer.find('.pagination').html(pagingHTML);
        _marktingRecipientsList.AppendRow(page);
    });
}

_marktingRecipientsList.AppendRow = function (page) {
    let isCheckbox = false;
    page.PageContainer.find('.recipients-table tr:gt(1)').remove();
    let index = page.RecipientsArray.pager.index;
    index = index > 0 ? index - 1 : 0;
    let pageSize = (Number(_initData.User.MySettings.GridRows) || 10);
    let newRecipientsArray = page.RecipientsArray.slice(index * pageSize, (index + 1) * pageSize);
    for (let i = 0; i < newRecipientsArray.length; i++) {
        let row = newRecipientsArray[i];
        let index = newRecipientsArray[i]["RowNo"];
        let tbrRow = '<tr class="data-row">';
        for (k in row) {
            if (k != undefined && k != "RowNo" && k != "PrimaryKey" && k != "IsValidEmail") {
                if (!isCheckbox) {
                    tbrRow += "<td><div><label class='lblChk'><input type='checkbox' index='" + index + "' class='chkRecipients'><span class='checkmark'></span></label></div></td>";
                    isCheckbox = true;
                }
                if (_common.CompareStringValue(k, page.RecipientsList["PrimaryKey"])) {
                    if (!row.IsValidEmail)
                        tbrRow += '<td class="email-field  cellDisp" style="background-color:red" index="' + index + '" recipientField="' + k + '">' + newRecipientsArray[i][k] + '</td>';
                    else
                        tbrRow += '<td class="email-field cellDisp" index="' + index + '" recipientField="' + k + '">' + newRecipientsArray[i][k] + '</td>';
                } else {
                    tbrRow += '<td class="cellDisp" index="' + index + '" recipientField="' + k + '">' + newRecipientsArray[i][k] + '</td>';
                }
            }
        }
        tbrRow += '</tr>';
        page.PageContainer.find('.recipients-table').append(tbrRow);
        isCheckbox = false;
    }
    _marktingRecipientsList.Paging(page);
    page.PageContainer.find('.recipients-table .data-row').unbind('dblclick');
    page.PageContainer.find('.recipients-table .data-row').on('dblclick', function () {
        let length = $(this).find('td').length;
        let rowData = $(this).find('td');
        let popUpdata = '<div class="row">';
        let index = 0;
        for (let i = 1; i < length; i++) {
            let lblName = $(rowData[i]).attr("recipientField");
            let val = $(rowData[i]).text();
            index = $(rowData[i]).attr("index");
            if ($(rowData[i]).hasClass('email-field')) {
                popUpdata += '<div class="form-group col-sm-6 vld-parent">\
                <label class="small">'+ lblName + '</label>\
                <input type="text" RowNo="'+ index + '" title="' + lblName + '" value="' + val + '" class="form-control form-control-sm msp-ctrl-email email-field" placeholder="Enter ' + lblName + '"    data-required  data-field="' + lblName + '">\
                \</div>'
            } else {
                popUpdata += '<div class="form-group col-sm-6">\
                <label class="small">'+ lblName + '</label>\
                <input type="text" RowNo="'+ index + '" title="' + lblName + '" value="' + val + '" class="form-control msp-ctrl" placeholder="Enter ' + lblName + '"  data-field="' + lblName + '">\
                \</div>'
            }
        }
        popUpdata += '</div>';
        let popUpForRecipientsField = _common.DynamicPopUp({ title: 'Recipients Field', body: popUpdata });
        popUpForRecipientsField.modal('show');
        popUpForRecipientsField.find('.btnSave')[0].RowNo = index;
        popUpForRecipientsField.find('.btnSave').on('click', function () {
            _common.Loading(true);
            let rowJson = {};
            rowJson["RowNo"] = parseInt($(this)[0].RowNo);
            if (_common.Validate(popUpForRecipientsField.find('.modal-body'))) {
                popUpForRecipientsField.find('.modal-body input').each(function () {
                    if ($(this).hasClass('email-field')) {
                        rowJson['IsValidEmail'] = _common.isValidEmailAddress($(this).val().trim()) == true ? true : false;
                        rowJson['PrimaryKey'] = $(this).attr('data-field');
                    }
                    rowJson[$(this).attr('data-field')] = $(this).val();
                })
                if (rowJson && index > 0) {
                    let obj = { mode: 'update', recipientDetailID: page.param._id, rowJson: rowJson };
                    if (page.PageContainer.attr('data-uid').split(':')[0] != "MarketingRecipientsDetail")
                        obj.mode = 'invalidDataMove';
                    _marktingRecipientsList.InsertRecipientsRecord(page, obj, function (res) {
                        if (res.statuscode == 200) {
                            if (page.PageContainer.attr('data-uid').split(':')[0] != "MarketingRecipientsDetail")
                                _marktingRecipientsList.CreateInvalidEmailRecipientDetailsTable(page);
                            else
                                _marktingRecipientsList.CreateRecipientDetailsTable(page);
                            swal(res.message, { icon: res.status });
                            _common.Loading(false);
                            popUpForRecipientsField.modal('hide');
                        }
                        else {
                            swal(res.message, { icon: "info" });
                            _common.Loading(false);
                        }
                    });
                }
            } else {
                _common.Loading(false);
            }
        });
        popUpForRecipientsField.find('.btnSave').attr('data-restriction', 'Modify');
        _userprofile.Auth({ pageData: page.Data, container: popUpForRecipientsField });
    })
}

_marktingRecipientsList.ShowMergePopup = function (self) {
    let data = [];
    self.CombineList = '';
    let selectedTr = self.param.PageContainer.find(".grid-table tr.selected");
    if (selectedTr.length == 0) {
        swal("Please select atleast one file from grid", { icon: "info" });
        return;
    }
    else if (selectedTr.length > 2) {
        swal("You can't select more than two files at a time for merge.", { icon: "info" });
        return;
    }
    else if (selectedTr.length == 1) {

        _marktingRecipientsList.GetRecipientsList([], function (res) {
            if (res.statuscode == 200) {
                self.CombineList = res.data;
                if (self.CombineList.length > 0) {
                    let popUpMerge = _common.DynamicPopUp({ title: 'Merge', body: _marketing.COMBINE_LIST_POPUP });
                    let fileFirstName = $(selectedTr.children()[1]).text();
                    popUpMerge.find('.lbl-selected-list>div').append("<b>" + fileFirstName + "</b>");
                    // Bind data
                    let id = selectedTr.attr('data-id');
                    for (let i = 0; i < self.CombineList.length; i++) {
                        if (id != self.CombineList[i]._id) {
                            let opt = $('<option></option>').attr('value', self.CombineList[i].Name).text(self.CombineList[i].Name);
                            opt[0].data = self.CombineList[i];
                            popUpMerge.find('.ddl-list').append(opt);
                        }
                    }
                    popUpMerge.find('.radio').on('change', function () {
                        if ($(this).val() == "combine") {
                            popUpMerge.find('.combine-section').removeClass('d-none');
                            popUpMerge.find('.create-section').addClass('d-none');
                        }
                        else {
                            popUpMerge.find('.create-section').removeClass('d-none');
                            popUpMerge.find('.combine-section').addClass('d-none');
                        }
                        popUpMerge.find('.ddl-list').val('');
                        popUpMerge.find('.tblMapping').html('');
                    })
                    popUpMerge.find('.btnSave').text('Combine');

                    popUpMerge.find('.btnSave').on('click', function () {
                        if (_common.Validate(popUpMerge)) {
                            _common.ConfirmDelete({ message: 'are you sure to combine list' }).then((confirm) => {
                                if (confirm) {
                                    _common.Loading(true);
                                    _marktingRecipientsList.MergeList(self, popUpMerge, function (res) {
                                        if (res.statuscode == '200') {
                                            swal(res.message, { icon: "success" });
                                            self.CombineList = [];
                                            popUpMerge.modal('hide');
                                            self.ReloadData()
                                            _common.Loading(false);
                                        } else if (res.statuscode == 422) {
                                            swal(res.message, { icon: "error" });
                                            _common.Loading(false);
                                            return;
                                        } else {
                                            swal(res.message, { icon: "info" });
                                            _common.Loading(false);
                                        }

                                    });
                                }
                            })
                        }
                    })
                    popUpMerge.find('.ddl-list').on('change', function () {
                        let fileName = $(this).children("option:selected")[0].data._id;
                        popUpMerge.find('.tblMapping').html('');
                        if (fileName == "0") return;
                        let firstList = self.CombineList.find(p => p._id == fileName);
                        let secondList = self.CombineList.find(p => p._id == selectedTr.attr('data-id'));
                        let pkValue = secondList.ColumnList['PrimaryKey'];
                        let ddlValueSecondList = '<select class="mappingColumn form-control form-control-sm"><option value="0">Select</option>';
                        for (column in secondList.ColumnList) {
                            if (column != 'RowNo' && column != 'PrimaryKey' && column != "IsValidEmail")
                                ddlValueSecondList += '<option value="' + column + '">' + column + '</option>';
                        }
                        ddlValueSecondList += '</select>';
                        let tbl = '<table class="table border"><tr class="head-row"><th>Primary Column</th><th>Source Column</th></tr>';
                        for (column in firstList.ColumnList) {
                            if (column == 'RowNo' || column == 'PrimaryKey' || column == "IsValidEmail")
                                tbl += '<tr class="d-none data-row"><td class="tdDestinationColumn">' + column + '</td><td class="tdSourceColumn"><select class="mappingColumn form-control form-control-sm"><option value="' + column + '">' + column + '<option></select></td></tr>';
                            else
                                if (firstList.ColumnList['PrimaryKey'].trim() == column.trim()) {
                                    tbl += '<tr class="data-row"><td class="tdDestinationColumn">' + column + '</td><td class="tdSourceColumn ddlPrimary">' + ddlValueSecondList + '</td></tr>';
                                } else {
                                    tbl += '<tr class="data-row"><td class="tdDestinationColumn">' + column + '</td><td class="tdSourceColumn">' + ddlValueSecondList + '</td></tr>';
                                }
                        }
                        tbl += '</table>';
                        popUpMerge.find('.tblMapping').html(tbl);
                        popUpMerge.find('.tblMapping .data-row .ddlPrimary select').val(pkValue.trim());
                        let dom = popUpMerge.find('.tblMapping .data-row  select').not('.ddlPrimary select');
                        dom.find('option[value="' + pkValue.trim() + '"]').remove();
                        popUpMerge.find('.tblMapping .data-row .ddlPrimary select').prop("disabled", true);
                    })
                    popUpMerge.modal('show');
                } else {
                    swal(_constantClient.ERROR_MSG, { icon: "info" });
                    return;
                }
            }
            else {
                swal(res.message, { icon: "info" });
            }
        })
        return;
    }
    else {
        $.each(selectedTr, function (k, v) {
            if ($(this).attr('data-id'))
                data.push({ '_id': $(this).attr('data-id') });
        })
        _marktingRecipientsList.GetRecipientsList(data, function (res) {
            if (res.statuscode == 200) {
                self.CombineList = res.data;
                if (self.CombineList.length > 0) {
                    let popUpMerge = _common.DynamicPopUp({ title: 'Merge', body: _marketing.COMBINE_LIST_POPUP });
                    let fileNameCommaSep = self.CombineList[1].Name + ", " + self.CombineList[0].Name;
                    popUpMerge.find('.lbl-selected-list>div').append("<b>" + fileNameCommaSep + "</b>");
                    // Bind data
                    for (let i = 0; i < self.CombineList.length; i++) {
                        let opt = $('<option></option>').attr('value', self.CombineList[i].Name).text(self.CombineList[i].Name);
                        opt[0].data = self.CombineList[i];
                        popUpMerge.find('.ddl-list').append(opt);
                    }
                    popUpMerge.find('.radio').on('change', function () {
                        if ($(this).val() == "combine") {
                            popUpMerge.find('.combine-section').removeClass('d-none');
                            popUpMerge.find('.create-section').addClass('d-none');
                        }
                        else {
                            popUpMerge.find('.create-section').removeClass('d-none');
                            popUpMerge.find('.combine-section').addClass('d-none');
                        }
                        popUpMerge.find('.ddl-list').val('');
                        popUpMerge.find('.tblMapping').html('');
                    })
                    popUpMerge.find('.btnSave').text('Combine');
                    popUpMerge.find('.btnSave').on('click', function () {
                        if (_common.Validate(popUpMerge)) {
                            _common.ConfirmDelete({ message: 'are you sure to combine list' }).then((confirm) => {
                                if (confirm) {
                                    _common.Loading(true);
                                    _marktingRecipientsList.MergeList(self, popUpMerge, function (res) {
                                        if (res.statuscode == '200') {
                                            swal(res.message, { icon: "success" });
                                            popUpMerge.modal('hide');
                                            self.ReloadData()
                                            _common.Loading(false);
                                        } else if (res.statuscode == 422) {
                                            swal(res.message, { icon: "error" });
                                            _common.Loading(false);
                                            return;
                                        } else {
                                            swal(res.message, { icon: "info" });
                                            _common.Loading(false);
                                        }
                                    });
                                }
                            })
                        }
                    })
                    popUpMerge.find('.ddl-list').on('change', function () {
                        let fileName = $(this).children("option:selected")[0].data._id;
                        popUpMerge.find('.tblMapping').html('');
                        if (fileName == "0") return;
                        let firstList = self.CombineList.find(p => p._id == fileName);
                        let secondList = self.CombineList.find(p => p._id != fileName);
                        let pkValue = secondList.ColumnList['PrimaryKey'];
                        let ddlValueSecondList = '<select class="mappingColumn form-control form-control-sm"><option value="0">Select</option>';
                        for (column in secondList.ColumnList) {
                            if (column != 'RowNo' && column != 'PrimaryKey' && column != "IsValidEmail")
                                ddlValueSecondList += '<option value="' + column + '">' + column + '</option>';
                        }
                        ddlValueSecondList += '</select>';
                        let tbl = '<table class="table border"><tr class="head-row"><th>Primary Column</th><th>Source Column</th></tr>';
                        for (column in firstList.ColumnList) {
                            if (column == 'RowNo' || column == 'PrimaryKey' || column == "IsValidEmail")
                                tbl += '<tr class="d-none data-row"><td class="tdDestinationColumn">' + column + '</td><td class="tdSourceColumn"><select class="mappingColumn form-control form-control-sm"><option value="' + column + '">' + column + '<option></select></td></tr>';
                            else
                                if (firstList.ColumnList['PrimaryKey'].trim() == column.trim()) {
                                    tbl += '<tr class="data-row"><td class="tdDestinationColumn">' + column + '</td><td class="tdSourceColumn ddlPrimary">' + ddlValueSecondList + '</td></tr>';
                                } else {
                                    tbl += '<tr class="data-row"><td class="tdDestinationColumn">' + column + '</td><td class="tdSourceColumn">' + ddlValueSecondList + '</td></tr>';
                                }
                        }
                        tbl += '</table>';
                        popUpMerge.find('.tblMapping').html(tbl);
                        popUpMerge.find('.tblMapping .data-row .ddlPrimary select').val(pkValue.trim());
                        let dom = popUpMerge.find('.tblMapping .data-row  select').not('.ddlPrimary select');
                        dom.find('option[value="' + pkValue.trim() + '"]').remove();
                        popUpMerge.find('.tblMapping .data-row .ddlPrimary select').prop("disabled", true);
                    })
                    popUpMerge.modal('show');
                } else {
                    swal(_constantClient.ERROR_MSG, { icon: "info" });
                    return;
                }
            }
            else {
                swal(res.message, { icon: "info" });
            }
        })
    }
}

_marktingRecipientsList.MergeList = function (self, popUpMerge, callback) {
    let rdoValue = popUpMerge.find('.radio[name="optradio"]:checked').val();
    let newList = {};
    let paramNew = {};
    if (rdoValue == "combine") {
        newList = _marktingRecipientsList.GetNewList(self, popUpMerge);
        if (newList) {
            paramNew = { 'url': '/api/marketing/CombineMarketingRecipientsList', 'type': 'post', 'contentType': 'application/json' };
            paramNew.data = {};
            paramNew.data = newList;
            _common.FetchApiByParam(paramNew, function (res) {
                callback(res);
            });
        } else {
            callback({ 'statuscode': 422, 'message': 'No. of records can not be more than 10000' });
        }
    } else {
        let url = { 'url': '/api/marketing/CheckMarketingRecipientsListNameExist', 'type': 'post', 'contentType': 'application/json' };
        url.data = { 'RecipientsListName': popUpMerge.find('.txtListName').val() }
        _common.FetchApiByParam(url, function (res) {
            if (res.data.isRecipientsListExists == false) {
                newList = _marktingRecipientsList.GetNewList(self, popUpMerge);
                if (newList) {
                    newList.Name = popUpMerge.find('.txtListName').val();
                    newList.IsDelete = popUpMerge.find('.lblChk input').prop('checked');
                    newList.PrvListId = [];
                    let selectedTr = self.param.PageContainer.find(".grid-table tr.selected");
                    if (selectedTr.length > 1)
                        newList.PrvListId = self.CombineList.map(p => p._id);
                    else
                        newList.PrvListId.push(selectedTr.attr('data-id'));
                    newList.CreatedDate = new Date();
                    delete newList["_id"];
                    paramNew = { 'url': '/api/marketing/InsertMarketingRecipientsList', 'type': 'post', 'contentType': 'application/json' };
                    paramNew.data = {};
                    paramNew.data = newList;
                    _common.FetchApiByParam(paramNew, function (res) {
                        callback(res);
                    });
                } else {
                    callback({ 'statuscode': 422, 'message': 'No. of records can not be more than 10000' });
                }
            }
            else {
                _common.Loading(false);
                swal(res.message, { icon: "info" });
            }
        });
    }
}

_marktingRecipientsList.GetNewList = function (self, popUpMerge) {
    let selectedTr = self.param.PageContainer.find(".grid-table tr.selected");
    let fileName = popUpMerge.find('.ddl-list').children("option:selected")[0].data._id;
    let newList = self.CombineList.find(p => p._id == fileName);
    let lastIndex = Math.max.apply(Math, newList.FileData.map(function (o) { return o.RowNo; }));
    let files = self.Data;
    let x = selectedTr.length == 2 ? self.CombineList.find(p => p._id != fileName)._id : selectedTr.attr('data-id');
    for (let s = 0; s < files.length; s++) {
        let listName = $(files[s]).attr('_id');
        if (x == listName) {
            let combineList = self.CombineList.find(p => p._id == listName);
            if (newList.FileData.length + combineList.FileData.length > 10000) { return false; }
            for (let i = 0; i < combineList.FileData.length; i++) {
                let newObj = {};
                $.each(popUpMerge.find('.tblMapping .data-row'), function () {
                    let sourceColumn = $(this).find('.tdSourceColumn .mappingColumn').val();
                    let destinationColumn = $(this).find('.tdDestinationColumn').text();
                    if (destinationColumn == 'RowNo') {
                        newObj[destinationColumn] = lastIndex + 1;
                    } else if (destinationColumn != undefined) {
                        newObj[destinationColumn] = combineList.FileData[i][sourceColumn] == undefined ? "" : combineList.FileData[i][sourceColumn];
                    }
                })
                lastIndex += 1;
                let e = newList.ColumnList.PrimaryKey;
                if (newObj && (newList.FileData.find(p => (p[e] || "").trim() == (newObj[e] || "").trim()) == undefined))
                    newList.FileData.push(newObj);
            }
        }
    }
    return newList;
}

_marktingRecipientsList.InitMarketingListPage = function (page) {

    page.Grid.find('.btnFilter').addClass('d-none');
    page.Grid.find('.set-default-icon, .default-icon').hide();
    if (!_initData.ProfileAuth.IsAdmin) {
        page.Grid.find('.table-filter').html('');
    }
    _common.GetStaticPage('/page/marketing/bouncedemails.html', function (pageHTML) {
        let bounceEmailsPage = $('<div data-pagename="BouncedEmailIDs" class="tab-item d-none">' + pageHTML + '</div>');
        page.Grid.append(bounceEmailsPage);

        let firstListView = page.PageData.ListViewFilters[0].Name;
        let secondListView = page.PageData.ListViewFilters[1].Name;

        page.Grid.find('.table-filter').off('click', '.dropdown-item');
        page.Grid.find('.table-filter').on('click', '.dropdown-menu a', function () {
            let filter = $(this)[0].filter;
            let filterSection = $(this).closest('.table-filter');
            filterSection.find('.dropdown-toggle').html(filter.DisplayName);
            filterSection.find('.dropdown-item.active').removeClass('active');
            filterSection.find('.dropdown-item[name="' + filter.Name + '"]').addClass('active');
            filterSection.find('.dropdown-item.default-item').removeClass('default-item');

            if ($(this).attr('name') == firstListView) {
                page.Grid.find('.grid-body.loading-container').removeClass('d-none');
                page.Grid.find('.page-top-button').removeClass('d-none');
                bounceEmailsPage.addClass('d-none');
            } else if ($(this).attr('name') == secondListView) {
                page.Grid.find('.grid-body.loading-container').addClass('d-none');
                page.Grid.find('.page-top-button').addClass('d-none');
                bounceEmailsPage.removeClass('d-none');
            } else {
                swal('Filter is not found in database.', { icon: 'info' });
            }
        });
        _marketingBounceEmailList.ShowBouncedEmails(page);

        page.Grid.find('.btnSearchBouncedEmail').unbind('click');
        page.Grid.find('.btnSearchBouncedEmail').click(function () {
            let searchText = $(this).closest('.bounced-email-container').find('.txtBouncedEmail').val();
            _marketingBounceEmailList.ShowBouncedEmails(page, searchText);
        });

        page.Grid.find('.bounced-email-search input').unbind('keypress');
        page.Grid.find('.bounced-email-search input').on("keypress", function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                $(this).closest('.bounced-email-container').find('.btnSearchBouncedEmail').trigger('click');
            }
        });

        // Delete from grid
        bounceEmailsPage.find('.btnDelete').unbind('click');
        bounceEmailsPage.find('.btnDelete').on('click', function () {
            let arrayID = [];
            let arrayEmailID = [];
            bounceEmailsPage.find('.bounced-emails-table tr td .chkSelect:checked').each(function () {
                arrayID.push($(this).closest('tr').attr('data-id'));
                arrayEmailID.push($(this).closest('tr').find('.email-id').text());
            })
            if (arrayID.length == 0) {
                swal('Please select record from grid.', { icon: "info" });
                return false;
            }
            _common.ConfirmDelete({ message: 'If you delete this it will be permanently removed for all users.' }).then((confirm) => {
                if (confirm) {
                    _common.FetchApiByParam({
                        url: "api/admin/marketingbouncedemailDF",
                        type: "post",
                        data: { 'bounceIDs': _controls.ToEJSON("multilookup", arrayID), 'bounceEmailIDs': arrayEmailID, 'mode': 'delete' }
                    }, function (res) {
                        if (res.statuscode == 200) {
                            _marketingBounceEmailList.ShowBouncedEmails(page);
                            swal(res.message, { icon: res.status });
                        }
                        else {
                            swal(res.message, { icon: res.status });
                        }
                    });
                }
            });
        });

        // Refresh grid
        bounceEmailsPage.find('.btnRefresh').unbind('click');
        bounceEmailsPage.find('.btnRefresh').on('click', function () {
            page.Grid.find('.bounced-emails-table .filter-row input').each(function () { $(this).val(''); });
            _marketingBounceEmailList.ShowBouncedEmails(page);
        });

        bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll').unbind('change');
        bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll').on('change', function () {
            bounceEmailsPage.find('.bounced-emails-table tr  .chkSelect').closest('.data-row').removeClass('selected');
            bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll').closest('.head-row').removeClass('selected');
            if ($(this).prop('checked')) {
                bounceEmailsPage.find('.bounced-emails-table tr  .chkSelect').prop('checked', true);
                bounceEmailsPage.find('.bounced-emails-table tr  .chkSelect:checked').closest('.data-row').addClass('selected');
                bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll').closest('.head-row').addClass('selected');
            }
            else {
                bounceEmailsPage.find('.bounced-emails-table tr  .chkSelect').prop('checked', false);
            }
        });

        bounceEmailsPage.find('.bounced-emails-table').off('change', '.data-row .chkSelect');
        bounceEmailsPage.find('.bounced-emails-table').on('change', '.data-row .chkSelect', function () {
            $(this).closest('.data-row').removeClass('selected');
            bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll').prop('checked', false);
            bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll').closest('.head-row').removeClass('selected');
            if ($(this).prop('checked')) {
                bounceEmailsPage.find('.bounced-emails-table .data-row .chkSelect:checked').closest('.data-row').addClass('selected');
                if (bounceEmailsPage.find('.bounced-emails-table .data-row .chkSelect:not(:checked)').length == 0) {
                    bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll').prop('checked', true);
                    bounceEmailsPage.find('.bounced-emails-table thead tr th .chkSelectAll:checked').closest('.head-row').addClass('selected');
                }
            }
        });
    });
}

_marketingBounceEmailList.ShowBouncedEmails = function (page, searchText) {
    _common.ContainerLoading(true, page.Grid.find('.msp-data-grid'));
    _common.FetchApiByParam({
        url: "api/admin/marketingbouncedemailDF",
        type: "post",
        data: { 'SearchText': searchText, 'mode': 'fetchemail' }
    }, function (result) {
        if (result.message == "success") {
            page.bouncedEmails = result.data;
            let length = result.data.length;
            let pagingIndex = length / (Number(_initData.User.MySettings.GridRows) || 10);
            pagingIndex = parseInt(pagingIndex) < pagingIndex ? parseInt(pagingIndex) + 1 : parseInt(pagingIndex);
            page.bouncedEmails.pager = { index: 1, pageSize: pagingIndex, totalPage: length };
            _marketingBounceEmailList.AppendBouncedEmailRow(page);
            page.Grid.find('.txtSearchInTable').unbind('keyup')
            page.Grid.find('.txtSearchInTable').keyup(function () {
                var searchTerm = $(this).val();
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

                $.extend($.expr[':'], {
                    'containsi': function (elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                    }
                });

                page.Grid.find(".bounced-emails-table  .data-row").not(":containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'false');
                    $(this).find(".cellDisp").not(":containsi('" + searchSplit + "')").each(function (e) {
                        $('.cellDisp').removeClass('text-found-marketing-contact');
                    });
                });

                page.Grid.find(".bounced-emails-table  .data-row:containsi('" + searchSplit + "')").each(function (e) {
                    $(this).attr('visible', 'true');
                    $(this).find(".cellDisp:containsi('" + searchSplit + "')").each(function (e) {
                        if (searchSplit != '')
                            $(this).addClass('text-found-marketing-contact');
                        else
                            $('.cellDisp').removeClass('text-found-marketing-contact');
                    });
                });

                var jobCount = page.Grid.find('.bounced-emails-table .data-row[visible="true"]').length;
                page.Grid.find('.total-count').text(jobCount + ' item');

                if (jobCount == '0') { page.Grid.find('.no-result').show(); }
                else { page.Grid.find('.no-result').hide(); }
            });
        }
    });
}

_marketingBounceEmailList.AppendBouncedEmailRow = function (page) {
    let index = page.bouncedEmails.pager.index;
    index = index > 0 ? index - 1 : 0;
    let pageSize = (Number(_initData.User.MySettings.GridRows) || 10);
    let newbouncedEmails = page.bouncedEmails.slice(index * pageSize, (index + 1) * pageSize);
    page.Grid.find('.bounced-emails-table tbody').html('');
    page.Grid.find('[data-pagename="BouncedEmailIDs"] .total-count-bouncedemails').text(page.bouncedEmails.length + ' Items');
    _common.ContainerLoading(false, page.Grid.find('.msp-data-grid'));
    for (let i = 0; i < newbouncedEmails.length; i++) {
        if (newbouncedEmails[i]) {
            let trEmail = '<tr class="data-row" data-id="' + newbouncedEmails[i]._id + '">';
            trEmail += '<td><div><label class="lblChk"><input type="checkbox" class="chkSelect"><span class="checkmark"></span></label></div></td>';
            trEmail += '<td class="cellDisp email-id" index="' + i + '">' + newbouncedEmails[i].EmailID + '</td>';
            trEmail += '</tr>';
            page.Grid.find('.bounced-emails-table tbody').append(trEmail);
        }
    }
    _marketingBounceEmailList.PagingBounceEmail(page);
};

_marketingBounceEmailList.PagingBounceEmail = function (page) {
    let pager = page.bouncedEmails.pager;
    let pagingHTML = '<li class="page-item first " pid="1"><a href="#" class="page-link">First</a></li>';
    pagingHTML += '<li class="page-item prev" pid="' + (pager.index - 1) + '"><a href="#" class="page-link">Previous</a></li>';
    let startIndex = pager.index;
    if (startIndex <= 0) {
        return;
    }
    let pageStatus = _common.PagerService(pager.totalPage, pager.index, (Number(_initData.User.MySettings.GridRows) || 10));
    _common.BuildPaging(pageStatus, page.Grid.find('.pagination-bouncedemail'));

    page.Grid.find('.pagination-bouncedemail .page-item').on("click", function () {
        let pid = parseInt($(this).attr('pid'));
        if (pid > pager.pageSize) {
            return false;
        }
        page.bouncedEmails.pager.index = pid;
        _marketingBounceEmailList.AppendBouncedEmailRow(page);
    });
}

//**************************************************** */Marketing List Code End**********************************************//


//**************************************************** */Campaign Code Start**********************************************//
_campaign.InitCampaignAttribute = function (page) {
    let campaignData = {};
    campaignData.data = {};
    campaignData.data.StatusID = '1';
    campaignData.data.CampaignType = 'regularCampaign';
    _common.GetPageDetail('MarketingCampaignBasic', function (pageData) {
        new Page({ 'pageName': 'CreateCampaign', 'campaignData': campaignData, 'basicPageData': pageData });
    });
}

_campaign.SaveData = function (params, cb) {
    _common.Loading(true);
    let campaignData = params.campaignData;
    let param = { 'url': '/api/marketing/updatecampaign' };
    param.data = campaignData.data;
    if (param.data._id == undefined) {
        param.url = '/api/marketing/addcampaign';
    }
    _common.FetchApiByParam(param, function (res) {
        _common.Loading(false);
        if (res.message == 'success') {
            campaignData.data._id = res.data._id;
            if (params.pageContainer) {
                if (params.pageContainer.closest('#content')[0]) {
                    let newDataUid = 'CreateCampaign:' + res.data._id
                    params.pageContainer.closest('#content').find('.nav-item[data-uid="CreateCampaign"]').attr('data-uid', newDataUid);
                    params.pageContainer.closest('#content').find('.page-content[data-uid="CreateCampaign"]').attr('data-uid', newDataUid);
                }
            }
            if (campaignData.data.StatusID == 2 || campaignData.data.StatusID == 3) {
                campaignData = {};
                _page.OpenGridPage('MarketingCampaignList');
                _page.ClosePage(params.currentUid);
                swal('Campaign Schedule Successfully', { icon: "success" });
            }
            if (cb) { cb(); }
        } else if (res.message == "error") {
            swal(res.data, { icon: "error" })
        } else {
            swal(res.message, { icon: "info" });
        }
    });
}

_campaign.LoadCampaignValue = function (page, tabName, data) {
    let campaignData = page.param.campaignData;
    let basicPageData = page.param.basicPageData;
    if (campaignData.data == undefined) {
        campaignData.data = {};
    }
    if (tabName == 'basic') {
        page.PageContainer.find('.containerTab .basic-page');
        _page.LoadJsonData(page.PageContainer.find('.containerTab .basic-page'), basicPageData, campaignData.data);
        // _campaign.LoadDynamicField(campaignData);
    }
    else if (tabName == "schedule") {
        let isSchedule = page.PageContainer.find('.containerTab.tab-pane.active .rdoSchedule:checked').val();
        if (isSchedule != undefined) {
            if (isSchedule == "SendNow") {
                campaignData.data["ScheduleDate"] = { $date: new Date() };
                campaignData.data["StatusID"] = "3";
                campaignData.data["Timezone"] = _initData.User.MySettings.Timezone;
            } else if (isSchedule == "SendLetter") {
                let scheduleDate = page.PageContainer.find('.containerTab.tab-pane.active .txtScheduleDate').val() + ' ' + page.PageContainer.find('.containerTab.tab-pane.active .txtScheduleTime').val();
                let timeZone = page.PageContainer.find('.containerTab.tab-pane.active .ddlTimezone').val()
                let format = _initData.Company.Setting.DateFormat + ' ' + _initData.Company.Setting.CalendarTimeFormat;
                let momentscheduleDate = moment.tz(scheduleDate, format, timeZone).format();
                campaignData.data["ScheduleDate"] = { $date: momentscheduleDate };
                campaignData.data["Timezone"] = timeZone;
                campaignData.data["StatusID"] = "2";
            }
        }
    } else if (tabName == "template") {
        if (data.TemplateID && data.TemplateID != 'ManualHTMLCode') {
            campaignData.data.TemplateBlockContent = data.EditBlock.find('body')[0].outerHTML;
            campaignData.data.PageStyle = data.PageStyle;
            campaignData.data.TemplateID = data.TemplateID;
            campaignData.data.TemplateEmailContent = _templateEditor.RenderEmailHTML(data.EditBlock);
        } else {
            campaignData.data.TemplateBlockContent = data.EditBlock.contents()[0].outerHTML;
            campaignData.data.PageStyle = data.PageStyle;
            campaignData.data.TemplateID = 'ManualHTMLCode';
            campaignData.data.TemplateEmailContent = data.EditBlock.contents()[0].outerHTML;
        }
        _campaign.SaveData({ 'campaignData': campaignData, 'pageContainer': page.PageContainer });
    }
}

_campaign.IterateCampaignTab = function (page, currentObject) {
    let campaignData = page.param.campaignData;
    let name = $(currentObject).attr('next');
    let currentTab = $(currentObject).attr('current');
    page.PageContainer.find('.campaign-container .containerTab.tab-pane.active .template-Bottam').removeClass('d-none');
    if (name == "template") {
        if (_common.Validate(page.PageContainer.find('.containerTab.tab-pane[name="basic"]'))) {
            _campaign.LoadCampaignValue(page, currentTab);
            _campaign.SaveData({ 'campaignData': campaignData }, () => {
                page.PageContainer.find('.campaign .nav-item .nav-link').removeClass('active');
                page.PageContainer.find('.campaign .nav-item .nav-link[name=' + name + ']').addClass('active');
                page.PageContainer.find('.containerTab.tab-pane').removeClass('active');
                page.PageContainer.find('.containerTab.tab-pane[name=' + name + ']').addClass('active');
                if (campaignData.data.TemplateID) {
                    page.PageContainer.find('.TemplateSavedBasic .templateList.marketing-list').html('');
                    page.PageContainer.find('.campaign-container .containerTab.tab-pane.active .template-Bottam').addClass('d-none');
                    let param =
                    {
                        content: campaignData.data.TemplateBlockContent,
                        pageStyle: campaignData.data.pageStyle,
                        templateId: campaignData.data.TemplateID
                    };
                    _marketing.OpenEditor(page, param);
                } else {
                    _marketing.InitTemplateCreate(page);
                }
                page.PageContainer.find('.campaign .nav-item a[name="' + currentTab + '"]')
                    .closest('li')
                    .removeClass('selected')
                    .addClass('completed');
                page.PageContainer.find('.campaign .nav-item a[name="' + name + '"]')
                    .closest('li')
                    .addClass('selected');
            });
        }
    } else {
        _campaign.SaveData({ 'campaignData': campaignData }, () => {
            page.PageContainer.find('.campaign .nav-item .nav-link').removeClass('active');
            page.PageContainer.find('.campaign .nav-item .nav-link[name=' + name + ']').addClass('active');
            page.PageContainer.find('.containerTab.tab-pane').removeClass('active');
            page.PageContainer.find('.containerTab.tab-pane[name=' + name + ']').addClass('active');
            // _campaign.LoadCampaignValue(page, currentTab);
            page.PageContainer.find('.campaign .nav-item a[name="' + currentTab + '"]')
                .closest('li')
                .removeClass('selected')
                .addClass('completed');
            page.PageContainer.find('.campaign .nav-item a[name="' + name + '"]')
                .closest('li')
                .addClass('selected');
        });

    }
}

_campaign.LoadDynamicField = function (recipientsListID) {
    return new Promise((resolve, reject) => {
        let data = [{ '_id': recipientsListID }];
        let dynamicFields = [];
        _marktingRecipientsList.GetRecipientsList(data, function (res) {
            if (res.statuscode == 200) {
                if (res.data[0].FileData.length > 0) {
                    for (let i in res.data[0].ColumnList) {
                        let value = i.replace(" ", "");
                        if (value != 'IsValidEmail' && value != 'PrimaryKey' && value != 'RowNo') {
                            dynamicFields.push({ key: value })
                        }
                    }
                }
                resolve(dynamicFields);
            } else {
                resolve(dynamicFields)
            }
        })
    })
}

_campaign.InitCampaign = function (page) {
    let campaignData = page.param.campaignData;
    let basicPageData = page.param.basicPageData
    if (page.param._id == undefined) {
        _page.BuildPage(page.PageContainer.find('.containerTab .basic-page'), basicPageData, undefined, false)
    }
    else {
        if (page.param._id) {
            _campaign.GetMarketingCampaignByID(page.param._id, campaignData, function (res) {
                _page.BuildPage(page.PageContainer.find('.containerTab .basic-page'), basicPageData, res, false);
                _page.nav.find('.nav-item.active[data-uid="' + _page.nav.find('.nav-item.active').attr('data-uid') + '"] .title').text(res.Name);
            });
        }
    }

    if (page.param._id == undefined) {
        _common.GetRegisteredEmail().then(function (data) {
            let domainList = "<option value=''></option>";
            for (let i = 0; i < data.length; i++) {
                if (data[i]) {
                    let allowedUsers = data[i].AllowedUsers;
                    if (allowedUsers && allowedUsers.length > 0) {
                        for (let j = 0; j < allowedUsers.length; j++) {
                            if (allowedUsers[j] == _initData.User._id)
                                domainList += "<option data-id='" + data[i]._id + "' value='" + data[i].EmailId + "'>" + data[i].EmailId + "</option>";
                        }
                    }
                }
            }
            page.PageContainer.find('[data-field="SenderEmail"]').html('').append(domainList);
        }, function (result) {
            _common.Loading(false);
        });
        page.PageContainer.find('[data-field="CampaignCategoryID"]').html('');
        page.PageContainer.find('[data-field="Tags"]').html('');
        page.PageContainer.find('[data-field="Tags"]').closest('.multiselect-native-select').find('div ul').html('');

        // page.PageContainer.find('[data-field="CampaignCategoryID"]').attr('disabled', true);
        // page.PageContainer.find('[data-field="Tags"] button').attr('disabled', true);

    } else {
        if (page.param._id) {
            _campaign.GetMarketingCampaignByID(page.param._id, campaignData, function (res) {
                let selectedEmail = res.SenderEmail;
                let selectedDomain = selectedEmail.split('@')[1];
                let selectedCategory = res.CampaignCategoryID;
                let selectedTag = res.Tags;
                _common.GetRegisteredEmail().then(function (data) {
                    page.PageContainer.find('[data-field="SenderEmail"]').html('');
                    let domainList = "<option value=''></option>";
                    for (let i = 0; i < data.length; i++) {
                        if (data[i]) {
                            let allowedUsers = data[i].AllowedUsers;
                            if (allowedUsers && allowedUsers.length > 0) {
                                for (let j = 0; j < allowedUsers.length; j++) {
                                    if (allowedUsers[j] == _initData.User._id) {
                                        if (selectedEmail == data[i].EmailId)
                                            domainList += "<option data-id='" + data[i]._id + "' value='" + data[i].EmailId + "' selected>" + data[i].EmailId + "</option>";
                                        else
                                            domainList += "<option data-id='" + data[i]._id + "' value='" + data[i].EmailId + "'>" + data[i].EmailId + "</option>";
                                    }
                                }
                            }
                        }
                    }
                    page.PageContainer.find('[data-field="SenderEmail"]').append(domainList);
                }, function (result) {
                    _common.Loading(false);
                });

                _admin.GetDomainName().then(function (domainData) {
                    domainData = domainData.filter(p => p.DomainName == selectedDomain);
                    let selectedDomainId = domainData[0]._id;
                    _common.GetListSchema("CampaignCategory")
                        .then(function (categoryData) {
                            if (categoryData) {
                                categoryData = categoryData.filter(p => p.DomainID == selectedDomainId);
                                page.PageContainer.find('[data-field="CampaignCategoryID"]').html('');
                                let campaignCategoryList = '<option></option>';
                                for (let i = 0; i < categoryData.length; i++) {
                                    if (selectedCategory == categoryData[i].Key)
                                        campaignCategoryList += "<option value='" + categoryData[i].Key + "' selected>" + categoryData[i].Value + "</option>";
                                    else
                                        campaignCategoryList += "<option value='" + categoryData[i].Key + "'>" + categoryData[i].Value + "</option>";
                                }
                                page.PageContainer.find('[data-field="CampaignCategoryID"]').append(campaignCategoryList);
                            }
                        }, function (result) {
                            swal(result.message, { icon: 'error' });
                        });
                    _common.GetListSchema("MarketingTags")
                        .then(function (TagsData) {
                            if (TagsData) {
                                let selectCtrl = page.PageContainer.find('[data-field="Tags"]');
                                selectCtrl.html('');
                                selectCtrl.closest('.multiselect-native-select').find('div ul').html('');
                                TagsData = TagsData.filter(p => p.DomainID == selectedDomainId);
                                let campaignTagsList = '';
                                for (let i = 0; i < TagsData.length; i++) {
                                    campaignTagsList += "<option value='" + TagsData[i].Key + "'>" + TagsData[i].Value + "</option>";
                                }
                                let ctrl = selectCtrl.closest('.ctrl-container');
                                selectCtrl.closest('.multiselect-native-select').remove();
                                ctrl.append(selectCtrl);
                                selectCtrl.html('').append(campaignTagsList).multiselect({
                                    nonSelectedText: '',
                                    enableCaseInsensitiveFiltering: true,
                                    includeSelectAllOption: true,
                                    onDropdownShow: function (event) {
                                        ctrl.find('.btn.multiselect-clear-filter').trigger('click');
                                    }
                                });
                                selectCtrl.multiselect('select', selectedTag, true);
                            }
                        }, function (result) {
                            swal(result.message, { icon: 'error' });
                        });
                }, function (result) {
                    _common.Loading(false);
                });
                setTimeout(function () {
                    let recipientsListID = page.PageContainer.find('[data-field="ContactListID"] div').attr('data-id');
                    let drpdwnString = '';
                    campaignData.DynamicField = [];
                    if (recipientsListID) {
                        _campaign.LoadDynamicField(recipientsListID).then(data => {
                            if (data) {
                                campaignData.DynamicField = data;
                                for (let i = 0; i < data.length; i++) {
                                    drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${data[i].key}</a>`;
                                }
                                let subjectDiv = page.PageContainer.find('[data-field="Subject"]').closest('.vld-parent');
                                subjectDiv.find('label').append(`<div class="btn-group">
                                                    <i class="" data-toggle="dropdown"></i>
                                                    <div class="dropdown-menu">
                                                        ${drpdwnString}
                                                    </div>
                                                </div>`);
                            }
                        });
                    }
                }, 100);
            });
        }
    }

    page.PageContainer.on('change', '[data-field="SenderEmail"]', function () {
        let selectedEmail = $(this).find('[value="' + $(this).val() + '"]').text();
        let selectedDomain = selectedEmail.split('@')[1];
        _admin.GetDomainName().then(function (domainData) {
            domainData = domainData.filter(p => p.DomainName == selectedDomain);
            let selectedDomainId = '';
            if (domainData.length != 0) {
                selectedDomainId = domainData[0]._id;
            }
            _common.GetListSchema("CampaignCategory")
                .then(function (categoryData) {
                    if (categoryData) {
                        categoryData = categoryData.filter(p => p.DomainID == selectedDomainId);
                        page.PageContainer.find('[data-field="CampaignCategoryID"]').html('');
                        let campaignCategoryList = '<option></option>';
                        for (let i = 0; i < categoryData.length; i++) {
                            campaignCategoryList += "<option value='" + categoryData[i].Key + "'>" + categoryData[i].Value + "</option>";
                        }
                        page.PageContainer.find('[data-field="CampaignCategoryID"]').append(campaignCategoryList);
                    }
                }, function (result) {
                    swal(result.message, { icon: 'error' });
                });
            _common.GetListSchema("MarketingTags")
                .then(function (TagsData) {
                    if (TagsData) {
                        TagsData = TagsData.filter(p => p.DomainID == selectedDomainId);
                        let campaignTagsList = '';
                        for (let i = 0; i < TagsData.length; i++) {
                            campaignTagsList += "<option value='" + TagsData[i].Key + "'>" + TagsData[i].Value + "</option>";
                        }
                        let selectCtrl = page.PageContainer.find('[data-field="Tags"]');
                        let ctrl = selectCtrl.closest('.ctrl-container');
                        selectCtrl.closest('.multiselect-native-select').remove();
                        ctrl.append(selectCtrl);
                        selectCtrl.html('').append(campaignTagsList).multiselect({
                            nonSelectedText: '',
                            enableCaseInsensitiveFiltering: true,
                            includeSelectAllOption: true,
                            onDropdownShow: function (event) {
                                ctrl.find('.btn.multiselect-clear-filter').trigger('click');
                            }
                        });
                    }
                }, function (result) {
                    swal(result.message, { icon: 'error' });
                });
        }, function (result) {
            _common.Loading(false);
        });
    });

    page.PageContainer.on('click', '[data-field-name="ContactListID"] div', function () {
        let recipientsListID = $(this).data().id;
        let drpdwnString = '';
        campaignData.DynamicField = [];
        if (recipientsListID) {
            _campaign.LoadDynamicField(recipientsListID).then(data => {
                if (data) {
                    campaignData.DynamicField = data;
                    for (let i = 0; i < data.length; i++) {
                        drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${data[i].key}</a>`;
                    }
                    let subjectDiv = page.PageContainer.find('[data-field="Subject"]').closest('.vld-parent');
                    subjectDiv.find('label .btn-group').remove();
                    subjectDiv.find('label').append(`<div class="btn-group">
                                            <i class="" data-toggle="dropdown"></i>
                                            <div class="dropdown-menu">
                                                ${drpdwnString}
                                            </div>
                                        </div>`);
                }
            });
        }
    });

    page.PageContainer.on('click', '[data-field="ContactListID"] .fa-times', function () {
        let subjectDiv = page.PageContainer.find('[data-field="Subject"]').closest('.vld-parent');
        let previousText = subjectDiv.find('input').val();
        let hashSplitString = previousText.trim().split('##');
        let newString = '';
        for (let i = 0; i < hashSplitString.length; i++) {
            if (campaignData.DynamicField.filter(p => p.key == hashSplitString[i]).length == 0) {
                newString += hashSplitString[i];
            }
        }
        subjectDiv.find('input').val(newString);
        subjectDiv.find('label .btn-group').remove();
        campaignData.DynamicField = [];
    });

    page.PageContainer.on('keypress', '[data-field="Subject"]', function (event) {
        let textContainer = $(this).closest('.vld-parent');
        let keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '35' || event.key == '#') {
            let text = $(this).val();
            let findHash = text.charAt(text.length - 1)
            if (findHash == '#') {
                if (campaignData.DynamicField && campaignData.DynamicField.length > 0) {
                    textContainer.find('.btn-group i').trigger('click');
                } else {
                    swal('Please select Recipients List.', { icon: 'info' });
                }
            }
        }
    });

    page.PageContainer.on('click', '.dropdown-menu .status-dropdown', function () {
        let selectedText = $(this).text();
        let inputElement = $(this).closest('.vld-parent').find('input');
        let previousText = inputElement.val();
        inputElement.val(previousText + selectedText + '## ');
    });

    page.PageContainer.find('.ddlTimezone').append(_controls.BuildSelectOptions(_common.GetListSchemaDataFromInitData('Timezone'), 'Key', 'Value'));
    page.PageContainer.find('.section-schedule-now').hide();
    page.PageContainer.find('.containerTab.tab-pane .btnBack').on('click', function (e) {
        page.PageContainer.find('.campaign .nav-item .nav-link').removeClass('active');
        let name = $(this).attr('back');
        let currentTab = $(this).attr('current');
        page.PageContainer.find('.campaign .nav-item .nav-link[name=' + name + ']').addClass('active');
        page.PageContainer.find('.containerTab.tab-pane').removeClass('active');
        page.PageContainer.find('.containerTab.tab-pane[name=' + name + ']').addClass('active');

        page.PageContainer.find('.campaign .nav-item a[name="' + name + '"]')
            .closest('li')
            .removeClass('completed')
            .addClass('selected');
        page.PageContainer.find('.campaign .nav-item a[name="' + currentTab + '"]')
            .closest('li')
            .removeClass('completed')
            .removeClass('selected');
        if (name == "template") {
            if (campaignData.data.TemplateID) {
                let param =
                {
                    content: campaignData.data.TemplateBlockContent,
                    pageStyle: campaignData.data.pageStyle,
                    templateId: campaignData.data.TemplateID
                };
                _marketing.OpenEditor(page, param);
            } else {
                _marketing.InitTemplateCreate(page);
            }
        }
    })

    page.PageContainer.find('.containerTab.tab-pane').on('click', '.btnNext', function () {
        _campaign.IterateCampaignTab(page, this);
    })
    page.PageContainer.find('.campaign .nav-link').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
    // radio button on campaign html page
    let idBtn1 = "rdoSchedule1" + (new Date()).getMilliseconds();
    let idBtn2 = "rdoSchedule2" + (new Date()).getMilliseconds();
    let nameBtn = "rdoSchedule" + (new Date()).getMilliseconds();

    let radioBtn1 = $(`<div class="form-group col-sm-6 mt-2">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="${idBtn1}" value="SendNow" opr="SendNow" name="${nameBtn}"
                            class="custom-control-input rdoSchedule" checked>
                        <label class="custom-control-label h5" for="${idBtn1}">Send Now</label>
                    </div>
                </div>`);
    let radioBtn2 = $(`<div class="form-group col-sm-6 mt-2">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="${idBtn2}" value="SendLetter" opr="SendLetter" name="${nameBtn}"
                            class="custom-control-input rdoSchedule">
                        <label class="custom-control-label h5" for="${idBtn2}">Schedule Later</label>
                    </div>
                </div>`);
    page.PageContainer.find('.tab-content [name="schedule"] > .row').prepend(radioBtn1, radioBtn2);

    page.PageContainer.find('.containerTab.tab-pane .rdoSchedule').on('change', function () {
        if ($(this).val() == "SendNow") {
            page.PageContainer.find('.section-schedule-now').hide();
            page.PageContainer.find('.section-send-now').show();

        } else {
            page.PageContainer.find('.section-schedule-now').show();
            page.PageContainer.find('.section-send-now').hide();
        }
    })
    page.PageContainer.find(".containerTab").on("click", ".txtScheduleDate, .txtScheduleTime", function (event) {
        if ($(event.target).hasClass("txtScheduleDate")) {
            page.PageContainer.find(".schedule-date-error").text("")
        } else {
            page.PageContainer.find(".schedule-time-error").text("")
        }

        $(event.target).closest(".input-group").removeClass("has-error")
    })
    page.PageContainer.find('.containerTab.tab-pane .btnSchdule').on('click', function () {
        let currentUid = $(this).closest('.page-content').attr('data-uid');
        let status = page.PageContainer.find('.containerTab.tab-pane.active .rdoSchedule:checked').val();
        if (status == undefined) {
            swal('Please select schedule option', { icon: "info" });
            return false;
        }
        if (status == 'SendLetter') {
            let isValid = _common.Validate(page.PageContainer.find('.containerTab.tab-pane.active'));
            if (!isValid)
                return false;

            let scheduledDateInput = page.PageContainer.find('.containerTab.tab-pane.active .txtScheduleDate');
            let scheduledTimeInput = page.PageContainer.find('.containerTab.tab-pane.active .txtScheduleTime');

            let selectedDate = scheduledDateInput.val();
            let selectedTime = scheduledTimeInput.val();
            let timeZone = page.PageContainer.find('.containerTab.tab-pane.active .ddlTimezone').val();
            let format = _initData.Company.Setting.DateFormat + ' ' + _initData.Company.Setting.CalendarTimeFormat;
            let scheduleDate = moment.tz(selectedDate + ' ' + selectedTime, format, timeZone);
            if (!scheduleDate.isValid()) {
                scheduledTimeInput.closest(".input-group").addClass("has-error");
                return
            }

            if (moment().tz(timeZone).isSameOrAfter(scheduleDate)) {
                swal("You can't select past date & time to create a new campaign", { icon: "error" });
                return false;
            }
        }
        _campaign.LoadCampaignValue(page, 'schedule');
        _campaign.SaveData({ 'campaignData': campaignData, 'currentUid': currentUid });
    });
    page.PageContainer.find('.campaign .nav-link').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    page.PageContainer.find('.sendTestEmail').modal('hide');
    page.PageContainer.find('.openSendMailModal').on('click', function () {
        page.PageContainer.find('.sendTestEmail').modal('show');
    })

    page.PageContainer.find('.txttestMail').keypress(function (event) {
        page.PageContainer.find(".txttestMail").css('width', "");
        let keycode = (event.keyCode ? event.keyCode : event.which);
        this.textContent = this.textContent.trim().replace(",", "");
        if (keycode == '13' || keycode == '32' || keycode == '44' || keycode == '9') {
            if (!this.textContent) { this.textContent = ""; return; }
            let renderEmails = function (email) {
                let isValid = _common.isValidCommaSepratedEmailAddress(email);
                let divElement = $(`<div style="display: inline-block; max-width:100%;" data-email="${email}" class="${isValid ? "" : "bg-danger text-white"} lookup-email border btn-outline-secondary btn btn-sm mb-1 mr-1">${email}<span class="fas fa-times pl-2 remove-email"></span> </div>`);
                page.PageContainer.find(".txttestMail").before(divElement);
            }
            renderEmails(this.textContent.trim());
            this.textContent = "";
            event.preventDefault();
        }
    });
    page.PageContainer.find('.txttestMail').keydown(function (event) {
        let keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '8' && this.textContent == "") {
            $(".txttestMail").prev().remove();
        }
    });

    page.PageContainer.on('blur', '.txttestMail', function (e) {
        if (!this.textContent) { this.textContent = ""; return; }
        else if (this.textContent.trim() == '' || this.textContent == ",") { this.textContent = ""; return; }
        let renderEmails = function (email) {
            let isValid = _common.isValidCommaSepratedEmailAddress(email);
            let divElement = $(`<div style="display: inline-block; max-width:100%;" data-email="${email}" class="${isValid ? "" : "bg-danger text-white"} lookup-email border btn-outline-secondary btn btn-sm mb-1 mr-1">${email}<span class="fas fa-times pl-2 remove-email"></span></div>`);
            page.PageContainer.find(".txttestMail").before(divElement);
        }
        renderEmails(this.textContent.trim());
        this.textContent = "";
    });

    page.PageContainer.on('dblclick', '.lookup-email', function () {
        let lookupEmail = $(this);
        lookupEmail.text(lookupEmail.text().trim());
        lookupEmail.attr("contenteditable", "true").removeClass('bg-danger text-white btn-outline-secondary');
        lookupEmail.children().remove();
        lookupEmail.unbind('blur');
        lookupEmail.unbind('keypress');
        lookupEmail.focus();

        lookupEmail.blur(function () {
            let value = this.textContent.trim();
            if (value == "") { $(this).remove(); return; }
            let renderEmails = function (email) {
                let isValid = _common.isValidCommaSepratedEmailAddress(email);
                isValid ? lookupEmail.removeClass('bg-danger text-white') : lookupEmail.addClass('bg-danger text-white');
            }
            renderEmails(value);
            lookupEmail.attr('data-email', value).removeAttr("contenteditable").addClass('btn-outline-secondary');
            lookupEmail.append('<span class="fas fa-times pl-2 remove-email"></span>');
        });

        $(this).keypress(function () {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13' || keycode == '32' || keycode == '44' || keycode == '9') {
                let value = this.textContent.trim();
                if (value == "") { $(this).remove(); return; }
                let renderEmails = function (email) {
                    let isValid = _common.isValidCommaSepratedEmailAddress(email);
                    isValid ? lookupEmail.removeClass('bg-danger text-white') : lookupEmail.addClass('bg-danger text-white');
                }
                renderEmails(value);
                lookupEmail.attr('data-email', value).removeAttr("contenteditable").addClass('btn-outline-secondary');
            }
        });
    });

    page.PageContainer.on('click', '.remove-email', function () {
        $(this).closest(".lookup-email").remove();
    });

    page.PageContainer.find('.send_emails').click(function () {
        page.PageContainer.find(".txttestMail").focus();
        page.PageContainer.find(".txttestMail").css('width', "10px");
    });

    page.PageContainer.find('.btnSendTestMail').on('click', function () {
        let testMail = '';
        page.PageContainer.find('.lookup-email').each(function () {
            testMail += $(this).attr('data-email') + ',';
        });
        testMail = testMail.substr(0, testMail.length - 1);
        if (testMail == '' || testMail.length == 0) {
            swal('Please enter email ID', { icon: "info" });
            return false;
        }
        if (!_common.isValidCommaSepratedEmailAddress(testMail)) {
            swal('Please enter valid email IDs', { icon: "info" });
            return false;
        }
        let param = { 'url': '/api/marketing/SendCampaignMail', 'type': 'post', 'contentType': 'application/json' };
        param.data = { mailID: testMail, campaignData: page.param.campaignData.data };

        _common.FetchApiByParam(param, function (res) {
            page.PageContainer.find('.sendTestEmail').modal('hide');
            swal(res.msg, { icon: "success" });
            page.PageContainer.find(".lookup-email").remove();
        });
    });
}

_campaign.GetMarketingCampaignByID = function (_id, campaignData, callback) {
    campaignData.data = {};
    let url = { 'url': '/api/marketing/GetMarketingCampaignByID', 'type': 'post', 'contentType': 'application/json' };
    url.data = { '_id': _id };
    _common.FetchApiByParam(url, function (res) {
        if (res.data.length > 0) {
            let data = res.data[0];
            let fields = data.FieldSchema.split(',');
            for (let i = 0; i < fields.length; i++) {
                if (fields[i] !== undefined && fields[i] != "")
                    campaignData.data[fields[i]] = data[fields[i].toString()];
            }
            callback(res.data[0]);
        }
    });
}


//Events : "MarketingCampaignList" GridOpenFunction
_campaign.MarketingCampaignGridOpenFunction = function (param) {
    if (param.record.StatusID == 1) {
        _common.GetPageDetail('MarketingCampaignBasic', function (pageData) {
            new Page({ 'pageName': 'CreateCampaign', '_id': param.record._id, 'campaignData': {}, 'basicPageData': pageData });
        });
    } else if (param.record.StatusID == 4) {
        new Page({ 'pageName': 'CampaignStatistics', '_id': param.record._id, 'pageTitle': param.record.Name });
    }
}

_campaign.InitStatistics = function (page) {
    _common.Loading(true);
    page["CampaignStatsData"] = {};
    let url = { 'url': '/api/marketing/GetCampaignReport', 'type': 'post', 'contentType': 'application/json' };
    url.data = { '_id': page.param._id };
    _page.nav.find('.nav-item.active[data-uid="' + _page.nav.find('.nav-item.active').attr('data-uid') + '"] .title').text(page.param.pageTitle);
    // page.PageContainer.find('.campaign-report-content .campaign-title').text(page.param.pageTitle);
    _common.FetchApiByParam(url, function (res) {
        _common.Loading(false);
        if (res.message == "success") {
            if (!(res.data && res.data.length > 0)) {
                swal("No Campaign found", { icon: true })
            }
            let { SendOutData, ...Campaign } = res.data[0];
            page.PageContainer.find('.campaign-report-content .campaign-Subject').text(Campaign.Subject);
            page.PageContainer.find('.campaign-report-content .campaign-Name').text(Campaign.Name);

            let StatsData = {
                Sent: [],
                NotSent: [],
                SoftBounce: [],
                HardBounce: [],
                Unsubscribe: [],
                Error: [],
                UniqueOpen: [],
                UniqueClick: [],
                Delivered: [],
                Requested: []
            }
            SendOutData.forEach(
                sendD => {
                    if (sendD.IsBounced) {
                        StatsData.HardBounce.push(sendD)
                    }
                    if (sendD.IsSoftBounce) {
                        StatsData.SoftBounce.push(sendD)
                    }
                    if (sendD.IsUnsubscribed) {
                        StatsData.Unsubscribe.push(sendD)
                    }
                    if (sendD.Sent) {
                        StatsData.Sent.push(sendD)
                    }
                    if (!sendD.Sent) {
                        StatsData.NotSent.push(sendD)
                    }
                    if (sendD.Error) {
                        StatsData.Error.push(sendD)
                    }
                    if (sendD.IsOpened) {
                        StatsData.UniqueOpen.push(sendD)
                    }
                    if (sendD.Links && sendD.Links.length > 0) {
                        StatsData.UniqueClick.push(sendD)
                    }
                    if (!sendD.Error && !sendD.IsBounced && !sendD.IsSoftBounce && sendD.Sent) {
                        StatsData.Delivered.push(sendD)
                    }

                    StatsData.Requested.push(sendD)
                }
            )

            let stats = {
                totalRequest: SendOutData.length,
                totalSent: StatsData.Sent.length,
                totalSoftBounce: StatsData.SoftBounce.length,
                totalHardBounce: StatsData.HardBounce.length,
                totalUnsubscribe: StatsData.Unsubscribe.length,
                totalError: StatsData.Error.length,
                totalUniqueOpen: StatsData.UniqueOpen.length,
                totalUniqueClick: StatsData.UniqueClick.length,
                totalDelivered: StatsData.Delivered.length

            }
            stats.totalBounce = stats.totalSoftBounce + stats.totalHardBounce;
            page["CampaignStatsData"] = { StatsData, stats, Campaign };
            let deliveryRate = ((stats.totalDelivered / stats.totalSent) * 100);
            let openRate = stats.totalDelivered ? ((stats.totalUniqueOpen / stats.totalDelivered) * 100) : 0;
            let clickThroughRate = stats.totalDelivered ? (stats.totalUniqueClick / stats.totalDelivered) * 100 : 0;
            let bounceRate = ((stats.totalBounce / stats.totalSent) * 100);
            page.PageContainer.find('.campaign-delivery-rate').text(deliveryRate.toFixed(2) + '%');
            page.PageContainer.find('.campaign-open-rate').text((openRate.toFixed(2)) + '%');
            page.PageContainer.find('.campaign-bouncerate').text((bounceRate.toFixed(2)) + '%');
            page.PageContainer.find('.campaign-ctr').text((clickThroughRate.toFixed(2)) + '%');

            page.PageContainer.find('.campaign-request').text(stats.totalRequest);
            page.PageContainer.find('.campaign-sent').text(stats.totalSent);
            page.PageContainer.find('.campaign-delivered').text(stats.totalDelivered);
            page.PageContainer.find('.campaign-error').text(stats.totalHardBounce + stats.totalSoftBounce);
            page.PageContainer.find(".campaign-unsubscribe").text(stats.totalUnsubscribe);
            //+++++++++++++++++++++Binging Stats+++++++++++++++++++++++++++++++++++
            _campaign.BindStatisticPieChart(page, stats);//pie chart in first page
            _campaign.BindLinkData(page, StatsData, Campaign.TemplateEmailContent);//click maps page
            // FIXME Done after error
            _campaign.BindGioCountryChart(page, StatsData, stats);
            _campaign.BindGioCityChart(page, StatsData, stats);
            _campaign.BindErrorPieChart(page, StatsData, stats)

            //+++++++++++++++++++++++Binding Useragent data+++++++++++++++++++++++++++++++++++
            let { browserDetails, deviceDetails, platfromDetails } = _campaign.GetUserAgentDetails(StatsData)
            _campaign.BindDonutDeviceChart(page, deviceDetails, stats);
            _campaign.BindDonutOSChart(page, platfromDetails, stats);
            _campaign.BindDonutBrowserChart(page, browserDetails, stats)

            //+++++++++++++hiding loaders and buttons++++++++++++++++++++++++++++++++++++++++
            page.PageContainer.find('[tab="byLocation"] .city-view').addClass("d-none")//to hide city map after rendering
            page.PageContainer.find(".toggle-region-view").removeClass("d-none"); //toggle city/country button 
            page.PageContainer.find(".location-tab-loader").addClass("d-none");//hide location loader
            page.PageContainer.find(".device-tab-loader").addClass("d-none");//hide device loader
            page.PageContainer.find(".btn-statistics-pdf .spinner-border-sm").addClass("d-none"); //pdf button loader
            page.PageContainer.find(".btn-statistics-pdf").prop('disabled', false)
        } else if (res.statuscode == 500) {
            swal(res.message, { "icon": "error" });
        }
    });

    page.PageContainer.find(".object-detail").on("click", ".camp-stat-popup", function () {
        _campaign.campaignPopups(page, $(this))
    })
    page.PageContainer.find('.campaign-report-nav').on('click', '.nav-link', function () {
        page.PageContainer.find('.campaign-report-nav .nav-link').removeClass('active');
        $(this).addClass('active');
        let tab = $(this).attr('tab');
        page.PageContainer.find('.campaign-report-content .tab-pane').removeClass('active').removeClass('show');
        page.PageContainer.find('.campaign-report-content .tab-pane[tab="' + tab + '"]').addClass('active').addClass('show');
        // _campaign.ShowTabWiseData(tab, page, res.data);
    })

    page.PageContainer.find('[tab="byLocation"] .toggle-region-view input').on("click", function () {
        $(this).closest("label").addClass("active");
        if ($(this).hasClass("cty")) {
            page.PageContainer.find('[tab="byLocation"] .city-view').removeClass("d-none");
            page.PageContainer.find('[tab="byLocation"] .country-view').addClass("d-none");
            page.PageContainer.find('[tab="byLocation"] .toggle-region-view .cnt').closest("label").removeClass("active");

        } else {
            page.PageContainer.find('[tab="byLocation"] .city-view').addClass("d-none");
            page.PageContainer.find('[tab="byLocation"] .country-view').removeClass("d-none");
            page.PageContainer.find('[tab="byLocation"] .toggle-region-view .cty').closest("label").removeClass("active");

        }
    })

    page.PageContainer.find('.btn-statistics-pdf').on('click', function () {
        // let url = { 'url': '/noauth/api/mail/printpdf', 'type': 'post', 'contentType': 'application/json' };
        // url.data = { 'content': content };
        let headTag = `<head>\
                            <meta charset="utf-8">\
                            <meta name="viewport" content="width=device-width, initial-scale=1">\
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />\
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>\
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>\
                            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>\
                            <style>\
                                .title-h{\
                                    background-color:#007EE3 !important; -webkit-print-color-adjust: exact; display:inline-block; color:#fff !important; padding:10px 20px;\
                                }\
                                .template-button{\
                                    -webkit-print-color-adjust: exact; display:inline-block;\
                                }\
                                  .page-footer, .page-footer-space {\
                                    height: 60px;\
                                    font-size:25px;\
                                  }\
                                  .page-footer {\
                                    text-align:center;\
                                    bottom: 0;\
                                    width: 100%;\
                                    background-color:#007EE3 !important; -webkit-print-color-adjust: exact; color:#fff !important; padding:10px 20px;\
                                  }\
                                  @page {\
                                    margin: 0;\
                                  }\
                                  @media print {\
                                     tfoot {display: table-footer-group;}\
                                     .p-border { page-break-before: always; height:100%;}\
                                  }\
                            </style>\
                       </head>`;
        let printWindow = window.open('', '', 'width=800,height=650');
        let reportPdfName = $("#page-nav .nav-item.active .title").text() + "_" + moment().tz(_initData.User.MySettings.Timezone).format("DD-MM-YYYY");
        //code to print window once image loaded
        let executeCommandAfterLoad = function () {
            document.title = reportPdfName;
            window.print();
            window.close();
        }
        let initPage = `<div class="initPage p-border" style="height:1080px">
        <div style="margin-top:500px; text-align:center">
            <img src="/images/my-sales.jpg" onload="reportPdfName='${reportPdfName}';executeCommandAfterLoad=${executeCommandAfterLoad};executeCommandAfterLoad()" style="height:180px"></img>
        </div>
    </div>
    <div class="page-footer">Copyright © ${moment().tz(_initData.User.MySettings.Timezone).format('YYYY')} Optimiser</div>  `;

        let locationPdfView = $(page.PageContainer.find('.byLocation')[0].outerHTML);
        locationPdfView.find(".toggle-region-view").remove();
        locationPdfView.find(".country-view").removeClass("d-none");
        locationPdfView.find(".city-view").removeClass("d-none");

        printWindow.document.write('<html>' + headTag + '<body>' + initPage);
        printWindow.document.write('<table><tbody><tr><td><div class="p-5 detail p-border"><h2 class="p-3 px-5 title-h">Campaign Statistics</h2>'
            + page.PageContainer.find('.detail').html() +
            '</div><div class="p-5 byLocation p-border"><h2 class="p-3 px-5 title-h">Open By Location</h3>'
            + locationPdfView.html() +
            '</div><div class="p-5 clicksMap p-border"><h2 class="p-3 px-5 title-h">Clicks Map</h2>'
            + page.PageContainer.find('.clicksMap').html() +
            '</div><div class="p-5 devices p-border"><h2 class="p-3 px-5 title-h">Devices</h2>'
            + page.PageContainer.find('.devices').html() +
            '</div><div class="p-5 statistics p-border"><h2 class="p-3 px-5 title-h">Error Statistics</h2>'
            + page.PageContainer.find('.statistics').html() +
            '</div></td></tr></tbody><tfoot><tr><td><div class="page-footer-space"></div></td></tr></tfoot></table></body></html>');
    });
    page.PageContainer.find(".email-status-exp").click(function () {
        let formModal = _common.DynamicPopUp({
            title: "Email Status Explanation",
            body: page.PageContainer.find("#emailStatus").html(),
            cancelButtonName: "Close"
        });
        formModal.find(".btnSave").addClass("d-none")
        formModal.find(".modal-lg").removeClass("modal-lg")
        formModal.modal("show")
    })
}

_campaign.GetUserAgentDetails = function (StatsData) {
    let browserDetails = {};
    let deviceDetails = {};
    let platfromDetails = {};
    let UniqueOpen = StatsData.UniqueOpen;
    UniqueOpen.forEach(
        ud => {
            if (ud.UserAgent) {
                let userAgent = ud.UserAgent;
                browserDetails[userAgent.browser] = browserDetails[userAgent.browser] ? ++browserDetails[userAgent.browser] : 1;
                deviceDetails[userAgent.device] = deviceDetails[userAgent.device] ? ++deviceDetails[userAgent.device] : 1;
                platfromDetails[userAgent.platfrom] = platfromDetails[userAgent.platfrom] ? ++platfromDetails[userAgent.platfrom] : 1;
            }
        }
    )
    return { browserDetails, deviceDetails, platfromDetails }
}


_campaign.BindLinkData = function (page, StatsData, template) {
    let clickDetails = StatsData.UniqueClick;
    if (!template)
        return
    let clickObj = {};
    page.PageContainer.find('.clicksMap .template-div').html(template);
    //disable href events events
    page.PageContainer.find('.clicksMap .template-div [href]').each(
        function () {
            $(this).css({
                "pointer-events": "none",
                "cursor": "default"
            })
        }
    )
    clickDetails.forEach(
        clickD => {
            if (clickD && clickD.Links) {
                clickD.Links.forEach(
                    link => {
                        clickObj[link.LinkID] = { count: clickObj[link.LinkID] ? ++clickObj[link.LinkID]["count"] : 1, url: link.Url }
                    }
                )
            }
        }
    )

    let tbl = '<tr class="head-row"><th class="border-top-0">URL</th><th class="border-top-0">Clicks</th></tr>';
    for (let lin in clickObj) {
        page.PageContainer.find('.clicksMap .template-div a[link="' + lin + '"]').append('<span class="link-count-style">' + clickObj[lin].count + '</span>');
        tbl += '<tr  class="camp-stat-popup cursor-pointer" data-type="clickcount" data-link="' + lin + '"  data-url="' + clickObj[lin].url + '"><td style="white-space: unset; word-break:break-all;">' + clickObj[lin].url + '</td><td>' + clickObj[lin].count + '</td></tr>';
    }
    page.PageContainer.find('.clicksMap .link-table').append($(tbl));

}
_campaign.BindDayLineChart = function (page, data) {

    if (data != undefined) {
        let param = {};
        let dayArray = [];
        dayArray.push(['Day', 'Opened']);
        let date = new Date();
        for (let i = 0; i < data.Day.length; i++) {
            dayArray.push([data.Day[i].Day.toString() + ' ' + _constantClient.Months[date.getMonth()].SortName, parseInt(data.Day[i].Opend)]);
        }
        let dataTable = google.visualization.arrayToDataTable(dayArray);
        param.data = dataTable;
        param.options = {
            title: 'Subscriber activity chart(monthly)',
            curveType: 'function',
            legend: { position: 'bottom' },
            colors: ['#007EE3'],
        };
        _chart.LineChart(page.PageContainer.find('.campaign-dayby-linechart')[0], param);
    }
}

_campaign.BindErrorPieChart = function (page, StatsData, stats) {
    let param = {};
    let errorArray = [];
    let softBouncePer = 0;
    let hardBouncePer = 0;
    let unknownErrorPer = 0;
    // if (stats.totalBounce > 0) {
    errorArray.push(['Error', 'Value']);
    errorArray.push(['Soft Bounce', parseInt(stats.totalSoftBounce)]);
    errorArray.push(['Hard Bounce', parseInt(stats.totalHardBounce)]);
    errorArray.push(["Other", parseInt(stats.totalError)]);
    param.data = google.visualization.arrayToDataTable(errorArray);
    param.options = { pieSliceText: 'value', is3D: true, sliceVisibilityThreshold: 0, title: "Error statistics" };
    _chart.PieChart(page.PageContainer.find('.campaign-error-pie-chart')[0], param);

    softBouncePer = ((stats.totalSoftBounce / stats.totalSent) * 100).toFixed(2);
    hardBouncePer = ((stats.totalHardBounce / stats.totalSent) * 100).toFixed(2);
    unknownErrorPer = ((stats.totalError / stats.totalSent) * 100).toFixed(2)

    // }
    let table = `
      <table class="table table-hover"  >
        <thead>
            <th>Event</th>
            <th>Count</th>
        </thead>
        <tbody>
            <tr class="camp-stat-popup cursor-pointer" data-type="soft-bounce"><td>Soft Bounce</td><td>${stats.totalSoftBounce}(${softBouncePer}%) </td><tr>
            <tr class="camp-stat-popup cursor-pointer" data-type="hard-bounce"><td>Hard Bounce</td><td>${stats.totalHardBounce}(${hardBouncePer}%)</td><tr>
            <tr class="camp-stat-popup cursor-pointer" data-type="other-error"><td>Other Error</td><td>${stats.totalError}(${unknownErrorPer}%)</td><tr>
      </tbody>
      </table>
    `
    page.PageContainer.find(".error-stats-tab").html(table);
}

_campaign.BindStatisticPieChart = function (page, stats) {
    let param = {};
    osArray = [
        ['Events', "Number of emails"],
        ['Opened', stats.totalUniqueOpen],
        ['Bounce', stats.totalBounce],
        ['Unopened', stats.totalDelivered - stats.totalUniqueOpen]
    ]
    param.data = google.visualization.arrayToDataTable(osArray);
    param.options = {
        pieSliceText: 'value',
        is3D: true,
        title: "Marketing statistics",
        sliceVisibilityThreshold: 0,
        // slices: { 0: { color: '#36b9cc' }, 1: { color: '#ef6c00' }, 2: { color: '#e74a3b' }, 3: { color: '#f6c23e' }}
    };
    _chart.PieChart(page.PageContainer.find('.campaign-piechart-statistics')[0], param);

    let uniqueOpenPer = ((stats.totalUniqueOpen / stats.totalDelivered) * 100).toFixed(2);
    let bouncePer = ((stats.totalBounce / stats.totalSent) * 100).toFixed(2);
    let unopnedPer = (((stats.totalDelivered - stats.totalUniqueOpen) / stats.totalDelivered) * 100).toFixed(2);
    let table = `
      <table class="table table-hover"  >
        <thead>
            <th>Event</th>
            <th>Count</th>
        </thead>
        <tbody>
            <tr  class="camp-stat-popup cursor-pointer" data-type="uniqueopen" > <td>Opened</td><td>${stats.totalUniqueOpen}(${uniqueOpenPer}%) </td><tr>
            <tr  class="camp-stat-popup cursor-pointer" data-type="bounce"> <td>Bounce</td><td>${stats.totalBounce}(${bouncePer}%)</td><tr>
            <tr  class="camp-stat-popup cursor-pointer" data-type="unopened"><td>Unopened</td><td>${stats.totalDelivered - stats.totalUniqueOpen}(${unopnedPer}%)</td><tr>
        </tbody>
      </table>
    `
    page.PageContainer.find(".campaign-static-table").html(table);
}

_campaign.BindGioCountryChart = function (page, StatsData, stats) {
    let param = {};
    let countryObj = {};
    let countryArray = [];
    let OpenData = StatsData.UniqueOpen;
    let table = '<table class="table bg-white byLocation-table"><tbody><tr class="head-row">'
    table += '<tr><th class="border-top-0">Country</th>';
    table += '<th class="border-top-0">Open</th>';
    table += '</tr>';
    OpenData.forEach(
        opd => {
            if (opd.Location) {
                let location = opd.Location;
                countryObj[location.countryName] = countryObj[location.countryName] ? ++countryObj[location.countryName] : 1
            }
        }
    )

    topCountries = _campaign.GetTopListCountryCity(countryObj);
    countryArray.push(['Country', 'Opened'])
    topCountries.forEach(
        country => {
            countryArray.push([country[0], country[1]])
            table += '<tr><td>' + country[0] + '</td><td>' + country[1] + '(' + ((country[1] / stats.totalDelivered) * 100).toFixed(2) + '%)</td></tr>';
        }
    )
    table += '</tbody></table>';
    page.PageContainer.find('.campaign-country-count').html(table);
    let dataTable = google.visualization.arrayToDataTable(countryArray);
    param.data = dataTable;
    param.options = {};
    _chart.GeoCharts(page.PageContainer.find('.campaign-gio-country-chart')[0], param);
}

_campaign.BindGioCityChart = function (page, StatsData, stats) {
    let param = {};
    let cityObj = {};
    let cityArray = [];
    let OpenData = StatsData.UniqueOpen;
    let table = '<table class="table bg-white bycity-table"><tbody><tr class="head-row">';
    table += '<tr><th class="border-top-0">City</th>';
    table += '<th class="border-top-0">Open</th>';
    table += '</tr>';

    cityArray.push(['City', 'Opened']);
    OpenData.forEach(
        opd => {
            if (opd.Location) {
                let location = opd.Location;
                cityObj[location.city] = cityObj[location.city] ? ++cityObj[location.city] : 1
            }
        }
    )
    topCities = _campaign.GetTopListCountryCity(cityObj);
    topCities.forEach(
        city => {
            cityArray.push([city[0], city[1]])
            table += '<tr><td>' + city[0] + '</td><td>' + city[1] + '(' + ((city[1] / stats.totalDelivered) * 100).toFixed(2) + '%)</td></tr>';
        }
    )
    page.PageContainer.find('.campaign-city-count').html(table);
    let dataTable = google.visualization.arrayToDataTable(cityArray);
    param.data = dataTable;
    param.options = {
        displayMode: 'markers',
        colorAxis: { colors: ['green'] },
        'magnifyingGlass.enable': true
    };

    _chart.GeoCharts(page.PageContainer.find('.campaign-gio-city-chart')[0], param);
}

_campaign.BindDonutDeviceChart = function (page, deviceDetails, stats) {
    let param = {};
    let deviceArray = [];
    let table = '<table class="table bg-white bycity-table"><tbody><tr class="head-row">';
    table += '<tr><th class="border-top-0">Device</th>';
    table += '<th class="border-top-0">Open</th>';
    table += '</tr>';
    deviceArray.push(['Device', 'Opened']);
    for (let device in deviceDetails) {
        deviceArray.push([device, parseInt(deviceDetails[device])]);
        table += '<tr><td>' + device + '</td><td>' + deviceDetails[device] + '(' + ((deviceDetails[device] / stats.totalUniqueOpen) * 100).toFixed(2) + '%)</td></tr>';
    }
    table += '</tbody></table>';
    page.PageContainer.find('.campaign-device-table').html(table);
    param.data = google.visualization.arrayToDataTable(deviceArray);
    param.options = { pieSliceText: 'value', is3D: true };
    _chart.PieChart(page.PageContainer.find('.campaign-device-chart')[0], param);
}

_campaign.BindDonutOSChart = function (page, platfromDetails, stats) {
    let param = {};
    let osArray = [];
    let table = '<table class="table bg-white bycity-table"><tbody><tr class="head-row">';
    table += '<tr><th class="border-top-0">OS</th>';
    table += '<th class="border-top-0">Open</th>';
    table += '</tr>';
    osArray.push(['OS', 'Opened']);
    for (let plat in platfromDetails) {
        osArray.push([plat, parseInt(platfromDetails[plat])]);
        table += '<tr><td>' + plat + '</td><td>' + platfromDetails[plat] + '(' + ((platfromDetails[plat] / stats.totalUniqueOpen) * 100).toFixed(2) + '%)</td></tr>';
    }
    table += '</tbody></table>';
    page.PageContainer.find('.campaign-os-table').html(table);
    param.data = google.visualization.arrayToDataTable(osArray);
    param.options = { pieSliceText: 'value', is3D: true };
    _chart.PieChart(page.PageContainer.find('.campaign-os-chart')[0], param);
}

_campaign.BindDonutBrowserChart = function (page, browserDetails, stats) {
    let param = {};
    let browserArray = [];
    let table = '<table class="table bg-white bycity-table"><tbody><tr class="head-row">';
    table += '<tr><th class="border-top-0">Browser</th>';
    table += '<th class="border-top-0">Open</th>';
    table += '</tr>';
    browserArray.push(['Browser', 'Opened']);
    for (let browser in browserDetails) {
        browserArray.push([browser, parseInt(browserDetails[browser])]);
        table += '<tr><td>' + browser + '</td><td>' + browserDetails[browser] + '(' + ((browserDetails[browser] / stats.totalUniqueOpen) * 100).toFixed(2) + '%)</td></tr>';

    }
    table += '</tbody></table>';
    page.PageContainer.find('.campaign-browser-table').html(table);
    param.data = google.visualization.arrayToDataTable(browserArray);
    param.options = { pieSliceText: 'value', is3D: true };
    _chart.PieChart(page.PageContainer.find('.campaign-browser-chart')[0], param);
}

//{ '_id': params._id, ModifiedDate: params.ModifiedDate }
_campaign.getOtherSetsOfAnayticData = function (params, set) {
    let url = { 'url': '/api/marketing/othersetofanalyticdata/' + set, 'type': 'post', 'contentType': 'application/json' };
    url.data = params;
    return new Promise(function (resolve, reject) {
        _common.FetchApiByParam(url, function (res) {
            if (res.message == "success") {
                resolve(res.data)
            }
        })
    })
}
//**************************************************** */Campaign Code End**********************************************//
_campaign.getMaxClickCount = function (Links) {
    let obj = {};
    Links.forEach(
        lk => {
            if (_common.isValidEmailAddress(lk.EmailID)) {
                obj[lk.EmailID] = obj[lk.EmailID] ? ++obj[lk.EmailID] : 1
            }
        }
    );

    console.log(obj, Object.keys(obj).length)
    return Object.keys(obj).length;
}

_campaign.campaignPopups = function (page, scope) {
    let { StatsData, stats, Campaign } = page["CampaignStatsData"];
    console.log(StatsData, stats, Campaign)
    let domainID = _initData.Company.Setting.MarketingSetting.Domains.find(d => d.DomainName == Campaign.SenderEmail.split("@")[1])._id;

    let { UniqueOpen } = StatsData;
    let type = $(scope).data().type;
    let params = {
        saveButtonName: "Download",
        cancelButtonName: "Close"
    }
    let body = "";
    let alltr = "";
    let table = "";
    let index = 0;
    let drpdwnString = '';
    let drpdwnArray = [];
    switch (type) {
        case "request":
            params["title"] = "Mails Requested";
            body = `<p><b>Total Request : </b> ${stats.totalRequest}</p>`;
            alltr = "";
            StatsData.Requested.forEach(
                (sd) => {
                    //========================TO SHOW ALL THE DETAILS OF ALL EMAILS=================
                    // let info = '';
                    // let status = "Delivered";
                    // if (sd.IsBounced || sd.IsSoftBounce) {
                    //     status = "Bounce"
                    // } else if (sd.IsUnsubscribed) {
                    //     status = "Unsubscribe";
                    //     info = `<span class="btnShowUnsubsLit pl-3 text-info" style="cursor: pointer;" title="Unsubscribe Info" data-toggle="tooltip" data-placement="right"><i class="fa fa-info-circle" aria-hidden="true"></i></span>`;
                    // }
                    // alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td><td>${status}${info}</td></tr>`;

                    let status = "Sent";
                    let reason = "unknown";
                    if (!sd.Sent) {
                        status = "Not Sent";
                        if (sd.NotSentReason && sd.NotSentReason.Bounce) {
                            reason = "Bounced"
                        } else if (sd.NotSentReason && sd.NotSentReason.Unsubscribe) {
                            reason = "Unsubscribed"
                        }
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td><td>${status} (${reason})</td></tr>`;
                    } else {
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td><td>${status}</td></tr>`;
                    }
                    if (!drpdwnArray.find(p => p == status)) {
                        drpdwnArray.push(status);
                        if (!sd.Sent)
                            drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${status}</a>`;
                        else
                            drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${status}</a>`;
                    }
                }
            )

            table = `
               <table class="table">
                   <thead>
                    <th width="100px">S.No</th>
                    <th width="750px">Email</th>
                    <th width="250px">Status
                        <div class="btn-group">
                            <i class="fa fa-caret-down pb-1 pl-1" data-toggle="dropdown" style="cursor: pointer;"></i>
                            <div class="dropdown-menu">
                                <a class="dropdown-item status-dropdown text-primary" href="#" data-type="clearFilter">Clear Filter</a>
                                ${drpdwnString}
                            </div>
                        </div>
                    </th>
                   </thead>
                   <tbody>
                   ${alltr}
                   </tbody>
               </table>
               `;
            body += table

            break;

        case "sent":
            params["title"] = "Mails Sent";
            body = `<p><b>Total Sent : </b> ${stats.totalSent}</p>`;
            alltr = "";
            StatsData.Sent.forEach(
                (sd) => {
                    let info = '';
                    let status = "Delivered";
                    if (sd.IsBounced || sd.IsSoftBounce) {
                        status = "Bounce"
                    } else if (sd.IsUnsubscribed) {
                        status = "Unsubscribe";
                        info = `<span class="btnShowUnsubsLit pl-3 text-info" style="cursor: pointer;" title="Unsubscribe Info" data-toggle="tooltip" data-placement="right"><i class="fa fa-info-circle" aria-hidden="true"></i></span>`;
                    }
                    alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td><td>${status}</td></tr>`;
                    if (drpdwnArray.filter(p => p == status).length == 0)
                        drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${status}</a>`;
                    drpdwnArray.push(status);
                }
            )

            table = `
               <table class="table">
                   <thead>
                    <th width="100px">S.No</th>
                    <th width="750px">Email</th>
                    <th width="250px">Status
                        <div class="btn-group">
                            <i class="fa fa-caret-down pb-1 pl-1" data-toggle="dropdown" style="cursor: pointer;"></i>
                            <div class="dropdown-menu">
                                <a class="dropdown-item status-dropdown text-primary" href="#" data-type="clearFilter">Clear Filter</a>
                                ${drpdwnString}
                            </div>
                        </div>
                    </th>
                   </thead>
                   <tbody>
                   ${alltr}
                   </tbody>
               </table>
               `;
            body += table

            break;

        case "delivered":
            params["title"] = "Mails Delivered";
            body = `<p><b>Total Delivered : </b> ${stats.totalDelivered}</p>`;
            alltr = "";
            StatsData.Delivered.forEach(
                (sd) => {
                    let status = sd.IsOpened && sd.IsOpened > 0 ? "Opened" : "Unopened"
                    alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td><td>${status}</td></tr>`;
                    if (drpdwnArray.filter(p => p == status).length == 0)
                        drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${status}</a>`;
                    drpdwnArray.push(status);
                }
            )
            table = `
               <table class="table">
                   <thead>
                    <th width="100px">S.No</th>
                    <th width="750px">Email</th>
                    <th width="250px">Status
                        <div class="btn-group">
                            <i class="fa fa-caret-down pb-1 pl-1" data-toggle="dropdown" style="cursor: pointer;"></i>
                            <div class="dropdown-menu">
                                <a class="dropdown-item status-dropdown text-primary" href="#" data-type="clearFilter">Clear Filter</a>
                                ${drpdwnString}
                            </div>
                        </div>
                    </th>
                   </thead>
                   <tbody>
                   ${alltr}
                   </tbody>
               </table>`;
            body += table
            break;

        case "bounce":
            params["title"] = "Mails Bounce";
            body = `<p><b>Total Bounce : </b> ${stats.totalBounce}</p>`;
            StatsData.Sent.forEach(
                sd => {
                    if (sd.IsBounced || sd.IsSoftBounce) {
                        let status = sd.IsSoftBounce ? "Soft Bounce" : "Hard Bounce";
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td><td>${status}</td></tr>`;
                        if (drpdwnArray.filter(p => p == status).length == 0)
                            drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${status}</a>`;
                        drpdwnArray.push(status);
                    }
                }
            )
            table = `
               <table class="table">
                   <thead>
                    <th width="100px">S.No</th>
                    <th width="750px">Email</th>
                    <th width="250px">Status
                        <div class="btn-group">
                            <i class="fa fa-caret-down pb-1 pl-1" data-toggle="dropdown" style="cursor: pointer;"></i>
                            <div class="dropdown-menu">
                                <a class="dropdown-item status-dropdown text-primary" href="#" data-type="clearFilter">Clear Filter</a>
                                ${drpdwnString}
                            </div>
                        </div>
                    </th>
                   </thead>
                   <tbody>
                   ${alltr}
                   </tbody>
               </table>
               `;
            body += table
            break;

        case "unsubscribe":
            params["title"] = "Mail Unsubscribe";
            body = `<p><b>Total Unsubscribe : </b> ${stats.totalUnsubscribe}</p>`;
            StatsData.Unsubscribe.forEach(
                sd => {

                    let status = null;
                    if (sd.CampaignSubsCategory && sd.CampaignSubsCategory.length == 0) {
                        status = "Full"
                    } else if (sd.CampaignUnsubsCategory.length > 0) {
                        status = "Partial"
                    }
                    if (status) {
                        info = `<span class="btnShowUnsubsLit pl-3 text-info" style="cursor: pointer;" title="Unsubscribe Info" data-toggle="tooltip" data-placement="right"><i class="fa fa-info-circle" aria-hidden="true"></i></span>`;
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td><td>${status}${info}</td></tr>`;
                        if (drpdwnArray.filter(p => p == status).length == 0)
                            drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${status}</a>`;
                        drpdwnArray.push(status);
                    }
                }
            )
            table = `
            <table class="table">
                <thead>
                 <th width="100px">S.No</th>
                 <th width="750px">Email</th>
                 <th width="250px">Status
                    <div class="btn-group">
                        <i class="fa fa-caret-down pb-1 pl-1" data-toggle="dropdown" style="cursor: pointer;"></i>
                        <div class="dropdown-menu">
                            <a class="dropdown-item status-dropdown text-primary" href="#" data-type="clearFilter">Clear Filter</a>
                            ${drpdwnString}
                        </div>
                    </div>
                 </th>
                </thead>
                <tbody>
                ${alltr}
                </tbody>
            </table>
            `;
            body += table
            break;

        case "uniqueopen":
            params["title"] = "Mails Opened";
            let uniqueCount = 0;
            let totalCount = 0;
            StatsData.UniqueOpen.forEach(
                (ol, index) => {
                    alltr += `<tr><td>${++index}</td><td data-type="email">${ol.To}</td><td>${ol.IsOpened}</td></tr>`;
                    uniqueCount++;
                    totalCount += parseInt(ol.IsOpened);
                    if (drpdwnArray.filter(p => p == ol.IsOpened).length == 0)
                        drpdwnString += `<a class="dropdown-item status-dropdown" href="#">${ol.IsOpened}</a>`;
                    drpdwnArray.push(ol.IsOpened);
                }
            )
            body = `<p class="p-2"><b>Total Opened : </b> ${totalCount} <b>Total Unique Opened : </b> ${uniqueCount}</p>`;
            table = `
            <table class="table">
                <thead>
                 <th width="100px">S.No</th>
                 <th width="750px">Email</th>
                 <th width="250px">Count
                    <div class="btn-group">
                        <i class="fa fa-caret-down pb-1 pl-1" data-toggle="dropdown" style="cursor: pointer;"></i>
                        <div class="dropdown-menu">
                            <a class="dropdown-item status-dropdown text-primary" href="#" data-type="clearFilter">Clear Filter</a>
                            ${drpdwnString}
                        </div>
                    </div>
                 </th>
                </thead>
                <tbody>
                ${alltr}
                </tbody>
            </table>
            `;
            body += table

            break;

        case "unopened":
            params["title"] = "Mails Unopened";
            let unopen = 0;
            StatsData.Delivered.forEach(
                sd => {
                    if (sd.IsOpened < 1) {
                        unopen++;
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td></tr>`;
                    }
                }
            )
            body = `<p><b>Total Unopened : </b> ${unopen}</p>`;
            table = `
            <table class="table">
                <thead>
                 <th width="100px">S.No</th>
                 <th width="750px">Email</th>
                </thead>
                <tbody>
                ${alltr}
                </tbody>
            </table>
            `;
            body += table
            break;

        case "clickcount":
            let { url, link } = $(scope).data();
            params["title"] = `Click on ${url}`;
            let uniqueClick = 0;
            let totalClick = 0;
            let unknownClicks = 0
            StatsData.UniqueOpen.forEach(
                lk => {
                    if (lk.Links && lk.Links.length > 0 && lk.Links.find(l => l.LinkID == link)) {
                        let clickedLink = lk.Links.find(l => l.LinkID == link)
                        uniqueClick++;
                        totalClick += parseInt(clickedLink.Count);

                        if (_common.isValidEmailAddress(lk.To)) {
                            alltr += `<tr><td>${++index}</td><td data-type="email">${lk.To}</td><td>${clickedLink.Count}</td></tr>`;
                        } else {
                            unknownClicks += parseInt(lk.ClickCount);
                        }
                    }
                }
            )
            if (unknownClicks > 0) {
                alltr += `<tr><td>${++index}</td><td>${"unknown"}</td><td>${unknownClicks}</td></tr>`;
            }
            body = `<p class="p-2"><b>Unique Clicks : </b> ${uniqueClick} <b>Total clicks : </b> ${totalClick}</p>`;
            table = `
            <table class="table">
                <thead>
                 <th width="100px">S.No</th>
                 <th width="750px">Email</th>
                 <th>Total clicks counts</th>
                </thead>
                <tbody>
                ${alltr}
                </tbody>
            </table>
            `;
            body += table
            break;

        case "hard-bounce":
            params["title"] = "Hard Bounce";
            let hBound = 0;
            StatsData.HardBounce.forEach(
                sd => {
                    if (sd.IsBounced) {
                        hBound++;
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td></tr>`;
                    }
                }
            )
            body = `<p><b>Total Hard Bounce : </b> ${hBound} `;
            table = `
            <table class="table">
                <thead>
                 <th width="100px">S.No</th>
                 <th width="750px">Email</th>
                </thead>
                <tbody>
                ${alltr}
                </tbody>
            </table>
            `;
            body += table
            break;

        case "soft-bounce":
            params["title"] = "Soft Bounce";
            let sBound = 0;
            StatsData.SoftBounce.forEach(
                sd => {
                    if (sd.IsSoftBounce) {
                        sBound++;
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td></tr>`;
                    }
                }
            )
            body = `<p><b>Total Soft Bounce : </b> ${sBound} `;
            table = `
            <table class="table">
                <thead>
                 <th width="100px">S.No</th>
                 <th width="750px">Email</th>
                </thead>
                <tbody>
                ${alltr}
                </tbody>
            </table>
            `;
            body += table
            break;

        case "other-error":
            params["title"] = "Error";
            let unError = 0;
            StatsData.Error.forEach(
                sd => {
                    if (sd.Error) {
                        unError++;
                        alltr += `<tr><td>${++index}</td><td data-type="email">${sd.To}</td></tr>`
                    }
                }
            )
            body = `<p><b>Total Error : </b> ${unError} `;
            table = `
            <table class="table">
                <thead>
                 <th width="100px">S.No</th>
                 <th width="750px">Email</th>
                </thead>
                <tbody>
                ${alltr}
                </tbody>
            </table>
            `;
            body += table
            break
    }
    params.body = body;
    let formModal = _common.DynamicPopUp(params);
    formModal.modal("show");
    formModal.find(".btnSave").remove()
    formModal.find('.status-dropdown').click(function () {
        $(this).closest('th').find('i').addClass('text-primary');
        let val = $(this).text();
        if ($(this).data().type == "clearFilter") {
            $(this).closest('th').find('i').removeClass('text-primary');
            $(this).closest('.table').find('tbody tr').each(function () {
                $(this).removeClass('d-none');
                $(this).find('td').removeClass('text-found');
            });
        } else {
            $(this).closest('.table').find('tbody tr').each(function () {
                let tr = $(this);
                val = val.toLowerCase();
                tr.find('td').each(function () {
                    let td = $(this);
                    let tdText = td.text();
                    if (tdText != undefined && tdText != '' && tdText.toLowerCase().split(" (")[0] == val) { //to handle -> "Not Sent (Bounce)""
                        td.addClass("text-found");
                    } else
                        td.removeClass("text-found");
                });
                if (tr.find('.text-found').length > 0)
                    tr.removeClass('d-none')
                else
                    tr.addClass('d-none');
            });
        }
    });
    formModal.find('.btnShowUnsubsLit').click(function () {
        let emailID = $(this).closest('tr').find('[data-type="email"]').text();
        let emailPopup = _common.DynamicPopUp({ title: '<b>' + emailID + '</b>', body: _marketing.emailPopupHTML });
        emailPopup.find('.modal-footer').html('').append('<button type="button" class="btn btn-primary pl-3 pr-3" data-dismiss="modal">OK</button>');
        let sdata = { Email: emailID, DomainId: domainID };
        let selectedDomainId = domainID;
        _common.ContainerLoading(true, emailPopup.find('.tblSubscription'));
        _common.FetchApiByParam({
            url: "api/common/getemailwiseunsubscribedata",
            type: "post",
            data: sdata
        }, function (result) {
            if (result.message == "success") {
                if (result.data && result.data.length > 0) {
                    let campaignCategoryList = result.data;
                    if (selectedDomainId != undefined)
                        campaignCategoryList = campaignCategoryList.filter(p => p.DomainID == selectedDomainId);
                    emailPopup.find('.tblSubscription tbody tr').remove();
                    for (let i = 0; i < campaignCategoryList.length; i++) {
                        let data = campaignCategoryList[i];
                        let tr = "<tr>";
                        tr += "<td>" + data.Value + "</td>";
                        if (!data.checked) {
                            tr += '<td class="subscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"><img src="/images/checked.svg"/></div></td>';
                            tr += '<td class="unsubscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"><img src="/images/unchecked.svg"/></div></td>';
                        } else {
                            tr += '<td class="subscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"></div><img src="/images/unchecked.svg"/></td>';
                            tr += '<td class="unsubscription"><div class="chkSelect' + data.Key + '" data-id="' + data.Key + '"><img src="/images/checked.svg"/></div></td>';
                        }
                        tr += '</tr>';
                        emailPopup.find('.tblSubscription tbody').append(tr);
                        _common.ContainerLoading(false, emailPopup.find('.tblSubscription'));
                        emailPopup.modal("show");
                        emailPopup.find(".tblSubscription tbody").on('change', '.chkSelect' + data.Key, function () {
                            emailPopup.find(".tblSubscription tbody .chkSelect" + data.Key).prop('checked', false);
                            $(this).prop('checked', true);
                        });
                    }
                } else {
                    swal('Data not found', { "icon": "error" });
                }
            } else {
                swal(result.message, { "icon": "error" });
            }
        });
    });
    formModal.find(".btnSave").click(
        function () {
            let url = { 'url': '/api/common/createjsontoxlxs', 'type': 'post', 'contentType': 'application/json' };
            let { rows, columns } = _campaign.getJsonFromTable(formModal)
            let fileName = `${Campaign.Name.split(" ").join("_")}_${type}_${moment().format("DD-MM-YY")}_${Math.floor(Math.random() * 1000000)}.xlsx`
            let obj = { data: [], params: { fileName: fileName } }
            obj.data = [
                {
                    SheetName: type,
                    columns: columns,
                    rows: rows
                }
            ]
            url.data = obj
            _common.FetchApiByParam(url, function (res) {
                if (res.message == "success") {
                    let dA = document.createElement("a");
                    dA.href = `/api/common/getjsontoxlxs/${res.data}`;
                    dA.click()
                } else {
                    swal("Something went wrong", { icon: "error" })
                }
            })
        }
    )
}

_campaign.getJsonFromTable = function (container) {
    let table = container.find("table");
    let columns = [];
    let rows = [];
    let ind = 1;
    table.find("thead tr th").each(function (e) {
        let th = $(this);
        columns.push({ header: th[0].innerText.trim(), key: ind++, width: Math.round(th.width() / 10) });
    })
    table.find("tbody tr ").each(function (e) {
        ind = 1;
        let tr = $(this);
        let row = {}
        tr.find("td").each(
            function () {
                row[ind++] = $(this).text();
            }
        )
        rows.push(row)
    })
    return { rows, columns }

}

_campaign.GetTopListCountryCity = function (entityObj) {
    let numReq = 5;
    entityArr = [];
    let dontAddUnknown = false;
    if (Object.keys(entityObj).length > numReq) {
        dontAddUnknown = true;
    }
    for (let entity in entityObj) {
        if (dontAddUnknown && entity == "unknown") {
            continue;
        }
        entityArr.push([entity, entityObj[entity]]);
    }
    let sortedEntities = entityArr.sort((e1, e2) => e2[1] - e1[1]);
    let filteredEntities = sortedEntities.slice(0, numReq);
    return filteredEntities
}