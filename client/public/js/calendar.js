const _calendar = {};
_calendar.grid = '';
_calendar.timeSheet = '';


_calendar.init = function () {
    var calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;

    _calendar.grid = '<div style="position:relative" class="day-grid">';
    _calendar.timeSheet = '<div class="col time-sheet">';

    for (var i = 0; i < 24; i++) {
        var t = i;
        var tt = '';
        var uClass = '';
        var lClass = '';

        if (calendarTimeFormat == 'hh:mm A') {
            if (i > 12) {
                t = i - 12;
                tt = ' PM';
            } else {
                tt = ' AM';
                if (i == 12) {
                    tt = ' PM';
                }
            }
        }
        var gtime = '' + t;
        if (t < 10) {
            gtime = '0' + t;
        }

        _calendar.grid += '<div class="hr u' + uClass + '" data-time="' + gtime + ':00' + tt + '"></div><div class="hr l' + lClass + '" data-time="' + gtime + ':30' + tt + '"></div>';
        if (i == 0) {
            _calendar.timeSheet += '<div class="p"><div class="c"></div></div>';
        } else {
            _calendar.timeSheet += '<div class="p"><div class="c">' + gtime + ':00' + tt + '</div></div>';
        }
    }
    _calendar.grid += '</div>';
    _calendar.timeSheet += '</div>';

}

_calendar.AddActivityFromOtherSource = function (page) {

    if (page.CurrentClickElement.next('.cal-activity-menu').length == 0) {
        let activityTypeList = _common.GetListSchemaDataFromInitData('ActivityType');
        let menu = ''
        for (let i = 0; i < activityTypeList.length; i++) {
            let obj = activityTypeList[i];
            menu += '<a class="dropdown-item" href="#" data-key="' + obj.Key + '">' + obj.Value + '</a>';
        }

        page.CurrentClickElement.after('<div class="dropdown-menu animated--fade-in cal-activity-menu">' + menu + '</div>');
        page.CurrentClickElement.attr('data-toggle', 'dropdown');
    }
    page.Grid.find('.page-top-button').on('click', '.cal-activity-menu .dropdown-item', function () {
        let param = {};
        param.pageName = $(this).attr("data-key");
        let cDate = new Date();
        let curDate = moment(cDate).tz(_initData.User.MySettings.Timezone).format();
        param.Timezone = _initData.User.MySettings.Timezone;
        param.StartDate = curDate;
        param.EndDate = moment(curDate).add('30', 'm').format()

        let gridObj = page.CurrentClickElement.closest('.msp-data-grid')[0].grid;
        let objectID = page.CurrentClickElement.closest('.object-detail')[0].obj.Data._id;
        if (page.PageData.PageName == 'ContactActivityList') {
            param.MatchFields = [{ Name: 'Attendees', Value: [objectID] }];
        }
        else if (page.PageData.PageName == 'PipelineActivityList') {
            let mainData = page.CurrentClickElement.closest('.object-detail')[0].obj.Data;
            let contactIdArray = _controls.ToEPARSE(mainData.ContactID)
            param.MatchFields = [{ Name: 'Attendees', Value: contactIdArray }, { Name: 'PipelineID', Value: objectID }];
        }
        param.onSave = function () {
            gridObj.ReloadData();
        }
        _calendar.NewActivityForm(param);
    });

}

_calendar.AdjustHeight = function (self) {
    var scrollViewHeight = $(window).height() - self.page.PageContainer.find('.view-calendar .view-scroll').offset().top;
    self.page.PageContainer.find('.view-calendar .view-scroll').height(scrollViewHeight).scrollTop(360);
}
_calendar.GetLongDay = function (day) {
    var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return weekday[day];
}
_calendar.GetShortDay = function (day) {
    var weekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    return weekday[day];
}
_calendar.GetVShortDay = function (day) {
    var weekday = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    return weekday[day];
}
_calendar.DaysInMonth = function (iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
}
_calendar.GetMonthView = function (currentMonth, currentYear, format, fromToDate) {
    let firstDay = (new Date(currentYear, currentMonth, 1)).getDay();
    let monthDays = _calendar.DaysInMonth(currentMonth, currentYear);
    let prevYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
    let prevMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
    let prevMonthDays = _calendar.DaysInMonth(prevMonth, prevYear);
    let nextYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
    let nextMonth = (currentMonth + 1) % 12;
    var calView = '<div class="row bg-white">';
    for (var i = 0; i < 7; i++) {
        var dayName = _calendar.GetShortDay(i);
        switch (format) {
            case 'long':
                dayName = _calendar.GetLongDay(i);
                break;
            case 'vshort':
                dayName = _calendar.GetVShortDay(i);
                break;
            case 'short':
                dayName = _calendar.GetShortDay(i);
                break;
        }
        calView += '<div class="col text-center border-bottom cal-dayname week-view-head pb-10 ">' + dayName + '</div>';

    }
    calView += '</div>';
    if (firstDay == 0) {
        calView += '<div>'
    } else {
        calView += '<div class="row date-container-row bg-white">';
    }
    var daysCount = 0;
    for (let i = 0; i < firstDay; i++) {
        daysCount++;
        let borderleft = '';
        if (i == 0) {
            borderleft = 'border-left';
        }
        var cday = prevMonthDays - (firstDay - i) + 1;
        var cDate = new Date(prevYear, prevMonth, cday);
        var stDate = moment(cDate).format('YYYY-MM-DD');
        if (fromToDate && i == 0) {
            fromToDate.StartDate = stDate
        }


        calView += '<div class="col inactive-days text-gray-500 text-center border-bottom border-right ' + borderleft + ' " data-date="' + stDate + '">' + cday + '</div>';
    }
    for (let i = 1; i <= monthDays; i++) {
        let borderleft = '';
        if (daysCount % 7 == 0) {
            calView += '</div><div class="row date-container-row bg-white">';
            borderleft = 'border-left';
        }
        var cDate = new Date(currentYear, currentMonth, i);
        var stDate = moment(cDate).format('YYYY-MM-DD');
        let datediv = '<div class="date-container">' + i + '</div>';
        calView += '<div class="col active-days text-center border-bottom border-right ' + borderleft + '" data-date="' + stDate + '">' + datediv + '</div>';
        daysCount++;
    }
    for (let i = 1; i <= 7; i++) {
        if (daysCount % 7 == 0) {
            calView += '</div>';
            break;
        } else {
            var cDate = new Date(nextYear, nextMonth, i);
            var stDate = moment(cDate).format('YYYY-MM-DD');
            if (fromToDate) {
                fromToDate.EndDate = stDate
            }

            calView += '<div class="col inactive-days text-gray-500 text-center border-right border-bottom" data-date="' + stDate + '">' + i + '</div>';
        }
        daysCount++;
    }
    return calView;
}

_calendar.ReturnTwoDigitDateString = function (num) {
    var gtime = '' + num;
    if (num < 10) {
        gtime = '0' + num;
    }
    return gtime;
}

_calendar.convertTimeFormat = (time, format) => {
    if (time.includes("AM") || time.includes("PM")) {
        if (format == '24') {
            return moment(time, ["h:mm A"]).format("HH:mm")
        } else {
            return time
        }
    } else {
        if (format == '24') {
            return time
        } else {
            return moment(time, ["HH:mm"]).format("hh:mm A");
        }
    }

}

_calendar.InitCalendarPage = function (calendarObj) {
    _calendar.init();
    _calendar.activityMenu = '';
    _calendar.eventFilterMenu = '';
    _calendar.userFilterMenu = '';
    _calendar.userFilterArray = [];
    _calendar.userFilterInfoArray = [];
    _calendar.userFilterDetails = [];
    _calendar.navCalDropdown = '';
    _calendar.eventFilterArray = [];
    _calendar.activityMenu = '';

    // IMPROVEMENT NOTE: Can get the values from list schema like : {key : "meeting" , value : "Meeting" , ShowOnActivityMenu: true , ShowOnFilterMenu : true}
    // Set Event Filter in that way 
    let activityTypeList = _common.GetListSchemaDataFromInitData('ActivityType');
    _calendar.eventFilterMenu = $('<div class="event-filter-menu mt-3"><span class="font-weight-bold">Activity Filters</span></div>');
    for (let i = 0; i < activityTypeList.length; i++) {
        let obj = activityTypeList[i];
        _calendar.activityMenu += '<a class="dropdown-item" href="#" data-key="' + obj.Key + '"><span class="activity-context-menu">' + obj.Icon + '</span> ' + obj.Value + '</a>';
        _calendar.eventFilterMenu.append('<label class="lblChk ' + obj.Key + ' "><input type="checkbox" class="event-filter-item chkHide " data-key="' + obj.Key + '" checked><span class="checkmark"></span>' + obj.Value + '</label>');
        _calendar.eventFilterArray.push(obj.Key);
    }

    _calendar.userFilterMenu = $('<div class="user-filter-menu"><span class="font-weight-bold">Colleagues</span> <span class="reset-userFilter">Reset</span><div class="mt-2" style="margin-bottom: 0.75rem !important;"><input type="text" class="form-control form-control-sm txtSearchInColleagues" placeholder="Search Colleagues"></div><div class="user-all-list"></div></div>');
    _calendar.userFilterMenu.find('.user-all-list').append('<label class="lblChk"><input type="checkbox" class="user-filter-item chkHide" data-id="' + _initData.User._id + '" checked><span class="checkmark"></span>My Calendar');
    _calendar.userFilterArray.push(_controls.ToEJSON("objectid", _initData.User._id));
    _calendar.userFilterInfoArray.push(_initData.User)

    _calendar.GetUserFilterDetails(function (users) {
        if (users.message == "success")
            for (let i = 0; i < users.data.length; i++) {
                if (users.data[i]._id != _initData.User._id) {
                    let suspendUserClass = "";
                    if (users.data[i].UserStatus) {
                        if (users.data[i].UserStatus == _constantClient.USER_STATUS.SUSPEND)
                            suspendUserClass = " suspended-user"
                        else if (users.data[i].UserStatus == _constantClient.USER_STATUS.REPLACE)
                            suspendUserClass = " replaced-user"
                    }

                    let userFilterListItem = '<label class="lblChk ' + suspendUserClass + '"><input type="checkbox" class="user-filter-item chkHide" data-id="' + users.data[i]._id + '"><span class="checkmark"></span>' + users.data[i].FirstName + ' ' + users.data[i].LastName
                    _calendar.userFilterMenu.find('.user-all-list').append(userFilterListItem);
                    let user = {};
                    user._id = users.data[i]._id;
                    user.FirstName = users.data[i].FirstName;
                    user.LastName = users.data[i].LastName
                    _calendar.userFilterDetails.push(user);
                }
            }
    });

    calendarObj.page.PageContainer.on('mouseover', '.view-calendar .view-scroll', function () {
        let containerWidth = $(this).find('.activity-container').width();
        $(this).find('.single-day-activity.actOwner:not(.recurred-activity)').draggable({
            revert: "invalid", scroll: true, containment: '.view-scroll .row', grid: [containerWidth, 20]
            , helper: function (event) {
                var helper = $(this.outerHTML);
                helper.css({ 'right': 'unset', 'width': $(event.target).css("width"), 'left': 'unset' });
                return helper;
            },
            start: function (e) { $(this).hide() },
            stop: function (e, ui) {
                $(this).show()
                $(e.target).css({ "top": ui.position.top, "left": ui.position.left, "width": $(e.target).css("width") });

            },
            drag: function (event, ui) {
                // if ($(".view-scroll").offset().top >= ui.offset.top) {
                //     $(".view-scroll").scrollTop($(".view-scroll").scrollTop() - 2)
                // }
            }
        });
    })

    calendarObj.page.PageContainer.on('mouseover', '.view-calendar .row', function () {
        let containerWidth = $(this).find('.month-activity-container').width();
        $(this).find('.month-cal-activity.actOwner:not(.recurred-activity)').draggable({ revert: "invalid", containment: '.view-calendar', grid: [containerWidth + 1, 20] });

    })

    calendarObj.page.PageContainer.on('mousedown', '.month-cal-activity:not(.recurred-activity),.single-day-activity', function () {
        $(this).addClass('clicked-activity')
    })

    calendarObj.page.PageContainer.on('mouseup', '.view-calendar', function () {
        $(document).find('.clicked-activity').removeClass('clicked-activity')
    })

    /***CLICK EVENTS */
    calendarObj.page.PageContainer.on('click', '.todayBtn', function () {
        var datePicker = $(document).find('.nav-calendar .msp-date-picker');
        let crdate = new Date(moment(_initData.CurrentUtcTime).tz(_initData.User.MySettings.Timezone).format("YYYY-MM-DDTHH:mm:ss"));
        let newD = moment(crdate).format('YYYY-MM-DD');
        datePicker[0].currentDate = crdate;
        datePicker[0].currentYear = moment(crdate).get('year');
        datePicker[0].currentMonth = moment(crdate).get('month');
        _controls.DatePicker.Build(datePicker);

        datePicker.find('[data-date=' + newD + ']').click();
    })

    calendarObj.page.PageContainer.on("click", '.view-calendar .activity-container:not(.bg-gray-100) .hr', function () {
        calendarObj.page.PageContainer.find('.view-calendar .hr.btn-primary').removeClass('btn-primary');
        $(this).addClass('btn-primary');
    });

    calendarObj.page.PageContainer.on("click", function () {
        $(document).find('.right-clicked-activity').removeClass('right-clicked-activity')
    });

    calendarObj.page.PageContainer.on("click", '.cal-activity-menu .dropdown-item', function () {
        let param = {};
        param.pageName = $(this).attr("data-key");
        param.cal = $(this).closest('.page-content')[0]._calendar;
        let cDate = new Date();
        let curDate = moment(cDate).tz(_initData.User.MySettings.Timezone).format();
        param.Timezone = _initData.User.MySettings.Timezone;
        param.StartDate = curDate;
        param.EndDate = moment(curDate).add('30', 'm').format()
        param.Timezone = _initData.User.MySettings.Timezone;
        _calendar.NewActivityForm(param);
    });

    //SECTION  double click on menu to open meeting dialog
    calendarObj.page.PageContainer.on('dblclick', '.view-scroll .day-grid .hr:not(.other-user):not(.pending)', function (e) {
        e.preventDefault();
        //STUB close any existing menu
        calendarObj.page.PageContainer.find('.view-calendar .hr.btn-primary').removeClass('btn-primary');
        calendarObj.page.PageContainer.find(".pending-activity-rightclick-menu").removeClass("show").hide().find('*').removeClass('disabled');
        calendarObj.page.PageContainer.find(".activity-rightclick-menu").removeClass("show").hide().find('*').removeClass('disabled');
        calendarObj.page.PageContainer.find(".cal-rightclick-menu").removeClass("show").hide().find('*').removeClass('disabled');
        $(this).addClass('btn-primary');
        //STUB open dialog 

        let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
        if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
            calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
        }
        let format = "YYYY-MM-DD " + calendarTimeFormat;
        let stTime = e.currentTarget.dataset.time;
        let enTime = e.currentTarget.nextSibling.dataset.time;
        let stDate = e.currentTarget.closest('.activity-container').dataset.date;

        params = {
            pageName: (_initData.User.MySettings.CalendarDoubleClick),
            Timezone: _initData.User.MySettings.Timezone,
            StartDate: moment.tz(stDate + " " + stTime, format, _initData.User.MySettings.Timezone).format(),
            EndDate: moment.tz(stDate + " " + enTime, format, _initData.User.MySettings.Timezone).format()
        }
        params.cal = $(this).closest('.page-content')[0]._calendar;
        if (params.pageName) {
            _calendar.NewActivityForm(params);
        }
    })

    //!SECTION 
    //SECTION right click on month view
    calendarObj.page.PageContainer.on('contextmenu', '.col .active-days', function (e) {
        _common.BeforeContextMenuOpenEvent()
        e.preventDefault();
        e.stopPropagation();
        //STUB hide other menus
        calendarObj.page.PageContainer.find('.view-calendar .hr.btn-primary').removeClass('btn-primary');
        calendarObj.page.PageContainer.find('*').removeClass('right-clicked-activity');
        let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
        if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
            calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
        }
        let cDate = e.currentTarget.dataset.date + " " + new Date().getHours() + ":" + new Date().getMinutes();
        let format = "YYYY-MM-DD " + calendarTimeFormat;
        stDateTime = moment(cDate).tz(_initData.User.MySettings.Timezone).format();
        enDateTime = moment(stDateTime).add('30', 'm').toDate()
        let parentOffset = $(".activity-rightclick-menu").closest('.col').offset();
        let contentMenu = calendarObj.page.PageContainer.find(".cal-rightclick-menu");

        contentMenu.attr({
            'data-startdatetime': stDateTime,
            'data-enddatetime': enDateTime
        });

        _common.ShowContextMenu(e, parentOffset, contentMenu);
    });
    //!SECTION 

    //open space right click
    calendarObj.page.PageContainer.on('contextmenu', '.view-calendar .activity-container .hr', function (e) {
        _common.BeforeContextMenuOpenEvent()
        e.preventDefault();
        e.stopPropagation();
        calendarObj.page.PageContainer.find('.view-calendar .hr.btn-primary').removeClass('btn-primary');
        calendarObj.page.PageContainer.find('*').removeClass('right-clicked-activity');
        $(this).addClass('btn-primary');
        let stTime = e.currentTarget.dataset.time;
        let stDate = e.currentTarget.closest('.activity-container').dataset.date;
        let enTime = e.currentTarget.nextSibling.dataset.time;
        let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
        if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
            calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
        }
        let format = "YYYY-MM-DD " + calendarTimeFormat;
        let stDateTime = moment.tz(stDate + " " + stTime, format, _initData.User.MySettings.Timezone).format();
        let enDateTime = moment.tz(stDate + " " + enTime, format, _initData.User.MySettings.Timezone).format();

        let userCalendarId = e.currentTarget.closest('.activity-container').dataset.userid;
        if (userCalendarId != _initData.User._id) {
            swal('You cannot create Activity on Other User\'s Calendar.', { icon: "info", });
            return false;
        }
        // if (organizerId == undefined)
        organizerId = _initData.User._id;
        let parentOffset = calendarObj.page.PageContainer.find(".cal-rightclick-menu").closest('.col').offset();
        let contentMenu = calendarObj.page.PageContainer.find(".cal-rightclick-menu");
        contentMenu.attr({ 'data-startdatetime': stDateTime, 'data-enddatetime': enDateTime, 'data-organizerid': organizerId });
        _common.ShowContextMenu(e, parentOffset, contentMenu);

    })

    calendarObj.page.PageContainer.on("click", '.cal-rightclick-menu .dropdown-item', function () {
        let param = {};
        param.pageName = $(this).attr("data-key");
        param.Timezone = _initData.User.MySettings.Timezone;
        if ($(this).closest('.cal-rightclick-menu').attr('data-startdatetime') != undefined)
            param.StartDate = $(this).closest('.cal-rightclick-menu').attr('data-startdatetime')
        if ($(this).closest('.cal-rightclick-menu').attr('data-enddatetime') != undefined)
            param.EndDate = $(this).closest('.cal-rightclick-menu').attr('data-enddatetime')
        if ($(this).closest('.cal-rightclick-menu').attr('data-organizerid') != undefined)
            param.OrganizerId = $(this).closest('.cal-rightclick-menu').attr('data-organizerid')
        var cal = $(this).closest('.page-content')[0]._calendar;
        param.cal = cal;
        _calendar.NewActivityForm(param);
    });

    //Activity Right Click
    calendarObj.page.PageContainer.on("contextmenu", '.single-day-activity:not(.other-user):not(.pending),.month-cal-activity:not(.other-user):not(.pending),.all-day-activity:not(.other-user):not(.pending)', function (e) {
        _common.BeforeContextMenuOpenEvent()
        e.stopPropagation();
        e.preventDefault();
        calendarObj.page.PageContainer.find('.view-calendar .hr.btn-primary').removeClass('btn-primary');
        calendarObj.page.PageContainer.find('*').removeClass('right-clicked-activity');
        calendarObj.page.PageContainer.find('.activity-rightclick-menu').find('*').removeClass('disabled').removeClass('active');
        calendarObj.page.PageContainer.find('*').removeClass('right-clicked-activity');
        $(this).addClass('right-clicked-activity');

        let userCalendarId = e.currentTarget.closest('.activity-container').dataset.userid;
        if (userCalendarId != _initData.User._id) {
            swal('You cannot make changes on Other User\'s Calendar.', { icon: "info", });
            return false;
        }

        var parentOffset = $(".activity-rightclick-menu").closest('.col').offset();
        let activityID = $(this).attr('data-id');
        let activityType = $(this).attr('data-activitytype');
        let statusType = $(this).attr('data-statustype');
        let activityLock = $(this).attr('data-lockActivity');
        let ownerID = $(this).attr('data-owner');
        let inviteAttendee = $(this).attr('data-inviteattendee');
        let activityStartDate = $(this).attr('data-originalstartdate')
        let isRecurred = false;
        let parentID = activityID;
        let editDisabled = '';
        let inviteDisabled = '';
        if ($(this).hasClass('recurred-activity')) {
            editDisabled = 'disabled'
            isRecurred = true;
            inviteDisabled = 'disabled'
        }
        if ($(this).attr('data-parentid') != "")
            parentID = $(this).attr('data-parentid');


        /**
         * TO append the case in list schema 
         */
        let activityModuleType = '';
        switch (activityType) {
            case "Meeting":
                activityModuleType = "Calendar"
                break;
            case "Call":
                activityModuleType = "Calendar"
                break;
            case "Saloon":
                activityModuleType = "Shop"
                break;
            default:
                activityModuleType = "Calendar"
                break;
        }


        let contentMenu;

        if (activityModuleType == "Shop")
            contentMenu = calendarObj.page.PageContainer.find('.activity-rightclick-menu.calendar-shop-item');
        else
            contentMenu = calendarObj.page.PageContainer.find('.activity-rightclick-menu.calendar-activity-item');

        contentMenu.attr({
            'data-activityid': activityID,
            'data-activitytype': activityType,
            'data-parentid': parentID,
            'data-isrecurred': isRecurred,
            'data-owner': ownerID,
            'data-activitystartdate': activityStartDate
        }).find('.edit-activity').addClass(editDisabled);

        contentMenu.find('[data-key = ' + statusType + ']').addClass('active')
        if (activityLock == "true") {
            contentMenu.find('[data-key = lock]').addClass('active')
        }
        else
            contentMenu.find('[data-key = unlock]').addClass('active')

        if (inviteAttendee == 'false') {
            inviteDisabled = 'disabled'
        }

        contentMenu.find('[data-key= add-participant]').addClass(inviteDisabled);
        _common.ShowContextMenu(e, parentOffset, contentMenu);

    });

    calendarObj.page.PageContainer.on("click", '.activity-rightclick-menu .dropdown-item', function () {
        let action = $(this).attr("data-key")
        let activityId = $(this).closest('.activity-rightclick-menu').attr('data-activityid');
        let activityType = $(this).closest('.activity-rightclick-menu').attr('data-activitytype');
        let isRecurred = JSON.parse($(this).closest('.activity-rightclick-menu').attr('data-isrecurred'));
        let parentID = $(this).closest('.activity-rightclick-menu').attr('data-parentID');
        let owner = $(this).closest('.activity-rightclick-menu').attr('data-owner');
        let activityStartDate = $(this).closest('.activity-rightclick-menu').attr('data-activitystartdate');
        var cal = $(this).closest('.page-content')[0]._calendar;
        let param = {};
        switch (action) {
            case "edit":
                param.pageName = activityType
                param._id = activityId
                param.cal = $(this).closest('.page-content')[0]._calendar;
                _calendar.EditActivityForm(param)
                break;
            case "view":
                param.pageName = activityType
                param._id = activityId
                _calendar.ReadOnlyActivityForm(param);
                break;
            case "add-participant":
                param.pageName = activityType
                param._id = activityId;
                param.onBuildComplete = function (form) {
                    setTimeout(function () {
                        form.find('[data-field="UserParticipants"]').focus();
                    }, 500);
                }
                _calendar.EditActivityForm(param)
                break;
            case "delete":
                if (_calendar.BackDateDeleteCheck(activityStartDate)) {
                    if (isRecurred) {
                        param.activityid = activityId;
                        param.cal = $(this).closest('.page-content')[0]._calendar;
                        _calendar.DeleteRecurrenceActivityModal(param);
                    }
                    else {
                        if (_calendar.BackDateDeleteCheck(activityStartDate)) {
                            _common.ConfirmDelete({ message: 'You want to delete this activity ?' }).then((confirm) => {
                                if (confirm) {
                                    _calendar.DeleteSingleActivity(activityId, function (result) {
                                        if (result.message == "success") {
                                            swal('Activity has been removed successfully.', { icon: "success", });
                                            cal.ResetActivities();
                                            _calendar.activityDeleted({ ResultData: result.oldActivityData, activityId, owner, activityStartDate })
                                        }
                                        else {
                                            swal(result.message, { icon: "error", });
                                            cal.ResetActivities();
                                        }
                                    })
                                }
                            });
                        }
                        else {
                            swal(_constantClient.BACKDATEDISALLOW, { icon: "info", });
                        }
                    }
                }
                else {
                    swal(_constantClient.BACKDATEDISALLOW, { icon: "info", });
                }
                break;
            case "busy":
            case "free":
                _calendar.ChangeUserActivityStatusType(activityId, action, function (result) {
                    if (result.message == "success") {
                        swal('Status has been updated', { icon: "success", });
                        cal.ResetActivities();
                    }
                })
                break;
            case "lock":
            case "unlock":
                _calendar.ChangeUserActivityLock(activityId, action, function (result) {
                    if (result.message == "success") {
                        swal('Status has been updated', { icon: "success", });
                        cal.ResetActivities();
                    }
                })
            case "payment":
                _shop.ShopPaymentFromCalendar(activityId)
            // _calendar.ShopPayment(activityId, action, function (result) {
            //     if (result.message == "success") {
            //         swal('Payment successful', { icon: "success", });
            //         cal.ResetActivities();
            //     }
            // })
        }
    });


    //pending activity right click 
    calendarObj.page.PageContainer.on("contextmenu", '.pending', function (e) {
        _common.BeforeContextMenuOpenEvent()
        e.preventDefault();
        e.stopPropagation();
        calendarObj.page.PageContainer.find('.view-calendar .hr.btn-primary').removeClass('btn-primary');
        calendarObj.page.PageContainer.find('*').removeClass('right-clicked-activity');
        var parentOffset = $(".pending-activity-rightclick-menu").closest('.col').offset();

        let userCalendarId = e.currentTarget.closest('.activity-container').dataset.userid;
        if (userCalendarId != _initData.User._id) {
            swal('You cannot make changes on Other User\'s Calendar.', { icon: "info", });
            return false;
        }

        let activityID = $(this).attr('data-id');
        let activityType = $(this).attr('data-activitytype');
        let isRecurred = false;
        let parentID = activityID;
        let contentMenu = calendarObj.page.PageContainer.find('.pending-activity-rightclick-menu');

        if ($(this).hasClass('recurred-activity'))
            isRecurred = true;
        if ($(this).attr('data-parentid') != "")
            parentID = $(this).attr('data-parentid');
        contentMenu.attr({
            'data-activityid': activityID,
            'data-activitytype': activityType,
            'data-parentid': parentID,
            'data-isrecurred': isRecurred
        });

        _common.ShowContextMenu(e, parentOffset, contentMenu);

    })

    calendarObj.page.PageContainer.on("click", '.pending-activity-rightclick-menu .dropdown-item', function () {
        let action = $(this).attr("data-key");
        let activityId = $(this).closest('.pending-activity-rightclick-menu').attr('data-activityid');
        let activityType = $(this).closest('.pending-activity-rightclick-menu').attr('data-activitytype');
        let isRecurred = JSON.parse($(this).closest('.pending-activity-rightclick-menu').attr('data-isrecurred'));
        let parentID = $(this).closest('.pending-activity-rightclick-menu').attr('data-parentID');
        var cal = $(this).closest('.page-content')[0]._calendar;
        let sparam = {};
        sparam.activity = activityId;
        if (isRecurred) {
            sparam.isRecurrence = true;
            sparam.recParent = parentID;
        }
        switch (action) {
            case "view":
                let param = {};
                param.pageName = activityType
                param._id = activityId
                param.pending = true;
                param.cal = cal;
                _calendar.ReadOnlyActivityForm(param);
                break;
            case "accept":
                sparam.response = "accepted";
                _calendar.UpdateUserResponseModal(sparam, function (result) {
                    if (result.message == 'success') {
                        swal('Activity added to the calendar successfully.', { icon: "success", });
                        cal.ResetActivities();
                    }
                })
                break;
            case "reject":
                sparam.response = "rejected";
                _calendar.UpdateUserResponseModal(sparam, function (result) {
                    if (result.message == 'success') {
                        swal('Activity removed from the calendar successfully.', { icon: "success", });
                        cal.ResetActivities();
                    }
                })
                break;
        }
    });


    calendarObj.page.PageContainer.on("dblclick", '.single-day-activity:not(.other-user[data-lockactivity="true"]),.month-cal-activity:not(.other-user[data-lockactivity="true"]),.all-day-activity:not(.other-user[data-lockactivity="true"])', function () {
        let param = {};
        param.pageName = $(this).data('activitytype');
        param._id = $(this).data('id');
        var cal = $(this).closest('.page-content')[0]._calendar;
        param.cal = cal;

        if ($(this).hasClass('pending')) {
            param.pending = true;
            _calendar.ReadOnlyActivityForm(param);
        }
        else if ($(this).hasClass('other-user')) {
            _calendar.ReadOnlyActivityForm(param);
        }
        else {
            _calendar.EditActivityForm(param);
        }


        // if ($(this).hasClass('other-user')) {
        //     _calendar.ReadOnlyActivityForm(param);
        // }
        // else {
        //     if ($(this).hasClass('pending')) {
        //         param.pending = true;
        //         _calendar.ReadOnlyActivityForm(param);
        //     }
        //     else {
        //         param.readOnly = true;
        //         if ($(this).hasClass('actOwner') && !($(this).hasClass('recurred-activity'))) {
        //             param.readOnly = false;
        //         }
        //         new EditActivityForm(param);
        //     }
        // }
    });

}
_calendar.BuildCalendarGrid = function (workingDayObj, timezone) {
    let grid = '';
    var sttime = moment.tz('1971-01-01 ' + workingDayObj.StartTime, "YYYY-MM-DD HH:mm", timezone)
    var entime = moment.tz('1971-01-01 ' + workingDayObj.Endtime, "YYYY-MM-DD HH:mm", timezone)
    let stm = moment(sttime).tz(_initData.User.MySettings.Timezone).format("HH:mm")
    let etm = moment(entime).tz(_initData.User.MySettings.Timezone).format("HH:mm")
    var workingHrsStart = parseInt(stm.toString().replace(':', ''));
    var workingHrsEnd = parseInt(etm.toString().replace(':', ''));
    var calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
    grid = '<div style="position:relative" class="day-grid">';
    for (var i = 0; i < 24; i++) {
        var t = i;
        var tt = '';
        var uClass = '';
        var lClass = '';
        var uTime = i * 100;
        var lTime = uTime + 30;
        if (uTime < workingHrsStart || uTime >= workingHrsEnd) {
            uClass = " bg-gray-100";
        }
        if (lTime < workingHrsStart || lTime >= workingHrsEnd) {
            lClass = " bg-gray-100";
        }
        if (calendarTimeFormat == 'hh:mm A') {
            if (i > 12) {
                t = i - 12;
                tt = ' PM';
            } else {
                tt = ' AM';
                if (i == 12) {
                    tt = ' PM';
                }
            }
        }
        var gtime = '' + t;
        if (t < 10) {
            gtime = '0' + t;
        }

        grid += '<div class="hr u' + uClass + '" data-time="' + gtime + ':00' + tt + '"></div><div class="hr l' + lClass + '" data-time="' + gtime + ':30' + tt + '"></div>';

    }
    grid += '</div>';

    return grid;
}

_calendar.GetUserFilterDetails = function (onFetch) {
    $.ajax({
        url: '/api/activity/getuserfilterdetails',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            onFetch(res);
        },
    })
}

_calendar.GetUserDetails = function (userid) {
    for (let i = 0; i < _calendar.userFilterDetails.length; i++) {
        let user = _calendar.userFilterDetails[i];
        if (user._id == userid) {
            return user;
        }
    }
}
//Check user Participants Availability 
_calendar.CheckUserParticipantsAvailability = function (options) {

    $.ajax({
        url: 'api/activity/checkuseravailability',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(options.userData),
        success: function (res) {
            let busyUserIds = [];
            if (res.usersSetting) {
                let searchUsers = [];
                res.usersSetting.forEach(
                    u => {
                        if (u.MySettings) {//To filter out users without onboarding complete
                            searchUsers.push({
                                _id: u._id, WorkingDays: u.MySettings.WorkingDays, Timezone: u.MySettings.Timezone, FirstName: u.FirstName, LastName: u.LastName, MySettings: u.MySettings
                            })
                        }
                    }
                )
                busyUserIds = _calendar.CheckWorkingHoursConflict({
                    ActivityStartTime: options.userData.StartDateTime,
                    ActivityEndTime: options.userData.EndDateTime,
                    userSettings: searchUsers
                })
            }
            if (res.hasConflict || busyUserIds.length > 0) {
                options.responseData = res;
                _calendar.UserParticipantsTimingModal(options, busyUserIds);
            }
            else {
                if (options.onComplete) {
                    options.onComplete({ 'Continue': true });
                }
            }

        },
        error: function (err) {
            console.log(err.message);
            //callback(err);
        }
    })
}

_calendar.CheckWorkingHoursConflict = function ({ ActivityStartTime, ActivityEndTime, userSettings }) {
    let busyUserId = [];
    ActivityStartTime = moment(ActivityStartTime)
    ActivityEndTime = moment(ActivityEndTime)
    userSettings.forEach(userTZ => {
        let dateOfworking = userTZ.WorkingDays.find(wrkDay => wrkDay.WorkingDay == ActivityStartTime.weekday())
        if (!dateOfworking) {
            busyUserId.push(userTZ)
        } else {
            let workingStartTime = parseInt(dateOfworking.StartTime.split(":")[0]) + parseInt(dateOfworking.StartTime.split(":")[1]) / 60;
            let workingEndTime = parseInt(dateOfworking.Endtime.split(":")[0]) + parseInt(dateOfworking.Endtime.split(":")[1]) / 60;
            let activityStartTime = parseInt(ActivityStartTime.tz(userTZ.Timezone).hours()) + parseInt(ActivityStartTime.tz(userTZ.Timezone).minutes()) / 60
            let activityEndTime = parseInt(ActivityEndTime.tz(userTZ.Timezone).hours()) + parseInt(ActivityEndTime.tz(userTZ.Timezone).minutes()) / 60

            if (activityStartTime < workingStartTime || activityEndTime > workingEndTime) {
                busyUserId.push(userTZ)
            }
        }
    })
    return busyUserId
}

_calendar.UserParticipantsTimingModal = function (options, busyUsers) {
    let self = this;

    let userFormModal = $('<div class="modal fade user-page-form" data-backdrop="static" tabindex="-2" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +

        '<div class="modal-header">' +
        '<h5 class="modal-title">Participants Timings</h5>' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +

        '<div class="modal-body user-modal-body"><div class="messageRow"></div><table class="table userTimingTable"><th>Users </th><th>Free timings</th></table></div>' +

        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-light btnCancel" data-dismiss="modal">Cancel</button>' +
        '<button type="button" class="btn btn-outline-primary btnContinue">Continue with same timings</button>' +
        '<button type="button" class="btn btn-primary btnChange">Change the timings</button>' +

        '</div>' +

        '</div>' +
        '</div>' +
        '</div>');
    userFormModal.modal('show');
    _common.Loading(false);

    let overlapActivities = [];
    let allActivities = [];
    let busyUsersDetails = []
    if (options.responseData) {
        overlapActivities = options.responseData.overlapActivities || [];
        allActivities = options.responseData.busyUsersAllActivities || [];
        busyUsersDetails = options.responseData.busyUsersDetails || [];
    }

    if (busyUsers) {
        busyUsers.forEach(
            usr => {
                if (!busyUsersDetails.find(u => u._id == usr._id)) {
                    busyUsersDetails.push(usr)
                }
            }
        )
    }
    this.GetUserWorkingTime = function (startTime, workingHours, Timezone) {
        let workingDayNum = moment(startTime).weekday();
        let workingDayObj = workingHours.find(wor => wor.WorkingDay == workingDayNum);
        let startDate = moment(startTime).format("YYYY-MM-DD hh:mm A");
        if (workingDayObj != undefined) {
            let newStartTime = moment.tz(startDate, "YYYY-MM-DD hh:mm A", Timezone);
            let newEndTime = moment.tz(startDate, "YYYY-MM-DD hh:mm A", Timezone);
            newStartTime.hours(workingDayObj.StartTime.split(":")[0]).minutes(workingDayObj.StartTime.split(":")[1])
            newEndTime.hours(workingDayObj.Endtime.split(":")[0]).minutes(workingDayObj.Endtime.split(":")[1])

            return { workingStartDate: newStartTime, workingEndDate: newEndTime };
        }
        else {
            return null;
        }
    }

    this.GetFreeTime = function (userActivities, workingDays, Timezone) {
        let freeSchedules = [];
        let workingTimeObj = this.GetUserWorkingTime(options.userData.StartDate, workingDays, Timezone)
        if (workingTimeObj == null)
            return freeSchedules;
        let startDay = moment(workingTimeObj.workingStartDate).valueOf();
        let freePoint = startDay;
        let endDay = moment(workingTimeObj.workingEndDate).valueOf();
        for (let i = 0; i < userActivities.length; i++) {
            let fs = {};
            let cActivity = userActivities[i];
            let cActivityStart = moment(cActivity.StartDate).valueOf();
            let cActivityEnd = moment(cActivity.EndDate).valueOf();
            if (cActivityStart <= freePoint) {
                if (cActivityEnd < freePoint) {
                    continue;
                }
                else {
                    freePoint = cActivityEnd;
                }
                if (cActivityEnd > endDay) {
                    return freeSchedules;
                }
            }
            if (cActivityStart > freePoint) {
                fs.StartTime = moment(freePoint).tz(_initData.User.MySettings.Timezone).format(_initData.Company.Setting.CalendarTimeFormat);
                fs.EndTime = moment(cActivityStart).tz(_initData.User.MySettings.Timezone).format(_initData.Company.Setting.CalendarTimeFormat);
                freeSchedules.push(fs);
                freePoint = cActivityEnd + 1
            }
        }
        if (freePoint < endDay) {
            let fs = {};
            fs.StartTime = moment(freePoint).tz(_initData.User.MySettings.Timezone).format(_initData.Company.Setting.CalendarTimeFormat);
            fs.EndTime = moment(endDay).tz(_initData.User.MySettings.Timezone).format(_initData.Company.Setting.CalendarTimeFormat);
            freeSchedules.push(fs);
        }
        return freeSchedules;
    }

    this.BuildTableRows = function (user, freeTime) {
        let cHtm = $('<tr class="userTimingRow"><td class="userDetail">' + user.FirstName + ' ' + user.LastName + '</td><td class="userTiming"></td></tr>');
        if (freeTime.length == 0) {
            cHtm.find('.userTiming').html('No free slots available ');
        }
        else {
            for (let i = 0; i < freeTime.length; i++) {
                const time = freeTime[i];
                if (time.StartTime == time.EndTime && time.StartTime == '12:00 AM') {
                    cHtm.find('.userTiming').html('No free slots available ');
                    break;
                }
                cHtm.find('.userTiming').append(time.StartTime + ' To ' + time.EndTime + ' || ')
            }
        }
        userFormModal.find('.userTimingTable').append(cHtm);
    }

    this.BuildErrorMessage = function () {
        let cHtm = $('<p> User(s) <span class="userDetail"></span> is Busy in other activities during this time .</p><p>Suggested Timings are : </p>');
        for (let i = 0; i < busyUsersDetails.length; i++) {
            const user = busyUsersDetails[i];
            cHtm.find('.userDetail').append(user.FirstName + ' ' + user.LastName + ' ');
        }
        userFormModal.find('.messageRow').html(cHtm);
    }
    self.BuildErrorMessage();

    for (let j = 0; j < busyUsersDetails.length; j++) {
        const user = busyUsersDetails[j];
        let userActivities = [];
        for (let i = 0; i < allActivities.length; i++) {
            const activity = allActivities[i];
            if (options.userData.ActivityID != undefined) {
                if (activity._id == options.userData.ActivityID)
                    continue;
            }
            let isExist = _calendar.CheckUserPresense(activity.AttendeesData, user._id);
            if (isExist) {
                userActivities.push(activity);
            }
        }
        _calendar.SortActivitiesByStartDate(userActivities);
        let freeTime = self.GetFreeTime(userActivities, user.MySettings.WorkingDays, user.MySettings.Timezone);
        self.BuildTableRows(user, freeTime);
    }


    userFormModal.find('.btnContinue').click(function () {
        _common.Loading(true);
        userFormModal.modal('hide');
        options.onComplete({ 'Continue': true });
    });

    userFormModal.find('.btnChange').click(function () {
        userFormModal.modal('hide');
        options.onComplete({ 'Continue': false });
    });

    userFormModal.find('.btnCancel ,.close').click(function () {
        userFormModal.modal('hide');
        options.onComplete({ 'Continue': false, 'Close': true });
    });

}

_calendar.CheckUserPresense = function (attendeeData, userid) {
    // if user if present in array return true
    for (let i = 0; i < attendeeData.length; i++) {
        if (attendeeData[i].Id == userid && (attendeeData[i].Status == "accepted" || attendeeData[i].Status == "pending")) {
            return true;
        }
    }
    return false;
}

_calendar.GetAttendeeDetails = function (attendeeData, userId) {
    return attendeeData.find(attendee => attendee.Id == userId);
}

_calendar.CheckUserInUserData = function (activity) {
    if (activity.AttendeesData != undefined) {
        for (let j = 0; j < activity.AttendeesData.length; j++) {
            const userObj = activity.AttendeesData[j];
            if (userObj.Id == _initData.User._id) {
                if (userObj.Status == 'pending' || userObj.Status == 'accepted')
                    return true;
                else
                    return false;
            }
        }
    } else
        return false;
}

//NOTE change User Statusd popup
_calendar.ChangeStatusModel = function (options) {
    console.log(options)
    let self = this;
    let status = null;
    let attendeeData = options.AttendeeData;
    let accountHtml = '';

    if (attendeeData.Type == 'user') {
        nameHtml = attendeeData.Name;
    }
    else {
        nameHtml = attendeeData.Name;
        if (attendeeData.AccountName && attendeeData.AccountName != "undefined")
            accountHtml = '<p class="small mb-0 mt-1 text-secondary d-inline-block">' + attendeeData.AccountName + '</p>';
    }

    let statusBody = `<div class="mb-4">
                <h5 class="mb-0 " data-id="${attendeeData.Id}">${attendeeData.Name}</h5>
                <p class="small mb-0 mt-1 text-secondary d-inline-block">${attendeeData.Email}</p>
                <p class="small mb-0 mt-1 text-secondary d-inline-block pl-2 pr-2">|</p>${accountHtml}
                </div>`;

    let changeStatusFormModal = _common.DynamicPopUp({ title: 'Change User Status', body: statusBody });
    changeStatusFormModal.modal('show');

    let pendId = "pend" + (new Date()).getMilliseconds();    
    let acceptId = "accept" + (new Date()).getMilliseconds();
    let rejectId = "reject" + (new Date()).getMilliseconds();

    let radioBtn1 = $(` <div class="custom-control custom-radio custom-control-block mb-2">
                    <input type="radio" value="accepted" id="${acceptId}" name="status" class="custom-control-input status-accepted">
                    <label class="custom-control-label" for="${acceptId}">Accepted</label>
                </div>`);

    let radioBtn2 = $(` <div class="custom-control custom-radio custom-control-block">
                    <input type="radio" value="rejected" id="${rejectId}" name="status" class="custom-control-input status-rejected">
                    <label class="custom-control-label" for="${rejectId}">Rejected</label>
                </div>`);

    let radioBtn3 = $(` <div class="custom-control custom-radio custom-control-block mb-2">
                    <input type="radio" value="pending" id="${pendId}" name="status" class="custom-control-input status-pending">
                    <label class="custom-control-label" for="${pendId}">Pending</label>
                </div>`);

    changeStatusFormModal.find('.modal-body').append(radioBtn3);            
    changeStatusFormModal.find('.modal-body').append(radioBtn1);
    changeStatusFormModal.find('.modal-body').append(radioBtn2);
    if (options.AttendeeData.Status == 'accepted') {
        changeStatusFormModal.find('.status-accepted').prop('checked', true);
        status = 'accepted'
    }else if (options.AttendeeData.Status == 'rejected') {
        changeStatusFormModal.find('.status-rejected').prop('checked', true);
        status = 'rejected'
    } else {
        changeStatusFormModal.find('.status-pending').prop('checked', true);
        status = 'pending'
    }

    changeStatusFormModal.find('input[type=radio]').click(function () {
        // console.log($(this).val(),options.AttendeeData.Id,options.ActivityID)
        status = $(this).val()
    })
    changeStatusFormModal.find('.btnSave').click(function () {
        let data = {
            status: status,
            attendeeId: options.AttendeeData.Id,
            activityId: options.ActivityID,
            type: options.AttendeeData.Type,
            privateFields: _calendar.getPrivateFieldsFromPageData(self.PageData)
        }
        _calendar.changeAttendeeStatusByCreater(data, (response) => {
            if (response.message == 'success') {
                changeStatusFormModal.modal('hide');
                options.ParentModel.modal('hide');
                swal('Status has been updated.', { icon: "success", });
            }
        })
    })
}

/*****Activity Reminder ****/

_calendar.getAddedReminderArray = function (container) {
    let reminderArr = [];
    let uniqueReminderArr = [];
    if (container) {
        let reminders = container.find(".reminderRow");
        if (reminders.length > 0) {
            reminders.each(
                function (e) {
                    reminderArr.push({
                        type: $(this).find("[data-field='ReminderType']").val(),
                        number: $(this).find("[data-field='ReminderNumber']").val(),
                        parameter: $(this).find("[data-field='ReminderParameter']").val(),
                        id: _controls.ToEJSON('objectid', $(this).attr('data-id') ? $(this).attr('data-id') : '')
                    })
                }
            )

            reminderArr.forEach(
                (rem) => {
                    let unique = true
                    uniqueReminderArr.some(
                        uRem => {
                            if (uRem.type == rem.type && uRem.number == rem.number && uRem.parameter == rem.parameter) {
                                unique = false;
                            }
                        }
                    )
                    if (unique) {
                        uniqueReminderArr.push(rem)
                    }
                    return unique
                }
            )
        }
    }
    return { reminderArr, uniqueReminderArr }

}


//NOTE Build Empty Reminder Row 
//1. If 
_calendar.BuildReminderRow = function (container) {
    let uniqueReminderArr = _calendar.getAddedReminderArray(container).uniqueReminderArr;
    let num = 15
    uniqueReminderArr.forEach(
        rem => {
            if (rem.type == "notification" && rem.parameter == "m") {
                num = parseInt(rem.number) + 5
            }
        }
    )
    let ctrlReminderRow = $(`<div class=" reminderRow bg-light d-flex pt-2 rounded">
                                <div class="form-group col-sm-2 vld-parent ">
                                    <select title="Type" data-field="ReminderType" data-ui="dropdown" class="msp-ctrl form-control form-control-sm msp-ctrl-dropdown">
                                        <option value="notification" >Notification</option>
                                        <option value="email" >Email</option>
                                        <option value="both" >Both</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-2 sub-parent">
                                    <input type="text" title="Reminder Time" data-field="ReminderNumber" data-ui="int" class="msp-ctrl form-control form-control-sm msp-ctrl-int" value=${num}>
                                </div>
                                <div class="form-group col-sm-3 sub-parent">
                                    <select title="Parameter" data-field="ReminderParameter" data-ui="dropdown" class="msp-ctrl form-control form-control-sm msp-ctrl-dropdown">
                                        <option value="m" selected="">Minutes</option>
                                        <option value="h">Hours</option>
                                        <option value="d">Days</option>
                                        <option value="w">Weeks</option>
                                    </select></div>
                                <div class="form-group col-sm-3 sub-parent"><button type="button"
                                    class="btn btn-sm btnDelReminderRow text-danger" data-toggle="tooltip" data-placement="top" title="Close"><i class="fas fa-times"></i></button>
                                </div>
                            </div>`);
    return ctrlReminderRow;
}

//Build Reminder Row With Value
_calendar.BuildReminderRowWithValue = function (data) {
    let ctrlReminderRow = $(`<div class=" reminderRow bg-light d-flex pt-2">
                                <div class="form-group col-sm-2 vld-parent ">
                                    <select title="Type" data-field="ReminderType" data-ui="dropdown" class="msp-ctrl form-control form-control-sm msp-ctrl-dropdown">
                                        <option value="notification" selected="">Notification</option>
                                        <option value="email" selected="">Email</option>
                                        <option value="both" selected="">Both</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-2 sub-parent">
                                    <input type="text" title="Reminder Time" data-field="ReminderNumber" data-ui="int" class="msp-ctrl form-control form-control-sm msp-ctrl-int" value="15">
                                </div>
                                <div class="form-group col-sm-3 sub-parent">
                                    <select title="Parameter" data-field="ReminderParameter" data-ui="dropdown" class="msp-ctrl form-control form-control-sm msp-ctrl-dropdown">
                                        <option value="m" selected="">Minutes</option>
                                        <option value="h">Hours</option>
                                        <option value="d">Days</option>
                                        <option value="w">Weeks</option>
                                    </select></div>
                                <div class="form-group col-sm-3 sub-parent"><button type="button"
                                    class="btn btn-sm btnDelReminderRow text-danger" data-toggle="tooltip" data-placement="top" title="Close"><i class="fas fa-times"></i></button>
                                </div>
                            </div>`);

    if (data != undefined || data != "") {
        if (data.Mail && !data.Notification) {
            ctrlReminderRow.find('[data-field = "ReminderType"]').val('email');
        }
        else if (!data.Mail && data.Notification) {
            ctrlReminderRow.find('[data-field = "ReminderType"]').val('notification');
        }
        else if (data.Mail && data.Notification) {
            ctrlReminderRow.find('[data-field = "ReminderType"]').val('both');
        }

        if (data.Number)
            ctrlReminderRow.find('[data-field = "ReminderNumber"]').val(data.Number)
        if (data.Parameter)
            ctrlReminderRow.find('[data-field = "ReminderParameter"]').val(data.Parameter);
        if (data._id)
            ctrlReminderRow.attr('data-id', data._id);
    }
    return ctrlReminderRow;
}

//Delete Already Added Reminder
_calendar.RemoveActivityReminder = function (reminderID, onDelete) {
    $.ajax({
        url: 'api/activity/removeactivityreminder',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ 'reminderid': reminderID }),
        success: function (res) {
            onDelete(res);
        },
        error: function (err) {
            console.log(err.message);
        }
    })
}

//Add Activity Reminder
//FIXME not used 
_calendar.AddActivityReminder = function (reminder, onAdd) {
    $.ajax({
        url: 'api/activity/addactivityreminder',
        type: 'post',
        data: JSON.stringify({ 'reminderarr': reminder }),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            onAdd(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

//Get Activity Reminder
_calendar.GetSingleActivityReminder = function (activityid, onFetch) {
    $.ajax({
        url: 'api/activity/getsingleactivityreminder',
        type: 'post',
        data: JSON.stringify({ 'activityid': activityid }),
        contentType: 'application/json',
        success: function (result) {
            onFetch(result);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

/*** Reminder End  */

/**** Reccurrence Start */

_calendar.DeleteRecurrenceActivityModal = function (data) {
    let dn = Date.now();
    let delFormModal = $(`
    <div class="modal fade delete-page-form" data-backdrop="static" tabindex="-2" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                     <h5 class="modal-title">Delete Recurrence Activity</h5>
                 </div
                 <div class="mb-10">
                    <div class="modal-body">
                        <div class="mb-2">Do you want to delete</div>

                        <div class="custom-control custom-radio custom-control-block mb-2">
                            <input type="radio" value="this_activity" id="1${dn}" name="deleteactivity" class="custom-control-input status-accepted">
                            <label class="custom-control-label" for="1${dn}">This occurance </label>
                        </div>
                        <div class="custom-control custom-radio custom-control-block mb-2">
                            <input type="radio" value="all_activity" id="2${dn}" name="deleteactivity" class="custom-control-input status-accepted">
                            <label class="custom-control-label" for="2${dn}">All occurances </label>
                        </div>
                        <div class="custom-control custom-radio custom-control-block mb-2">
                            <input type="radio" value="future_activity" id="3${dn}" name="deleteactivity" class="custom-control-input status-accepted">
                            <label class="custom-control-label" for="3${dn}">This and following occurances </label>
                        </div>
                    </div>
                <div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light canceldelete" >Cancel</button>
                        <button type="button" class="btn btn-primary continuedelete">Continue</button>
                    </div>
                 </div>
                 <div>
                 </div>
            </div>
        </div>
    </div>
    `);
    delFormModal.modal('show');
    delFormModal.find('input[name="deleteactivity"]')[0].checked = true;
    delFormModal.find('.canceldelete').click(() => { delFormModal.modal('hide') })
    delFormModal.find('.continuedelete').click(() => {
        let radioButtonRes = delFormModal.find('input[name="deleteactivity"]:checked');
        switch (radioButtonRes.val()) {
            case 'all_activity':
                _calendar.DeleteRecurrencesActivities({ activityId: data.activityid, type: radioButtonRes.val() }, function (result) {

                    gotResponse(result, 'Activities deleted successfully')
                });
                break;

            case 'this_activity':
                _calendar.DeleteSingleActivity(data.activityid, function (result) {
                    gotResponse(result, 'Activity deleted successfully')
                })
                break;

            case 'future_activity':
                _calendar.DeleteRecurrencesActivities({ activityId: data.activityid, type: radioButtonRes.val() }, function (result) {
                    gotResponse(result, 'Activities deleted successfully')
                })

                break;
        }
    })

    function gotResponse(response, msg) {
        if (response.message == 'success') {
            swal(msg, { icon: "success" })
        } else {
            swal('Something went wrong', { icon: "error" })
        }
        if (data.cal)
            data.cal.ResetActivities();
        delFormModal.modal('hide');
        if (data.ParentModel)
            data.ParentModel.modal('hide');
    }
    // delFormModal.find('.btnDelOneRec').click(function () {
    //     _common.ConfirmDelete({ message: 'You want to delete this activity ?' }).then((confirm) => {
    //         if (confirm) {
    //             _calendar.DeleteSingleActivity(data.activityid, data.ownerid, function (result) {
    //                 if (result.message == "success") {
    //                     delFormModal.modal('hide');
    //                     swal('Record deleted successfully.', { icon: "success", });
    //                     if (data.cal != undefined) {
    //                         data.cal.ResetActivities();
    //                     }
    //                     _calendar.activityDeleted({ activityId: data.activityid, owner: data.ownerid });
    //                 }
    //                 else
    //                     swal(result.message, { icon: "error", });
    //             })
    //         }
    //     });
    // });

    // delFormModal.find('.btnDelAllRec').click(function () {
    //     _common.ConfirmDelete({ message: 'You want to delete all the occurrences of this activity ?' }).then((confirm) => {
    //         if (confirm) {
    //             _calendar.DeleteAllRecurrences(data.parent, data.ownerid, function (result) {
    //                 if (result.message == "success") {
    //                     delFormModal.modal('hide');
    //                     _calendar.activityDeleted({ activityId: data.parent, owner: data.ownerid });
    //                     swal('Records deleted successfully.', { icon: "success", });
    //                     if (data.cal != undefined) {
    //                         data.cal.ResetActivities();
    //                     }
    //                 }
    //                 else
    //                     swal(result.message, { icon: "error", });
    //             })
    //         }
    //     });
    // });
}

_calendar.DeleteRecurrencesActivities = function (data, callback) {
    $.ajax({
        url: 'api/activity/deleteRecurrencesActivities',
        type: 'post',
        data: JSON.stringify(data),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            callback(res);
        },
    })
}
/*** Reccurrence End */

/************************ */
/***** USER PARTICIPANTS  */

_calendar.UpdateUserResponseModal = function (sparam, onSubmit) {
    if (sparam.isRecurrence) {
        let resp;
        if (sparam.response == 'accepted')
            resp = 'ACCEPT'
        else
            resp = 'REJECT'
        let recurenceRspForm = $('<div class="modal fade ' + resp + '-page-form" data-backdrop="static" tabindex="-2" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true">' +
            '<div class="modal-dialog modal-lg" role="document">' +
            '<div class="modal-content">' +

            '<div class="modal-header">' +
            '<h5 class="modal-title">' + resp + ' Recurrence Activity</h5>' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
            '</div>' +

            '<div class="modal-body">If you ' + resp + '. All the ocurrences will be ' + resp + 'ED </div>' +

            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-primary btnOneRec">' + resp + ' this Ocurrence</button>' +
            '<button type="button" class="btn btn-primary btnAllRec">' + resp + ' all the Ocurrences</button>' +
            '<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>' +

            '</div>' +

            '</div>' +
            '</div>' +
            '</div>');
        recurenceRspForm.modal('show');
        recurenceRspForm.find('.btnOneRec').click(function () {
            sparam.recurrAll = false;
            _calendar.SendUserResponse(sparam, function (data) {
                onSubmit(data);
                recurenceRspForm.modal('hide');
            });
        });

        recurenceRspForm.find('.btnAllRec').click(function () {
            sparam.recurrAll = true;
            _calendar.SendUserResponse(sparam, function (data) {
                onSubmit(data);
                recurenceRspForm.modal('hide');
            });
        });
    }
    else {
        _calendar.SendUserResponse(sparam, function (data) {
            onSubmit(data);
        });
    }
}

_calendar.SendUserResponse = function (sparam, onSubmit) {
    $.ajax({
        url: 'api/activity/updateuserresponse',
        type: 'post',
        data: JSON.stringify(sparam),
        contentType: 'application/json',
        success: function (result) {
            onSubmit(result);
            if (result.message = 'success') {
                if (!_calendar.CheckBackDate(result.data.StartDate)) {
                    let userResponseType = sparam.response == 'accepted' ? 'accept_request' : 'reject_request'
                    eventobj.SetActivityNotification(result.data._id, sparam.response);
                    eventobj.SendMailToUserParticipants(result.data._id, userResponseType);
                    eventobj.GenericPushNoitification(result.data._id, userResponseType);
                }
            }
        },
        error: function (error) {
            console.log(error);
        }
    })
}

_calendar.AddAttendeeToList = function (attendeeId, activityId, onAdd) {

    let sdata = {};
    sdata.AttendeeID = attendeeId;
    sdata.ActivityID = activityId;

    $.ajax({
        url: 'api/activity/addattendeetolist',
        type: 'post',
        data: JSON.stringify(sdata),
        contentType: 'application/json',
        success: function (result) {
            onAdd(result);
        },
        error: function (error) {
            console.log(error);
        }
    })
}


/*****USER PARTICIPANTS END */

/*******DISPLAY ACTIVITY  */
/************************ */
_calendar.SortActivitiesByStartDate = function (activities) {
    if (activities != undefined && activities.length > 0) {
        function compare(a, b) {
            const genreA = moment(a.StartDate);
            const genreB = moment(b.StartDate);

            let comparison = 0;
            if (genreA > genreB) {
                comparison = 1;
            } else if (genreA < genreB) {
                comparison = -1;
            }
            return comparison;
        }
        activities.sort(compare);
    }
}

_calendar.RemoveSameActivity = function (activities) {
    return true;
    //removes one activity when actParent activity and normal activity both are present
    //optional
    //dont do coz if teh userparticipant change the content and timing of the activity then it is actually on a different timing then the act parent activity 
}

_calendar.FilterActivitiesByEventFilter = function (activities, eventFilterArray) {
    let filteredActivities = [];
    for (let i = 0; i < activities.length; i++) {
        for (let j = 0; j < eventFilterArray.length; j++) {
            if (activities[i].ActivityType == eventFilterArray[j])
                filteredActivities.push(activities[i])
        }
    }
    return filteredActivities;
}

_calendar.CheckAllDayOrMultipleDays = function (activity) {
    let startMoment = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone);
    let endMoment = moment(activity.EndDate).tz(_initData.User.MySettings.Timezone);

    let startDate = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).hours(0).minutes(0).seconds(0).format();
    let endDate = moment(activity.EndDate).tz(_initData.User.MySettings.Timezone).hours(0).minutes(0).seconds(0).format();

    activity.ActDays = moment(endDate).diff(startDate, 'd') + 1

    if (activity.IsAllDay) {
        activity.ActDays = moment(activity.EndDate).diff(activity.StartDate, 'd') + 1;
        activity.StartDate = moment.tz(activity.StartDate, 'YYYY-MM-DD hh:mm', _initData.User.MySettings.Timezone).format()
        activity.EndDate = moment.tz(activity.EndDate, 'YYYY-MM-DD hh:mm', _initData.User.MySettings.Timezone).format()
    }
    if ((startDate != endDate && !(endMoment.format("HH:mm") == "00:00")) || endMoment.diff(startMoment, 'days') > 0 || activity.IsAllDay == true)
        return true;
    else
        return false;

}

_calendar.CalculateHeight = function (activity) {
    let height = 0;
    let duration = moment.duration(moment(activity.EndDate).diff(moment(activity.StartDate))).asHours();
    height = duration * 40;
    return height;
}

_calendar.CalculateTimeFromHours = function (durInHours) {
    let hours = Math.floor(durInHours);
    let mins = (durInHours % 1) * 60;
    mins = mins.toFixed(2);
    let time = hours + ':' + mins;
    return time
}

_calendar.CalculateActivityDurationInHours = function (activity) {
    return moment.duration(moment(activity.EndDate).diff(moment(activity.StartDate))).asHours();
}
_calendar.CalculateTop = function (activity) {
    let top = 0;
    let activityStartTime = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone);
    let diff = activityStartTime.hours() + activityStartTime.minutes() / 60;
    top = diff * 40;
    return top;
}

_calendar.ConfirmMove = function (param) {

    if (param === undefined) {
        param = {};
        param.title = 'Are you sure?';
        param.message = 'Are you sure you want to reschedule the activity ?';
    }

    return swal({
        title: param.title,
        text: param.message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
}

_calendar.ChangeUserActivityStatusType = function (activityid, statustype, callback) {
    $.ajax({
        url: 'api/activity/changeuseractivitystatustype',
        type: 'post',
        data: JSON.stringify({ 'activityid': activityid, 'statustype': statustype }),
        contentType: 'application/json',
        success: function (result) {
            callback(result);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

_calendar.ChangeUserActivityLock = function (activityId, action, callback) {
    $.ajax({
        url: 'api/activity/changeuseractivitylock',
        type: 'post',
        data: JSON.stringify({ 'activityid': activityId, 'lock': action }),
        contentType: 'application/json',
        success: function (result) {
            callback(result);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

/******ACTIVITY CRUD */
/******************* */
_calendar.GetActivityDataByDate = function (calendarObj, onFetch) {
    let sdata = {
        'startdate': calendarObj.startDate, 'enddate': calendarObj.endDate,
        "alldayStartDate": moment.utc(moment.tz(calendarObj.startDate, _initData.User.MySettings.Timezone).format("YYYY-MM-DD"), "YYYY-MM-DD").format(),
        "alldayEndDate": moment.utc(moment.tz(calendarObj.endDate, _initData.User.MySettings.Timezone).format("YYYY-MM-DD"), "YYYY-MM-DD").subtract(1, 's').format()
    };
    sdata.userarray = calendarObj.userArray;

    _common.Loading(true)
    $.ajax({
        url: 'api/activity/getallactivitydatabydateanduser',
        data: JSON.stringify(sdata),
        type: 'post',
        contentType: 'application/json',
        success: function (activities) {
            onFetch(activities);
            _common.Loading(false)

        },
        error: function (error) {
            console.log(error);
            _common.Loading(false)

        }
    })

}

_calendar.GetActivityByMonthYear = function (container) {
    let month = container[0].currentMonth;
    let year = container[0].currentYear;
    let monthDays = _calendar.DaysInMonth(month, year);
    let fromDate = new Date(year, month, 1);
    fromDate = moment.tz(moment(fromDate).format('YYYY-MM-DD HH:mm A'), 'YYYY-MM-DD HH:mm A', _initData.User.MySettings.Timezone);
    let toDate = new Date(year, month, (1 + monthDays));
    toDate = moment.tz(moment(toDate).format('YYYY-MM-DD HH:mm A'), 'YYYY-MM-DD HH:mm A', _initData.User.MySettings.Timezone);
    let sdata = {
        'startdate': fromDate, 'enddate': toDate,
        "alldayStartDate": moment.utc(moment.tz(fromDate, _initData.User.MySettings.Timezone).format("YYYY-MM-DD"), "YYYY-MM-DD").format(),
        "alldayEndDate": moment.utc(moment.tz(toDate, _initData.User.MySettings.Timezone).format("YYYY-MM-DD"), "YYYY-MM-DD").subtract(1, 's').format()
    };

    _common.FetchApiByParam({ url: 'api/activity/getactivitybymonthyear', data: sdata }, function (data) {
        container.find('.item-exists').removeClass('item-exists');
        for (let i = 0; i < data.length; i++) {

            activityDate = null;
            let activityStartDate = moment(data[i].StartDate).tz(_initData.User.MySettings.Timezone);
            let activityEndDate = moment(data[i].EndDate).tz(_initData.User.MySettings.Timezone);

            if (data[i].IsAllDay) {
                activityStartDate = moment(data[i].StartDate).tz(data[i].Timezone);
                activityEndDate = moment(data[i].EndDate).tz(data[i].Timezone);
            }
            let diffinDays = activityEndDate.diff(activityStartDate, "d")
            if (diffinDays > 0) {
                for (let k = 0; k <= diffinDays; k++) {
                    activityDate = activityStartDate.format('YYYY-MM-DD');
                    container.find('.active-days[data-date="' + activityDate + '"]').addClass('item-exists');
                    activityStartDate.add(1, "d");
                }

            }
            else {
                activityDate = activityStartDate.format('YYYY-MM-DD');
                container.find('.active-days[data-date="' + activityDate + '"]').addClass('item-exists');
            }



        }
    })
}

_calendar.AddActivityData = function (sdata, callback) {
    $.ajax({
        url: 'api/activity/addactivityobject',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            console.log(err.message);
        }
    })
}

_calendar.GetActivityViewPermissions = function (params, callback) {
    $.ajax({
        url: '/api/activity/getActivityViewPermissions',
        type: "post",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(params),
        success: function (res) { callback(res) },
        error: function (res) { callback(res) }
    })
}

_calendar.ReadActivityData = function (param) {
    let sdata = { '_id': param._id };
    if (param.afterDelete) {
        sdata.afterDelete = true;
    }
    $.ajax({
        url: '/api/activity/readactivityobject',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            param.callback(res);
        },
    });
}

_calendar.UpdateActivityData = function (sdata, callback) {
    $.ajax({
        url: 'api/activity/updateactivityobject',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(sdata),
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            console.log(err.message);
        }
    })
}

_calendar.DeleteSingleActivity = function (objectid, callback) {
    $.ajax({
        url: 'api/activity/deletesingleactivity',
        type: 'post',
        data: JSON.stringify({ 'objectid': objectid }),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            // eventobj.SetActivityNotification(objectid, 'rejected')
            callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}


//NOTE Delete Attendee from activity
_calendar.deleteAttendeeFromActivity = function (attendeeObj, callback) {
    $.ajax({
        url: 'api/activity/deletesingleattendee',
        type: 'post',
        data: JSON.stringify(attendeeObj),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

//NOTE Reinvite attendee in activity
_calendar.ReinviteAttendee = function (attendeeObj, callback) {
    $.ajax({
        url: 'api/activity/reinviteattendee',
        type: 'post',
        data: JSON.stringify(attendeeObj),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}


//NOTE Delete push and mail notification
_calendar.GenericMailAndPushNotification = function (configObj, callback) {
    $.ajax({
        url: 'api/activity/genericmailandpushnotification',
        type: 'post',
        data: JSON.stringify(configObj),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

// _calendar.GenericMailAndPushNotification({
//     activityId: "5db129f0a51f956a349e5063",
//     type: "delete",
//     notification: false,
//     mail: true,
//     attendeeIds: ["5d661c59ae84bc195c41fa74"],
//     all:true
// }, (data) => console.log(data))

//NOTE Change User status ->By creater
_calendar.changeAttendeeStatusByCreater = function (Obj, callback) {
    $.ajax({
        url: 'api/activity/changeuserstatus',
        type: 'post',
        data: JSON.stringify(Obj),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

//NOTE Get activity creater and current user of activity
_calendar.GetActivityCreaterAndCurrentUser = function (activityId, cb) {
    _calendar.ReadActivityData({
        _id: activityId, callback: (data) => {
            let creater = {};
            let loggedinUser = null;
            let otherUsers = [];
            data.AttendeesData.forEach(
                att => {
                    if (att.IsOrganiser) {
                        creater = att;
                    } else {
                        if (att.Id == _initData.User._id) {
                            loggedinUser = att
                        } else if (att.Status == 'accepted' || att.Status == 'pending') {
                            otherUsers.push(att)
                        }
                    }
                }
            )
            if (!loggedinUser) {
                loggedinUser = creater;
            }
            let activityData = data.data;
            activityData.AttendeesData = data.AttendeesData

            cb({ creater, loggedinUser, otherUsers, activityData: activityData[0] })
        }, afterDelete: true
    })
}


/****** *************/
/******EVENTS ***** */
/****** *************/

//Event : Meeting,Call
_calendar.CustomFormValidatation = function (data) {
    let datafields = {};
    let startDateCtrl = data.PageContainer.find('[data-field="StartDate"]');
    let endDateCtrl = data.PageContainer.find('[data-field="EndDate"]');
    //let allDayCtrl = data.PageContainer.find('[data-field="IsAllDay"]');

    if (data.PageData.Fields != undefined) {
        for (let i = 0; i < data.PageData.Fields.length; i++) {
            let field = data.PageData.Fields[i];
            if (field.Schema == undefined)
                continue;

            let val = _controls.GetCtrlValue({ field: field, container: data.PageContainer });
            datafields[field.Name] = val;
        }
    }


    if (datafields["StartDate"] >= datafields["EndDate"]) {
        _common.ValidateAddMsg(endDateCtrl, endDateCtrl.attr('title') + " should be greater than " + startDateCtrl.attr('title'));
        return false;
    }

    /** Outside working hours start */

    let workingStartTime = moment(datafields["StartDate"]).tz(_initData.User.MySettings.Timezone);
    let workingEndTime = moment(datafields["EndDate"]).tz(_initData.User.MySettings.Timezone);
    let currDate = moment(new Date());
    // if (moment(datafields["EndDate"]) > workingEndTime || weekends.indexOf(moment(datafields["EndDate"]).day().toString()) > -1) {
    //     _common.ValidateAddMsg(endDateCtrl, endDateCtrl.attr('title') + " is outside working hours ");
    //     return false;
    // }
    // if (moment(datafields["StartDate"]) < workingStartTime || weekends.indexOf(moment(datafields["StartDate"]).day().toString()) > -1) {
    //     _common.ValidateAddMsg(startDateCtrl, startDateCtrl.attr('title') + " is outside working hours ");
    //     return false;
    // }

    /**outside working hours end */

    if (!_initData.Company.Setting.IsCreateBackDateActivity && data.ParamData._id == undefined) {
        if (moment(datafields["StartDate"]) < currDate) {
            _common.ValidateAddMsg(startDateCtrl, " Back dated scheduling is prohibited by the company");
            return false;
        }
    }



    // if (data.PageContainer.find('.reminderRow').length > 0) {
    //     if (data.PageContainer.find('[data-field="ReminderNumber"]').val() == "" || data.PageContainer.find('[data-field="ReminderParameter"]').val() == "" || !(data.PageContainer.find('[data-field="ReminderMail"]').is(':checked') || data.PageContainer.find('[data-field="ReminderNotification"]').is(':checked'))) {
    //         alert('Enter Reminder Details');
    //         return false;
    //     }
    // }

    let isInternalCtrl = data.PageContainer.find('[data-field="IsInternal"]');
    if (isInternalCtrl.is(':checked')) {
        if (datafields['UserParticipants'] == '') {
            _common.ValidateAddMsg(data.PageContainer.find('[data-field="UserParticipants"]'), " No Participants added");
            return false;
        }
    }
    else {
        if (datafields['Accounts'] == '') {
            _common.ValidateAddMsg(data.PageContainer.find('[data-field="Accounts"]'), " No Accounts added");
            return false;
        }
        else if (datafields['Contacts'] == '') {
            _common.ValidateAddMsg(data.PageContainer.find('[data-field="Contacts"]'), " No Contacts added");
            return false;
        }
    }

}

//Event : Meeting,Call
_calendar.InitializeActivityForm = function (data) {
    // let recurrenceCtrl = data.PageContainer.find('[data-field="Recurrence"]');
    // let recurringOccurrenceCtrl = data.PageContainer.find('[data-field="RecurringOccurrence"]');
    // let indefiniteRecurrenceCtrl = data.PageContainer.find('[data-field="IndefiniteRecurrence"]');
    let self = this;
    let startDateCtrl = data.PageContainer.find('[data-field="StartDate"]');
    let endDateCtrl = data.PageContainer.find('[data-field="EndDate"]');
    let isAllDayCtrl = data.PageContainer.find('[data-field="IsAllDay"]');

    //NOTE Get the value of dateTime fields in dialog
    self.getDateFieldValue = function () {
        let startField = { 'Name': $(startDateCtrl).data('field') };
        startField.Schema = { 'UIDataType': $(startDateCtrl).data('ui') };
        let startVal = _controls.GetCtrlValue({ field: startField, container: data.PageContainer });
        let endField = { 'Name': $(endDateCtrl).data('field') }
        endField.Schema = { 'UIDataType': $(endDateCtrl).data('ui') };
        let endVal = _controls.GetCtrlValue({ field: endField, container: data.PageContainer });
        let selectedTimeZone = data.PageContainer.find("[data-field=Timezone]")[0].value;
        return {
            endVal, startVal, selectedTimeZone
        }
    }

    //NOTE If the startTime > endTime => change endTime
    startDateCtrl.on('change', function () {
        const { endVal, startVal, selectedTimeZone } = self.getDateFieldValue()
        let startMomentVal = moment.tz(startVal, selectedTimeZone);
        let endMomentVal = moment.tz(endVal, selectedTimeZone)
        if (startMomentVal.isSameOrAfter(endVal)) {
            let newEndDate = startMomentVal.add(parseInt(_initData.Company.Setting.CalendarTimeInterval), 'minute')
            if (moment(startMomentVal.format("HH:mm"), "HH:mm").isBefore(moment(endMomentVal.format("HH:mm"), "HH:mm"))) {
                newEndDate.hours(endMomentVal.format("HH")).minutes(endMomentVal.format("mm"))
            }
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-date').val(newEndDate.tz(selectedTimeZone).format(_initData.Company.Setting.DateFormat));
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').val(newEndDate.tz(selectedTimeZone).format(_initData.Company.Setting.CalendarTimeFormat));
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').attr('data-time', newEndDate.tz(selectedTimeZone).format('HH:mm'));
        }
    })

    //NOTE If the startTime < endTime => change startTime
    endDateCtrl.on('change', function () {
        const { endVal, startVal, selectedTimeZone } = self.getDateFieldValue();
        let startMomentVal = moment.tz(startVal, selectedTimeZone);
        let endMomentVal = moment.tz(endVal, selectedTimeZone)
        if (endMomentVal.isSameOrBefore(startVal)) {
            let newStartDate = endMomentVal.subtract(parseInt(_initData.Company.Setting.CalendarTimeInterval), 'minute');
            if (moment(startMomentVal.format("HH:mm"), "HH:mm").isBefore(moment(endMomentVal.format("HH:mm"), "HH:mm"))) {
                newStartDate.hours(startMomentVal.format("HH")).minutes(startMomentVal.format("mm"))
            }
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-date').val(newStartDate.tz(selectedTimeZone).format(_initData.Company.Setting.DateFormat));
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-time').val(newStartDate.tz(selectedTimeZone).format(_initData.Company.Setting.CalendarTimeFormat));
        }
    })

    isAllDayCtrl.on('change', function () {
        if ($(this).is(':checked')) {
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-time').addClass('d-none');
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').addClass('d-none');
            data.PageContainer.find('[data-field="Subject"]').parent().removeClass("col-sm-8").addClass("col-sm-12");
            data.PageContainer.find('select.timezone').val("Europe/London").closest(".vld-parent").addClass('d-none');

        }
        else {
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-time').removeClass('d-none');
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').removeClass('d-none');
            data.PageContainer.find('[data-field="Subject"]').parent().removeClass("col-sm-12").addClass("col-sm-8")
            data.PageContainer.find('select.timezone').val(_initData.User.MySettings.Timezone).closest(".vld-parent").removeClass('d-none');

        }
    })

    // Reminder Controls---------
    reminderDisplay = ''
    if (data.ParamData.readOnly) {
        reminderDisplay = 'd-none'
    }

    let remHtml = $(`<div class="form-group col-sm-12 vld-parent reminderGroup ${reminderDisplay}">
                    <button type="button" class=" btn btn-link btn-sm btnAddReminderRow p-0"> Add Reminder</button>
                    </div>`);

    $(remHtml).insertAfter(data.PageContainer.find('[data-field="Agenda"]').closest('.form-group'));

    data.PageContainer.find('.btnAddReminderRow').on('click', function () {
        if ($(this).closest('.reminderGroup').find('.reminderRow').length < 3) {
            let reminderRow = _calendar.BuildReminderRow(data.PageContainer);
            $(reminderRow).insertBefore($(this));
        }
        if ($(this).closest('.reminderGroup').find('.reminderRow').length >= 3) {
            $(this).hide();
        }
    })

    data.PageContainer.find('.reminderGroup').on('click', '.btnDelReminderRow', function () {
        let reminderId = $(this).closest('.reminderRow').attr('data-id')
        if (reminderId) {
            _calendar.RemoveActivityReminder(reminderId, function (data) {
                if (data.message != 'success') {
                    swal("Unable to delete error", { icon: "error" })
                }
            })
        }
        data.PageContainer.find('.btnAddReminderRow').show();
        $(this).closest('.reminderRow').remove();
    })


    data.PageContainer.find('.attendee-change-status').on('click', function () {
        if ($(this).attr('data-disable') == undefined) {  //To disable double click
            let btn = $(this);
            btn.attr('data-disable', true);
            setTimeout(function () { btn.removeAttr('data-disable'); }, 400);
            let options = {};
            let attendee = {};
            attendee.Id = $(this).closest('.list-group-item').attr('data-id');
            attendee.Name = $(this).closest('.list-group-item').attr('data-name');
            attendee.Type = $(this).closest('.list-group-item').attr('data-user-type');
            attendee.Status = $(this).closest('.list-group-item').attr('data-status');
            attendee.Email = $(this).closest('.list-group-item').attr('data-email');
            attendee.AccountName = $(this).closest('.list-group-item').attr('data-accountname');
            options.AttendeeData = attendee;
            options.ActivityID = data.Data._id;
            options.ParentModel = data.PageContainer;
            options.attendeeFieldData = data.PageData.Fields.find(f => f.Name == "Attendees");
            _calendar.ChangeStatusModel(options);
        }
    })

    //Edit Activity Case
    if (data.ParamData._id) {
        //Reminders 
        if (data.reminders && data.reminders.length > 0) {
            reminderArray = data.reminders;
            if (reminderArray.length > 0 && reminderArray != undefined) {
                for (let i = 0; i < reminderArray.length; i++) {
                    let reminderRow = _calendar.BuildReminderRowWithValue(reminderArray[i]);
                    $(reminderRow).insertBefore(data.PageContainer.find('.btnAddReminderRow'));
                }
            }
        }

        if (data.PageContainer.find('.btnAddReminderRow').closest('.reminderGroup').find('.reminderRow').length >= 3) {
            data.PageContainer.find('.btnAddReminderRow').hide();
        }

        if (isAllDayCtrl.is(':checked')) {
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-time').addClass('d-none')
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-time').val(moment("01-01-1970 00:00").format(_initData.Company.Setting.CalendarTimeFormat));
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').addClass('d-none')
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').val(moment("01-01-1970 23:59").format(_initData.Company.Setting.CalendarTimeFormat));
            data.PageContainer.find('select.timezone').val("Europe/London").closest(".vld-parent").addClass('d-none');
            data.PageContainer.find('[data-field="Subject"]').parent().removeClass("col-sm-8").addClass("col-sm-12");

        }
        else {
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-time').removeClass('d-none')
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').removeClass('d-none')
        }
        //All Day Cases
    }
}

//Event : Meeting,Call
_calendar.OnAddEvent = function (data) {
    // if (data.ResultData.IsRecurrence) {
    //     eventobj.SendMailWithIcs(data, 'newRecurrenceActivity');
    //     eventobj.SetActivityNotification(data.ResultData._id, 'newRecurrenceActivity');
    //     eventobj.SendMailToUserParticipants(data.ResultData._id, 'newRecurrenceActivity');
    //     eventobj.GenericPushNoitification(data.ResultData._id, 'newRecurrenceActivity');

    // } else {
    if (!_calendar.CheckBackDate(data.ResultData.StartDate)) {
        if (data.sendEmail == true) {
            eventobj.SendMailWithIcs(data, 'newActivity');
            eventobj.SendMailToUserParticipants(data.ResultData._id, 'newActivity');
        }
        eventobj.SetActivityNotification(data.ResultData._id, 'newActivity');
        eventobj.GenericPushNoitification(data.ResultData._id, 'newActivity');
    }

    // }
}

//Event : Meeting,Call
_calendar.OnUpdateEvent = function (data) {
    let activityData = data.Data;
    let ResultData = data.ReturnData;
    ResultData._id = activityData._id;
    if (activityData && ResultData) {
        if (activityData.StartDate != ResultData.StartDate || activityData.EndDate != ResultData.EndDate) {
            _calendar.ReinviteAttendee({ attendeeId: [], activityId: activityData._id, resetAll: true, event: 'update' }, (response) => {
                if (!_calendar.CheckBackDate(ResultData.StartDate)) {
                    if (data.sendEmail == true) {
                        eventobj.SendMailWithIcs({ ResultData }, 'reschedule');
                        eventobj.SendMailToUserParticipants(ResultData._id, 'reschedule');
                    }
                    eventobj.SetActivityNotification(activityData._id, 'reschedule');
                    eventobj.GenericPushNoitification(ResultData._id, 'reschedule');
                }

            })
        }
    }
}

//NOTE Delete Activity
_calendar.activityDeleted = function (data) {

    //NOTE Owner Deleted the Activity

    if (!_calendar.CheckBackDate(data.activityStartDate)) {
        if (data.owner == _initData.User._id) {
            eventobj.SendMailWithIcs(data, 'adminDelete');
            eventobj.SetActivityNotification(data.activityId, 'adminDelete');
            eventobj.SendMailToUserParticipants(data.activityId, 'adminDelete');
            eventobj.GenericPushNoitification(data.activityId, 'adminDelete');
        } else {//NOTE attendee deleted the activity
            eventobj.SendMailWithIcs(data, 'attendeeDelete');
            eventobj.SetActivityNotification(data.activityId, 'attendeeDelete');
            eventobj.SendMailToUserParticipants(data.activityId, 'attendeeDelete');
            eventobj.GenericPushNoitification(data.activityId, 'attendeeDelete');
        }
    }
}

/** Saloon Shop Init Function */
_calendar.InitializeShopActivityForm = function (data) {
    // let recurrenceCtrl = data.PageContainer.find('[data-field="Recurrence"]');
    // let recurringOccurrenceCtrl = data.PageContainer.find('[data-field="RecurringOccurrence"]');
    // let indefiniteRecurrenceCtrl = data.PageContainer.find('[data-field="IndefiniteRecurrence"]');
    let self = this;
    let startDateCtrl = data.PageContainer.find('[data-field="StartDate"]');
    let endDateCtrl = data.PageContainer.find('[data-field="EndDate"]');
    //let isAllDayCtrl = data.PageContainer.find('[data-field="IsAllDay"]');
    let shopCtrl = data.PageContainer.find('[data-field="ShopID"]');
    let locationCtrl = data.PageContainer.find('[data-field="LocationID"]');
    let serviceCtrl = data.PageContainer.find('[data-field="ServiceID"]');
    let employeeCtrl = data.PageContainer.find('[data-field="EmployeeID"]');
    let priceCtrl = data.PageContainer.find('[data-field="ServicePrice"]');
    let serviceTime = null;


    if (data.ParamData._id == undefined) {
        _common.ContainerLoading(true, $(locationCtrl).closest('.ctrl-container'));
        _common.ContainerLoading(true, $(shopCtrl).closest('.ctrl-container'));
        _common.ContainerLoading(true, $(employeeCtrl).closest('.ctrl-container'));
        let organizerId = _initData.User._id
        if (data.ParamData.OrganizerId) {
            organizerId = data.ParamData.OrganizerId
        }
        let staffData = _calendar.userFilterInfoArray.find(u => u._id == organizerId)
        _controls.SetLookupTypeValue({ 'field': data.PageData.Fields.find(x => x.Name == "EmployeeID"), 'ctrl': employeeCtrl.closest('.ctrl-container'), 'data': staffData });
        _common.ContainerLoading(false, $(employeeCtrl).closest('.ctrl-container'));
        let shopparam = {};
        shopparam.shopType = shopCtrl.attr('data-shopType');
        shopparam.employeeID = employeeCtrl.find(".lookup-item").attr("data-id");
        _shop.GetShopAndLocation(shopparam, function (resultData) {
            _controls.SetLookupTypeValue({ 'field': data.PageData.Fields.find(x => x.Name == "ShopID"), 'ctrl': shopCtrl.closest('.ctrl-container'), 'data': resultData.shopData });
            _controls.SetLookupTypeValue({ 'field': data.PageData.Fields.find(x => x.Name == "LocationID"), 'ctrl': locationCtrl.closest('.ctrl-container'), 'data': resultData.locationData });
            _common.ContainerLoading(false, $(locationCtrl).closest('.ctrl-container'));
            _common.ContainerLoading(false, $(shopCtrl).closest('.ctrl-container'));
        })
    }



    serviceCtrl.on('click', '.lookup-item .fa-times', function () {
        priceCtrl.val('');
        serviceTime = null;
    })

    shopCtrl.on('click', '.lookup-item .fa-times', function () {
        self.ClearLookupData();
    })

    locationCtrl.on('click', '.lookup-item .fa-times', function () {
        self.ClearLookupData();
    })

    serviceCtrl.on("onLookupChange", function () {
        let serviceId = $(this).find(".lookup-item").attr("data-id");
        let employeeId = employeeCtrl.find(".lookup-item").attr("data-id");
        if (serviceId != undefined && serviceId != null && employeeId != undefined && employeeId != null) {
            _shop.GetServicePriceAndTime({
                "serviceID": serviceId,
                "employeeID": employeeId,
                "callback": function (res) {
                    if (res.message == "success") {
                        if (res.servicePrice != null) {
                            if (res.servicePrice.WalkInDiscountedPrice != null)
                                priceCtrl.val(res.servicePrice.WalkInDiscountedPrice)
                            else if (res.servicePrice.WalkInOriginalPrice != null)
                                priceCtrl.val(res.servicePrice.WalkInOriginalPrice)
                        }
                        if (res.serviceTime != null) {
                            serviceTime = res.serviceTime.Time;
                            const { endVal, startVal, selectedTimeZone } = self.getDateFieldValue()
                            let startMomentVal = moment.tz(startVal, selectedTimeZone);
                            let newEndDate = startMomentVal.add(serviceTime, 'minute')
                            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-date').val(newEndDate.tz(selectedTimeZone).format(_initData.Company.Setting.DateFormat));
                            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').val(newEndDate.tz(selectedTimeZone).format(_initData.Company.Setting.CalendarTimeFormat));
                            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').attr('data-time', newEndDate.tz(selectedTimeZone).format('HH:mm'));
                        }
                        self.ChangeSubject();
                    }
                    else {
                        swal('Selected staff does not provide the service', { icon: "error" });
                        if (serviceCtrl.find('.lookup-item') != undefined) {
                            serviceCtrl.find('.lookup-item').remove();
                            serviceCtrl.attr('contenteditable', 'true');
                        }
                    }
                }
            })
        }
    })

    employeeCtrl.on('onLookupChange', function (e, el) {
        let newAttId = $(this).find(".lookup-item").attr("data-id");
        let serviceId = serviceCtrl.find(".lookup-item").attr("data-id");
        if (serviceId != undefined && serviceId != null && newAttId != null) {
            _common.GetObjectData({
                "pageName": "ShopProductServiceDetail",
                "_id": serviceId,
                "callback": function (res) {
                    let data = res.data;
                    if (data.EmployeeID.indexOf(newAttId) < 0) {
                        swal('Selected staff does not provide the service', { icon: "error" });
                        if (employeeCtrl.find('.lookup-item') != undefined) {
                            employeeCtrl.find('.lookup-item').remove();
                            employeeCtrl.attr('contenteditable', 'true');
                        }
                    }
                    else {
                        self.ChangeSubject();
                    }
                }
            })
        }
    })


    self.ChangeSubject = function () {
        let serviceName = '';
        if (serviceCtrl.find(".lookup-item").attr("data-id"))
            serviceName = serviceCtrl.find('.lookup-item').text();
        let employeeName = ''
        if (employeeCtrl.find(".lookup-item").attr("data-id"))
            employeeName = `BY ${employeeCtrl.find('.lookup-item').text()}`
        data.PageContainer.find('[data-field="Subject"]').val(serviceName + ' ' + employeeName);
    }

    //NOTE Get the value of dateTime fields in dialog
    self.getDateFieldValue = function () {
        let startField = { 'Name': $(startDateCtrl).data('field') };
        startField.Schema = { 'UIDataType': $(startDateCtrl).data('ui') };
        let startVal = _controls.GetCtrlValue({ field: startField, container: data.PageContainer });
        let endField = { 'Name': $(endDateCtrl).data('field') }
        endField.Schema = { 'UIDataType': $(endDateCtrl).data('ui') };
        let endVal = _controls.GetCtrlValue({ field: endField, container: data.PageContainer });
        let selectedTimeZone = data.PageContainer.find("[data-field=Timezone]")[0].value;
        return {
            endVal, startVal, selectedTimeZone
        }
    }

    self.ClearLookupData = function () {
        if (locationCtrl.find('.lookup-item') != undefined) {
            locationCtrl.find('.lookup-item').remove();
            locationCtrl.attr('contenteditable', 'true');
        }
        if (serviceCtrl.find('.lookup-item') != undefined) {
            serviceCtrl.find('.lookup-item').remove();
            serviceCtrl.attr('contenteditable', 'true');
        }
        priceCtrl.val('');
        serviceTime = null;
    }

    startDateCtrl.on('change', function () {
        const { endVal, startVal, selectedTimeZone } = self.getDateFieldValue()
        let startMomentVal = moment.tz(startVal, selectedTimeZone);
        let endMomentVal = moment.tz(endVal, selectedTimeZone)
        if (startMomentVal.isSameOrAfter(endVal)) {
            let newEndDate = startMomentVal.add(parseInt(_initData.Company.Setting.CalendarTimeInterval), 'minute')
            if (moment(startMomentVal.format("HH:mm"), "HH:mm").isBefore(moment(endMomentVal.format("HH:mm"), "HH:mm"))) {
                newEndDate.hours(endMomentVal.format("HH")).minutes(endMomentVal.format("mm"))
            }
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-date').val(newEndDate.tz(selectedTimeZone).format(_initData.Company.Setting.DateFormat));
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').val(newEndDate.tz(selectedTimeZone).format(_initData.Company.Setting.CalendarTimeFormat));
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-time').attr('data-time', newEndDate.tz(selectedTimeZone).format('HH:mm'));
        }
        else {
            data.PageContainer.find('[data-field="EndDate"].msp-ctrl-date').val(startMomentVal.tz(selectedTimeZone).format(_initData.Company.Setting.DateFormat));
        }
    })

    //NOTE If the startTime < endTime => change startTime
    endDateCtrl.on('change', function () {
        const { endVal, startVal, selectedTimeZone } = self.getDateFieldValue();
        let startMomentVal = moment.tz(startVal, selectedTimeZone);
        let endMomentVal = moment.tz(endVal, selectedTimeZone)
        if (endMomentVal.isSameOrBefore(startVal)) {
            let newStartDate = endMomentVal.subtract(parseInt(_initData.Company.Setting.CalendarTimeInterval), 'minute');
            if (moment(startMomentVal.format("HH:mm"), "HH:mm").isBefore(moment(endMomentVal.format("HH:mm"), "HH:mm"))) {
                newStartDate.hours(startMomentVal.format("HH")).minutes(startMomentVal.format("mm"))
            }
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-date').val(newStartDate.tz(selectedTimeZone).format(_initData.Company.Setting.DateFormat));
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-time').val(newStartDate.tz(selectedTimeZone).format(_initData.Company.Setting.CalendarTimeFormat));
        }
        else {
            data.PageContainer.find('[data-field="StartDate"].msp-ctrl-date').val(endMomentVal.tz(selectedTimeZone).format(_initData.Company.Setting.DateFormat));
        }
    })


    // Reminder Controls---------
    reminderDisplay = ''
    if (data.ParamData.readOnly) {
        reminderDisplay = 'd-none'
    }

    let remHtml = $(`<div class="form-group col-sm-12 vld-parent reminderGroup ${reminderDisplay}">
                    <button type="button" class=" btn btn-link btn-sm btnAddReminderRow p-0"> Add Reminder</button>
                    </div>`);

    $(remHtml).insertAfter(data.PageContainer.find('[data-field="Agenda"]').closest('.form-group'));

    data.PageContainer.find('.btnAddReminderRow').on('click', function () {
        if ($(this).closest('.reminderGroup').find('.reminderRow').length < 3) {
            let reminderRow = _calendar.BuildReminderRow(data.PageContainer);
            $(reminderRow).insertBefore($(this));
        }
        if ($(this).closest('.reminderGroup').find('.reminderRow').length >= 3) {
            $(this).hide();
        }
    })

    data.PageContainer.find('.reminderGroup').on('click', '.btnDelReminderRow', function () {
        let reminderId = $(this).closest('.reminderRow').attr('data-id')
        if (reminderId) {
            _calendar.RemoveActivityReminder(reminderId, function (data) {
                if (data.message != 'success') {
                    swal("Unable to delete error", { icon: "error" })
                }
            })
        }
        data.PageContainer.find('.btnAddReminderRow').show();
        $(this).closest('.reminderRow').remove();
    })


    data.PageContainer.find('.attendee-change-status').on('click', function () {
        if ($(this).attr('data-disable') == undefined) {  //To disable double click
            let btn = $(this);
            btn.attr('data-disable', true);
            setTimeout(function () { btn.removeAttr('data-disable'); }, 400);
            let options = {};
            let attendee = {};
            attendee.Id = $(this).closest('.list-group-item').attr('data-id');
            attendee.Name = $(this).closest('.list-group-item').attr('data-name');
            attendee.Type = $(this).closest('.list-group-item').attr('data-user-type');
            attendee.Status = $(this).closest('.list-group-item').attr('data-status');
            attendee.Email = $(this).closest('.list-group-item').attr('data-email');
            attendee.AccountName = $(this).closest('.list-group-item').attr('data-accountname');
            options.AttendeeData = attendee;
            options.ActivityID = data.Data._id;
            options.ParentModel = data.PageContainer;
            options.attendeeFieldData = data.PageData.Fields.find(f => f.Name == "Attendees");
            _calendar.ChangeStatusModel(options);
        }
    })

    //Edit Activity Case
    if (data.ParamData._id) {
        //Reminders 
        if (data.reminders && data.reminders.length > 0) {
            reminderArray = data.reminders;
            if (reminderArray.length > 0 && reminderArray != undefined) {
                for (let i = 0; i < reminderArray.length; i++) {
                    let reminderRow = _calendar.BuildReminderRowWithValue(reminderArray[i]);
                    $(reminderRow).insertBefore(data.PageContainer.find('.btnAddReminderRow'));
                }
            }
        }

        if (data.PageContainer.find('.singleAttendeeField')) {
            data.PageContainer.find('.singleAttendeeField').hide();
        }

        if (data.PageContainer.find('.btnAddReminderRow').closest('.reminderGroup').find('.reminderRow').length >= 3) {
            data.PageContainer.find('.btnAddReminderRow').hide();
        }
        //All Day Cases
    }
}


_calendar.BackDateDeleteCheck = function (startDate) {
    if (_initData.Company.Setting.IsDeleteBackDateActivity)
        return true
    else
        return (!_calendar.CheckBackDate(startDate));
}

_calendar.CheckBackDateUpdate = function (startDate) {
    if (_initData.Company.Setting.IsUpdateBackDateActivity)
        return true
    else
        return (!_calendar.CheckBackDate(startDate));
}

_calendar.CheckBackDate = function (startDate) {
    let actStartDate = moment(startDate);
    let currentDate = moment().tz(_initData.User.MySettings.Timezone);
    return actStartDate.isBefore(currentDate)
}
/****EVENT END */

_calendar.notificationOnDragEvent = function (id, act, sendEmail) {
    _calendar.ReadActivityData({
        _id: id, callback: function (data) {
            rData = data.data[0];
            rData.Contacts = [];
            data.AttendeesData.forEach(u => { if (u.Type == "contact") { rData.Contacts.push(u.Id) } })
            obj = {
                ReturnData: rData,
                Data: {
                    StartDate: act.startDT,
                    EndDate: act.endDT,
                    Timezone: act.timeZone,
                    _id: id
                },
                updatedData: {
                    StartDate: rData.StartDate,
                    Timezone: rData.Timezone
                },
                sendEmail: sendEmail
            }
            _calendar.OnUpdateEvent(obj);
        }
    })
}

//NOTE add private fields into the organizer data
_calendar.SeperatePrivateFields = function (data, pageSchema) {
    orgainzerIndex = null;
    data.AttendeesData.forEach((attendee, index) => { if (attendee.IsOrganiser) { orgainzerIndex = index } })
    fields = pageSchema.Fields;
    for (field of fields) {
        if (field.IsPrivate) {
            // if (data[field.Name]) {
            data.AttendeesData[orgainzerIndex][field.Name] = data[field.Name];
            delete data[field.Name];
            // }
        }
    }
    return data
}

//NOTE get private field names from pageData
_calendar.getPrivateFieldsFromPageData = function (pageData) {
    fields = [];
    pageData.Fields.forEach(f => {
        if (f.IsPrivate) {
            fields.push(f.Name)
        }
    })
    return fields
}

//SECTION   Open Activity Highlight
_calendar.OpenActivity = function () {
    let len = $(document).find('.modal.minimized').length;

    for (let i = 0; i < len; i++) {
        let str = $($(document).find('.modal.minimized')[i]).attr("data-uid");
        let res = str.replace("Activity:", "");
        setTimeout(function () {
            if ($('.modal.page-form').hasClass('minimized')) {
                $('div[data-id="' + res + '"]').css({ 'background-color': '#007EE3', 'color': '#fff' });
            }
        }, 50);
    }
}
//!SECTION 

// Get userFilterInfoArray Details
_calendar.GetUserSettingDetails = function (userIds, callback) {
    $.ajax({
        url: 'api/activity/getusersettingdetails',
        type: 'post',
        data: JSON.stringify({ userIds: userIds }),
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            callback(res);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

_calendar.NewActivityForm = function (param) {
    let self = this;
    this.PageName = param.pageName;
    this.OnSave = param.onSave;
    this.ParamData = param;

    let identifer = this.PageName;
    if (param._id != undefined)
        identifer += ':' + param._id;
    if (param.formClass != undefined) true
    identifer += ':' + param.formClass;
    let snav = $('.modal.page-form[data-uid="' + identifer + '"]');
    if (snav.length > 0) {
        if (snav.hasClass('minimized')) {
            snav[0].minSnippit.remove();
            snav.removeClass('minimized');
            snav.modal('show');
            return;
        } else {
            snav.remove();
        }

    }
    _common.Loading(true);
    let formModal = $('<div class="modal fade page-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true" data-uid="' + identifer + '">' +
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +

        '<div class="modal-header">' +
        '<h5 class="modal-title"></h5>' +
        '<button class="btn btn-sm bg-white ml-auto mr-0 modal-minimise" data-toggle="tooltip" data-placement="bottom" title="Minimize" value="Minimize"><i class="fas fa-minus"></i></button>' +
        '<button type="button" class="close ml-n1" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +

        '<div class="modal-body"></div>' +

        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>' +
        '<button type="button" class="btn btn-primary btnSaveObject">Save</button>' +
        '</div>' +

        '</div>' +
        '</div>' +
        '</div>');

    formModal.modal('show');

    if (param.formClass != undefined) {
        formModal.addClass(param.formClass);
        if (param.formClass.indexOf('secondary-form') >= 0) {
            formModal.find('.modal-header .modal-minimise').remove();
        }
    }
    this.PageContainer = formModal;

    this.Build = function () {
        formModal.find('.modal-title').html("New " + this.PageData.DisplayName);

        if (this.PageData.Fields != undefined && this.PageData.Fields.length > 0) {
            let frm = formModal.find('.modal-body').append('<div class="row"></div>').find('.row');
            for (let i = 0; i < this.PageData.Fields.length; i++) {
                let field = this.PageData.Fields[i];
                if (field.Schema == undefined)
                    continue;

                if (field.Name == "OwnerID")
                    continue;
                _page.BuildField({ field: field, container: frm, data: param });


            }
        }

        if (this.PageData.Groups != undefined) {
            for (let g = 0; g < this.PageData.Groups.length; g++) {
                let group = this.PageData.Groups[g];
                let pageGroup = $('<div><h6 class="mb-2 mt-3">' + group.DisplayName + '</h6><div class="row page-group"></div></div>');
                for (let i = 0; i < group.Fields.length; i++) {
                    let field = group.Fields[i];
                    if (field.Schema == undefined)
                        continue;
                    _page.BuildField({ field: field, container: pageGroup.find('.page-group'), data: param });
                }
                formModal.find('.modal-body').append(pageGroup);
            }
        }

        if (param.MatchFields == undefined && this.PageData.MatchFields != undefined)
            param.MatchFields = this.PageData.MatchFields;
        else if (param.MatchFields != undefined && this.PageData.MatchFields != undefined)
            param.MatchFields = param.MatchFields.concat(this.PageData.MatchFields);

        if (this._id == undefined && param.MatchFields != undefined) {
            for (let i = 0; i < param.MatchFields.length; i++) {
                let fld = param.MatchFields[i];
                let ctrl = formModal.find('[data-field="' + fld.Name + '"]');
                if (ctrl.data('ui') == 'lookup' || ctrl.data('ui') == 'multilookup') {
                    if (fld.Value != undefined) {
                        ctrl.html('');
                        for (let i = 0; i < fld.Value.length; i++) {
                            _common.GetLookupData(this.PageData.ObjectName, fld.Name, fld.Value[i], function (data) {
                                let cHtm = '<div tabindex="0" class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1" contenteditable="false" data-id="' + data._id + '">' + data.name + '<i class="fas fa-times"></i></div>';
                                ctrl.append(cHtm);
                            });
                        }
                    }
                }
                else
                    ctrl.val(fld.Value);
            }
        }

        if (self.PageData.Events != undefined && self.PageData.Events.InitForm != undefined) {
            eval(self.PageData.Events.InitForm);
        }

        _userprofile.Auth({ pageData: self.PageData, container: formModal });

    }

    this.GetAttendeesFieldRecord = function (field, container, sdata) {
        sdata.EjsonFields["Contacts"] = [];
        sdata.EjsonFields["Accounts"] = [];
        let contactArr = [];
        let accountArr = [];
        sdata.EjsonFields["Users"] = [];
        sdata.FieldsValue["Users"] = [];

        let attendeeArr = [];
        // If Organizer is not the same as the current user
        let organizerUser = {};
        if (param.OrganizerId != undefined) {
            organizerUser.Id = _controls.ToEJSON('objectid', param.OrganizerId);
            organizerUser.Type = 'user';
            organizerUser.IsOrganiser = true;
            organizerUser.Status = 'accepted';
        }
        else {
            organizerUser = {
                'Id': _controls.ToEJSON('objectid', _initData.User._id), 'Type': 'user', 'IsOrganiser': true, 'Status': 'accepted'
            };
        }

        // let currentUser = {
        //     'Id': _controls.ToEJSON('objectid', _initData.User._id), 'Type': 'user', 'IsOrganiser': true, 'Status': 'accepted'
        // };
        attendeeArr.push(organizerUser);

        container.find('.msp-ctrl[data-field="' + field.Name + '"]').find('.lookup-item').each(function () {
            let attendee = {}
            attendee.Id = _controls.ToEJSON('objectid', $(this).attr('data-id'));
            let IdInString = $(this).attr('data-id');
            attendee.Type = $(this).find('.attendee-type').hasClass('contact') ? 'contact' : 'user';
            attendee.Status = 'pending';
            if (attendee.Type == 'contact') {
                attendee.AccountID = $(this).find('[data-aid]').attr('data-aid');
                if (attendee.AccountID)
                    attendee.AccountID = _controls.ToEJSON('objectid', attendee.AccountID);
            }
            attendeeArr.push(attendee);

            if (attendee.Type == 'contact') {
                contactArr.push(attendee.Id)
                if (attendee.AccountID) {
                    if (accountArr.indexOf(attendee.AccountID) == -1)
                        accountArr.push(attendee.AccountID)
                }
            }
            else {
                sdata.EjsonFields["Users"].push(attendee.Id)
                sdata.FieldsValue["Users"].push(IdInString)
            }
            if (accountArr.length > 0) {
                sdata.EjsonFields["Accounts"] = accountArr;
            }
            if (contactArr.length > 0) {
                sdata.EjsonFields["Contacts"] = contactArr;
            }
        });

        sdata.EjsonFields.AttendeesData = attendeeArr;

    }

    _common.GetPageDetail(this.PageName, function (result) {
        if (result.Fields != undefined)
            result.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });

        self.PageData = result;
        self.Build();
        _common.Loading(false);
    });

    formModal.find('.btnSaveObject').click(function () {
        if (formModal.find('[data-field]').length == 0) {
            _common.Loading(false);
            return false;
        }

        if (_common.Validate(formModal)) {
            let sdata = { 'PageName': self.PageData.PageName };
            sdata.FieldsValue = {};
            sdata.EjsonFields = {};
            sdata.ReminderArr = [];
            sdata.AttendeesArr = [];
            if (self.PageData.Events != undefined && self.PageData.Events.CustomValidation != undefined) {
                if (eval(self.PageData.Events.CustomValidation) == false) {
                    _common.Loading(false);
                    return false;
                }
            }

            if (self.PageData.Fields != undefined) {
                for (let i = 0; i < self.PageData.Fields.length; i++) {
                    let field = self.PageData.Fields[i];
                    if (field.Schema == undefined)
                        continue;

                    if (field.Name == "Attendees") {
                        sdata.AttendeesArr = self.GetAttendeesFieldRecord(field, formModal, sdata)
                        continue;
                    }

                    let valWithJson = _controls.GetCtrlValue({ field: field, container: formModal, isEjsonFormat: true });
                    sdata.EjsonFields[field.Name] = valWithJson;
                    let val = _controls.GetCtrlValue({ field: field, container: formModal });
                    sdata.FieldsValue[field.Name] = val;
                }
            }

            if (self.PageData.Groups != undefined) {
                for (let g = 0; g < self.PageData.Groups.length; g++) {
                    let group = self.PageData.Groups[g];
                    for (let i = 0; i < group.Fields.length; i++) {
                        let field = group.Fields[i];
                        if (field.Schema == undefined)
                            continue;

                        let valWithJson = _controls.GetCtrlValue({ field: field, container: formModal, isEjsonFormat: true });
                        sdata.EjsonFields[field.Name] = valWithJson;
                        _page.CheckFieldValidation(field, valWithJson, self.PageData.ObjectName, sdata._id);

                        let val = _controls.GetCtrlValue({ field: field, container: formModal });
                        if (self._id != undefined && val == undefined)
                            val = null;
                        sdata.FieldsValue[field.Name] = val;
                    }
                }
            }

            if (sdata.EjsonFields.Accounts && sdata.EjsonFields.Accounts.length > 1) {
                let tempAccs = [];
                sdata.EjsonFields.Accounts.forEach(
                    acc => {
                        if (!tempAccs.find(t => t.$oid == acc.$oid)) {
                            tempAccs.push(acc);
                        }
                    }
                )
                sdata.EjsonFields.Accounts = tempAccs
            }

            if (sdata.FieldsValue['IsAllDay']) {
                let stDateTime = moment.utc(moment.tz(sdata.FieldsValue['StartDate'], sdata.FieldsValue['Timezone']).format('YYYY-MM-DD'));
                let enDateTime = moment.utc(moment.tz(sdata.FieldsValue['EndDate'], sdata.FieldsValue['Timezone']).format('YYYY-MM-DD')).add(1, 'd').subtract(1, 'ms');

                sdata.FieldsValue['StartDate'] = stDateTime;
                sdata.EjsonFields['StartDate'] = _controls.ToEJSON("datetime", stDateTime);
                sdata.FieldsValue['EndDate'] = enDateTime;
                sdata.EjsonFields['EndDate'] = _controls.ToEJSON("datetime", enDateTime);
            }
            else if (sdata.FieldsValue['Timezone'] != _initData.User.MySettings.Timezone) {
                let stDateTime = moment.tz(sdata.FieldsValue['StartDate'], sdata.FieldsValue['Timezone']).format();
                let enDateTime = moment.tz(sdata.FieldsValue['EndDate'], sdata.FieldsValue['Timezone']).format();

                sdata.FieldsValue['StartDate'] = stDateTime;
                sdata.EjsonFields['StartDate'] = _controls.ToEJSON("datetime", stDateTime);
                sdata.FieldsValue['EndDate'] = enDateTime;
                sdata.EjsonFields['EndDate'] = _controls.ToEJSON("datetime", enDateTime);
            }

            //get the value of attendee options and set the value in EJSONS Fields

            // if (formModal.find('.reminderRow').length > 0) {
            //     //let reminderArr = [];
            //     formModal.find('.reminderRow').each(function () {
            //         let reminderObj = {};
            //         reminderObj.ObjectName = "Activity";
            //         reminderObj.EventStartTime = _controls.ToEJSON("datetime", sdata.FieldsValue["StartDate"]);
            //         let reminderType = $(this).find('[data-field="ReminderType"]').val();
            //         switch (reminderType) {
            //             case "email":
            //                 reminderObj.Mail = true;
            //                 reminderObj.Notification = false;
            //                 break;
            //             case "notification":
            //                 reminderObj.Mail = false;
            //                 reminderObj.Notification = true;
            //                 break;
            //             case "both":
            //                 reminderObj.Mail = true;
            //                 reminderObj.Notification = true;
            //                 break;
            //         }
            //         let number = $(this).find('[data-field="ReminderNumber"]').val();
            //         reminderObj.Number = number
            //         let parameter = $(this).find('[data-field="ReminderParameter"]').val();
            //         reminderObj.Parameter = parameter;
            //         sdata.ReminderArr.push(reminderObj);
            //     })
            // }

            let uniqueReminderArr = _calendar.getAddedReminderArray(formModal).uniqueReminderArr;
            uniqueReminderArr.forEach(rem => {
                let reminderObj = {};
                reminderObj.ObjectName = "Activity";
                reminderObj.EventStartTime = _controls.ToEJSON("datetime", sdata.FieldsValue["StartDate"]);
                switch (rem.type) {
                    case "email":
                        reminderObj.Mail = true;
                        reminderObj.Notification = false;
                        break;
                    case "notification":
                        reminderObj.Mail = false;
                        reminderObj.Notification = true;
                        break;
                    case "both":
                        reminderObj.Mail = true;
                        reminderObj.Notification = true;
                        break;
                }
                reminderObj.Number = rem.number;
                reminderObj.Parameter = rem.parameter;
                sdata.ReminderArr.push(reminderObj);
            })

            sdata.EjsonFields = _calendar.SeperatePrivateFields(sdata.EjsonFields, self.PageData);

            let startDate = moment.tz(sdata.FieldsValue["StartDate"], _initData.User.MySettings.Timezone);
            startDate.hours(0).minutes(0).seconds(0);
            let endDate = moment.tz(sdata.FieldsValue["EndDate"], _initData.User.MySettings.Timezone);
            endDate.hours(0).minutes(0).seconds(0);
            endDate.add(1, 'd');
            let userData = {};
            userData.UserArray = sdata.EjsonFields["Users"];
            userData.StartDateTime = sdata.FieldsValue["StartDate"];
            userData.EndDateTime = sdata.FieldsValue["EndDate"];
            userData.StartDate = startDate;
            userData.EndDate = endDate;
            userData.UserArrayInString = sdata.FieldsValue["Users"];

            var options = {};
            options.userData = userData;
            options.ParentModel = formModal;
            options.onComplete = function (continueResponse) {

                if (!continueResponse.Continue) {
                    if (param.cal)
                        param.cal.ResetActivities();
                    if (!continueResponse.Close)
                        formModal.modal('show');

                    return false;
                }
                else {
                    _calendar.AddActivityData(sdata, function (result) {
                        if (result.message == 'success') {
                            self.ResultData = result.ResultData;
                            formModal.modal('hide');
                            self.sendEmail = false;
                            let isAttendeeExist = false;
                            let activityData = self.ResultData;
                            activityData.AttendeesData.forEach(
                                att => {
                                    if ((att.Type == 'contact' || att.Type == 'user') && att.IsOrganiser == undefined) {
                                        isAttendeeExist = true;
                                    }
                                }
                            )
                            if (isAttendeeExist && !_calendar.CheckBackDate(self.ResultData.StartDate)) {
                                swal({
                                    title: "Event Invitation",
                                    text: "Would you like to notify the attendees?",
                                    icon: "info",
                                    closeOnClickOutside: false,
                                    buttons: {
                                        confirm: {
                                            text: "Send",
                                            value: true,
                                            visible: true,
                                            closeModal: true
                                        },
                                        cancel: {
                                            text: "Don't Send",
                                            value: null,
                                            visible: true,
                                            closeModal: true,
                                        }
                                    },
                                    dangerMode: false,
                                }).then((confirm) => {
                                    if (confirm) {
                                        self.sendEmail = true;
                                    }
                                    if (self.PageData.Events != undefined && self.PageData.Events.Add) {
                                        self.ActivityData = sdata;
                                        eval(self.PageData.Events.Add);
                                    }
                                    swal('Activity has been created.', { icon: "success", });
                                    if (param.cal)
                                        param.cal.ResetActivities();
                                    if (self.OnSave)
                                        self.OnSave();
                                    _common.Loading(false);
                                })
                            }
                            else {
                                if (self.PageData.Events != undefined && self.PageData.Events.Add) {
                                    self.ActivityData = sdata;
                                    eval(self.PageData.Events.Add);
                                }
                                swal('Activity has been created.', { icon: "success", });
                                if (param.cal)
                                    param.cal.ResetActivities();
                                if (self.OnSave)
                                    self.OnSave();
                                _common.Loading(false);
                            }
                        }
                        else {
                            swal(result.message, { icon: "error", });
                            _common.Loading(false);
                        }

                    });
                }
            }
            if (sdata.FieldsValue.IsAllDay || sdata.FieldsValue.Recurrence != null || !moment(sdata.FieldsValue.StartDate, "YYYY-MM-DD").isSame(moment(sdata.FieldsValue.EndDate, "YYYY-MM-DD"))) {
                options.onComplete({ Continue: true })
            } else {
                _calendar.CheckUserParticipantsAvailability(options);
            }

        }
        else {
            _common.Loading(false);
            self.PageContainer.find('.has-error:first')[0].scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
        }
    })
}



//SECTION   Edit Activity Form
_calendar.EditActivityForm = function (param) {
    let self = this;
    this.PageName = param.pageName;
    this.ParamData = param;
    this.ReadOnly = true;
    let identifer = 'Activity';
    if (param._id != undefined)
        identifer += ':' + param._id;
    if (param.formClass != undefined)
        identifer += ':' + param.formClass;
    let snav = $('.modal.page-form[data-uid="' + identifer + '"]');
    if (snav.length > 0) {
        if (snav.hasClass('minimized')) {
            snav[0].minSnippit.remove();
            snav.removeClass('minimized');
            $('div[data-id="' + param._id + '"]').removeClass('minimized');
            snav.modal('show');
            return;
        } else {
            snav.remove();
        }
    }

    _common.Loading(true);

    let formModal = $('<div class="modal fade page-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true" data-uid="' + identifer + '">' +
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +

        '<div class="modal-header">' +
        '<h5 class="modal-title"></h5>' +
        '<button class="btn btn-sm bg-white ml-auto mr-0 modal-minimise" data-toggle="tooltip" data-placement="bottom" title="Minimize" value="Minimize"><i class="fas fa-minus"></i></button>' +
        '<button type="button" class="close ml-n1 closing-event" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +

        '<div class="modal-body"></div>' +

        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-light closing-event" data-dismiss="modal">Cancel</button>' +
        '<button type="button" class="btn btn-outline-primary btnDeleteObject">Delete</button>' +
        '<button type="button" class="btn btn-primary btnSaveObject">Save</button>' +
        '</div>' +

        '</div>' +
        '</div>' +
        '</div>');
    formModal.modal('show');
    if (param.formClass != undefined) {
        formModal.addClass(param.formClass);
        if (param.formClass.indexOf('secondary-form') >= 0) {
            formModal.find('.modal-header .modal-minimise').remove();
        }
    }

    this.PageContainer = formModal;

    this.BuildAttendeesField = function (container) {

        //add attendee field 
        let addAttendeeField = $('<div class="form-group col-sm-8 vld-parent singleAttendeeField"><label class="small">Attendee</label></div>');
        if ((self.Data.InviteAttendee || (self.Data.OwnerID == _initData.User._id)) && !self.Data.IsRecurrence && _calendar.CheckBackDateUpdate(self.Data.StartDate)) {
            addAttendeeField.append(self.AddSingleAttendeeField());
            container.append(addAttendeeField)
            if (self.Data.OwnerID == _initData.User._id) {
                self.BuildCheckBoxField(self.PageData.Fields.find(f => f.Name == "SeeAttendeeList"), container)
                self.BuildCheckBoxField(self.PageData.Fields.find(f => f.Name == "InviteAttendee"), container)
            }
        }
        else
            container.append(addAttendeeField)

        addAttendeeField.on("onLookupChange", function (e, el) {
            let newAttId = $(el).data('id');
            let lookupField = this;
            let ctrllookup = $(lookupField).find('.msp-ctrl-lookup');
            if (self.AttendeesData.find(u => !u.IsOrganiser && u.Id == newAttId)) {
                swal('Attendee Already Present', { icon: "info" });
                ctrllookup.find('.lookup-item').remove();
                ctrllookup.attr('contenteditable', 'true').focus();
                return false;
            } else {

                swal({
                    title: "Are you sure..!",
                    text: "Would you like to add " + $(el).find('.attendee-type').html() + " to this event?",
                    icon: "warning",
                    buttons: {
                        confirm: {
                            text: "Yes",
                            value: true,
                            visible: true,
                            closeModal: true
                        },
                        cancel: {
                            text: "No",
                            value: null,
                            visible: true,
                            closeModal: true,
                        }

                    },
                    dangerMode: true,
                })
                    .then((willAdd) => {
                        if (willAdd) {
                            console.log(self);
                            let dayStart = moment(self.Data.StartDate).tz(self.Data.Timezone);
                            dayStart.hours(0).minutes(0).seconds(0);
                            let dayEnd = moment(self.Data.EndDate).tz(self.Data.Timezone);
                            dayEnd.hours(0).minutes(0).seconds(0);
                            dayEnd.add(1, 'd');

                            let userData = {};

                            userData.UserArray = [{ $oid: newAttId }];
                            userData.StartDateTime = moment(self.Data.StartDate).add(1, 's');
                            userData.EndDateTime = moment(self.Data.EndDate).subtract(1, 's');
                            userData.StartDate = dayStart;
                            userData.EndDate = dayEnd;
                            userData.UserArrayInString = [newAttId];

                            var options = {};
                            options.userData = userData;
                            options.onComplete = function (continueResponse) {
                                if (!continueResponse.Continue) {
                                    ctrllookup.find('.lookup-item').remove();
                                    ctrllookup.attr('contenteditable', 'true').focus();
                                    return false;
                                }
                                else {
                                    _calendar.AddAttendeeToList(newAttId, param._id, function (response) {
                                        if (response.message == "success") {
                                            swal('Attendee Added Successfully', { icon: "success" });
                                            ctrllookup.find('.lookup-item').remove();
                                            ctrllookup.attr('contenteditable', 'true');
                                            _common.Loading(false);
                                            if (response.AllAttendeesData) {
                                                self.AttendeesData = response.AllAttendeesData;
                                            }
                                            if (response.NewAttendeeData) {
                                                let newatt = self.AppendAttendeeField(response.NewAttendeeData);
                                                $(newatt).insertAfter(container.find('.participant-list .organiserListItem'))
                                            }
                                            let newAttendeeData = response.NewAttendeeData;
                                            let activityAdmin = null;
                                            let ResultData = Object.assign({}, self.Data);
                                            if (!_calendar.CheckBackDate(ResultData.StartDate)) {
                                                activityAdmin = response.AllAttendeesData.find(u => u.Id == self.Data.OwnerID);
                                                if (newAttendeeData.AttendeeType == 'contact') {
                                                    ResultData.Contacts = [newAttendeeData.Id]
                                                    eventobj.SendMailWithIcs({ ResultData }, 'newActivity', [newAttendeeData.Id]);
                                                } else {
                                                    eventobj.SendMailToUserParticipants(ResultData._id, 'newActivity', [newAttendeeData.Id]);
                                                    eventobj.SetActivityNotification(ResultData._id, 'newActivity', [newAttendeeData.Id]);
                                                    eventobj.GenericPushNoitification(ResultData._id, 'newActivity', [newAttendeeData.Id]);
                                                }

                                                if (activityAdmin.Id != _initData.User._id) {
                                                    eventobj.SendMailToUserParticipants(ResultData._id, 'attendeeAddedByAttendee');
                                                    eventobj.SetActivityNotification(ResultData._id, 'attendeeAddedByAttendee');
                                                    eventobj.GenericPushNoitification(ResultData._id, 'attendeeAddedByAttendee');
                                                }

                                            }

                                        }
                                    });
                                }
                            }
                            _calendar.CheckUserParticipantsAvailability(options);



                        }
                        else {
                            ctrllookup.find('.lookup-item').remove();
                            ctrllookup.attr('contenteditable', 'true').focus();
                        }
                    });
            }
        })

        //Display Attendee Information
        let attendeeDiv = $('<div class="col-sm-12"></div>');
        let attendeeUl = $('<ul class="list-group participant-list border rounded"></ul></div>');
        let suspendedOrganiser = '';
        if (self.OrganiserData.UserStatus) {
            if (self.OrganiserData.UserStatus == _constantClient.USER_STATUS.SUSPEND)
                suspendedOrganiser = "suspended-user"
            else if (self.OrganiserData.UserStatus == _constantClient.USER_STATUS.REPLACE)
                suspendedOrganiser = "replaced-user"
        }
        let organiserListItem = `<li class="list-group-item d-flex pt-1 pb-1 pl-2 pr-2 bg-primary border-0 organiserListItem">
                        <div>
                            <h6 class="mb-0 text-white font-weight-bold ${suspendedOrganiser}">${self.OrganiserData.Name}</h6>
                            <p class="small mb-0 mt-0 text-white d-inline-block">${self.OrganiserData.Email}</p>
                        </div>
                      </li>`;
        attendeeUl.append(organiserListItem);

        let attendeeList = '';
        let attendeeArray = self.AttendeesData.filter(u => !u.IsOrganiser)
        attendeeArray.forEach(
            attendee => {
                attendeeList += self.AppendAttendeeField(attendee);
            }
        )

        // COndition for displaying attendeeelist
        if (self.Data.SeeAttendeeList || (self.Data.OwnerID == _initData.User._id)) {
            attendeeUl.append(attendeeList)
        }
        else {
            attendeeUl.append('<li class="list-group-item d-flex pt-2 pb-2 pl-3 pr-3 bg-light border-0"><span class="text-secondary">Organiser has prevented others to see the list</span></li>')
        }

        attendeeDiv.append(attendeeUl);
        container.append(attendeeDiv);
    }

    this.AppendAttendeeField = function (newAttendee) {
        let nameHtml = ''
        let emailHtml = ''
        let accountHtml = ''
        let mobileHtml = ''
        let statusClass = ''
        let emailHardBounced = false;
        let organiserOptions = '';
        let suspendedUser = '';


        nameHtml = newAttendee.Name;
        if (newAttendee.Type == 'user') {
            if (newAttendee.UserStatus) {
                if (newAttendee.UserStatus == _constantClient.USER_STATUS.SUSPEND)
                    suspendedUser = "suspended-user"
                else if (newAttendee.UserStatus == _constantClient.USER_STATUS.REPLACE)
                    suspendedUser = "replaced-user"
            }
            // if (_user.SuspendedUsers.find(u => u._id == newAttendee.Id) != undefined)
            //     suspendedUser = 'suspended-user'
        }
        else {
            debugger;
            if (newAttendee.AccountName)
                accountHtml = '<p class="small mb-0 mt-0 text-secondary d-inline-block pl-2 pr-2">|</p><p class="small mb-0 mt-0 text-secondary d-inline-block">' + newAttendee.AccountName + '</p>';
            if (newAttendee.Mobile)
                mobileHtml = '<p class="small mb-0 mt-0 text-secondary d-inline-block pl-2 pr-2">|</p><p class="small mb-0 mt-0 text-secondary d-inline-block">' + newAttendee.Mobile + '</p>';
        }

        //Hard Bounced Emails
        if (emailHardBounced)
            emailHtml = '<del>' + newAttendee.Email + '</del>';
        else
            emailHtml = newAttendee.Email;

        //Resend and Delete Options available for Organiser
        if (self.CurrentUserData != undefined && self.CurrentUserData.IsOrganiser && !self.Data.IsRecurrence) {
            organiserOptions = `<i class="btn btn-sm btn-secondary far fa-envelope attendee-reinvite" data-toggle="tooltip" data-placement="top" title="Re-Invite"></i>
                                    <i class="btn btn-sm btn-secondary fa fa-user-alt attendee-change-status" data-toggle="tooltip" data-placement="top" title="Change Status"> </i>
                                    <i class="btn btn-sm btn-danger fa fa-times attendee-remove-user" data-toggle="tooltip" data-placement="top" title="Remove Attendee"></i>`;
        }

        //Color on Status Basis
        switch (newAttendee.Status) {
            case "accepted":
                statusClass = 'text-success'
                break;
            case "rejected":
            case "deleted":
                statusClass = 'text-danger'
                break;
            default:
                statusClass = 'text-secondary'
                break;
        }

        let listItem = `<li class="list-group-item d-flex pt-1 pb-1 pl-2 pr-2 bg-light border-0" data-id="${newAttendee.Id}" data-name="${newAttendee.Name}" data-user-type="${newAttendee.Type}" data-status="${newAttendee.Status}" data-email="${newAttendee.Email}" data-accountname="${newAttendee.AccountName}" >
                         <div>
                            <h6 class="mb-0 font-weight-bold ${statusClass} ${suspendedUser}"  >${nameHtml}</h6>
                            <p class="small mb-0 mt-0 text-secondary d-inline-block">${emailHtml}</p>${accountHtml} ${mobileHtml}
                        </div>
                        <div class="participant-action ml-auto mt-1">${organiserOptions}</div>
                        </li>`;
        return listItem;
    }

    this.AddSingleAttendeeField = function () {
        let fieldAddAttendee = { 'Name': 'SingleAttendee', 'DisplayName': 'Attendee', 'ObjectName': 'Activity', 'HasAddButton': false };
        fieldAddAttendee.Schema = { UIDataType: "lookup", NotIncludeSelf: true };
        return _controls.BuildCtrl({ field: fieldAddAttendee });
    }

    this.BuildCheckBoxField = function (checkField, container) {
        _page.BuildField({ field: checkField, container: container, data: self.Data, removeEmpty: true });
    }

    this.BuildTimeField = function (field, container) {
        if (self.Data.IsAllDay) {
            let activityData = Object.assign({}, self.Data);
            //field.Schema.UIDataType = "date"
            activityData.StartDate = moment.tz(activityData.StartDate, 'YYYY-MM-DDTHH:mm:ss', activityData.Timezone).format();
            activityData.EndDate = moment.tz(activityData.EndDate, 'YYYY-MM-DDTHH:mm:ss', activityData.Timezone).format();
            _page.BuildField({ field: field, container: container, data: activityData, readOnly: self.ReadOnly, removeEmpty: self.ReadOnly });
        }
        else
            _page.BuildField({ field: field, container: container, data: self.Data, readOnly: self.ReadOnly, removeEmpty: self.ReadOnly });

    }

    this.BuildSequenceField = function (field, container) {
        let val = self.Data[field.Name]
        val = val + 1;
        self.Data[field.Name] = val;
        _page.BuildField({ field: field, container: container, data: self.Data, readOnly: self.ReadOnly, removeEmpty: false });
    }

    this.Build = function () {
        let title = '';
        formModal.find('.modal-title').html(this.PageData.DisplayName);
        if (this.PageData.Fields != undefined && this.PageData.Fields.length > 0) {
            let frm = formModal.find('.modal-body').append('<div class="row"></div>').find('.row');
            for (let i = 0; i < this.PageData.Fields.length; i++) {
                let field = this.PageData.Fields[i];
                if (field.Schema == undefined || field.Name == "OwnerID")
                    continue;

                if (field.Name == "SeeAttendeeList" || field.Name == "InviteAttendee")
                    continue;



                if (field.IsPrivate && !param.pending) {
                    if (self.CurrentUserData != undefined)
                        _page.BuildField({ field: field, container: frm, data: self.CurrentUserData, removeEmpty: false });
                }
                else if (field.Name == "Recurrence") {
                    _page.BuildField({ field: field, container: frm, data: self.Data, readOnly: true });
                }
                else if (field.Name == "StartDate" || field.Name == "EndDate")
                    this.BuildTimeField(field, frm);
                else if (field.Name == "Attendees")
                    this.BuildAttendeesField(frm);
                else if (field.Name == "Sequence")
                    this.BuildSequenceField(field, frm);
                else
                    _page.BuildField({ field: field, container: frm, data: self.Data, readOnly: self.ReadOnly, removeEmpty: false });
                if (field.IsPageTitle) {
                    let val = _controls.GetDisplayValue({ field: field, data: self.Data });
                    title += val + ' ';
                }
            }
        }

        if (this.PageData.Groups != undefined) {
            for (let g = 0; g < this.PageData.Groups.length; g++) {
                let group = this.PageData.Groups[g];
                let pageGroup = $('<div><h6 class="mb-2 mt-3">' + group.DisplayName + '</h6><div class="row page-group"></div></div>');
                for (let i = 0; i < group.Fields.length; i++) {
                    let field = group.Fields[i];
                    if (field.Schema == undefined)
                        continue;
                    if (self.ReadOnly) {
                        if (field.Name == "SearchAddress")
                            continue;
                        if (field.Name == "VenueName") {
                            field.ColumnWidth = 6;
                            _page.BuildField({ field: field, container: pageGroup.find('.page-group'), data: this.Data, readOnly: true, removeEmpty: self.ReadOnly });
                            continue;
                        }
                    }
                    _page.BuildField({ field: field, container: pageGroup.find('.page-group'), data: this.Data, readOnly: self.ReadOnly, removeEmpty: self.ReadOnly });
                }

                let allHidden = true;
                pageGroup.find('.vld-parent').each(function () {
                    if (!($(this).hasClass('d-none')))
                        allHidden = false
                })
                if (!allHidden)
                    formModal.find('.modal-body').append(pageGroup);

            }
        }

        if (this.Data != undefined) {
            formModal.find('.modal-title').html('Edit ' + title);
        }

        if (self.PageData.Events != undefined && self.PageData.Events.InitForm != undefined) {
            eval(self.PageData.Events.InitForm);
        }

        _userprofile.Auth({ pageData: self.PageData, container: formModal });
    }

    //STUB delete button clicked
    formModal.find('.btnDeleteObject').click(function () {
        if (self.Data._id != undefined || self.Data._id != "") {
            if (_calendar.BackDateDeleteCheck(self.Data.StartDate)) {
                if (self.Data.IsRecurrence) {
                    param.activityid = self.Data._id;
                    param.cal = self.ParamData.cal;
                    param.ParentModel = self.PageContainer;
                    _calendar.DeleteRecurrenceActivityModal(param);
                }
                else {
                    _common.ConfirmDelete({ message: 'You want to delete this activity ?' }).then((confirm) => {
                        if (confirm) {

                            _calendar.DeleteSingleActivity(self.Data._id, function (result) {
                                if (result.message == "success") {
                                    formModal.modal('hide');
                                    swal('Activity has been removed successfully.', { icon: "success", });
                                    if (self.ParamData.cal) {
                                        self.ParamData.cal.ResetActivities();
                                    }
                                    _calendar.activityDeleted({ ResultData: result.oldActivityData, activityId: self.Data._id, owner: self.Data.OwnerID, activityStartDate: self.Data.StartDate })
                                }
                                else
                                    swal(result.message, { icon: "error", });
                            })


                        }
                    });
                }
            }
            else {
                swal(_constantClient.BACKDATEDISALLOW, { icon: "info", });
            }
        }
    })

    //STUB Save button clicked
    formModal.find('.btnSaveObject').click(function () {
        if (!_common.Validate(formModal)) { return }
        let sdata = { 'PageName': self.PageData.PageName };
        sdata.FieldsValue = {};
        sdata.EjsonFields = {};
        sdata.ReminderArr = [];
        if (self.PageData.Events != undefined && self.PageData.Events.CustomValidation != undefined) {
            if (eval(self.PageData.Events.CustomValidation) == false) {
                return false;
            }
        }
        if (self.PageData.Fields != undefined) {
            for (let i = 0; i < self.PageData.Fields.length; i++) {
                let field = self.PageData.Fields[i];
                if (field.Schema == undefined)
                    continue;
                let valWithJson = _controls.GetCtrlValue({ field: field, container: formModal, isEjsonFormat: true });
                sdata.EjsonFields[field.Name] = valWithJson;
                let val = _controls.GetCtrlValue({ field: field, container: formModal });
                sdata.FieldsValue[field.Name] = val;
            }
        }

        if (self.PageData.Groups != undefined) {
            for (let g = 0; g < self.PageData.Groups.length; g++) {
                let group = self.PageData.Groups[g];
                for (let i = 0; i < group.Fields.length; i++) {
                    let field = group.Fields[i];
                    if (field.Schema == undefined)
                        continue;

                    let valWithJson = _controls.GetCtrlValue({ field: field, container: formModal, isEjsonFormat: true });
                    sdata.EjsonFields[field.Name] = valWithJson;

                    let val = _controls.GetCtrlValue({ field: field, container: formModal });
                    sdata.FieldsValue[field.Name] = val;
                }
            }
        }


        if (sdata.FieldsValue['IsAllDay']) {
            let stDateTime = moment.utc(moment.tz(sdata.FieldsValue['StartDate'], sdata.FieldsValue['Timezone']).format('YYYY-MM-DD'));
            let enDateTime = moment.utc(moment.tz(sdata.FieldsValue['EndDate'], sdata.FieldsValue['Timezone']).format('YYYY-MM-DD')).add(1, 'd').subtract(1, 'ms');

            sdata.FieldsValue['StartDate'] = stDateTime;
            sdata.EjsonFields['StartDate'] = _controls.ToEJSON("datetime", stDateTime);
            sdata.FieldsValue['EndDate'] = enDateTime;
            sdata.EjsonFields['EndDate'] = _controls.ToEJSON("datetime", enDateTime);
        }

        sdata._id = self.Data._id;
        sdata.FieldsValue.OwnerID = self.Data.OwnerID;
        sdata.EjsonFields.OwnerID = _controls.ToEJSON('objectid', self.Data.OwnerID);

        let uniqueReminderArr = _calendar.getAddedReminderArray(formModal).uniqueReminderArr;
        uniqueReminderArr.forEach(rem => {
            let reminderObj = { "ObjectName": "Activity", "EventName": "Activity", "EventID": _controls.ToEJSON('objectid', self.Data._id) };
            reminderObj.ObjectName = "Activity";
            reminderObj.EventStartTime = _controls.ToEJSON("datetime", sdata.FieldsValue["StartDate"]);
            reminderObj._id = rem.id
            switch (rem.type) {
                case "email":
                    reminderObj.Mail = true;
                    reminderObj.Notification = false;
                    break;
                case "notification":
                    reminderObj.Mail = false;
                    reminderObj.Notification = true;
                    break;
                case "both":
                    reminderObj.Mail = true;
                    reminderObj.Notification = true;
                    break;
            }
            reminderObj.Number = rem.number;
            reminderObj.Parameter = rem.parameter;
            sdata.ReminderArr.push(reminderObj);
        })




        let privateFields = [];
        self.PageData.Fields.forEach(
            field => {
                if (field.IsPrivate) {
                    privateFields.push(field.Name)
                }
            }
        )
        sdata.privateFields = privateFields

        let startDate = moment.tz(sdata.FieldsValue["StartDate"], _initData.User.MySettings.Timezone);
        startDate.hours(0).minutes(0).seconds(0);
        let endDate = moment.tz(sdata.FieldsValue["EndDate"], _initData.User.MySettings.Timezone);
        endDate.hours(0).minutes(0).seconds(0);
        endDate.add(1, 'd');
        let userData = {};
        userData.ActivityID = self.Data._id;
        userData.StartDateTime = moment(sdata.FieldsValue["StartDate"]).add(1, 's');
        userData.EndDateTime = moment(sdata.FieldsValue["EndDate"]).subtract(1, 's');
        userData.StartDate = startDate;
        userData.EndDate = endDate;

        var options = {};
        options.userData = userData;
        options.onComplete = function (continueResponse) {
            if (!continueResponse.Continue) {
                param.cal.ResetActivities();
                if (!continueResponse.Close)
                    formModal.modal('show');

                return false;
            }
            else {
                _calendar.UpdateActivityData(sdata, function (result) {
                    if (result.message == 'success') {
                        self.ReturnData = result.data;
                        formModal.modal('hide');
                        self.sendEmail = false;
                        // let activityStartDateDiff = moment.duration(moment(self.ReturnData.StartDate).diff(moment(self.Data.StartDate))).asMinutes();
                        // let activityEndDateDiff = moment.duration(moment(self.ReturnData.EndDate).diff(moment(self.Data.EndDate))).asMinutes();
                        let isAttendeeExist = false;
                        let activityData = self.ReturnData;
                        activityData.AttendeesData.forEach(
                            att => {
                                if ((att.Type == 'contact' || att.Type == 'user') && att.IsOrganiser == undefined) {
                                    isAttendeeExist = true;
                                }
                            }
                        )
                        if (isAttendeeExist && !_calendar.CheckBackDate(self.ReturnData.StartDate) && !(moment(self.ReturnData.StartDate).isSame(self.Data.StartDate) && moment(self.ReturnData.EndDate).isSame(self.Data.EndDate))) {
                            swal({
                                title: "Updated Event Invitation",
                                text: "Would you like to notify the attendees?",
                                icon: "info",
                                closeOnClickOutside: false,
                                buttons: {
                                    confirm: {
                                        text: "Send",
                                        value: true,
                                        visible: true,
                                        closeModal: true
                                    },
                                    cancel: {
                                        text: "Don't Send",
                                        value: null,
                                        visible: true,
                                        closeModal: true,
                                    }
                                },
                                dangerMode: false,
                            }).then((confirm) => {
                                if (confirm) {
                                    self.sendEmail = true;
                                }
                                if (self.ParamData.cal) {
                                    self.ParamData.cal.ResetActivities();
                                }
                                if (self.PageData.Events != undefined && self.PageData.Events.Update) {
                                    eval(self.PageData.Events.Update);
                                }
                                swal('Record saved successfully.', { icon: "success", });
                                if (self.OnSave != undefined) {
                                    self.OnSave();
                                }
                                _common.Loading(false);
                            })
                        } else {
                            if (self.ParamData.cal) {
                                self.ParamData.cal.ResetActivities();
                            }
                            if (self.PageData.Events != undefined && self.PageData.Events.Update) {
                                eval(self.PageData.Events.Update);
                            }
                            swal('Record saved successfully.', { icon: "success", });
                            if (self.OnSave != undefined) {
                                self.OnSave();
                            }
                            _common.Loading(false);
                        }

                    }
                    else {
                        swal(result.message, { icon: "error", });
                        _common.Loading(false);
                    }
                });
            }
        }
        if (sdata.FieldsValue.IsAllDay || self.Data.IsRecurrence || moment(sdata.FieldsValue.StartDate).format("YYYY-MM-DD") !== moment(sdata.FieldsValue.EndDate).format("YYYY-MM-DD") || (moment(self.Data.StartDate).tz(self.Data.Timezone).format() == moment(sdata.FieldsValue.StartDate).tz(sdata.FieldsValue.Timezone).format() && moment(self.Data.EndDate).tz(self.Data.Timezone).format() == moment(sdata.FieldsValue.EndDate).tz(sdata.FieldsValue.Timezone).format())) {
            options.onComplete({ Continue: true })
        } else {
            _calendar.CheckUserParticipantsAvailability(options);
        }

    })

    //STUB get attendeeId, attendeeType, activityId from the button context
    getDetailsInEditActivityForm = function (context) {
        console.log($(context).closest('.list-group-item').data("id"))
        return {
            attendeeId: $(context).closest('.list-group-item').data("id"),
            attendeeType: $(context).closest('.list-group-item').data("user-type"),
            activityId: self.Data._id
        }
    }

    //STUB remove attendee button 
    formModal.find('.modal-body').on('click', '.list-group-item .attendee-remove-user', function () {
        let { attendeeId, attendeeType, activityId } = getDetailsInEditActivityForm(this)
        swal({
            title: "Are you sure..!",
            text: "Would you like to remove the selected user?",
            icon: "warning",
            buttons: {
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    closeModal: true
                },
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    closeModal: true,
                }

            },
            dangerMode: true,
        }).then((confirm) => {
            if (confirm) {
                _calendar.deleteAttendeeFromActivity({ attendeeId, attendeeType, activityId }, (response) => {
                    if (response.message == 'success') {
                        $(this).closest('.list-group-item').remove();
                        self.AttendeesData = self.AttendeesData.filter(att => att.Id != attendeeId);
                        let updatedActivityData = response.ActivityData;
                        let deletedAttendee = updatedActivityData.AttendeesData.find(u => u.Id == response.deletedAttendeeId);
                        if (!_calendar.CheckBackDate(updatedActivityData.StartDate)) {
                            if (deletedAttendee.Type == 'contact') {
                                // eventobj.SendMailToUserParticipants(updatedActivityData._id, 'contactRemoved', [deletedAttendee.Id]);
                                eventobj.SendMailWithIcs({ ResultData: updatedActivityData }, 'contactRemoved', [deletedAttendee.Id]);
                            } else {
                                eventobj.SendMailToUserParticipants(updatedActivityData._id, 'userRemoved', [deletedAttendee.Id]);
                                eventobj.SetActivityNotification(updatedActivityData._id, 'userRemoved', [deletedAttendee.Id]);
                                eventobj.GenericPushNoitification(updatedActivityData._id, 'userRemoved', [deletedAttendee.Id]);
                            }
                        }
                    }
                })
            }
        })

    })

    //STUB reinvite button clicked
    formModal.find('.modal-body').on('click', '.list-group-item .attendee-reinvite', function () {
        let { attendeeId, attendeeType, activityId } = getDetailsInEditActivityForm(this)
        _common.ConfirmDelete({ message: 'You want to resend the invitation ?' }).then((confirm) => {
            if (confirm) {
                _calendar.ReinviteAttendee({ attendeeId, activityId, resetAll: false, event: 'add' }, (response) => {

                    $(this).closest('.list-group-item').attr('data-status', 'pending').find('h6')
                        .removeClass('text-success').addClass('text-secondary');

                    let { UpdatedActivityData, reinviteeId } = response
                    let reInvitedUser = UpdatedActivityData.AttendeesData.find(u => u.Id == reinviteeId);
                    if (!_calendar.CheckBackDate(UpdatedActivityData.StartDate)) {
                        if (reInvitedUser.Type == 'contact') {
                            eventobj.SendMailWithIcs({ ResultData: UpdatedActivityData }, 'newActivity', [reinviteeId]);
                        } else {
                            eventobj.SetActivityNotification(activityId, 'newActivity', [reinviteeId]);
                            eventobj.SendMailToUserParticipants(activityId, 'newActivity', [reinviteeId]);
                            eventobj.GenericPushNoitification(activityId, 'newActivity', [reinviteeId]);
                        }
                    }

                })
            }
        })
    })

    //STUB API call readactivitydata
    if (param._id != undefined) {
        _calendar.ReadActivityData({
            '_id': param._id,
            callback: function (result) {
                self.Data = result.data[0];
                if (result.pagedata.Fields != undefined)
                    result.pagedata.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
                self.PageData = result.pagedata;
                self.AttendeesData = result.AttendeesData;
                self.reminders = result.reminders;
                self.CurrentUserData = result.AttendeesData.find(u => u.Id == _initData.User._id)
                self.OrganiserData = result.AttendeesData.find(u => u.IsOrganiser == true)
                if (self.CurrentUserData != undefined && self.CurrentUserData.Id == self.Data.OwnerID && !self.Data.IsRecurrence)
                    self.ReadOnly = false;
                self.Build();
                _common.Loading(false);
            }
        });
    }

}
//!SECTION 

// Read Only Form
_calendar.ReadOnlyActivityForm = function (param) {
    let self = this;
    this.PageName = param.pageName;
    this.ParamData = param;
    self.ParamData.readOnly = true;

    let identifer = 'Activity';
    if (param._id != undefined)
        identifer += ':' + param._id;
    if (param.formClass != undefined)
        identifer += ':' + param.formClass;
    let snav = $('.modal.page-form[data-uid="' + identifer + '"]');
    if (snav.length > 0) {
        if (snav.hasClass('minimized')) {
            snav[0].minSnippit.remove();
            snav.removeClass('minimized');
            snav.modal('show');
            return;
        } else {
            snav.remove();
        }

    }
    _common.Loading(true);

    let formModal = $('<div class="modal fade page-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true" data-uid="' + identifer + '">' +
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +

        '<div class="modal-header">' +
        '<h5 class="modal-title"></h5>' +
        '<button class="btn btn-sm bg-white ml-auto mr-0 modal-minimise" data-toggle="tooltip" data-placement="bottom" title="Minimize" value="Minimize"><i class="fas fa-minus"></i></button>' +
        '<button type="button" class="close ml-n1" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +

        '<div class="modal-body"></div>' +

        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>' +
        '</div>' +

        '</div>' +
        '</div>' +
        '</div>');
    formModal.modal('show');
    if (param.formClass != undefined) {
        formModal.addClass(param.formClass);
        if (param.formClass.indexOf('secondary-form') >= 0) {
            formModal.find('.modal-header .modal-minimise').remove();
        }
    }

    this.PageContainer = formModal;

    this.BuildReccurrenceField = function (field, container) {
        let val;
        let dispName = _page.GetFieldDisplayName(field);
        if (this.Data != undefined) {
            val = _controls.GetDisplayValue({ field: field, data: this.Data });
        }

        let col = 6;
        if (field.ColumnWidth != undefined)
            col = 12 / field.ColumnWidth;

        let isHidden = (field.IsHidden) ? 'field-hidden d-none' : '';
        if (val == "" || val == undefined || val == '<img src="/images/unchecked.svg" />') {
            isHidden = 'field-hidden d-none';
        }

        let fieldContainer = $('<div class="form-group col-sm-' + col + ' vld-parent ' + isHidden + '"></div>');
        fieldContainer.append('<label class="mb-0">' + (field.DisplayName ? field.DisplayName : field.Schema.DisplayName) + '</label>');
        let readOnlyCtrl = $('<div>' + val + '</div>');
        fieldContainer.append(readOnlyCtrl);


        container.append(fieldContainer);
    }

    this.BuildAttendeesField = function (container) {

        //Display Attendee Information
        let attendeeDiv = $('<div class="col-sm-12"><label class="small">Attendee List</label></div>');
        let attendeeUl = $('<ul class="list-group participant-list border rounded"></ul></div>');
        let suspendedOrganiser = '';
        if (self.OrganiserData.UserStatus) {
            if (self.OrganiserData.UserStatus == _constantClient.USER_STATUS.SUSPEND)
                suspendedOrganiser = "suspended-user"
            else if (self.OrganiserData.UserStatus == _constantClient.USER_STATUS.REPLACE)
                suspendedOrganiser = "replaced-user"
        }
        // if (_user.SuspendedUsers.find(u => u._id == self.OrganiserData.Id) != undefined)
        //     suspendedOrganiser = 'suspended-user'
        let organiserListItem = `<li class="list-group-item d-flex pt-1 pb-1 pl-2 pr-2 bg-primary border-0 organiserListItem">
                        <div>
                            <h6 class="mb-0 text-white font-weight-bold ${suspendedOrganiser}">${self.OrganiserData.Name} </h6>
                            <p class="small mb-0 mt-0 text-white d-inline-block">${self.OrganiserData.Email}</p>
                        </div>
                      </li>`;
        attendeeUl.append(organiserListItem);

        let attendeeList = '';
        let attendeeArray = self.AttendeesData.filter(u => !u.IsOrganiser)
        attendeeArray.forEach(
            attendee => {

                attendeeList += self.AppendAttendeeField(attendee);
            }
        )

        // COndition for displaying attendeeelist
        if (self.Data.SeeAttendeeList || (self.Data.OwnerID == _initData.User._id)) {
            attendeeUl.append(attendeeList)
        }
        else {
            attendeeUl.append('<li class="list-group-item d-flex pt-2 pb-2 pl-3 pr-3 bg-light border-0"><span class="text-secondary">Organiser has prevented others to see the list</span></li>')
        }

        attendeeDiv.append(attendeeUl);
        container.append(attendeeDiv);

    }

    this.AppendAttendeeField = function (newAttendee) {
        let nameHtml = ''
        let emailHtml = ''
        let accountHtml = ''
        let statusClass = ''
        let emailHardBounced = false;
        let suspendedUser = '';

        if (newAttendee.Type == 'user') {
            nameHtml = newAttendee.Name;
            if (newAttendee.UserStatus) {
                if (newAttendee.UserStatus == _constantClient.USER_STATUS.SUSPEND)
                    suspendedUser = "suspended-user"
                else if (newAttendee.UserStatus == _constantClient.USER_STATUS.REPLACE)
                    suspendedUser = "replaced-user"
            }
            // if (_user.SuspendedUsers.find(u => u._id == newAttendee.Id) != undefined)
            //     suspendedUser = 'suspended-user'
        }
        else {
            nameHtml = newAttendee.Name;
            if (newAttendee.AccountName)
                accountHtml = '<p class="small mb-0 mt-0 text-secondary d-inline-block">' + newAttendee.AccountName + '</p>';
        }

        //Hard Bounced Emails
        if (emailHardBounced)
            emailHtml = '<del>' + newAttendee.Email + '</del>';
        else
            emailHtml = newAttendee.Email;

        //Color on Status Basis
        switch (newAttendee.Status) {
            case "accepted":
                statusClass = 'text-success'
                break;
            case "rejected":
            case "deleted":
                statusClass = 'text-danger'
                break;
            default:
                statusClass = 'text-secondary'
                break;
        }

        let listItem = `<li class="list-group-item d-flex pt-1 pb-1 pl-2 pr-2 bg-light border-0" data-id="${newAttendee.Id}" data-name="${newAttendee.Name}" data-user-type="${newAttendee.Type}" data-status="${newAttendee.Status}" data-email="${newAttendee.Email}" data-accountname="${newAttendee.AccountName}" >
                         <div>
                            <h6 class="mb-0 font-weight-bold ${statusClass} ${suspendedUser}"  >${nameHtml}</h6>
                            <p class="small mb-0 mt-0 text-secondary d-inline-block">${emailHtml}</p><p class="small mb-0 mt-0 text-secondary d-inline-block pl-2 pr-2">|</p>${accountHtml}
                        </div>
                        </li>`;
        return listItem;
    }

    this.BuildTimeField = function (field, container) {
        if (self.Data.IsAllDay) {
            let activityData = Object.assign({}, self.Data);
            //field.Schema.UIDataType = "date"
            activityData.StartDate = moment.tz(activityData.StartDate, 'YYYY-MM-DDTHH:mm:ss', activityData.Timezone).format();
            activityData.EndDate = moment.tz(activityData.EndDate, 'YYYY-MM-DDTHH:mm:ss', activityData.Timezone).format();
            _page.BuildField({ field: field, container: container, data: activityData, readOnly: true, removeEmpty: true });
        }
        else
            _page.BuildField({ field: field, container: container, data: self.Data, readOnly: true, removeEmpty: true });
    }

    this.Build = function () {
        let title = '';
        formModal.find('.modal-title').html(this.PageData.DisplayName);
        if (this.PageData.Fields != undefined && this.PageData.Fields.length > 0) {
            let frm = formModal.find('.modal-body').append('<div class="row"></div>').find('.row');
            for (let i = 0; i < this.PageData.Fields.length; i++) {
                let field = this.PageData.Fields[i];
                if (field.Schema == undefined || field.Name == "OwnerID")
                    continue;

                if (field.Name == "SeeAttendeeList" || field.Name == "InviteAttendee")
                    continue;

                if (field.IsPrivate && !param.pending) {
                    if (self.CurrentUserData != undefined)
                        _page.BuildField({ field: field, container: frm, data: self.CurrentUserData, readOnly: true, removeEmpty: true });
                }
                else if (field.Name == "IsAllDay") {
                    if (self.Data.IsAllDay)
                        _page.BuildField({ field: field, container: frm, data: self.Data, readOnly: true, removeEmpty: true });
                    else
                        continue;
                }
                else if (field.Name == "StartDate" || field.Name == "EndDate")
                    this.BuildTimeField(field, frm);
                else if (field.Name == "Attendees")
                    this.BuildAttendeesField(frm);
                else
                    _page.BuildField({ field: field, container: frm, data: this.Data, readOnly: true, removeEmpty: true });
                if (field.IsPageTitle) {
                    let val = _controls.GetDisplayValue({ field: field, data: self.Data });
                    title += val + ' ';
                }
            }
        }

        if (this.PageData.Groups != undefined) {
            for (let g = 0; g < this.PageData.Groups.length; g++) {
                let group = this.PageData.Groups[g];
                let pageGroup = $('<div><h6 class="mb-2 mt-3">' + group.DisplayName + '</h6><div class="row page-group"></div></div>');
                for (let i = 0; i < group.Fields.length; i++) {
                    let field = group.Fields[i];
                    if (field.Schema == undefined)
                        continue;
                    if (field.Name == "SearchAddress")
                        continue;
                    if (field.Name == "VenueName") {
                        field.ColumnWidth = 6;
                        _page.BuildField({ field: field, container: pageGroup.find('.page-group'), data: this.Data, readOnly: true, removeEmpty: true });
                        continue;
                    }

                    _page.BuildField({ field: field, container: pageGroup.find('.page-group'), data: this.Data, readOnly: true, removeEmpty: true });

                }
                let allHidden = true;
                pageGroup.find('.vld-parent').each(function () {
                    if (!($(this).hasClass('d-none')))
                        allHidden = false
                })
                if (!allHidden)
                    formModal.find('.modal-body').append(pageGroup);
            }
        }

        if (this.Data != undefined) {
            formModal.find('.modal-title').html('View ' + title);
        }

        if (self.PageData.Events != undefined && self.PageData.Events.InitForm != undefined) {
            eval(self.PageData.Events.InitForm);
        }

        if (param.pending && (self.CurrentUserData.Status == 'pending')) {
            let inviteBtn = '<button type="button" class="btn btn-success btnAcceptRequest">Accept</button>';
            inviteBtn += '<button type="button" class="btn btn-danger btnRejectRequest">Reject</button>';
            formModal.find('.modal-footer').append(inviteBtn);
            let sparam = {};
            sparam.activity = param._id;
            if (self.Data.IsRecurrence) {
                sparam.isRecurrence = true;
                if (self.Data.RecurrenceData.IsRecurrenceParent)
                    sparam.recParent = param._id;
                else
                    sparam.recParent = self.Data.RecurrenceData.RecurrenceParentId
            }

            formModal.find('.modal-footer').on('click', '.btnAcceptRequest', function () {
                sparam.response = 'accepted';

                _calendar.UpdateUserResponseModal(sparam, function (result) {
                    if (result.message == 'success') {
                        formModal.modal('hide');
                        swal('Activity added to the calendar successfully.', { icon: "success", });
                        if (param.cal != undefined) {
                            param.cal.ResetActivities();
                        } else {
                            //click on today button to reload the page
                            $(".active[data-uid='Calendar'] .todayBtn").click()
                        }
                    }
                })
            })
            formModal.find('.modal-footer').on('click', '.btnRejectRequest', function () {
                sparam.response = 'rejected';
                _calendar.UpdateUserResponseModal(sparam, function (result) {
                    if (result.message == 'success') {
                        formModal.modal('hide');
                        swal('Activity removed from the calendar.', { icon: "success", });

                        if (param.cal != undefined)
                            param.cal.ResetActivities();
                    }
                })
            })
        }
    }

    if (param._id != undefined) {
        _calendar.ReadActivityData({
            '_id': param._id,
            callback: function (result) {
                self.Data = result.data[0];
                if (result.pagedata.Fields != undefined)
                    result.pagedata.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
                self.PageData = result.pagedata;
                self.AttendeesData = result.AttendeesData
                self.CurrentUserData = result.AttendeesData.find(u => u.Id == _initData.User._id)
                self.OrganiserData = result.AttendeesData.find(u => u.IsOrganiser == true)
                self.Build();
                _common.Loading(false);
            }
        });
    }
}

//NOTE To get the user permissions before opening the activity form
_calendar.ReadActivityWithViewPermission = function (param) {
    _calendar.GetActivityViewPermissions({ activityId: param._id }, function (res) {
        if (res.message == "success") {
            if (res.permission) {
                _calendar.ReadOnlyActivityForm(param)
            } else {
                swal("User has denied the access to this activity", { icon: "info" })
            }

        } else {
            swal("Something went wrong, Please try again after some time.", { icon: "error" });
        }
    })
}

//Calendar Page 
function Calendar(page) {
    var self = this;
    this.page = page;
    this.page.PageContainer[0]._calendar = this;
    this.selectedDate = new Date(moment(_initData.CurrentUtcTime).tz(_initData.User.MySettings.Timezone).format("YYYY-MM-DDTHH:mm:ss"));
    this.BuildNav = function () {
        _controls.DatePicker(
            {
                el: self.page.PageContainer.find('.calendar-left-panel .nav-calendar'),
                selectedDate: self.selectedDate,
                onSelect: function (selectedDate) {
                    self.selectedDate = selectedDate;
                    self.page.PageContainer.find('.ddl-calendar-view .nav-link.active').click();
                },
                onCalenderBuild: function (datePickerCtrl) {
                    _calendar.GetActivityByMonthYear(datePickerCtrl);
                    self.datePickerCtrl = datePickerCtrl;
                }
            });

        this.page.PageContainer.find('.title-icon').click(function () {
            if ($(this).hasClass('show')) {
                $(this).removeClass('show');
                self.page.PageContainer.find('.calendar-left-panel').hide('slide');
            } else {
                $(this).addClass('show');
                self.page.PageContainer.find('.calendar-left-panel').show('slide');
            }
        })

    }
    this.BuildColumn = function (currentDate, addDays) {
        var ndt = new Date(currentDate.getTime());
        var today = new Date(moment(_initData.CurrentUtcTime).tz(_initData.User.MySettings.Timezone).format("YYYY-MM-DDTHH:mm:ss"))
        var todayClass = '';
        ndt.setDate(currentDate.getDate() + addDays);
        if (moment(ndt).format('YYYY-MM-DD') == moment(today).format('YYYY-MM-DD'))
            todayClass = ' class="btn-primary rounded-circle"';
        var stDate = moment(ndt).format('YYYY-MM-DD')
        return '<div class="col text-center" data-date="' + stDate + '"><div class="week-view-head">' + moment(ndt).format('ddd') + '</div><h4 ' + todayClass + '>' + moment(ndt).format('D') + '</h4></div>';
    }

    this.BuildDayColumn = function (currentDate, user) {
        var ndt = new Date(currentDate.getTime());
        var stDate = moment(ndt).format('YYYY-MM-DD')
        let src = _common.GetImageSourceURL({ access: 'public', dataID: user.ProfilePicture })
        let img = `<img class="img-profile-calendar rounded-circle" src="${src}" onerror="this.src='images/nouser.jpg';"></img>`
        return '<div class="col text-center" data-date="' + stDate + '" data-userid="' + user._id + '">' + img + '<div class="week - view - head">' + user.FirstName + ' ' + user.LastName + '</div></div>';
    }

    this.DisplayTimeLine = function () {
        var d = moment(_initData.CurrentUtcTime).tz(_initData.User.MySettings.Timezone);
        var timelineDate = d.format("YYYY-MM-DD");
        var tmhour = d.get('hour');
        var tmmin = d.get('minute');
        var top = (tmhour * 40) + (tmmin * 40 / 60);
        self.page.PageContainer.find('.msp-current-time').remove();
        var cTimediv = $('<div class="msp-current-time"></div>');
        cTimediv.css('top', top);

        self.page.PageContainer.find('.view-scroll [data-date="' + timelineDate + '"]').append(cTimediv);
    }

    this.DisplayTimeLineDayView = function (userObjArray) {
        for (let i = 0; i < userObjArray.length; i++) {
            const userObj = userObjArray[i];
            var d = moment(_initData.CurrentUtcTime).tz(userObj.MySettings.Timezone);
            var timelineDate = d.format("YYYY-MM-DD");
            var tmhour = d.get('hour');
            var tmmin = d.get('minute');
            var top = (tmhour * 40) + (tmmin * 40 / 60);
            self.page.PageContainer.find('.msp-current-time[data-uid="' + userObj._id + '"]').remove();
            var cTimediv = $('<div data-uid="' + userObj._id + '" class="msp-current-time"></div>');
            cTimediv.css('top', top);

            self.page.PageContainer.find('.view-scroll [data-date="' + timelineDate + '"][data-userid="' + userObj._id + '"]').append(cTimediv);
        }

    }

    this.DisplayFilters = function () {
        let filterMenu = $('<div class="filter-menu"></div>');
        filterMenu.append(_calendar.eventFilterMenu);
        filterMenu.append(`<hr>`);
        filterMenu.append(_calendar.userFilterMenu);
        self.page.PageContainer.find('.calendar-left-panel .filter-items-div').append(filterMenu);

        // search in colleagues
        self.page.PageContainer.find('.txtSearchInColleagues').keyup(function () {
            let val = $(this).val();
            if (val == undefined || val == '') {
                $(this).closest('.filter-menu').find('.user-all-list label').each(function () {
                    $(this).removeClass('d-none');
                    $(this).find('.user-filter-item').removeClass('text-found');
                });
                return false;
            }
            $(this).closest('.filter-menu').find('.user-all-list label').each(function () {
                let lbl = $(this);
                val = val.toLowerCase();
                let lblText = lbl.text();
                if (lblText != undefined && lblText != '' && lblText.toLowerCase().includes(val)) {
                    lbl.find('.user-filter-item').addClass("text-found");
                } else
                    lbl.find('.user-filter-item').removeClass("text-found");
                if (lbl.find('.text-found').length > 0)
                    lbl.removeClass('d-none')
                else
                    lbl.addClass('d-none');
            });
        });
    }

    this.DayView = function () {
        var currentDate = self.selectedDate;

        self.page.PageContainer.find('.label-calendar').html(moment(currentDate).format('dddd, DD MMM YYYY'));
        var calView = '<div class="row bg-white view-row">';
        let userGMTTimezone = _common.GetGMTTimezone(_common.GetListSchemaDataFromInitData('Timezone'), _initData.User.MySettings.Timezone);
        calView += `<div class="col dummy" style="padding:6px; margin-top:10px">${userGMTTimezone}</div>`;
        calView += this.BuildColumn(currentDate, 0);
        calView += '</div>';



        var calView = '<div class="row bg-white">';
        var fromDateMoment = moment.tz(currentDate.getFullYear() + '-' + _calendar.ReturnTwoDigitDateString(currentDate.getMonth() + 1) + '-' + _calendar.ReturnTwoDigitDateString(currentDate.getDate()), "YYYY-MM-DD", _initData.User.MySettings.Timezone).format('YYYY-MM-DD HH:mm A');
        let fromDate = new Date(fromDateMoment);

        for (let i = 0; i < _calendar.userFilterInfoArray.length; i++) {
            let user = _calendar.userFilterInfoArray[i];
            console.log(user);
            calView += this.BuildDayColumn(currentDate, user);
        }

        calView += '</div>';
        calView += '<div class="view-scroll-all-day"><div class="row all-day">';
        for (let i = 0; i <= _calendar.userFilterInfoArray.length; i++) {
            if (i == 0)
                calView += '<div class="col dummy"></div>';
            else {
                let userObj = _calendar.userFilterInfoArray[i - 1];
                calView += '<div data-userid="' + userObj._id + '"  data-date="' + moment(fromDate).format('YYYY-MM-DD') + '"  class="col all-day-activity-container border-left border-bottom"></div>';
            }
        }

        calView += '</div></div>';
        calView += '<div class="view-scroll" style="position:relative;"><div class="row">';
        calView += _calendar.timeSheet;

        for (let i = 0; i < _calendar.userFilterInfoArray.length; i++) {
            let userObj = _calendar.userFilterInfoArray[i];
            let workDayObj = {};
            let isWorkDay = false;
            if (userObj.MySettings != undefined && userObj.MySettings.WorkingDays != undefined) {
                let workingDays = userObj.MySettings.WorkingDays;

                let workDay = [];
                for (let i = 0; i < workingDays.length; i++) {
                    const element = workingDays[i];
                    workDay.push(element.WorkingDay)
                }
                for (let j = 0; j < workingDays.length; j++) {
                    if (currentDate.getDay() == workingDays[j].WorkingDay) {
                        workDayObj = workingDays[j];
                        isWorkDay = true;
                    }
                }
            }
            var weekendClass = '';
            if (!isWorkDay) {
                weekendClass = " bg-gray-100";
                calView += '<div data-userid="' + userObj._id + '" data-date="' + moment(fromDate).format('YYYY-MM-DD') + '"  class="col activity-container border-left' + weekendClass + ' ">' + _calendar.grid + '</div>';
            }
            else {
                calView += '<div data-userid="' + userObj._id + '" data-date="' + moment(fromDate).format('YYYY-MM-DD') + '"  class="col activity-container border-left' + weekendClass + ' ">' + _calendar.BuildCalendarGrid(workDayObj, userObj.MySettings.Timezone) + '</div>';
            }
        }

        calView += '</div></div>';
        self.page.PageContainer.find('.view-calendar').html(calView);
        // var scrollViewHeight = $(window).height() - self.page.PageContainer.find('.view-calendar .view-scroll').offset().top;
        //self.page.PageContainer.find('.view-calendar .view-scroll').height(scrollViewHeight).scrollTop(360);
        _calendar.AdjustHeight(self);

        self.DisplayTimeLineDayView(_calendar.userFilterInfoArray);
        self.DisplayDayActivities(currentDate)
    }

    this.WeekView = function () {
        var currentDate = self.selectedDate;
        var weekCount = currentDate.getDay();
        _initData.User.MySettings = _initData.User.MySettings ? _initData.User.MySettings : { WorkingDays: [], Timezone: "Europe/London" };
        var workingDays = _initData.User.MySettings.WorkingDays;
        let workDay = [];
        for (let i = 0; i < workingDays.length; i++) {
            const element = workingDays[i];
            workDay.push(element.WorkingDay)
        }

        var calView = '<div class="row bg-white">';
        var fromDateMoment = moment.tz(currentDate.getFullYear() + '-' + _calendar.ReturnTwoDigitDateString(currentDate.getMonth() + 1) + '-' + _calendar.ReturnTwoDigitDateString(currentDate.getDate()), "YYYY-MM-DD", _initData.User.MySettings.Timezone).format('YYYY-MM-DD HH:mm A');
        let fromDate = new Date(fromDateMoment);
        fromDate.setDate(currentDate.getDate() + (-1 * weekCount));
        var toDate = new Date(currentDate.getTime());
        toDate.setDate(currentDate.getDate() + (6 - weekCount));
        self.page.PageContainer.find('.label-calendar').html(moment(fromDate).format('DD MMM YYYY') + ' - ' + moment(toDate).format('DD MMM YYYY'));
        let userGMTTimezone = _common.GetGMTTimezone(_common.GetListSchemaDataFromInitData('Timezone'), _initData.User.MySettings.Timezone);
        calView += `<div class="col dummy" style="padding:6px; margin-top:10px">${userGMTTimezone}</div>`;
        for (let i = weekCount; i > 0; i--) {
            var addDays = (-1 * i);
            calView += this.BuildColumn(currentDate, addDays);
        }
        for (let i = 0; i < 7 - weekCount; i++) {
            var addDays = i;
            calView += this.BuildColumn(currentDate, addDays);
        }
        calView += '</div>';
        calView += '<div class="view-scroll-all-day"><div class="row all-day">';
        for (let i = 0; i <= 7; i++) {
            if (i == 0)
                calView += '<div class="col dummy"></div>';
            else {
                var ndt = new Date(fromDate.getTime());
                ndt.setDate(fromDate.getDate() + i - 1);
                calView += '<div data-date="' + moment(ndt).format('YYYY-MM-DD') + '"  class="col all-day-activity-container border-left border-bottom"></div>';
            }

        }
        calView += '</div></div>';
        calView += '<div class="view-scroll" style="position:relative;"><div class="row">';
        calView += _calendar.timeSheet;
        for (let i = 0; i < 7; i++) {
            let workDayObj = {};
            let isWorkDay = false;
            for (let j = 0; j < workingDays.length; j++) {
                if (i == workingDays[j].WorkingDay) {
                    workDayObj = workingDays[j];
                    isWorkDay = true;
                }
            }
            var weekendClass = '';
            var ndt = new Date(fromDate.getTime());
            ndt.setDate(fromDate.getDate() + i);
            if (!isWorkDay) {
                weekendClass = " bg-gray-100";
                calView += '<div data-userid="' + _initData.User._id + '" data-date="' + moment(ndt).format('YYYY-MM-DD') + '"  class="col activity-container border-left' + weekendClass + ' ">' + _calendar.grid + '</div>';
            }
            else {
                calView += '<div data-userid="' + _initData.User._id + '" data-date="' + moment(ndt).format('YYYY-MM-DD') + '"  class="col activity-container border-left' + weekendClass + ' ">' + _calendar.BuildCalendarGrid(workDayObj, _initData.User.MySettings.Timezone) + '</div>';
            }
        }


        calView += '</div></div>';
        self.page.PageContainer.find('.view-calendar').html(calView);
        // var scrollViewHeight = $(window).height() - self.page.PageContainer.find('.view-calendar .view-scroll').offset().top;
        //self.page.PageContainer.find('.view-calendar .view-scroll').height(scrollViewHeight).scrollTop(360);
        _calendar.AdjustHeight(self);
        self.CalendarWeekStartDate = fromDate;
        let weekstartdate = moment.tz(moment(fromDate).hours(0).minutes(0).seconds(0).format("DD-MMM-YYYY HH:mm"), "DD-MMM-YYYY HH:mm", _initData.User.MySettings.Timezone).format();
        let weekenddate = moment.tz(weekstartdate, _initData.User.MySettings.Timezone).add(7, 'd').format();
        // let weekenddate = new Date(fromDate.getTime());
        // weekenddate.setDate(fromDate.getDate() + 7);
        self.DisplayTimeLine();
        self.DisplayWeekActivities(weekstartdate, weekenddate);
    }
    this.MonthView = function () {
        var currentDate = self.selectedDate;
        var currentYear = currentDate.getFullYear();
        var currentMonth = currentDate.getMonth();
        let monthDays = _calendar.DaysInMonth(currentMonth, currentYear);
        let fromDate = new Date(currentYear, currentMonth, 1);
        let toDate = new Date(currentYear, currentMonth, (1 + monthDays));
        self.page.PageContainer.find('.label-calendar').html(moment(currentDate).format('MMM YYYY'));
        let fromToDate = {};
        var calView = _calendar.GetMonthView(currentMonth, currentYear, undefined, fromToDate);
        self.page.PageContainer.find('.view-calendar').html(calView);
        self.page.PageContainer.find('.view-calendar .date-container-row').addClass('activity-container-row');
        self.page.PageContainer.find('.view-calendar .date-container-row .date-container').addClass('activity-date-container');
        let crdate = new Date();
        let newD = moment(crdate).format('YYYY-MM-DD');
        self.page.PageContainer.find('.view-calendar .date-container-row .col[data-date=' + newD + '] .date-container').addClass('current-month-day');
        self.BuildMonthActivityContainer();
        if (fromToDate.StartDate) {
            fromDate = moment.tz(fromToDate.StartDate, 'YYYY-MM-DD', _initData.User.MySettings.Timezone);
        } else {
            fromDate = moment.tz(moment(fromDate).format('YYYY-MM-DD HH:mm A'), 'YYYY-MM-DD HH:mm A', _initData.User.MySettings.Timezone);
        }

        if (fromToDate.EndDate) {
            toDate = moment.tz(fromToDate.EndDate, 'YYYY-MM-DD', _initData.User.MySettings.Timezone);
        } else {
            toDate = moment.tz(moment(toDate).format('YYYY-MM-DD HH:mm A'), 'YYYY-MM-DD HH:mm A', _initData.User.MySettings.Timezone);
        }
        self.DisplayMonthActivities(fromDate, toDate);
    }

    _common.GetStaticPage('/page/calendar.html', function (res) {
        self.page.PageContainer.html(res);
        _calendar.InitCalendarPage(self);
        self.page.PageContainer.find('.cal-activity-menu,.cal-rightclick-menu').html(_calendar.activityMenu);
        self.BuildNav();
        self.DisplayFilters();

        self.page.PageContainer.find('.event-filter-menu .event-filter-item').change(function () {
            let selectedFilterMenu = $(this).closest('.event-filter-menu');
            _calendar.eventFilterArray = [];
            selectedFilterMenu.find('.event-filter-item').each(function () {
                if ($(this).prop('checked'))
                    _calendar.eventFilterArray.push($(this).data('key'));
            })
            self.ResetActivities();
        });
        self.page.PageContainer.find('.user-filter-menu ').on('change', '.user-filter-item', function () {
            let selectedFilterMenu = $(this).closest('.user-all-list');
            _calendar.userFilterArray = [];
            _calendar.userFilterInfoArray = [];

            selectedFilterMenu.find('.user-filter-item').each(function () {
                if ($(this).prop('checked'))
                    _calendar.userFilterArray.push(_controls.ToEJSON("objectid", $(this).data('id')));
            })
            _calendar.GetUserSettingDetails(_calendar.userFilterArray, function (result) {
                if (result.message = "success") {
                    _calendar.userFilterInfoArray = result.data;
                    self.ResetActivities();
                }
                else {
                    self.ResetActivities();
                }
            })

        });

        self.page.PageContainer.find('.user-filter-menu ').on('click', '.reset-userFilter', function () {
            let selectedFilterMenu = $(this).closest('.user-filter-menu').find('.user-all-list');
            _calendar.userFilterArray = [];
            _calendar.userFilterInfoArray = [];
            selectedFilterMenu.find('.user-filter-item').each(function () {
                $(this).prop('checked', false);
                if ($(this).data('id') == _initData.User._id) {
                    $(this).prop('checked', true);
                    _calendar.userFilterArray.push(_controls.ToEJSON("objectid", $(this).data('id')));
                    _calendar.userFilterInfoArray.push(_initData.User)
                }
            })
            self.ResetActivities();
        })
        self.page.PageContainer.find('.ddl-calendar-view .nav-item').click(function () {
            $(this).closest('.ddl-calendar-view').find('.nav-item').removeClass('active');
            $(this).addClass('active');
            // self.ResetActivities();
            self.viewType = $(this).attr('value');
            switch (self.viewType) {
                case 'week':
                    self.WeekView();
                    break;
                case 'day':
                    self.DayView();
                    break;
                case 'month':
                    self.MonthView();
                    break;
            }
        });
        //self.page.PageContainer.find('.calendar-left-panel .reset-userFilter').click();
        self.page.PageContainer.find('.ddl-calendar-view .nav-item .active').click();
        _userprofile.Auth({ pageData: self.page.Data, container: self.page.PageContainer });

    });

    this.ResetActivities = function () {
        self.viewType = self.page.PageContainer.find('.ddl-calendar-view .nav-item.active').attr('value');;
        switch (self.viewType) {
            case 'week':
                self.WeekView();
                break;
            case 'day':
                self.DayView();
                break;
            case 'month':
                self.MonthView();
                break;
        }

        if (this.datePickerCtrl) {
            _calendar.GetActivityByMonthYear(this.datePickerCtrl);
        }
    }

    this.AttachAttributesToActivity = function (element, userAttributes) {
        for (let attr in userAttributes) {
            element.attr('data-' + attr, userAttributes[attr])
        }
        return element;
    }

    this.SetAttributesOnUserBasis = function (activity, divActivity, userId) {
        if (userId == undefined)
            userId = _initData.User._id;
        if (_calendar.CheckUserPresense(activity.AttendeesData,userId)) {

            divActivity.attr('data-inviteattendee', activity.InviteAttendee)

            if (activity.OwnerID == userId) {
                divActivity.addClass('actOwner');
                divActivity.attr('data-inviteattendee', true)
            }

            let userAttendeeDetails = _calendar.GetAttendeeDetails(activity.AttendeesData,userId)
            divActivity = self.AttachAttributesToActivity(divActivity,
                {
                    statustype: !userAttendeeDetails.StatusType ? 'busy' : userAttendeeDetails.StatusType,
                    lockActivity: !userAttendeeDetails.Lock ? false : userAttendeeDetails.Lock

                })
            if (userAttendeeDetails.Status == 'pending') {
                divActivity.addClass('pending');
            }


        }
        else {
            let otherUserIdArray = _calendar.userFilterArray.filter(u => u.$oid != userId)

            let activityAttributes = {
                statustype: null,
                lockActivity: null
            }
            for (j = 0; j < activity.AttendeesData.length; j++) {
                if (otherUserIdArray.find(u => u.$oid == activity.AttendeesData[j].Id)) {
                    currentAttendee = activity.AttendeesData[j];
                    if (currentAttendee.StatusType == 'busy') {
                        activityAttributes.statustype = 'busy'
                    }
                    if (currentAttendee.Lock) {
                        activityAttributes.lockActivity = true
                    }
                }
            }
            divActivity = self.AttachAttributesToActivity(divActivity,
                {
                    statustype: !activityAttributes.statustype ? 'busy' : activityAttributes.statustypey,
                    lockActivity: !activityAttributes.lockActivity ? false : activityAttributes.lockActivity
                })
            divActivity.addClass('other-user');

        }

        divActivity.attr('data-sequence', activity.Sequence);
        divActivity.attr('data-priority', activity.Priority);
        //_calendar.CheckAllDayOrMultipleDays(activity)
        if (activity.Priority == '3') {
            divActivity.append("<span class='allday-high-activity text-danger'>!<span>")
        }
        if (activity.Priority == '1') {
            divActivity.append("<span class='allday-low-activity text-primary'>!<span>")

        }

        if (activity.IsRecurrence) {
            if (activity.RecurrenceData.IsRecurrenceParent) {
                divActivity.attr('data-parentid', activity._id);
            }
            else {
                divActivity.attr('data-parentid', activity.RecurrenceData.RecurrenceParentId);
            }
            divActivity.addClass('recurred-activity');
        }

    }


    /******************************* */
    /********  DAY ACTIVITY  ****** */
    /***************************** */

    this.DisplayDayActivities = function (fromDate) {
        let startdate = moment.tz(moment(fromDate).hours(0).minutes(0).seconds(0).format("DD-MMM-YYYY HH:mm"), "DD-MMM-YYYY HH:mm", _initData.User.MySettings.Timezone).format();
        let toDate = moment.tz(moment(fromDate).hours(0).minutes(0).seconds(0).format("DD-MMM-YYYY HH:mm"), "DD-MMM-YYYY HH:mm", _initData.User.MySettings.Timezone).add(1, 'd').format();
        let calDataObj = {};
        calDataObj.startDate = startdate;
        calDataObj.endDate = toDate;
        //calDataObj.userArray = _calendar.userFilterArray;
        for (let i = 0; i < _calendar.userFilterArray.length; i++) {
            calDataObj.userArray = []
            calDataObj.userArray.push(_calendar.userFilterArray[i]);
            let userId = _controls.ReverseEJSON("objectid", calDataObj.userArray[0])
            _calendar.GetActivityDataByDate(calDataObj, function (activities) {
                if (activities.length > 0) {
                    let filteredActivities = _calendar.FilterActivitiesByEventFilter(activities, _calendar.eventFilterArray);
                    _calendar.SortActivitiesByStartDate(filteredActivities);
                    self.BuildDayActivitiesDisplay(filteredActivities, startdate, userId);
                }
                _calendar.AdjustHeight(self);
            });
        }
    }

    this.BuildDayActivitiesDisplay = function (activities, selectedDate, userId) {
        let alldayactarr = [];
        let singledayactarr = [];
        for (let i = 0; i < activities.length; i++) {
            if (_calendar.CheckAllDayOrMultipleDays(activities[i]))
                alldayactarr.push(activities[i]);
            else
                singledayactarr.push(activities[i]);
        }
        self.DisplayDayAllDayActivities(alldayactarr, selectedDate, userId);
        self.DisplayWeekSingleDayActivities(singledayactarr, userId);
    }

    this.DisplayDayAllDayActivities = function (alldayactarr, selectedDate, userId) {
        for (let i = 0; i < alldayactarr.length; i++) {
            const activity = alldayactarr[i];
            let userElem = ''
            let activityStartDate = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format('YYYY-MM-DD');
            let activityEndDate = moment(activity.EndDate).tz(_initData.User.MySettings.Timezone).format('YYYY-MM-DD');
            let activityStartTime = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let activityEndTime = moment(activity.EndDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let durationInHours = _calendar.CalculateActivityDurationInHours(activity);
            let currentSelDate = moment(selectedDate).format('YYYY-MM-DD');
            let getActivityTypeIcon = _common.GetActivityTypeIcon(_common.GetListSchemaDataFromInitData('ActivityType'), activity.ActivityType);
            let divActivity = $('<div class="day-cal-activity" data-toggle="tooltip" data-boundary="window"  data-placement="top"' + `title="${activity.Subject}"` + ' data-id="' + activity._id + '" data-originalstartdate="' + activity.StartDate + '" data-originalenddate="' + activity.EndDate + '" data-starttime="' + activityStartTime + '" data-endtime="' + activityEndTime + '" data-activitytype="' + activity.ActivityType + '"  data-duration="' + durationInHours + '"  data-createdby="' + activity.CreatedBy + '" data-owner="' + activity.OwnerID + '"data-statustype="' + activity.StatusType + '"><span class="activity-context-menu">' + getActivityTypeIcon + '</span>' + activity.Subject + '</div>');
            divActivity.addClass('all-day-activity');
            self.SetAttributesOnUserBasis(activity, divActivity);
            divActivity.css({ 'height': '20px', 'width': '100%', 'z-index': '3' });
            if (moment(activityStartDate).isBefore(currentSelDate)) {
                divActivity.addClass("last-split-activity")
            }
            if (moment(activityEndDate).isAfter(currentSelDate)) {
                divActivity.addClass("first-split-activity")
                divActivity.css({ width: '99.5%' })
            }
            if (userId != undefined)
                userElem = '[data-userid="' + userId + '"]'
            self.page.PageContainer.find('.view-calendar .all-day .all-day-activity-container[data-date="' + currentSelDate + '"]' + userElem).append(divActivity);
        }

    }

    /******************************* */
    /*******  WEEK ACTIVITY  ****** */
    /***************************** */


    this.DisplayWeekActivities = function (fromDate, toDate) {

        let calDataObj = {};
        calDataObj.startDate = fromDate;
        calDataObj.endDate = toDate;
        calDataObj.userArray = _calendar.userFilterArray;
        // if (_calendar.multipleUserActivities == true) {
        //     calDataObj.userArray = _calendar.userFilterArray;
        // }
        _calendar.GetActivityDataByDate(calDataObj, function (activities) {
            if (activities.length > 0) {
                let filteredActivities = _calendar.FilterActivitiesByEventFilter(activities, _calendar.eventFilterArray);
                _calendar.SortActivitiesByStartDate(filteredActivities);
                self.BuildWeekActivitiesDisplay(filteredActivities, fromDate, toDate);
            }
            _calendar.AdjustHeight(self);
        })

    }

    this.BuildWeekActivitiesDisplay = function (activities, weekstart, weekend) {
        let alldayactarr = [];
        let singledayactarr = [];
        for (let i = 0; i < activities.length; i++) {
            if (_calendar.CheckAllDayOrMultipleDays(activities[i]))
                alldayactarr.push(activities[i]);
            else
                singledayactarr.push(activities[i]);
        }
        self.DisplayWeekAllDayActivities(alldayactarr, weekstart, weekend);
        self.DisplayWeekSingleDayActivities(singledayactarr);
    }

    this.DisplayWeekAllDayActivities = function (alldayactarr, weekstart, weekend) {
        for (let i = 0; i < alldayactarr.length; i++) {
            let startdate = moment(alldayactarr[i].StartDate).tz(_initData.User.MySettings.Timezone);
            var ndt = new Date(self.CalendarWeekStartDate.getTime());
            ndt.setDate(self.CalendarWeekStartDate.getDate() + 6);
            alldayactarr[i].StartDateObj = startdate.toDate();
        }
        self.BuildWeekAllDayActivity(alldayactarr, weekstart);
        let allDayContainerHeight = self.page.PageContainer.find('.view-calendar .all-day').height();

        for (let i = 0; i < alldayactarr.length; i++) {
            const activity = alldayactarr[i];
            let activityDate = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format('YYYY-MM-DD');
            let activityStartTime = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let activityEndTime = moment(activity.EndDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let durationInHours = _calendar.CalculateActivityDurationInHours(activity);
            let getActivityTypeIcon = _common.GetActivityTypeIcon(_common.GetListSchemaDataFromInitData('ActivityType'), activity.ActivityType);
            let divActivity = $('<div ' + `data-toggle="tooltip" data-boundary="window" data-placement="top" title="${activity.Subject}"` + 'class="cal-activity ' + activity.ActivityType + '"  data-id="' + activity._id + '" data-originalstartdate="' + activity.StartDate + '" data-originalenddate="' + activity.EndDate + '" data-starttime="' + activityStartTime + '" data-endtime="' + activityEndTime + '" data-activitytype="' + activity.ActivityType + '"  data-duration="' + durationInHours + '" data-createdby="' + activity.CreatedBy + '" data-owner="' + activity.OwnerID + '" data-statustype="' + activity.StatusType + '"><span class="activity-overflow"><span class="activity-context-menu">' + getActivityTypeIcon + '</span>' + activity.Subject + '</span></div>');
            divActivity.addClass('all-day-activity');

            self.SetAttributesOnUserBasis(activity, divActivity);

            var top = 20 * activity.slot;
            if (allDayContainerHeight < top + 20) {
                allDayContainerHeight = top + 20;
            }
            if (activity.IsSplitted) {
                divActivity.addClass(activity.SplitType + '-split-activity');
                let width = (activity.IsSplittedWidth * 100) - 2;
                divActivity.css({ 'top': top, 'height': '20px', 'width': width + '%', 'z-index': '1' });
                self.page.PageContainer.find('.view-calendar .all-day .all-day-activity-container[data-date="' + moment(activity.SplittedStartDate).format('YYYY-MM-DD') + '"]').append(divActivity);
            }
            else {
                divActivity.css({ 'top': top, 'height': '20px', 'width': (activity.ActDays * 100) + '%', 'z-index': '1' });
                self.page.PageContainer.find('.view-calendar .all-day .all-day-activity-container[data-date="' + activityDate + '"]').append(divActivity);
            }
            self.page.PageContainer.find('.view-calendar .all-day .all-day-activity-container[data-date="' + activityDate + '"]').append(divActivity);
        }

        self.SetAllDayActivityHeight(allDayContainerHeight);
        self.page.PageContainer.find('.view-calendar .all-day .all-day-activity-container').droppable({

            drop: function (event, ui) {

            }
        });

    }

    this.DisplayWeekSingleDayActivities = function (singledayactarr, userId) {
        self.AdjustActivityZindex(singledayactarr);
        self.AdjustActivityWidth(singledayactarr);
        let userElemId = _initData.User._id;
        if (userId != undefined)
            userElemId = userId
        for (let i = 0; i < singledayactarr.length; i++) {
            const activity = singledayactarr[i];
            let activityDate = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format('YYYY-MM-DD');
            let activityStartTime = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let activityEndTime = moment(activity.EndDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let displayTop = _calendar.CalculateTop(activity);
            let displayHeight = _calendar.CalculateHeight(activity);
            let durationInHours = _calendar.CalculateActivityDurationInHours(activity);
            let getActivityTypeIcon = _common.GetActivityTypeIcon(_common.GetListSchemaDataFromInitData('ActivityType'), activity.ActivityType);
            let divActivity = $('<div class="cal-activity" data-toggle="tooltip" ' + `title="${activity.Subject}"` + ' data-id="' + activity._id + '" data-timeZone="' + activity.Timezone + '" data-originalstartdate="' + activity.StartDate + '" data-originalenddate="' + activity.EndDate + '" data-starttime="' + activityStartTime + '" data-endtime="' + activityEndTime + '" data-activitytype="' + activity.ActivityType + '"  data-duration="' + durationInHours + '" data-createdby="' + activity.CreatedBy + '" data-owner="' + activity.OwnerID + '" data-userid="' + userElemId + '">' + `<span class="activity-overflow"><span class="activity-context-menu">${getActivityTypeIcon}</span> ${activity.Subject}</span>` + '</div>');
            divActivity.addClass('single-day-activity');

            self.SetAttributesOnUserBasis(activity, divActivity, userElemId);
            if (activity.left == undefined) { activity.left = 0 }
            divActivity.css({ 'left': activity.left + '%', 'top': displayTop, 'height': displayHeight, 'right': activity.right + '%', 'z-index': activity.zIndex });

            self.page.PageContainer.find('.view-calendar .view-scroll .activity-container[data-date="' + activityDate + '"][data-userid="' + userElemId + '"]').append(divActivity);

        }
        self.page.PageContainer.find('.view-calendar .view-scroll .activity-container').droppable({

            drop: function (event, ui) {
                if (ui.draggable[0].dataset.userid != event.target.dataset.userid) {
                    self.ResetActivities();
                    return false;
                }
                _calendar.ConfirmMove().then((confirm) => {
                    if (confirm) {
                        let format = "YYYY-MM-DD HH:mm"
                        let endHours = (ui.draggable[0].offsetTop / 40) + parseFloat(ui.draggable[0].dataset.duration);
                        let activityType = ui.draggable[0].dataset.activitytype;
                        let startdatetime = event.target.dataset.date + ' ' + _calendar.CalculateTimeFromHours(ui.draggable[0].offsetTop / 40);
                        let startdate = moment.tz(startdatetime, format, _initData.User.MySettings.Timezone).format();
                        let enddatetime = event.target.dataset.date + ' ' + _calendar.CalculateTimeFromHours(endHours);
                        let enddate = moment.tz(enddatetime, format, _initData.User.MySettings.Timezone).format();
                        let seqIncrement = parseInt(ui.draggable[0].dataset.sequence);
                        seqIncrement = seqIncrement + 1;
                        if (enddate == "Invalid date") {
                            enddatetime = moment(startdate).add(parseFloat(ui.draggable[0].dataset.duration), "h").format(format);
                            enddate = moment.tz(enddatetime, format, _initData.User.MySettings.Timezone).format();
                        }

                        let dayStart = moment.tz(startdatetime, format, _initData.User.MySettings.Timezone);
                        dayStart.hours(0).minutes(0).seconds(0);
                        let dayEnd = moment.tz(enddatetime, format, _initData.User.MySettings.Timezone);
                        dayEnd.hours(0).minutes(0).seconds(0);
                        dayEnd.add(1, 'd');

                        let userData = {};
                        userData.ActivityID = ui.draggable[0].dataset.id;
                        userData.StartDateTime = moment(startdate).add(1, 's');
                        userData.EndDateTime = moment(enddate).subtract(1, 's');
                        userData.StartDate = dayStart;
                        userData.EndDate = dayEnd;

                        var options = {};
                        options.userData = userData;
                        options.onComplete = function (continueResponse) {
                            if (!continueResponse.Continue) {
                                self.ResetActivities();
                                if (!continueResponse.Close) {
                                    let param = {}
                                    param._id = userData.ActivityID;
                                    param.pageName = activityType;
                                    _calendar.EditActivityForm(param)
                                }
                                return false;
                            }
                            else {
                                let objdata = {};
                                objdata._id = userData.ActivityID;
                                objdata.pageName = activityType;
                                objdata.fields = { "StartDate": _controls.ToEJSON("datetime", startdate), "EndDate": _controls.ToEJSON("datetime", enddate), "Sequence": seqIncrement };

                                _common.UpdateObjectData(objdata, function (result) {
                                    if (result.message == "success") {
                                        _calendar.ReinviteAttendee({ attendeeId: [], activityId: userData.ActivityID, resetAll: true, event: 'update' },
                                            (response) => {
                                                self.ReturnData = response.UpdatedActivityData;
                                                let isAttendeeExist = false;
                                                let activityData = self.ReturnData;
                                                activityData.AttendeesData.forEach(
                                                    att => {
                                                        if ((att.Type == 'contact' || att.Type == 'user') && att.IsOrganiser == undefined) {
                                                            isAttendeeExist = true;
                                                        }
                                                    }
                                                )
                                                if (isAttendeeExist && !_calendar.CheckBackDate(self.ReturnData.StartDate)) {
                                                    swal({
                                                        title: "Updated Event Invitation",
                                                        text: "Would you like to notify the attendees?",
                                                        icon: "info",
                                                        closeOnClickOutside: false,
                                                        buttons: {
                                                            confirm: {
                                                                text: "Send",
                                                                value: true,
                                                                visible: true,
                                                                closeModal: true
                                                            },
                                                            cancel: {
                                                                text: "Don't Send",
                                                                value: null,
                                                                visible: true,
                                                                closeModal: true,
                                                            }
                                                        },
                                                        dangerMode: false,
                                                    }).then((confirm) => {
                                                        let sendEmail = false;
                                                        if (confirm) {
                                                            sendEmail = true;
                                                        }
                                                        let dataSet = ui.draggable[0].dataset;
                                                        _calendar.notificationOnDragEvent(result.data._id, { startDT: dataSet.originalstartdate, endDT: dataSet.originalenddate, timeZone: dataSet.timezone }, sendEmail);
                                                        self.ResetActivities();
                                                    })
                                                }
                                                self.ResetActivities();
                                            }
                                        )
                                    }
                                })
                            }
                        }
                        _calendar.CheckUserParticipantsAvailability(options);
                    }
                    else {
                        self.ResetActivities();
                    }
                });
            }
        });
    }

    this.BuildWeekAllDayActivity = function (actarr, weekstart) {
        let allDayObject = {};
        for (let i = 0; i < 7; i++) {
            var nDate = new Date(self.CalendarWeekStartDate.getTime());
            nDate.setDate(self.CalendarWeekStartDate.getDate() + i);
            allDayObject[moment(nDate).format('YYYY-MM-DD')] = new Array();
        }
        let weekstartmoment = moment(weekstart).tz(_initData.User.MySettings.Timezone);
        let weekendmoment = moment(weekstartmoment).add(7, 'days');
        for (let i = 0; i < actarr.length; i++) {

            let actstartmoment = moment(actarr[i].StartDate).tz(_initData.User.MySettings.Timezone)
            let actendmoment = moment(actarr[i].EndDate).tz(_initData.User.MySettings.Timezone);
            let actdate = actstartmoment;
            let actDateDayStart = moment(actstartmoment).hours(0).minutes(0).seconds(0);
            let actDateDayEnd = moment(actendmoment).hours(0).minutes(0).seconds(0);
            actarr[i].IsSplitted = false;
            if (actstartmoment < weekstartmoment && actendmoment > weekendmoment) {
                actdate = weekstartmoment;
                actarr[i].IsSplitted = true;
                actarr[i].IsSplittedWidth = 7;
                actarr[i].SplittedStartDate = actdate;
                actarr[i].SplitType = 'middle';
            } else if (actstartmoment < weekstartmoment) {
                actdate = weekstartmoment;
                actarr[i].IsSplitted = true;
                actarr[i].IsSplittedWidth = actDateDayEnd.diff(weekstartmoment, 'days') + 1;
                actarr[i].SplittedStartDate = actdate;
                actarr[i].SplitType = 'last';
            } else if (actendmoment > weekendmoment) {
                actarr[i].IsSplitted = true;
                actarr[i].IsSplittedWidth = weekendmoment.diff(actDateDayStart, 'days');
                actarr[i].SplittedStartDate = actstartmoment;
                actarr[i].SplitType = 'first';
            }

            actdate = actdate.format('YYYY-MM-DD');

            for (let j = 0; j < 100; j++) {
                let fouldSlot = false;
                for (let k = 0; k < allDayObject[actdate].length; k++) {
                    if (allDayObject[actdate][k].slot == j) {
                        fouldSlot = true;
                        break;
                    }
                }
                if (!fouldSlot) {
                    allDayObject[actdate].push({ slot: j, activity: actarr[i] });
                    actarr[i].slot = j;
                    //if splitted than instead of startdate use actdate and than use issplittedwidth instead of actdays
                    if (actarr[i].IsSplitted == true) {
                        if (actarr[i].IsSplittedWidth > 1) {
                            for (let k = 1; k < actarr[i].IsSplittedWidth; k++) {
                                let nDate = moment(actdate).add(k, 'days');
                                allDayObject[nDate.format('YYYY-MM-DD')].push({ slot: j, activity: actarr[i] });
                            }
                        }
                    }
                    else {
                        if (actarr[i].ActDays > 1) {
                            for (let k = 1; k < actarr[i].ActDays; k++) {
                                var nDate = new Date(actarr[i].StartDateObj.getTime());
                                nDate.setDate(actarr[i].StartDateObj.getDate() + k);
                                allDayObject[moment(nDate).tz(_initData.User.MySettings.Timezone).format('YYYY-MM-DD')].push({ slot: j, activity: actarr[i] });
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    this.AdjustActivityZindex = function (activities) {
        let zi = 1;
        for (let i = 0; i < activities.length; i++) {
            activities[i].zIndex = zi++;
        }
    }

    this.AdjustActivityWidth = function (activities) {
        if (activities != undefined && activities.length > 0) {
            //Defining width
            for (let i = 0; i < activities.length; i++) {
                activities[i].Width = 100;
                activities[i].left = 0;
                activities[i].right = 0;
            }
            for (let i = 0; i < activities.length; i++) {
                for (let j = i + 1; j < activities.length; j++) {
                    if ((activities[j].StartDate >= activities[i].StartDate && activities[j].StartDate < activities[i].EndDate) || (activities[j].StartDate <= activities[i].StartDate && activities[j].EndDate > activities[i].StartDate)) {
                        activities[j].prev = activities[i];

                    }
                }
            }
            for (let i = 0; i < activities.length; i++) {
                if (activities[i].prev) {
                    var jCount = { Count: 1 };
                    activities[i].jCount = jCount;
                    Calendar.RecPrevActivites(activities[i]);
                    activities[i].right = 0;

                }
            }
            for (let i = 0; i < activities.length; i++) {
                if (activities[i].prev) {
                    activities[i].left = 100 - activities[i].prev.right;
                }
            }

        }
    }

    Calendar.RecPrevActivites = function (activity) {
        let left = 0;
        if (activity.prev) {
            activity.jCount.Count++;
            activity.prev.jCount = activity.jCount;
            Calendar.RecPrevActivites(activity.prev);
            left = 100 - activity.prev.right;
            activity.left = left;
        }

        let right = 100 - (100 / activity.jCount.Count) - left;
        if (right > activity.right || activity.right == 0) {
            activity.right = right;
        }


    }

    this.SetAllDayActivityHeight = function (height) {
        self.page.PageContainer.find('.view-calendar .all-day').height(height);
        if (height > 60) {
            height = 60;
            self.page.PageContainer.find('.view-calendar .view-scroll-all-day').height(height);
        }
    }
    /******************************* */
    /******  MONTH ACTIVITY  ****** */
    /***************************** */


    this.DisplayMonthActivities = function (fromDate, toDate) {

        let calDataObj = {};
        calDataObj.startDate = fromDate;
        calDataObj.endDate = toDate;
        calDataObj.userArray = _calendar.userFilterArray;
        // if (_calendar.multipleUserActivities == true) {
        //     calDataObj.userArray = _calendar.userFilterArray;
        // }
        _calendar.GetActivityDataByDate(calDataObj, function (activities) {
            if (activities.length > 0) {
                let filteredActivities = _calendar.FilterActivitiesByEventFilter(activities, _calendar.eventFilterArray);
                _calendar.SortActivitiesByStartDate(filteredActivities);
                self.BuildMonthActivitiesDisplay(fromDate, toDate, filteredActivities);
            }
        })

    }

    this.CreatePseudoActivity = function (startdate, enddate, originalActivity) {
        let activity = jQuery.extend({}, originalActivity);
        // activity = originalActivity;
        activity.StartDate = startdate;
        activity.EndDate = enddate;
        return activity;
    }

    this.SplitActivity = function (activity, activities, maxToDate) {

        let actdate = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone);
        let totalDuration = activity.ActDays;
        while (totalDuration > 0) {
            let duration = 7 - actdate.day();
            if (duration > totalDuration) {
                duration = totalDuration;
            }
            let endate = moment(actdate).add(duration - 1, 'd');
            let nactivity = self.CreatePseudoActivity(actdate, endate, activity);
            // console.log(nactivity.StartDate.tz(_initData.User.MySettings.Timezone).format(), nactivity.EndDate.tz(_initData.User.MySettings.Timezone).format())
            nactivity.ActDays = duration;
            nactivity.Splitted = true;
            nactivity.OriginalDuration = activity.ActDays;
            nactivity.OriginalStartDate = activity.StartDate;
            nactivity.OriginalEndDate = activity.EndDate;
            if (moment(nactivity.StartDate.format()).isBefore(maxToDate)) {
                activities.push(nactivity);
            }
            actdate = moment(endate).add(1, 'd');
            totalDuration -= duration;
        }
        activity.IsSplitted = true;
    }

    this.BuildMonthActivitiesDisplay = function (fromdate, todate, activities) {
        for (let i = 0; i < activities.length; i++) {
            _calendar.CheckAllDayOrMultipleDays(activities[i]);
            let dayofAct = moment(activities[i].StartDate).tz(_initData.User.MySettings.Timezone).day();
            if ((dayofAct + activities[i].ActDays) > 7) {
                self.SplitActivity(activities[i], activities, todate);
            }
        }

        let disapactarr = [];
        for (let i = 0; i < activities.length; i++) {
            if (activities[i].IsSplitted != true)
                disapactarr.push(activities[i]);
        }
        _calendar.SortActivitiesByStartDate(disapactarr);
        _calendar.CheckAllDayOrMultipleDays(disapactarr);
        self.BuildMonthActivty(fromdate, todate, disapactarr);

        for (let i = fromdate; i < todate;) {
            let stDate = moment(i).format('YYYY-MM-DD');
            let adayactarr = [];
            for (let j = 0; j < disapactarr.length; j++) {
                if ((moment(disapactarr[j].StartDate).tz(_initData.User.MySettings.Timezone).format("YYYY-MM-DD") == stDate)) {
                    adayactarr.push(disapactarr[j])
                }
            }
            self.DisplayMonthActivitiesBlock(adayactarr);
            i = moment(i).add(1, 'd');
        }

    }

    this.DisplayMonthActivitiesBlock = function (adayactarr) {
        for (let i = 0; i < adayactarr.length; i++) {
            const activity = adayactarr[i];
            let activityDate = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format('YYYY-MM-DD');
            let activityStartTime = moment(activity.StartDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let activityEndTime = moment(activity.EndDate).tz(_initData.User.MySettings.Timezone).format("LT");
            let durationInHours = _calendar.CalculateActivityDurationInHours(activity);
            let getActivityTypeIcon = _common.GetActivityTypeIcon(_common.GetListSchemaDataFromInitData('ActivityType'), activity.ActivityType);
            let margintop = 0;
            if (activity.slot > i) {
                margintop = 20 * (activity.slot - i);
                for (let j = i + 1; j < adayactarr.length; j++) {
                    adayactarr[j].slot = adayactarr[j].slot - 1;
                }
            }
            let divActivity = '';
            if (activity.Splitted == true) {
                divActivity = $('<div class="month-cal-activity" data-placement="top" data-boundary="window" data-toggle="tooltip"' + 'title="' + activity.Subject + '"  data-id="' + activity._id + '" data-starttime="' + activityStartTime + '" data-endtime="' + activityEndTime + '" data-activitytype="' + activity.ActivityType + '"  data-durationinhours="' + durationInHours + '"  data-slot="' + activity.slot + '" data-actdays="' + activity.OriginalDuration + '" data-originalactdays="' + activity.OriginalDuration + '" data-originalstartdate="' + activity.OriginalStartDate + '" data-originalenddate="' + activity.OriginalEndDate + '" data-createdby="' + activity.CreatedBy + '" data-owner="' + activity.OwnerID + '">' + `<span class="activity-overflow"><span class="activity-context-menu">${getActivityTypeIcon}</span> ${activity.Subject}</span>` + '</div>');
                if (moment(moment.tz(activity.OriginalStartDate, "YYYY-MM-DD", _initData.User.Timezone).format("YYYY-MM-DD")).isBefore(moment.tz(activity.StartDate, "YYYY-MM-DD", _initData.User.Timezone).format("YYYY-MM-DD")))
                    divActivity.addClass('last-split-activity')
                if (moment(moment.tz(activity.OriginalEndDate, "YYYY-MM-DD", _initData.User.Timezone).format("YYYY-MM-DD")).isAfter(moment.tz(activity.EndDate, "YYYY-MM-DD", _initData.User.Timezone).format("YYYY-MM-DD")))
                    divActivity.addClass('first-split-activity')

            }
            else {
                divActivity = $('<div class="month-cal-activity"' + `title="${activity.Subject}"` + 'data-placement="top" data-boundary="window" data-toggle="tooltip" data-id="' + activity._id + '" data-timeZone="' + activity.Timezone + '" data-starttime="' + activityStartTime + '" data-endtime="' + activityEndTime + '" data-activitytype="' + activity.ActivityType + '"  data-durationinhours="' + durationInHours + '"  data-slot="' + activity.slot + '" data-actdays="' + activity.ActDays + '" data-originalactdays="' + activity.OriginalDuration + '" data-originalstartdate="' + activity.StartDate + '" data-originalenddate="' + activity.EndDate + '" data-createdby="' + activity.CreatedBy + '" data-owner="' + activity.OwnerID + '">' + `<span class="activity-overflow"><span class="activity-context-menu">${getActivityTypeIcon}</span> ${activity.Subject}</span>` + '</div>');
            }

            divActivity.attr('data-isallday', activity.IsAllDay);

            self.SetAttributesOnUserBasis(activity, divActivity);

            divActivity.css({ 'margin-top': margintop, 'height': '20px', 'width': (activity.ActDays * 100) + '%', 'z-index': '4', 'position': 'relative' });
            self.page.PageContainer.find('.view-calendar [data-date="' + activityDate + '"] .month-activity-container').append(divActivity);
        }

        self.page.PageContainer.find('.view-calendar .activity-container-row .active-days').droppable({

            drop: function (event, ui) {

                _calendar.ConfirmMove().then((confirm) => {
                    if (confirm) {
                        let startdatetime = event.target.dataset.date + ' ' + ui.draggable[0].dataset.starttime;
                        let activityType = ui.draggable[0].dataset.activitytype
                        let actduration = ui.draggable[0].dataset.originalactdays;
                        if (actduration == "undefined")
                            actduration = ui.draggable[0].dataset.actdays
                        let endDate = moment(event.target.dataset.date).add(actduration - 1, 'd').format("YYYY-MM-DD");
                        let enddatetime = endDate + ' ' + ui.draggable[0].dataset.endtime;
                        let format = "YYYY-MM-DD HH:mm A"
                        let isallday = ui.draggable[0].dataset.isallday == "true" ? true : false;
                        let seqIncrement = parseInt(ui.draggable[0].dataset.sequence);
                        seqIncrement = seqIncrement + 1;
                        let startdate = moment.tz(startdatetime, format, _initData.User.MySettings.Timezone).format();
                        let enddate = moment.tz(enddatetime, format, _initData.User.MySettings.Timezone).format();
                        if (isallday) {
                            startdate = moment.utc(startdatetime, format).format();
                            enddate = moment.utc(enddatetime, format).format();
                        }
                        let dayStart = moment.tz(startdatetime, format, _initData.User.MySettings.Timezone);
                        dayStart.hours(0).minutes(0).seconds(0);
                        let dayEnd = moment.tz(enddatetime, format, _initData.User.MySettings.Timezone);
                        dayEnd.hours(0).minutes(0).seconds(0);
                        dayEnd.add(1, 'd');

                        let userData = {};
                        userData.ActivityID = ui.draggable[0].dataset.id;
                        userData.StartDateTime = startdate;
                        userData.EndDateTime = enddate;
                        userData.StartDate = dayStart;
                        userData.EndDate = dayEnd;

                        var options = {};
                        options.userData = userData;
                        options.onComplete = function (continueResponse) {
                            if (!continueResponse.Continue) {
                                self.ResetActivities();
                                if (!continueResponse.Close) {
                                    let param = {}
                                    param._id = userData.ActivityID;
                                    param.pageName = activityType;
                                    _calendar.EditActivityForm(param)
                                }
                                return false;
                            }
                            else {
                                let objdata = {};
                                objdata._id = userData.ActivityID;
                                objdata.pageName = activityType;
                                objdata.fields = { "StartDate": _controls.ToEJSON("datetime", startdate), "EndDate": _controls.ToEJSON("datetime", enddate), "Sequence": seqIncrement };

                                _common.UpdateObjectData(objdata, function (result) {
                                    if (result.message == "success") {
                                        _calendar.ReinviteAttendee({ attendeeId: [], activityId: userData.ActivityID, resetAll: true, event: 'update' },
                                            (response) => {
                                                self.ReturnData = response.UpdatedActivityData;
                                                let isAttendeeExist = false;
                                                let activityData = self.ReturnData;
                                                activityData.AttendeesData.forEach(
                                                    att => {
                                                        if ((att.Type == 'contact' || att.Type == 'user') && att.IsOrganiser == undefined) {
                                                            isAttendeeExist = true;
                                                        }
                                                    }
                                                )
                                                if (isAttendeeExist && !_calendar.CheckBackDate(self.ReturnData.StartDate)) {
                                                    swal({
                                                        title: "Updated Event Invitation",
                                                        text: "Would you like to notify the attendees?",
                                                        icon: "info",
                                                        closeOnClickOutside: false,
                                                        buttons: {
                                                            confirm: {
                                                                text: "Send",
                                                                value: true,
                                                                visible: true,
                                                                closeModal: true
                                                            },
                                                            cancel: {
                                                                text: "Don't Send",
                                                                value: null,
                                                                visible: true,
                                                                closeModal: true,
                                                            }
                                                        },
                                                        dangerMode: false,
                                                    }).then((confirm) => {
                                                        let sendEmail = false;
                                                        if (confirm) {
                                                            sendEmail = true;
                                                        }
                                                        let dataSet = ui.draggable[0].dataset;
                                                        _calendar.notificationOnDragEvent(result.data._id, { startDT: dataSet.originalstartdate, endDT: dataSet.originalenddate, timeZone: dataSet.timezone }, sendEmail);
                                                        self.ResetActivities();
                                                    })
                                                }
                                                self.ResetActivities();
                                            }
                                        )
                                    }
                                })
                            }
                        }
                        _calendar.CheckUserParticipantsAvailability(options);
                    }
                    else {
                        self.ResetActivities();
                    }
                });



            }
        });


    }

    this.BuildMonthActivty = function (fromdate, todate, activities) {
        let monthactobj = {};
        let totalDays = moment(todate).diff(fromdate, 'd')
        let date = moment(fromdate);
        for (let i = 0; i <= totalDays; i++) {
            monthactobj[date.format("YYYY-MM-DD")] = new Array();
            date.add(1, 'd');
        }

        for (let j = 0; j < activities.length; j++) {

            let actdate = moment(activities[j].StartDate).tz(_initData.User.MySettings.Timezone).format("YYYY-MM-DD");
            // let dayOfAct = moment((activities[j].EndDate)).tz(_initData.User.MySettings.Timezone).day();
            if (moment(actdate) < fromdate) {
                actdate = moment(fromdate).format("YYYY-MM-DD");
                fromdate = moment(fromdate).subtract(1, 's')
            }
            if (moment(actdate) > fromdate) {
                for (k = 0; k < 100; k++) {
                    let fouldSlot = false;
                    for (let l = 0; l < monthactobj[actdate].length; l++) {
                        if (monthactobj[actdate][l].slot == k) {
                            fouldSlot = true;
                            break;
                        }
                    }
                    if (!fouldSlot) {
                        monthactobj[actdate].push({ slot: k, activity: activities[j] });
                        activities[j].slot = k;
                        if (activities[j].ActDays > 1) {
                            for (let l = 1; l < activities[j].ActDays; l++) {
                                let newdt = new Date(actdate);
                                newdt.setDate(newdt.getDate() + l);
                                if (monthactobj[moment(newdt).format("YYYY-MM-DD")]) {
                                    monthactobj[moment(newdt).format("YYYY-MM-DD")].push({ slot: k, activity: activities[j] });
                                }
                            }
                        }
                        // if (activities[j].IsSplitted == false) {
                        //     if (dayOfAct == 6) {
                        //         activities[j].AddIcon = $('<i class="fa fa-chevron-circle-right"></i>')
                        //     }
                        //     else   
                        // }
                        break;
                    }
                }
            }
        }
        return monthactobj;
    }

    this.BuildMonthActivityContainer = function () {
        let divContainer = "<div class='month-activity-container'></div>";
        self.page.PageContainer.find('.view-calendar .active-days').append(divContainer);
        self.page.PageContainer.find('.view-calendar .inactive-days').append(divContainer);
    }
}
