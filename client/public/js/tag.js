const _tag = {};

_tag.AddTagToObject = function (param, callback) {
    $.ajax({
        url: '/api/tag/addtagtoobject',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(param),
        success: function (res) {
            callback(res);
        },
        error: function (res) {
            callback(res);
        }
    });
}


_tag.AddToTagFormModal = function (page) {
    let formModal = _common.DynamicPopUp({ title: "Add to Tag", body: "" });

    formModal.find(".btnSave").html("Add to tag").removeClass("btnSave").addClass("btnAdd");

    let FieldSchema = {
        Name: "Tag",
        UIDataType: "multilookup", DisplayName: "Tags",
        LookupObject: "Tag",
        LookupFields: [
            "Name"
        ]
    };
    let buildFieldObj = {
        Schema: FieldSchema,
        ColumnWidth: 12,
        Name: "Tag",
        Attribute: 'data-tagObject="' + page.objectName + '"',
        Match: '{\"TagType\": \"' + page.objectName + '\"}',
        ObjectName: page.objectName,
        IsRequired: true
    }



    let params = {};
    // params.data = entityData; //To pass data in multilookup
    params.field = buildFieldObj;
    params.container = formModal.find(".modal-body");
    //User multilookup
    _page.BuildField(params);

    let override = '';
    let overrideId = "override" + (new Date()).getMilliseconds();
    let appendId = "append" + (new Date()).getMilliseconds();

    let radioBtn1 = $(` <div class="custom-tag-control custom-radio custom-control-block mb-2">
                    <input type="radio" value="override" id="${overrideId}" name="status" class="custom-control-input status-override">
                    <label class="custom-control-label" for="${overrideId}">Override</label>
                </div>`);

    let radioBtn2 = $(` <div class="custom-tag-control custom-radio custom-control-block">
                    <input type="radio" value="append" id="${appendId}" name="status" class="custom-control-input status-append">
                    <label class="custom-control-label" for="${appendId}">Append</label>
                </div>`);

    formModal.find('.modal-body').append(radioBtn1);
    formModal.find('.modal-body').append(radioBtn2);
    formModal.find('.status-append').prop('checked', true);
    formModal.find('input[type=radio]').on('click', function () {
        override = $(this).val()
    })
    formModal.find('.btnAdd').click(function () {
        let sdata = {};
        sdata.objectName = page.objectName;
        sdata.records = page.ids;
        sdata.override = override;
        params.isEjsonFormat = true;
        sdata.tags = _controls.GetCtrlValue(params);
        if (sdata.tags == null) {
            alert('Please select tags')
        }
        else if (sdata.override == null) {
            alert('Select Override Option')
        }
        else {
            _common.Loading(true);
            _tag.AddTagToObject(sdata, function (result) {
                formModal.modal('hide');
                page.callback(result);
            })
        }
    });

    formModal.modal('show')

}

_tag.GetTagData = function (tagid, tagObject, fieldName, objectName, callback) {
    $.ajax({
        url: '/api/tag/gettagdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ tagid: tagid, tagobject: tagObject, fieldname: fieldName, objectname: objectName }),
        success: function (res) {
            callback(res);
        },
        error: function (res) {
            callback(res);
        }
    });
}

_tag.DeleteTagObject = function (param) {
    let tagid = param._id;
    let tagType = param.record.TagType;
    $.ajax({
        url: '/api/tag/deletetagdata',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ tagid: tagid, tagobject: tagType }),
        success: function (res) {
            param.callback(res);
        },
        error: function (res) {
            param.callback(res);
        }
    });
}

_tag.DatagridWithTagFilter = function (param) {
    let gridparam = {};
    gridparam.pageName = param.record.TagType + 'List';
    gridparam.HeaderFilters = [{ 'Name': "Tag", 'Value': param.record.Name }]
    gridparam.NewPage = true;
    //gridparam.PageTitle = param.record.Name;
    new Page(gridparam);
}
