﻿const _dataGrid = {}
_dataGrid.DraggableColumn = undefined;
_dataGrid.startOffset = undefined;

_dataGrid.FilterConditionButtons = '<i class="fas fa-plus-square btnAddFilterItem btn btn-sm btn-outline-primary float-left"></i><i class="fas fa-folder-plus btnAddCondition btn btn-sm btn-outline-primary float-left ml-1"></i> <i class="fas fa-trash-alt btnDeleteCondition btn btn-sm btn-outline-danger float-left ml-1"></i>';

_dataGrid.GetFieldForUserPageSetting = function (field) {
    let resultFld = { Name: field.Name };
    if (field.Sequence !== undefined)
        resultFld.Sequence = field.Sequence;
    if (field.IsHide !== undefined)
        resultFld.IsHide = field.IsHide;
    if (field.Width !== undefined)
        resultFld.Width = field.Width;
    if (field.IsWrap !== undefined)
        resultFld.IsWrap = field.IsWrap;
    return resultFld;
}

_dataGrid.SetAdvanceFilterConditions = function (advanceFilterFields, filterObj, ul) {
    filterObj.Condition = ul.find('.ddlCondition:first').val();
    filterObj.Filters = new Array();
    ul.children('li').each(function () {
        if ($(this).hasClass('filter-item')) {
            let field = advanceFilterFields.find(x => x.UniqueID == $(this).find('.ddlField').val());
            if (field) {
                let item = {};
                item.Type = "Filter";
                item.Name = field.UniqueID;
                item.Operator = $(this).find('.ddlOperator').val();
                item.Value = $(this).find('.msp-filter-ctrl').val();
                if (["dropdown", "multiselect"].includes(field.Schema.UIDataType) && item.Value.length == 0)
                    item.Value = '';
                else if (["text", "textarea", "texteditor", "email", "phone", "url", "formula", "lookup", "multilookup"].includes(field.Schema.UIDataType))
                    item.Value = item.Value.trim();
                else if (field.Schema.UIDataType == 'time' && $(this).attr('data-time') && item.Value.length > 0) {
                    item.Value = $(this).attr('data-time');
                }
                filterObj.Filters.push(item);
            }
        } else {
            if ($(this).find('ul').length > 0) {
                var childfilterObj = {};
                childfilterObj.Type = "Child";
                _dataGrid.SetAdvanceFilterConditions(advanceFilterFields, childfilterObj, $(this).find('ul:first'));
                filterObj.Filters.push(childfilterObj);
            }
        }
    })
}

_dataGrid.BuildAdvanceFilterConditions = function (advanceFilterFields, container, conditionItem) {
    if (conditionItem != undefined) {
        if (conditionItem.Condition == undefined)
            conditionItem.Condition = 'AND';

        let mainUl = $('<ul class="filter-ul condition-' + conditionItem.Condition + '"><li class="filter-li"><select class="ddlCondition form-control form-control-sm w-auto"><option value="AND">AND</option><option value="OR">OR</option></select></li></ul>');
        mainUl.find('.ddlCondition').val(conditionItem.Condition);

        for (let i = 0; i < conditionItem.Filters.length; i++) {
            let li = $('<li class="filter-li"></li>');
            let filter = conditionItem.Filters[i];
            if (filter.Type == "Child") {
                _dataGrid.BuildAdvanceFilterConditions(advanceFilterFields, li, filter);
            } else {
                li.addClass('filter-item form-group p-2 bg-white rounded shadow-sm');
                li.append('<div class="row"><div class="col"><label class="small mb-0">Field</label><select class="ddlField form-control form-control-sm">' + _controls.BuildSelectOptions(advanceFilterFields, 'UniqueID', 'DisplayName') + '</select></div><div class="col"><label class="small mb-0">Operator</label><span class="operator-container"></span></div><div class="col-12"><label class="small mb-0">Value</label><span class="value-container"></span></div></div><button class="btnRemoveFilterItem close text-danger"><i class="fas fa-times"></i></button>');
                li.find('.ddlField').val(filter.Name);

                let field = _common.GetObjectByKeyValueFromList(advanceFilterFields, 'UniqueID', filter.Name);
                let valueCtrl = _dataGrid.BuildAdvanceFilterValueCtrl(field, filter.Value);
                li.find('.value-container').append(valueCtrl);
                let operatorCtrl = _dataGrid.BuildAdvanceFilterOperatorCtrl(field, filter.Operator);
                li.find('.operator-container').append(operatorCtrl);
            }
            li.appendTo(mainUl);
        }
        mainUl.append('<li class="filter-li">' + _dataGrid.FilterConditionButtons + '</li>');
        mainUl.appendTo(container);
    }
}

_dataGrid.BuildAdvanceFilterOperatorCtrl = function (field, value) {
    let ctrl = $('<select class="ddlOperator form-control form-control-sm"></select>');
    let operatorArr;

    switch (field.Schema.UIDataType) {
        case "text":
        case "textarea":
        case "texteditor":
        case "email":
        case "phone":
        case "url":
        case "formula":
        case "lookup":
        case "multilookup":
            operatorArr = _constantClient.FilterOperators.filter(function (obj) { return (["CONTAINS", "NOT_CONTAINS", "EQUAL", "NOT_EQUAL", "STARTS_WITH", "END_WITH"].includes(obj.Key)); });
            break;
        case "dropdown":
        case "multiselect":
            operatorArr = _constantClient.FilterOperators.filter(function (obj) { return (["CONTAINS", "NOT_CONTAINS"].includes(obj.Key)); });
            break;
        case "date":
        case "datetime":
            operatorArr = _constantClient.FilterOperators.filter(function (obj) { return (["EQUAL", "LESS_THAN", "GREATER_THAN", "LESS_OR_EQUAL", "GREATER_OR_EQUAL"].includes(obj.Key)); });
            break;
        case "int":
        case "decimal":
        case "date":
        case "datetime":
        case "time":
            operatorArr = _constantClient.FilterOperators.filter(function (obj) { return (["EQUAL", "NOT_EQUAL", "LESS_THAN", "GREATER_THAN", "LESS_OR_EQUAL", "GREATER_OR_EQUAL"].includes(obj.Key)); });
            break;
        case "checkbox":
            operatorArr = _constantClient.FilterOperators.filter(function (obj) { return (["EQUAL"].includes(obj.Key)); });
            break;
        default:
            operatorArr = _constantClient.FilterOperators;
            break;
    }

    let option = '';
    for (let i = 0; i < operatorArr.length; i++) {
        option += '<option value="' + operatorArr[i].Key + '">' + operatorArr[i].DisplayName + '</option>';
    }
    ctrl.html(option);
    if (value !== undefined)
        ctrl.val(value);

    return ctrl;
}

_dataGrid.BuildAdvanceFilterValueCtrl = function (field, value) {
    let fieldSchema = field.Schema;
    let cls = "msp-filter-ctrl msp-ctrl-" + fieldSchema.UIDataType;
    let ctrl = '';
    let option = '';
    switch (fieldSchema.UIDataType) {
        case "dropdown":
        case "multiselect":
            if (fieldSchema.LookupObject)
                option = _controls.BuildSelectOptions(_common.GetDBListSchemaData(fieldSchema, "schema"), 'Key', 'Value');
            else
                option = _controls.BuildSelectOptions(_common.GetListSchemaDataFromInitData(fieldSchema.ListSchemaName), 'Key', 'Value');

            ctrl = $('<div class="w-100"><select multiple="multiple" data-field="' + field.UniqueID + '" data-ui="' + fieldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" >' + option + '</select></div>');
            if (value !== undefined)
                ctrl.find('select').val(value);

            ctrl.find('select').multiselect({ nonSelectedText: '' });
            break;
        case "checkbox":
            ctrl = $('<select data-field="' + field.UniqueID + '" data-ui="' + fieldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ><option value="true">True</option><option value="false">False</option></select>');
            if (value !== undefined)
                ctrl.val(value);
            break;
        case "date":
        case "datetime":
            ctrl = $('<div class="input-group input-group-sm"><div class="input-group-prepend"><button class="btn btn-outline-primary btn-sm" tabindex="-1"><i class="fas fa-calendar-day"></i></button></div><input type="text" data-field="' + field.UniqueID + '" data-ui="' + fieldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm msp-filter-ctrl msp-ctrl-date" ></div>');
            if (value !== undefined)
                ctrl.find('input').val(value);
            break;
        default:
            ctrl = $('<input type="text" data-field="' + field.UniqueID + '" data-ui="' + fieldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" >');
            if (value !== undefined)
                ctrl.val(value);
            break;
    }
    return ctrl;
}

_dataGrid.BuildGridFilterCtrl = function (field) {
    let fldSchema = field.Schema;
    let cls = "msp-filter-ctrl msp-ctrl-" + fldSchema.UIDataType;
    let ctrl = '';
    let option = '';

    switch (fldSchema.UIDataType) {
        case "dropdown":
        case "multiselect":
            if (fldSchema.LookupObject)
                option = _controls.BuildSelectOptions(_common.GetDBListSchemaData(fldSchema, "schema"), 'Key', 'Value');
            else
                option = _controls.BuildSelectOptions(_common.GetListSchemaDataFromInitData(fldSchema.ListSchemaName), 'Key', 'Value');

            ctrl = $('<div class="w-100"><select multiple="multiple" data-field="' + field.UniqueID + '" data-ui="' + fldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" >' + option + '</select></div>');
            ctrl.find('select').multiselect({ nonSelectedText: '' });
            break;
        case "checkbox":
            ctrl = '<select data-field="' + field.UniqueID + '" data-ui="' + fldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ><option value=""></option><option value="true">True</option><option value="false">False</option></select>';
            break;
        case "date":
        case "datetime":
            ctrl = '<div class="input-group input-group-sm"><div class="input-group-prepend"><button class="btn btn-outline-primary btn-sm" tabindex="-1"><i class="fas fa-calendar-day"></i></button></div><input type="text" data-field="' + field.UniqueID + '" data-ui="' + fldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm msp-filter-ctrl msp-ctrl-date" ></div>';
            break;
        case "file":
        case "image":
        case "location":
            ctrl = '<span data-field="' + field.UniqueID + '" data-ui="' + fldSchema.UIDataType + '" class="' + cls + '" ></span>';
            break;
        case "lookup":
        case "multilookup":
            ctrl = '<input type="text" data-field="' + field.UniqueID + '" data-ui="' + fldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm msp-filter-ctrl msp-ctrl-text" >';
            break;
        default:
            ctrl = '<input type="text" data-field="' + field.UniqueID + '" data-ui="' + fldSchema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" >'
            break;
    }
    return ctrl;
}

_dataGrid.Initialize = function (container) {
    //Open detail page   
    container.find('.grid-table tbody').on("dblclick", 'tr[data-id]', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let param = {};
        param._id = $(this).data('id');
        param.grid = gridObj;
        param.record = $(this)[0].record;
        if (gridObj.PageData.ChildPage != undefined) {
            param.onSave = function () {
                gridObj.ReloadData();
            }
            param.pageName = gridObj.PageData.ChildPage;
            new Page(param);
        } else if (gridObj.PageData.Events && gridObj.PageData.Events.GridOpenFunction) {
            eval(gridObj.PageData.Events.GridOpenFunction);
        }
    });
    container.find('.grid-table').on("dblclick", 'input[type="checkbox"], .cell-edit, .row-icon', function (e) {
        e.stopPropagation();
    });

    //Add object
    container.find('.page-top-button .btn-add-object').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let param = {}
        param.pageName = gridObj.PageData.ChildPage;
        if (gridObj.PageData.AddPage != undefined)
            param.pageName = gridObj.PageData.AddPage;
        if (gridObj.param.MatchFields != undefined) {
            param.MatchFields = gridObj.param.MatchFields;
        }
        param.onSave = function () {
            gridObj.ReloadData();
        }
        new PageForm(param);
    });

    //Check uncheck in grid
    container.find('.grid-table').on("click", '.chkSelectAll', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let gridBody = $(this).closest('.grid-body');
        $(this).closest('.grid-table').find('.chkSelect').prop('checked', $(this).prop('checked'));
        if ($(this).prop('checked')) {
            $(this).closest('table.grid-table').find('tbody tr[data-id]').addClass('selected');
            if (gridObj.TotalCount > gridObj.PageSize) {
                gridBody.find('.text-right').prepend('<div class="select-all-grid"><span class="select-all-grid-text">Select all ' + gridObj.TotalCount + ' records </span><span class="clear-selection-grid d-none"> Clear Selection</span></div>')
            }
        }
        else {
            $(this).closest('table.grid-table').find('tbody tr[data-id]').removeClass('selected');
            if (gridBody.find('.select-all-grid').length != 0) {
                gridBody.find('.select-all-grid').remove();
            }
            if (gridObj.AllCheck) {
                gridObj.AllCheck = false;
                gridObj.AllIDs = null;
            }
        }
    });

    container.find('.grid-body').on("click", '.clear-selection-grid', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let gridBody = $(this).closest('.grid-body');
        gridBody.find('.grid-table .chkSelect').prop('checked', false);
        gridBody.find('.grid-table .chkSelectAll').prop('checked', false)
        gridBody.find('table.grid-table').find('tbody tr[data-id]').removeClass('selected');
        if (gridBody.find('.select-all-grid').length != 0) {
            gridBody.find('.select-all-grid').remove();
        }
        gridObj.AllCheck = false;
        gridObj.AllIDs = null;
    });

    container.find('.grid-body').on('click', '.select-all-grid-text', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let gridBody = $(this).closest('.grid-body');
        if ($(this).hasClass('selected')) {
            //$(this).removeClass('selected');
        }
        else {
            _common.ContainerLoading(true, gridBody);
            _common.GetGridDataWithoutLimit({
                'pageName': gridObj.PageData.PageName,
                'sort': gridObj.SortData,
                'headerFilters': gridObj.HeaderFilters,
                'mustMatch': gridObj.MustMatch,
                'listFilter': gridObj.SelectedListViewFilter,
                callback: function (result) {
                    if (result.message == 'success') {
                        _common.ContainerLoading(false, gridBody);
                        gridBody.find('.select-all-grid-text').html('All ' + gridObj.TotalCount + ' records selected')
                        gridBody.find('.clear-selection-grid').removeClass('d-none')
                        gridObj.AllCheck = true;
                        gridObj.AllIDs = result.data.map(a => _controls.ToEJSON('objectid', a._id));
                    } else {
                        _common.ContainerLoading(false, gridBody)
                        swal(result.message, { icon: "error", });
                    }
                }
            });
            console.log(gridObj);
            $(this).addClass('selected');
        }
    })
    //select all
    container.find('.grid-table').on("click", '.chkSelect', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        if ($(this).prop('checked')) {
            $(this).closest('tr').addClass('selected');
            if ($(this).closest('.grid-table').find('.chkSelect:not(:checked)').length == 0) {
                $(this).closest('.grid-table').find('.chkSelectAll').prop('checked', true);
            }
        } else {
            $(this).closest('tr').removeClass('selected');
            $(this).closest('.grid-table').find('.chkSelectAll').prop('checked', false);
            if ($(this).closest('.grid-body').find('.select-all-grid').length != 0)
                $(this).closest('.grid-body').find('.select-all-grid').remove();
            if (gridObj.AllCheck) {
                gridObj.AllCheck = false;
                gridObj.AllIDs = null;
            }
        }
    });

    //Edit grid
    container.find('.gird-button-list .btnEdit').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        gridObj.ReloadData();
        $(this).closest('.msp-data-grid').addClass('mode-edit');
    });
    //Canel Edit grid
    container.find('.btnCancelGrid').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        if ($(this).closest('.msp-data-grid').find('.grid-table tbody tr td.changed').length > 0) {
            swal({
                title: "You have unsaved changes",
                text: "Are you sure you want to discard these changes?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((confirm) => {
                if (confirm) {
                    $(this).closest('.msp-data-grid').removeClass('mode-edit');
                    gridObj.ReloadData();
                }
            });
        } else {
            $(this).closest('.msp-data-grid').removeClass('mode-edit');
            gridObj.ReloadData();
        }

    });
    //Save grid
    container.find('.btnSaveGrid').click(function () {
        let self = $(this);
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let sdata = { 'pageName': gridObj.PageData.PageName };
        sdata.records = [];
        $(this).closest('.msp-data-grid').find('.grid-table tbody tr').each(function () {
            let tr = $(this);
            if (tr.find('td.changed').length > 0) {
                if (_common.Validate(tr)) {
                    let record = { '_id': tr.data('id') };
                    record.fields = {};
                    tr.find('td.changed').each(function () {
                        if ($(this).index() == 0) {
                            return true; //it work as continue in for loop
                        }
                        let field = $(this)[0].field;
                        let val = _controls.GetCtrlValue({ field: field, container: $(this).find('.cell-edit'), isEjsonFormat: true });
                        _page.CheckFieldValidation(field, tr, gridObj.PageData.ObjectName, record._id);
                        record.fields[field.Name] = val;
                    });
                    sdata.records.push(record);
                }
            }
        });

        if (container.find('.has-error').length > 0) {
            container.find('.has-error:first')[0].scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
            return false;
        }

        if (sdata.records.length > 0) {
            _common.Loading(true);
            _common.UpdateGridData(sdata, function (result) {
                if (result.message == 'success') {
                    self.closest('.msp-data-grid').find('.grid-table tbody tr td.changed .msp-ctrl-file').each(function () {
                        let fileid = $(this).next().find('.file-data').data('fileid');
                        if (fileid == undefined) {
                            fileid = '';
                        }
                        _common.AfterSaveDriveUpdate($(this).closest('tr').data('id'), fileid, 'Update');
                    });

                    self.closest('.msp-data-grid').removeClass('mode-edit');
                    _common.Loading(false);
                    gridObj.ReloadData();
                    swal('Record saved successfully.', { icon: "success", });
                } else {
                    _common.Loading(false);
                    swal(result.message, { icon: "error", });
                }
            });
        } else {
            $(this).closest('.msp-data-grid').removeClass('mode-edit');
        }
    });

    //Add to tag
    container.find('.page-top-button .btn-addtag-object').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        gridObj.AddToTag();
    });

    //Grid Single delete button click 
    container.find('.grid-table tbody').on("click", '.singleDeleteBtn', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let recordObj = $(this).closest('.grid-single-deleteBtn').closest('tr')[0].record;
        let param = {};
        param._id = recordObj._id;
        param.grid = gridObj;
        param.record = recordObj;
        param.pageName = gridObj.PageData.PageName;
        param.ids = [];
        param.ids.push(param._id)
        param.callback = function (result) {
            if (result.message == 'success') {
                _common.Loading(false);
                gridObj.ReloadData();
                swal('Record deleted successfully.', { icon: "success", });
            } else {
                _common.Loading(false);
                swal(result.message, { icon: "info", });
            }
        }

        _common.ConfirmDelete().then((confirm) => {
            if (confirm) {
                _common.Loading(true);
                if (gridObj.PageData.Events && gridObj.PageData.Events.GridSingleDeleteFunction) {
                    eval(gridObj.PageData.Events.GridSingleDeleteFunction);
                }
                else {
                    _common.DeleteMultipleObjectData(param);
                }
                if (gridObj.PageData.Events != undefined) {
                    if (gridObj.PageData.Events.AfterDelete != undefined)
                        eval(gridObj.PageData.Events.AfterDelete);
                }
            }
        });
    });

    //Change Field
    container.find('.grid-table tbody').on("input change", 'input, select, textarea, div[contenteditable]', function () {
        $(this).closest('td').addClass('changed');
    });
    container.find('.grid-table tbody').on("click", '.fa-times, .msp-lookup .item, .msp-multilookup .item', function () {
        $(this).closest('td').addClass('changed');
    });

    // Search grid
    container.find('.gird-button-list .btnSearch').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        container.find('.txtSearchInGrid').val('');
        $(this).closest('.msp-data-grid').find('.grid-table .chkSelectAll, .grid-table .chkSelect').prop('checked', false);
        if ($(this).closest('.grid-body').find('.select-all-grid').length != 0)
            $(this).closest('.grid-body').find('.select-all-grid').remove();
        gridObj.HeaderFilters = [];
        let trFilter = container.find('.filter-row');
        trFilter.find('th [data-field]').each(function () {
            let field = $(this).closest('th')[0].field;
            let val = $(this).val();
            if (field.Schema.UIDataType == 'time')
                val = $(this).attr('data-time');
            else if (field.Schema.UIDataType == 'dropdown' || field.Schema.UIDataType == 'multiselect') {
                if (field.Schema.LookupObject) {
                    val = _controls.ToEJSON("multilookup", val);
                }
            }
            if (val !== undefined && val !== null && val.length > 0) {
                gridObj.HeaderFilters.push({ 'Name': field.UniqueID, 'Value': val });
            }
        });
        gridObj.PageNumber = 1;
        gridObj.ReloadData();

    });
    container.find('.grid-table thead').on("keypress", '.filter-row .msp-ctrl', function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $(this).closest('.msp-data-grid').find('.gird-button-list .btnSearch').trigger('click');
        }
    });

    //Referesh grid
    container.find('.gird-button-list .btnRefresh').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        gridObj.RefereshGrid();
    });

    //Delete from grid
    container.find('.gird-button-list .btnDelete').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        gridObj.DeleteData();
    });

    //Setting of grid
    container.find('.gird-button-list .btnOpenSettingGrid').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let gridSettingModal = _common.DynamicPopUp({ title: "Grid Setting", body: '<table class="table table-bordered"><thead></thead><tbody></tbody></table>' });
        gridSettingModal.find('.modal-footer .btnSave').before('<button type="button" class="btn btn-outline-primary btnReset">Reset</button>')
        gridSettingModal.modal('show');
        for (let i = 0; i < gridObj.PageData.Fields.length; i++) {
            let field = gridObj.PageData.Fields[i];
            if (field.IsDataOnly)
                continue;

            let dispName = _page.GetFieldDisplayName(field);
            let groupName = ''
            if (field.GroupName) {
                let group = gridObj.GetGroupDetailByName(field.GroupName);
                if (group.IsExpandCollapse) {
                    groupName = field.GroupName;
                    if (field.IsMain === true)
                        dispName += (group.IsExpand === true) ? ' <i class="fa fa-arrow-left"></i>' : ' <i class="fa fa-arrow-right"></i>';
                }
            }
            let row = $('<tr><td class="text-center cursor-move"><i class="fa fa-bars"></i></td><td data-field="' + field.Name + '">' + dispName + '</td><td>' + groupName + '</td><td class="text-center"><label class="lblChk"><input type="checkbox" class="chkHide"><span class="checkmark"></span></label></td></tr>');
            row[0].field = field;
            if (field.IsHide == undefined || field.IsHide == false)
                row.find('.chkHide').prop('checked', true);

            gridSettingModal.find('tbody').append(row);
        }
        gridSettingModal.find('thead').html('<tr class="head-row"><th style="width:30px;">Position</th><th>Column Name</th><th>Group</th><th style="width:30px;">Display</th></tr>');
        gridSettingModal.find('tbody').sortable({ axis: "y" });

        //Update setting of grid
        gridSettingModal.find('.btnSave').click(function () {
            let findCheck = true;
            let chkBoxColumn = gridSettingModal.find('.ui-sortable');
            chkBoxColumn.find('.ui-sortable-handle').each(function (e) {
                if ($(this).find('[type="checkbox"]').is(':checked')) {
                    findCheck = false;
                }
            });
            if (findCheck) {
                swal({
                    title: "Select at least one field to display",
                    icon: "warning",
                    buttons: {
                        confirm: {
                            text: "Ok",
                            value: true,
                            visible: true,
                            closeModal: true
                        }
                    },
                    dangerMode: true,
                });
            } else {
                _common.Loading(true);
                let sdata = { 'PageName': gridObj.PageData.PageName };
                sdata.Fields = [];
                let seq = 1;
                gridSettingModal.find('table tbody tr').each(function () {
                    let field = _dataGrid.GetFieldForUserPageSetting($(this)[0].field);
                    field.IsHide = !$(this).find('.chkHide').prop('checked');
                    field.Sequence = seq;
                    sdata.Fields.push(field);
                    seq++;
                });
                _user.UpdateUserPageSetting(sdata, function (result) {
                    if (result.message == 'success') {
                        for (let j = 0; j < sdata.Fields.length; j++) {
                            let userFld = sdata.Fields[j];
                            for (let k = 0; k < gridObj.PageData.Fields.length; k++) {
                                let resultFld = gridObj.PageData.Fields[k];
                                if (userFld.Name == resultFld.Name) {
                                    resultFld.Sequence = userFld.Sequence;
                                    resultFld.IsHide = userFld.IsHide;
                                    break;
                                }
                            }
                        }
                        gridObj.PageData.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
                        gridObj.RemoveSelectAll();
                        gridObj.BuildHeader();
                        gridObj.DataBind();
                        gridSettingModal.modal('hide');
                        _common.Loading(false);
                    } else {
                        _common.Loading(false);
                    }
                })
            }
        });

        //Reset setting of grid
        gridSettingModal.find('.btnReset').click(function () {
            _common.Loading(true);
            let sdata = { 'PageName': gridObj.PageData.PageName, 'Fields': [] };
            _user.UpdateUserPageSetting(sdata, function (result) {
                if (result.message == 'success') {
                    _common.GetPageDetailWithUserPage(gridObj.PageData.PageName, function (data) {
                        let pageData = data.PageData;
                        if (pageData.Groups != undefined) {
                            for (let i = 0; i < pageData.Groups.length; i++) {
                                let group = pageData.Groups[i];
                                for (let j = 0; j < group.Fields.length; j++) {
                                    let fld = group.Fields[j];
                                    fld.GroupName = group.Name;
                                    pageData.Fields.push(fld);
                                }
                            }
                        }
                        pageData.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
                        gridObj.PageData = pageData;
                        gridObj.BuildHeader();
                        gridObj.DataBind();
                        gridObj.RemoveSelectAll();
                        gridSettingModal.modal('hide');
                        _common.Loading(false);
                    })
                } else {
                    _common.Loading(false);
                }
            })
        });
    });

    //Sorting grid
    container.find('.grid-table thead').on("click", '.head-row .sort-field', function () {
        let hasAsc = $(this).hasClass('asc');
        $(this).closest('.head-row').find('.sort-field').removeClass('asc desc');
        let field = $(this).closest('th')[0].field;
        let fieldName = field.UniqueID;
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        gridObj.SortData = {};

        switch (field.Schema.UIDataType) {
            case "lookup":
            case "multilookup":
            case "dropdown":
            case "multiselect":
            case "objectid":
                fieldName += '_SearchValue';
                break;
        }

        if (hasAsc == false) {
            $(this).addClass('asc');
            gridObj.SortData[fieldName] = 1;
        } else {
            $(this).addClass('desc');
            gridObj.SortData[fieldName] = -1;

        }
        gridObj.ReloadData();
    });

    //Expand group column
    container.find('.grid-table thead').on("click", '.head-row .group-main .expand', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let groupName = $(this).closest('th').attr('group-name');
        let isExpand = false;
        if ($(this).closest('th').hasClass('default')) {
            isExpand = true;
            $(this).removeClass('fa-arrow-right').addClass('fa-arrow-left');
            $(this).closest('.msp-data-grid').find('.grid-table').find('th[group-name="' + groupName + '"],td[group-name="' + groupName + '"]').removeClass('default');
        } else {
            $(this).addClass('fa-arrow-right').removeClass('fa-arrow-left');
            $(this).closest('.msp-data-grid').find('.grid-table').find('th[group-name="' + groupName + '"],td[group-name="' + groupName + '"]').addClass('default');
        }

        //update field        
        let sdata = { 'PageName': gridObj.PageData.PageName };
        sdata.Groups = [];
        for (let i = 0; i < gridObj.PageData.Groups.length; i++) {
            let group = gridObj.PageData.Groups[i];
            if (group.Name == groupName) {
                group.IsExpand = isExpand;
            }
            sdata.Groups.push(group);
        }
        _user.UpdateUserPageSetting(sdata);
    });

    //Wrap Text
    container.find('.grid-table thead').on("click", '.head-row .text-wrap-dropdown .dropdown-item', function () {
        let col = $(this).closest('th');
        let index = col.index() + 1;
        let isWrap = ($(this).data('text') == 'wrap') ? true : false;

        $(this).closest('.text-wrap-dropdown').find('.dropdown-item').removeClass('current');
        $(this).addClass('current');
        if (isWrap) {
            col.addClass('wrap');
            $(this).closest('table').find('tbody td:nth-child(' + index + ')').addClass('wrap');
        } else {
            col.removeClass('wrap');
            $(this).closest('table').find('tbody td:nth-child(' + index + ')').removeClass('wrap');
        }
        //update field
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;

        let sdata = { 'PageName': gridObj.PageData.PageName };
        sdata.Fields = [];
        for (let i = 0; i < gridObj.PageData.Fields.length; i++) {
            let field = _dataGrid.GetFieldForUserPageSetting(gridObj.PageData.Fields[i]);
            if (field.Name == col.attr('data-field')) {
                field.IsWrap = isWrap;
                gridObj.PageData.Fields[i].IsWrap = field.IsWrap;
            }
            sdata.Fields.push(field);
        }
        _user.UpdateUserPageSetting(sdata);

    });

    //Search text in grid
    container.find('.txtSearchInGrid').keyup(function () {
        let val = $(this).val();
        if (val == undefined || val == '') {
            $(this).closest('.msp-data-grid').find('.grid-table tbody tr').removeClass('d-none');
            $(this).closest('.msp-data-grid').find('.grid-table tbody tr td').removeClass('text-found');
            return false;
        }
        $(this).closest('.msp-data-grid').find('.grid-table tbody tr').each(function () {
            let tr = $(this);
            val = val.toLowerCase();
            tr.find('td').each(function () {
                let td = $(this);
                let tdText = td.find('.cell-disp').text();
                if (tdText != undefined && tdText != '' && tdText.toLowerCase().includes(val)) {
                    td.addClass("text-found");
                } else
                    td.removeClass("text-found");
            });
            if (tr.find('td.text-found').length > 0)
                tr.removeClass('d-none')
            else
                tr.addClass('d-none');
        })
    });

    //Change column width
    container.find('.grid-table-container').on('mousedown', '.grip', function (e) {
        var th = $(this).closest('th')[0];
        _dataGrid.DraggableColumn = $(this).closest('.d-flex')[0];
        _dataGrid.startOffset = th.offsetWidth - e.pageX;
        this.style.width = th.offsetWidth + 'px';
        $(this).addClass('active');
    }).on('mousemove', function (e) {
        if (_dataGrid.DraggableColumn) {
            $(_dataGrid.DraggableColumn).find('.grip')[0].style.width = _dataGrid.startOffset + e.pageX + 'px';
        }
    }).on('mouseup', function (e) {
        if (_dataGrid.DraggableColumn) {
            let colWidth = _dataGrid.startOffset + e.pageX + 'px';
            _dataGrid.DraggableColumn.style.width = colWidth;
            $(_dataGrid.DraggableColumn).closest('th')[0].style.width = colWidth;
            $(_dataGrid.DraggableColumn).find('.grip').removeClass('active')[0].style.width = '10px';

            //update field
            let gridObj = $(_dataGrid.DraggableColumn).closest('.msp-data-grid')[0].grid;
            _dataGrid.DraggableColumn = undefined;

            let sdata = { 'PageName': gridObj.PageData.PageName };
            sdata.Fields = [];
            for (let i = 0; i < gridObj.PageData.Fields.length; i++) {
                let field = _dataGrid.GetFieldForUserPageSetting(gridObj.PageData.Fields[i]);
                let col = container.find('.grid-table tr.head-row th[data-field="' + field.Name + '"]');
                if (col != undefined) {
                    field.Width = col.width();
                    gridObj.PageData.Fields[i].Width = field.Width;
                }
                sdata.Fields.push(field);
            }
            _user.UpdateUserPageSetting(sdata);
        }
    });

    //Set default advance filter
    container.find('.table-filter .set-default-icon').click(function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let filter = gridObj.SelectedListViewFilter;
        let sdata = { 'PageName': gridObj.PageData.PageName, 'DefaultListViewFilter': filter.Name };
        _user.UpdateUserPageSetting(sdata, function (result) {
            if (result.message == 'success') {
                container.find('.table-filter .set-default-icon').addClass('d-none');
                container.find('.table-filter .default-icon').removeClass('d-none');
                container.find('.table-filter .dropdown-item.default-item').removeClass('default-item');
                container.find('.table-filter .dropdown-item[name="' + filter.Name + '"]').addClass('default-item');
                gridObj.DefaultListViewFilterName = filter.Name;
            }
        });
    });

    //Change advance filter
    container.find('.table-filter').on('click', '.dropdown-item', function () {
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let filter = $(this)[0].filter;
        let filterSection = $(this).closest('.table-filter');
        filterSection.find('.dropdown-toggle').html(filter.DisplayName);
        filterSection.find('.dropdown-item.active').removeClass('active');
        filterSection.find('.dropdown-item[name="' + filter.Name + '"]').addClass('active');
        filterSection.find('.dropdown-item.default-item').removeClass('default-item');

        if (gridObj.DefaultListViewFilterName != undefined)
            filterSection.find('.dropdown-item[name="' + gridObj.DefaultListViewFilterName + '"]').addClass('default-item');

        if (gridObj.DefaultListViewFilterName != undefined && gridObj.DefaultListViewFilterName == filter.Name) {
            filterSection.find('.set-default-icon').addClass('d-none');
            filterSection.find('.default-icon').removeClass('d-none');
        } else {
            filterSection.find('.set-default-icon').removeClass('d-none');
            filterSection.find('.default-icon').addClass('d-none');
        }
        container.find('.advc-filter-sec').removeClass('active');
        gridObj.SelectedListViewFilter = filter;
        gridObj.RefereshGrid();
    });

    //Advance Filter
    container.find('.gird-button-list .btnFilter').click(function () {
        $(this).tooltip("hide");

        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        let filterSection = container.find('.advc-filter-sec');
        let conditionSection = filterSection.find('.treeview-conditions');
        let filterAddEditSection = container.find('.list-filter-edit');
        let selectedFilter = gridObj.SelectedListViewFilter;
        let advanceFilterFields = $(this)[0].AdvanceFilterFields;

        if (advanceFilterFields === undefined) {
            advanceFilterFields = [];
            let excludeUiDataTypes = ["file", "image", "texteditor", "location", "objectid"];
            let excludeFields = [];
            for (let i = 0; i < gridObj.PageData.Fields.length; i++) {
                let field = gridObj.PageData.Fields[i];
                if (field.Schema) {
                    if (field.IsFilter === false) {
                        excludeFields.push(field.UniqueID);
                    } else if (excludeUiDataTypes.includes(field.Schema.UIDataType) === false) {
                        let obj = JSON.parse(JSON.stringify(field));
                        obj.DisplayName = _page.GetFieldDisplayName(obj);
                        advanceFilterFields.push(obj);
                        excludeFields.push(field.UniqueID);
                    }
                }
            }

            for (let i = 0; i < gridObj.PageData.ObjectSchema.Fields.length; i++) {
                let fldSchema = gridObj.PageData.ObjectSchema.Fields[i];
                if (excludeFields.includes(fldSchema.Name) === true || fldSchema.IsFilter === false || fldSchema.DisplayName == undefined || excludeUiDataTypes.includes(fldSchema.UIDataType) === true)
                    continue;
                let obj = { 'UniqueID': fldSchema.Name, 'Name': fldSchema.Name, 'DisplayName': fldSchema.DisplayName };
                obj.Schema = JSON.parse(JSON.stringify(fldSchema));
                advanceFilterFields.push(obj);
            }

            advanceFilterFields.sort(function compare(a, b) {
                if (a.DisplayName < b.DisplayName)
                    return -1;
                if (a.DisplayName > b.DisplayName)
                    return 1;
                return 0;
            });
            $(this)[0].AdvanceFilterFields = advanceFilterFields;
        }

        if (selectedFilter) {
            if (selectedFilter.AccessBy == undefined)
                selectedFilter.AccessBy = 'all';

            filterSection.addClass('active').removeClass('edit-mode');
            let filterTypeName = 'All records';
            if (selectedFilter.FilterType == 'myrecord')
                filterTypeName = 'My records';
            else if (selectedFilter.FilterType == 'recentview')
                filterTypeName = 'Recently viewed';
            filterSection.find('.selected-filter-detail .filter-name').html('<h5>' + selectedFilter.DisplayName + '</h5><div class="text-gray-600">Filter type : ' + filterTypeName + '</div>');

            if (selectedFilter.IsSystemDefine == true || selectedFilter.CreatedBy != _initData.User._id)
                filterSection.addClass('system-define-filter');
            else
                filterSection.removeClass('system-define-filter');
        }


        filterSection.off('click', '.btn-add-edit-filter');
        filterSection.on('click', '.btn-add-edit-filter', function () {
            let actionType = $(this).attr('action-type');
            filterSection.addClass('edit-mode');
            filterAddEditSection.find('.action-type-name').html('New filter');
            filterAddEditSection.find('.txt-display-name,.txt-name').val('').prop('readonly', false);
            filterAddEditSection.find('input[type="radio"][name="filtertype"][value="all"]').prop('checked', true);
            filterAddEditSection.find('input[type="radio"][name="accessby"][value="all"]').prop('checked', true).closest('.form-group').removeClass('d-none');
            conditionSection.html('<ul class="filter-ul condition-AND"><li class="filter-li"><select class="ddlCondition form-control form-control-sm w-auto"><option value="AND">AND</option><option value="OR">OR</option></select></li><li class="filter-li">' + _dataGrid.FilterConditionButtons + '</li></ul>');

            if (actionType == 'edit') {
                filterAddEditSection.find('.action-type-name').html(selectedFilter.DisplayName);
                filterAddEditSection.find('.txt-display-name').val(selectedFilter.DisplayName);
                filterAddEditSection.find('.txt-name').val(selectedFilter.Name).prop('readonly', true);
                filterAddEditSection.find('input[type="radio"][name="filtertype"][value="' + selectedFilter.FilterType + '"]').prop('checked', true);
                filterAddEditSection.find('input[type="radio"][name="accessby"][value="' + selectedFilter.AccessBy + '"]').prop('checked', true);

                if (selectedFilter.Conditions != undefined) {
                    conditionSection.html('');
                    _dataGrid.BuildAdvanceFilterConditions(advanceFilterFields, conditionSection, selectedFilter.Conditions);
                }

                if (selectedFilter.CreatedBy != _initData.User._id)
                    filterAddEditSection.find('input[type="radio"][name="accessby"]:first').closest('.form-group').addClass('d-none');
            } else if (actionType == 'clone') {
                filterAddEditSection.find('.action-type-name').html('Copy of ' + selectedFilter.DisplayName);
                filterAddEditSection.find('.txt-display-name').val('');
                filterAddEditSection.find('.txt-name').val('').prop('readonly', false);
                filterAddEditSection.find('input[type="radio"][name="filtertype"][value="' + selectedFilter.FilterType + '"]').prop('checked', true);
                filterAddEditSection.find('input[type="radio"][name="accessby"][value="' + selectedFilter.AccessBy + '"]').prop('checked', true);
                if (selectedFilter.Conditions != undefined) {
                    conditionSection.html('');
                    _dataGrid.BuildAdvanceFilterConditions(advanceFilterFields, conditionSection, selectedFilter.Conditions);
                }
            }

            filterAddEditSection.find('.txt-display-name').unbind('change');
            filterAddEditSection.find('.txt-display-name').change(function () {
                if ($(this).val() != '' && filterAddEditSection.find('.txt-name').val() == '') {
                    filterAddEditSection.find('.txt-name').val(_common.FormatForApiName($(this).val()));
                }
            });

            filterSection.off('click', '.btn-save-filter');
            filterSection.on('click', '.btn-save-filter', function () {
                if (_common.Validate(filterAddEditSection)) {
                    let filterForSave = {};
                    if (actionType == 'edit' || actionType == 'clone') {
                        filterForSave = JSON.parse(JSON.stringify(selectedFilter));
                    }
                    filterForSave.Name = filterAddEditSection.find('.txt-name').val();
                    filterForSave.DisplayName = filterAddEditSection.find('.txt-display-name').val();
                    filterForSave.FilterType = filterAddEditSection.find('input[type="radio"][name="filtertype"]:checked').val();
                    filterForSave.AccessBy = filterAddEditSection.find('input[type="radio"][name="accessby"]:checked').val();
                    filterForSave.IsSystemDefine = false;
                    filterForSave.Conditions = {};
                    _dataGrid.SetAdvanceFilterConditions(advanceFilterFields, filterForSave.Conditions, filterSection.find('.treeview-conditions ul:first'));

                    _common.Loading(true);
                    _common.SaveListViewFilter({
                        pageName: gridObj.PageData.PageName,
                        actionType: actionType,
                        filter: filterForSave,
                        callback: function (result) {
                            if (result.message == 'success') {
                                _common.Loading(false);
                                gridObj.PageData.ListViewFilters = result.list;
                                filterSection.removeClass('active');
                                filterAddEditSection.modal('hide');
                                container.find('.table-filter .dropdown-menu').html('');
                                gridObj.BuildListViewFilter(filterForSave.Name);
                                gridObj.RefereshGrid();
                            } else {
                                _common.Loading(false);
                                swal(result.message, { icon: "warning", });
                            }

                        }
                    });
                }

            });
        });

        filterSection.off('click', '.btn-cancel-filter');
        filterSection.on('click', '.btn-cancel-filter', function () {
            filterSection.removeClass('edit-mode');
        });

        filterSection.find('.btn-delete-filter').unbind('click');
        filterSection.find('.btn-delete-filter').click(function () {
            _common.ConfirmDelete({ message: 'If you delete this it will be permanently removed for all users.' }).then((confirm) => {
                if (confirm) {
                    _common.Loading(true);
                    _common.SaveListViewFilter({
                        pageName: gridObj.PageData.PageName,
                        actionType: 'delete',
                        filter: selectedFilter,
                        callback: function (result) {
                            if (result.message == 'success') {
                                _common.Loading(false);
                                gridObj.PageData.ListViewFilters = result.list;
                                filterSection.removeClass('active');
                                container.find('.table-filter .dropdown-menu').html('');
                                gridObj.BuildListViewFilter();
                                gridObj.RefereshGrid();
                            } else {
                                _common.Loading(false);
                                swal(result.message, { icon: "warning", });
                            }
                        }
                    });
                }

            });
        });

        filterSection.find('.btn-remove-filter-conditions').unbind('click');
        filterSection.find('.btn-remove-filter-conditions').click(function () {
            $(this).closest('.advc-filter-sec').find('.treeview-conditions').html('<ul class="filter-ul condition-AND"><li class="filter-li"><select class="ddlCondition form-control form-control-sm w-auto"><option value="AND">AND</option><option value="OR">OR</option></select></li><li class="filter-li">' + _dataGrid.FilterConditionButtons + '</li></ul>');
        });

        filterSection.off('click', '.btnAddFilterItem');
        filterSection.on('click', '.btnAddFilterItem', function () {
            let filterItem = $('<li class="filter-li filter-item form-group p-2 bg-white rounded shadow-sm"><div class="row"><div class="col"><label class="small mb-0">Field</label><select class="ddlField form-control form-control-sm">' + _controls.BuildSelectOptions(advanceFilterFields, 'UniqueID', 'DisplayName') + '</select></div><div class="col"><label class="small mb-0">Operator</label><span class="operator-container"></span></div><div class="col-12"><label class="small mb-0">Value</label><span class="value-container"></span></div></div><button class="btnRemoveFilterItem close text-danger"><i class="fas fa-times"></i></button></li>');
            let field = _common.GetObjectByKeyValueFromList(advanceFilterFields, 'UniqueID', filterItem.find('.ddlField').val());
            let valueCtrl = _dataGrid.BuildAdvanceFilterValueCtrl(field);
            filterItem.find('.value-container').append(valueCtrl);
            let operatorCtrl = _dataGrid.BuildAdvanceFilterOperatorCtrl(field);
            filterItem.find('.operator-container').append(operatorCtrl);
            filterItem.insertBefore($(this).closest('li'));
        });
        filterSection.off('click', '.btnAddCondition');
        filterSection.on('click', '.btnAddCondition', function () {
            var liCondition = $('<li class="filter-li"><ul class="filter-ul condition-AND"><li class="filter-li"><select class="ddlCondition form-control form-control-sm w-auto"><option value="AND">AND</option><option value="OR">OR</option></select></li><li class="filter-li">' + _dataGrid.FilterConditionButtons + '</li></ul></li>');
            liCondition.insertBefore($(this).closest('li'));
        });
        filterSection.off('change', '.ddlCondition');
        filterSection.on('change', '.ddlCondition', function () {
            $(this).closest('ul').removeClass('condition-AND condition-OR').addClass('condition-' + $(this).val())
        });
        filterSection.off('click', '.btnRemoveFilterItem');
        filterSection.on('click', '.btnRemoveFilterItem', function () {
            $(this).closest('li').remove();
        });
        filterSection.off('click', '.btnDeleteCondition');
        filterSection.on('click', '.btnDeleteCondition', function () {
            $(this).closest('ul').parent().remove();
        });

        filterSection.off('change', 'select.ddlField');
        filterSection.on('change', 'select.ddlField', function () {
            let field = _common.GetObjectByKeyValueFromList(advanceFilterFields, 'UniqueID', $(this).val());
            let valueCtrl = _dataGrid.BuildAdvanceFilterValueCtrl(field);
            $(this).closest('li.filter-item').find('.value-container').html('').append(valueCtrl);
            let operatorCtrl = _dataGrid.BuildAdvanceFilterOperatorCtrl(field);
            $(this).closest('li.filter-item').find('.operator-container').html('').append(operatorCtrl);
        });

    });
    container.find('.advc-filter-sec .close').click(function () {
        $(this).closest('.advc-filter-sec').removeClass('active');
    });

    //Paging grid
    container.find('.paging').on("click", '.pagination .page-item', function () {
        let pid = parseInt($(this).attr('pid'));
        let pager = $(this).closest('.pagination')[0].pager;
        if (pid < 1 || pid > pager.totalPages || pid == pager.currentPage) {
            return false;
        }
        let gridObj = $(this).closest('.msp-data-grid')[0].grid;
        gridObj.PageNumber = pid;
        gridObj.ReloadData();
    });

}

function DataGrid(param) {
    let self = this;
    this.param = param;
    this._id = param._id;
    this.Grid = undefined;
    this.PageData = undefined;
    this.Data = [];
    this.PageNumber = 1;
    this.PageSize = 10;
    this.TotalCount = 0;
    this.SortData = undefined;
    this.HeaderFilters = undefined;
    this.MustMatch = undefined;
    if (param.MatchFields != undefined && param.MatchFields.length > 0) {
        this.MustMatch = {};
        for (let i = 0; i < param.MatchFields.length; i++) {
            let fld = param.MatchFields[i];
            let val = fld.Value;
            if (val && fld.UIDataType != undefined)
                val = _controls.ToEJSON(fld.UIDataType, val);
            if (fld.Operator != undefined) {
                if (fld.Operator == 'IN') {
                    if (!val) { val = []; }
                    val = { '$in': val };
                }
            }
            this.MustMatch[fld.Name] = val;
        }
    }

    if (param.HeaderFilters != undefined)
        this.HeaderFilters = param.HeaderFilters

    if (_initData.User.MySettings && _initData.User.MySettings.GridRows && Number(_initData.User.MySettings.GridRows) > 0) {
        this.PageSize = Number(_initData.User.MySettings.GridRows);
    }
    this.BuildHeader = function () {
        let row = $('<tr class="head-row"><th width="20px;"><div><label class="lblChk"><input type="checkbox" class="chkSelectAll"><span class="checkmark"></span></label></div></th></tr>');
        let rowFilter = $('<tr class="filter-row hide-edit"><th></th></tr>');
        for (let i = 0; i < this.PageData.Fields.length; i++) {
            let field = this.PageData.Fields[i];
            if (field.Schema == undefined || field.IsHide || field.IsDataOnly) {
                continue;
            }

            let cls = '';
            let attr = '';
            let fldExpand = '';
            if (field.GroupName) {
                let group = this.GetGroupDetailByName(field.GroupName);
                if (group.IsExpandCollapse) {
                    attr = 'group-name="' + group.Name + '"'
                    cls = 'group'
                    if (field.IsMain === true) {
                        cls += ' group-main';
                        fldExpand = (group.IsExpand === true) ? '<i class="fa fa-arrow-left expand"></i>' : '<i class="fa fa-arrow-right expand"></i>';
                    } else {
                        cls += ' group-child';
                    }
                    if (group.IsExpand === undefined || group.IsExpand === false)
                        cls += ' default';
                }
            }
            if (field.IsWrap)
                cls += ' wrap';

            let sort = (field.IsSort == undefined || field.IsSort) ? '<div class="sort-field hide-edit"></div>' : '';
            let styleWidth = (field.Width != undefined) ? 'style="width:' + field.Width + 'px;"' : '';
            let th = $('<th data-field="' + field.Name + '" class="' + cls + '" ' + attr + ' ' + styleWidth + ' ><div class="d-flex" ' + styleWidth + ' >' + '<div class="field-text">' + _page.GetFieldDisplayName(field) + '</div><div class="field-icon">' + fldExpand + sort + '<i class="fa fa-ellipsis-v text-wrap dropdown-toggle hide-edit" data-toggle="dropdown"></i> <div class="dropdown-menu text-wrap-dropdown"><a class="dropdown-item current" data-text="clip">Clip Text</a><a class="dropdown-item" data-text="wrap">Wrap Text</a></div><div class="grip"></div></div></div></th>');
            th[0].field = field;
            row.append(th);

            let thFilter = $('<th data-field="' + field.Name + '" class="' + cls + '" ' + attr + '><div></div></th>');
            let isFilter = true;
            if (field.IsFilter !== undefined)
                isFilter = field.IsFilter;
            else if (field.Schema.IsFilter !== undefined)
                isFilter = field.Schema.IsFilter;

            if (isFilter) {
                let ctrl = _dataGrid.BuildGridFilterCtrl(field);
                thFilter.find('div').append(ctrl);
                thFilter[0].field = field;
            }
            rowFilter.append(thFilter);
        }

        if (this.PageData.EnableSingleDelete) {
            let th = $('<th class=" single-delete-th " ><div class="d-flex"><div class="field-text"> Delete </div><div class="grip"></div></div></th>');
            let thFilter = $('<th><div></div></th>');
            row.append(th);
            rowFilter.append(thFilter);
        }
        this.Grid.find('.grid-table thead').html('').append(row).append(rowFilter);
        this.Grid.find('.msp-ctrl-lookup').data('objectname', this.PageData.ObjectName);
    }

    this.DataBind = function () {
        if (this.Data.length > 0) {
            let tbody = this.Grid.find('.grid-table tbody').html('');
            for (let i = 0; i < this.Data.length; i++) {
                let record = this.Data[i];
                let _id = record['_id'];
                let row = $('<tr data-id="' + _id + '"><td><div><label class="lblChk"><input type="checkbox" class="chkSelect"><span class="checkmark"></span></label></div></td></tr>');
                row[0].record = record;
                for (let c = 0; c < this.PageData.Fields.length; c++) {
                    let field = this.PageData.Fields[c];
                    if (field.Schema == undefined || field.IsHide || field.IsDataOnly) {
                        continue;
                    }

                    let cls = '';
                    let attr = '';
                    let group;
                    if (field.GroupName) {
                        group = this.GetGroupDetailByName(field.GroupName);
                        if (group.IsExpandCollapse) {
                            attr = 'group-name="' + group.Name + '"'
                            cls = 'group'
                            if (field.IsMain === true) {
                                cls += ' group-main';
                                fldExpand = (group.IsExpand === true) ? '<i class="fa fa-arrow-left expand"></i>' : '<i class="fa fa-arrow-right expand"></i>';
                            } else {
                                cls += ' group-child';
                            }
                            if (group.IsExpand === undefined || group.IsExpand === false)
                                cls += ' default';
                        }

                    }
                    if (field.IsWrap)
                        cls += ' wrap';

                    let val = _controls.GetDisplayValue({ field: field, data: record, group: group });
                    let ctrl = _controls.BuildCtrlWithValue({ field: field, data: record, group: group });
                    let td = $('<td class="' + cls + '" ' + attr + '><div class="grid-cell" data-field="' + field.Name + '" data-type="' + field.Schema.UIDataType + '"><div class="cell-disp hide-edit">' + val + '</div><div class="cell-edit vld-parent"></div></div></td>');
                    td[0].field = field;
                    if (field.Schema.UIDataType == "file") {
                        let filedatactrl = ctrl.find('.div-file-uploading .file-data');
                        if (filedatactrl.length > 0) {
                            // td.find('.cell-disp').append('<a  href="/api/drive/downloadfilelocal/' + filedatactrl.data('fileid') + '&' + filedatactrl.data('filename') + '"><i class="fa fa-download"></i></a>');
                        }
                    }
                    let isLock = false;
                    if (field.IsLock != undefined)
                        isLock = field.IsLock;
                    else if (field.Schema.IsLock != undefined)
                        isLock = field.Schema.IsLock;

                    if (field.Schema.UIDataType == 'formula' || ["CreatedDate", "CreatedBy", "_id", "ModifiedBy", "ModifiedDate"].includes(field.Name) || (group && group.IsRealLookup))
                        isLock = true;

                    if (isLock == true)
                        td.find('.cell-edit').append(val + '<i class="fas fa-lock float-right"></i>');
                    else
                        td.find('.cell-edit').append(ctrl);

                    row.append(td);
                }

                if (this.PageData.EnableSingleDelete) {
                    let btnDelete = '<div class="singleDeleteBtn"><i class="fas fa-trash-alt"></i></div>';
                    let td = $('<td class="grid-single-deleteBtn"><div class="grid-cell" ><div class="cell-disp hide-edit">' + btnDelete + '</div></div></td>');
                    row.append(td);
                }
                tbody.append(row);
                tbody.find('.msp-ctrl-lookup').data('objectname', this.PageData.ObjectName);
            }
            if (this.Data.length > 1)
                this.Grid.find('.total-count').html(this.TotalCount + ' Items');
            else
                this.Grid.find('.total-count').html('1 Item');

            //paging
            let pager = _common.PagerService(this.TotalCount, this.PageNumber, this.PageSize);
            _common.BuildPaging(pager, this.Grid.find('.paging'));

            //Start Set Witdh
            this.Grid.find('.grid-table tr.head-row th[data-field]').each(function () {
                if ($(this).attr('style') == undefined) {
                    let colWidth = $(this).width() + 'px';
                    $(this).css('width', colWidth).find('.d-flex').css('width', colWidth);
                }
            });
            //End Set Width

            if (self.PageData.ExpandPage != undefined) {
                _common.ExpendGrid(self);
            }
        } else {
            this.Grid.find('.total-count').html('No record found');
        }
    }

    this.DeleteData = function () {
        let idsArry = [];
        if (this.AllCheck) {
            idsArry = this.AllIDs;
            console.log(idsArry);
        } else {
            this.Grid.find('.grid-table tbody .chkSelect:checked').each(function () {
                idsArry.push(_controls.ToEJSON('objectid', $(this).closest('tr').data('id')));
            });
        }
        if (idsArry.length == 0) {
            swal('Please select at least one record to delete', { icon: "info", });
            return false;
        }

        let param = {};
        param.pageName = this.PageData.PageName;
        param.ids = idsArry;
        param.callback = function (result) {
            if (result.message == 'success') {
                _common.Loading(false);
                self.ReloadData();
                swal('Record deleted successfully.', { icon: "success", });
            } else {
                _common.Loading(false);
                swal(result.message, { icon: "info", });
            }
        }

        _common.ConfirmDelete().then((confirm) => {
            if (confirm) {
                _common.Loading(true);
                _common.DeleteMultipleObjectData(param);
                if (this.PageData.Events != undefined) {
                    if (this.PageData.Events.AfterDelete != undefined)
                        eval(this.PageData.Events.AfterDelete);
                }
            }
        });
    }

    this.AddToTag = function () {
        let idsArry = [];
        if (this.AllCheck) {
            idsArry = this.AllIDs;
            console.log(idsArry);
        } else {
            this.Grid.find('.grid-table tbody .chkSelect:checked').each(function () {
                idsArry.push(_controls.ToEJSON('objectid', $(this).closest('tr').data('id')));
            });
        }
        if (idsArry.length == 0) {
            swal('Please select at least one record to be added to a tag', { icon: "info", });
            return false;
        }

        let param = {};
        param.pageName = this.PageData.PageName;
        param.objectName = this.PageData.ObjectName;
        param.ids = idsArry;
        param.callback = function (result) {
            if (result.message == 'success') {
                _common.Loading(false);
                swal('Record(s) successfully added to Tag.', { icon: "success", });
                self.ReloadData();
            } else {
                _common.Loading(false);
                swal(result.message, { icon: "info", });
            }
        }
        _tag.AddToTagFormModal(param);
    }

    this.RemoveSelectAll = function () {
        self.AllCheck = false;
        self.AllIDs = null;
        if (self.Grid.find('.grid-body .chkSelectAll').prop('checked')) {
            self.Grid.find('.grid-body .chkSelectAll').prop('checked', false);
        }
        if (self.Grid.find('.grid-body .select-all-grid').length != 0) {
            self.Grid.find('.grid-body .select-all-grid').remove();
        }
    }

    this.GetGroupDetailByName = function (name) {
        for (let i = 0; i < this.PageData.Groups.length; i++) {
            if (this.PageData.Groups[i].Name == name)
                return this.PageData.Groups[i];
        }
    }

    this.ReloadData = function () {
        _common.ContainerLoading(true, this.Grid.find('.grid-body.loading-container'));
        _common.GetGridData({
            'pageName': this.PageData.PageName,
            'sort': this.SortData,
            'headerFilters': this.HeaderFilters,
            'currentPage': this.PageNumber,
            'pageSize': this.PageSize,
            'mustMatch': this.MustMatch,
            'listFilter': this.SelectedListViewFilter,
            callback: function (result) {
                if (result && result.message == 'success') {
                    if (self.Grid.find('.grid-body .select-all-grid').length != 0) {
                        self.Grid.find('.grid-body .select-all-grid').remove();
                    }
                    self.Grid.find('.grid-table .chkSelectAll').prop('checked', false);
                    self.Grid.find('.grid-table tbody').html('');
                    self.Grid.find('.paging').html('');
                    self.Data = result.data;
                    self.TotalCount = result.totalcount;
                    self.AllCheck = false;
                    self.AllIDs = null;
                    self.DataBind();
                    if (self.Grid.find('.grid-body .chkSelectAll').prop('checked')) {
                        self.Grid.find('.grid-body .chkSelectAll').prop('checked', false);
                    }
                    _common.ContainerLoading(false, self.Grid.find('.grid-body.loading-container'));
                } else {
                    _common.ContainerLoading(false, self.Grid.find('.grid-body.loading-container'))
                    swal(result.message, { icon: "error", });
                }
            }
        });
    }

    this.RefereshGrid = function () {
        this.Grid.find('.grid-table thead').html('');
        this.Grid.find('.txtSearchInGrid').val('');
        this.PageNumber = 1;
        this.PageSize = 10;
        this.TotalCount = 0;
        this.SortData = undefined;
        this.HeaderFilters = undefined;

        if (_initData.User.MySettings && _initData.User.MySettings.GridRows && Number(_initData.User.MySettings.GridRows) > 0) {
            this.PageSize = Number(_initData.User.MySettings.GridRows);
        }
        this.BuildHeader();
        this.ReloadData();
    }

    this.CheckDefaultListViewFilter = function (filter) {
        if (this.DefaultListViewFilterName != undefined) {
            if (filter.Name == this.DefaultListViewFilterName)
                return true;
        } else if (filter.FilterType == 'all' && filter.IsSystemDefine == true)
            return true;

        return false;
    }

    this.BuildListViewFilter = function (name) {
        let listViewFilterSection = this.Grid.find('.table-filter');
        this.SelectedListViewFilter = undefined;
        if (this.PageData.ListViewFilters != undefined && this.PageData.ListViewFilters.length > 0) {
            listViewFilterSection.removeClass('d-none');
            for (let i = 0; i < this.PageData.ListViewFilters.length; i++) {
                let filter = this.PageData.ListViewFilters[i];
                if (filter.AccessBy == undefined || filter.AccessBy == 'all' || filter.CreatedBy == _initData.User._id) {
                    let filterCtrl = $('<a class="dropdown-item" href="#" name="' + filter.Name + '">' + filter.DisplayName + '</a>');
                    filterCtrl[0].filter = filter;
                    listViewFilterSection.find('.dropdown-menu').append(filterCtrl);

                    //Set Selected Filter
                    if (name != undefined) {
                        if (filter.Name == name)
                            this.SelectedListViewFilter = filter;

                        if (this.CheckDefaultListViewFilter(filter))
                            this.DefaultListViewFilterName = filter.Name;
                    } else if (this.CheckDefaultListViewFilter(filter)) {
                        this.SelectedListViewFilter = filter
                        this.DefaultListViewFilterName = filter.Name;
                    }
                }

            }

            if (!this.SelectedListViewFilter) {
                this.SelectedListViewFilter = listViewFilterSection.find('.dropdown-item:first')[0].filter;
            }
            listViewFilterSection.find('.dropdown-toggle').html(this.SelectedListViewFilter.DisplayName);
            listViewFilterSection.find('.dropdown-item[name="' + this.SelectedListViewFilter.Name + '"]').addClass('active');

            if (this.DefaultListViewFilterName) {
                listViewFilterSection.find('.dropdown-item[name="' + this.DefaultListViewFilterName + '"]').addClass('default-item');
                if (this.DefaultListViewFilterName == this.SelectedListViewFilter.Name) {
                    listViewFilterSection.find('.set-default-icon').addClass('d-none');
                    listViewFilterSection.find('.default-icon').removeClass('d-none');
                }
                else {
                    listViewFilterSection.find('.set-default-icon').removeClass('d-none');
                    listViewFilterSection.find('.default-icon').addClass('d-none');
                }
            }
        } else {
            this.Grid.find('.gird-button-list .btnFilter').remove();
        }
    }

    _common.GetStaticPage('/page/grid.html', function (pageresult) {
        self.Grid = $(pageresult);
        self.param.PageContainer.html('').append(self.Grid);
        self.Grid[0].grid = self;
        _dataGrid.Initialize(self.Grid);

        _common.GetPageDetailWithUserPage(self.param.Data.PageName, function (data) {
            let pageData = data.PageData;
            let userPageData = data.UserPageData;
            let profileData = data.ProfileData;
            if (pageData.Groups != undefined) {
                for (let i = 0; i < pageData.Groups.length; i++) {
                    let group = pageData.Groups[i]
                    for (let j = 0; j < group.Fields.length; j++) {
                        let fld = group.Fields[j];
                        fld.GroupName = group.Name;
                        pageData.Fields.push(fld);
                    }
                }
            }
            if (userPageData != undefined) {
                self.DefaultListViewFilterName = userPageData.DefaultListViewFilter;

                if (userPageData.Fields != undefined && userPageData.Fields.length > 0) {
                    for (let j = 0; j < userPageData.Fields.length; j++) {
                        let userFld = userPageData.Fields[j];
                        for (let k = 0; k < pageData.Fields.length; k++) {
                            let resultFld = pageData.Fields[k];
                            if (resultFld.Name == userFld.Name) {
                                resultFld.Sequence = userFld.Sequence;
                                resultFld.IsHide = userFld.IsHide;
                                resultFld.Width = userFld.Width;
                                resultFld.IsWrap = userFld.IsWrap;
                                break;
                            }
                        }
                    }
                }
                if (userPageData.Groups != undefined && userPageData.Groups.length > 0) {
                    for (let j = 0; j < userPageData.Groups.length; j++) {
                        let userGroup = userPageData.Groups[j];
                        for (let k = 0; k < pageData.Groups.length; k++) {
                            let resultGroup = pageData.Groups[k];
                            if (resultGroup.Name == userGroup.Name) {
                                resultGroup.IsExpand = userGroup.IsExpand;
                                break;
                            }
                        }
                    }
                }
            }

            if (profileData && profileData.Fields) {
                for (let j = 0; j < profileData.Fields.length; j++) {
                    let obj = profileData.Fields[j];
                    if (obj.ReadOnly) {
                        for (let k = 0; k < pageData.Fields.length; k++) {
                            let fld = pageData.Fields[k];
                            if (fld.Name == obj.Name) {
                                fld.IsLock = true;
                                break;
                            }
                        }
                    }

                }
            }

            pageData.Fields.sort(function (a, b) { return a.Sequence - b.Sequence });
            self.PageData = pageData;
            if (pageData.DisplayName != undefined)
                self.Grid.find('.page-name').html(pageData.DisplayName);
            self.Grid.addClass(pageData.PageName);
            self.BuildListViewFilter();
            self.BuildHeader();
            self.ReloadData();

            if (self.PageData.AddButtonText != undefined) {
                self.Grid.find('.btn-add-object').html(self.PageData.AddButtonText);
            }
            if (self.PageData.HasAddButton === false) {
                self.Grid.find('.btn-add-object').remove();
            }
            if (self.PageData.HasEditButton === false) {
                self.Grid.find('.gird-button-list .btnEdit').remove();
            }
            if (self.PageData.HasDeleteButton === false) {
                self.Grid.find('.gird-button-list .btnDelete ').remove();
            }
            if (self.PageData.ObjectSchema.IsTagObject != undefined) {
                if (self.PageData.ObjectSchema.IsTagObject) {
                    self.Grid.find('.btn-addtag-object').removeClass('d-none');
                }
            }
            else {
                self.Grid.find('.btn-addtag-object').remove();
            }

            if (self.PageData.Events != undefined && self.PageData.Events.Init)
                eval(self.PageData.Events.Init);
            if (param.callback != undefined)
                param.callback(self);


            if (self.PageData.PageButtons != undefined) {
                for (let i = 0; i < self.PageData.PageButtons.length; i++) {
                    let obj = self.PageData.PageButtons[i];
                    if (obj.HTML) {
                        let btn = $(obj.HTML);
                        if (btn.find('.page-open').length > 0) {
                            btn.find('.page-open').each(function () {
                                this.onSave = function () {
                                    self.ReloadData();
                                }
                                if (self.param.MatchFields != undefined) {
                                    this.MatchFields = self.param.MatchFields;
                                }
                            });
                        }
                        self.Grid.find('.page-top-button').prepend(btn);
                    } else {
                        let hasfn = (obj.Function != undefined) ? 'data-fn=' + obj.Function : '';
                        let dataProps = '';
                        if (obj.DataRestriction) {
                            dataProps = ' data-restriction="' + obj.DataRestriction + '"';
                        }
                        let btn = '<button class="' + obj.Class + '" ' + hasfn + ' ' + dataProps + '>' + obj.DisplayName + '</button>';
                        self.Grid.find('.page-top-button').prepend(btn);
                    }

                }
                self.Grid.find('.page-top-button').on('click', 'button[data-fn]', function () {
                    self.CurrentClickElement = $(this);
                    eval($(this).data('fn'));
                });
            }
            _userprofile.Auth({ pageData: self.PageData, container: self.Grid });
        });
    });
}