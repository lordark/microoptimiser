
const _shop = {};

// _shop.ShopPriceModelOpen = function (page) {
//     console.log(page);
//     // let serviceID = page.grid.Grid.closest('.nav-tabs').find('')
//     _shop.GetEmployeePriceDetails(employeeIDs, function (result) {

//     })
// }



_shop.InitShopProductServiceDetail = function (page) {
    var self = this;

    let locationCtrl = page.PageContainer.find('[data-field="LocationID"]');
    let shopCtrl = page.PageContainer.find('[data-field="ShopID"]');
    let machineCtrl = page.PageContainer.find('[data-field="Machines"]');
    let roomCtrl = page.PageContainer.find('[data-field="Rooms"]');
    let employeeCtrl = page.PageContainer.find('[data-field="EmployeeID"]');

    shopCtrl.on('click', '.lookup-item .fa-times', function () {
        if (locationCtrl.find('.lookup-item') != undefined) {
            locationCtrl.find('.lookup-item').remove();
            locationCtrl.attr('contenteditable', 'true');
        }
        self.ClearLookupData();
    })

    locationCtrl.on('click', '.lookup-item .fa-times', function () {
        self.ClearLookupData();
    })

    locationCtrl.on('onLookupChange', function (e, el) {
        let newAttId = $(this).find(".lookup-item").attr("data-id");
        lookupField = this;
        let ctrllookup = $(lookupField).find('.msp-ctrl-lookup');
        if (newAttId != undefined && newAttId != null) {
            _common.GetObjectData({
                "pageName": "LocationDetail",
                "_id": newAttId,
                "callback": function (res) {
                    let data = res.data;
                    console.log(data);
                    console.log(res.data.Program_LookupData);
                    let machineDetail = data.Machines_LookupData;
                    let roomDetail = data.Rooms_LookupData;
                    let employeeDetail = data.Employee_LookupData;
                    if (machineDetail && machineDetail.length > 0) {
                        for (let i = 0; i < machineDetail.length; i++) {
                            machineCtrl.append('<div tabindex="0" class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1 " contenteditable="false" data-id="' + machineDetail[i]._id + '">' + machineDetail[i].Name + '<i class="fas fa-times pl-2"></i></div>');
                        }
                    }
                    if (roomDetail && roomDetail.length > 0) {
                        for (let i = 0; i < roomDetail.length; i++) {
                            roomCtrl.append('<div tabindex="0" class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1 " contenteditable="false" data-id="' + roomDetail[i]._id + '">' + roomDetail[i].Name + '<i class="fas fa-times pl-2"></i></div>');
                        }
                    }
                    if (employeeDetail && employeeDetail.length > 0) {
                        for (let i = 0; i < employeeDetail.length; i++) {
                            employeeCtrl.append('<div tabindex="0" class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1 " contenteditable="false" data-id="' + employeeDetail[i]._id + '">' + employeeDetail[i].FirstName + ' ' + employeeDetail[i].LastName + '<i class="fas fa-times pl-2"></i></div>');
                        }
                    }
                }
            })
        }
        else {
            self.ClearLookupData();
        }
    })

    self.ClearLookupData = function () {
        if (machineCtrl.find('.lookup-item') != undefined) {
            machineCtrl.find('.lookup-item').remove();
            machineCtrl.attr('contenteditable', 'true');
        }
        if (roomCtrl.find('.lookup-item') != undefined) {
            roomCtrl.find('.lookup-item').remove();
            roomCtrl.attr('contenteditable', 'true');
        }
        if (employeeCtrl.find('.lookup-item') != undefined) {
            employeeCtrl.find('.lookup-item').remove();
            employeeCtrl.attr('contenteditable', 'true');
        }
    }

}

_shop.InitServiceTimeDetail = function (page) {
    var self = this;

    let serviceCtrl = page.PageContainer.find('[data-field="ServiceID"]');
    let employeeCtrl = page.PageContainer.find('[data-field="EmployeeID"]');


    employeeCtrl.on('onLookupChange', function (e, el) {
        let newAttId = $(this).find(".lookup-item").attr("data-id");
        let serviceId = serviceCtrl.find(".lookup-item").attr("data-id");
        if (newAttId != undefined && newAttId != null && serviceId != null) {
            _common.GetObjectData({
                "pageName": "ShopProductServiceDetail",
                "_id": serviceId,
                "callback": function (res) {
                    let data = res.data;
                    if (data.EmployeeID.indexOf(newAttId) < 0) {
                        swal('Selected employee does not provide the service', { icon: "error" });
                        if (employeeCtrl.find('.lookup-item') != undefined) {
                            employeeCtrl.find('.lookup-item').remove();
                            employeeCtrl.attr('contenteditable', 'true');
                        }
                    }
                }
            })
        }
        else {
            swal('Something went wrong. Please try again later.', { icon: "error" });
            if (employeeCtrl.find('.lookup-item') != undefined) {
                employeeCtrl.find('.lookup-item').remove();
                employeeCtrl.attr('contenteditable', 'true').focus();
            }
        }
    })

}

_shop.InitShopProductProductDetail = function (data) {
}

_shop.InitEmployeeTimingPage = function (data) {
    let self = this;
    let locationID = data.MatchFields.find(x => x.Name == "LocationID").Value;

    data.PageContainer.find('.btn-add-employeeTiming-object').on('click', function (params) {

        data.InitCallback = function (result) {
            _common.Loading(false);
            if (result.message == "success") {
                result.formModal.modal('hide');
                swal('Record added successfully.', { icon: "success", });
                setTimeout(function () {
                    self.ResetEmployeeTimings();
                }, 1000);
            }
            else if (result.message == "conflict") {
                if (result.data.length > 0) {
                    for (let i = 0; i < result.data.length; i++) {
                        _shop.ValidationMsg(result.formModal.find('.user-working-days .daytiming[data-week=' + result.data[i].WorkingDay + ']'), "Conflict in Timing");
                    }
                }
            }
            else {
                result.formModal.modal('hide');
                swal('Something went wrong. Please try again.', { icon: "error" })
            }

        }
        self.AddEmployeeTimingFormModal(data);
    })


    self.ResetEmployeeTimings = function () {
        data.PageContainer.find('.user-details').html('');
        _common.ContainerLoading(true, data.PageContainer);
        _shop.GetEmployeeTimingDetails(locationID, function (result) {
            if (result.message == "success") {
                if (result.data != undefined && result.data.length > 0) {
                    let locationRecords = result.data.filter(item => item.EmployeeID == undefined);
                    let locationInfo = locationRecords[0].LocationID_LookupData;
                    self.DisplayTiming(locationInfo.LocationCode, locationID, "location", locationRecords)
                    let allEmpIDs = result.data.map(item => item.EmployeeID);
                    let empIDs = [...new Set(allEmpIDs)];
                    empIDs.splice(empIDs.indexOf(undefined), 1);
                    if (empIDs.length > 0)
                        data.PageContainer.find('.user-details').append('<tr class="head-row text-primary" style="text-align: center;"><th><div class="field-text"> Staff</div></th></tr>')

                    for (let i = 0; i < empIDs.length; i++) {
                        if (empIDs[i] != undefined) {
                            let records = result.data.filter(emp => emp.EmployeeID == empIDs[i]);
                            let empInfo = records[0].EmployeeID_LookupData;
                            self.DisplayTiming(empInfo.FirstName + ' ' + empInfo.LastName, empIDs[i], "employee", records)
                        }
                    }
                }

            }
            else {
                swal('No timings found.', { icon: "info" })
            }
            _common.ContainerLoading(false, data.PageContainer);
        })
    }

    self.DisplayTiming = function (Name, recordID, recordType, records) {
        let tableRow = $(`<tr class="user-timing-info-row" data-recordid="${recordID}" data-recordtype="${recordType}"></tr>`);

        tableRow.append(`<td data-recordid='${recordID}'>${Name}</td>`);
        for (let i = 0; i < 7; i++) {
            let record = records.find(x => x.WorkingDay == i);
            let td = $(`<td class="user-timings" data-id="${record._id}" data-weekday=${i}></td>`);
            td[0].record = record;
            if (record.IsWorking) {
                let startTime = moment(record.StartTime, 'HH:mm').format(_initData.Company.Setting.CalendarTimeFormat)
                let endTime = moment(record.EndTime, 'HH:mm').format(_initData.Company.Setting.CalendarTimeFormat);
                td.append(`${startTime} - ${endTime}`);
            }
            else {
                if (recordType == "location")
                    td.append(`Close`);
                else
                    td.append(`Not Working`)
            }
            tableRow.append(td);
        }

        if (recordType == "employee") {
            let deltd = $(`<td>
            <div class="d-flex">

                <div class="btn btn-sm p-1">
                    <i class="fa fa-edit"></i>
                </div>
                <div class="btn btn-sm p-1 btn-del-timing">
                    <i class="fa fa-trash"></i>
                </div>
            </div>
            </td>`);

            tableRow.append(deltd);
        }

        data.PageContainer.find('.user-details').append(tableRow)
    }

    self.ResetEmployeeTimings();
}

// _shop.GetEmployeePriceDetails = function (employeeIDs, callback) {
//     $.ajax({
//         url: '/api/shop/getemployeepricedetails',
//         type: 'post',
//         dataType: 'json',
//         contentType: 'application/json',
//         data: JSON.stringify({ employeeIDs: employeeIDs }),
//         success: function (res) {
//             callback(res);
//         },
//         error: function (res) {
//             callback(res);
//         }
//     });
// }

_shop.GetEmployeeTimingDetails = function (locationID, callback) {
    $.ajax({
        url: '/api/shop/getemployeetimingdetails',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ locationID: locationID }),
        success: function (res) {
            callback(res);
        },
        error: function (res) {
            callback(res);
        }
    });
}

_shop.AddEmployeeTimingFormModal = function (page) {
    let formModal = _common.DynamicPopUp({ title: "Add Staff", body: "", DistroyOnHide: true });

    let FieldSchema = {
        Name: "EmployeeID",
        UIDataType: "lookup", DisplayName: "Staff",
        LookupObject: "User",
        LookupFields: [
            "FirstName",
            "LastName"
        ],
        ExtraLookupFields: [
            "UserStatus"
        ]
    };
    let buildFieldObj = {
        Schema: FieldSchema,
        ColumnWidth: 12,
        Name: "EmployeeID",
        ObjectName: 'Timing',
        IsRequired: true
    }

    let params = {};
    // params.data = entityData; //To pass data in multilookup
    params.field = buildFieldObj;
    params.container = formModal.find(".modal-body");
    //User multilookup
    _page.BuildField(params);

    let timings = `<div class="form-group col-sm-12 vld-parent ">
                    <label class="small">Working Days</label>
                    <div class="text-secondary user-working-days" style="max-width: 500px; width:100%;">
                        <div class="custom-control custom-checkbox mb-2">
                            <div class="row daytiming" data-week="0">
                                <div class="col">
                                    <input type="checkbox" class="custom-control-input chkDay" data-id="0" id="chkWDSunday">
                                    <label class="custom-control-label text-black" for="chkWDSunday">Sunday</label>
                                </div>
                                <div class="d-inline-block ml-3 mt-n2">
                                    <div class="d-inline-block small starttime">
                                    </div>
                                    <div class="d-inline-block endtime">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <div class="row daytiming" data-week="1">
                                <div class="col">
                                    <input type="checkbox" class="custom-control-input chkDay" data-id="1" id="chkWDMonday">
                                    <label class="custom-control-label text-black" for="chkWDMonday">Monday</label>
                                </div>
                                <div class="d-inline-block ml-3 mt-n2">
                                    <div class="d-inline-block small starttime"></div>
                                    <div class="d-inline-block endtime"></div>
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <div class="row daytiming" data-week="2">
                                <div class="col">
                                    <input type="checkbox" class="custom-control-input chkDay" data-id="2" id="chkWDTuesday">
                                    <label class="custom-control-label text-black" for="chkWDTuesday">Tuesday</label>
                                </div>
                                <div class="d-inline-block ml-3 mt-n2">
                                    <div class="d-inline-block small starttime"></div>
                                    <div class="d-inline-block endtime"></div>
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <div class="row daytiming" data-week="3">
                                <div class="col">
                                    <input type="checkbox" class="custom-control-input chkDay" data-id="3" id="chkWDWednesday">
                                    <label class="custom-control-label text-black" for="chkWDWednesday">Wednesday</label>
                                </div>
                                <div class="d-inline-block ml-3 mt-n2">
                                    <div class="d-inline-block small starttime"></div>
                                    <div class="d-inline-block endtime"></div>
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <div class="row daytiming" data-week="4">
                                <div class="col">
                                    <input type="checkbox" class="custom-control-input chkDay" data-id="4" id="chkWDThursday">
                                    <label class="custom-control-label text-black" for="chkWDThursday">Thursday</label>
                                </div>
                                <div class="d-inline-block ml-3 mt-n2">
                                    <div class="d-inline-block small starttime"></div>
                                    <div class="d-inline-block endtime"></div>
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <div class="row daytiming" data-week="5">
                                <div class="col">
                                    <input type="checkbox" class="custom-control-input chkDay" data-id="5"
                                        id="chkWDFriday">
                                    <label class="custom-control-label text-black" for="chkWDFriday">Friday</label>
                                </div>
                                <div class="d-inline-block ml-3 mt-n2">
                                    <div class="d-inline-block small starttime"></div>
                                    <div class="d-inline-block endtime"></div>
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-2">
                            <div class="row daytiming" data-week="6">
                                <div class="col">
                                    <input type="checkbox" class="custom-control-input chkDay" data-id="6" id="chkWDSaturday">
                                    <label class="custom-control-label text-black" for="chkWDSaturday">Saturday</label>
                                </div>
                                <div class="d-inline-block ml-3 mt-n2">
                                    <div class="d-inline-block small starttime"></div>
                                    <div class="d-inline-block endtime"></div>
                                </div>
                            </div>
                        </div>
                        <div class="error-container vld-parent"></div>
                    </div>
                </div>`;

    formModal.find('.modal-body').append(timings);

    let fieldTimeCtrl_Start = { 'Name': 'StartTime', 'DisplayName': 'Start Time' };
    fieldTimeCtrl_Start.Schema = { UIDataType: "time" };
    let timerCtrlStartTime = _controls.BuildCtrl({ field: fieldTimeCtrl_Start });
    formModal.find('.user-working-days').find('.starttime').append(timerCtrlStartTime);

    let fieldTimerCtrl_End = { 'Name': 'EndTime', 'DisplayName': 'End Time' };
    fieldTimerCtrl_End.Schema = { UIDataType: "time" };
    let timerCtrlEndTime = _controls.BuildCtrl({ field: fieldTimerCtrl_End });
    formModal.find('.user-working-days').find('.endtime').append(timerCtrlEndTime);

    let UserSettings = _initData.User.MySettings;
    if (UserSettings.WorkingDays) {
        for (let i = 0; i < UserSettings.WorkingDays.length; i++) {
            let wDay = UserSettings.WorkingDays[i];
            let wDayContainer = formModal.find('.user-working-days .chkDay[data-id="' + wDay.WorkingDay + '"]');
            if (wDayContainer) {
                let wDayParent = wDayContainer.closest('.daytiming');
                wDayParent.find('.chkDay').attr('checked', 'checked');
                let startTime = moment(wDay.StartTime, 'HH:mm').format(_initData.Company.Setting.CalendarTimeFormat)
                let endTime = moment(wDay.Endtime, 'HH:mm').format(_initData.Company.Setting.CalendarTimeFormat);
                wDayParent.find('.starttime input[type="text"].msp-ctrl-time').val(startTime).attr('data-time', wDay.StartTime);
                wDayParent.find('.endtime input[type="text"].msp-ctrl-time').val(endTime).attr('data-time', wDay.Endtime);
            }
        }
    }

    formModal.find('.user-working-days .chkDay').each(function () {
        if (!$(this).is(':checked')) {
            _common.DisableField($(this).parent().next(), 'time');
        } else {
            $(this).parent().next().find('.starttime,.endtime').addClass('vld-parent');
            $(this).parent().next().find('.msp-ctrl-time').attr('data-required', true);
        }
    });
    formModal.find('.user-working-days .chkDay').on('click', function () {
        $(this).parent().next().find('input').val('').attr('data-time', '00:00');
        if ($(this).is(':checked')) {
            _common.EnableField($(this).parent().next(), 'time');
            $(this).parent().next().find('.starttime,.endtime').addClass('vld-parent');
            $(this).parent().next().find('.msp-ctrl-time').attr('data-required', true);

        } else {
            _common.DisableField($(this).parent().next(), 'time');
            $(this).parent().next().find('.starttime,.endtime').removeClass('vld-parent');
            $(this).parent().next().find('.msp-ctrl-time').removeAttr('data-required');
        }
    });

    formModal.on('change', '.user-working-days .starttime input[data-field="StartTime"]', function () {
        let row = $(this).closest('.daytiming')
        _common.FromTimeToTimeValid(row.find('.starttime input[data-field="StartTime"]'), row.find('.endtime input[data-field="EndTime"]'), "FromTime");
    });
    formModal.on('change', '.user-working-days .endtime input[data-field="EndTime"]', function () {
        let row = $(this).closest('.daytiming')
        _common.FromTimeToTimeValid(row.find('.starttime input[data-field="StartTime"]'), row.find('.endtime input[data-field="EndTime"]'), "ToTime");
    });

    formModal.on('onLookupChange', '.msp-ctrl[data-field="EmployeeID"]', function () {
        let newAttId = $(this).find(".lookup-item").attr("data-id");
        let attendees = page.PageContainer.find(`.user-details .user-timing-info-row[data-recordtype="employee"][data-recordid="${newAttId}"]`);
        if (attendees.length > 0) {
            swal('Selected Staff timing already exist', { icon: "error" });
            if ($(this).find('.lookup-item') != undefined) {
                $(this).find('.lookup-item').remove();
                $(this).attr('contenteditable', 'true');
            }
        }
    })

    let LocationID = page.MatchFields.find(x => x.Name == "LocationID").Value;
    let ShopID = page.MatchFields.find(x => x.Name == "ShopID").Value;

    formModal.find('.btnSave').click(function () {
        let sdata = {};
        let EmployeeID = _controls.GetCtrlValue(params);
        let anyclash = false;
        let conflictRecords = [];
        sdata.WorkingDays = [];
        sdata.EmployeeID = EmployeeID;
        sdata.LocationID = LocationID;
        formModal.find('.user-working-days .daytiming').each(function () {
            let WorkingDayObj = {};


            WorkingDayObj.WorkingDay = $(this).find('.chkDay').attr('data-id');
            WorkingDayObj.StartTime = $(this).find('.starttime input[type="text"].msp-ctrl-time').attr("data-time");
            WorkingDayObj.EndTime = $(this).find('.endtime input[type="text"].msp-ctrl-time').attr("data-time");

            if ($(this).find('.chkDay').is(':checked')) {
                WorkingDayObj.IsWorking = true;
                let prevRecord = page.PageContainer.find(`.user-details .user-timing-info-row[data-recordtype="location"] .user-timings[data-weekday="${WorkingDayObj.WorkingDay}"]`)[0].record;
                let clash = _shop.TimingClash(prevRecord, WorkingDayObj);
                if (clash) {
                    conflictRecords.push(WorkingDayObj);
                    anyclash = true;
                }
            }
            else
                WorkingDayObj.IsWorking = false;

            if (LocationID != undefined)
                WorkingDayObj.LocationID = _controls.ToEJSON("objectid", LocationID);
            if (ShopID != undefined)
                WorkingDayObj.ShopID = _controls.ToEJSON("objectid", ShopID);

            WorkingDayObj.EmployeeID = _controls.ToEJSON("objectid", EmployeeID);

            sdata.WorkingDays.push(WorkingDayObj);
        });
        if (_common.Validate(formModal)) {
            if (anyclash) {
                swal('Please select time when Shop is open.', { icon: "info" })
                for (let i = 0; i < conflictRecords.length; i++) {
                    _shop.ValidationMsg(formModal.find('.user-working-days .daytiming[data-week=' + conflictRecords[i].WorkingDay + ']'), "Please select time when Shop is open.");
                }
            }
            else {
                _common.Loading(true);
                _shop.AddEmployeeTimingObject(sdata, function (result) {
                    result.formModal = formModal;
                    page.InitCallback(result);
                })
            }
        }
    });

    formModal.modal('show')

}

_shop.AddEmployeeTimingObject = function (param, callback) {
    $.ajax({
        url: '/api/shop/addemployeetiming',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(param),
        success: function (res) {
            callback(res);
        },
        error: function (res) {
            callback(res);
        }
    });
}


_shop.TimingClash = function (prevRecord, newRecord) {
    if (prevRecord.IsWorking) {
        if (newRecord.StartTime == undefined)
            newRecord.StartTime = '00:00';
        else if (newRecord.EndTime == undefined)
            newRecord.EndTime = '00:00';
        else if (prevRecord.StartTime == undefined)
            prevRecord.StartTime = '00:00';
        else if (prevRecord.EndTime == undefined)
            prevRecord.EndTime = '00:00';

        let newStartTime = moment(newRecord.StartTime, 'HH:mm')
        let newEndTime = moment(newRecord.EndTime, 'HH:mm')
        let prevStartTime = moment(prevRecord.StartTime, 'HH:mm')
        let prevEndTime = moment(prevRecord.EndTime, 'HH:mm')


        if (newStartTime >= prevStartTime && newEndTime <= prevEndTime)
            return false;
        else
            return true;
    }
    else {
        return true;
    }

}

_shop.ValidationMsg = function (el, message) {
    if (!el.closest('.custom-control').hasClass('has-error'))
        el.closest('.custom-control').addClass('has-error').append('<p class="help-block vld-message">' + message + '</p>');
}

_shop.GetServicePriceAndTime = function (param) {
    $.ajax({
        url: '/api/shop/getservicepriceandtime',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ serviceID: param.serviceID, employeeID: param.employeeID }),
        success: function (res) {
            param.callback(res);
        },
        error: function (res) {
            param.callback(res);
        }
    });
}

_shop.GetShopAndLocation = function (param, callback) {
    $.ajax({
        url: '/api/shop/getshopandlocation',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ shoptype: param.shopType, employeeID: param.employeeID }),
        success: function (res) {
            callback(res);
        },
        error: function (res) {
            callback(res);
        }
    });
}

_shop.ShopPaymentFromCalendar = function (activityId) {
    let param = {};
    param._id = activityId;
    param.callback = function (data) {
        let creater = {};
        let loggedinUser = null;
        let clients = [];

        data.AttendeesData.forEach(
            att => {
                if (att.IsOrganiser) {
                    creater = att;
                } else {
                    if (att.Id == _initData.User._id) {
                        loggedinUser = att
                    } else if (att.Status == 'accepted' || att.Status == 'pending') {
                        clients.push(att)
                    }
                }
            }
        )
        if (!loggedinUser) {
            loggedinUser = creater;
        }
        let activityData = data.data;
        activityData.AttendeesData = data.AttendeesData;

        _shop.GetPaymentDetailsFromCalendar(activityId, function (result) {
            let pageParam = {};
            if (result.data) {
                pageParam._id = result.data._id;
            }
            else {
                pageParam.Data = data.data[0];
                pageParam.ClientData = clients[0];
                pageParam.ActivityID = activityId;

            }
            pageParam.pageName = "PaymentDetail";
            pageParam.PageTitle = "Payment Form"
            new PageForm(pageParam)
            // console.log(data)
        })

    }
    _calendar.ReadActivityData(param)
}



_shop.OnPaymentDone = function (page) {
    let paymentData = page.ResultData.data;
    let sdata = {
        'StaffName': paymentData.EmployeeID_LookupData.FirstName + ' ' + paymentData.EmployeeID_LookupData.LastName,
        'Location': paymentData.LocationID_LookupData.Address,
        'ShopName': paymentData.ShopID_LookupData.Name,
        'ServiceName': paymentData.ServiceID_LookupData.Name,
        'ServicePrice': paymentData.ServicePrice.$numberDecimal
    };
    if (paymentData.RemainingPrice)
        sdata['RemainingPrice'] = paymentData.RemainingPrice.$numberDecimal;
    if (paymentData.Discount)
        sdata['Discount'] = paymentData.Discount.$numberDecimal;
    if (paymentData.TotalPrice)
        sdata['TotalPrice'] = paymentData.TotalPrice.$numberDecimal;
    if (paymentData.PaymentMode_LookupData)
        sdata['ModeOfPayment'] = paymentData.PaymentMode_LookupData.Value;
    if (paymentData.Contacts)
        sdata['ContactID'] = paymentData.Contacts;

    _common.FetchApiByParam({
        url: "api/shop/sendemailtocustomer",
        type: "post",
        data: sdata
    }, function (result) {
        if (result.message == "success")
            swal('Payment successful.', { icon: "success" });
        else
            swal('Couldn’t connect with the server, Please try after sometime', { icon: "error" });
    });
}

_shop.PaymentInitForm = function (page) {
    if (page.ParamData._id) {
        page.PageContainer.find('.modal-footer .btnSaveObject ').remove();
    } else {
        if (page.ParamData.ActivityID) {
            let cHtm = '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' + page.ParamData.ActivityID + '"></div>';
            page.PageContainer.find('[data-field="ActivityID"].msp-ctrl').html(cHtm).attr('contenteditable', 'false');
        }
        if (page.ParamData.ClientData) {
            let cHtm = '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' + page.ParamData.ClientData.Id + '">' + '<span class="lookup-display-record" data-objname="Contact" data-objid="' + page.ParamData.ClientData.Id + '">' + page.ParamData.ClientData.Name + '</span>' + '</div>';
            page.PageContainer.find('[data-field="Contacts"].msp-ctrl').html(cHtm).attr('contenteditable', 'false');
        }
        page.PageContainer.find('[data-field="PaidStatus"].msp-ctrl').val('0');
        page.PageContainer.find('.modal-footer .btnSaveObject ').html('Pay');
    }

    let servicePrice = page.PageContainer.find('[data-field="ServicePrice"]').val();
    if (servicePrice != undefined) {
        page.PageContainer.find('[data-field="TotalPrice"]').val(servicePrice);
        // page.PageContainer.find('[data-field="Discount"]').val(0);
        // page.PageContainer.find('[data-field="RemainingPrice"]').val(0);

        page.PageContainer.find('[data-field="Discount"]').on('change', function () {
            let discountPrice = $(this).val();
            let totalPrice = parseInt(servicePrice) - parseInt(discountPrice);
            page.PageContainer.find('[data-field="TotalPrice"]').val(totalPrice);
            page.PageContainer.find('[data-field="RemainingPrice"]').val(0);
        });

        page.PageContainer.find('[data-field="TotalPrice"]').on('change', function () {
            let totalPrice = $(this).val();
            let discountPrice = page.PageContainer.find('[data-field="Discount"]').val();
            let tip = parseInt(totalPrice) - parseInt(servicePrice) + parseInt(discountPrice);
            page.PageContainer.find('[data-field="RemainingPrice"]').val(tip);
        });

    }

}

_shop.GetPaymentDetailsFromCalendar = function (activityId, callback) {
    $.ajax({
        url: '/api/shop/getpaymentdetailsfromcalendar',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ ActivityID: activityId }),
        success: function (res) {
            callback(res);
        },
        error: function (res) {
            callback(res);
        }
    });
}