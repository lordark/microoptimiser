const _integrationsetting = {};

//Events Page: IntegrationSettings 
_integrationsetting.Init = function (page) {
    let sdata = { "Module": "Integration", "ControlPanelEntityID": page.param.ControlPanelItemID }
    _common.FetchApiByParam({
        url: "/api/admin/getcontrolpanel"
        , type: "post"
        , contentType: 'application/json'
        , dataType: 'json'
        , data: sdata
    }, function (result) {
        if (result && result.status == "ok") {
            if (result.data) {
                let itemHtml = '';
                for (let i = 0; i < result.data.length; i++) {
                    let item = result.data[i];
                    itemHtml += '<div class="col-sm-3 box text-center pt-4 control-item link-page" data-id="' + item._id + '" page-name="' + (item.PageName || "") + '">';
                    itemHtml += '<div class="card"><div class="card-body">';
                    itemHtml += '<div class="' + item.EntityIcon + ' mb-4 mt-4 fa-4x"></div>';
                    itemHtml += '<h4 class="card-title mb-4">' + item.EntityName + '</h4>';
                    itemHtml += '</div>';
                    itemHtml += '</div>';
                    itemHtml += '</div>';
                }
                page.PageContainer.find('.integration-plugins').append(itemHtml);
            } else {
                swal(result.message, { icon: "error" });
            }
        }
    });
}
//Events Page: IntegrateEmailClient
_integrationsetting.EmailIntegrationSetting = function (page) {
    _common.FetchApiByParam({
        url: "api/admin/getcompanysettings",
        type: "post"
    }, function (result) {
        if (result && result.message == "success") {
            _initData.Company.Setting = result.data;
            _common.EmailAccountDefaultSettings().then(function (emailClientList) {
                page.EmailClientsList = emailClientList.data;
                let emailAccountSetting = page.EmailClientsList;

                for (let i = 0; i < emailAccountSetting.length; i++) {
                    let smtpSettings = emailAccountSetting[i].Protocols.find(x => x.Protocol == 'smtp');
                    let securePort = '', normalPort = '';
                    if (smtpSettings) {
                        securePort = smtpSettings.Secure_Port;
                        normalPort = smtpSettings.Port;
                    }
                    let data = emailAccountSetting[i];
                    let emailClientHtml = '<div class="client-name ' + data.Domain + '" data-emailproviderid="' + data._id + '" data-domain="' + data.Domain + '" data-emailaccountname="' + data.Name + '" data-secureport="' + securePort + '" port="' + normalPort + '">';
                    emailClientHtml += '<img src="/images/' + data.Domain + '.png"/>';
                    emailClientHtml += '<div class="mt-2">' + data.Name + '</div></div>';
                    page.PageContainer.find('.email-client').append(emailClientHtml);
                }

                let companySettings = _initData.Company.Setting;
                if (companySettings.EmailIntegration) {
                    if (companySettings.EmailIntegration.EmailIntegrationEnable == true) {
                        page.PageContainer.find('.frmEmailIntegration').removeClass('d-none');
                        let emailAccountSettingMasterValues = emailAccountSetting.find(x => x.Domain == companySettings.EmailIntegration.EmailAccountType);
                        if (emailAccountSettingMasterValues) {
                            if (emailAccountSettingMasterValues.AuthTypes) {
                                let AuthTypes = "<option value=''></option>";
                                for (let i = 0; i < emailAccountSettingMasterValues.AuthTypes.length; i++) {
                                    AuthTypes += "<option value='" + emailAccountSettingMasterValues.AuthTypes[i] + "'>" + emailAccountSettingMasterValues.AuthTypes[i] + "</option>";
                                }
                                page.PageContainer.find('.ddlAuthType').html(AuthTypes);
                            }
                        }
                        page.PageContainer.find('.chkIsEmailIntegration').attr('checked', 'checked');
                        page.PageContainer.find('.email-client .client-name[data-emailproviderid="' + companySettings.EmailIntegration.EmailProviderID + '"]').addClass('active');

                        page.PageContainer.find('.ddlEmailIntegrationType').val(companySettings.EmailIntegration.EmailIntegrationType);
                        page.PageContainer.find('.txtIncomingMailServer').val(companySettings.EmailIntegration.IncomingMailServer);

                        page.PageContainer.find('.txtIncomingMailServerPortNo').val(companySettings.EmailIntegration.IncomingMailServerPortNo);
                        page.PageContainer.find('.txtOutgoingMailServer').val(companySettings.EmailIntegration.OutgoingMailServer);
                        page.PageContainer.find('.txtOutgoingMailServerPortNo').val(companySettings.EmailIntegration.OutgoingMailServerPortNo);
                        page.PageContainer.find('.txtExchangeURL').val(companySettings.EmailIntegration.ExchangeUrl);

                        if (companySettings.EmailIntegration.AuthType) {
                            page.PageContainer.find('.ddlAuthType').val(companySettings.EmailIntegration.AuthType);
                        }
                        if (companySettings.EmailIntegration.EmailAccountType != "custom") {
                            page.PageContainer.find('.frmEmailIntegration .email-client-settings').addClass('d-none');
                            // page.PageContainer.find('.txtIncomingMailServer').attr('disabled', 'disabled');
                            // page.PageContainer.find('.txtIncomingMailServerPortNo').attr('disabled', 'disabled');
                            // page.PageContainer.find('.txtOutgoingMailServer').attr('disabled', 'disabled');
                            // page.PageContainer.find('.txtOutgoingMailServerPortNo').attr('disabled', 'disabled');
                            // page.PageContainer.find('.txtExchangeURL').attr('disabled', 'disabled');
                            page.PageContainer.find('.txtIncomingMailServer,.txtIncomingMailServerPortNo,.txtOutgoingMailServer,.txtOutgoingMailServerPortNo,.txtExchangeURL').prop('disabled', true);
                        } else {
                            page.PageContainer.find('.frmEmailIntegration .email-client-settings').removeClass('d-none');
                        }
                    } else {
                        page.PageContainer.find('.frmEmailIntegration').addClass('d-none');
                    }
                }
                page.PageSize = (Number(_initData.User.MySettings.GridRows) || 10);
                page.PageIndex = 1;
                _integrationsetting.DisplayRestrictEmailID(page);
                _integrationsetting.IntegrationSettingEvents(page);
            }, function (error) {
                if (error && error.message)
                    swal(error.message, { "icon": "error" });
            });
        } else {
            swal(result.message, { "icon": "error" });
        }
    });

}
_integrationsetting.IntegrationSettingEvents = function (page) {
    page.PageContainer.find('.email-client').on('click', '.client-name', function () {
        page.PageContainer.find('.email-client .client-name').removeClass('active');
        $(this).addClass('active');
        if ($(this).attr('data-domain') == "custom") {
            page.PageContainer.find('.frmEmailIntegration .email-client-settings').removeClass('d-none');
            page.PageContainer.find('.txtIncomingMailServer,.txtIncomingMailServerPortNo,.txtOutgoingMailServer,.txtOutgoingMailServerPortNo,.txtExchangeURL').val('').removeAttr('disabled');
            page.PageContainer.find('.txtIncomingMailServer,.txtIncomingMailServerPortNo,.txtOutgoingMailServer,.txtOutgoingMailServerPortNo').attr('data-required', true);
            // page.PageContainer.find('.txtIncomingMailServer').val('').attr('data-required', true).removeAttr('disabled');
            // page.PageContainer.find('.txtIncomingMailServerPortNo').val('').attr('data-required', true).removeAttr('disabled');
            // page.PageContainer.find('.txtOutgoingMailServer').val('').attr('data-required', true).removeAttr('disabled');
            // page.PageContainer.find('.txtOutgoingMailServerPortNo').val('').attr('data-required', true).removeAttr('disabled');
            // page.PageContainer.find('.txtExchangeURL').val('').removeAttr('disabled', 'disabled');
            page.PageContainer.find('.ddlEmailIntegrationType,.ddlAuthType').attr('data-required', true);
        } else {
            page.PageContainer.find('.frmEmailIntegration .email-client-settings').addClass('d-none');
            page.PageContainer.find('.txtIncomingMailServer,.txtIncomingMailServerPortNo,.txtOutgoingMailServer,.txtOutgoingMailServerPortNo,.txtExchangeURL').val('').removeAttr('data-required');
            page.PageContainer.find('.txtIncomingMailServer,.txtIncomingMailServerPortNo,.txtOutgoingMailServer,.txtOutgoingMailServerPortNo,.txtExchangeURL').prop('disabled', true);
            // page.PageContainer.find('.txtIncomingMailServer').val('').attr('disabled', 'disabled').removeAttr('data-required');
            // page.PageContainer.find('.txtIncomingMailServerPortNo').val('').attr('disabled', 'disabled').removeAttr('data-required');
            // page.PageContainer.find('.txtOutgoingMailServer').val('').attr('disabled', 'disabled').removeAttr('data-required');
            // page.PageContainer.find('.txtOutgoingMailServerPortNo').val('').attr('disabled', 'disabled').removeAttr('data-required');
            // page.PageContainer.find('.txtExchangeURL').val('').attr('disabled', 'disabled');
            page.PageContainer.find('.ddlEmailIntegrationType').val("imap").removeAttr('data-required');
            page.PageContainer.find('.ddlAuthType').removeAttr('data-required');
        }
        let emailAccountSetting = page.EmailClientsList;
        let emailAccountSettingMasterValues = emailAccountSetting.find(x => x.Domain == $(this).attr('data-domain'));
        if (emailAccountSettingMasterValues) {
            let settingIncomingServer = emailAccountSettingMasterValues.Protocols.find(x => x.Protocol == "imap");
            let settingOutgoingServer = emailAccountSettingMasterValues.Protocols.find(x => x.Protocol == "smtp");
            if (settingIncomingServer) {
                page.PageContainer.find('.txtIncomingMailServer').val(settingIncomingServer.Host);
                page.PageContainer.find('.txtIncomingMailServerPortNo').val(settingIncomingServer.Secure_Port);
            }
            if (emailAccountSettingMasterValues) {
                if (emailAccountSettingMasterValues.AuthTypes) {
                    let AuthTypes = "<option value=''></option>";
                    for (let i = 0; i < emailAccountSettingMasterValues.AuthTypes.length; i++) {
                        AuthTypes += "<option value='" + emailAccountSettingMasterValues.AuthTypes[i] + "'>" + emailAccountSettingMasterValues.AuthTypes[i] + "</option>";
                    }
                    page.PageContainer.find('.ddlAuthType').html(AuthTypes);
                    if (page.PageContainer.find('.ddlAuthType option').length == 2)
                        page.PageContainer.find('.ddlAuthType').val(page.PageContainer.find('.ddlAuthType option:eq(1)').val());
                }
            }
            if (settingOutgoingServer) {
                page.PageContainer.find('.txtOutgoingMailServer').val(settingOutgoingServer.Host);
                if (page.PageContainer.find('.ddlAuthType').val() == "SSL")
                    page.PageContainer.find('.txtOutgoingMailServerPortNo').val(settingOutgoingServer.Secure_Port);
                else
                    page.PageContainer.find('.txtOutgoingMailServerPortNo').val(settingOutgoingServer.Port);
            }
        }
    });
    page.PageContainer.find('.btnAddProhibitEmail').click(function () {
        if ($(this).attr('data-disable') == undefined) {
            let btn = $(this);
            btn.attr('data-disable', true);
            setTimeout(function () { btn.removeAttr('data-disable'); }, 400);
            if (_common.Validate($(this).closest('.restrict-email-form'))) {
                let emailID = page.PageContainer.find('.txtProhibitEmail').val();
                if (emailID == "") {
                    swal("Please enter a vaild Email ID", { icon: "info" }).then((result) => {
                        page.PageContainer.find('.txtProhibitEmail').focus();
                    });
                    return;
                }
                _integrationsetting.CheckRestrictEmailAlreadyExist(emailID).then(function (result) {
                    if (result == true) {
                        swal("Email Id already exists", { icon: "info" }).then((result) => {
                            page.PageContainer.find('.txtProhibitEmail').focus();
                        });
                    } else {
                        _integrationsetting.AddRestrictEmailID(page, emailID);
                    }
                }, function (result) {
                    swal(_constantClient.ERROR_MSG, { icon: "info" });
                });
            }
        }
    });
    page.PageContainer.find('.tblProhibitEmail').on('click', '.btnDeleteProhibitEmail', function () {
        _common.ConfirmDelete({ "message": "You want to remove this email from restricted email list!" }).then((confirm) => {
            if (confirm) {
                _integrationsetting.DeleteRestrictEmailID($(this).closest('tr'), $(this).closest('tr').attr('data-id'));
            }
        });
    });
    page.PageContainer.find('.chkIsEmailIntegration').click(function () {
        if ($(this).is(':checked')) {
            page.PageContainer.find('.frmEmailIntegration').removeClass('d-none');
        } else {
            page.PageContainer.find('.frmEmailIntegration').addClass('d-none');
        }
    });
    page.PageContainer.off("click", '.restrict-email-paging .pagination .page-item');
    page.PageContainer.on("click", '.restrict-email-paging .pagination .page-item', function () {
        let pid = parseInt($(this).attr('pid'));
        let pager = $(this).closest('.pagination')[0].pager;
        if (pid < 1 || pid > pager.totalPages || pid == pager.currentPage) {
            return false;
        }
        page.PageIndex = pid;
        _integrationsetting.DisplayRestrictEmailID(page);
    });

    page.PageContainer.find('.btnSaveIntegrationSettings').click(function () {
        if (_common.Validate(page.PageContainer.find('.email-settings'))) {
            if (page.PageContainer.find('.chkIsEmailIntegration').is(':checked') == true) {
                if (page.PageContainer.find('.email-client .client-name.active').length == 0) {
                    _common.ValidateAddMsg(page.PageContainer.find('.error-container'), "Please select email client.");
                    return;
                }
            }
            let EmailIntegration = {};
            EmailIntegration.EmailIntegrationEnable = page.PageContainer.find('.chkIsEmailIntegration').is(':checked');
            EmailIntegration.EmailProviderID = page.PageContainer.find('.email-client .client-name.active').attr('data-emailproviderid');
            EmailIntegration.EmailAccountName = page.PageContainer.find('.email-client .client-name.active').attr('data-emailaccountname');
            EmailIntegration.EmailAccountType = page.PageContainer.find('.email-client .client-name.active').attr('data-domain');
            EmailIntegration.EmailIntegrationType = page.PageContainer.find('.ddlEmailIntegrationType').val();
            EmailIntegration.IncomingMailServer = page.PageContainer.find('.txtIncomingMailServer').val();
            EmailIntegration.IncomingMailServerPortNo = page.PageContainer.find('.txtIncomingMailServerPortNo').val();
            EmailIntegration.OutgoingMailServer = page.PageContainer.find('.txtOutgoingMailServer').val();
            EmailIntegration.OutgoingMailServerPortNo = page.PageContainer.find('.txtOutgoingMailServerPortNo').val();
            EmailIntegration.AuthType = page.PageContainer.find('.ddlAuthType').val();
            EmailIntegration.ExchangeUrl = page.PageContainer.find('.txtExchangeURL').val();

            _common.FetchApiByParam({
                url: "api/admin/updateemailintegrationsettings",
                type: "post",
                data: EmailIntegration
            }, function (result) {
                if (result) {
                    if (result.message == "success") {
                        _admin.LoadCompanySetting();
                        swal('Settings has been saved successfully', { icon: "success", });
                    } else {
                        swal(result.message, { icon: "error", });
                    }
                } else {
                    swal(result.message, { icon: "error", });
                }
            });
        }
    });
    page.PageContainer.find('.ddlAuthType').change(function () {
        if (page.PageContainer.find('.ddlEmailAccount').val() != "") {
            if ($(this).val() == "SSL") {
                let securePortNo = page.PageContainer.find('.ddlEmailAccount option:selected').attr('secureport');
                if (securePortNo)
                    page.PageContainer.find('.txtOutgoingMailServerPortNo').val(securePortNo);
            } else {
                let normalPortNo = page.PageContainer.find('.ddlEmailAccount option:selected').attr('port');
                if (normalPortNo)
                    page.PageContainer.find('.txtOutgoingMailServerPortNo').val(normalPortNo);
            }
        }
    });
}
_integrationsetting.AddRestrictEmailID = function (page, emailID) {
    let sdata = {
        EmailID: emailID.toLowerCase(),
        Operation: "insert"
    }
    _common.FetchApiByParam({
        url: "api/integrationsetting/restrictemaildml",
        type: "post",
        data: sdata
    }, function (result) {
        if (result.message == "success") {
            let tr = "<tr data-id='" + result.data._id + "'>";
            tr += "<td><div class='field-text'>" + emailID.toLowerCase() + "</div></td>";
            tr += "<td><div class='field-text'>" + result.data.UserName + "</div></td>";
            tr += "<td><div class='field-text'>" + (result.data.IsSystemEmail == true ? "Yes" : "No") + "</div></td>";
            tr += "<td><div><i class='fas fa-trash-alt btn btn-sm btn-outline-danger btnDeleteProhibitEmail'></i></div></td></tr>";
            page.PageContainer.find('.tblProhibitEmail tbody').prepend(tr);
            page.PageContainer.find('.txtProhibitEmail').val('').focus();
        } else {
            swal(result.message, { "icon": "error" });
        }
    });
}
_integrationsetting.DeleteRestrictEmailID = function (elTR, restrictEmailDBID) {
    let sdata = {
        RestrictEmailDBID: restrictEmailDBID,
        Operation: "delete"
    }
    _common.FetchApiByParam({
        url: "api/integrationsetting/restrictemaildml",
        type: "post",
        data: sdata
    }, function (result) {
        if (result.message == "success") {
            swal("Email Id has been deleted successfully", { "icon": "success" });
            elTR.remove();
        } else {
            swal(result.message, { "icon": "error" });
        }
    });
}
_integrationsetting.CheckRestrictEmailAlreadyExist = function (emailID) {
    return new Promise(function (resolve, reject) {
        let sdata = {
            EmailID: emailID.toLowerCase(),
            Operation: "checkduplicate"
        }
        _common.FetchApiByParam({
            url: "api/integrationsetting/restrictemaildml",
            type: "post",
            data: sdata
        }, function (result) {
            if (result.message == "success") {
                resolve(result.data);
            } else {
                reject(result);
            }
        });
    });
}
_integrationsetting.DisplayRestrictEmailID = function (page) {
    let sdata = {
        Operation: "select",
        PageSize: page.PageSize,
        PageIndex: page.PageIndex
    }
    _common.FetchApiByParam({
        url: "api/integrationsetting/restrictemaildml",
        type: "post",
        data: sdata
    }, function (result) {
        if (result.message == "success") {
            let TotalEmailCount = 0;
            if (result.data && result.data.length > 0) {
                let resultSummary = result.data[0];
                if (resultSummary.TotalEmailCount && resultSummary.TotalEmailCount.length > 0) {
                    TotalEmailCount = resultSummary.TotalEmailCount[0].count;
                }
                page.PageContainer.find('.tblProhibitEmail tbody tr').remove();
                for (let i = 0; i < resultSummary.EmailsList.length; i++) {
                    let data = resultSummary.EmailsList[i];
                    let tr = "<tr data-id='" + data.EmailDBID + "'>";
                    tr += "<td><div class='field-text'>" + data.EmailID + "</div></td>";
                    let userName = "";
                    if (data.FirstName)
                        userName = data.FirstName + " ";
                    if (data.LastName)
                        userName += data.LastName;
                    tr += "<td><div class='field-text'>" + userName + "</div></td>";
                    tr += "<td><div class='field-text'>" + (data.IsSystemEmail == true ? "Yes" : "No") + "</div></td>";
                    tr += "<td><div><i class='fas fa-trash-alt btn btn-sm btn-outline-danger btnDeleteProhibitEmail'></i></div></td></tr>";
                    page.PageContainer.find('.tblProhibitEmail tbody').append(tr);
                }
                //modalUser.find('.total-count').text((TotalUserCount > 0 ? TotalUserCount + " Items" : "No data found"));
                let pager = _common.PagerService(TotalEmailCount, page.PageIndex, page.PageSize);
                _common.BuildPaging(pager, page.PageContainer.find('.restrict-email-paging'));
            }
        } else {
            swal(result.message, { "icon": "error" });
        }
    });
}
///////Opera Integration/////////////////
_integrationsetting.OperaIntegrationSetting = function (page) {
    page.PageContainer.find('.chkIsOperaIntegration').click(function () {
        if ($(this).is(':checked') == false) {
            page.PageContainer.find('.frmOperaIntegration').addClass('d-none');
        } else {
            page.PageContainer.find('.frmOperaIntegration').removeClass('d-none');
        }
    });
    page.PageContainer.find('.method-name').on('click', function () {
        $(this).parent().find(".method-name").removeClass("active");
        $(this).addClass('active');
    });
    _common.FetchApiByParam({
        url: "/api/integrationsetting/getOperaModules",
        type: "get",
    }, function (data) {
        let parentModules = data.filter(x => x.ParentModuleID == null || x.ParentModuleID == undefined);
        for (i = 0; i < parentModules.length; i++) {
            var ModuleNames = parentModules[i].Name;
            var ParentID = data[i].ParentModuleID
            var SubscribeModule = parentModules[i].Subscribed
            var ModuleID = parentModules[i]._id;
            let disable = "";
            if (SubscribeModule == false)
                disable = "disabled";
            var values = `<div class="custom-control custom-checkbox mt-2"><input type="checkbox" name="Module" ${disable} class="custom-control-input" id="` + ModuleNames + `" value="` + ModuleID + `"><label class="custom-control-label" for="` + ModuleNames + `">` + ModuleNames + `</label></div>`;
            page.PageContainer.find('.module').append(values);
            let childModules = data.filter(x => x.ParentModuleID == ModuleID);
            if (childModules) {
                page.PageContainer.find('input[value="' + ParentID + '"]').addClass('parent');
                for (let j = 0; j < childModules.length; j++) {
                    let childModule = childModules[j];
                    let ChildDisable = "";
                    if (childModule.Subscribed == false)
                        ChildDisable = "disabled";
                    values = `<div class="custom-control custom-checkbox mt-2 pl-5 "><input type="checkbox" name="Module" ${disable} ${ChildDisable} class="custom-control-input" data-parentid="` + ParentID + `" id="` + childModule.Name + `" value="` + childModule._id + `"><label class="custom-control-label" for="` + childModule.Name + `">` + childModule.Name + `</label></div>`;
                    page.PageContainer.find('.module').append(values);

                    page.PageContainer.find(".parent").on('change', function () {
                        if (page.PageContainer.find(".parent:checked").length) {
                            if (childModule.Subscribed == true) {
                                page.PageContainer.find('input[id="' + childModule.Name + '"]').prop("checked", true);
                            }
                        } else {
                            page.PageContainer.find("input[data-parentid]").prop("checked", false);
                        }
                    })
                }
            }
        }
        page.PageContainer.find("input[data-parentid]").on('change', function () {
            if (page.PageContainer.find("input[data-parentid]").is(':checked')) {
                page.PageContainer.find('.parent').prop('checked', true);
            }
            else { page.PageContainer.find('.parent').prop('checked', false) };
        })
        _integrationsetting.GettingData(page);
    });
}
_integrationsetting.OperaSettingEvents = function (page) {
    page.PageContainer.find('.btnSaveIntegration').click(function () {
        let modules = page.PageContainer.find('.custom-checkbox input[name="Module"]');
        let checkedModule = [];
        for (let i = 0; i < modules.length; i++) {
            if (modules[i].checked) {
                checkedModule.push(modules[i].value);
            }
        }
        let OperaIntegration = {};
        OperaIntegration.OperaIntegrationEnable = page.PageContainer.find('.chkIsOperaIntegration').is(':checked');
        OperaIntegration.Method = page.PageContainer.find('.method-name.active').attr('data-val');
        OperaIntegration.Priority = page.PageContainer.find('.opera-priority input[type="radio"]:checked').val();
        OperaIntegration.Module = checkedModule;
        _common.FetchApiByParam({
            url: "/api/integrationsetting/UpdateOperaIntegrationSettings",
            type: "post",
            data: OperaIntegration
        }, function (result) {
            if (result) {
                if (result.message == "success") {
                    _admin.LoadCompanySetting();
                    swal('Settings has been saved successfully', { icon: "success", });
                } else {
                    swal(result.message, { icon: "error", });
                }
            }
        });
    })
}
_integrationsetting.GettingData = function (page) {
    _common.FetchApiByParam({
        url: "api/admin/getcompanysettings",
        type: "post"
    }, function (result) {
        if (result && result.message == "success") {
            let companySettings = _initData.Company.Setting;
            if (companySettings.OperaIntegration) {
                if (companySettings.OperaIntegration.Method) {
                    page.PageContainer.find('.method [name="' + companySettings.OperaIntegration.Method + '"]').addClass('active');
                }
                if (companySettings.OperaIntegration.Priority) {
                    page.PageContainer.find('.opera-priority input[type="radio"][value="' + companySettings.OperaIntegration.Priority + '"]').attr('checked', 'checked');
                }
                if (companySettings.OperaIntegration.Module) {
                    for (let i = 0; i < companySettings.OperaIntegration.Module.length; i++) {
                        let OperaModules = page.PageContainer.find('.module input[name="Module"][value="' + companySettings.OperaIntegration.Module[i] + '"]')
                        if (OperaModules) {
                            OperaModules.attr('checked', 'checked');
                        }
                    }
                }
            }
        }
        _integrationsetting.OperaSettingEvents(page);
    })
}