const _chart = {};

_chart.ActivityCountMonthWise = function (objectType, _id, displayType, container) {
    let monthArr = [];
    let currentDate = new Date();
    let resultDate;

    let today = new Date();
    if (displayType == 'past') {
        currentDate.setDate(currentDate.getDate() - 1);
        resultDate = _common.GetDatesByCode('Before-N-MonthFistDayToCurrentDate', currentDate, 6, undefined);
        resultDate = _common.GetDatesByCode('Before-N-MonthFistDayToCurrentDate', currentDate, 6, 'YYYY-MM-DD HH:mm:ss.SSS');
        for (let i = 5; i >= 0; i--) {
            let d = new Date(today.getFullYear(), today.getMonth() - i, 1);
            let monthObj = _constantClient.Months[d.getMonth()];
            let year = d.getFullYear().toString().substr(-2);
            monthArr.push({ _id: monthObj.Key, DisplayName: monthObj.SortName + '-' + year });
        }
    }
    else {
        resultDate = _common.GetDatesByCode('After-N-MonthLastDayToCurrentDate', currentDate, 6, undefined);
        for (let i = 0; i <= 5; i++) {
            let d = new Date(today.getFullYear(), today.getMonth() + i, 1);
            let monthObj = _constantClient.Months[d.getMonth()];
            let year = d.getFullYear().toString().substr(-2);
            monthArr.push({ _id: monthObj.Key, DisplayName: monthObj.SortName + '-' + year });
        }
    }

    _common.ContainerLoading(true, $(container).closest('.loading-container'));
    let sdata = { objectType: objectType, _id: _id, fromDate: resultDate.fromDate, toDate: resultDate.toDate };
    _common.FetchApiByParam({ url: '/api/chart/activitycountmonthwise', data: sdata }, function (result) {
        if (result.message == 'success') {
            let options = {
                legend: { position: 'none' },
                colors: ['#007EE3'],
                vAxis: { title: 'Count' },
                hAxis: { title: 'Month' },
            };
            if (result.data.length == 0) {
                options.vAxis.ticks = [0];
            }

            let data = new _googleChart.visualization.DataTable();
            data.addColumn('string', 'Month');
            data.addColumn({ type: 'number', label: 'Total Activity' });

            data.addRows(monthArr.length);
            for (let i = 0; i < monthArr.length; i++) {
                let month = monthArr[i];
                let activityCount = 0;
                for (let j = 0; j < result.data.length; j++) {
                    let obj = result.data[j];
                    if (obj._id == month._id) {
                        activityCount = obj.Count;
                        break;
                    }
                }
                data.setCell(i, 0, month.DisplayName);
                data.setCell(i, 1, activityCount);
            }
            let chart = new _googleChart.visualization.LineChart(container);
            chart.draw(data, options);
            _common.ContainerLoading(false, $(container).closest('.loading-container'));
        }
        else {
            _common.ContainerLoading(false, $(container).closest('.loading-container'));
            swal(result.message, { icon: "error" });
        }

    });
}

_chart.GeoCharts = function (container, param) {
    let chart = new _googleChart.visualization.GeoChart(container);
    chart.draw(param.data, param.options);
}
_chart.PieChart = function (container, param) {
    let chart = new _googleChart.visualization.PieChart(container);
    chart.draw(param.data, param.options);
}
_chart.LineChart = function (container, param) {
    var chart = new _googleChart.visualization.LineChart(container);
    chart.draw(param.data, param.options);
}