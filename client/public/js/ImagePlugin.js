const IBP = {}; //Image Block Plugin
// $.fn.ImageBlockAdd = function () {
//     return this.each(function () {
//         let thisObj = $(this);
//         thisObj.FormUploadModal = IBP.GetImageUploadModal();
//         thisObj.OriginalImage = new Image();
//         IBP.CreateImageBlock(thisObj);
//         IBP.EventBinder(thisObj);
//     });
// }
IBP.ValidImageFiles = ["jpg", "png", "jpeg", "gif", "bmp"];
IBP.CreateImageBlock = function (thisObj) {
    let testImagePath = "/images/bg.png";
    let imgContainerHeight = (thisObj.attr('data-height') || "100px");
    let imgContainerWidth = (thisObj.attr('data-width') || "100px");
    let imgContainer = $('<div class="img-container"></div>');
    imgContainer.append("<img src='" + testImagePath + "' style='height:100%;width:100%'></img>");
    imgContainer.append("<div class='ctrls'><span class='btnImgEdit' style='cursor:pointer;'>Edit</span></div>");
    imgContainer.css({ "height": imgContainerHeight, "width": imgContainerWidth });
    imgContainer.css({ "background-color": "red" });
    thisObj.append(imgContainer);
    thisObj.ImageContainer = imgContainer;
    //alert('My name is Plugin, i am called.');
}
IBP.ImageEditor = function (imageBox) {
    if (imageBox.ImageContainer.find('.inputImageUpload').length == 0) {
        let imageBrowse = $('<input type="file" class="inputImageUpload" style="display:none" accept=".jpg, .png, .jpeg, .gif, .bmp"></input>');
        imageBox.ImageContainer.append(imageBrowse);
    }

    //imageBox.FormUploadModal = IBP.GetImageUploadModal();
    imageBox.OriginalImage = new Image();
    //imageBox.IsLoadCall = false;
    $(imageBox.OriginalImage).on('load', function (e) {
        //imageBox.IsLoadCall = true;
        IBP.CustomizeImage(imageBox);
    });
    IBP.EventBinder(imageBox);
    //imageBox.FormUploadModal.modal('show');
    imageBox.ImageContainer.find('.inputImageUpload').click();

}
IBP.EventBinder = function (thisObj) {
    // thisObj.ImageContainer.on('click', '.btnImgEdit', function () {
    //     thisObj.FormUploadModal.modal('show');
    // });
    // thisObj.on('click', '.uploadimage', function () {
    //     thisObj.FormUploadModal.find('.inputImageUpload').click();
    // });
    thisObj.ImageContainer.find('.inputImageUpload').unbind('click');
    thisObj.ImageContainer.find('.inputImageUpload').click(function (e) {
        e.stopPropagation();
        thisObj.ImageContainer.find('.inputImageUpload').val("");
    });
    thisObj.ImageContainer.find('.inputImageUpload').unbind('change');
    thisObj.ImageContainer.find('.inputImageUpload').change(function () {
        //Validation Pending for whether user has selected a file or not.
        let uploadedFile = $(this)[0].files[0];
        if (uploadedFile) {
            if (!IBP.CheckValidFile(uploadedFile)) {
                swal('Format should in JPG,PNG,GIF and BMP', { icon: "info", });
                return;
            }
            thisObj.FileSize = uploadedFile.size;
            thisObj.FileName = uploadedFile.name;
            thisObj.FileType = uploadedFile.type;
            let fileReader = new FileReader();
            fileReader.addEventListener("loadend", function () {
                thisObj.OriginalImage.src = fileReader.result;
                // setTimeout(function () {
                //     if (thisObj.IsLoadCall == false) {
                //         IBP.CustomizeImage(thisObj);
                //     }
                // }, 1000);
            });
            if (uploadedFile) {
                fileReader.readAsDataURL(uploadedFile);
            }
        } else {
            if (thisObj.callback) {
                thisObj.callback("");
            }
        }
    });
}
IBP.GetImageUploadModal = function () {
    let formModal = $('<div class="modal fade page-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="img-editor" aria-hidden="true">' +
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +

        '<div class="modal-header">' +
        '<h5 class="modal-title"></h5>' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +

        '<div class="modal-body">' +
        '<table><tr><td>' +
        '<div class="uploadimage" style="height:100px;width:140px;background-color:green;">Upload Image' +
        '<input type="file" class="inputImageUpload" style="display:none"></input>' +
        '</div>' +
        '</td></tr>' +
        '<tr><td><img class="imgOriginalImage" src=""></img></td></tr>' +
        '</table>' +
        '</div>' +

        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>' +
        '<button type="button" class="btn btn-primary btnSaveObject">Save</button>' +
        '</div>' +

        '</div>' +
        '</div>' +
        '</div>');
    return formModal;
}
IBP.GetImageEditModal = function () {
    let formModal = $('<div class="modal fade page-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="ObjectModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +

        '<div class="modal-header">' +
        '<h5 class="modal-title"></h5>' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Close" value="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>' +

        '<div class="modal-body">' +
        '<table>' +
        '<tr><td><div class="img-edit-controls">Zoom Image</div></td></tr>' +
        '<tr><td>' +
        '<div class="img-edit"><img src="" style="width:100%"/></div>' +
        '</td></tr>' +
        '</table>' +
        '</div>' +

        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>' +
        '<button type="button" class="btn btn-primary btnSaveObject">Save</button>' +
        '</div>' +

        '</div>' +
        '</div>' +
        '</div>');
    return formModal;
}
IBP.CustomizeImage = function (thisObj) {
    let zoomH=(window.innerHeight-180)/thisObj.Height;
    let zoomW=(window.innerWidth-100)/thisObj.Width;
    let minZoom=Math.min(zoomH,zoomW,1);
    //<span id="demo">0</span>\
    let IBPEditor = $(`<div class="img-editor">\
    <div class="header row"><h5 class="float-left pl-2">Edit Image</h5>\
    <span class="image-resolution"><div class="pl-3">${thisObj.Width } X ${thisObj.Height}</div></span>\
    <span class="btn-close close col-auto ml-auto mr-2"><span>×</span></span>\
    </div>\
    <div class="body" style="overflow:auto;zoom:${minZoom}">\
    <div id="img-drag-container">\
    <div class="uploaded-image">\
    <img src="" class="image" />\
    </div>\
    <div class="image-selector">\
    <img src="" class="image-crop"/>\
    </div>\
    </div></div>\
    <div class="footer fixed-bottom p-3">\
    <h5 class="float-left text-primary">Zoom</h5>\
    <div class="slidecontainer text-center">\
    <span class="btn btn-sm text-dark">0</span><div class="range-cover">\
    <input type="range" title="Zoom In or Zoom Out" min="1" max="100" zoom_min_percent="-25" zoom_max_percent="100"  value="1" class="slider" id="myRange">\
    <div class="range-filler"></div></div><span class="btn btn-sm text-dark">2</span>\
    <span class="btn-save btn btn-primary float-right">Save</span>\
    <span class="btn-close btn btn-secondary float-right mr-2">Cancel</span>\
    </div>\
    </div>\
    </div>`);
    IBPEditor.appendTo($('body'));


    IBPEditor.off('click', '.btn-close');
    IBPEditor.on('click', '.btn-close', function () {
        if (thisObj.callback) {
            thisObj.callback("");
        }
        IBPEditor.remove();
    });

    IBPEditor.off('click', '.btn-save');
    IBPEditor.on('click', '.btn-save', function () {
        _common.Loading(true);
        let crop_canvas_final = null;
        if (IBPEditor.ImageSelector.width() > IBPEditor.ImageSelectorFrontImage.width() || IBPEditor.ImageSelector.height() > IBPEditor.ImageSelectorFrontImage.height()) {
            let canvasHeight = 0,
                canvasWidth = 0,
                leftCrop = 0,
                topCrop = 0,
                leftDraw = 0,
                topDraw = 0,
                cropWidth = IBPEditor.ImageSelector.width(),
                cropHeight = IBPEditor.ImageSelector.height(),
                crop_canvas = document.createElement('canvas');

            if (IBPEditor.ImageSelector.width() >= IBPEditor.ImageSelectorFrontImage.width()) {
                canvasWidth = IBPEditor.ImageSelector.width();
            } else {
                canvasWidth = IBPEditor.ImageSelectorFrontImage.width();
            }
            if (IBPEditor.ImageSelector.height() >= IBPEditor.ImageSelectorFrontImage.height()) {
                canvasHeight = IBPEditor.ImageSelector.height();
            } else {
                canvasHeight = IBPEditor.ImageSelectorFrontImage.height();
            }
            crop_canvas.width = canvasWidth;
            crop_canvas.height = canvasHeight;
            $(crop_canvas).css({ "border": "1px solid black" });
            const ctx = crop_canvas.getContext('2d');
            if (IBPEditor.ImageSelectorFrontImage.offset().left >= IBPEditor.ImageSelector.offset().left) {
                leftDraw = IBPEditor.ImageSelectorFrontImage.offset().left - IBPEditor.ImageSelector.offset().left;
            }
            else {
                leftCrop = IBPEditor.ImageSelector.offset().left - IBPEditor.ImageSelectorFrontImage.offset().left;
            }
            if (IBPEditor.ImageSelectorFrontImage.offset().top >= IBPEditor.ImageSelector.offset().top) {
                topDraw = IBPEditor.ImageSelectorFrontImage.offset().top - IBPEditor.ImageSelector.offset().top;
            } else {
                topCrop = IBPEditor.ImageSelector.offset().top - IBPEditor.ImageSelectorFrontImage.offset().top;
            }

            ctx.drawImage(IBPEditor.ImageSelectorFrontImage[0], 0, 0, IBPEditor.ImageOriginalStats.Width, IBPEditor.ImageOriginalStats.Height, leftDraw, topDraw, IBPEditor.ImageCurrentStats.Width, IBPEditor.ImageCurrentStats.Height);

            let imageData = ctx.getImageData(leftCrop, topCrop, cropWidth, cropHeight);
            crop_canvas_final = document.createElement('canvas');
            crop_canvas_final.width = cropWidth;
            crop_canvas_final.height = cropHeight;
            crop_canvas_final.getContext('2d').putImageData(imageData, 0, 0);
            IBPEditor.remove();
            if (thisObj.ImageContainer)
                thisObj.ImageContainer.find('img').attr('src', crop_canvas.toDataURL("image/png"));
        } else {
            let crop_canvas = document.createElement('canvas');
            crop_canvas.width = IBPEditor.ImageSelectorFrontImage.width();
            crop_canvas.height = IBPEditor.ImageSelectorFrontImage.height();
            const ctx = crop_canvas.getContext('2d');
            ctx.drawImage(IBPEditor.ImageSelectorFrontImage[0], 0, 0, IBPEditor.ImageOriginalStats.Width, IBPEditor.ImageOriginalStats.Height, 0, 0, crop_canvas.width, crop_canvas.height);


            let left = Math.abs(IBPEditor.ImageSelector.offset().left - IBPEditor.ImageSelectorFrontImage.offset().left); //Math.abs(IBPEditor.ImageCurrentStats.Left);
            let top = Math.abs(IBPEditor.ImageSelector.offset().top - IBPEditor.ImageSelectorFrontImage.offset().top);  //Math.abs(IBPEditor.ImageCurrentStats.Top);
            let width = IBPEditor.ImageSelector.width();
            let height = IBPEditor.ImageSelector.height();

            let imageData = ctx.getImageData(left, top, width, height);
            crop_canvas_final = document.createElement('canvas');
            crop_canvas_final.width = width;
            crop_canvas_final.height = height;

            // crop_canvas1.getContext('2d').drawImage(IBPEditor.ImageSelectorFrontImage[0], left, top, width, height, 0, 0, width, height);
            crop_canvas_final.getContext('2d').putImageData(imageData, 0, 0);

            IBPEditor.remove();
            if (thisObj.ImageContainer)
                thisObj.ImageContainer.find('img').attr('src', crop_canvas_final.toDataURL("image/png"));
        }
        if (crop_canvas_final != null) {
            let imgOptions = {};
            if (thisObj.CallingControl) {
                imgOptions.ObjectProperties = {
                    ObjectName: "",
                    ObjectFieldName: "",
                    ObjectDocumentID: ""
                };
                let fieldDetails = thisObj.CallingControl[0].field;
                if (fieldDetails) {
                    imgOptions.ObjectProperties.ObjectName = fieldDetails.ObjectName;
                    imgOptions.ObjectProperties.ObjectFieldName = fieldDetails.Name;
                    let parentContainer = thisObj.CallingControl.closest('.page-form');
                    if (parentContainer.length > 0 && parentContainer[0].PageForm) {
                        imgOptions.ObjectProperties.ObjectDocumentID = parentContainer[0].PageForm._id;
                    } else {
                        parentContainer = thisObj.CallingControl.closest('.page-content');
                        if (parentContainer[0].Page && parentContainer[0].Page.param)
                            imgOptions.ObjectProperties.ObjectDocumentID = parentContainer[0].Page.param._id;
                    }
                    imgOptions.ObjectProperties.ObjectDocumentID = (imgOptions.ObjectProperties.ObjectDocumentID || "");
                }
            }
            imgOptions.Id = 0;
            imgOptions.FileName = thisObj.FileName;
            imgOptions.FileType = thisObj.FileType;
            imgOptions.FileSize = thisObj.FileSize;
            if (thisObj.galleryFolderID != undefined)
                imgOptions.GalleryFolderID = thisObj.galleryFolderID;
            if (thisObj.parentImageID != undefined)
                imgOptions.ParentImageID = thisObj.parentImageID;

            imgOptions.ImageData = crop_canvas_final.toDataURL(imgOptions.FileType, 0.5);
            if (imgOptions.ImageData.indexOf(";base64,")) {
                imgOptions.FileType = imgOptions.ImageData.split(';')[0].split(':')[1];
            }
            imgOptions.ImageData = imgOptions.ImageData.split(";base64,")[1]
            console.log(imgOptions,thisObj,'optionssss')
            IBP.SaveImage(imgOptions, function (result) {
                if (thisObj.callback) {
                    thisObj.callback(result.fileid);
                } else {
                    thisObj.ImageContainer.attr('data-imageid', result.fileid);
                }
            });
        }
    });

    IBPEditor.on('click', '.chkFitToScale', function () {
        IBP.CalculateImageResolution(IBPEditor);
        IBPEditor.ImageEditing.removeAttr('width');
        IBPEditor.ImageSelectorFrontImage.removeAttr('width');
        IBPEditor.ImageEditing.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        IBPEditor.ImageSelectorFrontImage.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        IBPEditor.ImageEditing.position({ left: 0, top: 0 });
        IBP.CalculateImageDragContainterResolution(IBPEditor);
        IBP.AdjustFrontImagePosition(IBPEditor);
    })

    let scaleValue = 1;
    if (thisObj.Height > 300 || thisObj.Width > 300) {
        scaleValue = 1;//0.5;
        IBPEditor.IsScalled = true;
    }
    IBPEditor.NewImage = {};
    IBPEditor.NewImage.Height = thisObj.Height * scaleValue;
    IBPEditor.NewImage.Width = thisObj.Width * scaleValue;
    IBPEditor.find('.image-selector').css({ "height": IBPEditor.NewImage.Height + "px", "width": IBPEditor.NewImage.Width + "px" });


    /*New Image*/
    IBPEditor.ImageEditingContainer = IBPEditor.find('.uploaded-image');
    IBPEditor.ImageEditing = IBPEditor.find('.uploaded-image .image');
    IBPEditor.ImageDraggingContainer = IBPEditor.find('#img-drag-container');
    IBPEditor.ImageSelector = IBPEditor.find('.image-selector');
    IBPEditor.ImageSelectorFrontImage = IBPEditor.find('.image-selector img');


    IBPEditor.ImageEditing.attr("src", thisObj.OriginalImage.src);
    IBPEditor.ImageSelectorFrontImage.attr("src", thisObj.OriginalImage.src);


    IBPEditor.ImageCurrentStats = {};
    IBPEditor.ImageOriginalStats = {};

    IBPEditor.ImageOriginalStats.Height = IBPEditor.ImageEditing.height() * scaleValue;
    IBPEditor.ImageOriginalStats.Width = IBPEditor.ImageEditing.width() * scaleValue;
    IBPEditor.ImageOriginalStats.Top = 0;
    IBPEditor.ImageOriginalStats.Left = 0;

    IBPEditor.ImageCurrentStats.Height = IBPEditor.ImageEditing.height() * scaleValue;
    IBPEditor.ImageCurrentStats.Width = IBPEditor.ImageEditing.width() * scaleValue;
    IBPEditor.ImageCurrentStats.Top = 0;
    IBPEditor.ImageCurrentStats.Left = 0;

    if (IBPEditor.ImageOriginalStats.Height <= IBPEditor.ImageOriginalStats.Width) {
        IBPEditor.ImageCurrentStats.Height = IBPEditor.NewImage.Height;
        IBPEditor.ImageEditing.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        IBPEditor.ImageCurrentStats.Width = IBPEditor.ImageEditing.width();
    } else {
        IBPEditor.ImageCurrentStats.Width = IBPEditor.NewImage.Width;
        IBPEditor.ImageEditing.attr('width', IBPEditor.ImageCurrentStats.Width + 'px');
        IBPEditor.ImageCurrentStats.Height = IBPEditor.ImageEditing.height();
    }
    IBPEditor.ImageSelectorFrontImage.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
    IBPEditor.ImageSelectorFrontImage.attr('width', IBPEditor.ImageCurrentStats.Width + 'px');

    IBPEditor.ImageEditingContainer.draggable({
        containment: "#img-drag-container", scroll: false,
        drag: function (event, ui) {
            // $('#demo1').html(ui.position.left);
            // $('#demo2').html(ui.position.top);
            IBPEditor.ImageSelectorFrontImage.offset({ "left": ui.offset.left, "top": ui.offset.top });
            IBPEditor.ImageCurrentStats.Top = IBPEditor.ImageSelectorFrontImage.position().top;
            IBPEditor.ImageCurrentStats.Left = IBPEditor.ImageSelectorFrontImage.position().left;
        }
    });
    IBPEditor.ImageSelectorFrontImage.draggable({
        containment: "#img-drag-container", scroll: false,
        drag: function (event, ui) {
            IBPEditor.ImageEditingContainer.offset({ "left": ui.offset.left, "top": ui.offset.top });
            IBPEditor.ImageCurrentStats.Top = ui.position.top;
            IBPEditor.ImageCurrentStats.Left = ui.position.left;
        }
    });

    //IBPEditor.ImageEditingContainer.css({ height: IBPEditor.ImageCurrentStats.Height + 'px', width: IBPEditor.ImageCurrentStats.Width + 'px' });


    /*To Limit Dragging so that on dragging image should not leave the selector box*/
    IBP.CalculateImageDragContainterResolution(IBPEditor);

    IBPEditor.Slider = IBPEditor.find("#myRange");
    //var slider = document.getElementById("myRange");
    //var output = document.getElementById("demo");
    //var currentZoom = null;
    // Update the current slider value (each time you drag the slider handle)
    IBPEditor.Slider.on('input', function () {
        //output.innerHTML = this.value;
        IBPEditor.ImageEditing.removeAttr('width');
        IBPEditor.ImageSelectorFrontImage.removeAttr('width');
        IBPEditor.ImageCurrentStats.Height = Number(this.value);
        IBPEditor.ImageEditing.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        IBPEditor.ImageCurrentStats.Width = IBPEditor.ImageEditing.width();

        IBP.CalculateImageDragContainterResolution(IBPEditor);

        IBPEditor.ImageSelectorFrontImage.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        // var diff = -1
        // if (currentZoom) {
        //     if (currentZoom > Number(this.value)) {
        //         diff = 1;
        //     }
        // }
        // currentZoom = Number(this.value);

        // let leftPos = IBPEditor.ImageEditingContainer.position().left - diff;
        // let topPos = IBPEditor.ImageEditingContainer.position().top - diff;
        // // let leftPos = (IBPEditor.ImageDraggingContainer.width() / 2) - (IBPEditor.ImageEditingContainer.width() / 2);
        // // let topPos = (IBPEditor.ImageDraggingContainer.height() / 2) - (IBPEditor.ImageEditingContainer.height() / 2);
        // IBPEditor.ImageEditingContainer.css({ "left": leftPos + 'px', "top": topPos + 'px' });

        IBPEditor.ImageSelectorFrontImage.offset({ "left": IBPEditor.ImageEditing.offset().left, "top": IBPEditor.ImageEditing.offset().top });
        let multiplier = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
        $(this).parent().find('.range-filler').width((multiplier * 100) + '%');


    });
    let zoomMinPercent = IBPEditor.Slider.attr('zoom_min_percent');
    let zoomMaxPercent = IBPEditor.Slider.attr('zoom_max_percent');
    let sliderMinValue = IBPEditor.ImageCurrentStats.Height + (IBPEditor.ImageOriginalStats.Height * (zoomMinPercent / 100));
    let sliderMaxValue = IBPEditor.ImageOriginalStats.Height + (IBPEditor.ImageOriginalStats.Height * (zoomMaxPercent / 100));

    if (sliderMinValue < 25) {
        sliderMinValue = 25;
    }

    IBPEditor.Slider.attr('min', sliderMinValue);
    IBPEditor.Slider.attr('max', sliderMaxValue);
    IBPEditor.Slider.val(IBPEditor.ImageCurrentStats.Height);
    let multiplier = (IBPEditor.Slider.val() - IBPEditor.Slider.attr('min')) / (IBPEditor.Slider.attr('max') - IBPEditor.Slider.attr('min'));
    IBPEditor.Slider.parent().find('.range-filler').width((multiplier * 100) + '%');

    //output.innerHTML = IBPEditor.Slider.val(); // Display the default slider value
    let leftPos = (IBPEditor.ImageDraggingContainer.width() / 2) - (IBPEditor.ImageEditingContainer.width() / 2)
    let topPos = (IBPEditor.ImageDraggingContainer.height() / 2) - (IBPEditor.ImageEditingContainer.height() / 2)

    IBPEditor.ImageEditingContainer.css({ "left": leftPos + 'px', "top": topPos + 'px' });

    IBP.AdjustFrontImagePosition(IBPEditor);
}
IBP.SaveImage = function (options, afterSaving) {
    let fileData = {};
    fileData.filename = options.FileName;
    fileData.data = options.ImageData;
    fileData.type = options.FileType;
    fileData.size = options.FileSize;
    fileData.accesstype = "Self";
    fileData.filetype = 'profilepicture'
    if (options.GalleryFolderID != undefined) {
        fileData.galleryFolderID = options.GalleryFolderID;
        options.Id = "123123";
    }
    if (options.ParentImageID != undefined) {
        fileData.parentImageID = options.ParentImageID;
    }

    fileData.confirmAttachment = (options.Id ? true : false);
    if (options.ObjectProperties) {
        fileData.ObjectName = options.ObjectProperties.ObjectName;
        fileData.ObjectFieldName = options.ObjectProperties.ObjectFieldName;
        fileData.ObjectDocumentID = options.ObjectProperties.ObjectDocumentID;
    }
    $.ajax({
        url: "/api/drive/uploadfile",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(fileData),
        headers:{  'socket': sessionStorage.getItem('socket-id')},
        success: function (result) {
            swal('Image has been updated successfully', { icon: "success" });
            _common.Loading(false);
            afterSaving(result);
        },
        error: function (result) {
            swal('Unable to upload  the file. Please try again', { icon: "error" });
            _common.Loading(false);
        }
    });
}

IBP.AdjustFrontImagePosition = function (IBPEditor) {
    let left = IBPEditor.ImageEditing.offset().left;
    let top = IBPEditor.ImageEditing.offset().top;
    IBPEditor.ImageSelectorFrontImage.offset({ "left": left, "top": top });
    IBPEditor.ImageCurrentStats.Top = top;
    IBPEditor.ImageCurrentStats.Left = left;
}
IBP.CalculateImageDragContainterResolution = function (IBPEditor) {
    let imageDragContainerHeight = IBPEditor.ImageCurrentStats.Height + (IBPEditor.ImageCurrentStats.Height - IBPEditor.NewImage.Height);
    let imageDragContainerWidth = IBPEditor.ImageCurrentStats.Width + (IBPEditor.ImageCurrentStats.Width - IBPEditor.NewImage.Width);
    if (imageDragContainerHeight < IBPEditor.ImageSelector.height())
        imageDragContainerHeight = IBPEditor.ImageSelector.height();
    if (imageDragContainerWidth < IBPEditor.ImageSelector.width())
        imageDragContainerWidth = IBPEditor.ImageSelector.width();
    IBPEditor.ImageDraggingContainer.css({ height: imageDragContainerHeight + 'px', width: imageDragContainerWidth + 'px' });
}
IBP.CalculateImageResolution = function (IBPEditor) {
    if (IBPEditor.find('.chkFitToScale').is(':checked')) {
        let NewImageHeight_Integer = IBPEditor.ImageCurrentStats.Height;
        let NewImageWidth_Integer = IBPEditor.ImageCurrentStats.Width;

        if (IBPEditor.ImageCurrentStats.Height < IBPEditor.ImageCurrentStats.Width) {
            NewImageWidth_Integer = parseInt(IBPEditor.NewImage.Width);
            let tHeight = (IBPEditor.ImageCurrentStats.Height / IBPEditor.ImageCurrentStats.Width) * IBPEditor.NewImage.Width;
            NewImageHeight_Integer = parseInt(tHeight);
        } else {
            NewImageHeight_Integer = parseInt(IBPEditor.NewImage.Height);
            let tWidth = (IBPEditor.ImageCurrentStats.Width / IBPEditor.ImageCurrentStats.Height) * IBPEditor.NewImage.Height;
            NewImageWidth_Integer = parseInt(tWidth);
        }

        IBPEditor.ImageCurrentStats.Height = NewImageHeight_Integer;
        IBPEditor.ImageCurrentStats.Width = NewImageWidth_Integer;
        IBPEditor.ImageEditing.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        IBPEditor.ImageSelectorFrontImage.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        // IBP.CalculateImageDragContainterResolution(IBPEditor);
        // IBP.AdjustFrontImagePosition(IBPEditor);
    } else {
        if (IBPEditor.ImageOriginalStats.Height <= IBPEditor.ImageOriginalStats.Width) {
            IBPEditor.ImageCurrentStats.Height = IBPEditor.NewImage.Height;
            IBPEditor.ImageEditing.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
            IBPEditor.ImageCurrentStats.Width = IBPEditor.ImageEditing.width();
        } else {
            IBPEditor.ImageCurrentStats.Width = IBPEditor.NewImage.Width;
            IBPEditor.ImageEditing.attr('width', IBPEditor.ImageCurrentStats.Width + 'px');
            IBPEditor.ImageCurrentStats.Height = IBPEditor.ImageEditing.height();
        }
        IBPEditor.ImageSelectorFrontImage.attr('height', IBPEditor.ImageCurrentStats.Height + 'px');
        IBPEditor.ImageSelectorFrontImage.attr('width', IBPEditor.ImageCurrentStats.Width + 'px');
        // IBP.CalculateImageDragContainterResolution(IBPEditor);
        // IBP.AdjustFrontImagePosition(IBPEditor);
    }
    IBPEditor.Slider.attr('min', IBPEditor.ImageCurrentStats.Height);
    IBPEditor.Slider.val(IBPEditor.ImageCurrentStats.Height);
    let multiplier = (IBPEditor.Slider.val() - IBPEditor.Slider.attr('min')) / (IBPEditor.Slider.attr('max') - IBPEditor.Slider.attr('min'));
    IBPEditor.Slider.parent().find('.range-filler').width((multiplier * 100) + '%');
}
IBP.CheckValidFile = function (inputFile) {
    let fileExt = "";
    let nameSplit = inputFile.name.split('.');
    if (nameSplit && nameSplit.length > 0) {
        fileExt = nameSplit.pop();
    }
    if (IBP.ValidImageFiles.indexOf(fileExt.toLowerCase()) >= 0) {
        return true;
    } else {
        return false;
    }
}