const _admin = {};

//Events Page: ControlPanel
_admin.InitControlPanel = function (page) {
  let adminSettings = { page: page };
  _admin.GetControlPanelEntities(adminSettings);
};
_admin.GetControlPanelEntities = function (options) {
  let sdata = { Module: "AllParentModule" };
  _common.FetchApiByParam(
    {
      url: "/api/admin/getcontrolpanel",
      type: "post",
      contentType: "application/json",
      dataType: "json",
      data: sdata,
    },
    function (result) {
      if (result && result.status == "ok") {
        if (result.data) {
          console.log(result.data);
          let itemHtml = "";
          for (let i = 0; i < result.data.length; i++) {
            let item = result.data[i];
            itemHtml +=
              '<div class="col-sm-3 box text-center pt-4 control-item" data-id="' +
              item._id +
              '" page-name="' +
              (item.PageName || "") +
              '">';
            itemHtml += '<div class="card"><div class="card-body">';
            itemHtml +=
              '<div class="' + item.EntityIcon + ' mb-4 mt-4 fa-4x"></div>';
            itemHtml +=
              '<h4 class="card-title mb-4">' + item.EntityName + "</h4>";
            itemHtml += "</div>";
            itemHtml += "</div>";
            itemHtml += "</div>";
          }
          options.page.PageContainer.find(".admin-controls-list").append(
            itemHtml
          );
        } else {
          swal(result.message, { icon: "info" });
        }
      }
    }
  );
  options.page.PageContainer.on("click", ".control-item", function () {
    let params = {
      ControlPanelItemID: $(this).attr("data-id"),
      pageName: $(this).attr("page-name"),
    };
    new Page(params);
  });
};
_admin.LoadCompanySetting = function () {
  _common.FetchApiByParam(
    {
      url: "api/admin/getcompanysettings",
      type: "post",
    },
    function (result) {
      if (result.message == "success" && result.data) {
        _initData.Company.Setting = result.data;
      }
    }
  );
};
//Events Page: CalendarEmailSettings
_admin.LoadCalendarSettings = function (page) {
  _common.FetchApiByParam(
    {
      url: "api/admin/getcompanysettings",
      type: "post",
    },
    function (result) {
      if (result && result.message == "success") {
        _initData.Company.Setting = result.data;
        let calendarSetting = _initData.Company.Setting;

        let fieldTimeCtrl_Start = {
          Name: "StartTime",
          DisplayName: "Start Time",
        }; //CompanyTimeCtrl
        fieldTimeCtrl_Start.Schema = { UIDataType: "time" };
        let timerCtrlStartTime = _controls.BuildCtrl({
          field: fieldTimeCtrl_Start,
        });
        page.PageContainer.find(".company-working-hours")
          .find(".starttime")
          .append(timerCtrlStartTime);
        let fieldTimeCtrl_End = { Name: "EndTime", DisplayName: "End Time" }; //CompanyTimeCtrl
        fieldTimeCtrl_End.Schema = { UIDataType: "time" };
        let timerCtrlEndTime = _controls.BuildCtrl({
          field: fieldTimeCtrl_End,
        });
        page.PageContainer.find(".company-working-hours")
          .find(".endtime")
          .append(timerCtrlEndTime);

        page.PageContainer.find(".ddlInterval").val(
          calendarSetting.CalendarTimeInterval
        );
        page.PageContainer.find(".ddlTimeFormat").val(
          calendarSetting.CalendarTimeFormat
        );
        page.PageContainer.find(".ddlShare").val(calendarSetting.CalendarShare);
        if (
          page.PageContainer.find(
            ".calendar-share ." + calendarSetting.CalendarShare
          )
        ) {
          page.PageContainer.find(
            ".calendar-share ." + calendarSetting.CalendarShare
          ).attr("checked", "checked");
        }
        if (
          calendarSetting.AllowOverride == "false" ||
          calendarSetting.AllowOverride == false
        ) {
          page.PageContainer.find(".chkDoNotOverride").prop(
            "checked",
            "checked"
          );
        }
        if (calendarSetting.IsCreateBackDateActivity == true) {
          page.PageContainer.find("#optCreateBackDateActivity").prop(
            "checked",
            "checked"
          );
        }
        if (calendarSetting.IsUpdateBackDateActivity == true) {
          page.PageContainer.find("#optUpdateBackDateActivity").prop(
            "checked",
            "checked"
          );
        }
        if (calendarSetting.IsDeleteBackDateActivity == true) {
          page.PageContainer.find("#optDeleteBackDateActivity").prop(
            "checked",
            "checked"
          );
        }
        if (calendarSetting.CompanyDefaultSignature) {
          page.PageContainer.find(".comp-signature .signature").html(
            calendarSetting.CompanyDefaultSignature
          );
          //page.PageContainer.find('.txtCompanyDefaultSignature .texteditor').html(calendarSetting.CompanyDefaultSignature);
        }
        if (calendarSetting.CompanyWorkingDays) {
          for (let i = 0; i < calendarSetting.CompanyWorkingDays.length; i++) {
            let workingDay = page.PageContainer.find(
              '.company-working-days input[Type="checkbox"][data-id="' +
                calendarSetting.CompanyWorkingDays[i] +
                '"]'
            );
            if (workingDay) {
              workingDay.attr("checked", "checked");
            }
          }
        }
        if (calendarSetting.WorkingHrsStart) {
          let startTime = moment(
            calendarSetting.WorkingHrsStart,
            "HH:mm"
          ).format(_initData.Company.Setting.CalendarTimeFormat);
          page.PageContainer.find(
            '.company-working-hours .starttime [data-field="StartTime"]'
          )
            .val(startTime)
            .attr("data-time", calendarSetting.WorkingHrsStart);
        }
        if (calendarSetting.WorkingHrsEnd) {
          let endTime = moment(calendarSetting.WorkingHrsEnd, "HH:mm").format(
            _initData.Company.Setting.CalendarTimeFormat
          );
          page.PageContainer.find(
            '.company-working-hours .endtime [data-field="EndTime"]'
          )
            .val(endTime)
            .attr("data-time", calendarSetting.WorkingHrsEnd);
        }
        //page.CalendarSettings = calendarSetting;
      } else {
        swal(result.message, { icon: "error" });
      }
    }
  );
  _admin.BindCalenderSettingEvents(page);
};
_admin.BindCalenderSettingEvents = function (page) {
  page.PageContainer.off("click", ".btnUpdateCalendarSettings");
  page.PageContainer.on("click", ".btnUpdateCalendarSettings", function () {
    if (_common.Validate(page.PageContainer)) {
      if (
        page.PageContainer.find(
          '.company-working-days input[Type="checkbox"]:checked'
        ).length == 0
      ) {
        _common.ValidateAddMsg(
          page.PageContainer.find(".company-working-days"),
          "Please select atleast one working day."
        );
        return;
      }
      let calendarSetting = {}; //(page.CalendarSettings || {});
      calendarSetting.CalendarTimeInterval = page.PageContainer.find(
        ".ddlInterval"
      ).val();
      calendarSetting.CalendarTimeFormat = page.PageContainer.find(
        ".ddlTimeFormat"
      ).val();
      if (
        page.PageContainer.find('.calendar-share input[type="radio"]:checked')
      ) {
        calendarSetting.CalendarShare = page.PageContainer.find(
          '.calendar-share input[type="radio"]:checked'
        ).attr("data-calshare");
      } else {
        calendarSetting.CalendarShare = "private";
      }
      calendarSetting.AllowOverride = page.PageContainer.find(
        ".chkDoNotOverride"
      ).is(":checked")
        ? false
        : true;
      calendarSetting.IsCreateBackDateActivity = page.PageContainer.find(
        "#optCreateBackDateActivity"
      ).is(":checked")
        ? true
        : false;
      calendarSetting.IsUpdateBackDateActivity = page.PageContainer.find(
        "#optUpdateBackDateActivity"
      ).is(":checked")
        ? true
        : false;
      calendarSetting.IsDeleteBackDateActivity = page.PageContainer.find(
        "#optDeleteBackDateActivity"
      ).is(":checked")
        ? true
        : false;
      calendarSetting.CompanyDefaultSignature =
        page.PageContainer.find(".comp-signature .signature").html() || "";
      let workingDays = [];
      page.PageContainer.find(
        '.company-working-days input[Type="checkbox"]:checked'
      ).each(function () {
        workingDays.push($(this).attr("data-id"));
      });
      calendarSetting.CompanyWorkingDays = workingDays;
      calendarSetting.WorkingHrsStart = page.PageContainer.find(
        '.company-working-hours .starttime [data-field="StartTime"]'
      ).attr("data-time");
      calendarSetting.WorkingHrsEnd = page.PageContainer.find(
        '.company-working-hours .endtime  [data-field="EndTime"]'
      ).attr("data-time");

      _common.FetchApiByParam(
        {
          url: "api/admin/updatecompanycalendarsettings",
          type: "post",
          data: calendarSetting,
        },
        function (result) {
          if (result) {
            if (result.status == "ok") {
              let userSignUpdateOptions = {};
              userSignUpdateOptions.Page = page;
              userSignUpdateOptions.companySignature =
                calendarSetting.CompanyDefaultSignature;
              userSignUpdateOptions.callback = function (options) {
                if (options.message == "success") {
                  swal("Saved successfully.", { icon: "success" });
                } else if (options.message == "cancelled") {
                  swal("Saved successfully for this user.", {
                    icon: "success",
                  });
                } else {
                  swal("Something went wrong. Please try again.", {
                    icon: "error",
                  });
                }
              };
              if (page.DefaultSignatureModified == true) {
                _user.UpdateCompanySignatureToUser(userSignUpdateOptions);
              } else {
                swal("Saved successfully.", { icon: "success" });
              }
              _admin.LoadCompanySetting();
            } else {
              swal("Something went wrong. Please try again.", {
                icon: "error",
              });
            }
          } else {
            swal("Something went wrong. Please try again.", { icon: "error" });
          }
        }
      );
    }
  });
  page.PageContainer.on("click", ".btnCancelCalendarSettings", function () {
    $(document)
      .find("#main-header #page-nav .nav-item.active .tab-close")
      .trigger("click");
  });
  page.PageContainer.find(".chkIsEmailIntegration").click(function () {
    if ($(this).is(":checked")) {
      page.PageContainer.find(".frmEmailIntegration").show();
    } else {
      page.PageContainer.find(".frmEmailIntegration").hide();
    }
  });
  page.PageContainer.find(".ddlEmailAccount").change(function () {
    if ($(this).val() == "custom") {
      page.PageContainer.find(
        ".ddlEmailIntegrationType,.txtIncomingMailServer,"
      );
    }
  });
  page.PageContainer.find(".comp-signature .btnEditCompanySignature").click(
    function () {
      let modalSignature = _common.DynamicPopUp({
        title: "Company Default Signature",
        body: "",
      });
      _common.GetPageDetail("UserDetail", function (result) {
        let userFieldList = [];
        if (result.Fields) {
          result.Fields.sort(function (a, b) {
            return a.Sequence - b.Sequence;
          });
          for (let i = 0; i < result.Fields.length; i++) {
            let userField = {};
            userField.FieldName = result.Fields[i].Name;
            userField.DisplayName = _page.GetFieldDisplayName(result.Fields[i]);
            userFieldList.push(userField);
          }
        }
        if (result.Groups) {
          for (let g = 0; g < result.Groups.length; g++) {
            let group = result.Groups[g];
            for (let i = 0; i < group.Fields.length; i++) {
              let fld = group.Fields[i];
              let userField = {};
              userField.FieldName = fld.Name;
              userField.DisplayName = _page.GetFieldDisplayName(fld);
              userFieldList.push(userField);
            }
          }
        }
        let userFields = '<ul class="user-fields list-unstyled">';
        for (let i = 0; i < result.ObjectSchema.Fields.length; i++) {
          let displayName = _page.GetFieldDisplayName(
            result.ObjectSchema.Fields[i]
          );
          if (displayName) {
            userFields += "<li>" + displayName;
            userFields +=
              ' <input type="text" class="d-none" value="##' +
              result.ObjectSchema.Fields[i].Name +
              '##"/>';
            userFields +=
              '<i class="fa fa-copy btnCopyUserFieldCode text-info" title="Copy field code"></i></li>';
          }
        }
        userFields += "</ul>";
        _common.LoadEditor(modalSignature.find(".modal-body"));
        modalSignature.find(".msp-ctrl-texteditor").addClass("hotkey-enabled");
        modalSignature.find(
          ".msp-ctrl-texteditor .texteditor"
        )[0].DataList = userFieldList;

        // modalSignature.find('.modal-body').append('<div class="user-fields">\
        // <button class="btnViewUserFields btn btn-primary">Show Dynamic Fields</button>\
        // <div class="user-fields-list d-none"></div></div>');
        modalSignature.modal("show");
        let CompanyDefaultSignature =
          _initData.Company.Setting.CompanyDefaultSignature;
        modalSignature.find(".modal-footer .btnSave").text("Ok");
        if (CompanyDefaultSignature) {
          modalSignature.find(".texteditor").html(CompanyDefaultSignature);
        }
        modalSignature
          .find(".user-fields-list")
          .append(userFields)
          .attr("list-added", true);
        modalSignature.on(
          "click",
          ".user-fields .btnCopyUserFieldCode",
          function () {
            let liObj = $(this);
            liObj
              .closest("li")
              .find('input[type="text"]')
              .removeClass("d-none")
              .select();
            document.execCommand("copy");
            liObj.closest("li").find('input[type="text"]').addClass("d-none");
            liObj.append(
              '<span class="copy-message text-primary">copied</span>'
            );
            liObj.find(".copy-message").fadeOut(700, function () {
              $(this).remove();
            });
          }
        );
      });

      modalSignature.on("click", ".btnViewUserFields", function () {
        $(this).toggleClass("show");
        if ($(this).hasClass("show")) {
          $(this).text("Show Dynamic Fields");
          modalSignature.find(".user-fields-list").removeClass("d-none");
        } else {
          $(this).text("Hide Dynamic Fields");
          modalSignature.find(".user-fields-list").addClass("d-none");
        }
      });
      modalSignature.on("click", ".btnSave", function () {
        page.PageContainer.find(".comp-signature .signature").html(
          modalSignature.find(".modal-body .texteditor").html()
        );
        modalSignature.modal("hide");
        page.DefaultSignatureModified = true;
      });
    }
  );
  page.PageContainer.on(
    "change",
    '.company-working-hours .starttime input[data-field="StartTime"]',
    function () {
      let row = $(this).closest(".company-working-hours");
      _common.FromTimeToTimeValid(
        row.find('.starttime input[data-field="StartTime"]'),
        row.find('.endtime input[data-field="EndTime"]'),
        "FromTime"
      );
    }
  );
  page.PageContainer.on(
    "change",
    '.company-working-hours .endtime input[data-field="EndTime"]',
    function () {
      let row = $(this).closest(".company-working-hours");
      _common.FromTimeToTimeValid(
        row.find('.starttime input[data-field="StartTime"]'),
        row.find('.endtime input[data-field="EndTime"]'),
        "ToTime"
      );
    }
  );
};
//Events Page: CalendarShareSettings
_admin.AdvanceCalendarSharing = function (page) {
  let userSelected = page.PageDetail.Data;
  _user.GetUsersDetails({ userId: userSelected._id }, function (
    userResultData
  ) {
    if (userResultData.status == "ok") {
      userSelected.MySettings = userResultData.data.MySettings;
    }
    let calendarSetting = _initData.Company.Setting.CalendarShare || "private";
    if (calendarSetting == "private") {
      //page.PageContainer.find('.ddlAllowedUsersList,.ddlDeniedUsersList').attr('disabled', 'disabled');
      page.PageContainer.find(".permit-users-list").addClass("d-none");
      page.PageContainer.find(".permit-users-list-message")
        .removeClass("d-none")
        .text("Management has stopped sharing of calendar.");
    } else if (calendarSetting == "public") {
      //page.PageContainer.find('.ddlAllowedUsersList,.ddlDeniedUsersList').attr('disabled', 'disabled');
      page.PageContainer.find(".permit-users-list").addClass("d-none");
      page.PageContainer.find(".permit-users-list-message")
        .removeClass("d-none")
        .text("Everyone can view everyone calendar.");
    } else if (
      calendarSetting == "team" ||
      calendarSetting == "teamrestricted"
    ) {
      let sdata = {
        UserID: userSelected._id,
        SupervisorID: userSelected.SupervisorID,
      };
      _common.FetchApiByParam(
        {
          url: "/api/user/getmyteamuserlist",
          type: "post",
          contentType: "application/json",
          dataType: "JSON",
          data: sdata,
        },
        function (result) {
          if (result && result.message == "success" && result.data) {
            _admin.ShowPermittedUsersList(result.data, page, userSelected);
          }
        }
      );
    }
    _user.GetUsersList(function (result) {
      if (result.status == "ok") {
        let usersList = result.data;
        let AllowedUsers = [],
          DeniedUsers = [],
          HideFromAllUser = false;
        if (userSelected.MySettings) {
          AllowedUsers = userSelected.MySettings.CalendarAllowedUsers || [];
          DeniedUsers = userSelected.MySettings.CalendarDeniedUsers || [];
          HideFromAllUser = userSelected.MySettings.IsHideFromAllUsers || false;
        }
        let optionUsersAllowed = "";
        let optionUsersDenied = "";
        let option = "";
        for (let i = 0; i < usersList.length; i++) {
          if (usersList[i]._id.toString() == userSelected._id.toString()) {
            continue;
          }
          option +=
            '<option value="' +
            usersList[i]._id +
            '">' +
            usersList[i].FirstName +
            " " +
            usersList[i].LastName +
            "</option>";
        }
        page.PageContainer.find(".ddlAllowedUsersList")
          .append(option)
          .multiselect({
            nonSelectedText: "",
            onChange: function (currentOption, isSelected) {
              let userid = currentOption.val();
              let userContainer = page.PageContainer.find(
                '.users-denied input[type="checkbox"][value="' + userid + '"]'
              );
              if (isSelected == true) {
                userContainer.prop("disabled", true);
                userContainer.closest("li").addClass("disabled");
              } else {
                userContainer.prop("disabled", false);
                userContainer.closest("li").removeClass("disabled");
              }
            },
          });
        page.PageContainer.find(".ddlDeniedUsersList")
          .append(option)
          .multiselect({
            nonSelectedText: "",
            onChange: function (currentOption, isSelected) {
              let userid = currentOption.val();
              let userContainer = page.PageContainer.find(
                '.users-allowed input[type="checkbox"][value="' + userid + '"]'
              );
              if (isSelected == true) {
                userContainer.prop("disabled", true);
                userContainer.closest("li").addClass("disabled");
              } else {
                userContainer.prop("disabled", false);
                userContainer.closest("li").removeClass("disabled");
              }
            },
          });

        if (HideFromAllUser == true) {
          page.PageContainer.find(".chkHideFromAll").attr("checked", "checked");
        }
        page.PageContainer.find(".ddlAllowedUsersList").multiselect(
          "select",
          AllowedUsers,
          true
        );
        page.PageContainer.find(".ddlDeniedUsersList").multiselect(
          "select",
          DeniedUsers,
          true
        );
      }
    });
    page.PageContainer.on("click", ".btnSaveUserCalendarSettings", function () {
      var sdata = {};
      sdata.UserID = userSelected._id;
      sdata.CalendarAllowedUsers = [];
      sdata.CalendarDeniedUsers = [];
      sdata.IsHideFromAllUsers = false;
      let calendarAllowedUsers = _controls.ToEJSON(
        "multilookup",
        page.PageContainer.find(".ddlAllowedUsersList").val()
      );
      let calendarDeniedUsers = _controls.ToEJSON(
        "multilookup",
        page.PageContainer.find(".ddlDeniedUsersList").val()
      );
      let IsHideFromAllUsers =
        page.PageContainer.find(".chkHideFromAll").is(":checked") || false;

      if (calendarAllowedUsers && calendarAllowedUsers.length > 0) {
        sdata.CalendarAllowedUsers.push.apply(
          sdata.CalendarAllowedUsers,
          calendarAllowedUsers
        );
      }
      if (calendarDeniedUsers && calendarDeniedUsers.length > 0) {
        sdata.CalendarDeniedUsers.push.apply(
          sdata.CalendarDeniedUsers,
          calendarDeniedUsers
        );
      }
      sdata.IsHideFromAllUsers = IsHideFromAllUsers;

      _common.FetchApiByParam(
        {
          url: "/api/user/saveuseradvancecalendarsharing",
          type: "post",
          data: sdata,
        },
        function (result) {
          if (result && result.message == "success") {
            swal("Saved successfully.", { icon: "success" });
          } else {
            swal(result.message, { icon: "error" });
          }
        }
      );
    });
  });
};
_admin.ShowPermittedUsersList = function (usersList, page, userNotDisplay) {
  if (usersList) {
    let LI = "";
    for (let i = 0; i < usersList.length; i++) {
      if (userNotDisplay) {
        if (usersList[i]._id.toString() == userNotDisplay._id.toString()) {
          continue;
        }
      }
      let suspendUserClass = "";
      if (usersList[i].UserStatus) {
        if (usersList[i].UserStatus == _constantClient.USER_STATUS.SUSPEND)
          suspendUserClass = " suspended-user";
        else if (usersList[i].UserStatus == _constantClient.USER_STATUS.REPLACE)
          suspendUserClass = " replaced-user";
      }
      LI +=
        '<li class="' +
        suspendUserClass +
        '">' +
        usersList[i].FirstName +
        " " +
        usersList[i].LastName +
        "</li>";
    }
    if (LI) {
      page.PageContainer.find(".permit-users-list").append(LI);
    }
  }
};

_admin.GetDomainName = function () {
  let domainPromise = new Promise(function (success, failure) {
    _common.FetchApiByParam(
      {
        url: "api/admin/marketingemaildml",
        type: "post",
        data: { Operation: "selectdomains" },
      },
      function (result) {
        if (result.message == "success") {
          success(result.data);
        } else {
          failure(result);
        }
      }
    );
  });
  return domainPromise;
};

//Events Page: MarketingSettings
_admin.MarketingSettingsInit = function (page) {
  let marketingSettingPage = page.PageContainer;
  _common.FetchApiByParam(
    {
      url: "api/admin/getcompanysettings",
      type: "post",
    },
    function (result) {
      if (result && result.message == "success") {
        _initData.Company.Setting = result.data;
        let companySetting = _initData.Company.Setting;
        const marketingSettingObj = companySetting.MarketingSetting || {};
        let selectedDomainId = "";
        let selectedDomainName = "";

        /*Scroll Page Events*/
        $(window).scroll(function () {
          if (
            $("#page-body .page-content[data-uid='MarketingSettings'].active")
              .length > 0
          ) {
            var sideN = marketingSettingPage.find(
              ".marketing-setting #side-nav"
            );
            var sideNSection = marketingSettingPage.find(".marketing-setting");
            let maxScrollTop = $(document).height() - $(window).height();
            if (sideN.length > 0) {
              var sideT = sideN.offset().top;
              var winS = $(window).scrollTop();
              if (winS >= sideT) {
                sideN.find("nav").css("position", "fixed");
              } else {
                sideN.find("nav").css("position", "relative");
              }
              var navL = sideN.find(".nav-link").length;
              for (i = 1; i <= navL; i++) {
                var itemN = sideNSection.find("#item-" + i);
                var itemTop = itemN.offset().top; // - 40;
                //if (winS >= itemTop) {
                if (winS + 100 >= itemTop || winS == maxScrollTop) {
                  sideN.find(".nav-link").removeClass("active");
                  sideN
                    .find('.nav-link[href="#item-' + i + '"]')
                    .addClass("active");
                }
              }
            }
          }
        });

        //#consumed Company Domain calculation

        let licenseSection = marketingSettingPage.find(".license-sec");
        if (licenseSection) {
          let license =
            _initData.Company.Setting.MarketingSetting.NoOfDomainLicense || 0;
          let licenseConsumed =
            _initData.Company.Setting.MarketingSetting
              .NoOfDomainLicenseConsumed || 0;
          if (license > 0) {
            let percent = (licenseConsumed / license) * 100;
            let decvalue = 250 - parseInt(2.5 * percent);
            let incvalue = parseInt(parseInt(2.5 * percent));
            licenseSection
              .find("#svgLicenseConsumed .pathcircle")
              .attr("stroke-dasharray", [incvalue, decvalue].join(","));
            licenseSection
              .find(".lblLicenseCount")
              .text(
                _initData.Company.Setting.MarketingSetting.NoOfDomainLicense ||
                  0
              );
            licenseSection
              .find(".lblLicenseConsumedCount")
              .text(
                _initData.Company.Setting.MarketingSetting
                  .NoOfDomainLicenseConsumed || 0
              );
          }
          if (license == licenseConsumed)
            marketingSettingPage
              .find(".btnRequestForCompanyDomain")
              .css("opacity", "0.6");
        }

        //#region Company Domain Settings

        let modalCompanyDomain = marketingSettingPage.find("#newDomain");
        let modalRequestForDomain = marketingSettingPage.find(
          "#newRequestforDomain"
        );

        modalCompanyDomain.on("change keypress", ".txtDomainName", function (
          e
        ) {
          let txtBoxDomainName = $(this);
          if (!_common.ValidateDomainName(txtBoxDomainName.val())) {
            _common.ValidateAddMsg(
              txtBoxDomainName,
              "Valid domain name required e.g. gmail.com, yahoo.com etc."
            );
          } else {
            if (e.type == "change") {
              let domainID = modalCompanyDomain.DomainID
                ? modalCompanyDomain.DomainID
                : undefined;
              marketingSettingPage
                .CheckDuplicateDomainName(domainID, txtBoxDomainName.val())
                .then(
                  function (result) {
                    if (result == true) {
                      _common.ValidateAddMsg(
                        txtBoxDomainName,
                        "Domain Name already exists."
                      );
                    }
                  },
                  function (result) {}
                );
            }
          }
          modalCompanyDomain
            .find(".btnSaveCompanyDomain")
            .attr("disabled", "disabled");
          modalCompanyDomain
            .find(".lblApprovedStatus")
            .text("Pending")
            .removeClass("text-success")
            .addClass("text-danger");
          modalCompanyDomain
            .find(".btnVerifyDomainByTestEmail")
            .removeAttr("disabled");
        });
        modalCompanyDomain.on(
          "change keypress",
          ".txtAmazonAccessKeyId,.txtAmazonSecretKey,.txtAmazonRegion",
          function () {
            modalCompanyDomain
              .find(".btnSaveCompanyDomain")
              .attr("disabled", "disabled");
            modalCompanyDomain
              .find(".lblApprovedStatus")
              .text("Pending")
              .removeClass("text-success")
              .addClass("text-danger");
            modalCompanyDomain
              .find(".btnVerifyDomainByTestEmail")
              .removeAttr("disabled");
          }
        );

        marketingSettingPage.on(
          "click",
          ".tblDomain .optMakeDemainDefault",
          function () {
            let sdata = {
              Operation: "makedefault",
              DomainID: _controls.ToEJSON(
                "objectid",
                $(this).closest("tr").attr("data-id")
              ),
            };
            _common.FetchApiByParam(
              {
                url: "api/admin/marketingdomaindml",
                type: "post",
                data: sdata,
              },
              function (result) {
                if (result.message == "success") {
                  marketingSettingPage.ShowDomainList();
                  // swal('Saved Successfully', { icon: "success" });
                } else {
                  swal(result.message, { icon: "error" });
                }
              }
            );
          }
        );

        marketingSettingPage.find(".btnAddCompanyDomain").unbind("click");
        marketingSettingPage.find(".btnAddCompanyDomain").click(function () {
          modalCompanyDomain
            .find(".modal-header .modal-title")
            .text("Add New Domain");
          modalCompanyDomain.modal("show");
          marketingSettingPage.ResetDomainForm();
        });

        marketingSettingPage
          .find(".btnRequestForCompanyDomain")
          .unbind("click");
        marketingSettingPage
          .find(".btnRequestForCompanyDomain")
          .click(function () {
            let license =
              _initData.Company.Setting.MarketingSetting.NoOfDomainLicense || 0;
            let licenseConsumed =
              _initData.Company.Setting.MarketingSetting
                .NoOfDomainLicenseConsumed || 0;
            let remainingLicense = license - licenseConsumed;
            if (license == licenseConsumed) {
              swal("Please buy additional licence to add extra domains.", {
                icon: "info",
              });
              return;
            } else {
              modalRequestForDomain.find(".input-field input").val("");
              modalRequestForDomain.modal("show");
              marketingSettingPage.ResetDomainForm();
              marketingSettingPage.find(".btnAddMoreDomain").unbind("click");
              modalRequestForDomain
                .find(".btnAddMoreDomain")
                .on("click", function () {
                  if (
                    modalRequestForDomain.find(".input-field").length <
                    remainingLicense
                  ) {
                    modalRequestForDomain
                      .find(".appendDiv")
                      .append(
                        `<div class="form-group vld-parent input-field"><input type="text" title="Domain Name (amazon.com,gmail.com)" class="form-control form-control-sm msp-ctrl txtDomainName mt-3" data-required /></div>`
                      );
                  }
                });
              marketingSettingPage.find(".btnDltMoreDomain").unbind("click");
              modalRequestForDomain
                .find(".btnDltMoreDomain")
                .on("click", function () {
                  if (modalRequestForDomain.find(".input-field").length > 1) {
                    modalRequestForDomain.find(".input-field:last").remove();
                  }
                });
            }
          });

        modalRequestForDomain.find(".btnSendRequestForDomain").unbind("click");
        modalRequestForDomain
          .find(".btnSendRequestForDomain")
          .click(function () {
            _common.Loading(true);
            let sdata = {};
            sdata.DomainName = [];
            let eachLength = modalRequestForDomain.find(".txtDomainName")
              .length;
            let eachCounter = 0;
            let domainID = modalRequestForDomain.DomainID
              ? modalRequestForDomain.DomainID
              : undefined;
            modalRequestForDomain.find(".txtDomainName").each(function () {
              let textInput = $(this);
              if (!_common.ValidateDomainName(textInput.val())) {
                _common.Loading(false);
                _common.ValidateAddMsg(
                  textInput,
                  "Valid domain name required e.g. gmail.com, yahoo.com etc."
                );
                return;
              }
              marketingSettingPage
                .CheckDuplicateDomainName(domainID, textInput.val())
                .then(
                  function (result) {
                    if (result == true) {
                      _common.Loading(false);
                      _common.ValidateAddMsg(
                        textInput,
                        "Domain Name already exists."
                      );
                    } else {
                      // if (_common.Validate(modalRequestForDomain)) {
                      sdata.DomainName.push(textInput.val());
                      if (eachCounter + 1 === eachLength) {
                        sdata.SenderEmail = "support@optimiser.com";
                        sdata.UserEmailId = _initData.User.Email;
                        sdata.UserName =
                          _initData.User.FirstName +
                          " " +
                          _initData.User.LastName;
                        _common.FetchApiByParam(
                          {
                            url: "api/admin/sendemailtosupport",
                            type: "post",
                            data: sdata,
                          },
                          function (result) {
                            _common.Loading(false);
                            modalRequestForDomain.modal("hide");
                            if (result.message == "success")
                              swal(
                                "Thank you for submitting your request. Optimiser support team will get back to you with next steps within 48 business hours.",
                                { icon: "success" }
                              );
                            else
                              swal(
                                "Couldn’t connect with the server, Please try after sometime",
                                { icon: "error" }
                              );
                          }
                        );
                      }
                      // }
                      _common.Loading(false);
                    }
                    eachCounter++;
                  },
                  function (result) {
                    console.log("error : " + result);
                  }
                );
            });
          });

        modalCompanyDomain
          .find(".btnVerifyDomainByTestEmail")
          .click(function (result) {
            _common.Loading(true);
            let thisBtn = $(this);
            if (
              !_common.ValidateDomainName(
                modalCompanyDomain.find(".txtDomainName").val()
              )
            ) {
              _common.Loading(false);
              _common.ValidateAddMsg(
                modalCompanyDomain.find(".txtDomainName"),
                "Valid domain name required e.g. gmail.com, yahoo.com etc."
              );
              return;
            }
            let domainID = modalCompanyDomain.DomainID
              ? modalCompanyDomain.DomainID
              : undefined;
            marketingSettingPage
              .CheckDuplicateDomainName(
                domainID,
                modalCompanyDomain.find(".txtDomainName").val()
              )
              .then(
                function (result) {
                  if (result == true) {
                    _common.Loading(false);
                    _common.ValidateAddMsg(
                      modalCompanyDomain.find(".txtDomainName"),
                      "Domain Name already exists."
                    );
                  } else {
                    if (_common.Validate(modalCompanyDomain)) {
                      modalCompanyDomain
                        .find(".lblApprovedStatus")
                        .text("Pending")
                        .removeClass("text-success")
                        .addClass("text-danger");
                      let sdata = {};
                      sdata.SenderEmail =
                        "test@" +
                        modalCompanyDomain.find(".txtDomainName").val();
                      sdata.UserEmailId = _initData.User.Email;
                      sdata.AccessKeyID = modalCompanyDomain
                        .find(".txtAmazonAccessKeyId")
                        .val();
                      sdata.SecretAccessKeyID = modalCompanyDomain
                        .find(".txtAmazonSecretKey")
                        .val();
                      sdata.Region = modalCompanyDomain
                        .find(".txtAmazonRegion")
                        .val();
                      sdata.MailBody = `<body style="background-color: #eee;">
                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#eee;">
                                    <tr>
                                        <td align="center" valign="top">
                                            <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#fff;">
                                                <tr>
                                                    <td align="left" valign="top" bgcolor="#ffffff" style="padding-top:16px; padding-bottom:12px; padding-left: 20px; padding-right: 20px; background-color: #fff; border-bottom: 1px solid #dee2e6;">
                                                        <img src="${_constantClient.SITE_URL}/images/MainLogo.png"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top" bgcolor="#fff" style="background-color: #fff; text-align: left; padding-right: 20px; padding-left: 20px; font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#000000;">
                                                        <p style="margin-top: 1em;">Hello,</p>
                                                        <p>Your domain has been configured with Optimiser.</p>
                                                        <br>
                                                        <p style="margin-bottom: 1em;">
                                                            Thanks,<br>
                                                            Optimiser team
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top" bgcolor="#1d3c73" style=" border-top: 1px solid #dee2e6; background-color: #1d3c73 ;">
                                                        <p style="font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#fff; margin:1.8em; font-weight:normal;">
                                                            <a href="https://www.optimiser.com/" style="color:#fff; text-decoration:underline; ">www.optimiser.com</a> | <a href="mailto:support@optimiser.com" style="color:#fff; text-decoration:underline; ">support@optimiser.com</a> | 
                                                            +44 (0)203 972 1666
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                </body>`;
                      _common.FetchApiByParam(
                        {
                          url: "api/admin/marketingTestAWSConfig",
                          type: "post",
                          data: sdata,
                        },
                        function (result) {
                          _common.Loading(false);
                          if (result.message == "success") {
                            modalCompanyDomain
                              .find(".lblApprovedStatus")
                              .text("Approved")
                              .removeClass("text-danger")
                              .addClass("text-success");
                            modalCompanyDomain
                              .find(".btnSaveCompanyDomain")
                              .removeAttr("disabled");
                            modalCompanyDomain
                              .find(".btnSaveCompanyDomain")
                              .removeAttr("disabled");
                            thisBtn.attr("disabled", "disabled");
                            swal("Validated successfully", { icon: "success" });
                          } else {
                            modalCompanyDomain
                              .find(".lblApprovedStatus")
                              .text("Validation Failed")
                              .removeClass("text-success")
                              .addClass("text-danger");
                            modalCompanyDomain
                              .find(".btnSaveCompanyDomain")
                              .attr("disabled", "disabled");
                            thisBtn.removeAttr("disabled");
                            swal(result.message, { icon: "error" });
                          }
                        }
                      );
                    }

                    _common.Loading(false);
                  }
                },
                function (result) {}
              );
          });

        modalCompanyDomain.find(".btnSaveCompanyDomain").unbind("click");
        modalCompanyDomain.find(".btnSaveCompanyDomain").click(function () {
          _common.Loading(true);
          if (
            !_common.ValidateDomainName(
              modalCompanyDomain.find(".txtDomainName").val()
            )
          ) {
            _common.Loading(false);
            _common.ValidateAddMsg(
              modalCompanyDomain.find(".txtDomainName"),
              "Valid domain name required e.g. gmail.com, yahoo.com etc."
            );
            return;
          }
          if (_common.Validate(modalCompanyDomain)) {
            let domainID = modalCompanyDomain.DomainID
              ? modalCompanyDomain.DomainID
              : undefined;
            marketingSettingPage
              .CheckDuplicateDomainName(
                domainID,
                modalCompanyDomain.find(".txtDomainName").val()
              )
              .then(
                function (result) {
                  if (result == true) {
                    _common.Loading(false);
                    _common.ValidateAddMsg(
                      modalCompanyDomain.find(".txtDomainName"),
                      "Domain Name already exists."
                    );
                  } else {
                    marketingSettingPage.SaveDomain();
                  }
                },
                function (result) {}
              );
          }
        });

        marketingSettingPage.find(".tblDomain").off("click", ".btnEditDomain");
        marketingSettingPage
          .find(".tblDomain")
          .on("click", ".btnEditDomain", function () {
            _common.Loading(true);
            let domainID = $(this).closest("tr").attr("data-id");
            modalCompanyDomain
              .find(".modal-header .modal-title")
              .text("Update Domain");
            marketingSettingPage.EditMarketingDomain(domainID);
          });

        marketingSettingPage
          .find(".tblDomain")
          .off("click", ".btnDeleteDomain");
        marketingSettingPage
          .find(".tblDomain")
          .on("click", ".btnDeleteDomain", function () {
            let domainID = $(this).closest("tr").attr("data-id");
            marketingSettingPage.RemoveDomain(domainID);
          });

        marketingSettingPage.SaveDomain = function () {
          let MarketingDomainObj = {};
          MarketingDomainObj.DomainName = modalCompanyDomain
            .find(".txtDomainName")
            .val();
          MarketingDomainObj.AmazonAccessKeyId = modalCompanyDomain
            .find(".txtAmazonAccessKeyId")
            .val();
          MarketingDomainObj.AmazonSecretKey = modalCompanyDomain
            .find(".txtAmazonSecretKey")
            .val();
          MarketingDomainObj.AmazonRegion = modalCompanyDomain
            .find(".txtAmazonRegion")
            .val();
          MarketingDomainObj.Approved =
            modalCompanyDomain.find(".lblApprovedStatus").text() == "Approved"
              ? true
              : false;
          MarketingDomainObj.IsDefault = false;
          MarketingDomainObj.IsActive = true;

          if (modalCompanyDomain.DomainID) {
            MarketingDomainObj._id = _controls.ToEJSON(
              "objectid",
              modalCompanyDomain.DomainID
            );
            MarketingDomainObj.ModifiedBy = _controls.ToEJSON(
              "objectid",
              _initData.User._id
            );
            MarketingDomainObj.ModifiedDate = new Date();
          } else {
            MarketingDomainObj.CreatedBy = _controls.ToEJSON(
              "objectid",
              _initData.User._id
            );
            MarketingDomainObj.CreatedDate = new Date();
          }

          marketingSettingObj.Domains = marketingSettingObj.Domains || [];
          marketingSettingObj.Domains.push(MarketingDomainObj);

          let sdata = {
            DomainData: MarketingDomainObj,
            Operation: "insert",
          };
          if (modalCompanyDomain.DomainID) {
            sdata.Operation = "update";
          }

          _common.FetchApiByParam(
            {
              url: "api/admin/marketingdomaindml",
              type: "post",
              data: sdata,
            },
            function (result) {
              _common.Loading(false);
              if (result.message == "success") {
                let mktDomainID = "";
                if (sdata.Operation == "update") {
                  mktDomainID = modalCompanyDomain.DomainID;
                } else {
                  if (result.data && result.data._id) {
                    mktDomainID = result.data._id;
                  }
                }

                modalCompanyDomain.modal("hide");
                swal("Saved Successfully.", { icon: "success" });
                marketingSettingPage.ResetDomainForm();
                marketingSettingPage.ShowDomainList();

                if (mktDomainID) {
                  mktDomainID = _controls.ToEJSON("objectid", mktDomainID);
                  _common.ListSchemaDML(
                    _constantClient.LIST_SCHEMA_TYPE
                      .MARKETING_REGISTERED_DOMAIN,
                    "Manual",
                    "ID",
                    mktDomainID,
                    MarketingDomainObj.DomainName,
                    sdata.Operation
                  );
                }
              } else {
                swal(result.message, { icon: "error" });
              }
            }
          );
        };

        marketingSettingPage.ResetDomainForm = function () {
          modalCompanyDomain.find('input[type="text"]').val("");
          modalCompanyDomain
            .find(".lblApprovedStatus")
            .attr("data-approved", "false")
            .text("Pending")
            .removeClass("text-success")
            .addClass("text-danger");
          modalCompanyDomain.find(".txtDomainName").focus();
          modalCompanyDomain.find(".vld-message").remove();
          modalCompanyDomain.find(".has-error").removeClass("has-error");
          modalCompanyDomain.DomainID = undefined;
        };

        marketingSettingPage.ShowDomainList = function () {
          _common.Loading(true);
          _common.FetchApiByParam(
            {
              url: "api/admin/marketingdomaindml",
              type: "post",
              data: { Operation: "select" },
            },
            function (result) {
              if (result.message == "success" && result.data) {
                let mktDomains = result.data;
                marketingSettingPage.find(".tblDomain tbody").html("");
                for (let i = 0; i < mktDomains.length; i++) {
                  if (mktDomains[i]) {
                    let trDomain = '<tr data-id="' + mktDomains[i]._id + '">';
                    trDomain += "<td>" + mktDomains[i].DomainName + "</td>";
                    trDomain +=
                      "<td>" +
                      (mktDomains[i].Approved == true
                        ? "Approved"
                        : "Pending") +
                      "</td>";
                    trDomain += "<td>";
                    trDomain += '<div class="custom-control custom-radio">';
                    trDomain += '<label class="text-black mr-2">';
                    trDomain +=
                      '<input type="radio" name="domainDefault" class="optMakeDemainDefault" ' +
                      (mktDomains[i].IsDefault == true ? "checked" : "") +
                      ">";
                    trDomain += "</label>";
                    trDomain += "</div>";
                    trDomain += "</td>";
                    //trDomain += '<td><button class="btn btn-link text-danger pl-1 pr-1 btnDeleteDomain d-none">Delete</button>';
                    //trDomain += '<button class="btn btn-link text-primary pr-1 pl-1 btnEditDomain">Edit</button>';
                    //trDomain += '</td>';
                    trDomain += "</tr>";
                    marketingSettingPage
                      .find(".tblDomain tbody")
                      .append(trDomain);
                    if (mktDomains[i].IsDefault) {
                      selectedDomainId = mktDomains[i]._id;
                      selectedDomainName = mktDomains[i].DomainName;
                      page.param.SelectedDomainID = mktDomains[i]._id;
                      marketingSettingPage.ShowRegisteredEmail(
                        selectedDomainId
                      );
                      marketingSettingPage.GetMarketingCampaignCategoryList(
                        selectedDomainId
                      );
                      marketingSettingPage.GetMarketingTagList(
                        selectedDomainId
                      );
                      marketingSettingPage.ShowUnsubscribePage(
                        selectedDomainId
                      );
                    }
                  }
                }
                setTimeout(function () {
                  _common.Loading(false);
                }, 500);
              } else {
                swal("Domain not found", { icon: "error" });
                _common.Loading(false);
              }
            }
          );
        };

        marketingSettingPage.EditMarketingDomain = function (domainID) {
          _common.FetchApiByParam(
            {
              url: "api/admin/marketingdomaindml",
              type: "post",
              data: { DomainID: domainID, Operation: "domaindata" },
            },
            function (result) {
              _common.Loading(false);
              if (result.message == "success") {
                marketingSettingPage.ResetDomainForm();
                modalCompanyDomain.DomainID = domainID;
                if (result.data) {
                  modalCompanyDomain.modal("show");
                  modalCompanyDomain
                    .find(".txtDomainName")
                    .val(result.data.DomainName || "");
                  modalCompanyDomain
                    .find(".txtAmazonAccessKeyId")
                    .val(result.data.AmazonAccessKeyId || "");
                  modalCompanyDomain
                    .find(".txtAmazonSecretKey")
                    .val(result.data.AmazonSecretKey || "");
                  modalCompanyDomain
                    .find(".txtAmazonRegion")
                    .val(result.data.AmazonRegion || "");
                  if ((result.data.Approved || false) == true) {
                    modalCompanyDomain
                      .find(".lblApprovedStatus")
                      .text("Approved")
                      .removeClass("text-danger")
                      .addClass("text-success");
                  } else {
                    modalCompanyDomain
                      .find(".lblApprovedStatus")
                      .text("Pending")
                      .removeClass("text-success")
                      .addClass("text-danger");
                  }
                }
              } else {
                swal(result.message, { icon: "error" });
              }
            }
          );
        };

        marketingSettingPage.CheckDuplicateDomainName = function (
          domainID,
          domainNameVal
        ) {
          var checkDuplicateDomainName = new Promise(function (accept, reject) {
            let sdata = {
              Operation: "checkduplicate",
              DomainName: domainNameVal,
            };
            if (domainID) {
              sdata.DomainID = _controls.ToEJSON("objectid", domainID);
            }
            _common.FetchApiByParam(
              {
                url: "api/admin/marketingdomaindml",
                type: "post",
                data: sdata,
              },
              function (result) {
                if (result.message == "success") {
                  if (result.exists) {
                    accept(true);
                  } else {
                    accept(false);
                  }
                } else {
                  accept(false);
                }
              }
            );
          });
          return checkDuplicateDomainName;
        };

        marketingSettingPage.RemoveDomain = function (domainID) {
          _common
            .ConfirmDelete({
              message:
                "If you delete this it will be permanently removed for all users.",
            })
            .then((confirm) => {
              if (confirm) {
                _common.Loading(true);
                let sdata = {
                  Operation: "deletedomain",
                  DomainID: _controls.ToEJSON("objectid", domainID),
                };
                _common.FetchApiByParam(
                  {
                    url: "api/admin/marketingdomaindml",
                    type: "post",
                    data: sdata,
                  },
                  function (result) {
                    _common.Loading(false);
                    if (result.message == "success") {
                      swal("Domain has been removed successfully", {
                        icon: "success",
                      });
                      marketingSettingPage.ShowDomainList();
                      _common.ListSchemaDML(
                        _constantClient.LIST_SCHEMA_TYPE
                          .MARKETING_REGISTERED_DOMAIN,
                        "Manual",
                        "ID",
                        sdata.DomainID,
                        "",
                        "delete"
                      );
                    } else {
                      swal(result.message, { icon: "error" });
                    }
                  }
                );
              }
            });
        };

        marketingSettingPage.ShowDomainList();

        //#endregion

        //#region Marketing Email Settings

        let modalMarketingEmail = marketingSettingPage.find("#newEmail");

        marketingSettingPage.find(".btnSaveMarketingEmail").click(function () {
          if ($(this).attr("data-disable") == undefined) {
            //To disable double click
            let btn = $(this);
            btn.attr("data-disable", true);
            setTimeout(function () {
              btn.removeAttr("data-disable");
            }, 400);

            _common.Loading(true);
            let emailId =
              modalMarketingEmail
                .find(".txtMarketingSendingEmail")
                .val()
                .trim() +
              "@" +
              modalMarketingEmail.find(".txtMarketingDomain").val().trim();
            if (!_common.isValidEmailAddress(emailId)) {
              _common.Loading(false);
              _common.ValidateAddMsg(
                modalMarketingEmail.find(".txtMarketingSendingEmail"),
                "Email Id not valid."
              );
              return;
            }
            if (_common.Validate(modalMarketingEmail)) {
              let domainDBID = modalMarketingEmail.Email_DB_ID
                ? modalMarketingEmail.Email_DB_ID
                : undefined;
              marketingSettingPage
                .CheckDuplicateRegisteredEmailId(
                  modalMarketingEmail.Email_DB_ID,
                  emailId,
                  selectedDomainId
                )
                .then(
                  function (result) {
                    if (result == true) {
                      _common.Loading(false);
                      _common.ValidateAddMsg(
                        modalMarketingEmail.find(".txtMarketingSendingEmail"),
                        "Email Id already exists."
                      );
                      //swal('Email Id already exists.', { icon: 'error' });
                    } else {
                      marketingSettingPage.SaveRegisteredEmail(
                        selectedDomainId
                      );
                    }
                  },
                  function (result) {}
                );
            }
            _common.Loading(false);
          }
        });

        marketingSettingPage
          .find(".tblMarketingRegisteredEmails")
          .on("click", ".btnEditRegisteredEmail", function () {
            marketingSettingPage.ResetRegisteredEmailForm();
            modalMarketingEmail
              .find(".modal-header .modal-title")
              .text("Update Email");
            let Email_DB_ID = $(this).closest("tr").attr("data-id");
            marketingSettingPage.EditMarketingRegisteredEmail(Email_DB_ID);
          });

        marketingSettingPage
          .find(".tblMarketingRegisteredEmails")
          .on("click", ".btnDeleteRegisteredEmail", function () {
            let emailID = $(this).closest("tr").attr("data-id");
            marketingSettingPage.RemoveEmailID(emailID);
          });

        marketingSettingPage.find(".btnAddMarketingEmail").unbind("click");
        marketingSettingPage.find(".btnAddMarketingEmail").click(function () {
          _common.Loading(true);
          modalMarketingEmail
            .find(".modal-header .modal-title")
            .text("Add New Email");
          marketingSettingPage.ResetRegisteredEmailForm();
          modalMarketingEmail
            .find(".txtMarketingDomain")
            .val("")
            .val(selectedDomainName);
          _user.GetUsersList(function (result) {
            if (result.status == "ok") {
              let userSelected = _initData.User;
              let usersList = result.data;
              let option = "";
              for (let i = 0; i < usersList.length; i++) {
                option +=
                  '<option value="' +
                  usersList[i]._id +
                  '">' +
                  usersList[i].FirstName +
                  " " +
                  usersList[i].LastName +
                  "</option>";
              }
              setTimeout(function () {
                modalMarketingEmail
                  .find(".ddlUsersList")
                  .html("")
                  .append(option)
                  .multiselect({
                    nonSelectedText: "",
                    enableCaseInsensitiveFiltering: true,
                    includeSelectAllOption: true,
                    onDropdownShow: function (event) {
                      modalMarketingEmail
                        .find(".btn.multiselect-clear-filter")
                        .trigger("click");
                    },
                  });
                modalMarketingEmail
                  .find(".ddlUsersList")
                  .multiselect("refresh");
                modalMarketingEmail
                  .find(".ddlUsersList")
                  .multiselect("select", userSelected._id, true);
              }, 100);
            }
            modalMarketingEmail.modal("show");
          });
          _common.Loading(false);
        });
        marketingSettingPage.SaveRegisteredEmail = function (domainId) {
          let sdata = {
            Operation: modalMarketingEmail.Email_DB_ID ? "update" : "insert",
            Data: {
              SenderName: modalMarketingEmail
                .find(".txtMarketingSenderName")
                .val(),
              EmailId:
                modalMarketingEmail
                  .find(".txtMarketingSendingEmail")
                  .val()
                  .trim() +
                "@" +
                modalMarketingEmail.find(".txtMarketingDomain").val().trim(),
              DomainId: _controls.ToEJSON("objectid", domainId),
              EmailIdWithoutDomain: modalMarketingEmail
                .find(".txtMarketingSendingEmail")
                .val(),
              AllowedUsers: _controls.ToEJSON(
                "multilookup",
                modalMarketingEmail.find(".ddlUsersList").val()
              ),
            },
          };
          // sdata.Data.AllowedUsers.push(_controls.ToEJSON('objectid', _initData.User._id));
          if (modalMarketingEmail.Email_DB_ID) {
            sdata.Data._id = _controls.ToEJSON(
              "objectid",
              modalMarketingEmail.Email_DB_ID
            );
          }
          _common.FetchApiByParam(
            {
              url: "api/admin/marketingemaildml",
              type: "post",
              data: sdata,
            },
            function (result) {
              _common.Loading(false);
              if (result.message == "success") {
                if (modalMarketingEmail.Email_DB_ID) {
                  swal("Email id has been updated successfully.", {
                    icon: "success",
                  });
                  mktEmail_DB_ID = modalMarketingEmail.Email_DB_ID;
                } else {
                  swal("Email id has been added successfully", {
                    icon: "success",
                  });
                  if (result.data && result.data._id) {
                    mktEmail_DB_ID = result.data._id;
                  }
                }
                modalMarketingEmail.modal("hide");
                marketingSettingPage.ShowRegisteredEmail(domainId);
              } else {
                swal(result.message, { icon: "error" });
              }
            }
          );
        };
        marketingSettingPage.ShowRegisteredEmail = function (domainId) {
          _common.FetchApiByParam(
            {
              url: "api/admin/marketingemaildml",
              type: "post",
              data: { Operation: "selectemails", DomainID: domainId },
            },
            function (result) {
              if (result.message == "success") {
                let mktRegisteredEmails = result.data;
                marketingSettingPage
                  .find(".tblMarketingRegisteredEmails tbody")
                  .html("");
                for (let i = 0; i < mktRegisteredEmails.length; i++) {
                  if (mktRegisteredEmails[i]) {
                    let trEmail =
                      '<tr data-id="' + mktRegisteredEmails[i]._id + '">';
                    trEmail +=
                      "<td>" + mktRegisteredEmails[i].SenderName + "</td>";
                    trEmail +=
                      "<td>" + mktRegisteredEmails[i].EmailId + "</td>";
                    trEmail +=
                      '<td><button data-toggle="tooltip" data-placement="right" title="" data-original-title="Edit Email" class="btn btn-sm btn-outline-primary ml-1 btnEditRegisteredEmail"><i class="fas fa-edit"></i></button>';
                    trEmail +=
                      '<button disabled data-toggle="tooltip" data-placement="right" title="" data-original-title="Delete Email" class="btn btn-sm btn-outline-danger ml-1 btnDeleteRegisteredEmail d-none"><i class="fas fa-trash-alt"></i></button>';
                    trEmail += "</td>";
                    trEmail += "</tr>";
                    marketingSettingPage
                      .find(".tblMarketingRegisteredEmails tbody")
                      .append(trEmail);
                  }
                }
              }
            }
          );
        };

        marketingSettingPage.ResetRegisteredEmailForm = function () {
          modalMarketingEmail.find('input[type="text"]').val("");
          modalMarketingEmail.find("select").val("");
          modalMarketingEmail.find(".vld-message").remove();
          modalMarketingEmail.find(".has-error").removeClass("has-error");
          modalMarketingEmail.Email_DB_ID = undefined;
        };

        marketingSettingPage.EditMarketingRegisteredEmail = function (
          Email_DB_ID
        ) {
          _common.Loading(true);
          modalMarketingEmail
            .find(".txtMarketingDomain")
            .val("")
            .val(selectedDomainName);
          let sdata = {
            Email_DB_ID: _controls.ToEJSON("objectid", Email_DB_ID),
            Operation: "emaildata",
          };
          _common.FetchApiByParam(
            {
              url: "api/admin/marketingemaildml",
              type: "post",
              data: sdata,
            },
            function (emailResult) {
              _common.Loading(false);
              modalMarketingEmail.Email_DB_ID = Email_DB_ID;
              if (emailResult.message == "success") {
                modalMarketingEmail.Email_DB_ID = Email_DB_ID;
                if (emailResult.data) {
                  modalMarketingEmail
                    .find(".txtMarketingSendingEmail")
                    .val(emailResult.data.EmailIdWithoutDomain || "");
                  modalMarketingEmail
                    .find(".txtMarketingDomain")
                    .val(emailResult.data.EmailId.split("@")[1] || "");
                  modalMarketingEmail
                    .find(".txtMarketingSenderName")
                    .val(emailResult.data.SenderName || "");
                  _user.GetUsersList(function (result) {
                    if (result.status == "ok") {
                      let usersList = result.data;
                      let option = "";
                      for (let i = 0; i < usersList.length; i++) {
                        option +=
                          '<option value="' +
                          usersList[i]._id +
                          '">' +
                          usersList[i].FirstName +
                          " " +
                          usersList[i].LastName +
                          "</option>";
                      }
                      setTimeout(function () {
                        modalMarketingEmail
                          .find(".ddlUsersList")
                          .html("")
                          .append(option)
                          .multiselect({
                            nonSelectedText: "",
                            enableCaseInsensitiveFiltering: true,
                            includeSelectAllOption: true,
                            onDropdownShow: function (event) {
                              modalMarketingEmail
                                .find(".btn.multiselect-clear-filter")
                                .trigger("click");
                            },
                          });
                        modalMarketingEmail
                          .find(".ddlUsersList")
                          .multiselect("refresh");
                        if (emailResult.data.AllowedUsers)
                          modalMarketingEmail
                            .find(".ddlUsersList")
                            .multiselect(
                              "select",
                              emailResult.data.AllowedUsers,
                              true
                            );
                      }, 100);
                    }
                    modalMarketingEmail.modal("show");
                  });
                }
              } else {
                swal(result.message, { icon: "error" });
              }
            }
          );
          _common.Loading(false);
        };

        marketingSettingPage.CheckDuplicateRegisteredEmailId = function (
          email_DB_ID,
          emailId,
          domainId
        ) {
          var checkDuplicateEmail = new Promise(function (success, failure) {
            let sdata = {
              Operation: "checkduplicate",
              EmailId: emailId,
              DomainId: domainId,
            };
            if (email_DB_ID) {
              sdata.Email_DB_ID = _controls.ToEJSON("objectid", email_DB_ID);
            }

            _common.FetchApiByParam(
              {
                url: "api/admin/marketingemaildml",
                type: "post",
                data: sdata,
              },
              function (result) {
                if (result.message == "success") {
                  if (result.data && _common.isEmptyObj(result.data) == false) {
                    success(true);
                  } else {
                    success(false);
                  }
                } else {
                  success(false);
                }
              }
            );
          });
          return checkDuplicateEmail;
        };

        marketingSettingPage.RemoveEmailID = function (email_DB_ID) {
          _common
            .ConfirmDelete({
              message: "If you delete this it will be permanently remove.",
            })
            .then((confirm) => {
              if (confirm) {
                _common.Loading(true);
                let sdata = {
                  Operation: "deleteemailid",
                  Email_DB_ID: _controls.ToEJSON("objectid", email_DB_ID),
                };
                _common.FetchApiByParam(
                  {
                    url: "api/admin/marketingemaildml",
                    type: "post",
                    data: sdata,
                  },
                  function (result) {
                    _common.Loading(false);
                    if (result.message == "success") {
                      swal("Email id has been removed successfully.", {
                        icon: "success",
                      });
                      marketingSettingPage.ShowRegisteredEmail();
                    } else {
                      swal(result.message, { icon: "error" });
                    }
                  }
                );
              }
            });
        };

        //#endregion

        //#region Marketing Category

        let modalMarketingCategory = marketingSettingPage.find("#newCategory");

        marketingSettingPage
          .find(".btnAddMarketingCampaignCategory")
          .click(function () {
            modalMarketingCategory
              .find(".modal-header .modal-title")
              .text("Add New Category");
            marketingSettingPage.ResetCategoryForm();
            modalMarketingCategory.modal("show");
          });

        marketingSettingPage
          .find(".tblMktCampaignCategory")
          .on("click", ".btnEditMktCampaignCategory", function () {
            modalMarketingCategory
              .find(".modal-header .modal-title")
              .text("Update Category");
            marketingSettingPage.ResetCategoryForm();
            let cTr = $(this).closest("tr");
            modalMarketingCategory.modal("show");
            modalMarketingCategory
              .find(".txtMarketingCategory")
              .val(cTr.find(".tdValue").text().trim());
            modalMarketingCategory.CategoryKey = cTr.attr("data-id");
          });

        marketingSettingPage
          .find(".tblMktCampaignCategory")
          .on("click", ".btnDeleteMktCampaignCategory", function () {
            _common
              .ConfirmDelete({
                message: "If you delete this it will be permanently remove.",
              })
              .then((confirm) => {
                if (confirm) {
                  _common.Loading(true);
                  let categoryKey = $(this).closest("tr").attr("data-id");
                  let categoryName = $(this)
                    .closest("tr")
                    .find(".tdValue")
                    .text();
                  _common
                    .ListSchemaDML(
                      "CampaignCategory",
                      "Auto",
                      "Number",
                      categoryKey,
                      categoryName,
                      "delete",
                      selectedDomainId
                    )
                    .then(
                      function () {
                        _common.Loading(false);
                        swal("Category has been deleted successfully", {
                          icon: "success",
                        });
                        marketingSettingPage.GetMarketingCampaignCategoryList(
                          selectedDomainId
                        );
                      },
                      function () {
                        _common.Loading(false);
                        swal(
                          "Unable to delete the category ,Please try again",
                          { icon: "error" }
                        );
                      }
                    );
                }
              });
          });

        modalMarketingCategory
          .find(".btnSaveMarketingCategory")
          .click(function () {
            if (_common.Validate(modalMarketingCategory)) {
              _common.Loading(true);
              let categoryKey = null;
              let operation = "insert";
              if (modalMarketingCategory.CategoryKey) {
                categoryKey = modalMarketingCategory.CategoryKey;
                operation = "update";
              }
              let categoryName = modalMarketingCategory
                .find(".txtMarketingCategory")
                .val();
              _common
                .ListSchemaDML(
                  "CampaignCategory",
                  "Auto",
                  "Number",
                  categoryKey,
                  categoryName,
                  "checkduplicate",
                  selectedDomainId
                )
                .then(
                  function () {
                    _common.Loading(false);
                    _common.ValidateAddMsg(
                      modalMarketingCategory.find(".txtMarketingCategory"),
                      "Category already exists."
                    );
                    //swal('Category already exists.', { icon: 'error' });
                  },
                  function (result) {
                    _common
                      .ListSchemaDML(
                        "CampaignCategory",
                        "Auto",
                        "Number",
                        categoryKey,
                        categoryName,
                        operation,
                        selectedDomainId
                      )
                      .then(
                        function () {
                          _common.Loading(false);
                          modalMarketingCategory.modal("hide");
                          if (operation == "update")
                            swal("Category has been updated successfully.", {
                              icon: "success",
                            });
                          else
                            swal("Category has been added successfully.", {
                              icon: "success",
                            });
                          marketingSettingPage.ResetCategoryForm();
                          marketingSettingPage.GetMarketingCampaignCategoryList(
                            selectedDomainId
                          );
                        },
                        function (result) {
                          _common.Loading(false);
                          if (operation == "update")
                            swal(
                              "Unable to update the category, Please try again",
                              { icon: "error" }
                            );
                          else
                            swal(
                              "Unable to add the category, Please try again",
                              { icon: "error" }
                            );
                        }
                      );
                  }
                );
            }
            _common.Loading(false);
          });

        marketingSettingPage.GetMarketingCampaignCategoryList = function (
          DomainId
        ) {
          _common.GetListSchema("CampaignCategory").then(
            function (data) {
              if (data) {
                data = data.filter((p) => p.DomainID == DomainId);
                marketingSettingPage
                  .find(".tblMktCampaignCategory tbody")
                  .html("");
                /*To Display Campaign Category in Unsubscribe Page*/
                marketingSettingPage.categorySubscription = "";
                marketingSettingPage.categorySubscription +=
                  "<ul class='pl-0' style='font-size: 13px; list-style-type: none;'><li><label><input type='checkbox' class='chkSelectAll' data-id='-1' disabled/>&nbspSelect All</label></li>";
                for (let i = 0; i < data.length; i++) {
                  let trCategory = '<tr data-id="' + data[i].Key + '">';
                  //trCategory += '<td>' + data[i].Key + '</td>';
                  trCategory +=
                    '<td class="tdValue">' + data[i].Value + "</td>";
                  trCategory +=
                    '<td><button data-toggle="tooltip" data-placement="right" title="" data-original-title="Edit Category" class="btn btn-sm btn-outline-primary ml-1 btnEditMktCampaignCategory"><i class="fas fa-edit"></i></button>';
                  trCategory +=
                    '<button data-toggle="tooltip" data-placement="right" title="" data-original-title="Delete Category" class="btn btn-sm btn-outline-danger ml-1 btnDeleteMktCampaignCategory"><i class="fas fa-trash-alt"></i></button>';
                  trCategory += "</td>";
                  trCategory += "</tr>";
                  marketingSettingPage
                    .find(".tblMktCampaignCategory tbody")
                    .append(trCategory);
                  marketingSettingPage.categorySubscription +=
                    "<li><label><input type='checkbox' class='chkSelect' data-id='" +
                    data[i].Key +
                    "' disabled/>&nbsp" +
                    data[i].Value +
                    "</label></li>";
                }
                marketingSettingPage.categorySubscription += "</ul>";
              }
              marketingSettingPage
                .find(".unsubscribePageTemplate .campaign-category-list")
                .html(marketingSettingPage.categorySubscription);
            },
            function (result) {
              swal(result.message, { icon: "error" });
            }
          );
        };

        marketingSettingPage.ResetCategoryForm = function () {
          modalMarketingCategory.find('input[type="text"]').val("");
          modalMarketingCategory.find(".vld-message").remove();
          modalMarketingCategory.find(".has-error").removeClass("has-error");
          modalMarketingCategory.CategoryKey = null;
        };

        //#endregion

        //#region Marketing Tag Settings

        let modalMarketingTag = marketingSettingPage.find("#newTag");
        marketingSettingPage.find(".btnAddMarketingTag").click(function () {
          modalMarketingTag
            .find(".modal-header .modal-title")
            .text("Add New Tag");
          marketingSettingPage.ResetTagForm();
          modalMarketingTag.modal("show");
        });

        marketingSettingPage
          .find(".tblMktTags")
          .on("click", ".btnEditMktTag", function () {
            modalMarketingTag
              .find(".modal-header .modal-title")
              .text("Update Tag");
            marketingSettingPage.ResetTagForm();
            let cTr = $(this).closest("tr");
            modalMarketingTag.modal("show");
            modalMarketingTag
              .find(".txtMarketingTagName")
              .val(cTr.find(".tdValue").text().trim());
            modalMarketingTag.TagKey = cTr.attr("data-id");
          });

        marketingSettingPage
          .find(".tblMktTags")
          .on("click", ".btnDeleteMktTag", function () {
            _common
              .ConfirmDelete({
                message: "If you delete this it will be permanently remove.",
              })
              .then((confirm) => {
                if (confirm) {
                  _common.Loading(true);
                  let tagKey = $(this).closest("tr").attr("data-id");
                  let tagName = $(this).closest("tr").find(".tdValue").text();
                  _common
                    .ListSchemaDML(
                      "MarketingTags",
                      "Auto",
                      "Number",
                      tagKey,
                      tagName,
                      "delete",
                      selectedDomainId
                    )
                    .then(
                      function () {
                        _common.Loading(false);
                        swal("Marketing tag has been deleted successfully.", {
                          icon: "success",
                        });
                        marketingSettingPage.GetMarketingTagList(
                          selectedDomainId
                        );
                      },
                      function () {
                        _common.Loading(false);
                        swal("Marketing Tag not deleted", { icon: "error" });
                      }
                    );
                }
              });
          });

        modalMarketingTag.find(".btnSaveMarketingTags").click(function () {
          if (_common.Validate(modalMarketingTag)) {
            _common.Loading(true);
            let tagKey = null;
            let operation = "insert";
            if (modalMarketingTag.TagKey) {
              tagKey = modalMarketingTag.TagKey;
              operation = "update";
            }
            let tagName = modalMarketingTag.find(".txtMarketingTagName").val();
            _common
              .ListSchemaDML(
                "MarketingTags",
                "Auto",
                "Number",
                tagKey,
                tagName,
                "checkduplicate",
                selectedDomainId
              )
              .then(
                function () {
                  _common.Loading(false);
                  _common.ValidateAddMsg(
                    modalMarketingTag.find(".txtMarketingTagName"),
                    "Marketing Tag already exists."
                  );
                  //swal('Marketing Tag already exists.', { icon: 'error' });
                },
                function (result) {
                  _common
                    .ListSchemaDML(
                      "MarketingTags",
                      "Auto",
                      "Number",
                      tagKey,
                      tagName,
                      operation,
                      selectedDomainId
                    )
                    .then(
                      function () {
                        _common.Loading(false);
                        modalMarketingTag.modal("hide");
                        if (operation == "update")
                          swal("Marketing tag has been updated successfully.", {
                            icon: "success",
                          });
                        else
                          swal("Marketing tag has been added successfully.", {
                            icon: "success",
                          });
                        marketingSettingPage.ResetTagForm();
                        marketingSettingPage.GetMarketingTagList(
                          selectedDomainId
                        );
                      },
                      function (result) {
                        _common.Loading(false);
                        if (operation == "update")
                          swal("Marketing tag not updated.", { icon: "error" });
                        else
                          swal("Marketing tag not added.", { icon: "error" });
                      }
                    );
                }
              );
          }
          _common.Loading(false);
        });

        marketingSettingPage.GetMarketingTagList = function (DomainId) {
          _common.GetListSchema("MarketingTags").then(
            function (data) {
              if (data) {
                data = data.filter((p) => p.DomainID == DomainId);
                marketingSettingPage.find(".tblMktTags tbody").html("");
                for (let i = 0; i < data.length; i++) {
                  let trTag = '<tr data-id="' + data[i].Key + '">';
                  //trCategory += '<td>' + data[i].Key + '</td>';
                  trTag += '<td class="tdValue">' + data[i].Value + "</td>";
                  trTag +=
                    '<td><button data-toggle="tooltip" data-placement="right" title="" data-original-title="Edit Tag" class="btn btn-sm btn-outline-primary ml-1 btnEditMktTag"><i class="fas fa-edit"></i></button>';
                  trTag +=
                    '<button data-toggle="tooltip" data-placement="right" title="" data-original-title="Delete Tag" class="btn btn-sm btn-outline-danger ml-1 btnDeleteMktTag"><i class="fas fa-trash-alt"></i></button>';
                  trTag += "</td>";
                  trTag += "</tr>";
                  marketingSettingPage.find(".tblMktTags tbody").append(trTag);
                }
              }
            },
            function (result) {
              swal(result.message, { icon: "error" });
            }
          );
        };

        marketingSettingPage.ResetTagForm = function () {
          modalMarketingTag.find('input[type="text"]').val("");
          modalMarketingTag.find(".vld-message").remove();
          modalMarketingTag.find(".has-error").removeClass("has-error");
          modalMarketingTag.TagKey = null;
        };

        //#endregion

        //#region Amazon Campaign Tracking Urls
        // if (_initData.Company && _initData.Company.Setting && _initData.Company.Setting.MarketingSetting) {
        //     marketingSettingPage.find('.AWSBounceURL').text((_initData.Company.Setting.MarketingSetting.AWSBouceEmailTrackUrl || ""));
        //     marketingSettingPage.find('.txtAWSBounceURL').val((_initData.Company.Setting.MarketingSetting.AWSBouceEmailTrackUrl || ""));
        //     marketingSettingPage.find('.AWSComplaintURL').text((_initData.Company.Setting.MarketingSetting.AWSComplaintEmailTrackUrl || ""));
        //     marketingSettingPage.find('.txtAWSComplaintURL').val((_initData.Company.Setting.MarketingSetting.AWSComplaintEmailTrackUrl || ""));
        // }
        // marketingSettingPage.find('.btnCopy').click(function () {
        //     let liObj = $(this).parent().find('.' + $(this).attr('data-copy')).removeClass('d-none').select();
        //     document.execCommand("copy");
        //     liObj.addClass('d-none');
        //     swal("Link copied", { "icon": "success" });
        // });
        //#endregion

        //#subscribe page setting

        let modalSendSubscribePage = marketingSettingPage.find(
          "#sendSubscribePage"
        );

        marketingSettingPage.find(".btnOpenSendSubscribePage").unbind("click");
        marketingSettingPage
          .find(".btnOpenSendSubscribePage")
          .click(function () {
            _common.FetchApiByParam(
              {
                url: "api/admin/marketingemaildml",
                type: "post",
                data: { Operation: "selectemails", DomainID: selectedDomainId },
              },
              function (result) {
                if (result.message == "success") {
                  let mktRegisteredEmails = result.data;
                  let domainList = "<option value=''></option>";
                  for (let i = 0; i < mktRegisteredEmails.length; i++) {
                    if (mktRegisteredEmails[i]) {
                      domainList +=
                        "<option data-id='" +
                        mktRegisteredEmails[i]._id +
                        "' value='" +
                        mktRegisteredEmails[i].EmailId +
                        "'>" +
                        mktRegisteredEmails[i].EmailId +
                        "</option>";
                    }
                  }
                  modalSendSubscribePage
                    .find(".ddlMarketingDomainList")
                    .html("")
                    .append(domainList);
                  modalSendSubscribePage.modal("show");
                }
              }
            );
          });

        modalSendSubscribePage.find(".txttestMail").keypress(function (event) {
          modalSendSubscribePage.find(".txttestMail").css("width", "");
          let keycode = event.keyCode ? event.keyCode : event.which;
          this.textContent = this.textContent.trim().replace(",", "");
          if (
            keycode == "13" ||
            keycode == "32" ||
            keycode == "44" ||
            keycode == "9"
          ) {
            if (!this.textContent) {
              this.textContent = "";
              return;
            }
            let renderEmails = function (email) {
              let isValid = _common.isValidCommaSepratedEmailAddress(email);
              let divElement = $(
                `<div style="display: inline-block; max-width:100%;" data-email="${email}" class="${
                  isValid ? "" : "bg-danger text-white"
                } lookup-email border btn-outline-secondary btn btn-sm mb-1 mr-1">${email}<span class="fas fa-times pl-2 remove-email"></span> </div>`
              );
              modalSendSubscribePage.find(".txttestMail").before(divElement);
            };
            renderEmails(this.textContent.trim());
            this.textContent = "";
            event.preventDefault();
          }
        });
        modalSendSubscribePage.find(".txttestMail").keydown(function (event) {
          let keycode = event.keyCode ? event.keyCode : event.which;
          if (keycode == "8" && this.textContent == "") {
            $(".txttestMail").prev().remove();
          }
        });

        modalSendSubscribePage.on("blur", ".txttestMail", function (e) {
          if (!this.textContent) {
            this.textContent = "";
            return;
          } else if (this.textContent.trim() == "" || this.textContent == ",") {
            this.textContent = "";
            return;
          }
          let renderEmails = function (email) {
            let isValid = _common.isValidCommaSepratedEmailAddress(email);
            let divElement = $(
              `<div style="display: inline-block; max-width:100%;" data-email="${email}" class="${
                isValid ? "" : "bg-danger text-white"
              } lookup-email border btn-outline-secondary btn btn-sm mb-1 mr-1">${email}<span class="fas fa-times pl-2 remove-email"></span></div>`
            );
            modalSendSubscribePage.find(".txttestMail").before(divElement);
          };
          renderEmails(this.textContent.trim());
          this.textContent = "";
        });

        modalSendSubscribePage.on("dblclick", ".lookup-email", function () {
          let lookupEmail = $(this);
          lookupEmail.text(lookupEmail.text().trim());
          lookupEmail
            .attr("contenteditable", "true")
            .removeClass("bg-danger text-white btn-outline-secondary");
          lookupEmail.children().remove();
          lookupEmail.unbind("blur");
          lookupEmail.unbind("keypress");
          lookupEmail.focus();

          lookupEmail.blur(function () {
            let value = this.textContent.trim();
            if (value == "") {
              $(this).remove();
              return;
            }
            let renderEmails = function (email) {
              let isValid = _common.isValidCommaSepratedEmailAddress(email);
              isValid
                ? lookupEmail.removeClass("bg-danger text-white")
                : lookupEmail.addClass("bg-danger text-white");
            };
            renderEmails(value);
            lookupEmail
              .attr("data-email", value)
              .removeAttr("contenteditable")
              .addClass("btn-outline-secondary");
            lookupEmail.append(
              '<span class="fas fa-times pl-2 remove-email"></span>'
            );
          });

          $(this).keypress(function () {
            var keycode = event.keyCode ? event.keyCode : event.which;
            if (
              keycode == "13" ||
              keycode == "32" ||
              keycode == "44" ||
              keycode == "9"
            ) {
              let value = this.textContent.trim();
              if (value == "") {
                $(this).remove();
                return;
              }
              let renderEmails = function (email) {
                let isValid = _common.isValidCommaSepratedEmailAddress(email);
                isValid
                  ? lookupEmail.removeClass("bg-danger text-white")
                  : lookupEmail.addClass("bg-danger text-white");
              };
              renderEmails(value);
              lookupEmail
                .attr("data-email", value)
                .removeAttr("contenteditable")
                .addClass("btn-outline-secondary");
            }
          });
        });

        modalSendSubscribePage.on("click", ".remove-email", function () {
          $(this).closest(".lookup-email").remove();
        });

        modalSendSubscribePage.find(".send_emails").click(function () {
          modalSendSubscribePage.find(".txttestMail").focus();
          modalSendSubscribePage.find(".txttestMail").css("width", "10px");
        });

        modalSendSubscribePage
          .find(".btnSendSubscribePage")
          .on("click", function () {
            let testMail = "";
            modalSendSubscribePage.find(".lookup-email").each(function () {
              testMail += $(this).attr("data-email") + ",";
            });
            testMail = testMail.substr(0, testMail.length - 1);
            if (_common.Validate(modalSendSubscribePage)) {
              if (testMail == "" || testMail.length == 0) {
                swal("Please enter email ID", { icon: "info" });
                return false;
              }
              if (!_common.isValidCommaSepratedEmailAddress(testMail)) {
                swal("Please enter valid email IDs", { icon: "info" });
                return false;
              }
              let param = {
                url: "/api/marketing/SendEmailForSubscription",
                type: "post",
                contentType: "application/json",
              };
              let selectedEmail = modalSendSubscribePage
                .find(".ddlMarketingDomainList")
                .val();
              param.data = {
                mailID: testMail,
                DomainID: selectedDomainId,
                CompanyID: _initData.Company._id,
                FromEmail: selectedEmail,
                SenderEmail: _initData.User._id,
              };

              _common.FetchApiByParam(param, function (res) {
                modalSendSubscribePage.modal("hide");
                swal(res.msg, { icon: res.status });
                modalSendSubscribePage.find(".lookup-email").remove();
              });
            }
          });

        //#endregion

        //#region Marketing Unsubscribe Page

        let modalUnsubscibePage = marketingSettingPage.find("#unsubscribePage");

        marketingSettingPage.ShowUnsubscribePage = function (domainId) {
          let templateId = null;
          _admin
            .GetUnsubscribePageIdsfromCompany()
            .then(function (unsubscribeIDs) {
              if (unsubscribeIDs && unsubscribeIDs.length > 0) {
                templateId = unsubscribeIDs;
              }
              _admin
                .GetUnsubscribePage(templateId, domainId)
                .then((template) => {
                  marketingSettingPage
                    .find(".unsubscribePageTemplate")
                    .html(template.TemplateShow);

                  marketingSettingPage
                    .find(".unsubscribePageTemplate")
                    .find('[contenteditable="true"]')
                    .attr("contenteditable", "false");
                  marketingSettingPage
                    .find(".unsubscribePageTemplate")
                    .on("click", "a", function (e) {
                      e.preventDefault();
                    });
                  marketingSettingPage
                    .find(".unsubscribePageTemplate")
                    .find("button, textarea")
                    .attr("disabled", true);

                  marketingSettingPage
                    .find(".btnPreviewUnsubscribePage")
                    .unbind("click");
                  marketingSettingPage
                    .find(".btnPreviewUnsubscribePage")
                    .click(function () {
                      window.open(
                        "noauth/api/mail/emailsubscription?cid=" +
                          _initData.Company._id.toString() +
                          "&did=" +
                          selectedDomainId.toString() +
                          "&mode=preview",
                        "_blank"
                      );
                    });

                  marketingSettingPage
                    .find(".btnEditUnsubscribePage")
                    .unbind("click");
                  marketingSettingPage
                    .find(".btnEditUnsubscribePage")
                    .click(function () {
                      let param = page.param;
                      param.EditUnsubsPage = true;
                      param.content = template.TemplateEdit;
                      _marketing.OpenEditor(page, param);
                    });

                  marketingSettingPage.GetMarketingCampaignCategoryList(
                    selectedDomainId
                  );
                });
            });
        };
      } else {
        swal(result.message, { icon: "error" });
      }
    }
  );
};

/*Company Basic Settings Region*/
_admin.InitCompanyBasicSettings = function (page) {
  _common.FetchApiByParam(
    {
      url: "api/admin/getcompanysettings",
      type: "post",
    },
    function (result) {
      if (result.message == "success" && result.data) {
        _initData.Company.Setting = result.data;
        let fieldCurrency = {
          Name: "Currency",
          ObjectName: "CommonObjectSchema",
        };
        fieldCurrency.Schema = {
          UIDataType: "dropdown",
          ListSchemaName: "Currency",
          DisplayName: "Currency",
          IsRequired: true,
        };
        let fieldCurrencyCtrl = _controls.BuildCtrl({
          field: fieldCurrency,
          data: result.data,
        });
        page.PageContainer.find(".currency-ctrl").append(
          fieldCurrencyCtrl[0].outerHTML
        );
        if (_initData.Company.Setting.Currency)
          page.PageContainer.find('[data-field="Currency"]').val(
            _initData.Company.Setting.Currency
          );
        page.PageContainer.find(".btnSaveCompSetting").click(function () {
          let sdata = {};
          sdata.Currency = _controls.GetCtrlValue({
            field: fieldCurrency,
            container: page.PageContainer,
          });
          _common.FetchApiByParam(
            {
              url: "api/admin/savecompanybasicsettings",
              type: "post",
              data: sdata,
            },
            function (result) {
              if (result) {
                if (result.message == "success") {
                  swal("Settings has been saved successfully", {
                    icon: "success",
                  });
                } else {
                  swal(result.message, { icon: "error" });
                }
              } else {
                swal(_constantClient.ERROR_MSG, { icon: "error" });
              }
            }
          );
        });
        page.PageContainer.on("click", ".btnCancelCompSetting", function () {
          $(document)
            .find("#main-header #page-nav .nav-item.active .tab-close")
            .trigger("click");
        });
      }
    }
  );
};

_admin.GetUnsubscribePage = function (templateId, selectedDomainId) {
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: "/api/common/getTemplate",
      type: "post",
      data: { templateId, selectedDomainId },
      success: function (res) {
        resolve(res);
      },
    });
  });
};

_admin.GetUnsubscribePageIdsfromCompany = function () {
  let unsubsPagePromise = new Promise(function (success, failure) {
    _common.FetchApiByParam(
      {
        url: "api/admin/marketingemaildml",
        type: "post",
        data: { Operation: "selectUnsubscribePageIds" },
      },
      function (result) {
        if (result.message == "success") {
          success(result.data);
        } else {
          failure(result);
        }
      }
    );
  });
  return unsubsPagePromise;
};
