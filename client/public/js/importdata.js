var DataFields = [];
const ImportData = {};
var unMapped = "unMapped";
var excludeColumnsIndex = [];
//Events Page: AccountList>PageButtons>Import
ImportData.OpenPage = function (name) {
    new Page({ 'pageName': 'ImportData', 'ImportPageName': name });
}

ImportData.Init = function (param) {

    param.PageContainer.find('.btn-csv-file').click(function () {
        param.PageContainer.find('.data-info').html('');
        param.PageContainer.find('.data-error').html('');
        ImportData.UploadFile(param.PageContainer);
    });

    param.PageContainer.find('.insertCSV').click(function () {
        ImportData.CreateCSVParameter(param.PageContainer, 0);
    })

    param.PageContainer.find('.updateCSV').click(function () {
        ImportData.SelectFilterColumn(param.PageContainer, 1);
    })

    param.PageContainer.find('.insertUpdateCSV').click(function () {
        ImportData.SelectFilterColumn(param.PageContainer, 2);
    })

    param.PageContainer.find('.data-Field-Modal').click(function () {
        ImportData.CreateCSVParameter(param.PageContainer, -1);
    })



    param.PageContainer.find('.file-upload').on('change', function () {
        param.PageContainer.find('.lblFileUpload').html($(this).val());
        param.PageContainer.find('.data-info').html('');
        param.PageContainer.find('.csvColumn').html('');
        param.PageContainer.find('.btnOperation').css('display', 'none');
        param.PageContainer.find('.data-error').html('');
    })

    _common.GetPageDetail(param.param.ImportPageName, function (result) {
        DataFields = [];

        if (result.Fields != undefined && result.Fields.length > 0) {
            for (let i = 0; i < result.Fields.length; i++) {
                let field = result.Fields[i];
                if (field.Schema == undefined || field.Name === 'OwnerID' || field.InImportDataField == false)
                    continue;
                DataFields.push(field);
            }
        }

        if (result.Groups != undefined && result.Groups.length > 0) {
            for (let g = 0; g < result.Groups.length; g++) {
                let group = result.Groups[g];
                for (let i = 0; i < group.Fields.length; i++) {
                    let field = group.Fields[i];
                    if (field.Schema == undefined || field.Name == 'OwnerID' || field.InImportDataField == false)
                        continue;
                    let dispName = _page.GetFieldDisplayName(field);
                    field.ColumnName = dispName;
                    field.ColumnNameWithPrefix = dispName + '(' + group.DisplayName + ')';
                    field.DbColumnName = field.Name;
                    field.IsRequired = (field.Schema != undefined ? field.Schema.IsRequired : false);
                    DataFields.push(field);
                }
            }
        }
    })
}

ImportData.LoadColumnName = function (container) {
    let ddlColumn = container.find('.ddlColumnName');
    ddlColumn.html('');
    container.find('.ddlColumnName').html('');
    for (let i = 0; i < DataFields.length; i++) {
        let obj = DataFields[i];
        container.find('.ddlColumnName').append('<option columnValue="' + obj.ColumnName + '" value="' + obj.Name + '">' + obj.ColumnNameWithPrefix.trim() + '</option>');
    }
}

ImportData.LoadPrimaryColumnName = function (container) {
    let ddlColumn = container.find('.ddlPrimaryColumnName');
    ddlColumn.html('');
    for (let i = 0; i < DataFields.length; i++) {
        let obj = DataFields[i];
        if (ImportData.CheckPropertyExistsValue(obj.Schema, 'IsRequired') == true)
            ddlColumn.append('<option columnValue="' + obj.ColumnName + '" value="' + obj.Name + '">' + obj.ColumnNameWithPrefix.trim() + '</option>');
    }
}

ImportData.UploadFile = function (container) {
    ImportData.Header = []
    container.find('.info-data').html('');
    let fileUpload = container.find(".file-upload")[0];
    container.find('.fileUploaded').text(fileUpload.Name);
    let regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            let reader = new FileReader();
            reader.onload = function (e) {
                let rows = e.target.result.split("\n");
                ImportData.CsvData = rows;
                if (rows.length > 0) {
                    container.find('.btnOperation').css('display', 'block');
                }
                ImportData.MapCSVColumn(container, rows);
            }
            reader.readAsText(fileUpload.files[0]);
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid CSV file.");
    }
}

ImportData.MapCSVColumn = function (container, rows) {
    ImportData.Header = [];
    if (rows && rows.length > 0) {
        let chkArray = rows[0].split(",");

        for (let i = 0; i < chkArray.length; i++) {
            if (chkArray[i].trim() == "")
                excludeColumnsIndex.push(i);
        }
        //chkArray = chkArray.filter(p => p.trim() != "");
        for (let k = 0; k < chkArray.length; k++) {
            let objData = {
                ColumnNameCSV: chkArray[k].toString(),
                Index: k,
                Mode: 'Insert'
            };
            container.dmlCSVData = objData;
            ImportData.MappingHeader(container);
        }
    }
}

ImportData.MappingHeader = function (container, domObj) {
    let dataObj = DataFields.filter(p => (p.ColumnName || "").trim() === (container.dmlCSVData && container.dmlCSVData.ColumnNameCSV || "").trim());
    let duplicateColumn = ImportData.DuplicateColumnExists(container.dmlCSVData.ColumnNameCSV, container.dmlCSVData.Index);
    let mappedColumnName = ImportData.CommaSepratedString(container.dmlCSVData.ColumnNameCSV);
    let actionName = '';
    let mappingmoreThanColumn = 1;
    if (container.dmlCSVData.Mode == 'Update') {
        actionName = (dataObj.length > 0) ? 'Change' : 'Map';
        domObj.closest('tr').find('[index=' + container.dmlCSVData.Index + ']').html(actionName);
        if (actionName == "Change") {
            let btnUnMap = domObj.closest('tr').find('.btnUnMap');
            if (btnUnMap.length == 0)
                domObj.closest('tr').find('[index=' + container.dmlCSVData.Index + ']').after('<a href="" class="btnUnMap"> | Unmap </a>');
        }
        for (var i in ImportData.Header) {
            if (ImportData.Header[i].index == container.dmlCSVData.Index) {
                ImportData.Header[i].actionName = actionName;
                ImportData.Header[i].dbColumnName = dataObj[0] != undefined ? dataObj[0].Name : '';
                ImportData.Header[i].columnName = dataObj[0] != undefined ? dataObj[0].ColumnName : '';
                ImportData.Header[i].mappedColumnName = mappedColumnName;
                //ImportData.Header[i].csvColumnName = container.dmlCSVData.ColumnNameCSV;
                ImportData.Header[i].duplicateColumn = duplicateColumn;
                ImportData.Header[i].mappingmoreThanColumn = mappingmoreThanColumn;
                ImportData.Header[i].unMapped = '';
                ImportData.Header[i].objectName = dataObj[0] != undefined ? dataObj[0].ObjectName : '';
                ImportData.Header[i].dataType = dataObj[0] != undefined ? dataObj[0].Schema.UIDataType : '';
                ImportData.Header[i].isRequired = dataObj.length > 0 ? (dataObj[0].Schema.IsRequired != undefined) ? dataObj[0].Schema.IsRequired : false : false;
                ImportData.Header[i].isValid = (duplicateColumn > 0 || mappingmoreThanColumn > 1) ? 0 : 1
                break;
            }
        }
        let icon = ImportData.GetErrorIcon(ImportData.Header[container.dmlCSVData.Index]);
        let value = '<span>' + mappedColumnName + '</span>';
        domObj.closest('tr').find('.mappedColumnName').html(value + " " + icon);
        ImportData.UpdateHeader(container);
    } else if (container.dmlCSVData.Mode == 'Insert') {
        mappingmoreThanColumn = DataFields.filter(p => (p.ColumnName || "").trim() === (container.dmlCSVData.ColumnNameCSV || "").trim()).length;
        if (dataObj.length > 0) {
            mappedColumnName = dataObj[0].ColumnNameWithPrefix;
        } else {
            if (mappedColumnName && mappedColumnName.split(",").length == 1)
                dataObj = DataFields.filter(p => p.ColumnNameWithPrefix === mappedColumnName.trim());
        }
        actionName = (dataObj.length > 0 || mappedColumnName != unMapped) ? 'Change </a><a href="#" class="btnUnMap"> | Unmap' : 'Map';
        ImportData.Header.push({
            index: container.dmlCSVData.Index,
            csvColumnName: container.dmlCSVData.ColumnNameCSV,
            mappedColumnName: mappedColumnName,
            columnName: dataObj.length > 0 ? ImportData.CheckPropertyExistsValue(dataObj[0], 'ColumnName') : '',
            dbColumnName: dataObj.length > 0 ? ImportData.CheckPropertyExistsValue(dataObj[0], 'Name') : '', //dataObj.length > 0 ? dataObj[0].Name : '',
            dataType: dataObj.length > 0 ? ImportData.CheckPropertyExistsValue(dataObj[0].Schema, 'UIDataType') : '',
            actionName: actionName,
            duplicateColumn: duplicateColumn,
            unMapped: mappedColumnName == unMapped ? mappedColumnName : '',
            objectName: dataObj.length > 0 ? dataObj[0].ObjectName : '',
            isRequired: dataObj.length > 0 ? ImportData.CheckPropertyExistsValue(dataObj[0].Schema, 'IsRequired') : '',
            mappingmoreThanColumn: mappingmoreThanColumn,
            isValid: (duplicateColumn > 0 || mappedColumnName == unMapped || mappingmoreThanColumn > 1) ? 0 : 1
        })
        ImportData.GenerateHeaderHtml(container, ImportData.Header);
    }

}

ImportData.CheckPropertyExistsValue = function (obj, propName) {
    if (obj.hasOwnProperty(propName)) {
        return obj[propName];
    }
    return '';
}

ImportData.GenerateHeaderHtml = function (container, headerObj) {
    let tbl = "<table border='1' class='table table-bordered ColumnMappingtbl'><tr><th>Edit</th><th>Field In Optimiser</th><th>Field In CSV File</th></tr>";
    for (let k = 0; k < headerObj.length; k++) {
        let span = ImportData.GetErrorIcon(headerObj[k]);
        tbl += "<tr><td><a  class='btn-link' data-toggle='modal' data-target='#modalColumnMapping' style='color:blue' index='" + k + "'  >" + headerObj[k].actionName + "</a></td><td  dataIndex=" + k + " class='mappedColumnName'>" + headerObj[k].mappedColumnName + " " + span + "</td><td class='headerName'>" + headerObj[k].csvColumnName + "</td></tr>";
    }
    tbl += "</table>";
    container.find('.csvColumn').html(tbl);
    container.find('.ColumnMappingtbl').on('click', '.btn-link', function () {
        ImportData.LoadPopup(container, this);
    })
    container.find('.ColumnMappingtbl').on('click', '.btnUnMap', function (e) {
        // e.stopPropagation();
        e.preventDefault();
        let index = $(this).siblings('.btn-link').attr('index');
        let actionName = "Map";
        let mappedColumnName = unMapped;
        $(this).closest('tr').find('[index=' + index + ']').html(actionName);
        for (var i in ImportData.Header) {
            if (ImportData.Header[i].index == index) {
                ImportData.Header[i].actionName = actionName;
                ImportData.Header[i].mappedColumnName = mappedColumnName;
                ImportData.Header[i].unMapped = unMapped;
                break;
            }
        }
        let icon = ImportData.GetErrorIcon(ImportData.Header[index]);
        let value = '<span>' + mappedColumnName + '</span>';
        $(this).closest('tr').find('.mappedColumnName').html(value + " " + icon);
        $(this).remove();
        //ImportData.UpdateHeader(container);
    })
}

ImportData.LoadPopup = function (container, objBtn) {
    var obj = $(objBtn);
    ImportData.LoadColumnName(container);
    container.find('.modalColumnMapping .btnSelectColumn').unbind('click');
    container.find('.modalColumnMapping .btnSelectColumn').click(function () {

        let ddlSelectedValue = container.find('.modalColumnMapping .ddlColumnName option:selected').attr('columnValue'); //$('.ddlColumnName option:selected').text();
        let dbColumnName = container.find('.modalColumnMapping .ddlColumnName option:selected').val(); //$('.ddlColumnName option:selected').val();
        let index = obj.attr('index');
        let objData = {
            ColumnNameCSV: ddlSelectedValue.trim(),
            DbColumnName: dbColumnName.trim(),
            Index: index,
            Mode: 'Update'
        };
        container.dmlCSVData = objData;
        ImportData.MappingHeader(container, obj);
    })
}

ImportData.SelectFilterColumn = function (container, arg) {
    ImportData.LoadPrimaryColumnName(container);
    container.find('.modalForDataField .btnSelectColumn').unbind('click');
    container.find('.modalForDataField .btnSelectColumn').click(function () {
        ImportData.CreateCSVParameter(container, arg);
    })
}

ImportData.CreateCSVParameter = function (container, arg) {
    let opr = { 'ObjectName': DataFields[0].ObjectName };
    if (arg == 1) {
        let dbColumnName = container.find('.modalForDataField .ddlPrimaryColumnName option:selected').val(); //$('.ddlPrimaryColumnName option:selected').val();
        opr.columnName = dbColumnName;
        opr.url = '/api/importdata/UpdateCSVData';
        opr.upsert = false;
        opr.type = 'post';
        opr.mode = arg;
        opr.ConfirmMsg = 'Are you sure to continue for update record ?';
    } else if (arg == 2) {
        let dbColumnName = container.find('.modalForDataField .ddlPrimaryColumnName option:selected').val(); //$('.ddlPrimaryColumnName option:selected').val();
        opr.columnName = dbColumnName;
        opr.upsert = true;
        opr.url = '/api/importdata/UpdateCSVData';
        opr.type = 'post';
        opr.mode = arg;
        opr.ConfirmMsg = 'Are you sure to continue for update/insert record ?';
    } else {
        opr.url = '/api/importdata/InsertAccountData';
        opr.type = 'post';
        opr.mode = arg;
        opr.ConfirmMsg = 'Are you sure to continue for insert record ?';
    }
    container.param = opr;
    ImportData.CSVCrudOperation(container);
}

ImportData.CSVCrudOperation = async function (container) {

    if (ImportData.ValidateCsv()) {
        return false;
    }
    let status = confirm(container.param.ConfirmMsg);
    if (!status)
        return false;
    _common.Loading(true);
    // container.find('.data-info').html("<center><img src='/images/loader.gif' /></center>");
    let isValid = true;
    let headerData = ImportData.Header;
    let csvData = ImportData.CsvData;
    ImportData.FinalCSVData = [];
    let primaryColumn = DataFields.filter(p => p.Schema.IsPrimary)[0];
    //-----------Table  New Header  Start--------------------//

    let newheadr_Obj = "";
    let str_tbl = "<table border='1' class='table table-bordered'><tbody><tr>";
    str_tbl += "<th>Row No.</th><th>Column Name</th><th>Column Value</th><th>Error</th>";
    for (let i = 0; i < headerData.length; i++) {
        if (headerData[i].isValid) {
            newheadr_Obj += headerData[i].index + ",";
            //  str_tbl += "<th>" + headerData[i].dbColumnName + "</th>";
        }
    }
    str_tbl += "</tr>";
    if (newheadr_Obj)
        newheadr_Obj = newheadr_Obj.substr(0, newheadr_Obj.length - 1)
    //-----------Table  New Header   END--------------------//
    let columnCount = headerData.length;
    for (let i = 1; i < csvData.length; i++) {
        let rowNo = i + 1;
        let jsonObj = {};
        // let new_tr = csvData[i].length > 0 ? csvData[i].match(/((?:"(?:"{2}|,|\n|[^"]*)+")|(?:[^,"\n]+))/g) : [];    //CSV data split
        let new_tr = csvData[i].length > 0 ? ImportData.CSVRowtoArray(csvData[i]) : []; //CSV data split
        let isTd = 0;
        if (new_tr == null || new_tr.length == 0) {
            continue;
        }
        for (let k = 0; k < new_tr.length; k++) {
            if (excludeColumnsIndex.includes(k)) {
                continue;
            }
            //csv data iterate
            let data = headerData.filter(p => p.index == k);
            if (!jsonObj[data[0].objectName])
                jsonObj[data[0].objectName] = {};
            if (data[0].unMapped == "unMapped")
                continue;
            let isMsg = ImportData.GetCSVFieldErrorMsg(data[0], new_tr[k].toString().trim());
            if (isMsg != '') {
                isValid = false;
                if (isTd == 0) {
                    str_tbl += "<tr>";
                    isTd = 1
                }
                str_tbl += "<td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_tr[k] + "</td><td class='error-col'> " + isMsg + ".</td></tr>";
                jsonObj[data[0].objectName]['isValid'] = false;
            }
            if (data[0].dbColumnName == primaryColumn.Name) {
                if ((primaryColumn.IsUnique || primaryColumn.Schema.IsUnique) && data[0].objectName) {
                    if (new_tr[k] == undefined || new_tr[k] == null || new_tr[k].toString().trim() == '') {
                        str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_tr[k] + "</td><td class='error-col'> This field is mondatory.</td></tr>";
                        isValid = false;
                    }
                    let param2 = { 'name': data[0].objectName };
                    param2.match = [];
                    if ((primaryColumn.UniqueFieldCombination || primaryColumn.Schema.UniqueFieldCombination)) {
                        let uniqueCheck = primaryColumn.UniqueFieldCombination || primaryColumn.Schema.UniqueFieldCombination;
                        for (let i=0; i<uniqueCheck.length; i++){
                            let tempObj = {};
                            let fldName = uniqueCheck[i];
                            if (primaryColumn.Schema.CaseSensitive !== undefined)
                                tempObj[fldName] = { 'UniqueString': { 'Value': new_tr[k].toString().trim(), 'CaseSensitive': primaryColumn.Schema.CaseSensitive } };
                            else
                                tempObj[fldName] = new_tr[k].toString().trim();
                            param2.match.push(tempObj);
                        }
                    } else {
                        let tempObj = {};
                        if (primaryColumn.Schema.CaseSensitive !== undefined)
                            tempObj[primaryColumn.Name] = { 'UniqueString': { 'Value': new_tr[k].toString().trim(), 'CaseSensitive': primaryColumn.Schema.CaseSensitive } };
                        else
                            tempObj[primaryColumn.Name] = new_tr[k].toString().trim();
                        param2.match.push(tempObj);
                    }
                    let count = _common.CountUniqueFields(param2);
                    if (count > 0) {
                        str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_tr[k] + "</td><td class='error-col'> " + new_tr[k] + " already exists.</td></tr>";
                        isValid = false;
                    }
                }
            }
            let param = {};
            let notFoundErrMsg = "Unable to find this record in our System";
            if (new_tr[k].toString().trim() != "") {
                if (data[0].dataType == "dropdown") {
                    let fieldObj = DataFields.find(x => x.Name == data[0].dbColumnName);
                    let listSchemaName = fieldObj.Schema.ListSchemaName;
                    param = { collection: "ListSchema", value: listSchemaName };
                    let result = await _common.GetImportObjectDataID(param);
                    var keyList = result.data[0].Data.find(x => x.Value == new_tr[k]);
                    if (keyList != undefined)
                        jsonObj[data[0].objectName][data[0].dbColumnName] = keyList.Key;
                    else {
                        str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_tr[k] + "</td><td class='error-col'> " + notFoundErrMsg + ".</td>";
                        jsonObj[data[0].objectName]['isValid'] = false;
                        isValid = false;
                    }
                } else if (data[0].dataType == "multiselect") {
                    let new_trArr = new_tr[k].split(',');
                    let fieldObj = DataFields.find(x => x.Name == data[0].dbColumnName);
                    let listSchemaName = fieldObj.Schema.ListSchemaName;
                    param = { collection: "ListSchema", value: listSchemaName };
                    let result = await _common.GetImportObjectDataID(param);
                    var keyList;
                    let keysOfValues = [];
                    for (let z = 0; z < new_trArr.length; z++) {
                        keyList = result.data[0].Data.find(x => x.Value == new_trArr[z]);
                        if (keyList != undefined) {
                            keysOfValues[z] = keyList.Key;
                        } else {
                            str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_trArr[z] + "</td><td class='error-col'> " + notFoundErrMsg + ".</td>";
                            jsonObj[data[0].objectName]['isValid'] = false;
                            isValid = false;
                        }
                    }
                    if (keysOfValues.length > 0)
                        jsonObj[data[0].objectName][data[0].dbColumnName] = keysOfValues;
                } else if (data[0].dataType == "country") {
                    param = { collection: "Country", value: new_tr[k] };
                    let result = await _common.GetImportObjectDataID(param);
                    if (result.data.length > 0) {
                        if (result.data[0]._id != undefined)
                            jsonObj[data[0].objectName][data[0].dbColumnName] = result.data[0]._id;
                    } else {
                        str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_tr[k] + "</td><td class='error-col'> " + notFoundErrMsg + ".</td>";
                        jsonObj[data[0].objectName]['isValid'] = false;
                        isValid = false;
                    }
                } else if (data[0].dataType == "lookup") {
                    if (data[0].dbColumnName == 'City')
                        param = { collection: "City", value: new_tr[k] };
                    else
                        param = { collection: data[0].objectName, value: new_tr[k] };
                    let result = await _common.GetImportObjectDataID(param);
                    if (result.data.length > 0) {
                        if (result.data[0]._id != undefined)
                            jsonObj[data[0].objectName][data[0].dbColumnName] = _controls.ToEJSON('objectid', result.data[0]._id);
                    } else {
                        str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_tr[k] + "</td><td class='error-col'> " + notFoundErrMsg + ".</td>";
                        jsonObj[data[0].objectName]['isValid'] = false;
                        isValid = false;
                    }
                } else if (data[0].dataType == "multilookup") {
                    let new_trArr = new_tr[k].split(',');
                    let oidsOfValues = [];
                    for (let z = 0; z < new_trArr.length; z++) {
                        param = { collection: data[0].objectName, value: new_trArr[z] };
                        let result = await _common.GetImportObjectDataID(param);
                        if (result.data.length > 0) {
                            if (result.data[0]._id != undefined)
                                oidsOfValues[z] = _controls.ToEJSON('objectid', result.data[0]._id);
                        } else {
                            str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='" + data[0].csvColumnName + "'>" + data[0].csvColumnName + "</td><td>" + new_tr[k] + "</td><td class='error-col'> " + notFoundErrMsg + ".</td>";
                            jsonObj[data[0].objectName]['isValid'] = false;
                            isValid = false;
                        }
                    }
                    jsonObj[data[0].objectName][data[0].dbColumnName] = oidsOfValues;
                } else {
                    jsonObj[data[0].objectName][data[0].dbColumnName] = new_tr[k].toString().trim();
                }
            } else {
                jsonObj[data[0].objectName][data[0].dbColumnName] = new_tr[k].toString().trim();
            }
        }
        if (jsonObj)
            ImportData.FinalCSVData.push(jsonObj);
        if (isTd == 1)
            str_tbl += "</tr>"
        if (isValid) {
            if (i == 1)
                continue;
            for (let j = 0; j < ImportData.FinalCSVData.length - 1; j++) {
                let arrayObjData = ImportData.FinalCSVData[j].Account;
                let nextObjData = jsonObj.Account;
                if (nextObjData.Name == arrayObjData.Name && nextObjData.AccountType == arrayObjData.AccountType) {
                    str_tbl += "<tr><td> " + rowNo + "</td><td class='column' index='1' col='Account Name'>Account Name</td><td>" + nextObjData.Name + "</td><td class='error-col'> " + nextObjData.Name + " already exists with same Account Type in the file.</td></tr>";
                    isValid = false;
                }
            }
        }
    }
    str_tbl += "</tbody></table>";
    container.param.data = ImportData.FinalCSVData;
    if (isValid) {
        ImportData.CSVDMLOperation(container);
    } else {
        _common.Loading(false);
        container.find('.data-error').html(str_tbl);
        container.find('.data-info').html("");
    }
}

ImportData.CSVRowtoArray = function (text) {
    //  var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    //  if (!re_valid.test(text)) return null;
    var a = []; // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function (m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

ImportData.GetCSVFieldErrorMsg = function (data, value) {
    if (data.isRequired) {
        if (value == '' || value == undefined || value.length == 0) {
            return "field is required";
        }
    } else if (!_controls.IsValidDataType(data.dataType, value)) {
        return "Please enter valid " + data.dataType + " value";
    }
    return '';
}

ImportData.CSVDMLOperation = function (param) {
    $.ajax({
        url: param.param.url,
        type: param.param.type,
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(param.param),
        success: function (res) {
            if (param.param.mode == 1) {
                let modified = res.filter(p => p.nModified == 1).length
                let tbl = "<table border='1' class='table table-bordered '><tr><th>Type</th><th>Count</th></tr>";
                tbl += "<tr><td> Modified  </td><td class='headerName'>" + modified + "</td></tr>";
                tbl += "</table>";
                param.find('.data-info').html(tbl);
            } else if (param.param.mode == 2) {
                let modified = res.filter(p => p.nModified == 1).length
                let insertCount = res.filter(p => p.hasOwnProperty('upserted')).length
                let tbl = "<table border='1' class='table table-bordered '><tr><th>Type</th><th>Count</th></tr>";
                tbl += "<tr><td> Modified  </td><td class='headerName'>" + modified + "</td></tr>";
                tbl += "<tr><td> Inserted  </td><td class='headerName'>" + insertCount + "</td></tr>";
                tbl += "</table>";
                param.find('.data-info').html(tbl);
            } else {
                let existCount = 0;
                let insertCount = res.data;
                let tbl = "<table border='1' class='table table-bordered '><tr><th>Type</th><th>Count</th></tr>";
                tbl += "<tr><td> No. of account inserted </td><td class='headerName'>" + insertCount + "</td></tr>";
                // tbl += "<tr><td> Already Exist </td><td class='headerName'>" + existCount + "</td></tr>";
                tbl += "</table>";
                param.find('.data-info').html(tbl);
            }
            _common.Loading(false);
            console.log(res);
        },
        error: function (err) {
            _common.Loading(false);
            alert(err.message);
        }
    })
}

ImportData.UpdateHeader = function (container) {
    let dbHeaderCount = DataFields.length;
    for (let c = 0; c < dbHeaderCount; c++) {
        let data = ImportData.Header.filter(p => p.mappedColumnName == DataFields[c].ColumnNameWithPrefix);
        if (data.length == 1) {
            ImportData.Header[data[0].index].duplicateColumn = 0;
            ImportData.Header[data[0].index].mappingmoreThanColumn = 1;
            ImportData.Header[data[0].index].unMapped = '';
            ImportData.Header[data[0].index].isValid = 1
            let cont = container.find('.ColumnMappingtbl .mappedColumnName[dataIndex=' + data[0].index + '] span');
            if (cont.hasClass('fas fa-exclamation'))
                cont.removeClass('fas fa-exclamation');
        }
        if (data.length > 1) {
            for (let i in data) {
                ImportData.Header[data[i].index].duplicateColumn = 1;
                ImportData.Header[data[i].index].isValid = 0;
                let icon = ImportData.GetErrorIcon(ImportData.Header[data[i].index]);
                let value = '<span>' + data[i].mappedColumnName + '</span>';
                container.find('.ColumnMappingtbl .mappedColumnName[dataIndex=' + data[i].index + ']').html(value + " " + icon);
            }
        }
    }
}

ImportData.ValidateCsv = function () {
    let headerData = ImportData.Header;
    let flag = false;
    let ul = '<ul>';
    let flagArray = [];
    for (let i = 0; i < headerData.length; i++) {
        if ((flagArray && flagArray.length > 0 && flagArray.filter(x => x.dbColumnName == headerData[i].dbColumnName).length > 0)) {
            continue;
        }
        if (headerData[i].duplicateColumn) {
            ul += '<li>duplicate column map ' + headerData[i].dbColumnName + '</li>';
            flag = true;
            flagArray.push({ dbColumnName: headerData[i].dbColumnName });
        }
        if (headerData[i].mappingmoreThanColumn > 1) {
            ul += '<li>more than mapping exits ' + headerData[i].csvColumnName + '</li>';
            flag = true;
            flagArray.push({ dbColumnName: headerData[i].dbColumnName });
        }
    }

    for (let i = 0; i < DataFields.length; i++) {
        if (DataFields[i].IsRequired != undefined && DataFields[i].IsRequired == true &&
            DataFields[i].DbColumnName != "OwnerID" && DataFields[i].DbColumnName != "BranchCode") {
            let k = ImportData.Header.filter(p => p.dbColumnName == DataFields[i].Name);
            if (k.length == 0 || k[0].unMapped == "unMapped") {
                ul += '<li>please map required column ' + _page.GetFieldDisplayName(DataFields[i]) + '</li>';
                flag = true;
            }
        }
    }
    ul += '</ul>';
    $('.errorMsg').html(ul);
    return flag;
}



ImportData.GetErrorIcon = function (data) {
    let span = '';
    if (data.unMapped === unMapped) {
        span = '<span data-toggle="tooltip" title="column does not exists please map" class="fas fa-exclamation text-danger"></span>';
    } else if (data.mappingmoreThanColumn > 1) {
        span = '<span data-toggle="tooltip" title="more than column exists" class="fas fa-exclamation text-danger"></span>';
    } else if (data.duplicateColumn == 1) {
        span = '<span data-toggle="tooltip" title="column map already" class="fas fa-exclamation text-danger"></span>';
    }
    return span;
}

ImportData.CommaSepratedString = function (val) {
    let obj = DataFields.filter(p => p.ColumnName === val.toString().trim());
    var str = '';
    if (obj.length > 0) {
        $.each(obj, function (val, item) {
            str += item.ColumnNameWithPrefix + ',';
        })
    }
    return str.length > 0 ? str.substring(0, str.length - 1) : unMapped;
}

ImportData.isColumnExists = function (val) {
    let obj = DataFields.filter(p => p.ColumnName == val.toString().trim());
    return obj.length;
}

///Check header Column exists also check Multipe column exists
ImportData.DuplicateColumnExists = function (val, index) {
    let obj = ImportData.Header.filter(p => p.mappedColumnName == val.toString().trim() && p.index.toString().trim() != index);
    return obj.length;
}
