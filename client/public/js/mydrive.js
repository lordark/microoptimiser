const _mydrive = {};

$(function () {
    $(document).on("click", function (e) {

        if ($(e.target).closest(".file-box").length == 0) {
            $('.drivecontent .file-box .card').removeClass("drive-selected");
        } if ($(e.target).closest(".folder-box").length == 0) {
            $('.folder-box .card').removeClass("drive-selected");
        }
        if ($(e.target).closest(".gridfolder-box").length == 0) {
            // $(".gridfolder-box").removeClass('drive-selected')
        }




    });
})

//Event : MyDrive
_mydrive.InitMyDrive = function (page) {
    // For Main BreadCrumb in Both Views
    _mydrive.Page = page;
    _mydrive.SelectedFolder = [{ 'ViewValue': 'Drive', 'FolderId': 'root' }];
    // To keep record of multiple selected files in the list view
    _mydrive.ListViewSelectedFileIds = [];
    //To get all the details from the selected file in the list view
    _mydrive.ListViewContextSelection = [];
    //to manage viewState and infinte scroll
    _mydrive.ViewInfo = { 'CurrentView': 'ListView', 'PageSize': 60, 'PageNumber': 1, 'Loading': false, 'LastPage': false, "ViewType": "myUploads" };
    // for move file breadCrumb
    _mydrive.MoveFileBreadCrumb = [{ 'ViewValue': 'Drive', 'FolderId': 'root' }];

    _mydrive.MoveViewInfo = { 'CurrentView': 'ListView', 'PageSize': 20, 'PageNumber': 1, 'Loading': false, 'LastPage': false };
    //to change padding of the  drive for infinite scroll view
    $('#content-wrapper').addClass('drive-padding')
    page.PageContainer.on('pageactive', () => {
        $('#content-wrapper').addClass('drive-padding')
    })
    page.PageContainer.on('pageinactive', () => {
        $('#content-wrapper').removeClass('drive-padding')
    })

    if (window.location.hash == "#share") {
        _mydrive.toggleView(page, "shareWithMe");
    }

    new _mydrive.ListView(page);
    new _mydrive.GridView(page);





    //View toggle  section (list /grid)
    page.PageContainer.find('.gird-button-list').on('click', '.show-list-view', function () {
        $(this).removeClass('btn-outline-secondary').addClass('btn-secondary');
        page.PageContainer.find('.gird-button-list .show-grid-view').addClass('btn-outline-secondary').removeClass('btn-secondary');
        let listView = page.PageContainer.find('.main-drive-list');
        let gridView = page.PageContainer.find('.main-drive-grid');
        page.PageContainer.find('.chkSelectAll').prop('checked', false);
        page.PageContainer.find('.folder-head').addClass('d-none');
        page.PageContainer.find('.file-head').addClass('d-none');
        page.PageContainer.find(".removeSharingbtn").addClass('d-none');
        let CopyBtn = page.PageContainer.find('.copy-btn');
        let MoveBtn = page.PageContainer.find('.move-btn');
        let DeleteBtn = page.PageContainer.find('.btnDelete');
        let DownloadBtn = page.PageContainer.find('.btnDownload');
        let ExternaShareBtn = page.PageContainer.find('.external-share-btn');
        let InternalShareBtn = page.PageContainer.find('.internal-share-btn');
        let SearchText = (page.PageContainer.find('.search-in-list .txtSearchInGrid').val().length == 0) ? null : page.PageContainer.find('.search-in-list .txtSearchInGrid').val();
        _mydrive.ListViewSelectedFileIds = [];
        page.PageContainer.find('.main-drive-list').css("display", "block");
        page.PageContainer.find('.main-drive-grid .folder-section').html('');
        page.PageContainer.find('.main-drive-grid .drivecontent').html('');
        _mydrive.ListViewContextSelection = [];
        gridView.removeClass('d-none');
        listView.addClass('d-none');
        page.PageContainer.find('.main-drive-grid').css('display', 'block');
        ExternaShareBtn.addClass('d-none');
        InternalShareBtn.addClass('d-none');
        DownloadBtn.addClass('d-none');
        CopyBtn.addClass('d-none');
        MoveBtn.addClass('d-none');
        DeleteBtn.addClass('d-none');
        _mydrive.ViewInfo['CurrentView'] = 'ListView';
        _mydrive.ViewInfo['PageSize'] = 60;
        _mydrive.ViewInfo['PageNumber'] = 1;
        _mydrive.ViewInfo['Loading'] = true;
        _mydrive.ViewInfo['LastPage'] = false;
        page.ListView.ReloadData(SearchText, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo.ViewType, false);
    })

    page.PageContainer.find('.gird-button-list').on('click', '.show-grid-view', function () {
        $(this).removeClass('btn-outline-secondary').addClass('btn-secondary');
        page.PageContainer.find('.gird-button-list .show-list-view').addClass('btn-outline-secondary').removeClass('btn-secondary');
        let listView = page.PageContainer.find('.main-drive-list');
        let gridView = page.PageContainer.find('.main-drive-grid');

        // let ExternaShareBtn = page.PageContainer.find('.external-share-btn');
        // let InternalShareBtn = page.PageContainer.find('.internal-share-btn');
        page.PageContainer.find('.chkSelectAll').prop('checked', false);
        page.PageContainer.find('.folder-head').addClass('d-none');
        page.PageContainer.find('.file-head').addClass('d-none');
        page.PageContainer.find(".removeSharingbtn").addClass('d-none');
        // let CopyBtn = page.PageContainer.find('.copy-btn');
        // let MoveBtn = page.PageContainer.find('.move-btn');
        // let DeleteBtn = page.PageContainer.find('.btnDelete');
        // let DownloadBtn = page.PageContainer.find('.btnDownload');
        let SearchText = (page.PageContainer.find('.search-in-list .txtSearchInGrid').val().length == 0) ? null : page.PageContainer.find('.search-in-list .txtSearchInGrid').val();
        _mydrive.ListViewSelectedFileIds = [];
        gridView.addClass('d-none');
        listView.removeClass('d-none');
        page.PageContainer.find('.main-drive-list').show();
        _mydrive.ListViewContextSelection = []
        _mydrive.ViewInfo['LastPage'] = false;
        _mydrive.ViewInfo['CurrentView'] = 'GridView';
        _mydrive.ViewInfo['PageSize'] = 60;
        _mydrive.ViewInfo['PageNumber'] = 1;
        _mydrive.ViewInfo['Loading'] = true;
        page.GridView.ReloadData(SearchText, _mydrive.ViewInfo.ViewType, false);
    });
    // Upload file  section
    page.PageContainer.find('.main-drive').on('change', '.uploadFile', function () {
        let Files = $(this)[0].files;

        let AllFileSizes = []
        for (let f = 0; f < Files.length; f++) {
            AllFileSizes.push(Files[f].size)
        }
        if (Files.length > 0 && Files.length < 6) {
            let CanUpload = _mydrive.CanUpload(page, AllFileSizes)
            if (CanUpload) {
                page.PageContainer.find('.upload-count').removeClass('bg-danger').addClass('bg-primary');
                _drive.uploadToDrive(page.PageContainer, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, Files).then((res) => {
                    if (res && res[0] && res[0].statusCode == 426) {
                        page.PageContainer.find('.upload-section').addClass('d-none');
                        swal('Please Upgrade Your Space.', { icon: "info" });
                    } else if (res && res[0] && res[0].statusCode == 413) {
                        let UploadContainer = page.PageContainer.find('.upload-container');
                        let UploadSection = page.PageContainer.find('.upload-section');
                        UploadSection.addClass('d-none');
                        UploadContainer.find('ul').html("");
                        page.PageContainer.find('.upload-section').addClass('d-none');

                        swal('Cannot upload files more than 150 mb', { icon: "info" });
                    } else if (res && res[0] && res[0].statusCode == 200) {
                        for (let i = 0; i < res.length; i++) {
                            let AllFiles = page.PageContainer.find(".upload-section .upload-container li").length;
                            let LoaderContainer = page.PageContainer.find(`.upload-container ul li.${res[i].data.UID} .file-loader`);
                            LoaderContainer.addClass('file-uploaded').removeClass('inactive');
                            LoaderContainer.removeClass('file-loader');
                            page.PageContainer.find(`.upload-container ul li.${res[i].data.UID}`).addClass('file-uploaded').removeClass('inactive');
                            for (let j = 0; j < _drive.filesInProgress.length; j++) {
                                if (_drive.filesInProgress[j].Name == res[i].data.UID) {
                                    _drive.filesInProgress[j].Status = 'done'
                                }
                            }
                            let UploadedFiles = _drive.filesInProgress.filter(o => o.Status == 'done').length;
                            page.PageContainer.find('.upload-count .files-count').html(`${UploadedFiles} ${UploadedFiles > 1 ? 'Items' : 'Item'}  Uploaded of ${AllFiles}`)
                        }
                        _mydrive.ReloadPage();
                    }

                }).catch((e) => {

                    if (e.status == 0) {
                        console.log("Ready State Error:", e)
                    } else if (e && e.responseJSON && e.responseJSON.statusCode != 426 && !('status' in e)) {
                        console.log(e, "Error While Uploading Files:");
                        let UploadSection = page.PageContainer.find('.upload-section');
                        let UploadContainer = page.PageContainer.find('.upload-container');
                        UploadContainer.find('ul').html("")
                        UploadSection.addClass('d-none')
                        //
                        swal('Internal Server Error !', { icon: 'error' });
                        // swal("FileFormat Not Suppoerted", { icon: "error", });
                    }
                });
            } else {
                $(this).val('')
                //clear dialog box download
                // let UploadSection = page.PageContainer.find('.upload-section');
                // let UploadContainer = page.PageContainer.find('.upload-container');
                // UploadSection.addClass('d-none');
                // UploadContainer.find('ul').html("");
                // _drive.filesInProgress = [];
                // _drive.filesToRetry = [];

                swal('Please Upgrade Your Space!', { icon: "info" });
            }
        } else if (Files.length > 5) {
            swal('Cannot Upload more than 5 items', { icon: "info", });
        }

    });

    // to show download limit on folder download
    page.PageContainer.on('click', '.download-disabled', function () {
        swal("You can not download folders more than 100 mb .", { icon: "info" })
    })
    // toggle share/my drive section
    page.PageContainer.find('.dropdown-menu a').on('click', function () {
        page.PageContainer.find('.dropdown-menu a').removeClass('active')
        $(this).addClass('active')
    })
    // to click on hidden upload form
    page.PageContainer.find('.upload-button').on('click', function (e) {
        page.PageContainer.find('.uploadFile')[0].click();

    });
    // to create new folder

    page.PageContainer.find('.page-top-button .newfolder').on('click', function () {

        let body = `<div class="row">
                        <div class="form-group col-sm-12">
                            <label class="small">Folder Name</label>
                                <input type="text" class="form-control form-control-sm foldername" />
                        </div>
                    </div>`
        let FolderModal = _common.DynamicPopUp({ title: 'Add New Folder', body: body })
        FolderModal.modal("show")
        FolderModal.find('.modal-dialog').removeClass('modal-lg').addClass('modal-sm');
        FolderModal.find('.btnSave').prop("disabled", true);
        FolderModal.find('.foldername').on('input', function (e) {

            if (e.target.value.length > 0) {
                FolderModal.find('.btnSave').prop("disabled", false)
            } else if (e.target.value.length == 0) {

                FolderModal.find('.btnSave').prop("disabled", true)
            }
        })

        FolderModal.find('.btnSave').one('click', function (e) {
            let folderName = FolderModal.find('.foldername').val();
            _drive.CreateFolder(folderName, 'folder', _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId).then(() => {
                _mydrive.ViewInfo['LastPage'] = false;
                _mydrive.ViewInfo['PageSize'] = 60;
                _mydrive.ViewInfo['PageNumber'] = 1;
                _mydrive.ViewInfo['Loading'] = true;
                $('.modal-backdrop').hide();
                $('#newFolder').hide();
                FolderModal.modal("hide")
                page.PageContainer.find('.folder-head').addClass('d-none');
                page.PageContainer.find('.file-head').addClass('d-none');
                if (_mydrive.ViewInfo.CurrentView == "GridView") {

                    page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                } else {

                    page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false)
                }


            })
        })
    });



    page.PageContainer.find('.drive-view .dropdown-menu a').click(function () {
        let viewType = $(this).data().viewtype;
        _mydrive.toggleView(page, viewType);
        page.ListView.ReloadData(null, "root", null, false);
        page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
    })


    // to close all copy button
    page.PageContainer.on('click', '.copy-container .close-copy-container', function () {
        let type = page.PageContainer.find('.copy-container').attr("type")
        if (type == "zip") {
            _drive.CancelZipFileRequest(page.PageContainer)
        } else {
            _drive.CancelCopyFileRequest(page.PageContainer)
        }
        // $(this).parent().addClass('d-none').removeClass('d-flex')
    })

    //context menu of drive
    page.PageContainer.find('.main-drive').on('contextmenu', '.main-drive-grid .folder-section .folder-box .align-items-center, .drivecontent .file-box', function (e) {
        // $(this).find('.tooltip').remove();
        let SelectedContent = $(this).closest('.folder-box,.file-box,.gridfile-box,.gridfolder-box')[0].data;
        let SelectedBox = $(this).closest('.folder-box,.file-box,.gridfile-box,.gridfolder-box')
        if (_mydrive.ViewInfo['CurrentView'] == 'ListView') {
            page.PageContainer.find('.drive-selected').removeClass('drive-selected');
            SelectedBox.find('.card').addClass('drive-selected');
        } else {
            $(this).addClass('drive-selected');
            let checked = $(this).find('.chkSelect').is(":checked");
            if (!checked) {
                $(this).find('.chkSelect').attr('checked', true);
                $(this).addClass('drive-selected');

            }
            _mydrive.GridViewCheckBoxClick(page, !checked, SelectedContent);
        }

        let Type = SelectedContent.Type;
        let FolderId = $(this).attr('data-folder');
        let FileId = SelectedContent._id
        let FolderName = $(e.target.parentElement).find('.card-title').text() || $(this).find('.card-title').text();

        let FileName = $(this).find('.card-title span').html();
        page.PageContainer.find('.drive-rightclick-menu .download').removeClass('downloadfolder');
        page.PageContainer.find('.drive-rightclick-menu .download').removeAttr('href');
        let id = FolderId || FileId;
        let top = e.pageY;
        let left = e.pageX;

        let internalShareOption = page.PageContainer.find("[data-key='share-internally']");
        let externalShareOption = page.PageContainer.find("[data-key='share-externally']");
        let removeSharingOption = page.PageContainer.find("[data-key='remove']");

        let deleteOption = page.PageContainer.find("[data-key='delete']");
        let renameOption = page.PageContainer.find("[data-key='rename']");
        let copyOption = page.PageContainer.find("[data-key='copy']");
        let moveOption = page.PageContainer.find("[data-key='move']");
        e.preventDefault();
        e.stopPropagation();
        let obj = {}

        if (Type == 'folder') {
            obj['Type'] = 'folder';
            obj['ID'] = _controls.ToEJSON('objectid', FolderId);
            obj['Name'] = FolderName;
            obj['FileDetails'] = SelectedContent;
            if (!_mydrive.DownloadSizeCheck(SelectedContent.Size)) {
                page.PageContainer.find('.drive-context-menu .download').addClass('download-disabled');
            } else {
                page.PageContainer.find('.drive-context-menu .download').removeClass('download-disabled').addClass('downloadfolder');
            }

        } else {
            obj['Type'] = 'file'
            obj['ID'] = _controls.ToEJSON('objectid', FileId);
            obj['Name'] = FileName;
            obj['FileDetails'] = SelectedContent;
            FileId = SelectedContent.Orignal ? SelectedContent.Orignal : SelectedContent._id;
            let DefaultDoc = SelectedContent.DefaultDoc && SelectedContent.DefaultDoc == true ? 'default' : 'drive'
            page.PageContainer.find('.drive-rightclick-menu .download').attr('href', `/api/drive/download/${FileId}/${FileName}/${DefaultDoc}`)
            page.PageContainer.find('.drive-context-menu .download').removeClass('download-disabled');
        }
        // //Hide show share button
        if (_mydrive.ViewInfo && _mydrive.ViewInfo.ViewType == "shareWithMe") {
            removeSharingOption.removeClass("d-none");
            deleteOption.addClass("d-none");
            renameOption.addClass("d-none");
            copyOption.addClass("d-none");
            moveOption.addClass("d-none");
            internalShareOption.addClass("d-none");
            externalShareOption.addClass("d-none");

        } else {
            removeSharingOption.addClass("d-none");
            deleteOption.removeClass("d-none");
            renameOption.removeClass("d-none");
            copyOption.removeClass("d-none");
            moveOption.removeClass("d-none");
            if (_initData.User.MySettings.DriveSettings.CanShare) {
                if (Type == "folder") {
                    internalShareOption.addClass("d-none")
                    externalShareOption.addClass("d-none");
                } else {
                    internalShareOption.removeClass("d-none")
                    externalShareOption.removeClass("d-none");
                }

            } else {
                internalShareOption.addClass("d-none")
                externalShareOption.addClass("d-none");
            }

        }

        _mydrive.ListViewContextSelection = [obj];
        page.PageContainer.find('.drive-context-menu').css("cssText", "display:block !important;");
        page.PageContainer.find(".drive-rightclick-menu").removeClass('d-none').css({ display: "block", left: left, top: top });



    });
    // right click menu  list view
    page.PageContainer.on('click', '.drive-rightclick-menu', function (e) {

        let type = _mydrive.ListViewContextSelection[0].Type;
        let id = _mydrive.ListViewContextSelection[0].ID;
        let name = _mydrive.ListViewContextSelection[0].Name;
        let size = _mydrive.ListViewContextSelection[0].Size || _mydrive.ListViewContextSelection[0].FileDetails.Size;
        let DefaultDoc = _mydrive.ListViewContextSelection[0].FileDetails.DefaultDoc && _mydrive.ListViewContextSelection[0].FileDetails.DefaultDoc == true ? true : false
        let selectionType = $(e.target).attr('data-key');
        page.PageContainer.find('.context-menu .dropdown-menu a').removeClass('active')
        if (selectionType == 'copy') {
            let toCopy = _mydrive.ListViewContextSelection[0]['FileDetails'];
            if (type == 'folder') _mydrive.ViewInfo['PageNumber'] = 1;
            let CopyStatusContainer = page.PageContainer.find('.copy-container');
            CopyStatusContainer.attr("type", "copy")
            toCopy.FileName = `Copy of ${toCopy.FileName}`;
            toCopy.UploadDate = _controls.ToEJSON('datetime', new Date().toISOString());
            id = _mydrive.ListViewContextSelection[0].FileDetails.Orignal ? _mydrive.ListViewContextSelection[0].FileDetails.Orignal : id
            toCopy.Orignal = _controls.ToEJSON('objectid', id);
            toCopy.OwnerID = _controls.ToEJSON('objectid', _mydrive.ListViewContextSelection[0].FileDetails.OwnerID);
            _mydrive.ListViewContextSelection[0].FileDetails.ThumbnailId == null ? toCopy.ThumbnailId = null : toCopy.ThumbnailId = _controls.ToEJSON('objectid', _mydrive.ListViewContextSelection[0].FileDetails.ThumbnailId);
            toCopy.CompanyID = _controls.ToEJSON('objectid', _mydrive.ListViewContextSelection[0].FileDetails.CompanyID);
            delete toCopy._id
            let CanUpload = _mydrive.CanUpload(page, [parseInt(toCopy.Size != null ? toCopy.Size : 0)])
            if (CanUpload) {
                CopyStatusContainer.removeClass('d-none').addClass('d-flex');
                CopyStatusContainer.find('.copy').html(`Creating ${toCopy.FileName}`);
                toCopy.UserID = _controls.ToEJSON("objectid", _initData.User._id);
                toCopy.FolderId = (toCopy.FolderId == 'root') ? toCopy.FolderId : _controls.ToEJSON("objectid", toCopy.FolderId);
                toCopy.CreatedBy = _controls.ToEJSON("objectid", _initData.User._id);

                _drive.CopyFile([toCopy]).then((res) => {
                    if (res != 200) {
                        CopyStatusContainer.removeClass('d-flex').addClass('d-none')
                    }
                }).catch((e) => {
                    CopyStatusContainer.find('.copy').html(` `);
                    CopyStatusContainer.addClass('d-none').removeClass('d-flex');
                }).finally((e) => {
                    _mydrive.ListViewContextSelection = [];
                    _mydrive.ViewInfo['LastPage'] = false;
                    _mydrive.ViewInfo['PageSize'] = 60;
                    _mydrive.ViewInfo['PageNumber'] = 1;
                    _mydrive.ViewInfo['Loading'] = true;
                    page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
                    page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                    CopyStatusContainer.find('.copy').html(`Done`)
                    CopyStatusContainer.removeClass('d-flex').addClass('d-none')
                })
            } else {
                swal('Please Upgrade Your Drive Storage Space', { icon: "info" })
            }
        } else if (selectionType == 'delete') {

            _common.ConfirmDelete().then((confirm) => {
                if (confirm) {
                    let EjsonIds = _controls.ToEJSON("objectid", id)
                    let Obj = {
                        id: EjsonIds,
                        size: size,
                        defaultDoc: DefaultDoc
                    }
                    let Container;
                    if (_mydrive.ViewInfo["ViewType"] == 'ListView') {
                        Container = page.PageContainer.find('.main-drive-grid')
                    } else {
                        Container = page.PageContainer.find('.grid-table-container')
                    }
                    _common.ContainerLoading(true, Container)
                    _drive.DeleteContent([Obj]).then(() => {
                        _common.ContainerLoading(false, Container)
                        _mydrive.ListViewContextSelection = [];
                        _mydrive.ViewInfo['LastPage'] = false;
                        _mydrive.ViewInfo['PageSize'] = 60;
                        _mydrive.ViewInfo['PageNumber'] = 1;
                        _mydrive.ViewInfo['Loading'] = true;
                        page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
                        page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                    })
                }

            });
        } else if (selectionType == 'rename') {

            let body = `<div class="row">
                                <div class="form-group col-sm-12">
                                    <label class="small">New Name</label>
                                        <input type="text" class="form-control form-control-sm newname" />
                                </div>
                            </div>`

            let Title = type == 'file' ? 'Rename File' : 'Rename Folder';
            let RenameModal = _common.DynamicPopUp({ title: Title, body: body })
            RenameModal.modal("show")
            RenameModal.find(".modal-dialog").removeClass('modal-lg')
            RenameModal.find('.newname').val(name);
            let Extension = [];
            if (name.includes('.') && type == 'file') {
                let NameArray = name.split('.');
                Extension = NameArray[NameArray.length - 1];
            }
            RenameModal.find('.newname').on('input', function (e) {
                if (e.target.value.length > 0) {
                    RenameModal.find('.btnSave').prop("disabled", false)
                } else {
                    RenameModal.find('.btnSave').prop("disabled", true)
                }

            })
            RenameModal.find('.btnSave').one('click', function (e) {
                let NewName = RenameModal.find('.newname').val();
                let NewNameArray = NewName.split('.')
                let NewExtension = NewNameArray[NewNameArray - 1];
                if (NewExtension != Extension && type == 'file') {
                    NewName += `.${Extension}`
                }
                //   console.log(NewExtension,Extension,NewName);
                _mydrive.ListViewContextSelection = [];
                _drive.RenameFile(id, NewName, type).then(() => {
                    _mydrive.ViewInfo['LastPage'] = false;
                    _mydrive.ViewInfo['PageSize'] = 60;
                    _mydrive.ViewInfo['PageNumber'] = 1;
                    _mydrive.ViewInfo['Loading'] = true;
                    page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false)
                    // page.GridView.ReloadData(null,'myUploads',false);
                    page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                    // self.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId)
                    RenameModal.modal('hide')
                })
            })
        } else if (selectionType == 'share-internally') {
            _mydrive.InternalExternalSharingWrapper(_mydrive.ListViewContextSelection, "Internal", page)
            // new _mydrive.openInternalShareModal(_mydrive.ListViewContextSelection[0], page)

        } else if (selectionType == 'share-externally') {
            _mydrive.InternalExternalSharingWrapper(_mydrive.ListViewContextSelection, "External", page)
            // new _mydrive.openExternalShareModal(_mydrive.ListViewContextSelection[0], page)

        } else if (selectionType == 'move') {

            let MoveModal = page.PageContainer.find('.move-modal');
            MoveModal.modal("show");
            new _mydrive.MoveModalView(MoveModal, page);
        } else if (selectionType == 'remove') {
            _mydrive.removeSharing([_mydrive.ListViewContextSelection[0].FileDetails._id], page)
        }


    })

    //folder Selection in move modal;    
    page.PageContainer.on('click', '.move-folder-list .folder-item', function () {
        let FolderId = $(this).attr('data-folderid');
        let FolderName = $(this).text();
        if (FolderId != 'nofolders') {
            _mydrive.MoveFileBreadCrumb.push({ 'ViewValue': FolderName, 'FolderId': FolderId })
            page.MoveModalView.ReloadData(false, FolderId);
        }
    })

    // move breadcrum view
    page.PageContainer.on('click', '.move-breadcrumb .move-breadcrumb-backicon', function (e) {
        let FolderId = $(this).attr('data-folderid');
        let selectedindex = _mydrive.MoveFileBreadCrumb.findIndex((o) => o.FolderId == FolderId);
        _mydrive.MoveViewInfo['PageNumber'] = 1;
        _mydrive.MoveViewInfo['Loading'] = false;
        _mydrive.MoveViewInfo['LastPage'] = false
        // _mydrive.ListViewContextSelection = [];
        if (selectedindex > -1 && selectedindex != 0) {
            let id = _mydrive.MoveFileBreadCrumb[selectedindex - 1].FolderId;
            _mydrive.MoveFileBreadCrumb.splice(selectedindex, _mydrive.SelectedFolder.length);
            page.MoveModalView.ReloadData(false, id);
        } else if (selectedindex == 0) {
            let id = _mydrive.MoveFileBreadCrumb[selectedindex].FolderId;
            _mydrive.MoveFileBreadCrumb.splice(selectedindex, _mydrive.SelectedFolder.length);
            page.MoveModalView.ReloadData(false, id);
        }
    })

    // move for list view
    page.PageContainer.on('click', '.move-modal .list-move-btn', function (e) {
        //Id of selected file/folder

        // _common.Loading(true)
        let ToMove = [];
        let FolderId;
        _mydrive.MoveFileBreadCrumb[_mydrive.MoveFileBreadCrumb.length - 1].FolderId == 'root' ? FolderId = 'root' : FolderId = _controls.ToEJSON("objectid", _mydrive.MoveFileBreadCrumb[_mydrive.MoveFileBreadCrumb.length - 1].FolderId)
        let MoveTo = FolderId;

        for (let j = 0; j < _mydrive.ListViewContextSelection.length; j++) {
            let SelectedFileId = _controls.ToEJSON('objectid', _mydrive.ListViewContextSelection[j].ID);
            let date = _controls.ToEJSON('datetime', new Date().toISOString());
            ToMove.push({ moveTo: MoveTo, date: date, fileId: SelectedFileId });
        }
        let MoveModal = page.PageContainer.find('.move-modal');
        _drive.MoveFilesOrFolder(ToMove).then((res) => {

            // page.PageContainer.find('.btnDelete').addClass('d-none');
            // page.PageContainer.find('.copy-btn').addClass('d-none');
            // page.PageContainer.find('.move-btn').addClass('d-none');
            // page.PageContainer.find('.btnDownload').addClass('d-none');
            _mydrive.ListViewContextSelection = [];
            _mydrive.ViewInfo.PageNumber = 1;
            page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false)
            page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
            MoveModal.modal("hide");
            _mydrive.ListViewSelectedFileIds = [];
        }).finally(() => {
            // _common.Loading(false)
        })

    })

    page.PageContainer.on('click', '.drivecontent .file-box .card', function (e) {
        page.PageContainer.find('.drive-selected').removeClass('drive-selected');
        $(this).addClass('drive-selected');
    })

    //send notification on list view
    page.PageContainer.find('#sendNotification').on('click', function () {
        let checked = $(this).is(":checked")
        if (checked) {
            page.PageContainer.find('.message').show()
        } else {
            page.PageContainer.find('.message').hide()
        }
    })
    //share notification on list view
    page.PageContainer.find('#shareFile').on('shown.bs.modal', function () { })
    // Rename functionality
    page.PageContainer.find(".grid-view-content").on('click', '.fileoptions .rename-file , .fileoptions .rename-folder', (e) => {
        let id, type
        if ($(e.target).parent()[0].hasAttribute('data-folder')) {
            id = $(e.target).parent().attr('data-folder')
            name = $(e.target).parent().attr('data-filename');
            type = "folder"
        } else {
            id = $(e.target).parent().attr('data-pid')
            name = $(e.target).parent().attr('data-filename');
            type = "file"
        }
        let body = `<div class="row">
                                  <div class="form-group col-sm-12">
                                      <label class="small">New Name</label>
                                          <input type="text" class="form-control form-control-sm newname" />
                                  </div>
                  </div>`
        let RenameModal = _common.DynamicPopUp({ title: 'Rename Folder', body: body })
        RenameModal.modal("show")
        RenameModal.find(".modal-dialog").removeClass('modal-lg')
        RenameModal.find('.newname').val(name);

        RenameModal.find('.btnSave').one('click', (e) => {
            let NewName = RenameModal.find('.newname').val();

            _drive.RenameFile(id, NewName, type).then(() => {
                page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId)
                page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"]);
                RenameModal.modal('hide')
            })
        });

    });
    //Delete functionality
    page.PageContainer.on('click', '.fileoptions .delete-file, .fileoptions .delete-folder', (e) => {
        let id
        // console.log($(e.target).parent());
        if ($(e.target).parent()[0].hasAttribute('data-folder')) {
            id = $(e.target).parent().attr('data-folder')
        } else {
            id = $(e.target).parent().attr('data-pid')
        }

        _common.ConfirmDelete().then((confirm) => {
            if (confirm) {
                let EjsonIds = _controls.ToEJSON("multilookup", [id])
                _drive.DeleteContent(EjsonIds).then(() => {
                    page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"])
                    page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"]);

                })
            }
        });

    });
    //Delete functionality
    page.PageContainer.on('click', '.fileoptions .delete-file, .fileoptions .delete-folder', (e) => {
        let id
        // console.log($(e.target).parent());
        if ($(e.target).parent()[0].hasAttribute('data-folder')) {
            id = $(e.target).parent().attr('data-folder')
        } else {
            id = $(e.target).parent().attr('data-pid')
        }

        _common.ConfirmDelete().then((confirm) => {
            if (confirm) {
                let EjsonIds = _controls.ToEJSON("multilookup", [id])
                _drive.DeleteContent(EjsonIds).then(() => {
                    page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"])
                    page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"]);
                    // self.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId)
                })
            }

        });
    });
    //toggle  on selectfile in grid view
    // page.PageContainer.on('change', ".grid-view-content .chkSelect", function(e)  {
    //     let checked = $(this).is(":checked");
    //     console.log("1:",checked)
    //     if(!checked){
    //         $(this).find('.chkSelect').attr('checked',true);
    //         $(this).addClass('drive-selected');

    //     }else{
    //         $(this).find('.chkSelect').attr('checked',false);
    //         $(this).removeClass('drive-selected');
    //     }
    //     let selectedContent = $(this).closest('.gridfile-box,.gridfolder-box')[0].data;
    //     _mydrive.GridViewCheckBoxClick(page,!checked,selectedContent);
    //     e.stopPropagation();
    // })

    //to check all the files in the grid view
    page.PageContainer.on('click', '.chkSelectAll', (e) => {

        let AllCheckboxes = page.PageContainer.find(".grid-view-content [type='checkbox']");
        let DeleteBtn = page.PageContainer.find('.btnDelete');
        let CopyBtn = page.PageContainer.find('.copy-btn');
        let MoveBtn = page.PageContainer.find('.move-btn')
        if (!$(e.target).is(":checked")) {
            $.each(AllCheckboxes, function (index, value) {
                let id = $(value).attr('data-id').toString();
                let SelectedContent = $(value).closest('.gridfile-box,.gridfolder-box')[0].data
                _mydrive.ListViewSelectedFileIds = _mydrive.ListViewSelectedFileIds.filter(o => o._id != SelectedContent._id);
            });
            AllCheckboxes.prop('checked', false);
            DeleteBtn.addClass('d-none')
            CopyBtn.addClass('d-none');
            MoveBtn.addClass('d-none');
        } else {
            $.each(AllCheckboxes, function (index, value) {
                let id = $(value).attr('data-id')
                let SelectedContent = $(value).closest('.gridfile-box,.gridfolder-box')[0].data
                if (!_mydrive.ListViewSelectedFileIds.find(o => o._id == (SelectedContent._id))) _mydrive.ListViewSelectedFileIds.push(SelectedContent);
            });
            AllCheckboxes.prop('checked', true);
            DeleteBtn.removeClass('d-none');
            CopyBtn.removeClass('d-none');
            MoveBtn.removeClass('d-none');

        }

    })
    // delete a particular file in grid view
    page.PageContainer.on('click', '.btnDelete', (e) => {


        if (_mydrive.ListViewSelectedFileIds.length > 0) {
            _common.ConfirmDelete().then((confirm) => {
                _common.ContainerLoading(true, page.PageContainer.find('.grid-table-container'))
                let TotalSize = 0;
                if (confirm) {
                    let Files = [];
                    for (let j = 0; j < _mydrive.ListViewSelectedFileIds.length; j++) {
                        let obj = {};
                        obj['id'] = _controls.ToEJSON("lookup", _mydrive.ListViewSelectedFileIds[j]._id);
                        obj['size'] = _mydrive.ListViewSelectedFileIds[j].Size;
                        Files.push(obj)
                    }

                    _drive.DeleteContent(Files).then(() => {

                        // page.PageContainer.find('.btnDelete').addClass('d-none');
                        // page.PageContainer.find('.copy-btn').addClass('d-none');
                        // page.PageContainer.find('.move-btn').addClass('d-none');
                        // page.PageContainer.find('.chkSelectAll').prop('checked', false);
                        // page.PageContainer.find('.btnDownload').addClass('d-none');
                        _common.ContainerLoading(false, page.PageContainer.find('.grid-table-container'));

                        _mydrive.ViewInfo['LastPage'] = false;
                        _mydrive.ViewInfo['PageSize'] = 60;
                        _mydrive.ViewInfo['PageNumber'] = 1;
                        _mydrive.ViewInfo['Loading'] = true;

                        page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"])
                        page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"]);
                        _mydrive.ListViewSelectedFileIds = [];
                    })
                } else {
                    _common.ContainerLoading(false, page.PageContainer.find('.grid-table-container'))
                }

            })
        } else {
            swal('Please select a file/folder to delete', { icon: 'info' })
        }
    })

    // folder click functionality
    page.PageContainer.on('dblclick', '.folder-content', (e) => {
        _mydrive.ListViewSelectedFileIds = []
        _mydrive.ListViewContextSelection = []
        let folderId = $(e.target).closest("tr").attr("data-id");
        let folderName = $(e.target).closest("tr").find('.folder-name').attr('data-original-title')
        _mydrive.ViewInfo['Loading'] = true;
        _mydrive.ViewInfo['LastPage'] = false;
        _mydrive.ViewInfo['PageNumber'] = 1;
        _mydrive.SelectedFolder.push({ 'ViewValue': folderName, 'FolderId': folderId, 'type': _mydrive.ViewInfo["ViewType"] });
        if (_mydrive.ViewInfo.CurrentView == 'ListView') {
            page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"])
        } else {
            page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"]);
        }
    });

    page.PageContainer.on('click', '.lblChk', function (e) {
        page.PageContainer.find('.drive-rightclick-menu').addClass('d-none');
        let selectedContent = $(this).closest('.gridfile-box,.gridfolder-box')[0].data;
        let checked = $(this).find('.chkSelect').is(":checked");
        if (!checked) {
            $(this).find('.chkSelect').attr('checked', true);
            $(this).closest('.gridfile-box,.gridfolder-box').addClass('drive-selected');

        } else {
            $(this).find('.chkSelect').attr('checked', false);
            $(this).closest('.gridfile-box,.gridfolder-box').removeClass('drive-selected');
        }
        _mydrive.GridViewCheckBoxClick(page, !checked, selectedContent);
        e.stopPropagation()
        e.preventDefault();

    })
    // page.PageContainer.on('click', '.gridfile-box,.gridfolder-box', function (e) {
    //     page.PageContainer.find('.drive-rightclick-menu').addClass('d-none');
    //     let selectedContent = $(this).closest('.gridfile-box,.gridfolder-box')[0].data;
    //     let checked = $(this).find('.chkSelect').is(":checked");

    //     if (!checked) {
    //         $(this).find('.chkSelect').attr('checked', true);
    //       //  $(this).addClass('drive-selected');

    //     } else {
    //         $(this).find('.chkSelect').attr('checked', false);
    //       //  $(this).removeClass('drive-selected');
    //     }
    //     _mydrive.GridViewCheckBoxClick(page, !checked, selectedContent);
    //     e.stopPropagation()
    //     e.preventDefault();
    // });

    // File/Folder Search in all views
    page.PageContainer.on('input', '.search-in-list .txtSearchInGrid', function (e) {
        let searchString = e.target.value;
        // if (_mydrive.ViewInfo['Loading'] == false) {
        _mydrive.ViewInfo['LastPage'] = false;
        _mydrive.ViewInfo['PageSize'] = 60;
        _mydrive.ViewInfo['PageNumber'] = 1;
        _mydrive.ViewInfo['Loading'] = true;
        page.ListView.ReloadData(searchString, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false)
        page.GridView.ReloadData(searchString, _mydrive.ViewInfo["ViewType"], false);

        // }else{
        //     _mydrive.ViewInfo['LastPage'] = false;
        //     _mydrive.ViewInfo['PageSize'] = 60;
        //     _mydrive.ViewInfo['PageNumber'] = 1;
        //     _mydrive.ViewInfo['Loading'] = true;
        //     page.ListView.ReloadData(searchString, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, 'myUploads', false)
        //     page.GridView.ReloadData(searchString, 'myUploads', false);

        // }

        e.preventDefault();

    })

    // folder  Click In ListView
    page.PageContainer.on('dblclick', '.main-drive-grid .folder-section .align-items-center', function (e) {
        _mydrive.ListViewSelectedFileIds = []
        _mydrive.ListViewContextSelection = []
        page.PageContainer.find('.folder-head').addClass('d-none');
        page.PageContainer.find('.file-head').addClass('d-none');

        let folderId = $(this).attr("data-folder");
        let folderName = e.target.innerText;
        _mydrive.ViewInfo.PageNumber = 1;
        _mydrive.ViewInfo.LastPage = false;
        _mydrive.ViewInfo['Loading'] = true;

        if (!_mydrive.SelectedFolder.some(o => o.FolderId === folderId)) {
            _mydrive.SelectedFolder.push({ 'ViewValue': folderName, 'FolderId': folderId })
        }
        if (_mydrive.ViewInfo.CurrentView == 'ListView') {
            page.ListView.ReloadData(null, folderId, _mydrive.ViewInfo["ViewType"], false)
        } else {
            page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
        }

    });
    page.PageContainer.on('click', '.folder-box .card', function (e) {
        page.PageContainer.find('.drive-selected').removeClass('drive-selected');
        $(this).addClass('drive-selected');
    });

    //BreadCrumb Click Function
    page.PageContainer.on('click', '.card-body .breadcrumb .breadcrumb-item', function (e) {

        let folderId = $(this).attr('data-folderId');
        _mydrive.ViewInfo['PageNumber'] = 1
        _mydrive.ViewInfo.LastPage = false;
        _mydrive.ViewInfo['Loading'] = true;
        // page.PageContainer.find('.folder-head').addClass('d-none');
        // page.PageContainer.find('.file-head').addClass('d-none');
        // page.PageContainer.find('.copy-btn').addClass('d-none');
        // page.PageContainer.find('.move-btn').addClass('d-none');
        // page.PageContainer.find('.btnDownload').addClass('d-none');
        // page.PageContainer.find('.btnDelete').addClass('d-none');
        if (_mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId == 'shared' || folderId == 'shared') {

            _mydrive.SelectedFolder = [{ 'ViewValue': 'Shared with me ', 'FolderId': 'shared' }]
            page.PageContainer.find('.main-drive-grid .folder-section').html('');
            page.PageContainer.find('.main-drive-grid .drivecontent').html('');
            page.ListView.ReloadData(null, 'root', 'shared')
            page.GridView.ReloadData(null, shared);
        }

        if (_mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId != folderId && _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId != 'shared') {
            let selectedindex = _mydrive.SelectedFolder.findIndex((o) => o.FolderId == folderId)
            if (selectedindex > -1) {
                _mydrive.SelectedFolder.splice(selectedindex + 1, _mydrive.SelectedFolder.length)
            }
            page.PageContainer.find('.main-drive-grid .folder-section').html('');
            page.PageContainer.find('.main-drive-grid .drivecontent').html('');
            if (_mydrive.ViewInfo.CurrentView == "GridView") {
                page.PageContainer.find('.main-drive-list').show();
                page.PageContainer.find('.main-drive-grid').addClass('d-none');
                page.GridView.ReloadData(null, _mydrive.ViewInfo.ViewType, false);
            } else {
                page.PageContainer.find('.main-drie-list').hide();
                page.PageContainer.find('.main-drive-grid').removeClass('d-none');
                page.ListView.ReloadData(null, folderId, _mydrive.ViewInfo.ViewType)
            }

        }


    })

    //List view infinite scroll event
    page.PageContainer.find('.main-drive-grid').on('scroll', function () {

        if ($(page.PageContainer.find('.main-drive-grid')).scrollTop() >= $(page.PageContainer.find('.list-scroll')).height() - $(page.PageContainer.find('.main-drive-grid')).height() - 15) {
            //Add something at the end of the page 
            if (!_mydrive.ViewInfo['Loading'] && _mydrive.ViewInfo['CurrentView'] == 'ListView' && !_mydrive.ViewInfo['LastPage']) {
                _mydrive.ViewInfo['Loading'] = true;
                _mydrive.ViewInfo.PageNumber++;
                page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"]);
                _mydrive.AppendLoader(page.PageContainer);
            } else if (!_mydrive.ViewInfo['Loading'] && _mydrive.ViewInfo['CurrentView'] == 'GridView' && !_mydrive.ViewInfo['LastPage']) {
                _mydrive.ViewInfo['Loading'] = true;
                _mydrive.ViewInfo.PageNumber++;
                page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"]);
                _mydrive.AppendLoader(page.PageContainer);

            }
        }
    });

    //scroll on Move Section
    page.PageContainer.find('.move-file-container').on('scroll', function () {

        if ($(page.PageContainer.find('.move-file-container')).scrollTop() >= $(page.PageContainer.find('.move-folder-list')).height() - $(page.PageContainer.find('.move-file-container')).height() - 15) {
            if (!_mydrive.MoveViewInfo.Loading && !_mydrive.MoveViewInfo.LastPage) {
                _mydrive.MoveViewInfo.Loading = true;
                _mydrive.MoveViewInfo.PageNumber++
                _mydrive.AppendLoader(page.PageContainer);
                page.MoveModalView.ReloadData(true, _mydrive.MoveFileBreadCrumb[_mydrive.MoveFileBreadCrumb.length - 1].FolderId);
            }

        }
    })

    //Grid view For Moving MultipleItems
    page.PageContainer.on('click', '.gird-button-list .move-btn', function () {
        if (_mydrive.ListViewSelectedFileIds.length > 0) {
            _mydrive.ListViewContextSelection = [];
            _mydrive.MoveViewInfo.LastPage = false
            _mydrive.MoveViewInfo.Loading = false;
            _mydrive.MoveViewInfo.PageNumber = 1;
            _mydrive.MoveFileBreadCrumb = [{ 'ViewValue': 'Drive', 'FolderId': 'root' }];
            let MoveModal = page.PageContainer.find('.move-modal');
            MoveModal.modal("show");
            let SelectedFiles = [];
            for (let j = 0; j < _mydrive.ListViewSelectedFileIds.length; j++) {
                let ID;
                _mydrive.ListViewSelectedFileIds[j]._id == 'root' ? ID = 'root' : ID = _controls.ToEJSON('objectid', _mydrive.ListViewSelectedFileIds[j]._id);
                SelectedFiles.push({ Type: _mydrive.ListViewSelectedFileIds[j].Type, ID: ID, Name: _mydrive.ListViewSelectedFileIds[j].FileName, FileDetails: _mydrive.ListViewSelectedFileIds[j] })

            }
            _mydrive.ListViewContextSelection = [...SelectedFiles];

            new _mydrive.MoveModalView(MoveModal, page);
        } else {
            swal('Please select a file/folder to move', { icon: 'info' })
        }

    });


    //MultipleCopy for List View
    page.PageContainer.on('click', '.gird-button-list .copy-btn', function () {
        _mydrive.ViewInfo['CurrentView'] = 'GridView';
        _mydrive.ViewInfo['PageNumber'] = 1;
        let CopyStatusContainer = page.PageContainer.find('.copy-container');
        CopyStatusContainer.attr("type", "copy")
        let FilesArray = [];
        let SizeArray = [];
        if (_mydrive.ListViewSelectedFileIds.length > 0) {
            for (let i = 0; i < _mydrive.ListViewSelectedFileIds.length; i++) {
                let toCopy = {};
                toCopy = { ..._mydrive.ListViewSelectedFileIds[i] };
                toCopy.FileName = `Copy of ${_mydrive.ListViewSelectedFileIds[i].FileName}`;
                toCopy.UploadDate = _controls.ToEJSON('datetime', new Date().toISOString());
                toCopy.UserID = _controls.ToEJSON("objectid", _mydrive.ListViewSelectedFileIds[i].UserID);
                toCopy.CreatedBy = _controls.ToEJSON("objectid", _mydrive.ListViewSelectedFileIds[i].CreatedBy);
                let Oid = _mydrive.ListViewSelectedFileIds[i].Orignal ? _mydrive.ListViewSelectedFileIds[i].Orignal : _mydrive.ListViewSelectedFileIds[i]._id;
                toCopy.Orignal = _controls.ToEJSON('objectid', Oid);
                toCopy.OwnerID = _controls.ToEJSON('objectid', _mydrive.ListViewSelectedFileIds[i].OwnerID);
                _mydrive.ListViewSelectedFileIds[i].ThumbnailId == null ? toCopy.ThumbnailId = null : toCopy.ThumbnailId = _controls.ToEJSON('objectid', _mydrive.ListViewSelectedFileIds[i].ThumbnailId);
                toCopy.CompanyID = _controls.ToEJSON('objectid', _mydrive.ListViewSelectedFileIds[i].CompanyID);
                toCopy.FolderId = (_mydrive.ListViewSelectedFileIds[i].FolderId == 'root') ? 'root' : _controls.ToEJSON("objectid", _mydrive.ListViewSelectedFileIds[i].FolderId);
                delete toCopy._id;
                FilesArray.push(toCopy);
                SizeArray.push(parseInt(toCopy.Size == null ? 0 : toCopy.Size));
            }
            let CanUpload = _mydrive.CanUpload(page, SizeArray);
            if (CanUpload) {
                CopyStatusContainer.removeClass('d-none').addClass('d-flex');
                CopyStatusContainer.find('.close-copy-conatainer').addClass('d-none');
                CopyStatusContainer.find('.copy').html(`Coping ${FilesArray.length}  ${FilesArray.length == 1 ? 'File' : 'Files'}`);

                _drive.CopyFile(FilesArray).then((statusCode) => {
                    if (statusCode != 200) {
                        CopyStatusContainer.removeClass('d-flex').addClass('d-none');
                    }
                    $(this).blur();
                }).finally(() => {
                    _mydrive.ViewInfo['LastPage'] = false;
                    _mydrive.ViewInfo['PageSize'] = 60;
                    _mydrive.ViewInfo['PageNumber'] = 1;
                    _mydrive.ViewInfo['Loading'] = true;
                    page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                    page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
                    CopyStatusContainer.find('.copy').html(`Done`)
                    CopyStatusContainer.removeClass('d-flex').addClass('d-none')
                    _mydrive.ListViewSelectedFileIds = [];
                })
            } else {
                swal('Please Upgrade Your Drive Storage Space', { icon: 'info' })
            }
        } else {
            swal('Please select a file/folder to copy', { icon: 'info' })
        }
    })


    //
    page.PageContainer.on('click', '.close-move-modal', function () {
        _mydrive.MoveViewInfo.PageNumber = 1;
        _mydrive.MoveViewInfo.Loading = false;
        _mydrive.MoveViewInfo.LastPage = false;
        // page.PageContainer.find('.btnDelete').addClass('d-none');
        // page.PageContainer.find('.copy-btn').addClass('d-none');
        // page.PageContainer.find('.move-btn').addClass('d-none');
        // page.PageContainer.find('.btnDownload').addClass('d-none');
        let AllCheckboxes = page.PageContainer.find(".grid-view-content [type='checkbox']");
        AllCheckboxes.attr('checked', false);
        page.PageContainer.find(".drive-selected").removeClass('drive-selected');
        _mydrive.ListViewContextSelection = [];
        _mydrive.ListViewSelectedFileIds = [];
        page.PageContainer.find('.move-modal').modal('hide');

    })
    //Grid view Scroll
    page.PageContainer.find('.main-drive-list').on('scroll', function () {
        if ($(page.PageContainer.find('.main-drive-list')).scrollTop() >= $(page.PageContainer.find('.grid-view-content')).height() - $(page.PageContainer.find('.main-drive-list')).height() - 15) {

            if (!_mydrive.ViewInfo.Loading && !_mydrive.ViewInfo.LastPage) {
                _mydrive.ViewInfo
                _mydrive.ViewInfo.Loading = true;
                _mydrive.ViewInfo.PageNumber++;
                page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], true);
                _mydrive.AppendLoader(page.PageContainer);
            }
        }

    })

    page.PageContainer.on('click', '.request-space', function () {
        let Name = _initData.User.FirstName;
        let isAdmin = _initData.ProfileAuth && _initData.ProfileAuth.IsAdmin ? _initData.ProfileAuth.IsAdmin : false;
        _drive.RequestMoreSpace(Name, isAdmin).then(() => {
            swal('Request Sent', { icon: "success" });
        })
    })

    //To remove sharing (in list view)
    page.PageContainer.on("click", ".removeSharingbtn", function () {
        if (_mydrive.ListViewSelectedFileIds.length > 0) {
            let ids = _mydrive.ListViewSelectedFileIds.map(ent => ent._id);
            _mydrive.removeSharing(ids, page)
        } else {
            swal("Please Select a File!", { icon: 'error' })
        }
    })

    // Internal Sharing (in list view)
    page.PageContainer.on("click", ".internal-share-btn", function () {
        _mydrive.InternalExternalSharingWrapper(_mydrive.ListViewSelectedFileIds, "Internal", page)
    })

    page.PageContainer.on("click", ".external-share-btn", function () {
        _mydrive.InternalExternalSharingWrapper(_mydrive.ListViewSelectedFileIds, "External", page)
    })


    // To Download a Folder (Works for Grid And List View Both)
    page.PageContainer.on('click', '.downloadfolder', function () {
        let StatusContainer = page.PageContainer.find('.copy-container');

        StatusContainer.attr("type", "zip");
        StatusContainer.removeClass('d-none');
        StatusContainer.find('.close-copy-container').removeClass('d-none');
        StatusContainer.find('.copy').html('Zipping files ...');
        let SelectedContent = _mydrive.ListViewContextSelection[0].FileDetails;
        let id;
        (SelectedContent.Orignal) ? id = SelectedContent.Orignal : id = SelectedContent._id;
        let FolderContent = [{ id: id, name: SelectedContent.FileName, ext: null, type: 'folder', size: SelectedContent.Size }];
        _drive.CreateZip(FolderContent).then((res) => {
            if (res.statusCode == 200) {
                StatusContainer.addClass('d-none')
                window.open(res.url);

            } else if (res.statusCode == 401) {
                swal('Folder Size Exceeded Max Limit', 'error');
            }
            _mydrive.ListViewContextSelection = []

        });

    })
    //To Download Multiple File in Grid View(Works For Grid View)
    page.PageContainer.on('click', '.download-btn', function (e) {

        // if (_mydrive.ListViewSelectedFileIds.length > 0) {
        let ToDownload = []
        let SelectedFolder = $(this).closest('.gridfolder-box')[0].data;
        let StatusContainer = page.PageContainer.find('.copy-container');
        StatusContainer.attr("type", "zip")
        StatusContainer.removeClass('d-none');
        StatusContainer.find('.close-copy-container').removeClass('d-none');
        StatusContainer.find('.copy').html('Zipping files Please Wait...');
        let id;
        let defaultFile = false;
        SelectedFolder.Orignal ? id = SelectedFolder.Orignal : id = SelectedFolder._id;
        SelectedFolder.DefaultDoc && SelectedFolder.DefaultDoc == true ? defaultFile = true : defaultFile = false;
        if (SelectedFolder.Type == 'file') {
            ToDownload.push({ id: id, name: SelectedFolder.FileName, ext: SelectedFolder.Extension, type: SelectedFolder.Type, size: SelectedFolder.Size, DefaultDoc: defaultFile })
        } else {
            ToDownload.push({ id: id, name: SelectedFolder.FileName, ext: null, type: SelectedFolder.Type, size: null, DefaultDoc: defaultFile })
        }


        _drive.CreateZip(ToDownload).then((res) => {
            if (res.statusCode == 200) {
                window.open(res.url);

            } else if (res.statusCode == 401) {
                swal('Folder Size Exceeded Max Limit', 'error');
            }

        }).finally(() => {
            $(e.target).blur();
            StatusContainer.addClass('d-none')
            // _mydrive.ListViewSelectedFileIds = [];
            // _mydrive.ListViewContextSelection = [];
            // StatusContainer.addClass('d-none');
            // _mydrive.ViewInfo['LastPage'] = false;
            // _mydrive.ViewInfo['PageSize'] = 60;
            // _mydrive.ViewInfo['PageNumber'] = 1;
            // _mydrive.ViewInfo['Loading'] = true;
            // page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
            // page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"]);
        }).catch((err) => {
            if (err && err.status != 0) {
                console.log("Error While Zipping Files:", err);
                swal('Internal Server Error', { icon: 'error' })
            }
        })

        // } else {
        //     swal('Please select a file/folder to download', { icon: 'info' });
        // }
    })

}

_mydrive.InternalExternalSharingWrapper = function (entity, scope, page) {
    if (_initData.User.MySettings && _initData.User.MySettings.DriveSettings && _initData.User.MySettings.DriveSettings.CanShare == false) {
        swal("You don't have permission to share file", { icon: "error" });
        return
    }
    if (entity.length == 0) {
        swal("Please select a file.", { icon: "info" })
        return
    }
    if (entity.length > 1 || entity[0].Type == "folder") {
        swal("Multiple files can not be shared. Please select the single file.", { icon: "info" });
        return
    }
    _common.Loading(true);
    entity = entity[0];
    let entId = null;
    if (entity.FileDetails && entity.FileDetails._id) {
        entId = entity.FileDetails._id
    } else if (entity._id) {
        entId = entity._id
    } else {
        swal("Please refresh the page", { icon: "error" })
    }
    _drive.GetEntityData(entId).then(
        entityData => {
            _common.Loading(false);
            if (!entityData) {
                swal("File not found. May be the file is removed.");
                return
            }
            switch (scope) {
                case "Internal":
                    new _mydrive.openInternalShareModal(entityData, page)
                    break;
                case "External":
                    new _mydrive.openExternalShareModal(entityData, page)
                    break;
            }
        }
    ).catch(
        e => {
            _common.Loading(false);
        }
    )
}


//SECTION -Switch view
_mydrive.toggleView = function (page, viewType) {
    window.location.hash = "";
    _mydrive.ViewInfo['LastPage'] = false;
    _mydrive.ViewInfo['PageSize'] = 60;
    _mydrive.ViewInfo['PageNumber'] = 1;
    _mydrive.ViewInfo['Loading'] = true;
    _mydrive.ViewInfo["ViewType"] = viewType;
    _mydrive.ListViewSelectedFileIds = [];
    _mydrive.ListViewContextSelection = [];
    page.PageContainer.find(".txtSearchInGrid").val("")
    page.PageContainer.find(".removeSharingbtn").addClass('d-none');
    page.PageContainer.find('.copy-btn').addClass('d-none');
    page.PageContainer.find('.move-btn').addClass('d-none');
    page.PageContainer.find('.btnDelete').addClass('d-none');
    page.PageContainer.find('.btnDownload').addClass('d-none');
    page.PageContainer.find('.external-share-btn').addClass('d-none');
    page.PageContainer.find('.internal-share-btn').addClass('d-none');



    switch (viewType) {
        case "myUploads":
            _mydrive.SelectedFolder = [{ 'ViewValue': 'Drive', 'FolderId': 'root' }];
            page.PageContainer.find('.drive-view .selected-view').text("My Uploads");
            page.PageContainer.find('.page-top-button').removeClass('d-none');
            // page.PageContainer.find('.newfolder').removeClass('d-none');
            break;
        case "shareWithMe":
            _mydrive.SelectedFolder = [{ 'ViewValue': 'Shared With Me', 'FolderId': 'root' }];
            page.PageContainer.find('.drive-view .selected-view').text("Shared with me");
            page.PageContainer.find('.page-top-button').addClass('d-none');
            break;
        case "shareByMe":
            _mydrive.SelectedFolder = [{ 'ViewValue': 'Shared By Me', 'FolderId': 'root' }];
            page.PageContainer.find('.drive-view .selected-view').text("Shared By Me");
            page.PageContainer.find('.page-top-button').addClass('d-none');
    }

}

//!SECTION
_mydrive.clearSelectionAndReload = function (formModal, page) {
    formModal.find('[data-dismiss="modal"]').on('click', () => {
        _mydrive.ViewInfo['LastPage'] = false;
        _mydrive.ViewInfo['PageSize'] = 60;
        _mydrive.ViewInfo['PageNumber'] = 1;
        _mydrive.ViewInfo['Loading'] = true;
        page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
        page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);

        _mydrive.ListViewSelectedFileIds = [];
        _mydrive.ListViewContextSelection = [];

    })
}

//SECTION - Internal Share Modal
_mydrive.openInternalShareModal = function (entityData, page) {
    // console.log(entityData)
    let popupBody = _mydrive.GetDriveTemplates("Internal_Share_Modal");
    let multilookupplaceholder = $(`<span class="text-secondary multilookup-placeholder" style="line-height:2">Enter users <span>`);
    let formModal = _common.DynamicPopUp({
        body: popupBody,
        title: entityData.FileName,
        DistroyOnHide: true,
        ClearDriveDataOnClose: true
    })

    formModal.find(".btnSave").html("Share").removeClass("btnSave").addClass("share").attr('disabled', true);
    _mydrive.clearSelectionAndReload(formModal, page);
    //Bind data in edit mode
    if (entityData.Shared) {
        if (entityData.SharedUserIDs && entityData.SharedUserIDs.length > 0) {
            formModal.find(".share-message-container").addClass('d-none');
            formModal.find(".modal-title").append(`<span class="text-secondary small d-block">${entityData.SharedUserIDs.length} users</span>`)
        } else {
            formModal.find(".shared-users-container").addClass('d-none');
        }

        if (entityData.SharedUserIDs_LookupData && entityData.SharedUserIDs_LookupData.length > 0) {
            entityData.SharedUserIDs_LookupData.forEach(function (sharedUserData) {
                formModal.find(".shared-users").append(_mydrive.GetDriveTemplates('Internal_Shared_User_List', sharedUserData))
            })

        }

        formModal.find('.shared-users').on('click', '.remove-user-share', function () {
            if (formModal.find(".shared-user-li").length == 0) {
                formModal.find(".remove-all-users").addClass("d-none");
            }
            swal({
                title: "Are you sure..!",
                text: "Would you like to remove the selected user?",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: "Yes",
                        value: true,
                        visible: true,
                        closeModal: true
                    },
                    cancel: {
                        text: "No",
                        value: null,
                        visible: true,
                        closeModal: true,
                    }

                },
                dangerMode: true,
            }).then(
                ans => {
                    if (ans) {
                        _common.Loading(true);
                        _drive.RemoveUserFromSharing(entityData._id, [$(this).closest(".shared-user-li").data().id]).then(
                            data => {
                                //remove removeall button
                                $(this).closest(".shared-user-li").remove();
                                swal("User removed successfully", { icon: "success" });
                                _common.Loading(false);
                                cleanDataAfterDelete([$(this).closest(".shared-user-li").data().id])
                            }
                        )
                    }
                }
            )
        })

        //remove all users button
        formModal.find(".remove-all-users").click(function () {

            swal({
                title: "Are you sure..!",
                text: "Would you like to remove all the user?",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: "Yes",
                        value: true,
                        visible: true,
                        closeModal: true
                    },
                    cancel: {
                        text: "No",
                        value: null,
                        visible: true,
                        closeModal: true,
                    }

                },
                dangerMode: true,
            }).then(
                ans => {
                    if (ans) {
                        _common.Loading(true);
                        let userIds = [];
                        $(".shared-users-container .shared-users .shared-user-li").each(
                            function () {
                                userIds.push($(this).data().id);
                                _drive.RemoveUserFromSharing(entityData._id, userIds).then(
                                    data => {
                                        formModal.find(".shared-users").empty();
                                        swal("User removed successfully", { icon: "success" });
                                        _common.Loading(false);
                                        cleanDataAfterDelete(userIds)

                                    }
                                )

                            }
                        )
                    }
                }
            )



        })
    } else {
        formModal.find(".shared-users-container").addClass("d-none")
    }


    //Stop duplicate entry of user
    formModal.on('onLookupChange', ".msp-ctrl-multilookup", function (e, el) {
        let newID = $(el).data().id;
        let added = true;
        formModal.find(".shared-user-li").each(function () {
            if ($(this).data().id == newID) {
                added = false
                formModal.find(`.multilookup-main .lookup-item[data-id="${newID}"]`).remove();
                swal('File already shared with the user.', { icon: "info", });
                return
            }
        })
        if (added) {
            formModal.find(".share-message-container").removeClass('d-none');
            formModal.find(".shared-users-container").addClass("d-none");
            formModal.find(".share").html("Share")
            formModal.find(".multilookup-placeholder").addClass("d-none");
            formModal.find(".share").attr("disabled", false)
        }
    })

    //added email ids are removed
    formModal.find(".multilookup-main").on("click", ".lookup-item .fa-times", function () {
        let emailsaddedlength = formModal.find(`.multilookup-main .lookup-item[data-id]`).length;
        if (emailsaddedlength == 1) {
            formModal.find(".share").attr("disabled", true)
            if (entityData.SharedUserIDs && entityData.SharedUserIDs.length > 0) {
                formModal.find(".share-message-container").addClass('d-none');
                formModal.find(".shared-users-container").removeClass("d-none");
            }
        }
    })

    let FieldSchema = {
        Name: "SharedUserIDs",
        NotIncludeSelf: true,
        UIDataType: "multilookup", DisplayName: "Users", "LookupObject": "User",
        "LookupFields": [
            "FirstName",
            "LastName"
        ]
    };
    let buildFieldObj = {
        Schema: FieldSchema,
        ColumnWidth: 12,
        Name: "SharedUserIDs",
        ObjectName: "Drive",
        IsRequired: true
    }

    let params = {};
    // params.data = entityData; //To pass data in multilookup
    params.field = buildFieldObj;
    params.container = formModal.find(".multilookup-main");
    //User multilookup
    _page.BuildField(params);

    //Share Button Event
    formModal.find("button.share").click(function () {
        $(this).attr("disabled", true)
        let SharedUserIds = _controls.GetCtrlValue({ field: buildFieldObj, container: formModal.find(".multilookup-main") }) || [];
        let NewUserIds = [...SharedUserIds];
        formModal.find(".shared-user-li").each(function () {
            SharedUserIds.push($(this).data().id)
        })

        let SharedMessage = formModal.find(".message-field").val();

        let saveObject = {
            SharedMessage,
            SharedUserIDs: SharedUserIds,
            ShareScope: "Internal",
            NewUserIds: NewUserIds,
            EntityType: entityData.Type
        }
        _drive.ShareFileAndFolder(saveObject, entityData._id).then(
            savedData => {
                swal(`Successfully Shared`, { icon: 'success' });
                formModal.modal('hide')
                _mydrive.ViewInfo['LastPage'] = false;
                _mydrive.ViewInfo['PageSize'] = 60;
                _mydrive.ViewInfo['PageNumber'] = 1;
                _mydrive.ViewInfo['Loading'] = true;

                page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
                page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                _mydrive.ListViewSelectedFileIds = [];
                _mydrive.ListViewContextSelection = [];
            }
        )
    })

    formModal.modal('show')

    let cleanDataAfterDelete = function (removedUserIds) {
        if (entityData.SharedUserIDs) {
            let newIds = [];
            entityData.SharedUserIDs.forEach(
                u => {
                    if (!removedUserIds.find(rid => rid == u)) {
                        newIds.push(u)
                    }
                }
            )
            entityData.SharedUserIDs = newIds;
        }

        if (entityData.SharedUserIDs_LookupData) {
            let newIdlookup = [];
            entityData.SharedUserIDs_LookupData.forEach(
                u => {
                    if (!removedUserIds.find(rid => rid == u._id)) {
                        newIdlookup.push(u)
                    }
                }
            )
            entityData.SharedUserIDs_LookupData = newIdlookup;
        }
        if (formModal.find(".shared-user-li").length == 0) {
            formModal.find(".share-message-container").removeClass('d-none');
            formModal.find(".remove-all-users").addClass("d-none");
            formModal.find(".shared-users-container").addClass("d-none")
        }
    }

}
//!SECTION


//SECTION -External Share Modal
_mydrive.openExternalShareModal = function (entityData, page) {
    console.log(entityData)
    let externalShareData = entityData.ExternalShare || {};
    let externalUserEmails = [];
    let expireTimeVal = null;
    let popupBody = _mydrive.GetDriveTemplates("External_Share_Modal");
    let ExpireTimeField = null;
    let formModal = _common.DynamicPopUp({
        body: popupBody,
        title: entityData.FileName,
        saveButtonName: "Share",
        DistroyOnHide: true,
        ClearDriveDataOnClose: true,
        leftSideButtonName: "Delete Link"

    })
    formModal.find(".left-button").removeClass(".btn-outline-primary").addClass("btn-outline-danger")


    formModal.modal('show');
    _mydrive.clearSelectionAndReload(formModal, page);

    //active disable fields when link expired
    let manageFieldsLinkExp = function (isBackDate) {
        formModal.find(".shareable-link").attr("disabled", isBackDate);
        formModal.find(".copy-link").attr("disabled", isBackDate);
        formModal.find("input[type='radio']").attr("disabled", isBackDate);
        formModal.find(".message-field").attr("disabled", isBackDate);
        if (isBackDate) {
            formModal.find('.to-emails').css({ "background": "#f8f9fc", "pointer-events": "none" })
        } else {
            formModal.find('.to-emails').css({ "background": "", "pointer-events": "" })
        }

        if (isBackDate && externalShareData && externalShareData.ExpireTime) {
            formModal.find(".show-error").append("Link has expired.")
        } else {
            formModal.find(".show-error").empty()
        }
    }

    let validDateAdded = function () {
        formModal.find(".show-error").empty();
        formModal.find('.btnSave').attr("disabled", false);
        manageFieldsLinkExp(false)
    }


    let externalEmailsField = new _controls.inputEmailMultiSelect("Email", formModal.find('.multilookup-main'), null, {
        alreadyAddedEmails: externalUserEmails,
        validEmailAdded: () => {
            //show message container and hide already added users if valid email added
            formModal.find(".share-message-container").removeClass("d-none");
            formModal.find(".shared-users").addClass("d-none");
            formModal.find(".btnSave").html("Share")
        },
        emailIdRemoved: function (e, allemails) {
            if (allemails.length == 0) {
                formModal.find(".share-message-container").addClass("d-none");
                formModal.find(".shared-users").removeClass("d-none");
                // formModal.find(".btnSave").html("Save")

            }
        }

    })

    //Bind data in edit mode
    if (externalShareData && Object.keys(externalShareData).length > 0) {
        formModal.find(".copy-link").prop('disabled', false).html("Copy Link");
        formModal.find(`input[type='radio'][value='${externalShareData.ShareType || "Public"}']`).attr('checked', true);
        if (externalShareData.ExternalUsers) {
            _mydrive.BindExternalUserData(externalShareData.ExternalUsers, formModal)
            externalShareData.ExternalUsers.forEach(
                usr => {
                    externalUserEmails.push(usr.Email)
                }
            )
        }

        //if Link expired
        if (externalShareData.ExpireTime) {
            formModal.find("#exp-time-checbox").prop('checked', true);
            expireTimeVal = externalShareData.ExpireTime;
            ExpireTimeField = _mydrive.BuildTimeField(formModal.find('.exp-time-pickup'), expireTimeVal, "Expiry Date", { isBackDate: manageFieldsLinkExp, validDateAdded: validDateAdded })
        }
    } else {
        formModal.find(`input[type='radio'][value='Public']`).attr('checked', true);
    }

    //Show hide message container and shared users
    if (externalShareData && externalShareData && externalShareData.ExternalUsers && externalShareData.ExternalUsers.length > 0) {
        formModal.find(".share-message-container").addClass("d-none")
    } else {
        formModal.find(".share-message-container").removeClass("d-none");
        formModal.find(".shared-users").addClass("d-none");
    }

    //Shareable link 
    if (externalShareData && externalShareData.ShareableLink) {
        formModal.find(".shareable-link").val(_constantClient.SITE_URL + "/noauth/api/driveshare/" + externalShareData.ShareableLink);
    } else {
        _drive.GetShareableLink(entityData._id).then(
            ({ SharableLink }) => {
                formModal.find(".shareable-link").val(_constantClient.SITE_URL + "/noauth/api/driveshare/" + SharableLink);
                formModal.find(".copy-link").prop('disabled', false).html("Copy Link")
            }
        )
    }



    //Copy Link Button events
    formModal.find(".copy-link").click(function () {
        formModal.find('.shareable-link').select();
        document.execCommand('copy');
        $(this).attr('data-original-title', 'Link Copied')
        setTimeout(() => {
            formModal.find(".copy-link").tooltip('show');
        }, 100);
    })

    formModal.find(".copy-link").mouseleave(function () {
        $(this).attr('data-original-title', 'Copy Shareable Link')
    })

    //Expirylink checkbox event
    formModal.find("#exp-time-checbox").click(function () {
        if ($(this).is(":checked")) {
            ExpireTimeField = _mydrive.BuildTimeField(formModal.find('.exp-time-pickup'), expireTimeVal, "Expiry Date");
        } else {
            formModal.find('.exp-time-pickup').empty();
            manageFieldsLinkExp(false)
        }
    })

    //Delete Link button
    formModal.find(".left-button").click(function () {
        swal({
            title: "Delete Link",
            text: "If you delete the link, then all the recipients you have shared it with will no longer be able to download the file via this link.",
            icon: "warning",
            buttons: true
        }).then(
            ans => {
                if (ans) {
                    _drive.DeleteExternalShareLink(entityData._id).then(
                        success => {
                            swal("Link Deleted Successfully", { icon: "success" });
                            formModal.modal("hide")
                            _mydrive.ViewInfo['LastPage'] = false;
                            _mydrive.ViewInfo['PageSize'] = 60;
                            _mydrive.ViewInfo['PageNumber'] = 1;
                            _mydrive.ViewInfo['Loading'] = true;
                            page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
                            page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);

                        }
                    )
                }
            }
        )
    })

    //Save button
    formModal.find('.btnSave').click(function () {
        let externalEmailIds = externalEmailsField.getEmailIdsFromModal();
        let newEmailIdsAdded = [...externalEmailIds];
        if (externalEmailIds == "Invalid_email") {
            return;
        }
        let saveObject = {
            ShareType: formModal.find("input[type='radio']:checked").val(),
            EntityType: entityData.Type,
            ExternalUsers: externalEmailIds.map(
                eusr => ({
                    Email: eusr,
                    AddedByOwner: true,
                    ViewDate: "",
                    AddedDate: _initData.CurrentUtcTime
                })
            ),
            newEmailIdsAdded: newEmailIdsAdded,
            Message: formModal.find(".message-field").val(),
            ShareScope: "External"
        }

        if (formModal.find("#exp-time-checbox").is(":checked")) {
            let expireTime = ExpireTimeField()
            if (!expireTime) {
                return
            }
            saveObject.ExpireTime = expireTime;
        } else {
            saveObject.ExpireTime = ""
        }

        if (entityData.ExternalShare && entityData.ExternalShare.ExternalUsers) {
            saveObject.ExternalUsers.push(...entityData.ExternalShare.ExternalUsers)
        }
        $(this).attr("disabled", true);
        _drive.ShareFileAndFolder(saveObject, entityData._id).then(
            savedData => {
                swal(`Successfully Shared`, { icon: 'success' });
                formModal.modal('hide')
                _mydrive.ViewInfo['LastPage'] = false;
                _mydrive.ViewInfo['PageSize'] = 60;
                _mydrive.ViewInfo['PageNumber'] = 1;
                _mydrive.ViewInfo['Loading'] = true;
                page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
                page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                _mydrive.ListViewSelectedFileIds = [];
            }
        )
    })

}

_mydrive.BindExternalUserData = function (externalUsers, formModal) {
    _drive.GetExternalUserData(externalUsers.map(usr => usr.Email))
        .then(
            userList => {
                let allUniqueEmailIds = [];
                externalUsers.forEach(exUser => {
                    if (exUser.AddedByOwner || exUser.Downloaded) { //if added by user or downloaded the file
                        let foundUser = userList.find(usr => usr.Email.toLowerCase() == exUser.Email.toLowerCase());
                        if (foundUser) {
                            allUniqueEmailIds.push(foundUser);
                        } else {
                            allUniqueEmailIds.push({
                                Name: "",
                                Email: exUser.Email
                            })
                        }
                    }
                })
                allUniqueEmailIds.forEach(
                    usr => {
                        let externalUserHtml = _mydrive.GetDriveTemplates('External_Shared_User_List', usr);
                        formModal.find(".shared-users").append(externalUserHtml);

                    }
                )
            }
        )
}
//!SECTION

//SECTION
_mydrive.removeSharing = function (ids, page) {
    let isfiles = ids.length > 1 ? "files" : "file"
    swal({
        title: `Remove ${isfiles}`,
        text: `Are you sure you want to remove ${isfiles} from your sharing list?`,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(
        ans => {
            if (ans) {
                _drive.RemoveSharing(ids).then(
                    success => {
                        _mydrive.ViewInfo['LastPage'] = false;
                        _mydrive.ViewInfo['PageSize'] = 60;
                        _mydrive.ViewInfo['PageNumber'] = 1;
                        _mydrive.ViewInfo['Loading'] = true;
                        page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
                        page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
                        swal("File removed successfully", { icon: "success" })

                    }
                )
                _mydrive.ListViewSelectedFileIds = [];
                _mydrive.ListViewContextSelection = [];

            }
        }
    )
}
//!SECTION



_mydrive.ConvertSize = function (bytes = 0, decimals = 2) {
    if (bytes === 0) return '0 KB';
    kbs = bytes / 1024;
    if (kbs < 1) {
        return kbs.toFixed(decimals) + " KB";
    }
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(kbs) / Math.log(k));

    return parseFloat((kbs / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

_mydrive.ConvertDate = function (date) {
    return moment(date).tz(_initData.User.MySettings.Timezone).format("DD-MM-YYYY")
}

_mydrive.GridViewCheckBoxClick = function (page, checked, selectedContent) {

    let DeleteBtn = page.PageContainer.find('.btnDelete');
    let CopyBtn = page.PageContainer.find('.copy-btn');
    let MoveBtn = page.PageContainer.find('.move-btn')
    let DownloadBtn = page.PageContainer.find('.btnDownload');
    let RemoveSharingBtn = page.PageContainer.find(".removeSharingbtn")
    let SelectedContent = selectedContent
    if (selectedContent.Type == "folder") {
        //RemoveSharingBtn.addClass('d-none')
    }
    // let SelectedContent = $(e.target).closest('.gridfile-box,.gridfolder-box')[0].data

    if (checked) {
        if (!_mydrive.ListViewSelectedFileIds.find(o => o._id == SelectedContent._id)) _mydrive.ListViewSelectedFileIds.push(SelectedContent);
        if (_mydrive.ViewInfo.ViewType == "shareWithMe") {
            DownloadBtn.removeClass('d-none');
            RemoveSharingBtn.removeClass("d-none");
        } else {
            DeleteBtn.removeClass('d-none');
            CopyBtn.removeClass('d-none');
            MoveBtn.removeClass('d-none');
            DownloadBtn.removeClass('d-none');
        }

    } else {

        _mydrive.ListViewSelectedFileIds = _mydrive.ListViewSelectedFileIds.filter(o => o._id != SelectedContent._id);
        let allCheckedCheckboxes = page.PageContainer.find(".grid-view-content .chkSelect:checkbox:checked");
        if (allCheckedCheckboxes.length == 0) {
            // DeleteBtn.addClass('d-none')
            // CopyBtn.addClass('d-none');
            // MoveBtn.addClass('d-none');
            // DownloadBtn.addClass('d-none');
            // RemoveSharingBtn.addClass("d-none");
            page.PageContainer.find('.chkSelectAll').prop('checked', false);
        }

    }
}
_mydrive.DownloadSizeCheck = function (size) {
    return parseInt(size) < 104857600
}

_mydrive.GridView = function (page) {
    this.Container = page.PageContainer;
    this.PageNumber = _mydrive.ViewInfo.PageNumber;
    this.PageSize = _mydrive.ViewInfo.PageSize;
    this.searchString = null;
    this.TotalCount = 0;
    _mydrive.ViewInfo.Loading = true;
    page.GridView = this;

    this.ReloadData = (searchString, type, scroll = false) => {
        // to Remove tooltip on folder Click
        $('.tooltip').remove();

        if (!scroll) {
            page.PageContainer.find('.grid-view-content').html('');

        }

        (_mydrive.ViewInfo.PageNumber == 1) ? _common.ContainerLoading(true, page.PageContainer.find('.grid-table-container')) : '';
        _drive.GetUserSpace().then((data) => {
            this.TotalSpace = data.TotalSpace;
            this.UsedSpace = data.UserSpace;
            this.searchString = searchString;
            _drive.GetMyDriveData({
                'currentPage': _mydrive.ViewInfo.PageNumber,
                'pageSize': _mydrive.ViewInfo.PageSize,
                'FolderId': _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId,
                'SearchString': searchString,
                'ViewType': type || _mydrive.ViewInfo["ViewType"],
                callback: (result) => {
                    this.TotalCount = result.count;
                    this.Folders = result.data.filter(o => o.Type != 'file');
                    this.Data = result.data.filter(o => o.Type != 'folder');
                    page.PageContainer.find('.folder-head').removeClass('d-none');
                    page.PageContainer.find('.file-head').removeClass('d-none');
                    (result.count > (_mydrive.ViewInfo.PageNumber * _mydrive.ViewInfo.PageSize)) ? _mydrive.ViewInfo['LastPage'] = false : _mydrive.ViewInfo['LastPage'] = true;
                    if (page.PageContainer.find('.grid-view-content .file-loader').length != 0) {
                        page.PageContainer.find('.grid-view-content .file-loader').closest('.gridfile-box').remove();
                    }

                    if (searchString != null && page.PageContainer.find('.search-in-list .txtSearchInGrid').val() == searchString) {
                        this.DataBind();
                    } else if (searchString == null) {
                        this.DataBind();
                    }

                }

            });
        })

    }

    this.DataBind = () => {
        let Files = this.Data
        let Folders = this.Folders
        let Container = this.Container
        let GridView = Container.find(".grid-view-content");
        let SpaceOccupiedPercent = ((this.UsedSpace) / (this.TotalSpace) * 100).toFixed(2);
        let spacebar = this.Container.find('.main-drive-spacebar')
        spacebar.find('p').html(`${_mydrive.ConvertSize(this.UsedSpace)} used of ${_mydrive.ConvertSize(this.TotalSpace)}`);
        spacebar.find('.progress-bar').css("width", SpaceOccupiedPercent + '%');
        let AllContent = [...this.Folders, ...this.Data];
        this.Container.find('.main-drive .breadcrumb').html(' ');
        let noContent = (AllContent.length == 0 && _mydrive.ViewInfo.PageNumber == 1 && this.searchString == null) ? true : false;
        let NoFileContainer = this.Container.find('.welcome-drive');
        let DeleteBtn = page.PageContainer.find('.btnDelete');
        let CopyBtn = page.PageContainer.find('.copy-btn');
        let MoveBtn = page.PageContainer.find('.move-btn')
        let DownloadBtn = page.PageContainer.find('.btnDownload');
        let RemoveSharingBtn = page.PageContainer.find('.removeSharingbtn');
        let ExternalSharingBtn = page.PageContainer.find('.external-share-btn');
        let InternalSharingBtn = page.PageContainer.find('.internal-share-btn');
        let ListViewBtn = page.PageContainer.find('.show-list-view');
        let RemoveSharingbtn = page.PageContainer.find('.removeSharingbtn');

        //  page.PageContainer.find('.btnDelete').addClass('d-none');

        if (noContent) {

            NoFileContainer.removeClass('d-none').show();
            InternalSharingBtn.addClass('d-none');
            DeleteBtn.addClass('d-none');
            CopyBtn.addClass('d-none');
            MoveBtn.addClass('d-none');
            DownloadBtn.addClass('d-none');

            RemoveSharingBtn.addClass("d-none");
            ExternalSharingBtn.addClass("d-none");
            ListViewBtn.addClass('d-none');
            // NoFileContainer.find(' p strong').text("Please Upload a new File")
            NoFileContainer.find(' p br').text(" ")
            // this.Container.find('.main-drive-folder').hide();
            this.Container.find('.main-drive-list').hide();
            //to remove tooltip on folder click
            this.Container.find('.welcome-drive').removeClass('d-none').show();
            this.Container.find('.request-space').hide();
            this.Container.find('.txtSearchInGrid').hide();
            this.Container.find('.show-grid-view').hide();
            this.Container.find('.main-drive-grid').hide();
            // this.PageContainer.find('.main-drive-spacebar').hide();
            this.Container.find('.main-drive-grid').addClass('d-none');

        } else {

            (_mydrive.SelectedFolder.length > 0) ? this.Container.find('nav').show() : '';
            ListViewBtn.removeClass('d-none');
            this.Container.find('.main-drive-list').show();
            this.Container.find('.main-drive-grid').show();
            this.Container.find('.welcome-drive').addClass('d-none').hide();
            this.Container.find('.request-space').show();
            this.Container.find('.txtSearchInGrid').show();
            NoFileContainer.removeClass('d-none').hide();

            if (_mydrive.ViewInfo.CurrentView == 'GridView') {
                InternalSharingBtn.removeClass('d-none');
                if (_mydrive.ViewInfo.ViewType == "shareWithMe") { RemoveSharingbtn.removeClass('d-none'); }
                ExternalSharingBtn.removeClass("d-none");
            }

            NoFileContainer.find(' p br').text(" ");
            this.Container.find('.show-grid-view').show();
            // this.PageContainer.find('.main-drive-spacebar').show();
        }

        if (!noContent && _mydrive.ViewInfo.CurrentView == 'GridView') {
            DeleteBtn.removeClass('d-none');
            CopyBtn.removeClass('d-none');
            MoveBtn.removeClass('d-none');
            DownloadBtn.removeClass('d-none');
            ExternalSharingBtn.removeClass("d-none");
            InternalSharingBtn.removeClass("d-none");
        }
        if (_mydrive.ViewInfo.ViewType == "shareWithMe" && _mydrive.ViewInfo.CurrentView == 'GridView') {
            DeleteBtn.addClass('d-none');
            CopyBtn.addClass('d-none');
            MoveBtn.addClass('d-none');
            ExternalSharingBtn.addClass("d-none");
            InternalSharingBtn.addClass("d-none");
            if (!noContent && _mydrive.ViewInfo.ViewType == "shareWithMe") RemoveSharingBtn.removeClass("d-none")
        }
        if (_mydrive.ViewInfo.CurrentView == 'ListView' && noContent == false) {
            this.Container.find('.main-drive-list').hide()
            this.Container.find('.main-drive-grid').show();
        } else if (_mydrive.ViewInfo.CurrentView == 'GridView' && noContent == false) {
            this.Container.find('.main-drive-grid').hide();
            this.Container.find('.main-drive-list').show()
        }


        for (let m = 0; m < _mydrive.SelectedFolder.length; m++) {
            let htmlcontent = `<li class="breadcrumb-item" data-folderId=${_mydrive.SelectedFolder[m].FolderId}><a >${_mydrive.SelectedFolder[m].ViewValue}</a></li>`
            this.Container.find('.main-drive .breadcrumb').append(htmlcontent);
        }

        for (let i = 0; i < Folders.length; i++) {
            let folder = Folders[i]
            let list = $(`  <tr  class="gridfolder-box"   title=""  data-id=${folder._id} data-name=${folder.FileName} data-folder=${folder._id} date-shared=${folder.Shared ? true : false}>
                                             <td><div><label class="lblChk"><input type="checkbox" class="chkSelect selectfile" data-id=${folder._id}><span class="checkmark"></span></label></div></td>
                                                <td data-toggle="tooltip" style="max-width:500px"  class="folder-name" ><div style="max-width:500px"  class="grid-cell pr-15 align-items-center folder-content" data-type="text"><div ><i class="fas fa-folder text-secondary" style="font-size: 1.2rem;"></i></div><div class="flex-grow-1 pl-2 folder-name text-ellipse">${folder.FileName}</div></div></td>
                                                 <td class=" folder-content group-child"><div class="grid-cell"> ${ folder.Size == 0 || folder.Size == null ? 'N/A' : _mydrive.ConvertSize(folder.Size)} </div></td>
                                                 <td>
                                                     <div>${_mydrive.ConvertDate(folder.UploadDate)}</div>
                                                 </td>
                                                 <td>
                                                     <div>${folder.UserID == _initData.User._id ? 'me' : ''}</div>
                                                 </td>
                                                 <td> <div class="grid-download" > </div> </td>
                                         </tr>`);

            list[0].data = folder;

            if (_mydrive.DownloadSizeCheck(folder.Size)) {

                list.find('.grid-download').append(`<a class="download-btn" > <span data-toggle="tooltip" data-original-title="Download" class="fas fa-download text-secondary"></span> </a> `);

            } else {
                list.find('.grid-download').append(`<a class="download-disabled" > <span data-toggle="tooltip" data-original-title="Download" class="fas fa-download text-secondary "></span> </a> `);
            }
            list.find('.folder-name').attr('data-original-title', folder.FileName);
            GridView.append(list);

        }
        {/* <img src="${file.DefaultIcon}" style="width: 18px; height: auto;"/> */ }
        for (let i = 0; i < Files.length; i++) {
            let file = Files[i];
            let DefaultDoc = Files[i].DefaultDoc && Files[i].DefaultDoc == true ? 'default' : 'drive'
            let ownerName = "";
            if (file.UserID == _initData.User._id) {
                ownerName = "me"
            } else if (file.Shared && file.OwnerID_LookupData) {
                ownerName = `${file.OwnerID_LookupData.FirstName} ${file.OwnerID_LookupData.LastName}`
            }
            let list = $(`  <tr class="gridfile-box"   data-pid=${file._id}>
            <td><div><label class="lblChk"><div>  <input type="checkbox" class="chkSelect" data-id=${file._id} date-shared=${file.Shared ? true : false}><span class="checkmark selectfile" ></span></label> </div> </div></td>
               <td data-toggle="tooltip"  style="max-width:500px"  class="file-name" ><div style="max-width:500px"  class="grid-cell pr-15 align-items-center" data-type="text"><div >  <i >   </i>    </div><div class="flex-grow-1 pl-2 folder-name text-ellipse">${file.FileName}</div></div></td>
                <td class="group-child"><div class="grid-cell"> ${_mydrive.ConvertSize(file.Size)} </div></td>
                <td>
                    <div>${_mydrive.ConvertDate(file.UploadDate)}</div>
                </td>
                <td>
                <div>${ownerName}</div>
            </td>
              <td><div class="grid-download" > </div> </td>
        </tr>`);
            let fileId = "";
            (file && file.Orignal) ? fileId = file.Orignal : fileId = file._id
            // console.log(file, "===--=")
            list.find('.grid-download').append(`<a target="_blank"  href= '/api/drive/download/${fileId}/${file.FileName}/${DefaultDoc}'> <span data-toggle="tooltip" data-original-title="Download" class="fas fa-download text-secondary" ></span> </a>`);
            list[0].data = file
            list.find('.file-name').attr('data-original-title', file.FileName);
            list.find('i').addClass(file.DefaultIcon)
            GridView.append(list);
        }
        _mydrive.ViewInfo.Loading = false;
        (_mydrive.ViewInfo.PageNumber == 1) ? _common.ContainerLoading(false, page.PageContainer.find('.grid-table-container')) : '';

    }

    this.Init = () => {
        this.ReloadData();
    }


    this.Init();


}

_mydrive.CanUpload = function (page, ToUploadSize) {
    let Size = page.ListView.UsedSpace;
    for (let i = 0; i < ToUploadSize.length; i++) {
        Size = Size + ToUploadSize[i]
    }
    return page.ListView.TotalSpace > Size

}
_mydrive.ListView = function (page) {

    let self = this;
    _mydrive.ViewInfo.Loading = true;
    this.PageContainer = page.PageContainer;
    this.PageNumber = _mydrive.ViewInfo.PageNumber;
    this.PageSize = _mydrive.ViewInfo.PageSize;
    this.TotalCount = 0;
    this.Folders = [];
    this.Data = [];
    this.UsedSpace = 0;
    this.TotalSpace = 0;
    this.FolderId = 'root'
    page.ListView = this;
    this.searchString = null
    this.ReloadData = function (searchString, folderid = 'root', type, scroll = true) {


        if (!scroll) {
            page.PageContainer.find('.main-drive-grid .folder-section').html('');
            page.PageContainer.find('.main-drive-grid .drivecontent').html('');
        }


        (_mydrive.ViewInfo.PageNumber == 1 && _mydrive.ViewInfo.Loading) ? _common.ContainerLoading(true, page.PageContainer.find('.main-drive-grid')) : '';
        _drive.GetUserSpace().then((data) => {
            self.TotalSpace = data.TotalSpace;
            self.UsedSpace = data.UserSpace;
            self.searchString = searchString
            self.PageNumber = _mydrive.ViewInfo.PageNumber;
            _drive.GetMyDriveData({
                'currentPage': _mydrive.ViewInfo.PageNumber,
                'pageSize': _mydrive.ViewInfo.PageSize,
                'FolderId': folderid,
                'SearchString': searchString,
                'type': type,
                'ViewType': type || _mydrive.ViewInfo["ViewType"],
                callback: function (result) {
                    self.Data = result.data.filter(o => o.Type == 'file');
                    self.Folders = result.data.filter(o => o.Type == 'folder');
                    self.TotalCount = result.count;
                    self.FolderId = folderid;
                    (result.count > (_mydrive.ViewInfo.PageNumber * _mydrive.ViewInfo.PageSize)) ? _mydrive.ViewInfo['LastPage'] = false : _mydrive.ViewInfo['LastPage'] = true;
                    if (searchString != null && page.PageContainer.find('.search-in-list .txtSearchInGrid').val() == searchString) {
                        self.DataBind();
                    } else if (searchString == null) {
                        self.DataBind();
                    }

                }
            });
        });

    }

    this.DataBind = function () {
        page.PageContainer.find('.folder-head').removeClass('d-none');
        page.PageContainer.find('.file-head').removeClass('d-none');
        page.PageContainer.find('.drivecontent .file-loader').remove();
        let SpaceOccupiedPercent = ((this.UsedSpace) / (this.TotalSpace) * 100).toFixed(2);
        let spacebar = this.PageContainer.find('.main-drive-spacebar');
        spacebar.find('p').html(`${_mydrive.ConvertSize(this.UsedSpace)} used of ${_mydrive.ConvertSize(this.TotalSpace)}`);
        spacebar.find('.progress-bar').css("width", SpaceOccupiedPercent + '%');
        this.PageContainer.find('.main-drive .breadcrumb').html('');
        this.PageContainer.find('.total-count')[0].innerHTML = this.TotalCount;
        this.PageContainer.find('.search-in-list .txtSearchInGrid').html('');
        let ListViewBtn = this.PageContainer.find('.show-list-view');
        let RemoveSharingBtn = page.PageContainer.find('.removeSharingbtn');
        let ExternalSharingBtn = page.PageContainer.find('.external-share-btn');
        let InternalSharingBtn = page.PageContainer.find('.internal-share-btn');

        for (let m = 0; m < _mydrive.SelectedFolder.length; m++) {
            let htmlcontent = `<li class="breadcrumb-item" data-folderId=${_mydrive.SelectedFolder[m].FolderId}><a >${_mydrive.SelectedFolder[m].ViewValue}</a></li>`
            this.PageContainer.find('.main-drive .breadcrumb').append(htmlcontent);
        }


        let AllContent = [...this.Folders, ...this.Data];
        // if their is no content uploaded
        let noContent = (AllContent.length == 0 && _mydrive.ViewInfo.PageNumber == 1 && this.searchString == null) ? true : false;
        let NoFileContainer = this.PageContainer.find('.welcome-drive');

        if (noContent) {
            NoFileContainer.removeClass('d-none').show();
            ListViewBtn.addClass('d-none');
            // NoFileContainer.find(' p strong').text("Please Upload a new File")
            NoFileContainer.find(' p br').text(" ")
            this.PageContainer.find('.main-drive-folder').hide();
            this.PageContainer.find('.main-drive-file').hide();
            this.PageContainer.find('.request-space').hide();
            this.PageContainer.find('.txtSearchInGrid').hide();
            this.PageContainer.find('.show-grid-view').hide();
            // this.PageContainer.find('.main-drive-spacebar').hide();
            this.PageContainer.find('.main-drive-list').hide()
            this.PageContainer.find('.main-drive-grid').addClass('d-none');
            this.PageContainer.find('.welcome-drive').removeClass('d-none').show();
        } else {

            (_mydrive.SelectedFolder.length > 0) ? this.PageContainer.find('nav').show() : '';
            this.PageContainer.find('.main-drive-grid').removeClass('d-none');
            ListViewBtn.removeClass('d-none');
            this.PageContainer.find('.main-drive-folder').css('display', 'block');
            this.PageContainer.find('.main-drive-file').css('display', 'block');
            this.PageContainer.find('.welcome-drive').addClass('d-none').hide();
            this.PageContainer.find('.request-space').show();
            this.PageContainer.find('.txtSearchInGrid').show();
            this.PageContainer.find('.main-drive-list').show();
            // NoFileContainer.removeClass('d-none').hide();
            // NoFileContainer.find(' p strong').text("")
            NoFileContainer.find(' p br').text(" ");
            this.PageContainer.find('.show-grid-view').show();
            // this.PageContainer.find('.main-drive-spacebar').show();
        }
        if (_mydrive.ViewInfo.CurrentView == 'ListView') {

            this.PageContainer.find('.main-drive-grid').show();
            RemoveSharingBtn.addClass('d-none');
            ExternalSharingBtn.addClass('d-none');
            InternalSharingBtn.addClass('d-none');


        } else {
            this.PageContainer.find('.main-drive-grid').hide();
            // this.PageContainer.find('.main-drive-list').show()
        }
        (this.Folders.length == 0 && this.PageNumber == 1) ? this.PageContainer.find('.main-drive-folder').addClass('d-none') : this.PageContainer.find('.main-drive-folder').removeClass('d-none')

        for (let j = 0; j < this.Folders.length; j++) {
            // let FolderId = this.Folders[j]._id
            let folderData = this.Folders[j]
            // data-original-title=${ JSON.stringify(this.Folders[j].FileName) }
            let htmlContent = $(`<div class="col folder-box" data-toggle="tooltip" >
                                <div class="card" >
                                    <div class="card-body d-flex align-items-center" data-Folder=${folderData._id} data-Shared=${folderData.Shared ? true : false} >
                              
                                     
                                        <div class="text-secondary"><i class="fas fa-folder fa-2x"></i></div>  
                                          <div class="card-title pl-2 mb-0">${this.Folders[j].FileName}</div>
                                    </div>
                                </div>
                            </div>`)

            htmlContent[0].data = this.Folders[j]
            htmlContent.attr('data-original-title', this.Folders[j].FileName);
            // htmlContent.tooltip('disable');
            this.PageContainer.find('.main-drive-grid .folder-section').append(htmlContent);


        }


        (this.Data.length == 0 && this.PageNumber == 1) ? this.PageContainer.find('.main-drive-file').addClass('d-none') : this.PageContainer.find('.main-drive-file').removeClass('d-none');
        for (let i = 0; i < this.Data.length; i++) {
            let FileId = this.Data[i].Orignal ? this.Data[i].Orignal : this.Data[i]._id;
            let DefaultDoc = this.Data[i].DefaultDoc && this.Data[i].DefaultDoc == true ? 'default' : 'drive'
            //     style="background:url(${this.Data[i].Thumbnail ? this.Data[i].Thumbnail : this.Data[i].DefaultIcon}) no-repeat center center;"
            let htmlContent = $(` <div class="col file-box ${this.Data[i]._id}" data-toggle="tooltip">
                        <div class="card h-100">
                          <div class="card-body p-0">
    
                              <div class="file-bg rounded-top ${ this.Data[i].Thumbnail ? '' : 'default-icon'} ${this.Data[i].Thumbnail ? '' : this.Data[i].DefaultIcon}"  ></div>
                              <div class="card-title mb-0 row m-0" data-pid=${this.Data[i]._id}><span class="col pl-0">${this.Data[i].FileName}</span> <a href= '/api/drive/download/${FileId}/${this.Data[i].FileName}/${DefaultDoc}'class="btn btn-sm btn-primary col-auto mt-n1"> <i class="fas fa-download"></i></a></div>
                         </div>
                      </div>`);
            htmlContent[0].data = this.Data[i];
            htmlContent.attr('data-original-title', this.Data[i].FileName);
            if (this.Data[i].Thumbnail) htmlContent.find('.file-bg').css('background-image', 'url(' + this.Data[i].Thumbnail + ')');
            this.PageContainer.find('.drivecontent').append(htmlContent);

        }
        _mydrive.ViewInfo.Loading = false;
        (_mydrive.ViewInfo.PageNumber == 1) ? _common.ContainerLoading(false, page.PageContainer.find('.main-drive-grid')) : '';
    }



    this.ReloadData();



}
_mydrive.ReloadPage = function () {
    // let UploadedFiles = socketEvents.files.filter(o => o.Status == 'done');
    // if (UploadedFiles.length == socketEvents.files.length) {
    let container = {}
    _mydrive.ViewInfo.LastPage = false;
    container.PageContainer = _mydrive.Page;
    _mydrive.ViewInfo.PageNumber = 1;
    _mydrive.ViewInfo.Loading = true;
    if (_mydrive.ViewInfo['CurrentView'] == 'ListView') {

        _mydrive.Page.ListView.ReloadData(null, _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId, _mydrive.ViewInfo["ViewType"], false);
    } else {
        _mydrive.Page.GridView.ReloadData(null, _mydrive.ViewInfo["ViewType"], false);
    }
    // }
}

_mydrive.MoveModalView = function (container, page) {

    let self = this;
    this.folderID = _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId
    this.PageContainer = container;
    this.PageNumber = 1;
    this.PageSize = 20;
    this.TotalCount = 0;
    this.Folders = [];
    page.MoveModalView = this;
    let MoveBreadCrumb = this.PageContainer.find('.move-breadcrumb');
    let MoveFolderSection = this.PageContainer.find('.move-folder-list');
    _mydrive.MoveFileBreadCrumb = [..._mydrive.SelectedFolder];
    this.ReloadData = (scroll, folderID = _mydrive.SelectedFolder[_mydrive.SelectedFolder.length - 1].FolderId) => {
        $('.tooltip').remove();
        if (!scroll) {
            MoveBreadCrumb.html('');
            MoveFolderSection.html('');
        }
        (_mydrive.ViewInfo.PageNumber == 1) ? _common.ContainerLoading(true, page.PageContainer.find('.move-folder-list')) : '';
        let Type = 'myUploads';
        let folderId = folderID;
        this.folderID = folderID;
        let PageNumber = _mydrive.MoveViewInfo.PageNumber;
        let PageSize = _mydrive.MoveViewInfo.PageSize;
        let FolderContent = [];

        for (let i = 0; i < _mydrive.ListViewContextSelection.length; i++) {
            if (_mydrive.ListViewContextSelection[i].Orignal) {
                _mydrive.ListViewContextSelection[i].ID = _controls.ToEJSON('objectid', _mydrive.ListViewContextSelection[i].Orignal);
            }
        }
        FolderContent = [..._mydrive.ListViewContextSelection];
        if (_mydrive.ListViewContextSelection[0].FileDetails.FolderId == folderId) {
            page.PageContainer.find('.list-move-btn').attr("disabled", true);
        } else {
            page.PageContainer.find('.list-move-btn').attr("disabled", false);
        }

        _drive.GetAllFolders(folderId, Type, PageNumber, PageSize, FolderContent).then((res) => {
            if (res.statusCode === 200) {
                this.Folders = res.data.Folders;
                this.TotalCount = res.data.TotalCount;
                (this.TotalCount > (_mydrive.MoveViewInfo.PageNumber * _mydrive.MoveViewInfo.PageSize)) ? _mydrive.MoveViewInfo['LastPage'] = false : _mydrive.MoveViewInfo['LastPage'] = true;
                this.PageContainer.find('.Loader').remove();
                this.DataBind();
                (_mydrive.ViewInfo.PageNumber == 1) ? _common.ContainerLoading(false, page.PageContainer.find('.move-folder-list')) : '';
            }
        })

    }
    this.DataBind = () => {
        MoveBreadCrumb.html('');

        let BreadCrumbCurrentState = (_mydrive.MoveFileBreadCrumb.length == 1) ? 0 : _mydrive.MoveFileBreadCrumb.length - 1
        let breadcrumb = `<ol class="breadcrumb pl-3 pr-3 pt-2 pb-2 m-0 rounded-0">

                      <li class="breadcrumb-item"  ><a> <i  data-toggle="tooltip" data-original-title="Back" style="margin-right:4px" data-folderid=${_mydrive.MoveFileBreadCrumb[BreadCrumbCurrentState].FolderId}  class="${_mydrive.MoveFileBreadCrumb[BreadCrumbCurrentState].FolderId == 'root' ? 'd-none' : ''} fas fa-arrow-left move-breadcrumb-backicon"> </i>  ${" " + _mydrive.MoveFileBreadCrumb[BreadCrumbCurrentState].ViewValue}</a></li>
                          </ol>`

        MoveBreadCrumb.append(breadcrumb)

        if (this.Folders.length == 0 && this.folderID == 'root') {
            let folder = ` <a  class="list-group-item d-flex align-items-center folder-item text-secondary list-group-item-action" data-folderid=${'nofolders'} >
             Please add  a folder and then try again 
         </a>`
            MoveFolderSection.append(folder);
        } else {
            this.PageContainer.find('.modal-footer').removeClass('d-none')
        }

        // to append folders list in the view    
        for (let j = 0; j < this.Folders.length; j++) {
            let folder = $(` <a  data-toggle="tooltip" class="list-group-item d-flex align-items-center folder-item text-secondary list-group-item-action" data-folderid=${this.Folders[j]._id} >
            <i class="fas fa-folder fa-2x mr-2"></i> <span class="folder-item-text">${ this.Folders[j].FileName}</span> <i class="fa fa-chevron-right ml-auto forward"  ></i>
        </a>`);

            folder.attr('data-original-title', ' Go to ' + this.Folders[j].FileName);

            MoveFolderSection.append(folder)
        }
        _mydrive.MoveViewInfo.Loading = false;
    }

    this.ReloadData(false)
}

_mydrive.AppendLoader = function (contaner) {

    if (_mydrive.ViewInfo['CurrentView'] == 'GridView' && _mydrive.MoveViewInfo['Loading'] == false) {
        let GridView = contaner.find(".grid-view-content");
        let Loader = `<tr class="gridfile-box" >
    <td colspan="6"><div style="height: 40px;margin-bottom: 0;" class="position-relative col file-loader"><div class="loading"><img src="/images/loader-sm.gif"></div></div>
    </td>
    </tr>`
        GridView.append(Loader);
    } else if (_mydrive.MoveViewInfo['Loading'] == true) {
        contaner.find('.move-folder-list').append(`<a  class="list-group-item d-flex align-items-center folder-item text-secondary list-group-item-action Loader"  >
            <i class="fas fa-folder fa-2x mr-2"></i> <span class="folder-item-text"><div class="loading"><img src="/images/loader-sm.gif"></div></span> 
        </a>`)
    } else if (_mydrive.ViewInfo['CurrentView'] == 'ListView' && _mydrive.MoveViewInfo['Loading'] == false) {
        let LoaderContent = `<div class="position-relative col file-loader"><div class="loading"><img src="/images/loader-sm.gif"></div></div>`
        contaner.find('.drivecontent').append(LoaderContent);
    }
}


//Params
//isBackDate -> CB to execute when back date 
//validDateAdded -> CB to execute when valid date added

_mydrive.BuildTimeField = function (container, date, name, params = {}) {
    if (!date) {
        date = moment.tz(_initData.CurrentUtcTime, _initData.User.MySettings.Timezone).add(24, 'h').format()
    }
    let data = {};
    let fDate = name.split(" ").join("-");
    data[fDate] = date;
    let field = {
        "Name": fDate,
        "Interval": 30,
        "TimezoneField": "Timezone",
        "ColumnWidth": 12,
        "StaticField": true,
        "Schema": {
            "Name": fDate,
            "DisplayName": name,
            "UIDataType": "datetime",
            "IsRequired": true,
            "TimezoneField": "Timezone"
        }
    }
    _page.BuildField({ field, container, data });
    container.find("label").remove();

    //Triggers
    container.find(".msp-ctrl-datetime").change(function () {
        let selectedTime = _controls.GetCtrlValue({ field, container });
        if (params.validDateAdded && moment.utc(_initData.CurrentUtcTime).isBefore(selectedTime)) {
            params.validDateAdded();
        }
    })

    if (moment(date).isBefore(_initData.CurrentUtcTime)) {
        if (params.isBackDate) {
            params.isBackDate(true)
        }
    }

    //Get value
    return function () {
        let selectedTime = _controls.GetCtrlValue({ field, container });
        if (moment.utc(_initData.CurrentUtcTime).isAfter(selectedTime)) {
            swal("Expire Time should be greater than current time", { icon: "error" })
            return false
        } else {
            return moment.utc(selectedTime).format();
        }
    }
}

_mydrive.GetDriveTemplates = function (name, values) {
    switch (name) {
        case 'Internal_Share_Modal':
            return `<div class="drive-share row">
            <!--User multillokup-->
             <div class="form-group multilookup-main w-100">
 
             </div>
            
             <div class="form-group message col-sm-12 share-message-container">
                 <label class="small">Type Your Message(Optional)</label>
                 <textarea type="text" class="form-control form-control-sm message-field" rows="5"></textarea>
             </div>
 
             <!--shared users-->
             <div class="col-sm-12 shared-users-container">
             <h6 class="mt-2" >Shared With <button class="btn btn-sm btn-outline-danger float-right mb-2 lh-1 remove-all-users" >Remove all</button>
             </h6>
             <ul class='from-group pl-0 shared-users'>  
             </ul>
             </div>
 </div>`

        case 'Internal_Shared_User_List':
            return ` <li class="list-group-item d-flex pt-1 pb-1 pl-2 pr-2 d-flex w-100 justify-content-between shared-user-li" data-id="${values._id}">
            <div>
               <h6 class="mb-0 font-weight-bold text-secondary ">${values.FirstName} ${values.LastName}</h6>
               <p class="small mb-0 mt-0 text-secondary d-inline-block">${values.Email}</p><p class="small mb-0 mt-0 text-secondary d-inline-block pl-2 pr-2">${values.Department ? "| " + values.Department : ""}</p>
           </div>
           <div>
           <span class="btn btn-sm text-danger remove-user-share mt-1" data-toggle="tooltip" data-placement="top" title="Remove sharing">
           <i class="fas fa-times "></i>
           </span>
           </div>
           </li>`

        case 'External_Shared_User_List':
            let nameExist = values.Name ? true : false;
            return `
            <li class="list-group-item d-flex pt-1 pb-1 pl-2 pr-2 d-flex w-100 justify-content-between shared-user-li" data-id="${values._id}">
            <div ${!nameExist ? "style='padding:7px 0px'" : ""}>
                ${nameExist ? `<h6 class="mb-0 font-weight-bold text-secondary ">${values.Name}</h6>` : ""}
               <p class="small mb-0 mt-0 text-secondary d-inline-block" >${values.Email}</p>
           </div>
           </li>
            `

        case 'External_Share_Modal':
            return `<div class="row drive-share">


            <div class="form-group col-sm-12 vld-parent ">
                <label class="small">Shareable Link</label>
                <div class='input-group'>
                    <input type="text" title="Shareable Link"
                        class="shareable-link msp-ctrl form-control form-control-sm msp-ctrl-text" readonly='true'>
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-primary copy-link" type="button" disabled="true"
                            title='Copy Shareable Link' data-toggle="tooltip" data-placement="top">
                            <span class=" msp_ctrl-text spinner-border spinner-border-sm" role="status" aria-hidden="true"
                                style="width: 0.8rem;height: 0.8rem;border-width:0.1em"></span> Loading..
                        </button>
                    </div>
                </div>
                <!--Share Type-->
                <div class="input-group mt-2">
                    <label class="small mr-3">Share Type</label>
                    <div class="col-12"></div>
                    <div class="custom-control custom-radio d-inline-block mr-2">
                        <input class="custom-control-input" type="radio" name='shareType'  id="public-share" value="Public">
                        <label class="custom-control-label" for="public-share">Public</label>
                    </div>
                    <div class="custom-control custom-radio d-inline-block mr-2">
                        <input class="custom-control-input" type="radio" name='shareType' id="restricted-share" value="Restricted">
                        <label class="custom-control-label" for="restricted-share">Restricted</label>
                    </div>
                </div>
            </div>
            <!--User multilookup-->
            <div class="form-group multilookup-main col-sm-12">
        
            </div>
        
            <div class="form-group share-message-container col-sm-12">
                <label class="small">Type Your Message(Optional)</label>
                <textarea type="text" class="form-control form-control-sm message-field" rows="5"></textarea>
            </div>
        
            <!--shared users-->
            <div class='from-group col-sm-12 shared-users'>
                <label class="small">Shared Users</label>
        
        
            </div>
            <div class="mt-4 col-sm-12 form-group">
                <h6>Advanced Link Setting</h6>
                <div class="custom-control custom-checkbox d-inline-block mr-2">
                  <input class="custom-control-input " type="checkbox"  id="exp-time-checbox">
                  <label class="custom-control-label" for="exp-time-checbox">Add an expiration date</label>
                  <!--Date pickup-->
                <div class="exp-time-pickup d-inline-block">
        
                </div>
                </div>
                <div class="show-error text-danger"></div>
            </div>
        </div>`
    }
}
