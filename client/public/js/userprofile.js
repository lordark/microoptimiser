const _userprofile = {};
_userprofile.PermissionModules = {};
_userprofile.Init = function (page) {
    $(document).find('#main-header #page-nav .nav-item.active .title').text(page.param.record.Name);
    _userprofile.GetModuleList().then(function (moduleList) {
        _userprofile.PermissionModules = moduleList;
        let fieldCheckBoxCtrl = { 'Name': 'ModulePermission', 'DisplayName': 'Module Permission' };
        fieldCheckBoxCtrl.Schema = { UIDataType: "checkbox" };
        let CheckBoxCtrl = _controls.BuildCtrl({ field: fieldCheckBoxCtrl });

        for (let i = 0; i < moduleList.length; i++) {
            let moduleDataRow = $("<tr data-id='" + moduleList[i]._id + "'></tr>");
            moduleDataRow.append("<td><div>" + moduleList[i].DisplayName + "</div></td>");
            moduleDataRow.append("<td><div>" + CheckBoxCtrl[0].outerHTML + "</div></td>");

            let permissionObj = "<td><div>";
            if (moduleList[i].Operations != undefined) {
                permissionObj += "<select multiple='multiple' title='Users List' class='msp-ctrl form-control form-control-sm text-left ddlModuleOperations'>";
                for (let j = 0; j < moduleList[i].Operations.length; j++) {
                    let oppObj = moduleList[i].Operations[j];
                    permissionObj += '<option value="' + oppObj + '">' + oppObj + '</option>';
                }
                permissionObj += '</select>';
            }
            permissionObj += "</div></td>";
            moduleDataRow.append(permissionObj);
            moduleDataRow.appendTo(page.PageContainer.find('.tbl-module-list'));
            moduleDataRow[0].ModuleOptions = moduleList[i];
            if (moduleList[i].IsAdminModule == true) {
                moduleDataRow.attr('data-IsAdminModule', true);
            }
            if (moduleList[i].IsPaidPlugin == true) {
                moduleDataRow.attr('data-IsPaidPlugin', true);
            }
            page.PageContainer.find('.tbl-module-list').append(moduleDataRow);
        }
        page.PageContainer.find('.tbl-module-list').find('.ddlModuleOperations').multiselect({ nonSelectedText: '' });
        _userprofile.GetAuthDetailOfProfile(page, page.param._id);
    }, function (moduleList) {
    });
    page.PageContainer.find('.btnUpdateProfile').click(function () {
        _userprofile.UpdateProfileDetails(page);
    });
    page.PageContainer.find('.btnEditProfileBasicDetail').click(function () {
        let modalProfileDetail = page.PageContainer.find('.modalEditProfile');
        modalProfileDetail.modal('show');
        modalProfileDetail.find('.profile-name').val(page.PageContainer.find('.page-header .profile-name').text());
        modalProfileDetail.find('.profile-description').val(page.PageContainer.find('.page-header .profile-description').text());
        modalProfileDetail.find('.btnUpdateProfileBasicDetails').unbind('click');
        modalProfileDetail.find('.btnUpdateProfileBasicDetails').click(function () {
            if (_common.Validate(modalProfileDetail)) {
                let sdata = { "Operation": "update", "UserProfileID": page.param._id, "UserProfileName": modalProfileDetail.find('.profile-name').val() }
                _common.FetchApiByParam({
                    url: "/api/userprofile/checkduplicateprofilename"
                    , type: "post", contentType: 'application/json', dataType: 'JSON'
                    , data: sdata
                }, function (result) {
                    if (result.message == "success") {
                        if (result.data && result.data.length == 0) {
                            let Data = {};
                            Data.ProfileID = page.param._id;
                            Data.ProfileName = modalProfileDetail.find('.profile-name').val();
                            Data.ProfileDescription = modalProfileDetail.find('.profile-description').val();
                            _common.FetchApiByParam({
                                url: "/api/userprofile/updateprofileheaders"
                                , type: "post", contentType: 'application/json', dataType: 'JSON'
                                , data: Data
                            }, function (result) {
                                if (result && result.message == "success") {
                                    swal("Profile updated successfully", { "icon": "success" });
                                    _common.ResetDBListSchemaData("UserProfile");
                                    page.PageContainer.find('.page-header .profile-name').text(Data.ProfileName);
                                    if (Data.ProfileDescription.length > 100) {
                                        page.PageContainer.find('.page-header .btnProfileDescReadMore').removeClass('d-none');
                                        page.PageContainer.find('.page-header .profile-description').html(Data.ProfileDescription);
                                        page.PageContainer.find('.page-header .profile-description-summary').html(Data.ProfileDescription.substr(0, 100) + "...");
                                    } else {
                                        page.PageContainer.find('.page-header .btnProfileDescReadMore').addClass('d-none');
                                        page.PageContainer.find('.page-header .profile-description-summary,.page-header .profile-description').html(Data.ProfileDescription);
                                    }
                                    modalProfileDetail.modal('hide');

                                } else {
                                    swal(result.message, { "icon": "error" });
                                }
                            });
                        } else {
                            _common.ValidateAddMsg(modalProfileDetail.find('.profile-name'), "Name already exists");
                        }
                    }
                });
            }
        });
    });
    page.PageContainer.find('.btnCloneProfile').click(function () {
        let modalCloneProfile = page.PageContainer.find('.modalCloneProfile');
        modalCloneProfile.find('.vld-message').remove();
        modalCloneProfile.find('.has-error').removeClass('has-error');
        modalCloneProfile.find('.lblExistingProfileName').attr('data-id', page.param._id);
        modalCloneProfile.find('.lblExistingProfileName').text(page.PageContainer.find('.page-header .profile-name').text());
        modalCloneProfile.find('.txtProfileName').val("Clone_" + page.PageContainer.find('.page-header .profile-name').text());
        modalCloneProfile.find('.txtProfileDescription').val(page.PageContainer.find('.page-header .profile-description').text());
        modalCloneProfile.modal('show');
        modalCloneProfile.find('.btnSaveNewProfile').unbind('click');
        modalCloneProfile.find('.btnSaveNewProfile').click(function () {
            _common.Loading(true)
            if (_common.Validate(modalCloneProfile)) {
                let sdataDupCheck = { "Operation": "clone", "UserProfileName": modalCloneProfile.find('.txtProfileName').val() }
                _common.FetchApiByParam({
                    url: "/api/userprofile/checkduplicateprofilename"
                    , type: "post", contentType: 'application/json', dataType: 'JSON'
                    , data: sdataDupCheck
                }, function (result) {
                    if (result.message == "success") {
                        if (result.data && result.data.length == 0) {
                            let sdata = {};
                            sdata.ProfileID = modalCloneProfile.find('.lblExistingProfileName').attr('data-id');
                            sdata.ProfileName = modalCloneProfile.find('.txtProfileName').val();
                            sdata.ProfileDescription = modalCloneProfile.find('.txtProfileDescription').val();
                            _common.FetchApiByParam({
                                url: "/api/userprofile/cloneuserprofile"
                                , type: "post", contentType: 'application/json', dataType: 'JSON'
                                , data: sdata
                            }, function (result) {
                                if (result) {
                                    if (result.message == "success") {
                                        _common.ResetDBListSchemaData("UserProfile")
                                        _common.Loading(false);
                                        modalCloneProfile.modal('hide');
                                        swal("Profile updated successfully", { "icon": "success" });
                                    } else {
                                        _common.Loading(false)
                                        swal(result.message, { "icon": "error" });
                                    }
                                } else {
                                    swal(_constantClient.ERROR_MSG, { "icon": "error" });
                                }
                            });
                        } else {
                            _common.ValidateAddMsg(modalCloneProfile.find('.txtProfileName'), "Name already exists");
                        }
                    }
                    else {
                        _common.Loading(false)
                        swal(_constantClient.ERROR_MSG, { "icon": "error" });
                    }
                });
            } else {
                _common.Loading(false)
            }
        });
        // 
        modalCloneProfile.find('.txtProfileName').focus();
    });
    page.PageContainer.find('.tbl-module-list').on('click', '.lblChk [data-field="ModulePermission"]', function () {
        let tr = $(this).closest('tr');
        if ($(this).is(':checked')) {
            tr.find('.ddlModuleOperations').multiselect('selectAll', false, true).multiselect('refresh');
        } else {
            tr.find('.ddlModuleOperations').multiselect('deselectAll', false, true).multiselect('refresh');
        }
    });
    page.PageContainer.find('.btnViewProfileUsers').click(function () {
        if ($(this).attr('data-disable') == undefined) {
            let btn = $(this);
            btn.attr('data-disable', true);
            setTimeout(function () { btn.removeAttr('data-disable'); }, 400);
            _userprofile.ShowProfileUsersList(page.param._id, page.param.record.Name);
        }
    });
    page.PageContainer.on('click', '.btnCancelProfileEdit', function () {
        $(document).find('#main-header #page-nav .nav-item.active .tab-close').trigger('click');
    });
    page.PageContainer.on('click', '.btnProfileDescReadMore', function () {
        $(this).closest('div').addClass('d-none');
        page.PageContainer.find('.page-header .profile-description').parent('div').removeClass('d-none');
    });
    page.PageContainer.on('click', '.btnProfileDescReadLess', function () {
        $(this).closest('div').addClass('d-none');
        page.PageContainer.find('.page-header .profile-description-summary').parent('div').removeClass('d-none');
    });
}
_userprofile.UpdateProfileDetails = function (page) {
    let Data = {};
    Data.ProfileID = page.param._id;
    Data.Permissions = [];
    Data.UserMenu = [];
    page.PageContainer.find('.tbl-module-list tbody tr').each(function () {
        if ($(this).find('[data-field="ModulePermission"]').is(':checked')) {
            let objModule = {}, modueOptions = $(this)[0].ModuleOptions;
            objModule["ModuleID"] = $(this).attr("data-id");
            objModule["Operations"] = ($(this).find('select.ddlModuleOperations').val() || "");
            Data.Permissions.push(objModule);
            if (modueOptions.IsMenu == true) {
                let objUserMenu = {};
                objUserMenu["PageName"] = modueOptions.MainPageName;
                objUserMenu["DisplayName"] = modueOptions.DisplayName;
                objUserMenu["Sequence"] = Number(modueOptions.Sequence);
                objUserMenu["Icon"] = modueOptions.Icon;
                objUserMenu["ModuleID"] = $(this).attr("data-id");
                Data.UserMenu.push(objUserMenu);
            }
        }
    });
    _common.FetchApiByParam({
        url: "/api/userprofile/updateprofilepermissions"
        , type: "post", contentType: 'application/json', dataType: 'JSON'
        , data: Data
    }, function (result) {
        if (result && result.message == "success") {
            swal("Profile updated successfully", { "icon": "success" });
        }
    });
}

_userprofile.GetModuleList = function () {
    return new Promise(function (resolve, reject) {
        _common.FetchApiByParam({
            url: "/api/userprofile/getmodulelist", type: "post", contentType: 'application/json', dataType: 'JSON'
        }, function (result) {
            if (result && result.message == "success") {
                resolve(result.data);
            } else {
                reject("No Module list found");
            }
        });
    });
}

_userprofile.HasModuleAccess = function (param) {
    let pageData = param.pageData;
    let hasAccess = false;

    let auth = _initData.ProfileAuth.Permissions;
    for (let i = 0; i < auth.length; i++) {
        let moduleData = auth[i].ModuleData;
        if (moduleData) {
            if ((pageData.ObjectName && moduleData.Objects && moduleData.Objects.indexOf(pageData.ObjectName) >= 0)
                || (moduleData.Pages && moduleData.Pages.indexOf(pageData.PageName) >= 0)
            ) {
                hasAccess = true;
                break;
            }
        }
    }
    return hasAccess;
}

_userprofile.Auth = function (param) {
    let pageData = param.pageData;
    let container = param.container;
    let hasAccess = false;
    if (pageData.IsPermissionRequired == false) {
        if (container != undefined) {
            container.find('[data-restriction]').each(function () {
                $(this).removeClass('d-none');
            });
        }
        return true;
    }
    let auth = _initData.ProfileAuth.Permissions;
    for (let i = 0; i < auth.length; i++) {
        let moduleData = auth[i].ModuleData;
        if (moduleData) {
            if ((pageData.ObjectName && moduleData.Objects && moduleData.Objects.indexOf(pageData.ObjectName) >= 0)
                || (moduleData.Pages && moduleData.Pages.indexOf(pageData.PageName) >= 0)
            ) {
                hasAccess = true;
                let permissionsGiven = auth[i].Operations;
                if (permissionsGiven && container != undefined) {
                    container.find('[data-restriction]').each(function () {
                        if (permissionsGiven.indexOf($(this).attr('data-restriction')) == -1) {
                            $(this).remove();
                        }
                        else {
                            $(this).removeClass('d-none');
                        }
                    });
                }
                break;
            }
        }
    }
    if (hasAccess == false) {
        _userprofile.AuthError(pageData, container);
    }
    // if (_initData.ProfileAuth.IsAdmin === true) {
    //     hasAccess = true;
    // }
    return hasAccess;
}
_userprofile.AuthError = function (pageData, container) {
    //if (_initData.ProfileAuth.IsAdmin !== true) {
    if (container != undefined) {
        if (container.hasClass('modal')) {
            container.find('.modal-body').html(`
            <div class="mb-2 mt-2 table-sec msp-data-grid UserEmailSync"><h2 class="info text-primary font-weight-bold mb-1 h3">Accesss Denied</h2>
            <h4 class="info text-secondary h5">You are not authorised to access this page.</h4></div>
        `);
            container.find('.modal-footer').remove();
        }
        else
            container.html(`<div class="mb-2 mt-2 table-sec msp-data-grid UserEmailSync"><h2 class="info text-primary font-weight-bold mb-1 h3">Accesss Denied</h2>
            <h4 class="info text-secondary h5">You are not authorised to access this page.</h4></div>`);
    }
    else
        swal('You are not authorised to access this page.', { icon: 'info' });
    //}
}

_userprofile.GetAuthDetailOfProfile = function (page, profileID) {
    let sdata = { "ProfileID": profileID }
    _common.FetchApiByParam({
        url: "/api/userprofile/getauthdetailsofprofile", type: "post", contentType: 'application/json', dataType: 'JSON'
        , data: sdata
    }, function (result) {
        if (result && result.message == "success" && result.data) {
            page.PageContainer.find('.profile-name').html(result.data.Name);
            if (result.data.Description) {
                if (result.data.Description.length > 100) {
                    page.PageContainer.find('.page-header .btnProfileDescReadMore').removeClass('d-none');
                    page.PageContainer.find('.page-header .profile-description').html(result.data.Description);
                    page.PageContainer.find('.page-header .profile-description-summary').html(result.data.Description.substr(0, 100) + "...");
                } else {
                    page.PageContainer.find('.page-header .btnProfileDescReadMore').addClass('d-none');
                    page.PageContainer.find('.page-header .profile-description-summary,.page-header .profile-description').html(result.data.Description);
                }
            } else {
                page.PageContainer.find('.page-header .btnProfileDescReadMore').addClass('d-none');
                page.PageContainer.find('.page-header .profile-description-summary,.page-header .profile-description').html("");
            }
            if (result.data.IsSystemDefine) {
                swal("This is system generated profile, you cannot modify it. However you can clone it.", { icon: 'info' })
                page.PageContainer.find('.user-profile-detail').attr('data-SystemDefine', "true");
            }
            if (!result.data.IsAdmin) {
                page.PageContainer.find('.tbl-module-list tbody tr[data-IsAdminModule]').remove();
            } else {
                page.PageContainer.find('.user-profile-detail').attr('data-AdminProfile', "true");
                page.PageContainer.find('.tbl-module-list tbody tr[data-IsPaidPlugin]').each(function () {
                    $(this).find('input[data-field="ModulePermission"]').attr('checked', true);
                    _common.DisableField($(this).find('input[data-field="ModulePermission"]').parent(), "checkbox");
                    $(this).find('.ddlModuleOperations').multiselect('selectAll', false, true).multiselect('refresh');
                    _common.DisableField($(this).find('.ddlModuleOperations'), "multiselect");
                });
            }
            if (result.data.Permissions) {
                for (let i = 0; i < result.data.Permissions.length; i++) {
                    let data = result.data.Permissions[i];
                    let tr = page.PageContainer.find('.tbl-module-list tbody tr[data-id="' + data.ModuleID + '"]');
                    if (tr) {
                        tr.find('[data-field="ModulePermission"]').prop('checked', true)
                        tr.find('.ddlModuleOperations').multiselect('select', (data.Operations || []));
                    }
                }
            }
            _userprofile.DisableDefaultUserProfile(page, result.data);
        }
    });
}

_userprofile.DisableDefaultUserProfile = function (page, data) {
    if (data.IsSystemDefine) {
        page.PageContainer.find('.btnUpdateProfile,.btnEditProfileBasicDetail').remove();
        page.PageContainer.find('.tbl-module-list').find('input[type="checkbox"]').prop('disabled', true);
    } else {
        page.PageContainer.find('.btnUpdateProfile,.btnEditProfileBasicDetail').removeClass('d-none');
    }
    if (page.PageContainer.find('.page-top-button button.btn-primary').length == 0) {
        page.PageContainer.find('.page-top-button button:last').addClass('btn-primary').removeClass('btn-outline-primary');
    }
}
_userprofile.ShowProfileUsersList = function (userProfileID, userProfileName) {

    _common.GetStaticPage('/page/company/profileuserlist.html', function (res) {
        _common.Loading(false);
        let modalBody = $('<div>' + res + '</div>').find('#content .modal-body').html();
        let modalTitle = "Users in Profile";
        if (userProfileName) {
            modalTitle = `Users in ${userProfileName} Profile`;
        }
        let modalUser = _common.DynamicPopUp({ title: modalTitle, body: modalBody });
        modalUser.PageSize = (Number(_initData.User.MySettings.GridRows) || 10);
        modalUser.PageIndex = 1;

        modalUser.find('.modal-footer button').remove();

        modalUser.find('.txtSearchInGrid').keydown(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13 && $(this).val().trim().length > 0) {
                modalUser.SearchValue = $(this).val();
                modalUser.GetUsersList();
            }
        });
        modalUser.find('.txtSearchInGrid').keyup(function () {
            if ($(this).val().trim().length == 0) {
                modalUser.SearchValue = "";
                modalUser.GetUsersList();
            }
        });
        modalUser.off("click", '.user-list-paging .pagination .page-item');
        modalUser.on("click", '.user-list-paging .pagination .page-item', function () {
            let pid = parseInt($(this).attr('pid'));
            let pager = $(this).closest('.pagination')[0].pager;
            if (pid < 1 || pid > pager.totalPages || pid == pager.currentPage) {
                return false;
            }
            modalUser.PageIndex = pid;
            modalUser.GetUsersList();
        });

        modalUser.GetUsersList = function () {
            let sdata = { "UserProfileID": userProfileID, "SearchValue": modalUser.SearchValue, PageSize: modalUser.PageSize, PageIndex: modalUser.PageIndex };
            _common.FetchApiByParam({
                url: "/api/userprofile/getprofileuserlist", type: "post", contentType: 'application/json', dataType: 'JSON'
                , data: sdata
            }, function (result) {
                modalUser.modal('show');
                modalUser.find('.datarow').remove();
                if (result && result.message == "success") {
                    let TotalUserCount = 0;
                    if (result.data && result.data.length > 0) {
                        let resultUserData = result.data[0];
                        if (resultUserData.TotalUserCount && resultUserData.TotalUserCount.length > 0) {
                            TotalUserCount = resultUserData.TotalUserCount[0].count;
                        }
                        for (let i = 0; i < resultUserData.UserList.length; i++) {
                            let data = resultUserData.UserList[i];
                            // let tr = "<tr class='datarow'><td>" + (data.UserName || "") + "</td>";
                            // tr += "<td>" + data.JobTitle + "</td>";
                            // tr += "<td>" + data.Status + "</td></tr>";
                            let tr = "<tr class='datarow w-100'><td><div class='grid-cell'><div class='cell-disp'>" + (data.UserName || "") + "</div></div></td>";
                            tr += "<td><div class='grid-cell'><div class='cell-disp'>" + data.JobTitle + "</div></div></td>";
                            tr += "<td><div class='grid-cell'><div class='cell-disp'>" + data.UserStatus + "</div></div></td></tr>";
                            modalUser.find('.tblUsers tbody').append(tr);
                        }
                    }
                    modalUser.find('.total-count').text((TotalUserCount > 0 ? TotalUserCount + " Items" : "No Data Found"));
                    let pager = _common.PagerService(TotalUserCount, modalUser.PageIndex, modalUser.PageSize);
                    _common.BuildPaging(pager, modalUser.find('.user-list-paging'));
                } else {
                    swal(result.message, { "icon": "info" });
                }
            });
        }
        modalUser.GetUsersList();
    });
}
