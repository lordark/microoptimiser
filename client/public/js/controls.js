$(function () {
    // Minimise plugin
    $(document).on('click', '.modal-minimise', function () {
        var modal = $(this).closest('.modal');
        var minSnippit = $('<button type="button" class="btn btn-dark modal-min-snippit shadow ml-2 pt-0 pb-0 pr-0 pl-2 rounded-top mb-n1"><span>' + modal.find('.modal-title').html() + '</span><i class="far fa-window-maximize btn btn-sm p-2 text-light"></i></button>');
        minSnippit.appendTo($('.minimize-panel'));
        minSnippit[0].modal = modal;
        modal[0].minSnippit = minSnippit;
        modal.addClass("minimized");
        modal.modal('hide');
        $('.minimize-panel').removeClass('bg-none').addClass('bg-white');
        if ($('.cal-activity').hasClass('opened-activity')) {
            $('.opened-activity').addClass('minimized');
        }

        // if($('.cal-activity').hasClass('dblclick-activity')){
        //     $('.dblclick-activity').css({'background-color':'#007EE3', 'color':'#fff'});
        // }

        let closeSnippit = $('.closing-event');
        closeSnippit.on('click', function () {
            //    $('.dblclick-activity').css({'background-color':'', 'color':''});
            $('.cal-activity').not('.cal-activity.minimized').removeClass('opened-activity');
        });

        minSnippit.click(function () {
            this.modal.modal('show');
            this.modal.removeClass("minimized");
            if ($('.minimize-panel button').length == 1) {
                $('.minimize-panel').removeClass('bg-white').addClass('bg-none');
            }
            $(this).remove();
        })
    })

    $(document).on('click', '.closing-event', function () {
        $('.cal-activity').not('.cal-activity.minimized').removeClass('opened-activity');
    })

    $(document).on('click', '.focus-shadow', function (e) {
        $(this).addClass('active');
        e.stopPropagation();
    })

    $(document).on('click', 'select', function () {
        this.isOpen = !this.isOpen;
    })
    $(document).on('blur', 'select', function () {
        this.isOpen = !this.isOpen;
    });
    $(document).on('keyup', 'select', function (e) {
        if (e.keyCode == 27) {
            if (this.isOpen) {
                this.isOpen = !this.isOpen;
            }
        }
    });

    $(document).on('click contextmenu', function (e) {
        $('.focus-shadow.active').removeClass('active');

        if ($('.msp-popup').length > 0) {
            if (!$(e.target).hasClass('trigger-popup')) {
                //to clear the selected object in the context menu;
                _mydrive.ListViewContextSelection = [];
                $(this).removeClass('drive-selected');
                $('.msp-popup').remove();
            }
        }
        if ($('.context-menu:visible').length > 0) {
            $('.context-menu:visible').hide();
        }
    });

    $(document).on('click', '.email-section .email', function () {
        let toEmails = $(this).closest('.email-section').find('.email').html().split(",");
        _controls.ComposeMailPopup(toEmails)

    })

    $(document).on('click', '.lookup-display-record', function (e) {
        e.stopPropagation();
        let self = $(this);
        if (self.hasClass('deleted-record')) {
            swal('This record has been deleted.', { icon: "info", });
            return;
        }
        if (self.data('object-schema') == undefined) {
            _common.Loading(true);
            let obj = { 'objectName': self.data('objname'), 'fields': 'Name' };
            obj.fields = { _id: 0, DetailPage: 1 };
            obj.callback = function (res) {
                _common.Loading(false);
                if (res == null) { res = {} };
                self.data('object-schema', res);
                if (self.data('object-schema').DetailPage != undefined) {
                    new Page({ pageName: self.data('object-schema').DetailPage, _id: self.data('objid') });
                }
            }
            _common.GetObjectSchemaDetail(obj);
        }
        else if (self.data('object-schema').DetailPage != undefined) {
            new Page({ pageName: self.data('object-schema').DetailPage, _id: self.data('objid') });
        }
    });

    //calendar ctrl
    $(document).on('mouseenter', '.msp-date-picker .monthYear', function (e) {
        if ($(this).find('.cal-month-menu').length == 0) {
            let navCalDropdown = $('<div class="nav-calendar-dropdowns row"><select class="dropdown-nav cal-month-menu h5 border-0 mb-0 col-auto custom-select"></select><select class="custom-select dropdown-nav cal-year-menu h5 border-0 mb-0 col-auto"></select></div>');
            var date = $(this).html();
            var navDom = $(this);
            let mon = moment(date).get('month');
            let year = moment(date).get('year');

            let months = '';
            let years = '';
            for (let i = 0; i < _constantClient.Months.length; i++) {
                let obj = _constantClient.Months[i];
                months += '<option class="dropdown-item" value="' + obj.Index + '">' + obj.SortName + '</option>';
            }
            for (let i = -50; i < 50; i++) {
                years += '<option class="dropdown-item" value="' + (year + i) + '">' + (year + i) + '</option>';
            }
            navCalDropdown.find('.cal-month-menu').append(months);
            navCalDropdown.find('.cal-year-menu').append(years);

            navDom.html(navCalDropdown);
            navDom.find('.cal-month-menu').val(mon);
            navDom.find('.cal-year-menu').val(year);
        }
    });
    $(document).on('mouseleave', '.msp-date-picker .monthYear', function (e) {
        if ($(this).find('.cal-month-menu').length > 0) {
            if ($(this).find('.cal-month-menu')[0].isOpen || $(this).find('.cal-year-menu')[0].isOpen) {
                return;
            }
            let arrMonth = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var navDom = $(this);
            var month = navDom.find('.cal-month-menu').val();
            var year = navDom.find('.cal-year-menu').val();
            $(this).html(arrMonth[month] + ' ' + year);
        }

    });
    $(document).on('click', '.msp-date-picker .monthYear select', function (e) {
        e.stopPropagation();
    });
    $(document).on('change', '.msp-date-picker .monthYear select', function (e) {
        e.stopPropagation();
        var datePicker = $(this).closest('.msp-date-picker');
        var currentYear = datePicker.find('.cal-year-menu').val();
        var currentMonth = datePicker.find('.cal-month-menu').val();
        currentYear = parseInt(currentYear);
        currentMonth = parseInt(currentMonth);
        datePicker[0].currentYear = currentYear;
        datePicker[0].currentMonth = currentMonth;
        _controls.DatePicker.Build(datePicker);
    })

    //validation according to ctrl
    $(document).on('input keydown keyup mousedown mouseup select contextmenu drop', 'input[data-ui="int"], input[data-ui="decimal"]', function (e) {
        let isValid = true;
        let dataType = $(this).attr('data-ui');
        let value = this.value;
        let max = $(this).attr('max-value');
        let min = $(this).attr('min-value');

        switch (dataType) {
            case "int":
                isValid = /^-?\d*$/.test(value);
                break;
            case "decimal":
                isValid = /^-?\d*[.,]?\d*$/.test(value);
                break;
        }
        if (max !== undefined && isValid) {
            isValid = (value === '' || value === '-' || parseFloat(value) <= max);
        }
        if (min !== undefined && isValid) {
            isValid = (value === '' || value === '-' || parseFloat(value) >= min);
        }

        if (isValid) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        }
        else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
    });

    $(document).on('input keydown keyup mousedown mouseup select contextmenu drop', 'input[max-length], textarea[max-length]', function (e) {
        let isValid = true;
        let maxLength = $(this).attr('max-length');

        if (this.value)
            isValid = (this.value.length <= maxLength);

        if (isValid) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        }
        else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
    });

    $(document).on('change', 'input.msp-ctrl-api-name', function (e) {
        if ($(this).val() != '')
            $(this).val(_common.FormatForApiName($(this).val()));
    });

    $(document).on('focus', 'input.msp-ctrl-date', function (e) {
        e.stopPropagation();
        _controls.DatePicker({ el: $(this) });
    });
    $(document).on('focus', 'input.msp-ctrl-time', function (e) {
        e.stopPropagation();
        var popup = _controls.GetPopup($(this));
        popup.html(_controls.TimePicker($(this)));
        popup[0].ctrl = $(this);
        this.popup = popup;
        if ($(this).attr('data-time')) {
            let sItem = popup.find('.item[data-time="' + $(this).attr('data-time') + '"]').addClass('item-selected');
            if (sItem.length > 0) {
                var sTop = sItem.offset().top - $(this).offset().top - sItem.height() - 20;
                popup.find('.msp-time-picker').scrollTop(sTop);
            }
        }
    });
    $(document).on('click', 'input.msp-ctrl-date', function (e) { e.stopPropagation(); });
    $(document).on('click', 'input.msp-ctrl-time', function (e) { e.stopPropagation(); });
    $(document).on('keyup', 'input.msp-ctrl-date', function (e) {
        var newDate = moment($(this).val(), [_initData.Company.Setting.DateFormat]).toDate();
        if (_controls.isValidDate(newDate)) {
            _controls.DatePicker({ el: $(this) });
        }
    });
    $(document).on('keydown', 'input.msp-ctrl-time', function (e) {
        e.stopPropagation();
        if (_controls.BindNavKeys({
            event: e, element: this, onSelect: function (el) {
                var ctrl = el.closest('.msp-time-picker').parent()[0].ctrl
                ctrl.val(el.text());
                ctrl.attr('data-time', el.text());
            }
        })) { e.preventDefault(); return; }
    });
    $(document).on('keydown', '.msp-ctrl-time-picker', function (e) {
        e.stopPropagation();
        if (_controls.BindNavKeys({
            event: e, element: this, onSelect: function (el) {
                var ctrl = el.closest('.msp-time-picker').parent()[0].ctrl;
                ctrl.val(el.text());
                ctrl.attr('data-time', el.text());
            }
        })) { e.preventDefault(); return; }
    });
    $(document).on('keyup focus', '.msp-ctrl-lookup', function (e) {
        e.stopPropagation();
        if (_controls.CheckNavKey(e)) { e.preventDefault(); return; }

        var self = this;
        var val = '';
        if ($(this).is('input'))
            val = $(this).val();
        else
            val = $(this).text();

        let field = $(this).closest('[data-field]')[0].field;
        if (field !== undefined && field.Schema != undefined) {
            if (self.IsLoadingData) { return false; }
            self.IsLoadingData = true;

            let param = { 'objectName': field.ObjectName, 'fieldName': field.Name, 'text': val };
            if (field.Collections) {
                param.Collections = field.Collections;
            }
            let match = {};
            if (field.Match != undefined)
                match = JSON.parse(field.Match);
            else if (field.Schema.Match != undefined)
                match = JSON.parse(field.Schema.Match);

            let excludeArry = [];
            if (field.Schema.NotIncludeSelf)
                excludeArry.push(_controls.ToEJSON("objectid", _initData.User._id));

            if (field.Schema.IsExcludeParent) {
                let form = $(this).closest('tr[data-id]');
                let _id;
                if (form.length > 0)
                    _id = form.attr('data-id');
                else {
                    form = $(this).closest('.page-form');
                    if (form.length > 0)
                        _id = form[0].PageForm._id;
                }
                if (_id != undefined) {
                    excludeArry.push(_controls.ToEJSON("objectid", _id));
                    match[field.Name] = { $nin: [_controls.ToEJSON("objectid", _id)] };
                }

            }

            if (excludeArry.length > 0)
                match["_id"] = { $nin: excludeArry };

            if (field.Schema.Limit != undefined) {
                param.limit = field.Schema.Limit;
            }

            let matchByFields = field.Schema.MatchByFields;
            if (field.MatchByFields != undefined)
                matchByFields = field.MatchByFields;

            if (matchByFields != undefined) {
                for (let i = 0; i < matchByFields.length; i++) {
                    let form = $(this).closest('tr[data-id]');
                    if (form == undefined || form.length == 0) {
                        form = $(this).closest('.page-form');
                    }
                    if (form != undefined) {
                        let objMatchField = matchByFields[i];
                        if (objMatchField.Value !== undefined) {
                            match[objMatchField.Field] = objMatchField.Value;
                        }
                        else {
                            let matchField = form.find('[data-field="' + objMatchField.ValueFrom + '"].msp-ctrl')[0].field;
                            if (matchField != undefined) {
                                let matchFieldValue = _controls.GetCtrlValue({ field: matchField, container: form, isEjsonFormat: true });
                                if (matchFieldValue) {
                                    if (matchField.Schema.UIDataType == 'multilookup') {
                                        match[objMatchField.Field] = { $in: matchFieldValue };
                                    }
                                    else {
                                        match[objMatchField.Field] = matchFieldValue;
                                    }
                                }
                                else
                                    return false;

                            }
                        }
                    }


                }
            }

            let ctrlWidth = $(this).width() + 'px';
            let ctrlHTM = '<div class="msp-lookup" style="min-width:' + ctrlWidth + ';max-height:300px;min-height:20px;overflow-y:auto" data-field-name="' + field.Name + '"></div>';

            let isShowAddItem = true;
            if (field.HasAddButton !== undefined)
                isShowAddItem = field.HasAddButton;
            else if (field.Schema.HasAddButton !== undefined)
                isShowAddItem = field.Schema.HasAddButton;
            if ($(this).closest('.page-form').length > 0 && $(this).closest('.page-form').hasClass('secondary-form') == true)
                isShowAddItem = false;

            if (Object.keys(match).length > 0)
                param.match = match;

            param.callback = function (data) {
                self.IsLoadingData = false;
                let popup = _controls.GetPopup($(self));
                self.popup = popup;
                popup[0].ctrl = $(self);
                popup.html(ctrlHTM);
                if (data && data.objectdata && data.objectdata.length) {
                    for (let i = 0; i < data.objectdata.length; i++) {
                        let item = data.objectdata[i];
                        let itemSec = $('<div class="item" data-id="' + item._id + '">' + item.name + '</div>');
                        itemSec[0].item = item;
                        popup.find('.msp-lookup').append(itemSec)
                    }
                }
                if (param.text != '' && data.objectdata.length == 0) {
                    popup.find('.msp-lookup').append('<div class="no-data-found">No data found</div>');
                }
                if (isShowAddItem && param.objectName != 'User') {
                    popup.find('.msp-lookup').append('<div class="add-new" data-id="-1">Add New <i class="fa fa-plus-square"></i></div>');
                }
                if (popup.find('.msp-lookup div').length == 0)
                    popup.remove();
            }
            _common.GetLookupList(param);
        }

    });

    $(document).on('keydown', '.msp-ctrl-lookup', function (e) {
        e.stopPropagation();
        if (_controls.BindNavKeys({
            event: e, element: this, onEnter: function (el) {
                if (el.length > 0) {
                    _controls.SetLookupValue(el);
                }
            }
        })) { e.preventDefault(); return false; }
    });
    $(document).on('keyup focus', '.msp-ctrl-multilookup', function (e) {
        e.stopPropagation();
        if (_controls.CheckNavKey(e)) { e.preventDefault(); return; }

        var self = this;
        let inputCtrl = $(this).find('input');
        if ($(this).hasClass('focus-ctrl') == false)
            $(this).addClass('focus-ctrl')
        if (inputCtrl.is(":focus") == false)
            inputCtrl.focus();

        let val = inputCtrl.val();
        let field = $(this).closest('[data-field]')[0].field;
        let disableLookup = false;
        if (field !== undefined && field.Schema != undefined) {
            if (field.DisableLookup != undefined)
                disableLookup = field.DisableLookup;
            if (!disableLookup) {
                if (self.IsLoadingData) { return false; }
                self.IsLoadingData = true;

                let param = { 'objectName': field.ObjectName, 'fieldName': field.Name, 'text': val, 'multilookupfield': true };
                if (field.Collections) {
                    param.Collections = field.Collections;
                }
                let match = {};
                if (field.Match != undefined)
                    match = JSON.parse(field.Match);
                else if (field.Schema.Match != undefined)
                    match = JSON.parse(field.Schema.Match);

                let excludeArry = _controls.GetCtrlValue({ field: field, container: $(this).closest('[data-field]').parent(), isEjsonFormat: true });
                excludeArry = excludeArry ? excludeArry : [];

                if (field.Schema.NotIncludeSelf)
                    excludeArry.push(_controls.ToEJSON("objectid", _initData.User._id));

                if (field.Schema.IsExcludeParent) {
                    let form = $(this).closest('tr[data-id]');
                    let _id;
                    if (form.length > 0)
                        _id = form.attr('data-id');
                    else {
                        form = $(this).closest('.page-form');
                        if (form.length > 0)
                            _id = form[0].PageForm._id;
                    }
                    if (_id != undefined) {
                        excludeArry.push(_controls.ToEJSON("objectid", _id));
                        match[field.Name] = { $nin: [_controls.ToEJSON("objectid", _id)] };
                    }

                }
                if (excludeArry.length > 0)
                    match["_id"] = { $nin: excludeArry };

                if (field.Schema.Limit != undefined) {
                    param.limit = field.Schema.Limit;
                }

                let matchByFields = field.Schema.MatchByFields;
                if (field.MatchByFields != undefined)
                    matchByFields = field.MatchByFields;

                if (matchByFields != undefined) {
                    for (let i = 0; i < matchByFields.length; i++) {
                        let form = $(this).closest('tr[data-id]');
                        if (form == undefined || form.length == 0) {
                            form = $(this).closest('.page-form');
                        }
                        if (form != undefined) {
                            let objMatchField = matchByFields[i];
                            if (objMatchField.Value !== undefined) {
                                match[objMatchField.Field] = objMatchField.Value;
                            }
                            else {
                                let matchField = form.find('[data-field="' + objMatchField.ValueFrom + '"]')[0].field;
                                if (matchField != undefined) {
                                    let matchFieldValue = _controls.GetCtrlValue({ field: matchField, container: form, isEjsonFormat: true });
                                    if (matchFieldValue) {
                                        if (matchField.Schema.UIDataType == 'multilookup') {
                                            match[objMatchField.Field] = { $in: matchFieldValue };
                                        }
                                        else {
                                            match[objMatchField.Field] = matchFieldValue;
                                        }
                                    }
                                    else
                                        return false;

                                }
                            }

                        }

                    }
                }


                let ctrlWidth = $(this).width() + 'px';
                let ctrlHTM = '<div class="msp-multilookup" style="min-width:' + ctrlWidth + ';max-height:300px;min-height:20px;overflow-y:auto" data-field-name="' + field.Name + '"></div>';

                let isShowAddItem = true;
                if (field.HasAddButton !== undefined)
                    isShowAddItem = field.HasAddButton;
                else if (field.Schema.HasAddButton !== undefined)
                    isShowAddItem = field.Schema.HasAddButton;
                if ($(this).closest('.page-form').length > 0 && $(this).closest('.page-form').hasClass('secondary-form') == true)
                    isShowAddItem = false;

                if (Object.keys(match).length > 0)
                    param.match = match;

                param.callback = function (data) {
                    self.IsLoadingData = false;
                    let popup = _controls.GetPopup($(self));
                    self.popup = popup;
                    popup[0].ctrl = $(self);
                    popup.html(ctrlHTM);
                    if (data && data.tagdata && data.tagdata.length) {
                        for (let i = 0; i < data.tagdata.length; i++) {
                            let item = data.tagdata[i];
                            let itemSec = $(`<div class="item-tag" data-id="${item._id}" data-tagobject="${item.TagType}">${item.Name} <i class="fa fa-tags"></i><span class='secondary-label'>${item.OwnerID_LookupData.FirstName} ${item.OwnerID_LookupData.LastName}</span></div>`);
                            itemSec[0].item = item;
                            popup.find('.msp-multilookup').append(itemSec)
                        }
                    }
                    if (data && data.objectdata && data.objectdata.length) {
                        for (let i = 0; i < data.objectdata.length; i++) {
                            let item = data.objectdata[i];
                            let itemSec = $('<div class="item" data-id="' + item._id + '">' + item.name + '</div>');
                            itemSec[0].item = item;
                            popup.find('.msp-multilookup').append(itemSec)
                        }
                    }
                    if (param.text != '' && data.objectdata && data.objectdata.length == 0) {
                        popup.find('.msp-multilookup').append('<div class="no-data-found">No data found</div>');
                    }
                    if (param.text != '' && data.tagdata && data.tagdata.length == 0) {
                        popup.find('.msp-multilookup').append('<div class="no-data-found">No tag found</div>');
                    }
                    if (isShowAddItem) {
                        popup.find('.msp-multilookup').append('<div class="add-new" data-id="-1">Add New <i class="fa fa-plus-square"></i></div>');
                    }
                    if (popup.find('.msp-multilookup div').length == 0)
                        popup.remove();
                }
                _common.GetLookupList(param);

                if (e.type == "focusin" && $(e.target).hasClass('msp-ctrl-multilookup')) {
                    if ($(this).find('input.trigger-popup').length == 0) {
                        $(this).append('<input class="trigger-popup" />');
                    }
                    $(this).find('input.trigger-popup').select();
                }
            }
        }
    });

    $(document).on('keydown', '.msp-ctrl-multilookup', function (e) {
        e.stopPropagation();
        if (_controls.BindNavKeys({
            event: e, element: this, onEnter: function (el) {
                if (el.length > 0) {
                    _controls.SetMultiLookupValue(el);
                }
            }
        })) { e.preventDefault(); return false; }
    });

    $(document).on('blur', '.msp-ctrl-multilookup input', function (e) {
        $(this).val('').closest('.msp-ctrl').removeClass('focus-ctrl')
    });

    //City Change
    $(document).on('onLookupChange', '.msp-ctrl-lookup[country-field]', function (e, el) {
        let item = el.item;
        if (item && item.LookupData && item.LookupData.CountryID) {
            let countryID = item.LookupData.CountryID;
            let container = $(this).closest('tr[data-id]');
            if (container == undefined || container.length == 0)
                container = $(this).closest('.page-form');

            let ctrl = container.find('.msp-ctrl[data-field="' + $(this).attr('country-field') + '"]');
            if (ctrl && ctrl.val() != countryID) {
                ctrl.val(countryID);
                if (container.hasClass('page-form') == false)
                    ctrl.closest('td').addClass('changed');
            }
        }
    });
    //Country Change
    $(document).on('change', '.msp-ctrl[city-field]', function () {
        let container = $(this).closest('tr[data-id]');
        if (container == undefined || container.length == 0)
            container = $(this).closest('.page-form');

        let ctrl = container.find('.msp-ctrl-lookup[data-field="' + $(this).attr('city-field') + '"]');
        if (ctrl && ctrl.length > 0 && ctrl.html() != '') {
            ctrl.html('');
            ctrl.attr('contenteditable', 'true');
            if (container.hasClass('page-form') == false)
                ctrl.closest('td').addClass('changed');
        }
    });

    $(document).on('keyup', '.msp-ctrl-texteditor.hotkey-enabled .texteditor', function (e) {
        e.stopPropagation();
        var self = $(this);
        if (e.key == "@") {
            if (self.find('.msp-popup').length > 0) {
                self.find('.msp-popup').parent().remove();
            }
            var popup = $('<div class="border bg-white msp-popup shadow-lg rounded-bottom" style="position:absolute;z-index:1060;" data-allowed="true" data-pos="0"></div>');
            self.popup = popup;
            let dataList = self[0].DataList;
            for (let i = 0; i < dataList.length; i++) {
                let itm = $('<div class="item" data-fieldName="' + dataList[i].FieldName + '">' + dataList[i].DisplayName + '</div>');
                popup.append(itm);
            }
            setTimeout(function () {
                let left = 0;
                let top = '21px';
                popup.css({ 'left': left, 'top': top });
                var pSpan = $('<span style="position:relative;width:1px;height:1px" class="popup-container" contenteditable="false"></span>');
                var sel = window.getSelection();
                var range = sel.getRangeAt(0);
                if (range) {
                    popup.attr('data-pos', range.startOffset);
                    range.insertNode(pSpan[0]);
                    popup.appendTo(pSpan);
                    range.collapse(false);
                    popup[0].range = range;
                }

                popup.on('mousedown', '.item', function (e) {
                    if (e.button == 0) {
                        //let field = $(this).attr('data-fieldName');
                        //popup.parent().before(field);
                        //popup.parent().remove();
                        e.preventDefault();
                        if ($(this).attr('data-fieldName')) {
                            var textNode = document.createTextNode($(this).attr('data-fieldName'));
                            let rangeTemp = self.find('.msp-popup')[0].range;
                            let clearSearchValueFlag = self.find('.msp-popup')[0].clearSearchValue;
                            let searchRange = self.find('.msp-popup')[0].srange;
                            let searchRangeTextLength = (self.find('.msp-popup')[0].SearchValueLength || 0);

                            if (rangeTemp) {
                                if (clearSearchValueFlag == true) {
                                    if (searchRange) {
                                        if (searchRange.startContainer) {
                                            searchRange.startContainer.replaceData(0, searchRangeTextLength, "");
                                        }
                                    }
                                }
                                rangeTemp.insertNode(textNode);
                                self.find('.msp-popup').parent().remove();
                            }
                        } else {
                            self.find('.msp-popup').parent().remove();
                        }
                    }
                });
            }, 100);
        } else {
            if (self.find('.msp-popup').attr('data-allowed') == "true"
                && (e.key != "ArrowUp" && e.key != "ArrowDown" && e.key != "Enter")) {
                var sel = window.getSelection();
                var range = sel.getRangeAt(0);
                if (range) {
                    self.find('.msp-popup').html('');
                    let searchVal = "";
                    let clearSearchVal = true;
                    if (range.startContainer.substringData) {
                        searchVal = range.startContainer.substringData(0, range.startOffset);
                    } else {
                        try {
                            searchVal = range.startContainer.substringData(self.find('.msp-popup').attr('data-pos'), range.startOffset);
                        }
                        catch (ex) {
                            clearSearchVal = false;
                            searchVal = "";
                        }
                    }
                    searchVal = (searchVal || "").toString().toLowerCase().trim();
                    let dataList = self[0].DataList;
                    for (let i = 0; i < dataList.length; i++) {
                        if (dataList[i].DisplayName.toLowerCase().indexOf(searchVal) >= 0) {
                            let itm = $('<div class="item" data-fieldName="' + dataList[i].FieldName + '">' + dataList[i].DisplayName + '</div>');
                            self.find('.msp-popup').append(itm);
                        }
                    }
                    if (self.find('.msp-popup .item').length == 0) {
                        self.find('.msp-popup').attr('data-allowed', "false");
                        self.find('.msp-popup').closest('.popup-container').remove();
                    } else {
                        self.find('.msp-popup')[0].srange = range;
                        self.find('.msp-popup')[0].clearSearchValue = clearSearchVal;
                        self.find('.msp-popup')[0].SearchValueLength = searchVal.length;
                    }
                }
            }
        }
    });
    $(document).on('keydown', '.msp-ctrl-texteditor.hotkey-enabled .texteditor', function (e) {
        e.stopPropagation();
        let self = $(this);
        if (self.find('.msp-popup').length > 0) {
            let cIndex = 0;
            if (self.find('.msp-popup')[0].currentIndex) {
                cIndex = self.find('.msp-popup')[0].currentIndex;
            }
            self.find('.msp-popup .item').removeClass('selected');
            if (e.key == "ArrowUp" || e.key == "ArrowDown" || e.key == "Enter") {
                e.preventDefault();
                if (e.key == "ArrowUp") {
                    if (cIndex > 0) {
                        cIndex -= 1;
                    }
                    self.find('.msp-popup')[0].currentIndex = cIndex;
                } else if (e.key == "ArrowDown") {
                    if (cIndex < self.find('.msp-popup .item').length - 1) {
                        cIndex += 1;
                    }
                    self.find('.msp-popup')[0].currentIndex = cIndex;
                }
                self.find('.msp-popup .item').eq(cIndex).addClass('selected');
                if (e.key == "Enter") {
                    let field = self.find('.msp-popup .item.selected');
                    if (field && field.attr('data-fieldName')) {
                        var textNode = document.createTextNode(field.attr('data-fieldName'));
                        let rangeTemp = self.find('.msp-popup')[0].range;
                        let clearSearchValueFlag = self.find('.msp-popup')[0].clearSearchValue;
                        let searchRange = self.find('.msp-popup')[0].srange;
                        let searchRangeTextLength = (self.find('.msp-popup')[0].SearchValueLength || 0);
                        self.find('.msp-popup').parent().remove();
                        if (rangeTemp) {
                            if (clearSearchValueFlag == true) {
                                if (searchRange) {
                                    if (searchRange.startContainer) {
                                        searchRange.startContainer.replaceData(0, searchRangeTextLength, "");
                                    }
                                }
                            }
                            rangeTemp.insertNode(textNode);
                        }
                    }
                    self.find('.msp-popup').parent().remove();
                }
            }
            else {
                if (e.key == " ") {
                    self.find('.msp-popup').parent().remove();
                }
            }
        }
    });

    $(document).on('change', 'input.msp-ctrl-date', function (e) {
        var newDate = moment($(this).val(), [_initData.Company.Setting.DateFormat]).toDate();
        var oldDate = moment($(this).attr('data-date')).toDate();
        if ($(this).val() == '') {
            $(this).removeAttr('data-date');
        } else if (_controls.isValidDate(newDate)) {
            $(this).val(moment($(this).val()).format(_initData.Company.Setting.DateFormat));
            $(this).attr('data-date', $(this).val());
        } else if (_controls.isValidDate(oldDate)) {
            $(this).val(moment($(this).attr('data-date')).format(_initData.Company.Setting.DateFormat));
        } else {
            $(this).val('');
        }
    });
    $(document).on('change', 'input.msp-ctrl-time', function (e) {
        var format = 'HH:mm';
        let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
        if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
            calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
        }
        if ($(this).val() == '') {
            $(this).removeAttr('data-time');
        } else if (moment($(this).val(), calendarTimeFormat, true).isValid()) {
            $(this).attr('data-time', moment($(this).val(), calendarTimeFormat).format(format));
            $(this).val(moment($(this).attr('data-time'), format).format(calendarTimeFormat));
        } else {
            if (moment($(this).attr('data-time'), format, true).isValid()) {
                $(this).val(moment($(this).attr('data-time'), format).format(calendarTimeFormat))
            } else {
                $(this).val('');
            }
        }
    });


    $(document).on('click', '.msp-date-picker .fa-angle-left', function (e) {
        e.stopPropagation();
        var datePicker = $(this).closest('.msp-date-picker');
        var currentYear = datePicker[0].currentYear;
        var currentMonth = datePicker[0].currentMonth;
        currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
        currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
        datePicker[0].currentYear = currentYear;
        datePicker[0].currentMonth = currentMonth;
        _controls.DatePicker.Build(datePicker);
    });
    $(document).on('click', '.msp-date-picker .fa-angle-right', function (e) {
        e.stopPropagation();
        var datePicker = $(this).closest('.msp-date-picker');
        var currentYear = datePicker[0].currentYear;
        var currentMonth = datePicker[0].currentMonth;
        currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
        currentMonth = (currentMonth + 1) % 12;
        datePicker[0].currentYear = currentYear;
        datePicker[0].currentMonth = currentMonth;
        _controls.DatePicker.Build(datePicker);
    });
    $(document).on('click', '.msp-date-picker [data-date]', function (e) {
        var datePicker = $(this).closest('.msp-date-picker');
        var currentYear = datePicker[0].currentYear;
        var currentMonth = datePicker[0].currentMonth;
        datePicker.find('.cal-body .item-selected[data-date]').removeClass('item-selected').removeClass('rounded-circle');
        $(this).addClass('item-selected').addClass('rounded-circle');
        datePicker.find('.cal-body .btn-primary[data-date]').addClass('rounded-circle');
        var selectedDate = moment($(this).attr('data-date')).toDate();
        var selYear = selectedDate.getFullYear();
        var selMonth = selectedDate.getMonth();
        datePicker[0].currentDate = selectedDate;
        if (!(selMonth == currentMonth && selYear == currentYear)) {
            datePicker[0].currentMonth = selMonth;
            datePicker[0].currentYear = selYear;
            _controls.DatePicker.Build(datePicker);
        }
        if (datePicker[0].options.onSelect) {
            datePicker[0].options.onSelect(selectedDate);
        }
        if (datePicker[0].options.input) {
            datePicker[0].options.input.val(moment(selectedDate).format(_initData.Company.Setting.DateFormat));
            datePicker[0].options.input.attr('data-date', moment(selectedDate).format('YYYY-MM-DD'));
            datePicker.closest('.msp-popup').remove();
            datePicker[0].options.input.change();
        }

    });
    $(document).on("click", ".msp-time-picker .item", function () {
        var ctrl = $(this).closest('.msp-time-picker').parent()[0].ctrl;
        ctrl.val($(this).text());
        ctrl.attr('data-time', $(this).attr('data-time'));
        $(this).closest('.msp-popup').remove();
        ctrl.change();
    });
    $(document).on("click", ".msp-lookup .item", function () {
        _controls.SetLookupValue($(this));
        $(this).closest('.msp-popup').remove();
    });
    $(document).on("click", ".msp-multilookup .item", function () {
        _controls.SetMultiLookupValue($(this));
        $(this).closest('.msp-popup').remove();
    });
    $(document).on("click", ".msp-multilookup .item-tag", function () {
        _controls.SetTagValue($(this));
        $(this).closest('.msp-popup').remove();
    });
    $(document).on("click", ".ctrl-container .msp-popup .add-new", function () {
        var ctrl = $(this).closest('.ctrl-container');
        let previousPage = $(this).closest(".page-form");
        var field = ctrl[0].field;
        let objectName = field.Schema.LookupObject;
        if (objectName == "ViewAttendee") {
            objectName = "Contact";
        }
        _common.FetchApiByParam({ url: "/api/common/getobjectschemadetail", data: { objectname: objectName, fields: { _id: 0, DetailPage: 1 } } }, function (data) {
            if (data) {
                let param = {};
                param.pageName = data.DetailPage;
                param.childContainor = previousPage;
                param.formClass = "secondary-form";
                if(previousPage[0] && previousPage[0].PageForm != undefined){
                    let pagedata = previousPage[0].PageForm;
                    if(pagedata.PageName == "AccountContactRelationshipDetail"){
                        if(pagedata.ParamData != undefined && pagedata.ParamData.MatchFields != undefined){
                            param.MatchFields = pagedata.ParamData.MatchFields;
                        }
                    }
                }
                if (field.Schema.LookupObject != "ViewAttendee") {
                    param.onSave = function (pageObj) {
                        _controls.SetLookupTypeValue({ 'field': field, 'ctrl': ctrl, 'data': pageObj.ResultData.data });
                    }
                }
                param.sourceFormCtrl = ctrl;
                if (objectName == "Tag") {
                    let tagObject = ctrl.find('.msp-ctrl').attr('data-tagObject');
                    let matchField = [];
                    matchField.push({ 'Name': 'TagType', 'Value': tagObject })
                    param.MatchFields = matchField;
                }
                new PageForm(param);
            } else {
                swal("Addition form for this object " + objectName + " is not configure.", { icon: "info", });
            }
        });
    });

    $(document).on('focus', ".lookup-item", function (e) {
        e.stopPropagation();
    });
    $(document).on('click', ".msp-ctrl-lookup .lookup-item .fa-times", function (e) {
        e.stopPropagation();
        let ctrl = $(this).closest('.msp-ctrl-lookup');
        $(this).closest('.lookup-item').remove();
        ctrl.attr('contenteditable', 'true').focus();
    });
    $(document).on('click', ".msp-ctrl-multilookup .lookup-item .fa-times", function (e) {
        e.stopPropagation();
        let ctrl = $(this).closest('.msp-ctrl-multilookup');
        $(this).closest('.lookup-item').remove();
        if (ctrl.find('.lookup-item').length == 0) {
            ctrl.focus();
        }
    });
    $(document).on("change", ".msp-ctrl-file", function (value) {
        // $(this).html('<div class="div-file-uploading d-none "></div>');
        let FilesPresent = $(this).find('input')[0].files.length > 0
        let FileSize = $(this).find('input')[0].files;
        //to limit upload to 40 mb for documents
        if (FileSize[0].size <= 41943040) {
            if (FilesPresent) {
                _controls.AttachFile($(this), value);
                $(this).find('input[type="file"]').val('')
                $(this).hide();

            }
        } else {
            $(this).find('input[type="file"]').val('')
            return swal("Sorry.You can upload file up to 40mb in size. Please reduce the size of your file and try again.", { icon: "error" });
        }
    });
    $(document).on('click', ".div-file-uploading .fa-times", function () {
        $(this).closest('.div-file-uploading').html('').prev().show();
    });

    $(document).on('click', ".msp-ctrl-image-edit", function () {
        let imageBox = {};
        imageBox.ImageContainer = $(this).closest('.msp-ctrl-image');
        imageBox.Height = (Number(imageBox.ImageContainer.attr('data-height')) || 0);
        imageBox.Width = (Number(imageBox.ImageContainer.attr('data-width')) || 0);
        if ($(this).parent().hasClass("profile-img-edit")) {//To get better quality profile image
            imageBox.Height = 300;
            imageBox.Width = 300;
        }

        imageBox.CallingControl = $(this).closest('.msp-ctrl-image');
        IBP.ImageEditor(imageBox);
    });
    $(document).on('click', ".msp-ctrl-image-remove", function () {
        let defaultImageURL = $(document).find('[enctype="multipart/form-data"]>img').attr("src");
        if (defaultImageURL != "/images/bg.png") {
            swal({
                icon: "warning",
                text: "Do you really wanted to remove the picture",
                buttons: true
            })
                .then((value) => {
                    if (value) {
                        let imageParent = $(this).closest('.msp-ctrl-image');
                        let imageId = imageParent.attr('data-imageid');
                        imageParent.removeAttr('data-imageid');
                        imageParent.find('img').attr('src', "/images/bg.png");
                        // if (imageId) {
                        //     let sdata = {};
                        //     sdata.filedata = imageId;
                        //     $.ajax({
                        //         url: "/api/drive/deletefile",
                        //         type: "post",
                        //         contentType: "application/json",
                        //         data: JSON.stringify(sdata),
                        //         success: function (result) {
                        //             alert('Image successfully deleted');
                        //             imageParent.removeAttr('data-imageid');
                        //             imageParent.find('img').attr('src', "/images/bg.png");
                        //         },
                        //         error: function (result) {
                        //             alert('Image not deleted');
                        //         }
                        //     });
                        // }
                        swal({ icon: "success", text: "Profile picture has been removed sucessfully" });
                    }
                });
        }
    });

    $(document).on('change', '[data-field="UserStatus"][title="Status"]', function () {
        let self = $(this);
        if (self.val() == "Suspend") {
            swal({
                title: "This action will suspend all relevent activities associated with the user.",
                text: "Do you really want to continue?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((change) => {
                    if (!change) {
                        self.val('Active');
                    }
                });
        }
    });


    $(document).on('mousedown', '.msp-ctrl-texteditor .texteditor', function (e) {
        //  e.stopPropagation();
        //  $('.msp-ctrl-texteditor .texteditor').prop('contenteditable', false);
        $('.msp-ctrl-texteditor .editor-tool-box').children().prop('disabled', true);
        $(this).prop('contenteditable', true);
        $(this).parent().find('.editor-tool-box').children().prop('disabled', false);
    })

    $(document).on('click', '.msp-ctrl-texteditor .editortool', function () {
        editor.load($(this));
    })
    $(document).on('change', '.msp-ctrl-texteditor .editor-ddlchange,.editor-ddlFont', function () {
        editor.load($(this));
    })

    $(document).on('paste', '[contenteditable]', function (e) {
        if (!$(this).closest('.emailsignature').attr('allow-formattedpaste')) {
            e.preventDefault();
            let text = e.originalEvent.clipboardData.getData("text/plain");
            document.execCommand('inserttext', false, text);
        }
    });
    $(document).on('click', '.msp-ctrl-location .btnSearchAddress', function () {
        $(this).attr("disabled", true)//to disable the search address popup button
        let parentForm = null,
            locationFields = null,
            locationCtrl = $(this).parent('.msp-ctrl-location'),
            options = { IsEditMode: false };

        parentForm = locationCtrl.closest('.page-form.show');
        locationFields = locationCtrl[0].field.Schema.LocationFileds;
        if (parentForm && parentForm[0].PageForm) {
            if (parentForm[0].PageForm._id != null)
                options.IsEditMode = true;
        }
        options = {
            ClickRef: this,
            CallingControl: locationCtrl,
            callback: function (options) {

                if (options.NewAddress) {
                    if (locationFields) {
                        if (parentForm.find('[data-field="' + locationFields.VenueName + '"]')) {
                            parentForm.find('[data-field="' + locationFields.VenueName + '"]').val(options.NewAddress.venuename);
                        }
                        if (parentForm.find('[data-field="' + locationFields.Address + '"]')) {
                            parentForm.find('[data-field="' + locationFields.Address + '"]').val(options.NewAddress.address);
                        }
                        if (parentForm.find('[data-field="' + locationFields.City + '"]') && options.NewAddress.city) {
                            let param = {};
                            param.url = "/api/common/getcitysearch";
                            param.type = "POST";

                            param.data = {
                                City: options.NewAddress.city,
                                CountryID: options.NewAddress.country,
                                State: options.NewAddress.state,
                                IsCreateNew: true
                            };
                            _common.FetchApiByParam(param, function (result) {
                                if (result.status == "ok") {
                                    if (result.data) {
                                        let cityVal = '';
                                        if (result.data.CityID) {
                                            cityVal = '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' + result.data.CityID + '"><span class="city-name">' + result.data.CityName + '</span><i class="fas fa-times"></i></div>';
                                        }
                                        parentForm.find('[data-field="' + locationFields.City + '"]').html(cityVal);
                                    }
                                }
                            });
                        }
                        else if (parentForm.find('[data-field="' + locationFields.City + '"]')) {
                            if (options.NewAddress.cityHTML)
                                parentForm.find('[data-field="' + locationFields.City + '"]').html(options.NewAddress.cityHTML);
                            else {
                                parentForm.find('[data-field="' + locationFields.City + '"]').html('')
                                parentForm.find('[data-field="' + locationFields.City + '"]').attr('contenteditable', 'true')
                            }


                        }
                        if (parentForm.find('[data-field="' + locationFields.State + '"]')) {
                            parentForm.find('[data-field="' + locationFields.State + '"]').val(options.NewAddress.state);
                        }
                        if (parentForm.find('[data-field="' + locationFields.Country + '"]')) {
                            parentForm.find('[data-field="' + locationFields.Country + '"]').val(options.NewAddress.country);
                        }
                        if (parentForm.find('[data-field="' + locationFields.PostalCode + '"]')) {
                            parentForm.find('[data-field="' + locationFields.PostalCode + '"]').val(options.NewAddress.postalcode);
                        }
                        locationCtrl.attr('data-lat', options.NewAddress.lat);
                        locationCtrl.attr('data-lng', options.NewAddress.lng);
                    }
                } else {
                    swal('Address not available', { icon: "info", });
                }
            }
        };
        GoogleMaps.GoogleAutoComplete(options)
    });
    $(document).on('click', '.msp-ctrl-location .btnDetailLocateOnMap', function () {
        var lat = parseFloat($(this).attr("data-lat"));
        var lng = parseFloat($(this).attr("data-lng"));
        console.log(lat + " " + lng);
        options = {
            MapBoxTitle: "",
            lat: lat,
            lng: lng,
            zoomLevel: 16,
            IsRemoveBtnsaveButton: true
        };

        GoogleMaps.LocateOnMap(options);

    });
    $(document).on('click', '.msp-ctrl-location .btnLocateOnMap', function () {
        let parentorm = null,
            locationFields = null,
            options = {},
            locationCtrl = $(this).parent('.msp-ctrl-location');

        parentForm = locationCtrl.closest('.page-form.show');
        locationFields = locationCtrl[0].field.Schema.LocationFileds;
        options = {
            ClickRef: this,
            address: "",
            city: "",
            state: "",
            country: "",
            postalcode: "",
            IsLocateOnMap: true,
            lat: parseFloat(locationCtrl.attr('data-lat')),
            lng: parseFloat(locationCtrl.attr('data-lng')),
            CallingControl: locationCtrl,
            ShowSearchBox: false
        };
        if (locationFields) {
            let addressField = parentForm.find('[data-field="' + locationFields.Address + '"]:visible');
            if (addressField) {
                if (addressField.attr('data-required') != undefined && addressField.val() == "") {
                    swal('Please fill the address.', { icon: "info", });
                    return;
                }
                else {
                    options.address = parentForm.find('[data-field="' + locationFields.Address + '"]:visible').val();
                }
            }
            let cityField = parentForm.find('[data-field="' + locationFields.City + '"]:visible');
            if (cityField) {
                let cityFieldVal = cityField.find('.city-name').text();
                if (cityField.attr('data-required') != undefined && cityFieldVal == "") {
                    swal('Please fill the city.', { icon: "info", });
                    return;
                } else {
                    options.city = cityFieldVal;
                }
            }
            let stateField = parentForm.find('[data-field="' + locationFields.State + '"]:visible');
            if (stateField) {
                if (stateField.attr('data-required') != undefined && stateField.val() == "") {
                    swal('Please fill the state.', { icon: "info", });
                    return;
                } else {
                    options.state = stateField.val();
                }
            }
            let countryField = parentForm.find('[data-field="' + locationFields.Country + '"]:visible');
            if (countryField) {
                if (countryField.attr('data-required') != undefined && countryField.find('option:selected').text() != "") {
                    swal('Please select the country.', { icon: "info", });
                    return;
                } else {
                    options.country = countryField.find('option:selected').text();
                }
            }
            let postalcodeField = parentForm.find('[data-field="' + locationFields.PostalCode + '"]:visible');
            if (postalcodeField) {
                if (postalcodeField.attr('data-required') != undefined && postalcodeField.val() != "") {
                    swal('Please fill the postalcode.', { icon: "info", });
                    return;
                } else {
                    options.postalcode = postalcodeField.val();
                }
            }
        }
        $(this).attr('disabled', 'disabled');
        GoogleMaps.GetLatLongByAddress(options);
    });
});

var _controls = {};

_controls.BuildSelectOptions = function (listArr, value, displayValue) {
    let result = '';
    if (listArr.length) {
        for (let i = 0; i < listArr.length; i++) {
            result += '<option value="' + listArr[i][value] + '">' + listArr[i][displayValue] + '</option>';
        }
    }
    return result;
}

_controls.isValidDate = function (d) {
    return d instanceof Date && !isNaN(d);
}
_controls.GetPopup = function (el) {
    if ($('.msp-popup').length > 0) {
        $('.msp-popup').remove();
    }

    let left = 0;
    if (el.prev('.input-group-prepend').length > 0) {
        left = el.prev('.input-group-prepend').outerWidth();
    }
    let top = el.outerHeight();


    let ctrl = $('<div contenteditable="false" class="border bg-white msp-popup shadow-lg rounded-bottom" style="position:absolute;z-index:1060;visibility:hidden"></div>').css({ top: top, left: left });
    el.after(ctrl);
    setTimeout(function () {
        let modalBody = el.closest('.modal-body');
        if (modalBody.length > 0) {
            if ((modalBody.offset().top + modalBody.height()) <= (el.offset().top + el.outerHeight() + ctrl.outerHeight())) {
                if ((el.offset().top - modalBody.offset().top) > ctrl.outerHeight()) {
                    let newTop = -1 * (ctrl.outerHeight());
                    ctrl.css({ top: newTop });
                }

            }
            if (modalBody.offset().left + modalBody.width() <= (el.offset().left + ctrl.outerWidth())) {
                ctrl.css({ right: 0, left: 'unset' });
            }
        }
        ctrl.css('visibility', 'visible');
    }, 200);
    return ctrl;
}
_controls.DatePicker = function (options) {
    let currentDate = new Date(moment(_initData.CurrentUtcTime).tz(_initData.User.MySettings.Timezone).format("YYYY-MM-DDTHH:mm:ss"));
    let ctrl;
    if (!options) {
        options = {};
    }
    if (options.el) {
        if (options.display == 'inline' || options.el.attr('data-display') == 'inline') {
            ctrl = options.el;
        }
        else {
            $('.msp-popup').remove();
            var el = options.el[0];
            if (el.ctrl) {
                if (el.ctrl.length > 0) {
                    ctrl = el.ctrl;
                }
            }
            ctrl = _controls.GetPopup($(el));
            options.input = $(el);
            options.selectedDate = moment($(el).val(), [_initData.Company.Setting.DateFormat]).toDate();
            if (_controls.isValidDate(options.selectedDate)) {
                currentDate = options.selectedDate;
            }
            el.ctrl = ctrl;
        }
    }
    let ctrlHTM = '<div class="msp-date-picker">';
    ctrlHTM += '<div class="row"><h5 class="col-auto monthYear"></h5>';
    ctrlHTM += '<div class="col text-right"><i class="fas fa-angle-left btn p-1" data-toggle="tooltip" title="Previous Month"></i> <i class="fas fa-angle-right btn p-1" data-toggle="tooltip" title="Next Month"></i></div></div>';
    ctrlHTM += '<div class="cal-body text-dark">';
    ctrlHTM += '</div></div></div>';
    ctrl.html(ctrlHTM);
    let datePicker = ctrl.find('.msp-date-picker');
    datePicker[0].currentYear = currentDate.getFullYear();
    datePicker[0].currentMonth = currentDate.getMonth();
    datePicker[0].currentDate = currentDate;
    datePicker[0].options = options;
    _controls.DatePicker.Build(datePicker);
}

_controls.DatePicker.Build = function (datePicker) {
    datePicker.find('.monthYear').html(moment(new Date(datePicker[0].currentYear, datePicker[0].currentMonth)).format('MMM YYYY'));
    datePicker.find('.cal-body').html(_calendar.GetMonthView(datePicker[0].currentMonth, datePicker[0].currentYear, 'vshort'));
    datePicker.find('.cal-body [data-date="' + moment(datePicker[0].currentDate).format('YYYY-MM-DD') + '"]').addClass('item-selected').addClass('rounded-circle');
    datePicker.find('.cal-body [data-date="' + moment(new Date(moment(_initData.CurrentUtcTime).tz(_initData.User.MySettings.Timezone).format("YYYY-MM-DDTHH:mm:ss"))).format('YYYY-MM-DD') + '"]').addClass('current-day').addClass('rounded-circle');
    if (datePicker[0].options.onCalenderBuild) {
        datePicker[0].options.onCalenderBuild(datePicker);
    }
}

_controls.TimePicker = function (el) {
    // consider both Format, intervel
    let hrs = new Array();
    var intervel = 60;
    if (el.attr("data-interval")) {
        var iv = parseInt(el.attr("data-interval"));
        if (iv < 60 && iv > 0 && iv != NaN) {
            intervel = iv;
        }
    }
    var parts = parseInt(60 / intervel);
    for (let i = 0; i < parts; i++) {
        if (i == 0) {
            hrs.push('00')
        } else {
            var time = 60 * i / parts;
            if (time < 10) {
                hrs.push('0' + time);
            } else {
                hrs.push('' + time);
            }
        }
    }
    var ctrlHTM = '<div class="msp-time-picker" style="width:6rem;height:150px;overflow-y:auto">';
    for (let i = 0; i < 24; i++) {
        var t = i;
        var tt = '';
        let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
        if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
            calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
        }
        if (calendarTimeFormat == 'hh:mm A') {
            if (i > 12) {
                t = i - 12;
                tt = ' PM';
            } else {
                tt = ' AM';
                if (i == 12) {
                    tt = ' PM';
                }
            }
        }
        var gtime = '' + t;
        var stgtime = '' + i;
        if (t < 10) {
            gtime = '0' + t;
        }
        if (i < 10)
            stgtime = '0' + i;
        for (let j = 0; j < hrs.length; j++) {
            var stTime = gtime + ':' + hrs[j] + tt;
            var stdTime = stgtime + ':' + hrs[j];
            if (i == 0 && tt == ' AM') {
                stTime = '12:' + hrs[j] + tt;
            }
            ctrlHTM += '<div class="item" data-time="' + stdTime + '">' + stTime + '</div>';
        }
    }
    ctrlHTM += '</div>';
    return ctrlHTM;
}
_controls.BindNavKeys = function (options) {
    var e = options.event;
    var el = options.element;
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 40) {
        if (el.popup && el.popup.length > 0) {
            popup = el.popup;
            var sel = popup.find('.item-selected');
            if (sel.length > 0) {
                sel.removeClass('item-selected')
                sel.next().addClass('item-selected')
            } else {
                popup.find('.item').first().addClass('item-selected')
            }
            if (options.onSelect) { options.onSelect(popup.find('.item.item-selected')) }
        }
        return true;
    } else if (code == 38) {
        popup = el.popup;
        var sel = popup.find('.item.item-selected');
        if (sel.length > 0) {
            sel.removeClass('item-selected')
            sel.prev().addClass('item-selected')
        } else {
            popup.find('.item').first().addClass('item-selected')
        }
        if (options.onSelect) { options.onSelect(popup.find('.item.item-selected')) }
        return true;
    } else if (code == 13) {
        if (options.onEnter) { options.onEnter(popup.find('.item.item-selected')) };
        popup.remove();
        return true;
    }
}
_controls.CheckNavKey = function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 40 || code == 38 || code == 13) { return true; }
    return false;
}
_controls.SetLookupValue = function (el) {
    var ctrl = el.closest('.ctrl-container');
    if (ctrl && ctrl.length) {
        var cHtm = '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' + el.attr('data-id') + '">';
        cHtm += el.html();
        cHtm += '<i class="fas fa-times"></i></div>';
        ctrl.find('.msp-ctrl').html(cHtm).attr('contenteditable', 'false');
        ctrl.find('.msp-ctrl').trigger("onLookupChange", el);
    }
}
_controls.SetMultiLookupValue = function (el) {
    var ctrl = el.closest('.ctrl-container');
    if (ctrl && ctrl.length) {
        var cHtm = '<div tabindex="0" class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1 " contenteditable="false" data-id="' + el.attr('data-id') + '">';
        cHtm += el.html();
        cHtm += '<i class="fas fa-times pl-2"></i></div>';
        ctrl.find('input').val('').focus().before(cHtm);
        ctrl.find('.msp-ctrl').trigger("onLookupChange", el);
    }
}
//Set Tag Value to a multilookup field
_controls.SetTagValue = function (el) {
    var ctrl = el.closest('.ctrl-container');
    var tagid = el.attr('data-id');
    var tagObject = el.attr('data-tagobject');
    let fieldName = ctrl.find('.msp-ctrl').attr('data-field');
    let objectName = ctrl.find('.msp-ctrl').attr('data-object');

    let lookupfields = ctrl.find('.msp-ctrl').attr('data-lookupfields')
    let lookupformat = ctrl.find('.msp-ctrl').attr('data-lookupformat')
    let displayConcatArry = [];


    if (lookupformat != 'undefined') {
        let str = lookupformat;
        let regex = /{{|}}/;
        let displayElem = str.split('<span');
        let concatArray = displayElem[0].split(regex);
        for (let i = 0; i < concatArray.length; i++) {
            if (concatArray[i].charAt(0) == '$')
                displayConcatArry.push(concatArray[i].slice(1));
        }
    } else {
        let str1 = lookupfields;
        displayConcatArry = str1.split(',');
    }

    if (fieldName == 'Attendees') {
        displayConcatArry = [];
        displayConcatArry.push('FirstName');
        displayConcatArry.push('LastName');
    }
    _common.ContainerLoading(true, ctrl);


    _tag.GetTagData(tagid, tagObject, fieldName, objectName, function (resultData) {
        console.log(resultData);
        if (resultData && resultData.length) {
            let data = resultData;
            for (let i = 0; i < data.length; i++) {
                const item = data[i];
                if (ctrl && ctrl.length) {
                    if (ctrl.find('[data-id=' + item._id + ']').length == 0) {
                        var cHtm = '<div tabindex="0" class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1 " contenteditable="false" data-id="' + item._id + '">';
                        cHtm += item.name;
                        cHtm += '<i class="fas fa-times pl-2"></i></div>';
                        ctrl.find('input').val('').focus().before(cHtm);
                        ctrl.find('.msp-ctrl').trigger("onLookupChange", el);
                    }
                }
            }
            _common.ContainerLoading(false, ctrl);
        }
        else {
            _common.ContainerLoading(false, ctrl);
            alert('No Item attahced to this Tag')
        }
    });
}

_controls.AttachFile = function (el, value) {
    let formData = new FormData(el.closest('form')[0])

    let objectProperties = {
        ObjectName: "",
        ObjectFieldName: "",
        ObjectDocumentID: ""
    };
    let fieldDetails = el[0].field;
    if (fieldDetails) {
        objectProperties.ObjectName = fieldDetails.ObjectName;
        objectProperties.ObjectFieldName = fieldDetails.Name;
        let parentContainer = el.closest('.page-form');
        if (parentContainer.length > 0 && parentContainer[0].PageForm) {
            objectProperties.ObjectDocumentID = parentContainer[0].PageForm._id;
        } else {
            parentContainer = el.closest('.page-content');
            if (parentContainer[0].Page && parentContainer[0].Page.param)
                objectProperties.ObjectDocumentID = parentContainer[0].Page.param._id;
        }
        objectProperties.ObjectDocumentID = (objectProperties.ObjectDocumentID || "");
    }
    let file = value.target.files[0];
    var element = el.next();
    objectProperties.isActive = false
    element.show().html("<label class='file-data' data-filename='" + file.name + "'> Attaching " + file.name + "</label><i class='fas fa-times'></i><br><progress class='uploading-progress-bar' max='100'></progress>");
    _common.Loading(true)
    _common.UploadFormFile(element, objectProperties, formData, function (val) {
        element.find('.file-data').attr('data-fileid', val);
        element.find('.uploading-progress-bar').hide();
        element.find('.file-data').html(file.name + ` <a href="/api/drive/download/${val}/${file.name}/document" target="_blank"  ><i class='fas fa-download'></i></a>`)
        _common.Loading(false)
    })
}

_controls.ToEJSON = function (UIDataType, val) {
    switch (UIDataType) {
        case "int":
            val = { $numberLong: val }
            break;
        case "decimal":
            val = { $numberDecimal: val }
            break;
        case "lookup":
        case "file":
        case "objectid":
        case "image":
            val = { $oid: val }
            break;
        case "multilookup":
            if (val.length > 0) {
                for (let i = 0; i < val.length; i++) {
                    if (val[i])
                        val[i] = { $oid: val[i] };
                }
            }
            break;
        case "date":
        case "datetime":
            val = { $date: val }
            break;
    }
    return val;
}
/**
 * 
 * @param {datatype} UIDataType 
 * @param {value} val
 * 
 * Hamza : 17July2020: Pass EJSON value and returns Normal Value. 
 */
_controls.ReverseEJSON = function (UIDataType, val) {
    switch (UIDataType) {
        case "int":
            if (val.$numberLong != undefined)
                val = val.$numberLong;
            break;
        case "decimal":
            if (val.$numberDecimal != undefined)
                val = val.$numberDecimal;
            break;
        case "lookup":
        case "file":
        case "objectid":
        case "image":
            if (val.$oid != undefined)
                val = val.$oid;
            break;
        case "multilookup":
            let valArray = [];
            if (val != undefined && val.length > 0) {
                for (let i = 0; i < val.length; i++) {
                    if (val[i].$oid != undefined)
                        valArray.push(val[i].$oid);
                    else
                        valArray.push(val[i])
                }
            }
            val = valArray
            break;
        case "date":
        case "datetime":
            if (val.$date != undefined)
                val = val.$date;
            break;
    }
    return val;
}

_controls.ToEPARSE = function (val) {
    let valArray = [];
    if (val != undefined && val.length > 0) {
        for (let i = 0; i < val.length; i++) {
            if (val[i].$oid != undefined)
                valArray.push(val[i].$oid);
            else
                valArray.push(val[i])
        }
    }
    return valArray;
}

_controls.ParseValue = function (UIDataType, val) {
    let result = val;
    if (val) {
        switch (UIDataType) {
            case "int":
                result = parseInt(val);
                break;
            case "decimal":
                result = parseFloat(val);
                break;
            case "date":
            case "datetime":
                result = Date.parse(val);
                break;
        }
    }
    return result;
}

_controls.IsValidDataType = function (UIDataType, val) {
    let result = true;
    switch (UIDataType) {
        case "int":
            result = (parseInt(val) == val);
            break;
        case "decimal":
            result = (parseFloat(val) == val);
            break;
        case "date":
        case "datetime":
            result = (Date.parse(val) ? true : false);
            break;
    }
    return result;
}

_controls.MultiSelect = function (ctrl, field) {
    let enableFilter = true;
    let selectAll = true;

    if (field.EnableFilter !== undefined)
        enableFilter = field.EnableFilter;
    else if (field.Schema.EnableFilter !== undefined)
        enableFilter = field.Schema.EnableFilter;

    if (field.SelectAll !== undefined)
        selectAll = field.SelectAll;
    else if (field.Schema.SelectAll !== undefined)
        selectAll = field.Schema.SelectAll;

    ctrl.multiselect({
        nonSelectedText: '',
        enableCaseInsensitiveFiltering: enableFilter,
        includeSelectAllOption: selectAll,
        onDropdownShow: function (event) {
            ctrl.closest('.ctrl-container').find('.btn.multiselect-clear-filter').trigger('click');
        }
    });
}

_controls.SingleSelect = function (ctrl, field) {
    let enableFilter = true;
    if (field.EnableFilter !== undefined)
        enableFilter = field.EnableFilter;
    else if (field.Schema.EnableFilter !== undefined)
        enableFilter = field.Schema.EnableFilter;

    ctrl.chosen({ width: '100%' });
}

//get data for display to user
_controls.GetDisplayValue = function (params) {
    let field = params.field;
    let group = params.group;

    let result = '';
    let record = (group && group.IsRealLookup) ? params.data[group.Name] : params.data;
    if (record === undefined || record === null) {
        return '';
    }

    let fieldRecord = '';
    if (field.Schema.IsNestedObject == true && !_common.isEmptyObj(record[field.Name])) {
        fieldRecord = record[field.Name][field.ChildPropertyName];
    } else {
        fieldRecord = record[field.Name];
    }

    if (fieldRecord == undefined) {
        if (field.Schema.UIDataType == 'checkbox')
            return '<img src="/images/unchecked.svg" />';
        else
            return '';
    }
    let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
    if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
        calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
    }

    switch (field.Schema.UIDataType) {
        case "lookup":
            if (record[field.Name + _constantClient.LookupAlias]) {
                let sept = ' ';
                let lookupRecord = record[field.Name + _constantClient.LookupAlias];
                let name = '';
                let classes = 'lookup-display-record';
                let isHyperlink = true;
                if (field.IsHyperlink != undefined)
                    isHyperlink = field.IsHyperlink;
                else if (field.Schema.IsHyperlink != undefined)
                    isHyperlink = field.Schema.IsHyperlink;

                if (isHyperlink === false) {
                    classes = '';
                }

                if (field.Schema.LookupFormat) {
                    name = field.Schema.LookupFormat;
                    for (let j = 0; j < field.Schema.LookupFields.length; j++) {
                        name = name.replace('{{$' + field.Schema.LookupFields[j] + '}}', _common.GetObjectByString(lookupRecord, field.Schema.LookupFields[j]));
                    }
                }
                else {
                    for (let j = 0; j < field.Schema.LookupFields.length; j++) {
                        if (j == field.Schema.LookupFields.length - 1)
                            sept = '';
                        name += _common.GetObjectByString(lookupRecord, field.Schema.LookupFields[j]) + sept;
                    };
                }
                if (field.Schema.LookupObject == "User") {
                    if (lookupRecord.UserStatus) {
                        if (lookupRecord.UserStatus == _constantClient.USER_STATUS.SUSPEND)
                            classes += ' suspended-user';
                        else if (lookupRecord.UserStatus == _constantClient.USER_STATUS.REPLACE)
                            classes += ' replaced-user';
                    }
                }
                if (lookupRecord.IsActive === false) {
                    classes += ' deleted-record';
                }
                result = '<span class="' + classes + '" data-objname="' + field.Schema.LookupObject + '" data-objid="' + lookupRecord._id + '">' + name + '</span>';
            }
            break;
        case "multilookup":
            if (record[field.Name + _constantClient.LookupAlias] && record[field.Name + _constantClient.LookupAlias].length > 0) {
                for (let i = 0; i < record[field.Name + _constantClient.LookupAlias].length; i++) {
                    let lookupRecord = record[field.Name + _constantClient.LookupAlias][i];
                    let sept = ' ';
                    let name = '';
                    let classes = 'lookup-display-record';
                    let isHyperlink = true;
                    if (field.Schema.IsHyperlink != undefined)
                        isHyperlink = field.Schema.IsHyperlink;
                    if (field.IsHyperlink != undefined)
                        isHyperlink = field.IsHyperlink;
                    if (isHyperlink === false) {
                        classes = '';
                    }

                    if (field.Schema.LookupFormat) {
                        name = field.Schema.LookupFormat;
                        for (let j = 0; j < field.Schema.LookupFields.length; j++) {
                            name = name.replace('{{$' + field.Schema.LookupFields[j] + '}}', _common.GetObjectByString(lookupRecord, field.Schema.LookupFields[j]));
                        }
                    }
                    else {
                        for (let j = 0; j < field.Schema.LookupFields.length; j++) {
                            if (j == field.Schema.LookupFields.length - 1)
                                sept = '';

                            name += _common.GetObjectByString(lookupRecord, field.Schema.LookupFields[j]) + sept;
                        };
                    }
                    if (field.Schema.LookupObject == "User") {
                        if (lookupRecord.UserStatus) {
                            if (lookupRecord.UserStatus == _constantClient.USER_STATUS.SUSPEND)
                                classes += ' suspended-user';
                            else if (lookupRecord.UserStatus == _constantClient.USER_STATUS.REPLACE)
                                classes += ' replaced-user';
                        }
                    }
                    if (lookupRecord.IsActive === false) {
                        classes += ' deleted-record';
                    }
                    result += '<span class="' + classes + '" data-objname="' + field.Schema.LookupObject + '" data-objid="' + lookupRecord._id + '">' + name + '</span>, ';
                }
                result = result.slice(0, -2);
            }
            break;
        case "dropdown":
            if (record[field.Name + _constantClient.LookupAlias] && record[field.Name + _constantClient.LookupAlias].IsActive) {
                if (field.Schema.LookupObject) {
                    for (let i = 0; i < field.Schema.LookupFields.length; i++) {
                        let val = record[field.Name + _constantClient.LookupAlias][field.Schema.LookupFields[i]];
                        if (val)
                            result += val + ' ';
                    }
                    result = result.trim();
                }
                else
                    result = record[field.Name + _constantClient.LookupAlias].Value;
            }
            break;
        case "multiselect":
            if (record[field.Name + _constantClient.LookupAlias] && record[field.Name + _constantClient.LookupAlias].length > 0) {
                let listArr = record[field.Name + _constantClient.LookupAlias];
                for (let i = 0; i < listArr.length; i++) {
                    let obj = listArr[i];
                    if (obj.IsActive) {
                        if (field.Schema.LookupObject) {
                            let tempVal = '';
                            for (let j = 0; j < field.Schema.LookupFields.length; j++) {
                                let val = obj[field.Schema.LookupFields[j]];
                                if (val)
                                    tempVal += val + ' ';
                            }
                            tempVal = tempVal.trim();
                            result += tempVal + ', ';
                        }
                        else
                            result += obj.Value + ', ';
                    }
                }
                if (result != '')
                    result = result.slice(0, -2);
            }
            break;
        case "decimal":
            if (fieldRecord.$numberDecimal != undefined)
                result = fieldRecord.$numberDecimal;
            break;
        case "checkbox":
            result = (fieldRecord == true) ? '<img src="/images/checked.svg" />' : '<img src="/images/unchecked.svg" />';
            break;
        case "date":
            if (fieldRecord)
                result = moment(fieldRecord).format(_initData.Company.Setting.DateFormat);
            break;
        case "time":
            result = moment(fieldRecord, 'HH:mm').format(calendarTimeFormat);
            break;
        case "datetime":
            var format = _initData.Company.Setting.DateFormat + ' ' + calendarTimeFormat;

            let tz = _initData.User.MySettings.Timezone;
            if (record[field.Schema.TimezoneField])
                tz = record[field.Schema.TimezoneField]

            result = moment(fieldRecord).tz(tz).format(format);
            break;
        case "file":
            if (record[field.Name + _constantClient.LookupAlias] && record[field.Name + _constantClient.LookupAlias].IsActive) {
                result = record[field.Name + _constantClient.LookupAlias].FileName + ` <a href="/api/drive/download/${record[field.Name + _constantClient.LookupAlias]._id}/${record[field.Name + _constantClient.LookupAlias].FileName}/document" target="_blank"  ><i class='fas fa-download'></i></a>`;
            }
            break;
        case "email":
            //<i class="email-icon fas fa-envelope fa-fw"></i>
            result = '<span class="email-section"><span class="email" >' + fieldRecord + '</span></span>';
            break;
        case "url":
            result = '<a target="_blank" href="' + _common.Addhttp(fieldRecord) + '">' + fieldRecord + '</a>';
            break;
        case "image":
            let imageExtension = "";
            if (record[field.Name + _constantClient.LookupAlias] && record[field.Name + _constantClient.LookupAlias].IsActive) {
                let imageDetails = record[field.Name + _constantClient.LookupAlias];
                if (imageDetails && imageDetails.FileName) {
                    imageExtension = imageDetails.FileName;
                    let index = imageExtension.lastIndexOf(".");
                    imageExtension = imageExtension.substring(index);
                }
            }
            result = '<div class="msp-ctrl-image" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" data-height="' + field.Height + '" data-width="' + field.Width + '" style="height:' + field.Height + 'px; width:' + field.Width + 'px;">';
            if (fieldRecord)
                result += '<img src="' + _constantClient.CDN + '/public/' + _initData.Company._id + '/' + fieldRecord + '"/>';
            else
                result += '<img src="images/nouser.jpg?ref=1"/>';
            result += '</div>';
            break;
        case "location":
            //lat lng available or not
            //Locate on map isRemoved or not
            let latlngObj = fieldRecord;
            if (latlngObj.lat && latlngObj.lng) {
                // result = '<div data-lat="" data-lng="" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="fade msp-ctrl msp-ctrl-location" title="Locate On Map">';
                // let src = "https://maps.googleapis.com/maps/api/staticmap?center=##lat##,##lng##&zoom=##zoom##&size=##width##x##height##&maptype=roadmap&markers=color:red%7Clabel:A%7C##mlat##,##mlng##&key=##mapkey##";
                // src = src.replace('##mapkey##', _common.GetMapKey());
                // src = src.replace('##lat##', latlngObj.lat);
                // src = src.replace('##lng##', latlngObj.lng);
                // src = src.replace('##mlat##', latlngObj.lat);
                // src = src.replace('##mlng##', latlngObj.lng);
                // src = src.replace('##zoom##', "10");
                // src = src.replace('##width##', "");
                // src = src.replace('##height##', "");
                // result += '<img src="' + src + '"  height="" width:"" style="display: block;" />';
                // result += '</div>';
                result = '<div  data-field="" data-ui="" class="msp-ctrl msp-ctrl-location">\
                <button data-lat="'+ latlngObj.lat + '" data-lng="' + latlngObj.lng + '" class="btn btn-sm btn-outline-primary btnDetailLocateOnMap">Show On Map</button>\
                </div>';
            }
            else {
                result = "";
            }
            break;
        case "collection":
            result = '';
            break;
        default:
            if (fieldRecord != null)
                result = fieldRecord;
            break;
    }

    if (fieldRecord != null) {
        if (field.Schema.IsCurrency == true)
            result = _common.GetCompanyCurrencySymbol() + result;
        else if (field.Schema.IsPercent == true)
            result = result + '%';
    }


    return result;
}

//get data for saving in db
_controls.GetCtrlValue = function (params) {
    let field = params.field;
    let panel = params.container;
    let isEjsonFormat = params.isEjsonFormat;
    let val;
    let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
    if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
        calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
    }

    let objectDataType = field.Schema.UIDataType;

    switch (field.Schema.UIDataType) {
        case "text":
        case "int":
        case "decimal":
        case "email":
        case "phone":
        case "url":
        case "textarea":
        case "objectid":
            val = panel.find('.msp-ctrl[data-field="' + field.Name + '"]').val();
            break;
        case "texteditor":
            val = panel.find('.msp-ctrl[data-field="' + field.Name + '"]').find('.texteditor').html();
            break;
        case "checkbox":
            val = panel.find('input[data-field="' + field.Name + '"]').prop('checked');
            break;
        case "dropdown":
            if (field.Schema.LookupObject)
                objectDataType = "objectid";
            val = panel.find('.msp-ctrl[data-field="' + field.Name + '"]').val();
            break;
        case "multiselect":
            if (field.Schema.LookupObject)
                objectDataType = "multilookup";
            val = panel.find('select[data-field="' + field.Name + '"]').val();
            if (val.length == 0) {
                val = null;
            }
            break;
        case "lookup":
            val = panel.find('.msp-ctrl[data-field="' + field.Name + '"]').find('.lookup-item').data('id');
            break;
        case "multilookup":
            val = [];
            panel.find('.msp-ctrl[data-field="' + field.Name + '"]').find('.lookup-item').each(function () {
                val.push($(this).data('id'));
            })
            if (val.length == 0) {
                val = null;
            }
            break;
        case "date":
            val = panel.find('input[data-field="' + field.Name + '"]').val();
            if (val)
                val = moment.utc(val, _initData.Company.Setting.DateFormat).format();
            break;
        case "time":
            val = panel.find('input[data-field="' + field.Name + '"]').attr('data-time');
            break;
        case "datetime":
            val = panel.find('.msp-ctrl-date[data-field="' + field.Name + '"]').val() + ' ' + panel.find('.msp-ctrl-time[data-field="' + field.Name + '"]').val();
            var format = _initData.Company.Setting.DateFormat + ' ' + calendarTimeFormat;
            val = _common.ConvertDateByTimezone(panel, val, format);
            break;
        case "file":
            val = panel.find('label[data-field="' + field.Name + '"]').next('.div-file-uploading').find('.file-data').data('fileid');
            break;
        case "image":
            val = panel.find('.msp-ctrl-image[data-field="' + field.Name + '"]').attr('data-imageid');
            break;
        case "location":
            val = {
                lat: panel.find('.msp-ctrl-location[data-field="' + field.Name + '"]').attr('data-lat'),
                lng: panel.find('.msp-ctrl-location[data-field="' + field.Name + '"]').attr('data-lng'),
            };
            break;
        case "collection":
            val = [];
            if (field.Schema.Fields) {
                let fields = field.Schema.Fields
                panel.find('.msp-ctrl-collection[data-field="' + field.Name + '"]').find('>.msp-coll-frm').each(function () {
                    var itemObj = {};
                    itemObj._id = '$id';
                    for (let i = 0; i < fields.length; i++) {
                        let childField = $.extend({}, fields[i]);
                        childField.Schema = fields[i];
                        itemObj[fields[i].Name] = _controls.GetCtrlValue({ field: childField, container: $(this), isEjsonFormat: true });

                    }
                    val.push(itemObj);
                });
            }
            break;
    }

    if (val === '' || val === undefined) {
        val = null;
    }

    if (val !== null && isEjsonFormat == true) {
        val = _controls.ToEJSON(objectDataType, val);
    }

    return val;
}

_controls.GetCtrlDisplayValue = function (field, panel) {
    let val;

    switch (field.Schema.UIDataType) {
        case "text":
        case "int":
        case "decimal":
        case "email":
        case "phone":
        case "url":
        case "textarea":
        case "dropdown":
        case "objectid":
        case "date":
        case "time":
            val = panel.find('.msp-ctrl[data-field="' + field.Name + '"]').val();
            break;
        case "texteditor":
            val = panel.find('.msp-ctrl[data-field="' + field.Name + '"]').find('.texteditor').html();
            break;
        case "checkbox":
            val = panel.find('input[data-field="' + field.Name + '"]').prop('checked');
            break;
        case "multiselect":
            val = panel.find('select[data-field="' + field.Name + '"]').val();
            if (val.length == 0) {
                val = null;
            }
            break;
        case "lookup":
        case "multilookup":
            val = panel.find('.msp-ctrl[data-field="' + field.Name + '"]').find('.lookup-item').text();
            break;

        case "datetime":
            val = panel.find('.msp-ctrl-date[data-field="' + field.Name + '"]').val() + ' ' + panel.find('.msp-ctrl-time[data-field="' + field.Name + '"]').val();
            break;
    }

    if (val === '' || val === undefined) {
        val = null;
    }
    return val;
}

//build empty control
_controls.BuildCtrl = function (params) {
    let field = params.field;
    let pageData = params.pageData;
    let groupData = params.groupData;
    let isForEdit = (params.isForEdit != undefined) ? params.isForEdit : false;

    let title = _page.GetFieldDisplayName(field);
    if (field.Title)
        title = field.Title;

    let cls = "msp-ctrl-" + field.Schema.UIDataType;
    if (field.Class != undefined)
        cls += ' ' + field.Class;
    else if (field.Schema.Class != undefined)
        cls += ' ' + field.Schema.Class;

    let attr = '';
    if (field.Attribute != undefined)
        attr += ' ' + field.Attribute;
    else if (field.Schema.Attribute != undefined)
        attr += ' ' + field.Schema.Attribute;


    if (field.Schema.MinValue != undefined)
        attr += ' min-value="' + field.Schema.MinValue + '" ';
    if (field.Schema.MaxValue != undefined)
        attr += ' max-value="' + field.Schema.MaxValue + '" ';
    if (field.Schema.MaxLength != undefined)
        attr += ' max-length="' + field.Schema.MaxLength + '" ';

    let req = (field.Schema.IsRequired) ? ' data-required ' : '';
    if (field.IsRequired != undefined)
        req = (field.IsRequired) ? ' data-required ' : '';

    let ctrl;
    let option = '';

    switch (field.Schema.UIDataType) {
        case "text":
        case "int":
        case "decimal":
        case "email":
        case "phone":
        case "url":
            ctrl = $('<input type="text" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + attr + req + '>');
            break;
        case "textarea":
            ctrl = $('<textarea title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + attr + req + '></textarea>');
            break;
        case "texteditor":
            ctrl = $('<div title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl ' + cls + '" ' + attr + req + '>\
            '+ _constantClient.EDITOR_TOOL + '\
            <div class="texteditor" contenteditable="true" style="margin-top: 0px;  resize: vertical;\
            overflow: auto; margin-bottom: 0px; height: 70px;border:1px solid #d1d3e2"></div>\
            </div>');
            break;
        case "checkbox":
            ctrl = $('<label class="lblChk ctrl-container"><input type="checkbox" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl ' + cls + '" ' + attr + req + '><span class="checkmark"></span></label>');
            break;
        case "dropdown":
            if (field.Schema.LookupObject) {
                option = _controls.BuildSelectOptions(_common.GetDBListSchemaData(field.Schema), 'Key', 'Value');
            } else {
                option = _controls.BuildSelectOptions(_common.GetListSchemaDataFromInitData(field.Schema.ListSchemaName), 'Key', 'Value');
            }
            ctrl = $('<div class="ctrl-container"><select title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + attr + req + '><option></option>' + option + '</select></div>');
            //_controls.SingleSelect(ctrl.find('select'), field);
            break;
        case "multiselect":
            if (field.Schema.LookupObject) {
                option = _controls.BuildSelectOptions(_common.GetDBListSchemaData(field.Schema), 'Key', 'Value');
            } else {
                option = _controls.BuildSelectOptions(_common.GetListSchemaDataFromInitData(field.Schema.ListSchemaName), 'Key', 'Value');
            }
            ctrl = $('<div class="ctrl-container"><select multiple="multiple" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + attr + req + '>' + option + '</select></div>');
            _controls.MultiSelect(ctrl.find('select'), field);
            break;
        case "lookup":
            ctrl = $('<div class="input-group input-group-sm ctrl-container"><div class="input-group-prepend"><button class="btn btn-outline-primary btn-sm"><i class="fas fa-search"></i></button></div><div contenteditable="true" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl trigger-popup form-control form-control-sm ' + cls + '" ' + attr + req + '></div></div>');
            break;
        case "multilookup":
            ctrl = $('<div class="input-group input-group-sm align-items-stretch ctrl-container"><div class="input-group-prepend"><div class="btn btn-outline-primary btn-sm"><div class="pt-1"><i class="fas fa-search"></i></div></div></div><div tabindex="0" title="' + title + '" data-field="' + field.Name + '" data-object="' + field.ObjectName + '" data-ui="' + field.Schema.UIDataType + '" data-lookupformat="' + field.Schema.LookupFormat + '" data-lookupfields="' + field.Schema.LookupFields + '" class="msp-ctrl trigger-popup form-control form-control-sm ' + cls + '" ' + attr + req + '><input class="trigger-popup" /></div></div>');
            //ctrl[0] = field.Schema.LookupFormat;
            break;
        case "date":
            ctrl = $('<div class="input-group input-group-sm ctrl-container"><div class="input-group-prepend"><button class="btn btn-outline-primary btn-sm" tabindex="-1"><i class="fas fa-calendar-day"></i></button></div><input type="text" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + attr + req + '></div>');
            break;
        case "time":
            var dataInterval = '60';
            if (_initData.Company.Setting.CalendarTimeInterval) {
                dataInterval = _initData.Company.Setting.CalendarTimeInterval;
            }
            ctrl = $('<div class="input-group input-group-sm ctrl-container"><div class="input-group-prepend"><button class="btn btn-outline-primary btn-sm" tabindex="-1"><i class="fas fa-clock"></i></button></div><input type="text" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '"  class="msp-ctrl form-control form-control-sm ' + cls + '" ' + attr + req + '></div>');
            ctrl.find('.msp-ctrl-time').attr('data-interval', dataInterval);
            break;
        case "datetime":
            var dataInterval = '60';
            if (_initData.Company.Setting.CalendarTimeInterval) {
                dataInterval = _initData.Company.Setting.CalendarTimeInterval;
            }
            ctrl = $('<div class="row ctrl-container">'
                + '<div class="col-sm-7 col-xs-7 input-group input-group-sm"><div class="input-group-prepend"><button class="btn btn-outline-primary btn-sm" tabindex="-1"><i class="fas fa-calendar-day"></i></button></div><input type="text" title="' + title + '" class="msp-ctrl msp-ctrl-date form-control form-control-sm ' + cls + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" ' + attr + req + '></div>'
                + '<div class="col-sm-5 col-xs-5 input-group input-group-sm"><input type="text" title="' + title + '" class="msp-ctrl msp-ctrl-time trigger-popup form-control form-control-sm ' + cls + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" ' + attr + req + '></div>'
                + '</div>');
            ctrl.find('.msp-ctrl-time').attr('data-interval', dataInterval);
            break;
        case "file":
            ctrl = $('<form enctype="multipart/form-data" class="ctrl-container"><label title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" ' + attr + req + '><input type="file" name="file" style="visibility:hidden" ></label><div class="div-file-uploading "></div></form>');
            break;
        case "location":
            ctrl = $('<div data-lat="" data-lng="" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl msp-ctrl-location" ' + attr + req + '>\
            <button class="btn btn-sm btn-outline-primary btnLocateOnMap">Locate On Map</button>\
            <button class="btn btn-sm btn-outline-primary btnSearchAddress">Search Address</button>\
            </div>');
            if (field.LocateOnMap) {
                if (field.LocateOnMap.DisplayName)
                    ctrl.find('.btnLocateOnMap').html(field.LocateOnMap.DisplayName);
                if (field.LocateOnMap.Class)
                    ctrl.find('.btnLocateOnMap').removeClass("btn btn-sm btn-outline-primary").addClass(field.LocateOnMap.Class);
                if (field.LocateOnMap.IsRemove === true)
                    ctrl.find('.btnLocateOnMap').remove();
            }
            if (field.SearchAddress) {
                if (field.SearchAddress.DisplayName)
                    ctrl.find('.btnSearchAddress').html(field.SearchAddress.DisplayName);
                if (field.SearchAddress.Class)
                    ctrl.find('.btnSearchAddress').removeClass("btn btn-sm btn-outline-primary").addClass(field.SearchAddress.Class);
                if (field.SearchAddress.IsRemove === true)
                    ctrl.find('.btnSearchAddress').remove();
            }
            break;
        case "objectid":
            ctrl = $('<input type="text" title="' + title + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl form-control form-control-sm ' + cls + '" >');
            break;
        case "image":
            ctrl = $('<div class="edi-bx msp-ctrl msp-ctrl-image profile-pic" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" data-height="' + field.Height + '" data-width="' + field.Width + '" style="width:150px;">\
                     <form enctype="multipart/form-data"> \
                     <input type="file" name="file" style="display:none" >\
                    <img src="/images/bg.png" class="rounded" style="width:150px; height:150px;" onerror="this.src=\'images/bg.png\';"/>\
                    <div class="profile-img-edit">\
                    <i class="fa fa-edit msp-ctrl-image-edit btn btn-sm btn-primary" aria-hidden="true" title="edit"></i>\
                    <i class="fa fa-trash msp-ctrl-image-remove btn btn-sm btn-danger" aria-hidden="true" title="remove"></i>\
                    </div>\
                    <form>\
                    </div>');
            break;
        case "autoincrement":
            ctrl = $('<div data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '" class="msp-ctrl msp-ctrl-autoincrement" ' + attr + '></div>');
            break;
        case "collection":
            ctrl = $('<div class="pt-2 ' + cls + '" data-field="' + field.Name + '" data-ui="' + field.Schema.UIDataType + '"><button class="btn btn-sm btn-outline-primary btn-add float-left mt-n4" >Add ' + field.Schema.DisplayName + '</button></div>');
            let schema = $.extend({}, field.Schema);
            let btnAdd = ctrl.find('.btn-add');
            let readOnly = false;
            let data = undefined;
            if (field.Collections) {
                schema.Collections = field.Collections;
            } else {
                let col = $.extend({}, field.Schema);
                col.Fields = undefined;
                schema.Collections = [col];
            }
            schema.Schema = $.extend({}, field.Schema);
            // No need to create collection in new form.
            //     _controls.BuildCollectionControl(ctrl, schema, readOnly, data, pageData, groupData);
            btnAdd.click(function () {
                if (_common.Validate(ctrl)) {
                    ctrl.find('.msp-coll-frm').each(function () {
                        _controls.BuildCrtlCollectionLabel($(this), schema);
                    })
                    ctrl.find('.msp-coll-lbl').show();
                    ctrl.find('.msp-coll-row').hide();
                    _controls.BuildCollectionControl(ctrl, schema, readOnly, data, pageData, groupData);
                }
            });

            break;

    }

    if (ctrl == undefined)
        return ctrl;

    ctrl[0].field = field;
    if (ctrl.attr('data-field') == undefined) {
        ctrl.find('[data-field]').each(function () {
            this.field = field;
        });
    }

    if (isForEdit !== true) {
        let defaultValue = undefined;
        if (field.DefaultValue != undefined)
            defaultValue = field.DefaultValue;
        else if (field.Schema.DefaultValue != undefined)
            defaultValue = field.Schema.DefaultValue;
        if (defaultValue != undefined) {
            switch (field.Schema.UIDataType) {
                case "checkbox":
                    ctrl.find('.msp-ctrl').prop('checked', defaultValue);
                    break;
                case "date":
                    if (defaultValue == 'today') {
                        ctrl.find('.msp-ctrl').val(moment().format(_initData.Company.Setting.DateFormat));
                    }
                    else if (defaultValue == 'yesterday') {
                        ctrl.find('.msp-ctrl').val(moment().subtract(1, 'd').format(_initData.Company.Setting.DateFormat));
                    }
                    else if (defaultValue == 'tomorrow') {
                        ctrl.find('.msp-ctrl').val(moment().add(1, 'd').format(_initData.Company.Setting.DateFormat));
                    }
                    break;
                case "dropdown":
                    ctrl.find("select").val(defaultValue);
                    break;
                default:
                    ctrl.val(defaultValue);
                    break;
            }
        }
    }

    let constantValue = undefined;
    if (field.ConstantValue != undefined)
        constantValue = field.ConstantValue;
    else if (field.Schema.ConstantValue != undefined)
        constantValue = field.Schema.ConstantValue;
    if (constantValue != undefined) {
        switch (field.Schema.UIDataType) {
            case "checkbox":
                ctrl.find('.msp-ctrl').prop('checked', constantValue);
                break;
            case "date":
                if (constantValue == 'today') {
                    ctrl.find('.msp-ctrl').val(moment().format(_initData.Company.Setting.DateFormat));
                }
                else if (constantValue == 'yesterday') {
                    ctrl.find('.msp-ctrl').val(moment().subtract(1, 'd').format(_initData.Company.Setting.DateFormat));
                }
                else if (constantValue == 'tomorrow') {
                    ctrl.find('.msp-ctrl').val(moment().add(1, 'd').format(_initData.Company.Setting.DateFormat));
                }
                break;
            case "dropdown":
                ctrl.find("select").val(constantValue);
                break;
            default:
                ctrl.val(constantValue);
                break;
        }
    }

    if (field.Events) {
        if (field.Events.Change) {
            ctrl.change(function () {
                eval(field.Events.Change);
            })
        }
        if (field.Events.Init) {
            setTimeout(function () {
                eval(field.Events.Init);
            }, 2000);
        }
    }

    let disableON = field.Schema.DisableON;
    if (field.DisableON != undefined)
        disableON = field.DisableON;
    if (disableON !== undefined) {
        if (disableON == 'both')
            _common.DisableField(ctrl, field.Schema.UIDataType);
        else if (disableON == 'add' && isForEdit == false)
            _common.DisableField(ctrl, field.Schema.UIDataType);
        else if (disableON == 'edit' && isForEdit == true)
            _common.DisableField(ctrl, field.Schema.UIDataType);
    }

    return ctrl;
}

_controls.BuildCollectionControl = function (ctrl, schema, readOnly, data, pageData, groupData) {
    if (schema.Fields != undefined && schema.Fields.length > 0) {
        let item = $('<div class="msp-coll-frm mt-2"><div class="msp-coll-lbl bg-light form-control form-control-sm p-1 my-1" style="display:none">Item</div><div class="msp-coll-row row border rounded ml-0 mr-0 pt-1"></div></div>');
        let label = item.find('.msp-coll-lbl');
        let btnPanel = $('<div class="form-group col-sm-12 text-right"><button class="btn btn-sm btn-light btn-delete"><i class="fa fa-times" aria-hidden="true"></i></button> <button class="btn btn-sm btn-primary btn-save" ><i class="fa fa-check"></i></button></div>');
        let frm = item.find('.msp-coll-row');
        ctrl.append(item);
        for (let i = 0; i < schema.Fields.length; i++) {
            let field = $.extend({}, schema.Fields[i]);
            let col = $.extend({}, field);
            col.Fields = undefined;
            field.Collections = schema.Collections.slice();
            field.Collections.push(col);
            field.Schema = $.extend({}, field);
            field.ObjectName = pageData.ObjectName;
            _page.BuildField({ field: field, container: frm, data: data, readOnly: readOnly, pageData: pageData, groupData: groupData });
        }
        frm.append(btnPanel);
        label.click(function () {
            if (_common.Validate(ctrl)) {
                ctrl.find('.msp-coll-lbl').show();
                ctrl.find('.msp-coll-row').hide();
                $(this).hide().next().show();
            }
        })
        btnPanel.find('.btn-save').click(function () {
            if (_common.Validate(ctrl)) {
                $(this).closest('.msp-coll-row').hide().prev().show();
                _controls.BuildCrtlCollectionLabel($(this).closest('.msp-coll-frm'), schema);
            }
        });
        btnPanel.find('.btn-delete').click(function () {
            $(this).closest('.msp-coll-frm').remove();
        });
        ctrl.trigger("onItemAdd", item);
    }
}
_controls.BuildCrtlCollectionLabel = function (frm, schema) {
    if (schema.DisplayFormat) {
        let label = schema.DisplayFormat;
        schema.DisplayFormat.match(/{{\$(.+?)}}/g).forEach(function (key) {
            let fieldName = key.replace('{{$', '').replace('}}', '');
            let field = schema.Fields.find(x => x.Name == fieldName);
            field.Schema = $.extend({}, field);
            let data = _controls.GetCtrlDisplayValue(field, frm);
            if (data == null) { data = '' };
            label = label.replace(key, data);
        });
        frm.find('.msp-coll-lbl').first().html(label);
    } else {
        frm.find('.msp-coll-lbl').first().html('Item');
    }

}
//build control with value
_controls.BuildCtrlWithValue = function (params) {
    let field = params.field;
    let group = params.group;
    let pageData = params.pageData;
    let groupData = params.groupData;
    let isForEdit = (params.isForEdit === undefined) ? true : params.isForEdit;
    let ctrl = _controls.BuildCtrl({ field: field, pageData: pageData, groupData: groupData, isForEdit: isForEdit });

    if (ctrl == undefined) { return '' }
    let record = (group && group.IsRealLookup) ? params.data[group.Name] : params.data;;
    if (record == undefined) {
        return ctrl;
    }


    let constantValue = undefined;
    if (field.Schema.ConstantValue != undefined)
        constantValue = field.Schema.ConstantValue;
    if (field.ConstantValue != undefined)
        constantValue = field.ConstantValue;
    if (constantValue != undefined) {
        return ctrl;
    }

    let fieldRecord = record[field.Name];
    if (fieldRecord === undefined || fieldRecord === null) {
        return ctrl
    }
    let calendarTimeFormat = _initData.Company.Setting.CalendarTimeFormat;
    if (_initData.User.MySettings && _initData.User.MySettings.CalendarTimeFormat) {
        calendarTimeFormat = _initData.User.MySettings.CalendarTimeFormat;
    }

    let disVal;
    switch (field.Schema.UIDataType) {
        case "decimal":
            if (fieldRecord.$numberDecimal != undefined)
                ctrl.val(fieldRecord.$numberDecimal)
            break;
        case "texteditor":
            ctrl.find('.texteditor').html(fieldRecord);
            break;
        case "checkbox":
            ctrl.find('input').prop('checked', fieldRecord);
            break;
        case "dropdown":
            ctrl.find('select').val(fieldRecord);
            break;
        case "multiselect":
            ctrl.find('select').multiselect('select', fieldRecord);
            break;
        case "lookup":
            if (record[field.Name + _constantClient.LookupAlias] && record[field.Name + _constantClient.LookupAlias].IsActive) {
                disVal = _controls.GetDisplayValue({ field: field, data: record });
                if (disVal) {
                    let cHtm = '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' + fieldRecord + '">' + disVal + '<i class="fas fa-times"></i></div>';
                    ctrl.find('.msp-ctrl').html(cHtm).attr('contenteditable', 'false');
                }
            }
            break;
        case "multilookup":
            if (record[field.Name + _constantClient.LookupAlias] && record[field.Name + _constantClient.LookupAlias].length > 0) {
                for (let i = 0; i < record[field.Name + _constantClient.LookupAlias].length; i++) {
                    let lookupRecord = record[field.Name + _constantClient.LookupAlias][i];
                    if (lookupRecord.IsActive) {
                        disVal = '';
                        let sept = ' ';
                        let _id = record[field.Name + _constantClient.LookupAlias][i]._id;
                        if (field.Schema.LookupFormat) {
                            disVal = field.Schema.LookupFormat;
                            for (let j = 0; j < field.Schema.LookupFields.length; j++) {
                                disVal = disVal.replace('{{$' + field.Schema.LookupFields[j] + '}}', _common.GetObjectByString(lookupRecord, field.Schema.LookupFields[j]));
                            }
                        }
                        else {
                            for (let j = 0; j < field.Schema.LookupFields.length; j++) {
                                if (j == field.Schema.LookupFields.length - 1)
                                    sept = '';

                                disVal += _common.GetObjectByString(lookupRecord, field.Schema.LookupFields[j]) + sept;
                            };
                        }
                        let userSuspendedClass = "";
                        if (field.Schema.LookupObject == "User") {
                            if (lookupRecord.UserStatus) {
                                if (lookupRecord.UserStatus == _constantClient.USER_STATUS.SUSPEND)
                                    userSuspendedClass += ' suspended-user';
                                else if (lookupRecord.UserStatus == _constantClient.USER_STATUS.REPLACE)
                                    userSuspendedClass += ' replaced-user';
                            }
                        }

                        let cHtm = '<div class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1 ' + userSuspendedClass + '" contenteditable="false" data-id="' + _id + '">' + disVal + '<i class="fas fa-times pl-2"></i></div>'
                        ctrl.find('.msp-ctrl').prepend(cHtm);
                    }
                }
            }
            break;
        case "date":
            ctrl.find('input').val(moment.utc(fieldRecord).format(_initData.Company.Setting.DateFormat));
            break;
        case "time":
            ctrl.find('input').val(moment(fieldRecord, 'HH:mm').format(calendarTimeFormat));
            break;
        case "datetime":
            let tz = _initData.User.MySettings.Timezone;
            if (record[field.Schema.TimezoneField])
                tz = record[field.Schema.TimezoneField]
            ctrl.find('.msp-ctrl-date').val(moment(fieldRecord).tz(tz).format(_initData.Company.Setting.DateFormat));
            ctrl.find('.msp-ctrl-time').val(moment(fieldRecord).tz(tz).format(calendarTimeFormat));
            ctrl.find('.msp-ctrl-time').attr('data-time', moment(fieldRecord).tz(tz).format('HH:mm'));
            break;
        case "file":
            if (record[field.Name + _constantClient.LookupAlias]) {
                let file = record[field.Name + _constantClient.LookupAlias];
                let cHtml = "<label class='file-data' data-filename='" + file.FileName + "' data-fileid='" + file._id + "'>" + file.FileName + ` <a href="/api/drive/download/${file._id}/${file.name}/document" target="_blank"  ><i class='fas fa-download'></i></a>` + "</label><i class='fas fa-times'></i>";
                ctrl.find('.div-file-uploading').html(cHtml).prev().hide();
            }
            break;
        case "location":
            if (!fieldRecord) {
                fieldRecord = _common.GetDefaultLatLng();
            }
            ctrl.attr('data-lat', fieldRecord.lat).attr('data-lng', fieldRecord.lng);
            break;
        case "image":
            let imageExtension = "";
            if (record[field.Name + _constantClient.LookupAlias]) {
                let imageDetails = record[field.Name + _constantClient.LookupAlias];
                if (imageDetails && imageDetails.FileName) {
                    imageExtension = imageDetails.FileName;
                    let index = imageExtension.lastIndexOf(".");
                    imageExtension = imageExtension.substring(index);
                }
            }
            ctrl.attr('data-imageid', fieldRecord);
            ctrl.find('img').attr('src', _constantClient.CDN + '/public/' + _initData.Company._id + '/' + fieldRecord);
            break;
        case "autoincrement":
            if (fieldRecord)
                ctrl.html(fieldRecord);
            break;
        default:
            if (fieldRecord != null)
                ctrl.val(fieldRecord)
            break;

    }

    let disableON = field.Schema.DisableON;
    if (field.DisableON != undefined)
        disableON = field.DisableON;
    if (disableON !== undefined) {
        if (disableON == 'both')
            _common.DisableField(ctrl, field.Schema.UIDataType);
        else if (disableON == 'add' && isForEdit == false)
            _common.DisableField(ctrl, field.Schema.UIDataType);
        else if (disableON == 'edit' && isForEdit == true)
            _common.DisableField(ctrl, field.Schema.UIDataType);
    }

    return ctrl;
}

_controls.SetLookupTypeValue = function (params) {
    let field = params.field;
    let ctrl = params.ctrl;
    let data = params.data;
    if (field == undefined || ctrl == undefined || data == undefined) { return false }

    let name = '';
    let sept = ' ';
    if (field.Schema.LookupFormat) {
        name = field.Schema.LookupFormat;
        for (let j = 0; j < field.Schema.LookupFields.length; j++) {
            name = name.replace('{{$' + field.Schema.LookupFields[j] + '}}', _common.GetObjectByString(data, field.Schema.LookupFields[j]));
        }
    }
    else {
        for (let j = 0; j < field.Schema.LookupFields.length; j++) {
            if (j == field.Schema.LookupFields.length - 1)
                sept = '';
            name += _common.GetObjectByString(data, field.Schema.LookupFields[j]) + sept;
        };
    }

    switch (field.Schema.UIDataType) {
        case "lookup":
            if (name) {
                let cHtm = '<div class="lookup-item border btn-outline-secondary btn btn-sm" contenteditable="false" data-id="' + data._id + '">' + name + '<i class="fas fa-times"></i></div>';
                ctrl.find('.msp-ctrl').html(cHtm).attr('contenteditable', 'false');
            }
            break;
        case "multilookup":
            if (name) {
                let cHtm = '<div class="lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1" contenteditable="false" data-id="' + data._id + '">' + name + '<i class="fas fa-times pl-2"></i></div>'
                ctrl.find('.msp-ctrl').prepend(cHtm);
            }
            break;
        default:
            break;

    }
    return ctrl;
}

_controls.OpenLinkDialouge = function () {
    let range = window.getSelection().getRangeAt(0);
    if (range.collapsed) {
        alert('Please get selection of words from which you want to create a link.');
        return false;
    }
    let formModal = _common.DynamicPopUp({ title: 'Create Link', body: _constantClient.CREATE_LINK_BODY });
    formModal.modal('show');
    formModal.find('.dvBtnText').hide();
    formModal.find('.dvLinkEmail').hide();
    _controls.SetLinkData(formModal, range);
    formModal.find('.modal-body .ddlLinkType').on('change', function () {
        if ($(this).val() == 'url') {
            formModal.find('.dvLinkURL').show();
            formModal.find('.dvLinkEmail').hide();
        } else {
            formModal.find('.dvLinkURL').hide();
            formModal.find('.dvLinkEmail').show();
        }
    })

    formModal.find('.btnSave').click(function () {
        let linkValue = '';
        let flag = 0;
        if (formModal.find('.ddlLinkType').val() == 'email') {
            flag = 1;
            let msgmail = formModal.find('.txtLinkEmailAddress').val().trim();
            let msgSubject = formModal.find('.txtLinkMessageSubject').val().trim();
            let msgAddress = formModal.find('.txtLinkMessageBody').val().trim();
            if (msgmail == '') {
                swal("Please enter mail address", { icon: "info", });
                return false;
            }
            if (!_common.isValidEmailAddress(msgmail)) {
                swal("Please enter valid mail address", { icon: "info", });
                return false;
            }
            if (msgSubject.indexOf('&') != -1 || msgAddress.indexOf('&') != -1) {
                swal("special char(&,?) not allowed");
                return false;
            }
            linkValue = 'mailto:' + msgmail + '?subject=' + msgSubject + '&body=' + msgAddress;
        } else {
            linkValue = formModal.find('.txtURL').val();
            if (!(/([^\s])/.test(linkValue))) {
                formModal.find('.txtURL').focus();
                swal("please enter url value", { icon: "info", });
                return false;
            }
        }
        if (flag == 0 && (!_common.isValidUrl(linkValue) || linkValue.length == 0)) {
            swal(_constantClient.URL_INVALID_MSG, { icon: "info", });
            return false;
        } else {
            var content = range.extractContents();
            var link = document.createElement('a');
            $(link).attr('target', '_blank');
            link.appendChild(content);
            $(link).attr('href', linkValue)
            range.insertNode(link);
            formModal.modal('hide');
        }
    });
}


_controls.SetLinkData = function (formModal, range) {
    let parentElement = range.commonAncestorContainer;
    if (parentElement.parentNode.hasAttribute('href')) {
        let hrefData = parentElement.parentNode.href;
        if (hrefData.indexOf('mailto') != -1) {
            formModal.find('.modal-body .ddlLinkType').val('email');
            formModal.find('.dvLinkURL').hide();
            formModal.find('.dvLinkEmail').show();
            let spl = hrefData.split('?');
            let subject = spl[1].split('&')[0].split('=')[1];
            let body = spl[1].split('&')[1].split('=')[1];
            formModal.find('.txtLinkEmailAddress').val(spl[0].split('?')[0].split(':')[1]);
            formModal.find('.txtLinkMessageSubject').val(subject.replace(new RegExp('%20', 'g'), ' '));
            formModal.find('.txtLinkMessageBody').val(body.replace(new RegExp('%20', 'g'), ' '));
        } else {
            formModal.find('.dvLinkURL').show();
            formModal.find('.dvLinkEmail').hide();
            formModal.find('.modal-body .ddlLinkType').val('url');
            let url = window.location.href.substring(0, window.location.href.length - 1);
            hrefData = hrefData.replace(new RegExp(url, 'g'), '');
            formModal.find('.txtURL').val(hrefData);
        }
    }
}

_controls.CreateRange = function (node, chars, range) {
    if (!range) {
        range = document.createRange()
        range.selectNode(node);
        range.setStart(node, 0);
    }

    if (chars.count === 0) {
        range.setEnd(node, chars.count);
    } else if (node && chars.count > 0) {
        if (node.nodeType === Node.TEXT_NODE) {
            if (node.textContent.length < chars.count) {
                chars.count -= node.textContent.length;
            } else {
                range.setEnd(node, chars.count);
                chars.count = 0;
            }
        } else {
            for (var lp = 0; lp < node.childNodes.length; lp++) {
                range = _controls.CreateRange(node.childNodes[lp], chars, range);

                if (chars.count === 0) {
                    break;
                }
            }
        }
    }

    return range;
};


//SECTION email-client popup
_controls.ComposeMailPopup = function (toEmails, mailSubject, mailBody) {
    let emailServiceStatus = _common.ServiceSubscribeStatus("IntegrateEmailClient");
    if (!emailServiceStatus.status) {
        return;
    } else if (!emailServiceStatus.enabled) {
        swal("Your access has been disabled. Please contact System Administrator for more information.", { icon: 'info' })
        return;
    } else if (_initData.User.MySettings && !_initData.User.MySettings.IsValidEmailPassword) {
        swal("Please enter your email's password", { icon: "warning", buttons: ["Later", "Change Password"] }).then(
            changePass => {
                if (changePass) {
                    _user.TakeUserEmailPassword(true);
                }
            }
        )
        return;
    }
    _common.Loading(true);



    let renderEmails = function (formModal, email) {
        let isValid = _common.isValidEmailAddress(email)
        let divElement = $(`<div data-email=${email} class="${isValid ? "" : "bg-danger text-white"}  lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1">${email} <span class="fas fa-times pl-2 remove-email"></span> </div>`);
        formModal.find(".to_emails-mails").append(divElement);
        divElement.find(".remove-email").click(function () {
            $(this).closest(".lookup-item").remove();
        })

    }

    //STUB multipselect 
    let inputFuntionForm = function (type, container, initialEmails) {

        let inputField = $(`
        <div class="${type}-field-container">
        <label class="small">${type}</label>
        <div class="msp-ctrl trigger-popup form-control form-control-sm to-emails" style="height: auto">
                               
        <div class="to_emails-mails" style="display: inline-block;">

        </div>
        <input type="text" title="to" data-field="to-email-input" data-ui="text" style="outline: none;display: none;">
        </div>
        <div>`)
        container.append(inputField);
        if (initialEmails && initialEmails.length > 0) {
            initialEmails.forEach(ema => {
                renderEmails(container.find(`.${type}-field-container`), ema.trim())
            })
        }
        container.find(`.${type}-field-container [data-field='to-email-input']`).keyup(function (event) {
            this.value = this.value.trim().replace(",", "");
            if (this.value.length > 20) {
                $(this).css({ width: $(this).width() + 6 })
            }

            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (this.value.length == 0 && keycode == 8) {
                container.find(`.${type}-field-container .to-emails [data-email]`).last().remove()
            }

            if (keycode == '13' || keycode == '32' || keycode == '44' || keycode == '188') {
                if (!this.value) { this.value = ""; return; }
                renderEmails(container.find(`.${type}-field-container`), this.value)
                this.value = "";
                $(this).css({ width: "auto" })
            }
        }).blur(function () {
            if (this.value.trim()) {
                renderEmails(container.find(`.${type}-field-container`), this.value)
                this.value = "";

            }
        });


        container.find(`.${type}-field-container .to-emails`).on('click', function () {
            container.find(`.${type}-field-container [data-field='to-email-input']`).css({ display: 'inline', width: 'auto' }).select()
        })
        inputField.on('blur', '[data-field="to-email-input"]', function () {
            $(this).hide();
        });
    }

    let getEmailIdsFromModal = function (formModal, type) {
        let inputContainer = formModal.find(`.${type}-field-container`);
        emails = [];
        invalidEmail = ""

        inputContainer.find(".to-emails [data-email]").each(
            function (e) {
                let currentEmail = $(this).data("email");
                if (_common.isValidEmailAddress(currentEmail)) {
                    emails.push(currentEmail)
                } else {
                    invalidEmail = currentEmail;
                }
            }
        )
        if (!invalidEmail) {
            return emails;
        } else {
            swal(`"${invalidEmail}" is not an valid Email`, { icon: "error" });
            return 'Invalid_email'
        }

    }

    let showAttachements = function (container, attachments, cb) {
        let filesDiv = "";
        attachments.forEach((f, index) => {
            filesDiv += `<div data-file-index=${index}>
                        <i class="fa fa-link"></i> 
                        ${f.name} - ${(f.size / 1024).toFixed(2)}kb 
                        <span style="cursor:pointer;margin-left:5px" class="remove-attachment"><i class="fas fa-times" ></i></span>
                        </div>`
        })
        let jDivEle = $(filesDiv)
        //STUB Remove attachment clicked
        jDivEle.find(".remove-attachment").click(function () {
            cb($(this).parent().data("fileIndex"))
            $(this).parent().remove()
        })
        container.find(".uploaded-images-section").html("");
        container.find(".uploaded-images-section").html(jDivEle);
    }


    //STUB  Showing modal
    _common.GetStaticPage('/page/common/compose-email.html', function (result) {
        let popup = $(result).modal("show");
        _common.Loading(false);
        let formModal = popup.find(".modal-dialog .modal-content");
        let container = formModal.find(".input-container");
        let cc = "Cc";
        let bcc = "Bcc";
        let to = "To";
        let fData = "";
        let attachments = [];
        new inputFuntionForm(to, container, toEmails);

        //STUB cc link clicked
        formModal.find(".cc-link").click(function () {
            if (formModal.find(`.${cc}-field-container`).length > 0) {
                formModal.find(`.${cc}-field-container`).toggle()
            } else {
                new inputFuntionForm(cc, container);
            }
        })
        //STUB bcc link clicked
        formModal.find(".bcc-link").click(function () {
            if (formModal.find(`.${bcc}-field-container`).length > 0) {
                formModal.find(`.${bcc}-field-container`).toggle()
            } else {
                new inputFuntionForm(bcc, container);
            }
        })
        formModal.find(".texteditor").html("").css({ padding: "5px" })
        formModal.find("[data-field='Subject']").val(mailSubject);
        formModal.find(".texteditor").html(mailBody)

        //STUB click on attachment button
        formModal.find(".upload-button").on("click", function () {
            formModal.find(".uploadFile")[0].click()
        })

        //STUB file upload button clicked
        formModal.find(".uploadFile").on('change', function (e) {
            let files = $(this)[0].files;
            attachments.push(...Array.from(files));
            $(this).val("")
            let cb = function (index) {
                attachments.splice(index, 1);
                showAttachements(formModal, attachments, cb)
            }
            showAttachements(formModal, attachments, cb)
        })

        popup.find('.close-button').click(function () {
            swal("Do you really wanted to close this mail?", { icon: "warning", buttons: ["No", "Yes"] }).then(
                close => {
                    if (close) {
                        popup.modal('hide');
                    }
                }
            )
        })
        popup.on('hidden.bs.modal', function () {
            $(this).remove();
        });
        //STUB Send button
        formModal.find(".send-button").click(function () {
            let emailObj = {}
            let toEmails = getEmailIdsFromModal(formModal, to);
            let ccEmail = getEmailIdsFromModal(formModal, cc);
            let bccEmail = getEmailIdsFromModal(formModal, bcc);

            if (toEmails == "Invalid_email" || ccEmail == "Invalid_email" || bccEmail == "Invalid_email") {
                return
            }

            //getting to email
            if ((!toEmails || toEmails.length == 0) && (!ccEmail || ccEmail.length == 0) && (!bccEmail || bccEmail.length == 0)) {
                swal(`Please specify at least one recipient.`, { icon: "error" });
                return
            } else {
                emailObj.toEmails = toEmails
            }
            //getting bcc emails
            if (formModal.find(`.${bcc}-field-container`).length > 0 && formModal.find(`.${bcc}-field-container`).css("display") == 'block') {
                bccEmails = getEmailIdsFromModal(formModal, bcc);
                if (!bccEmails) {
                    return
                }
                emailObj.bccEmails = bccEmails
            }
            //getting cc emails
            if (formModal.find(`.${cc}-field-container`).length > 0 && formModal.find(`.${cc}-field-container`).css("display") == 'block') {
                ccEmails = getEmailIdsFromModal(formModal, cc);
                if (!ccEmails) {
                    return
                }
                emailObj.ccEmails = ccEmails
            }
            // console.log(emailObj)
            emailObj.subject = formModal.find(".email-subject").val();
            emailObj.body = formModal.find(".texteditor").html();

            if (!emailObj.subject.trim() && !emailObj.body.trim()) {
                swal("To send this email, add a subject or body content.", { icon: "error" });
                return
            }

            fData = new FormData();
            fData.append("data", JSON.stringify(emailObj))
            attachments.forEach(
                f => fData.append('file', f)
            )
            _common.Loading(true);
            $.ajax({
                url: '/api/event/sendMailFromUserAccount',
                type: 'post',
                processData: false,
                contentType: false,
                data: fData,
                success: function (res) {
                    if (res.message == "success") {
                        popup.modal('hide');
                        _common.Loading(false);
                        swal("Mail sent Successfully", { icon: "success" });
                    } else if (res.message == "failed") {
                        popup.modal('hide');
                        swal(res.reason, { icon: "error", buttons: ["Later", "Change Password"] }).then(
                            changePass => {
                                if (changePass) {
                                    _user.TakeUserEmailPassword(true);
                                }
                                _common.Loading(false);
                            }
                        )
                    } else {
                        popup.modal('hide');
                        swal("Something went wrong", { icon: "error" });
                        _common.Loading(false);
                    }
                },
                error: function (err) {
                    console.log("----->", err)
                    _common.Loading(false);
                    swal("Something went wrong", { icon: "error" })
                }
            })
        })

    })

}
//!SECTION Email client end


//Options
//    -disableDelete --already added emails can't be removed
//    -alreadyAddedEmails --Emails which are already added
//    -validEmailAdded --Callback fired everytime email added
//    -emailIdRemoved --Callback fired when emailId removed

_controls.inputEmailMultiSelect = function (type, container, initialEmails, options) {
    let self = this;
    let renderEmails = function (formModal, email, disableDelete) {
        let isValid = _common.isValidEmailAddress(email);
        let divElement = $(`<div data-email=${email} class="${isValid ? "" : "bg-danger text-white"}  lookup-item border btn-outline-secondary btn btn-sm mb-1 mr-1"><span class="email-id">${email}</span> ${disableDelete ? "" : '<span class="fas fa-times pl-2 remove-email"></span>'} </div>`);
        formModal.find(".to_emails-mails").append(divElement);
        divElement.find(".remove-email").click(function () {
            $(this).closest(".lookup-item").remove();
            if (options.emailIdRemoved) {
                options.emailIdRemoved(email, self.getEmailIdsFromModal())
            }
        })
        //Code to edit added emails
        divElement.click(function (e) {
            e.stopPropagation();
            let self = $(this);
            self.find(".email-id").attr("contenteditable", true).focus();
            self.find(".email-id").keyup(function () {
                let keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == 13) {
                    emailEditComplete(self);
                    return
                }
                let value = $(this).text().trim();
                divElement.attr("data-email", value)
            })

            self.focusout(function () {
                emailEditComplete(self)
            })
        })

        let emailEditComplete = function (self) {
            let value = $(self).text().trim();
            self.find(".email-id").text(value);
            self.find(".email-id").attr("contenteditable", false);
            if (!_common.isValidEmailAddress(value)) {
                divElement.addClass("bg-danger text-white");
                return;
            } else {
                divElement.removeClass("bg-danger text-white");
            }
            divElement.attr("data-email", "")
            if (isDuplicateEmails(value)) {
                divElement.remove();
                return
            }
            divElement.attr("data-email", value)
        }
        //end
    }

    let isDuplicateEmails = function (newEmail) {
        let alreadyAddedEmails = [];
        container.find(`.${type}-field-container  [data-email]`).each(
            function (e) {
                let addedEmail = $(this).data("email");
                if (addedEmail) {
                    alreadyAddedEmails.push(addedEmail);
                }
            }
        )
        if (options && options.alreadyAddedEmails) {
            alreadyAddedEmails.push(...options.alreadyAddedEmails)
        }
        if (alreadyAddedEmails.length > 0 && alreadyAddedEmails.find(em => em.toLowerCase() == newEmail.toLowerCase())) {
            swal("Email already added", { icon: "error" })
            container.find(`.${type}-field-container [data-field='to-email-input']`).val("");
            return true;
        } else {
            if (options && options.validEmailAdded) {
                options.validEmailAdded(newEmail);
            }
            return false;
        }

    }


    let inputField = $(`
    <div class="${type}-field-container">
    <label class="small">${type}</label>
    <div class="msp-ctrl trigger-popup form-control form-control-sm to-emails" style="height: auto">
                           
    <div class="to_emails-mails" style="display: inline-block;">

    </div>
    <input type="text" title="to" data-field="to-email-input" data-ui="text" style="outline: none;display: none;">
    </div>
    <div>`)
    container.append(inputField);
    if (initialEmails && initialEmails.length > 0) {
        initialEmails.forEach(ema => {
            renderEmails(container.find(`.${type}-field-container`), ema.trim(), options.disableDelete)
        })
    }
    container.find(`.${type}-field-container [data-field='to-email-input']`).keyup(function (event) {
        this.value = this.value.trim().replace(",", "");
        if (this.value.length > 20) {
            $(this).css({ width: $(this).width() + 6 })
        }

        var keycode = (event.keyCode ? event.keyCode : event.which);

        if (keycode == 8 && !inputField[0].preval) {
            let removeEmailBox = container.find(`.${type}-field-container .to-emails [data-email]`).last();
            if (removeEmailBox.find(".remove-email").length > 0) {
                let removedEmailId = removeEmailBox.data()
                removeEmailBox.remove();
                if (options.emailIdRemoved) {
                    options.emailIdRemoved(removedEmailId, self.getEmailIdsFromModal())
                }
            }
        }
        inputField[0].preval = this.value;

        if (keycode == '13' || keycode == '32' || keycode == '44' || keycode == '188') {
            if (!this.value) { this.value = ""; return; }
            if (isDuplicateEmails(this.value)) {
                return
            }
            renderEmails(container.find(`.${type}-field-container`), this.value)
            this.value = "";
            inputField[0].preval = ""
            $(this).css({ width: "auto" })
        }
    }).blur(function () {
        if (this.value.trim()) {
            if (isDuplicateEmails(this.value)) {
                return
            }
            inputField[0].preval = ""
            renderEmails(container.find(`.${type}-field-container`), this.value)
            this.value = "";

        }
    });


    container.find(`.${type}-field-container .to-emails`).on('click', function () {
        container.find(`.${type}-field-container [data-field='to-email-input']`).css({ display: 'inline', width: 'auto' }).select()
    })
    inputField.on('blur', '[data-field="to-email-input"]', function () {
        $(this).hide();
    });

    self.getEmailIdsFromModal = function () {
        let inputContainer = container.find(`.${type}-field-container`);
        emails = [];
        invalidEmail = ""

        inputContainer.find(".to-emails [data-email]").each(
            function (e) {
                let currentEmail = $(this).data("email");
                if (_common.isValidEmailAddress(currentEmail)) {
                    emails.push(currentEmail)
                } else {
                    invalidEmail = currentEmail;
                }
            }
        )
        if (!invalidEmail) {
            return emails;
        } else {
            swal(`"${invalidEmail}" is not an valid Email`, { icon: "error" });
            return 'Invalid_email'
        }

    }
}