$(document).ready(function () {
    let url = window.location.href.split('/')
    let hash = url[url.length - 1]
    if (!hash || hash.length < 5) {
        window.location.replace(location.origin + "/page/forgotpassword.html")
        return
    }

    let IsPasswordValid = function (password) {
        return /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(password)
    }

    let SetPassword = function (passObj) {
        $(".spinner-border").removeClass("d-none");
        $(".setpassword").attr("disabled", true);
        try {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: '/noauth/api/user/setpassword',
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(passObj),
                    success: function (res) {
                        resolve(res)
                        $(".spinner-border").addClass("d-none");
                        $(".setpassword").attr("disabled", true);
                    }
                })
            })
        } catch (e) {
            $(".spinner-border").addClass("d-none");
            $(".setpassword").attr("disabled", false);
            swal('Error While Setting password', { icon: "error" })
        }
    }
    //hide show password eye button
    $('.view-hidepassword').on('click', function () {
        if ($(this).hasClass('fas fa-eye')) {
            $(this).removeClass('fas fa-eye').addClass('fas fa-eye-slash')
            $('.password').attr('type', 'password');
        } else {
            $('.password').attr('type', 'text');
            $(this).removeClass('fas fa-eye-slash').addClass('fas fa-eye')
        }
    });

    $(".password, .confirmpassword").on('change', function (event) {
        let password = $('.password').val();
        let confirmpassword = $('.confirmpassword').val();

        if (!IsPasswordValid(password) || password.length < 7) {
            $('.invalid-pattern').removeClass('d-none');
        }

        if (confirmpassword !== password && confirmpassword.length > 0) {
            $('.password-no-match').removeClass('d-none')
        }
    })

    $(".password, .confirmpassword").on("input", function () {
        let password = $('.password').val();
        let confirmpassword = $('.confirmpassword').val();
        if (password == confirmpassword) {
            $('.password-no-match').addClass('d-none')
        }
        if (IsPasswordValid(password)) {
            $('.invalid-pattern').addClass('d-none');
        }
        if (IsPasswordValid(password) && password == confirmpassword) {
            $('.setpassword').attr('disabled', false)
        } else {
            $('.setpassword').attr('disabled', true)
        }
        if (password) {
            $(".pass-required").addClass("d-none")
        } else {
            $(".conf-required").addClass("d-none")
        }
    })

    $('.setpassword').on('click', function () {
        let password = $('.password').val();
        let confirmpassword = $('.confirmpassword').val();
        if (!password) {
            $(".pass-required").removeClass("d-none")
        }
        if (!confirmpassword) {
            $(".conf-required").removeClass("d-none")
        }
        if (IsPasswordValid(password) && password == confirmpassword) {
            let url = window.location.href.split('/');
            let hash = url[url.length - 1];
            let passwordObj = { password, hash };
            SetPassword(passwordObj).then((res) => {
                if (res.message == 'success') {
                    document.cookie.split(";").forEach(function (c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
                    swal('Password set successfully.', { icon: "success", }).then(async () => {
                        document.cookie = "user_email=" + $("input[type='hidden']").val() + ';path=/';
                        setTimeout(async () => {
                            await window.location.assign(window.location.origin, document.cookie)
                        }, 2000)

                    })

                } else {
                    swal(res.message, { icon: "error" });
                    setTimeout(() => {
                        window.location = `${window.location.origin}/page/forgotpassword.html`
                    }, 2000);
                }
            })

        }

    })

})