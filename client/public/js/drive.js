const _drive = {};
_drive.filesInProgress = [];
_drive.filesToRetry = [];
_drive.AllRequest = [];
_drive.GetUserSpace = function () {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/drive/userspace`,
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                if (result.statusCode == 200) {
                    resolve(result.data)
                } else {
                    reject(result.data)
                }
            }
        })

    })

}
_drive.RequestMoreSpace = function (Name, isAdmin) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/drive/requestmorespace/`,
            type: "POST",
            data: JSON.stringify({ Name, isAdmin }),
            contentType: "application/json",
            success: function (result) {
                if (result.statusCode == 200) {
                    resolve(result.data)
                }
            },
            error: function (result) {
                swal(result.message, { icon: "info", });
                resolve(result.data)
            }

        })

    })
}
_drive.ShareFileAndFolder = function (param, id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/drive/share/${id}`,
            type: "POST",
            data: JSON.stringify(param),
            contentType: "application/json",
            success: function (result) {
                if (result.message == 'success') {
                    resolve(result.data)
                } else {
                    swal(result.data, { icon: "error" });
                    reject(result.data)
                }
            },
            error: function (result) {
                let msg = "Something went wrong"
                if (result.responseJSON && result.responseJSON.data) {
                    msg = result.responseJSON.data;
                }
                swal(msg, { icon: "info", });
                reject(result.data)
            }

        })
    })
}

_drive.DeleteExternalShareLink = function (fileID) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `/api/drive/deleteExternalLink/${fileID}`,
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                if (result.message == "success") {
                    resolve()
                }
            }
        });
    })
}


_drive.GetMyDriveData = function (param) {
    $.ajax({
        url: `/api/drive/mydrive/`,
        type: "GET",
        data: { 'pageNo': param.currentPage, 'size': param.pageSize, 'folderid': param.FolderId, 'viewtype': param.ViewType },
        headers: {
            'searchstring': (param.SearchString) ? param.SearchString : null,
        },
        contentType: "application/json",
        success: function (result) {
            if (result.message == 'success') {
                param.callback(result);
            } else {
                swal(result.message, { icon: "error", });
            }
        }
    });
}

//API to remove sharing from non-owner end
_drive.RemoveSharing = function (fileIds) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/drive/removeSharing`,
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ fileIds: fileIds }),
            success: function (result) {
                if (result.message == 'success') {
                    resolve(result.data)
                } else {
                    swal(result.data, { icon: "error", });
                    reject()
                }
            },
            error: function () {
                swal("Something went wrong", { icon: "error", });
            }
        });
    })
}


_drive.GetShareableLink = function (fileID) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/drive/sharableLink/${fileID}`,
            type: "GET",
            contentType: "application/json",
            success: function (result) {
                if (result.message == 'success') {
                    resolve(result.data)
                } else {
                    swal(result.data, { icon: "error", });
                    reject()
                }
            },
            error: function () {
                swal("Something went wrong", { icon: "error", });
            }
        });
    })

}


_drive.CancelAllRequest = function () {
    for (let i = 0; i < _drive.AllRequest.length; i++) {
        _drive.AllRequest[i].xhr.abort();
    }
}

_drive.GetExternalUserData = function (emails) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/common/getusercontactData`,
            type: "Post",
            contentType: "application/json",
            data: JSON.stringify({ Emails: emails }),
            success: function (result) {
                if (result.message == 'success') {
                    resolve(result.data)
                } else {
                    swal(result.data, { icon: "error", });
                    reject()
                }
            },
            error: function () {
                swal("Something went wrong", { icon: "error", });
            }
        });
    })

}


_drive.RenameFile = function (FileId, FileName, type) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: 'api/drive/rename',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ fileid: FileId, filename: FileName, Type: type }),
            success: function (result) {
                if (result.message == 'success') {
                    resolve()
                } else {
                    reject()
                }
            },
            error: function (error) {
                reject()
            }
        })



    })

}

_drive.UploadFile = function (element, accesstype, onUpload) {


    let file = element.find('.upload-file')[0].files[0];
    let folder = element.find('.hdnFolderId').attr('data-folder');
    let reader = new FileReader();
    let fileData = {};
    reader.onload = function (event, reader) {
        var result = event.target.result;
        fileData.folder = folder;
        fileData.filename = file.name;
        fileData.data = window.btoa(result);
        fileData.type = file.type;
        fileData.size = file.size;
        fileData.filetype = 'file',
            fileData.accesstype = accesstype;
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        //  progressbar.show();
                        element.find('.up-status').html(percentComplete + '%');
                        //percentcomplete.progressbar("option", "value", percentComplete);
                        if (percentComplete === 100) {
                            // progressbar.hide();
                            element.find('.progress').find('.up-status').html('');
                            //percentcomplete.progressbar("option", "value", 0);
                        }
                    }
                }, false);

                return xhr;
            },
            url: "/api/drive/uploadfile",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify(fileData),
            headers: {
                'socket': sessionStorage.getItem('socket-id')
            },
            success: function (result) {
                onUpload();
            },
            error: function (result) {
                alert('not uploaded')
            }
        });
    }
    reader.readAsBinaryString(file);
}


_drive.CopyFile = function (CopyArray) {
    return new Promise((resolve, reject) => {
        $.ajax({
            xhr: function () {
                let xhr = new window.XMLHttpRequest();
                _drive.AllRequest.push({ xhr: xhr, type: 'copy' });
                return xhr
            },
            url: "/api/drive/copy",
            type: "POST",
            data: JSON.stringify(CopyArray),
            contentType: "application/json",
            success: function (result) {
                if (result.statusCode == 200) {
                    _drive.AllRequest = _drive.AllRequest.filter(o => o.type != "copy")
                    resolve(result.statusCode)
                } else {
                    swal(result.message, { icon: "error", });
                    resolve(esult.statusCode);
                }
            },
            error: function (err) {
                _drive.AllRequest = _drive.AllRequest.filter(o => o.type != "copy")
                console.error('Error Occured:', err);
                resolve(err.status);
            }
        });
    }).catch((e) => {
        swal(e.message, { icon: "error", });
    })
}


_drive.DeleteContent = function (obj) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "/api/drive/remove",
            type: "POST",
            data: JSON.stringify(obj),
            contentType: "application/json",
            success: function (result) {
                if (result.message == 'success') {
                    resolve()
                }
            },
            error: function (err) {
                swal(err.responseJSON.message, { icon: "error" });
                resolve();
            },

        });
    })
}

_drive.GetRootFilesAndFolder = function (callback) {
    $.ajax({
        url: "/api/drive/getrootfilesandfolder",
        type: "post",
        contentType: "application/json",
        success: function (result) {
            callback(result);
        }
    });
}

_drive.GetFilesAndFolderByFolder = function (folder, accessType, callback) {
    let sdata = {};
    sdata.folder = folder;
    sdata.accesstype = accessType;
    $.ajax({
        url: "/api/drive/getfilesandfolderbyfolder",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(sdata),
        success: function (result) {
            callback(result);
        },
        error: function (error) {
            alert("error message : " + error);
        }
    })
}

_drive.GetFileSize = function (bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}


_drive.DeleteData = function (element) {
    let fileid = element.find('.downloadableFile').data('fileid');
    $.ajax({
        url: 'api/drive/deletefile',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({ filedata: fileid }),
        success: function (result) {
            if (result.success) {
                element.remove();
            }
        },
        error: function (error) { }
    })
}
_drive.CancelCopyFileRequest = function (page) {
    swal({
        title: "Do you want to cancel file copy in progress?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((UploadCanceled) => {
        if (UploadCanceled) {
            let request = _drive.AllRequest.find(o => o.type == 'copy');
            if (request) {
                request.xhr.abort();
                _drive.AllRequest = _drive.AllRequest.filter(o => o.type != 'copy');
                let StatusContainer = page.find('.copy-container');
                StatusContainer.addClass('d-none').removeClass('d-flex');
            }

        }
    })
}

_drive.CancelZipFileRequest = function (page) {
    swal({
        title: "Do you want to cancel download?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((UploadCanceled) => {
        if (UploadCanceled) {
            let request = _drive.AllRequest.find(o => o.type == 'zip');
            request.xhr.abort();
            _drive.AllRequest = _drive.AllRequest.filter(o => o.type != 'zip');
            let StatusContainer = page.find('.copy-container');
            StatusContainer.addClass('d-none').removeClass('d-flex')

        }
    })
}

_drive.CreateZip = function (SelectedFiles) {
    return new Promise((resolve, reject) => {
        $.ajax({
            xhr: function () {
                let xhr = new window.XMLHttpRequest();
                _drive.AllRequest.push({ xhr: xhr, type: 'zip' });
                return xhr;
            },
            url: `api/drive/createzip`,
            type: 'POST',
            data: JSON.stringify(SelectedFiles),
            contentType: 'application/json',
            success: function (result) {
                _drive.AllRequest = _drive.AllRequest.filter(o => o.type != 'zip');
                resolve(result)
            },
            error: function (error) {
                _drive.AllRequest = _drive.AllRequest.filter(o => o.type != 'zip');
                resolve(error)
            }
        })

    })
}

_drive.DeleteFolder = function (element) {

    let folderid = element.find('.folderData').data('folder');
    $.ajax({
        url: 'api/drive/deletefolder',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({ folderdata: folderid }),
        success: function (result) {
            if (result.success) {
                element.remove();
            }
        },
        error: function (error) { }
    })
}

_drive.MoveFilesOrFolder = function (ToMove) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/drive/move`,
            type: "POST",
            data: JSON.stringify({ move: ToMove }),
            contentType: "application/json",

            success: function (result) {
                if (result.statusCode == 200) {
                    resolve(result)
                } else {
                    swal(result.message, { icon: "error", });
                    resolve()
                }
            }
        });


    })
}


_drive.GetAllFolders = function (FolderID, Type, PageNumber, PageSize, SelectedFiles) {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/api/drive/allfolder`,
            type: "POST",
            data: JSON.stringify({ PageNumber, Type, FolderID, PageSize, SelectedFiles }),
            contentType: "application/json",

            success: function (result) {
                if (result.message == 'success') {
                    resolve(result)
                } else {
                    reject()
                    swal(result.message, { icon: "error", });
                }
            }
        });
    })
}
_drive.CreateFolder = function (FolderName, Type, FolderId) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "/api/drive/createfolder/",
            type: "POST",
            data: JSON.stringify({ 'name': FolderName, 'Type': Type, 'FolderId': FolderId }),
            contentType: "application/json",
            success: function (result) {
                if (result.message == 'success') {
                    resolve(result)
                } else {
                    swal(result.message, { icon: "error", });
                }
            }
        });
    })

}

_drive.GenerateUniqueId = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });

}
_drive.UpdateFileStatusBar = function (page, UniqueID) {
    let AllFiles = page.find(".upload-section .upload-container li").length;
    let LoaderContainer = page.find(`.upload-container ul li.${UniqueID} .file-loader`);
    let ErroredFile = page.find(`.upload-container ul li.${UniqueID} .upload-error`).length;

    let AllErroredFiles = page.find(`.upload-container ul li .upload-error`).length;
    let UploadedFile = page.find(`.upload-container ul li .file-uploaded`).length;
    let UploadInProgress = page.find(`.upload-container ul li .file-loader`).length
    // console.log(AllErroredFiles, UploadInProgress);

    if (AllErroredFiles > 0 && UploadInProgress == 0) {
        page.find('.upload-count .files-count').html(`Error While Uploading File ! Please try to upload again`);
        page.find('.upload-count').removeClass('bg-primary').addClass('bg-danger');
    } else {
        page.find('.upload-count').removeClass('bg-danger').addClass('bg-primary');
        if (_drive.filesInProgress.find(o => o.Name == UniqueID) && _drive.filesInProgress.find(o => o.Status == "done") != undefined && !_drive.filesToRetry.find(o => o.UniqueID == UniqueID)) {
            LoaderContainer.addClass('file-uploaded')
            LoaderContainer.removeClass('file-loader');
            let UploadedFiles = page.find(`.upload-container ul li .file-uploaded`).length;
            page.find('.upload-count .files-count').html(`${UploadedFiles} ${UploadedFiles > 1 ? 'Items' : 'Item'}  uploaded of ${AllFiles}`)

        } else {
            UploadedFiles = UploadInProgress;
            page.find('.upload-count .files-count').html(`${UploadedFiles} ${UploadedFiles > 1 ? 'Items' : 'Item'}  uploading of ${AllFiles}`)


        }

    }
}
_drive.RetryUpload = function (Page, UniqueID, FileDetails) {

    let FileToRetry = _drive.filesToRetry.find(o => o.UniqueID == UniqueID);
    let FileContainerToRetry = Page.find(`.upload-container ul li.${UniqueID}`);
    _drive.SetLoaderToDefault(FileContainerToRetry);
    Page.find(`.upload-container ul li.${UniqueID} #progress-bar-per`).removeClass('upload-error');
    Page.find(`.upload-container ul li.${UniqueID} #progress-bar-per`).addClass('file-loader');
    _drive.UpdateFileStatusBar(Page, UniqueID);
    _drive.uploadFile(FileDetails, FileToRetry.File, Page).then(() => {
        _drive.UpdateFileStatusBar(Page, UniqueID);
        // Page.find(`.upload-container ul li.${UniqueID} .file-uploaded`).unbind("click");
        _mydrive.ReloadPage();
    })

}

_drive.SetLoaderToDefault = function (FileNameContainer) {
    let Loader = FileNameContainer.find('path');
    FileNameContainer.find('inactive').removeClass('inactive');
    FileNameContainer.find('span').text(0);
    Loader[0].setAttribute('stroke-dasharray', '0,250');


}
_drive.RemoveErroredFile = function (Page, FileContainer, UniqueID, FileDetails) {
    FileContainer.find('.file-loader').addClass('upload-error').removeClass('file-loader').removeClass('inactive');
    let FileContainerToRetry = Page.find(`.upload-container ul li.${UniqueID}`);
    _drive.SetLoaderToDefault(FileContainerToRetry);
    _drive.UpdateFileStatusBar(Page, UniqueID);
    FileContainer.on('dblclick', '.upload-error', function (e) {
        e.preventDefault();
    });
    FileContainer.off('click').on('click', '.upload-error', function (e) {
        // console.log($(this), "hgfrertyuhijk")
        $(this).removeClass('upload-error');
        e.stopPropagation();
        Page.find('.upload-count').removeClass('bg-danger').addClass('bg-primary');
        FileContainer.find('.file-loader').removeClass('upload-error').removeClass('file-uploaded');
        Page.find(`.upload-container ul li.${UniqueID} #progress-bar-per`).addClass('file-loader');
        let LoaderContainer = FileContainer.find(`.upload-container ul li.${UniqueID} .file-loader`)
        LoaderContainer.addClass('inactive');
        _drive.RetryUpload(Page, UniqueID, FileDetails);
    })
}


_drive.ForceCancelAllUploads = function (page) {

    let AllFilesContainer = page.find(`.upload-container ul li`);
    let UploadInProgress = _drive.filesInProgress.every(o => o.Status == "done");
    let ZipFileInProgress = _drive.AllRequest.find(o => o.type == 'zip');
    // console.log(AllFilesContainer);
    if (!UploadInProgress) {
        for (let i = 0; i < AllFilesContainer.length; i++) {
            let FileContainer = $(AllFilesContainer[i]);
            let ContainerId = FileContainer.attr("fileid")
            for (let j = 0; j < _drive.filesInProgress.length; j++) {
                if (_drive.filesInProgress[j].Status != "done") {
                    _drive.filesInProgress[j].Status = "error";
                }
            }
            if (!_drive.filesToRetry.find(o => o.UniqueID == ContainerId)) {
                let FileRef = _drive.AllRequest.find(o => o.id == ContainerId);
                if (FileRef && _drive.filesInProgress.find(o => o.Name != "done")) {
                    _drive.filesToRetry.push({ File: FileRef.file, UniqueID: ContainerId });
                    _drive.RemoveErroredFile(page, FileContainer, ContainerId, FileRef.obj);
                }
            } else {
                _drive.SetLoaderToDefault(FileContainer);
                FileContainer.find('.file-loader').addClass('upload-error').removeClass('file-loader');
                page.find('.upload-count .files-count').html(`Error While Uploading File ! Please try to upload again`);
                page.find('.upload-count').removeClass('bg-primary').addClass('bg-danger');
            }
        }
        if (AllFilesContainer.length > 0) _drive.CancelAllRequest();
    }

    if (ZipFileInProgress && ZipFileInProgress.xhr) {
        let request = _drive.AllRequest.find(o => o.type == 'zip');
        request.xhr.abort();
        _drive.AllRequest = _drive.AllRequest.filter(o => o.type != 'zip');
        let StatusContainer = page.find('.copy-container');
        StatusContainer.addClass('d-none').removeClass('d-flex');
        swal("Error Please Check Your Internet Connection", { icon: 'error' });
    }




}
_drive.uploadFilesUsingSignedUrl = async function (file, url, contentType, page, UniqueID, obj) {
    return new Promise((resolve, reject) => {
        //to clear input on change input value;
        let ClearUploadInput = page.find('.uploadFile').val('');
        $.ajax({
            xhr: function () {
                let xhr = new window.XMLHttpRequest();
                _drive.AllRequest.push({ xhr: xhr, id: UniqueID, file: file, obj: obj });
                xhr.upload.addEventListener("progress", function (evt) {

                    if (evt.lengthComputable && sessionStorage.getItem("Conected") == 'true') {
                        let percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        let FileNameContainer = page.find(`.upload-container ul li.${UniqueID}`);
                        FileNameContainer.removeClass('upload-error').addClass('file-loader')
                        if (_drive.filesToRetry.find(o => o.UniqueID == UniqueID)) {
                            _drive.filesToRetry = _drive.filesToRetry.filter(o => o.UniqueID != UniqueID);
                        }
                        let Loader = FileNameContainer.find('path');
                        FileNameContainer.find('.inactive').removeClass('inactive');
                        let StrokeArray = Loader.attr('stroke-dasharray')
                        if (StrokeArray) {
                            let progressbar = StrokeArray.split(',');
                            let decvalue = 250 - parseInt(2.5 * parseInt(percentComplete));
                            let incvalue = parseInt(parseInt(2.5 * parseInt(percentComplete)));
                            progressbar[0] = incvalue;
                            progressbar[1] = decvalue;
                            Loader[0].setAttribute('stroke-dasharray', progressbar.join(','));
                            FileNameContainer.find('#count').html(percentComplete);
                            if (_drive.filesInProgress.find(o => o.Name == UniqueID)) {
                                for (let i = 0; i < _drive.filesInProgress.length; i++) {
                                    if (_drive.filesInProgress[i].Name == UniqueID) {
                                        _drive.filesInProgress[i].Status = percentComplete
                                    }
                                }
                            } else {
                                _drive.filesInProgress.push({ Name: UniqueID, Status: percentComplete })
                            }

                        }
                    }
                }, false);

                return xhr;
            },
            type: "PUT",
            url: url,
            processData: false,
            contentType: 'application/octet-stream',
            data: file,
            // timeout: 2000,
            headers: {
                "Content-Type": 'application/octet-stream',
                "reportProgress": true,
                'x-amz-acl': 'private'
            },
            success: function (result) {
                _drive.filesToRetry = _drive.filesToRetry.filter(o => o.UniqueID != UniqueID);
                _drive.AllRequest = _drive.AllRequest.filter(o => o.id != UniqueID);
                for (let j = 0; j < _drive.filesInProgress.length; j++) {
                    if (_drive.filesInProgress[j].Name == UniqueID) {
                        _drive.filesInProgress[j].Status = 'done'
                    }
                }

                resolve({ statusCode: 200 })

            },

            error: function (XMLHttpRequest, result, err) {
                console.log("Error Occured:", err);
                _drive.AllRequest = _drive.AllRequest.filter(o => o.id != UniqueID);
                for (let j = 0; j < _drive.filesInProgress.length; j++) {
                    if (_drive.filesInProgress[j].Name == UniqueID) {
                        _drive.filesInProgress[j].Status = 'error'
                    }
                }
                let FileNameContainer = page.find(`.upload-container ul li.${UniqueID}`)
                if (!_drive.filesToRetry.find(o => o.UniqueID === UniqueID.toString())) {
                    _drive.filesToRetry.push({ UniqueID: UniqueID, File: file });
                    _drive.RemoveErroredFile(page, FileNameContainer, UniqueID, obj);
                }
                //else {
                //     _drive.SetLoaderToDefault(FileNameContainer);
                //     FileNameContainer.find('.file-loader').addClass('upload-error').removeClass('file-loader');
                //     page.find('.upload-count .files-count').html(`Error While Uploading File ! Please try to upload again`);
                //     page.find('.upload-count').removeClass('bg-primary').addClass('bg-danger');
                // }
                resolve({ statusCode: 500 })
            }
        });
    })
}



_drive.ShowDriveProgressBar = function (Page, Files) {
    // to check Size before setting value of container


    let page = Page
    let UploadSection = page.find('.upload-section');
    let UploadContainer = page.find('.upload-container');
    let UploadDetailContainer = UploadSection.find('.upload-count .files-count');
    let MinimiseProgress = UploadSection.find('.minimise-progressbar');
    let CloseProgressBar = UploadSection.find('.close-progressbar');
    let MaximiseProgress = UploadSection.find('.maximise-progressbar');
    let ErroredFile = page.find('.upload-error').length;
    let TotalFiles = page.find(".upload-section .upload-container li").length + Files.length;
    let UploadedFile = page.find('li .file-uploaded').length;
    let Count = TotalFiles - UploadedFile - ErroredFile;
    UploadSection.removeClass('d-none');

    // if (Files.every(o => _drive.PermitUpload(o.size))) {
    UploadDetailContainer[0].innerHTML = `${Count} ${(Count > 1) ? 'Items' : 'Item'}  Uploading ${TotalFiles > 0 ? ('of ' + TotalFiles) : ''}`;
    // }
    for (let j = 0; j < Files.length; j++) {
        let FileContainer;
        // if (_drive.PermitUpload(Files[j].size)) {
        FileContainer = ` <li fileId="${Files[j].UniqueID}" class="${Files[j].UniqueID}">
            <div class="row align-items-center">
                <div class="col filename">${Files[j].name}</div>
                <div class="col-auto">
                    <div id="progress-bar-per" class="file-loader inactive">
                        <svg id="animated" viewbox="0 0 100 100">
                            <circle cx="50" cy="50" r="45" fill="#b3b3b3" />
                            <path class="pathcircle" fill="none" stroke-linecap="round" stroke-width="12"
                                stroke="#fff" stroke-dasharray="0,250"
                                d="M50 10 a 40 40 0 0 1 0 80 a 40 40 0 0 1 0 -80">
                            </path>
                            
                        </svg>
                        <span id="count" font-size="20">0</span>
                    </div>
                </div>
            </div>
        </li>
        `

        // } else {
        //     return swal('Cannot upload files more than 150 mb', { icon: "info" });
        // }
        if (FileContainer) {
            UploadSection.find('ul').prepend(FileContainer);
            CloseProgressBar.removeClass('d-none');
        } else {
            UploadDetailContainer[0].innerHTML = '';
            UploadSection.addClass('d-none');
        }

    }
    UploadContainer.removeClass('d-none');
    MaximiseProgress.addClass('d-none');
    MinimiseProgress.removeClass('d-none');
    MinimiseProgress.on('click', function () {
        UploadContainer.addClass('d-none');
        $(this).addClass('d-none');
        MaximiseProgress.removeClass('d-none');

    })

    CloseProgressBar.on('click', function () {
        let AllFileUploaded = _drive.filesInProgress.every(o => o.Status == "done")
        if (!AllFileUploaded) {
            swal({
                title: "Cancel all Uploads ?",
                text: "Your uploads are not complete. Would you like to cancel all ongoing uploads?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((UploadCanceled) => {
                if (UploadCanceled) {
                    UploadSection.addClass('d-none');
                    _drive.CancelAllRequest();
                    UploadContainer.find('ul').html("");
                    _drive.filesInProgress = [];
                    _drive.filesToRetry = [];

                }
            })
        } else {
            UploadSection.addClass('d-none');
            UploadContainer.find('ul').html("");
            _drive.filesInProgress = [];
            _drive.filesToRetry = [];
        }
    })

    MaximiseProgress.on('click', function () {
        UploadContainer.removeClass('d-none');
        $(this).addClass('d-none');
        MinimiseProgress.removeClass('d-none');

    })

}

_drive.updateFileStatus = function (UpdatObj) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/drive/uploadstatus',
            type: 'POST',
            data: UpdatObj,
            headers: { 'socket': sessionStorage.getItem('socket-id') },
            success: function (result) {
                resolve(result)
            },
            error: function (error) {
                resolve(error)

            }
        });

    })

}

_drive.checkSpace = function (FileDetailsObj) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: '/api/drive/checkspace',
            type: 'POST',
            data: FileDetailsObj,
            success: async function (result) {
                resolve(result)
            },
            error: function (error) {
                // console.error("Error Occured While Checking Space:", error);
                resolve(error);
            }
        });


    })
}
_drive.RemoveUploadContainer = function (page, Uid) {
    let FileCount = page.find('.upload-section .upload-container li').length;
    let ContainerToRemove = page.find(`.upload-section .upload-container li.${Uid}`);
    let FileName = ContainerToRemove.find('.filename').text();
    if (FileCount == 1) {
        page.find('.upload-count .files-count')[0].innerHTML = '';
        page.find('.upload-section').addClass('d-none');
        page.find('.upload-section .upload-container ul').html(' ');
        swal(`Your File ${FileName} can not be uploaded.Please Check YourStorage Space`, { icon: 'info' })
    } else {
        // let ToRemove = page.find(`.upload-section .upload-container .${Uid}`);
        // page.find(`.upload-section .upload-container .${Uid}`).remove();
        swal(`Your File ${FileName} can not be uploaded . Please Check YourStorage Space `, { icon: 'info' })

    }

}

_drive.uploadFile = function (obj, file, page) {

    return new Promise(async (resolve, reject) => {
        try {
            let SpaceCheck = await _drive.checkSpace(obj);

            if (SpaceCheck.statusCode == 200) {

                let { statusCode } = await _drive.uploadFilesUsingSignedUrl(file, SpaceCheck.data.Url, obj.ContentType, page, obj['UniqueID'], obj);
                let ID = _controls.ToEJSON('objectid', SpaceCheck.data.ID);
                let StatusUpdate = await _drive.updateFileStatus({ ID: ID, Status: statusCode, FileSize: file.size });
                if (StatusUpdate && StatusUpdate.message != 'Limit Excedded') {
                    resolve(StatusUpdate);
                } else if (StatusUpdate && StatusUpdate.message == 'Limit Excedded') {
                    // We will consider this as an error as the Limit has been exceeded
                    // _drive.RemoveUploadContainer(page, StatusUpdate.data.UID);
                    let FileNameContainer = page.find(`.upload-container ul li.${obj['UniqueID']}`)
                    if (!_drive.filesToRetry.find(o => o.UniqueID === StatusUpdate.data.UID.toString())) {
                        _drive.filesToRetry.push({ File: file, UniqueID: StatusUpdate.data.UID.toString() });
                        _drive.RemoveErroredFile(page, FileNameContainer, StatusUpdate.data.UID.toString(), obj);
                    }
                    else {
                        _drive.SetLoaderToDefault(FileNameContainer);
                        FileNameContainer.find('.file-loader').addClass('upload-error').removeClass('file-loader');
                        page.find('.upload-count .files-count').html(`Error While Uploading File ! Please try to upload again`);
                        page.find('.upload-count').removeClass('bg-primary').addClass('bg-danger');
                    }
                    resolve({ statusCode: 426 });
                }
            } else {

                resolve({ statusCode: 426 });

            }
        } catch (err) {
            if (err && err.responseJSON && err.responseJSON.statusCode == 426) {
                let Uid = err.responseJSON.data.UniqueID;
                _drive.RemoveUploadContainer(page, Uid);
                resolve({ statusCode: 426 });

            } else {
                // no Internet
                let FileNameContainer = page.find(`.upload-container ul li.${obj['UniqueID']}`)
                // console.log("No Internet:", err);
                for (let j = 0; j < _drive.filesInProgress.length; j++) {
                    if (_drive.filesInProgress[j].Name == obj['UniqueID']) {
                        _drive.filesInProgress[j].Status = "error";
                    }
                }

                if (!_drive.filesToRetry.find(o => o.UniqueID === obj['UniqueID'].toString())) {
                    // console.log("Called from UploadToDrive", _drive.filesToRetry, obj['UniqueID']);
                    _drive.filesToRetry.push({ File: file, UniqueID: obj['UniqueID'] });
                    _drive.RemoveErroredFile(page, FileNameContainer, obj['UniqueID'], obj);
                }
                else {

                    _drive.SetLoaderToDefault(FileNameContainer);
                    FileNameContainer.find('.file-loader').addClass('upload-error').removeClass('file-loader');
                    page.find('.upload-count .files-count').html(`Error While Uploading File ! Please try to upload again`);
                    page.find('.upload-count').removeClass('bg-primary').addClass('bg-danger');
                }
            }


        }
    })

}



_drive.PermitUpload = function (Size) {
    return Size <= 5368709120
}
_drive.uploadToDrive = function (page, folderID, Files) {

    return new Promise(async (resolve, reject) => {
        let FileDetails = []
        let AllPromises = []

        for (let file of Files) {
            let obj = {
                FileSize: file.size,
                FileName: file.name,
                UniqueID: _drive.GenerateUniqueId(),
                Type: 'file',
                FolderID: folderID,
                ContentType: file.type
            }
            file.UniqueID = obj.UniqueID;

            // if (!_drive.PermitUpload(file.size)) {
            //     return swal('Cannot upload files more than 5GB', { icon: "info" });
            // }

            FileDetails.push(obj);
            _drive.filesInProgress.push({ Name: obj['UniqueID'], Status: 0 });

        }

        _drive.ShowDriveProgressBar(page, Files);

        for (let j = 0; j < FileDetails.length; j++) {
            // if (_drive.PermitUpload(FileDetails[j].FileSize)) {
            AllPromises.push(_drive.uploadFile(FileDetails[j], Files[j], page));
            // }
        }
        Promise.all(AllPromises).then((AllStatus) => {
            resolve(AllStatus)
        }).catch((err) => {
            console.log(err, "Error Occured:")
            resolve(err)
        })

    })
}




_drive.UploadFile = function (element, accesstype, onUpload) {
    let file = element.find('.upload-file')[0].files[0];
    let folder = element.find('.hdnFolderId').attr('data-folder');
    let reader = new FileReader();
    let fileData = {};
    reader.onload = function (event, reader) {
        var result = event.target.result;
        fileData.folder = folder;
        fileData.filename = file.name;
        fileData.data = window.btoa(result);
        fileData.type = file.type;
        fileData.size = file.size;
        fileData.accesstype = accesstype;
        fileData.filetype = 'file',
            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            //  progressbar.show();
                            element.find('.up-status').html(percentComplete + '%');
                            //percentcomplete.progressbar("option", "value", percentComplete);
                            if (percentComplete === 100) {
                                // progressbar.hide();
                                element.find('.progress').find('.up-status').html('');
                                //percentcomplete.progressbar("option", "value", 0);
                            }
                        }
                    }, false);

                    return xhr;
                },
                url: "/api/drive/uploadfile",
                type: "post",
                contentType: "application/json",
                data: JSON.stringify(fileData),
                headers: {
                    'socket': sessionStorage.getItem('socket-id')
                },
                success: function (result) {
                    onUpload();
                },
                error: function (result) {
                    alert('not uploaded')
                }
            });
    }
    reader.readAsBinaryString(file);
}




_drive.GetRootFilesAndFolder = function (callback) {
    $.ajax({
        url: "/api/drive/getrootfilesandfolder",
        type: "post",
        contentType: "application/json",
        success: function (result) {
            callback(result);
        }
    });
}

_drive.GetFilesAndFolderByFolder = function (folder, accessType, callback) {
    let sdata = {};
    sdata.folder = folder;
    sdata.accesstype = accessType;
    $.ajax({
        url: "/api/drive/getfilesandfolderbyfolder",
        type: "post",
        contentType: "application/json",
        data: JSON.stringify(sdata),
        success: function (result) {
            callback(result);
        },
        error: function (error) {
            alert("error message : " + error);
        }
    })
}

_drive.GetFileSize = function (bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

_drive.DeleteData = function (element) {
    //alert(element.find('.downloadableFile').data('fileid'));
    let fileid = element.find('.downloadableFile').data('fileid');
    $.ajax({
        url: 'api/drive/deletefile',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({ filedata: fileid }),
        success: function (result) {
            if (result.success) {
                element.remove();
            }
        },
        error: function (error) { }
    })
}

_drive.DeleteFolder = function (element) {
    alert(element.find('.folderData').data('folder'));
    let folderid = element.find('.folderData').data('folder');
    $.ajax({
        url: 'api/drive/deletefolder',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({ folderdata: folderid }),
        success: function (result) {
            if (result.success) {
                element.remove();
            }
        },
        error: function (error) { }
    })
}


_drive.GetEntityData = function (id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: 'api/drive/getentitydata/' + id,
            type: 'GET',
            contentType: 'application/json',
            success: function (result) {
                if (result.message == "success") {
                    resolve(result.data)
                } else {
                    swal("Something went wrong. Please try again after some time.", { icon: "error" });
                    reject()
                }
            },
            error: function (error) {
                swal("Something went wrong. Please try again after some time.", { icon: "error" })
                reject()
            }
        })
    })
}

_drive.RemoveUserFromSharing = function (id, userIDs) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: 'api/drive/removeusersformsharing/' + id,
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({ userIDs: userIDs }),
            success: function (result) {
                if (result.message == "success") {
                    resolve(result.data)
                } else {
                    swal("Something went wrong. Please try again after some time.", { icon: "error" })
                }
            },
            error: function (error) {
                swal("Something went wrong. Please try again after some time.", { icon: "error" })
            }
        })
    })
}