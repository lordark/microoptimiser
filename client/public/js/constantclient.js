let ENVType = "local";
let Activity_Notification_Email = "invite@notify.optimiser.com";
switch (window.location.origin.split("://")[1].split(".")[0]) {
    case "platform":
        ENVType = "prod";
        Activity_Notification_Email = "invite@notify.optimiser.com";
        break;
    case "sandbox":
        ENVType = "sand";
        Activity_Notification_Email = "invite@notifysand.optimiser.com";
        break;
    case "dev":
        ENVType = "dev";
        Activity_Notification_Email = "invite@notifydev.optimiser.com";
        break;
    default:
        ENVType = "dev"
        Activity_Notification_Email = "invite@notifydev.optimiser.com";
        break;
}
const _constantClient = {

    EMAIL_VALIDATE_REGEX: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    TEMPLATE_PUBLIC_IMAGE_URL: '../images/marketing-template-image/',
    TEMPLATE_SERVER_IMAGE_URL: `https://cdn.optimiser.com/${ENVType}/`,
    SITE_URL: window.location.origin,
    CDN: `https://cdn.optimiser.com/${ENVType}`,
    FilterOperators: [{ 'Key': 'CONTAINS', 'DisplayName': 'contains' },
    { 'Key': 'NOT_CONTAINS', 'DisplayName': 'not contains' },
    { 'Key': 'EQUAL', 'DisplayName': 'equals' },
    { 'Key': 'NOT_EQUAL', 'DisplayName': 'not equal to' },
    { 'Key': 'GREATER_THAN', 'DisplayName': 'greater than' },
    { 'Key': 'LESS_THAN', 'DisplayName': 'less than' },
    { 'Key': 'GREATER_OR_EQUAL', 'DisplayName': 'greater or equal to' },
    { 'Key': 'LESS_OR_EQUAL', 'DisplayName': 'less or equal to' },
    { 'Key': 'STARTS_WITH', 'DisplayName': 'start with' },
    { 'Key': 'END_WITH', 'DisplayName': 'end with' }],

    Months: [{ 'Key': 1, 'Index': 0, 'Name': 'January', 'SortName': 'Jan' },
    { 'Key': 2, 'Index': 1, 'Name': 'February', 'SortName': 'Feb' },
    { 'Key': 3, 'Index': 2, 'Name': 'March', 'SortName': 'Mar' },
    { 'Key': 4, 'Index': 3, 'Name': 'April', 'SortName': 'Apr' },
    { 'Key': 5, 'Index': 4, 'Name': 'May', 'SortName': 'May' },
    { 'Key': 6, 'Index': 5, 'Name': 'June', 'SortName': 'Jun' },
    { 'Key': 7, 'Index': 6, 'Name': 'July', 'SortName': 'Jul' },
    { 'Key': 8, 'Index': 7, 'Name': 'August', 'SortName': 'Aug' },
    { 'Key': 9, 'Index': 8, 'Name': 'September', 'SortName': 'Sep' },
    { 'Key': 10, 'Index': 9, 'Name': 'October', 'SortName': 'Oct' },
    { 'Key': 11, 'Index': 10, 'Name': 'November', 'SortName': 'Nov' },
    { 'Key': 12, 'Index': 11, 'Name': 'December', 'SortName': 'Dec' }],

    LookupAlias: '_LookupData',
    LookupTypeFields: ["lookup", "multilookup", "dropdown", "multiselect", "file", "image"],

    SEND_OUT_STATUS: [
        { 'StatusID': 1, 'Value': 'Draft' },
        { 'StatusID': 2, 'Value': 'Scheduled' },
        { 'StatusID': 3, 'Value': 'InProcess' },
        { 'StatusID': 4, 'Value': 'Sent' }
    ],

    CREATE_LINK_BODY: '<div class="row">\
    <div class="dvBtnText col-sm-6 form-group">\
        <label>Button Text</label>\
         <input type="text" class="txtText req form-control" maxlength="50" title="Button Text"/>\
    </div>\
    <div class="col-sm-6 form-group"><label>Link Type</label>\
       <select class="ddlLinkType form-control">\
           <option value="url">URL</option><option value="email">Email</option>\
        </select></div>\
    <div class="dvLinkURL col-sm-6 form-group">\
       <label>URL</label>\
       <input type="text" class="txtURL form-control" title="URL"  placeholder="https://www.url.com"/>\
    </div><div class="col-sm-12"></div>\
    <div class="dvLinkEmail col-12 form-group"><div class="row">\
        <div class="col-sm-6"><label>Email Address</label>\
        <input type="text" class="txtLinkEmailAddress email form-control" title="email address"/></div>\
    <div class="col-sm-6"><label>Message Subject</label>\
    <input type="text" class="txtLinkMessageSubject form-control"/></div>\
    <div class="col-sm-6"><label>Message Body</label>\
    <br /><textarea  class="txtLinkMessageBody form-control" rows="3" cols="3" style="width:300px;padding:4px 10px;"></textarea></div>\
    </div></div><div class="col-sm-6 form-group gp-underline" style="display:none"><label>Underline</label>\
       <input type="checkbox" class="isUnderline"/>\
    </div>'+ '<div class="modal-footer">\
    </div>\
    </div>',
    URL_INVALID_MSG: 'Please enter valid url !',
    EDITOR_TOOL: '<div style="display: block;" class="mb-0 bg-light p-2 shadow-sm editor-tool-box">\
    <select class="editor-ddlFont form-control form-control-sm w-auto d-inline" value="fontFamily" title="Font Family"  style="width: 110px">\
        <option value="Century Gothic">Century Gothic</option>\
        <option value="Arial, sans-serif">Arial</option>\
        <option value="Comic Sans MS, cursive, sans-serif">Comic Sans MS</option>\
        <option value="Impact, Charcoal, sans-serif">Impact</option>\
        <option value="Lucida Sans Unicode, Lucida Grande, sans-serif">Lucida Sans Unicode\
        </option>\
        <option value="Tahoma, Geneva, sans-serif">Tahoma</option>\
        <option value="Trebuchet MS, Helvetica, sans-serif">Trebuchet MS</option>\
        <option value="Verdana, Geneva, sans-serif">Verdana</option>\
    </select>\
    <i class="btn btn-sm fas fa-bold editortool" data-toggle="tooltip" title="bold" value="bold"></i>\
    <i class="btn btn-sm fas fa-italic editortool" data-toggle="tooltip" title="italic"\
        value="italic"></i>\
    <i class="btn btn-sm fas fa-underline editortool" data-toggle="tooltip" title="underline"\
        value="underline"></i>\
    <i class="btn btn-sm fas fa-subscript editortool" data-toggle="tooltip" title="subscript"\
        value="subscript"></i>\
    <i class="btn btn-sm fas fa-superscript editortool" data-toggle="tooltip" title="superscript"\
        value="superscript"></i>\
    <i class="btn btn-sm fas fa-redo editortool" data-toggle="tooltip" title="redo" value="redo"></i>\
    <i class="btn btn-sm fas fa-undo editortool" data-toggle="tooltip" title="undo" value="undo"></i>\
    <i class="btn btn-sm fas fa-align-center editortool" data-toggle="tooltip" title="justifyCenter"\
        value="justifyCenter"></i>\
    <i class="btn btn-sm fas fa-align-justify editortool" data-toggle="tooltip" title="justifyFull"\
        value="justifyFull"></i>\
    <i class="btn btn-sm fas fa-align-left editortool" data-toggle="tooltip" title="justifyLeft"\
        value="justifyLeft"></i>\
    <i class="btn btn-sm fas fa-align-right editortool" data-toggle="tooltip" title="justifyRight"\
        value="justifyRight"></i>\
    <i class="btn btn-sm fas fa-list-ul editortool" data-toggle="tooltip" title="UnorderedList"\
        value="insertUnorderedList"></i>\
    <i class="btn btn-sm fas fa-list-ol editortool" data-toggle="tooltip" title="OrderedList"\
        value="insertOrderedList"></i>\
    <i class="btn btn-sm fas fa-outdent editortool" data-toggle="tooltip" title="outdent"\
        value="outdent"></i>\
    <i class="btn btn-sm fas fa-link editortool" data-toggle="tooltip" title="create Link" value="createLink"></i>\
    <i class="btn btn-sm fas fa-unlink editortool" data-toggle="tooltip" title="Remove Link"\
        value="unlink"></i>\
    </div>',
    FOLDER_NAME_EMPTY_MSG: 'Please enter valid Folder name.',
    FOLDER_NAME_EXISTS_MSG: 'Folder name already exists.',
    FOLDER_INSERT_MSG: 'Folder name inserted successfully.',
    ERROR_MSG: 'Something went wrong.',

    NOTIFICATION_KEY: "BGakl6XJAWLE4EQ69gna2uzmx49Rh0KeqYoo4_jVlai55-yEMwUYp9UbY0EMFxZXE0wD3FGl48Gv6KpzmEE6oPI",
    /*USER STATUS ENUM*/
    USER_STATUS: {
        ACTIVE: 'Active',
        SUSPEND: 'Suspend',
        REPLACE: "Replace"
    },
    LIST_SCHEMA_TYPE: {
        MARKETING_REGISTERED_EMAIL: 'MarketingRegisteredEmail',
        MARKETING_REGISTERED_DOMAIN: 'MarketingRegisteredDomain'
    },

    /******* Calendar Constants Start*/
    BACKDATEDISALLOW: "Back Date Activity is not allowed to be deleted.",
    /******* Calendat Constants End */
    ActivityNotificationEmail: Activity_Notification_Email
}
