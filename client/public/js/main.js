﻿var _initData = {};
var _DBListSchemaData = {};
$(function () {
    // _common.Loading(true);

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
    $('body').on('show.bs.tooltip', function () {
        $('.tooltip').not(this).remove();
    });
    $('body').on('show.bs.dropdown', function () {
        $('.context-menu:visible').hide();
    });

    //tooltip on menu button
    $('#main-header #sidebarToggleTop').tooltip({
        title: function () {
            return $(this).hasClass("cross") ? "Close menu" : "Main menu";
        }
    })
    // //hide tooltip
    // $('#main-header #sidebarToggleTop').click(function(){
    //     $('#main-header #sidebarToggleTop').tooltip('hide')
    // })
    $('body').on('click', function () {
        $('[data-toggle="tooltip"]').tooltip("hide");
    })
    //disable tooltip and add glow in profile image
    $("#main-header").on('show.bs.dropdown', function () {
        $(this).find(" #userDropdown .img-profile").addClass("img-profile-glow");
        $(this).find('#userDropdown *[title]').tooltip('disable');
        $('[data-toggle="tooltip"]').tooltip("hide");
    })
    //enable tooltip and hide glow in profile image
    $("#main-header").on('hide.bs.dropdown', function () {
        $(this).find("#userDropdown .img-profile").removeClass("img-profile-glow");
        $(this).find('#userDropdown *[title]').tooltip('enable');
    });
    $(".btnLogout").click(function () {
        //delete all cookies
        document.cookie.split(";").forEach(function (c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
        _user.Logout();
    });
    $('body').on('hidden.bs.modal', '.modal[role="dialog"]', function () {
        if ($('.modal[role="dialog"].show').length > 0) {
            $('body').addClass('modal-open');
        }
    });
    $('body').on('shown.bs.modal', '.modal[role="dialog"]', function () {
        /*To make this modal as sub module*/
        let modelOpen = $(document).find('.modal[role="dialog"].show').length;
        if (modelOpen > 1) {
            $(this).addClass('modal-' + modelOpen);
            let backdroList = $(this).prevAll('.modal-backdrop');
            if (backdroList.length > 0) {
                $(backdroList[0]).addClass('backdrop-' + backdroList.length);
            }
        }
    });

    $(document).ajaxError(function (event, request, settings) {

        if (request && request.status && request.status == 401) {
            sessionStorage.setItem('status', request.responseJSON.status)
            document.cookie.split(";").forEach(function (c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
            window.location.reload();
            return
        }

        if (request && request.status && request.status == 403) {
            sessionStorage.setItem('status', request.responseJSON.status)
            document.cookie.split(";").forEach(function (c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
            window.location.reload();
            return
        }
    });
})

function InitApp() {
    _common.GetInitData(function (result) {
        //result.Company.Menu.sort(function (a, b) { return a.Sequence - b.Sequence });
        _initData = result;
        if (_initData.Company.Setting && _initData.Company.Setting.CDNName) {
            _constantClient.CDN = `${_initData.Company.Setting.CDNName}/${ENVType}`;
            _constantClient.TEMPLATE_SERVER_IMAGE_URL = `${_initData.Company.Setting.CDNName}/${ENVType}/`;
        }
        _user.SetUserTopNavBar();
        let sideMenu = '';
        if (result.ProfileAuth.UserMenu) {
            result.ProfileAuth.UserMenu.sort(function (a, b) { return a.Sequence - b.Sequence });

            let defaultPageName = "";
            if (_initData.User.MySettings && _initData.User.MySettings.DefaultTab) {
                let defaultTab = _initData.ProfileAuth.UserMenu.find(x => x.PageName == _initData.User.MySettings.DefaultTab)
                if (defaultTab) {
                    defaultPageName = defaultTab.PageName;
                }
            }

            for (let i in _initData.ProfileAuth.UserMenu) {
                let menu = _initData.ProfileAuth.UserMenu[i];
                let isBetaTemplate="";
                try {
                    let moduleData=_initData.ProfileAuth.Permissions.find(p=>p.ModuleID==menu.ModuleID).ModuleData
                    if(moduleData && moduleData.IsBeta){
                        isBetaTemplate=`<span class="bg-success small beta-nav-menu">Beta</span>`
                    }
                } catch (error) {
                    
                }
                if (defaultPageName) {
                    if (defaultPageName == menu.PageName) {
                        new Page({ 'pageName': menu.PageName });
                        sideMenu += '<li class="nav-item active" menu-data="' + menu.PageName + '"><a class="nav-link" href="#"><i class="' + menu.Icon + '"></i><span>' + menu.DisplayName + '</span>'+isBetaTemplate+'</a></li>';
                        defaultPageName = "";
                    }
                    else
                        sideMenu += '<li class="nav-item" menu-data="' + menu.PageName + '"><a class="nav-link" href="#"><i class="' + menu.Icon + '"></i><span>' + menu.DisplayName + '</span>'+isBetaTemplate+'</a></li>';
                }
                else if (i == 0) {
                    new Page({ 'pageName': menu.PageName });
                    sideMenu += '<li class="nav-item active" menu-data="' + menu.PageName + '"><a class="nav-link" href="#"><i class="' + menu.Icon + '"></i><span>' + menu.DisplayName + '</span>'+isBetaTemplate+'</a></li>';
                }
                else
                    sideMenu += '<li class="nav-item" menu-data="' + menu.PageName + '"><a class="nav-link" href="#"><i class="' + menu.Icon + '"></i><span>' + menu.DisplayName + '</span>'+isBetaTemplate+'</a></li>';
            }
        }

        //Update server time 
        setInterval(function () {
            _initData.CurrentUtcTime = moment.utc(_initData.CurrentUtcTime).add(1, 's').format()
        }, 1000)

        $('#page-menu #nav-list').html(sideMenu);

        _user.BindUserEvents();
        //NOTE check for desktop notification (i.e check for aid in queryparam)
        _desktopNotification.checkDesktopNotification()
        if (!_initData.ProfileAuth.IsAdmin) {
            $('.btn-for-admin').remove();
        }
    });
}


function InitSetup() {
    _common.GetInitData(function (result) {
        //result.Company.Menu.sort(function (a, b) { return a.Sequence - b.Sequence });
        _initData = result;

        //Update server time 
        setInterval(function () {
            _initData.CurrentUtcTime = moment.utc(_initData.CurrentUtcTime).add(1, 's').format()
        }, 1000)
    });
}