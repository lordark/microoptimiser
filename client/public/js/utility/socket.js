
const socket = io(window.location.origin, { reconnect: true, transports: ['websocket'] });
const socketEvents = {}
socketEvents.files = [];
socketEvents.UploadType = ""

// socketEvents.FileUploadProgress = function(progress) {
//     const { FileName, Progress, FileType, UploadType,UniqueID } = progress
//     socketEvents.UploadType = UploadType;
//         let FileNameContainer = $(`.upload-container ul li.${UniqueID}`);
//         let LoaderContainer = FileNameContainer.find('.file-loader');
//             let Loader = LoaderContainer.find('path');
//             LoaderContainer.removeClass('inactive');
//             let progressbar = Loader.attr('stroke-dasharray').split(',');
//             let decvalue = 250 - parseInt(2.5 * parseInt(Progress));
//             let incvalue = parseInt(parseInt(2.5 * parseInt(Progress)));
//             progressbar[0] = incvalue;
//             progressbar[1] = decvalue;
//             Loader[0].setAttribute('stroke-dasharray', progressbar.join(','));
//             FileNameContainer.find('#count').html(Progress);
//             if(socketEvents.files.find(o=>o.Name == UniqueID)){
//                 for(let i= 0; i < socketEvents.files.length ; i++){
//                     if(socketEvents.files[i].Name == UniqueID ){
//                         socketEvents.files[i].Status = Progress 
//                     }
//                 }
//             }else{
//                 socketEvents.files.push({Name:UniqueID,Status:100})
//             }

// }

// socketEvents.FileUploaded = function(UploadStatus) {
//     const {FileName,FileType,UploadType,UniqueID} = UploadStatus;
//     let AllFiles =  $(".upload-section .upload-container li").length;
//     let message = ($('.upload-count .files-count') && $('.upload-count .files-count').length > 0) ? $('.upload-count .files-count')[0].innerHTML : ''    
//         let LoaderContainer = $(`.upload-container ul li.${UniqueID} .file-loader`);
//             LoaderContainer.addClass('file-uploaded');
    
//             for(let i= 0; i < socketEvents.files.length ; i++){
//                 if(socketEvents.files[i].Name == UniqueID ){
//                     socketEvents.files[i].Status = 'done' 
//                 }
//             }
//         let UploadedFiles = socketEvents.files.filter(o=>o.Status == 'done').length;
//         $(`.upload-container ul li.${UniqueID}`).addClass('file-uploaded');
//         $('.upload-count .files-count').html(`${UploadedFiles} ${UploadedFiles > 1 ? 'Items' : 'Item' }  Uploaded of ${AllFiles}`)
//         _mydrive.ReloadPage();


//     socket.removeListener('file-uploaded', socketEvents.FileUploaded);
// }

socketEvents.RefreshNotificationBar = function(status) {
    notification.GetUnseenNotifications(function(result) {
        $('.notification-div').find('.notification-item').remove();
        notification.UpdateNotificationList(result);
        $("#notificationDropdown i.fa-bell").addClass("bell-swing")
    });
}

socketEvents.MaxFileSize = function(){
    console.log("Max File Size Exceeded")
    $('.upload-section').addClass('d-none');

};

socketEvents.ServerException = function(status){
   console.log("Internal Server Exception : ",status) 
    $('.upload-container').find('ul').html("");
   $('.upload-section').addClass('d-none');

}


socketEvents.ThumbnailStatus = function(status){
    const {ThumbPath,FileId} = status;
    $(`.${FileId} .file-bg`).css("background-image", "url(" + ThumbPath + ")");
    $(`.${FileId} .file-bg`).removeClass(function (index, className) {
        return (className.match (/\bfa-file\S+/g) || []).join(' ');
    });
    $(`.${FileId} .file-bg`).removeClass('default-icon')
}

socket.on('connect', function(e) {
    console.log("Socket Connected to Server")
});
socketEvents.SpaceUpgradeRequired = function(){
    $('.upload-container').find('ul').html("");
    $('.upload-section').addClass('d-none');

}

socket.on('disconnect', function(e) {
    let id = sessionStorage.getItem('socket-id')
    // socket.removeListener(`${id}-file-uploaded`, socketEvents.FileUploaded);
    //socket.removeListener(`${id}-file-upload-progress`, socketEvents.FileUploadProgress);
    socket.removeListener(`${id}-Thumbnail-Converted`,socketEvents.ThumbnailStatus);
    socket.removeListener(`${id}-File-Size-Limit-Exceeded`,socketEvents.MaxFileSize);
    socket.removeListener(`${id}-Internal-Server-Error`,socketEvents.ServerException);
    socket.removeListener(`${id}-Space-Upgrade-Required`,socketEvents.SpaceUpgradeRequired);
})




socket.on('id', function(id) {
    sessionStorage.setItem('socket-id', id);
   // socket.on(`${id}-file-upload-progress`, socketEvents.FileUploadProgress);
    // socket.on(`${id}-file-uploaded`, socketEvents.FileUploaded);
    socket.on(`${id}-Thumbnail-Converted`,socketEvents.ThumbnailStatus);
    socket.on(`${id}-File-Size-Limit-Exceeded`,socketEvents.MaxFileSize);
    socket.on(`${id}-Internal-Server-Error`,socketEvents.ServerException);
    socket.on(`${id}-Space-Upgrade-Required`,socketEvents.SpaceUpgradeRequired);
})

socket.on('refresh-notification', socketEvents.RefreshNotificationBar)