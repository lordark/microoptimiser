//"http://localhost:7000/noauth/api/mail/unsubscribepage";
$(document).ready(function () {

    const url = window.location.origin + '/';
    let unsubscribePage = $('.unsubscribe-page');
    unsubscribePage.html("<center><img src='/images/loader.gif' /></center>");
    if (unsubscribePage != undefined) {
        loadData();
        function loadData() {
            let key = getQueryParams();
            let unsbUrl = url + '' + 'noauth/api/mail/unsubscribepage';
            // unsubscribePage.append('<input type="hidden" class="hdncid" value="' + key.cid + '">');
            $.ajax({
                url: unsbUrl,
                data: key,
                dataType: 'html',
                success: function (result) {
                    unsubscribePage.html("");
                    if (result != '' && result != undefined && result.length > 0) {
                        let page = $(result);
                        // let companyName = result.substr(result.indexOf("<title>") + 7, result.indexOf("</title>") - result.indexOf("<title>") - 7);
                        $(document).find('title').html(page[0].innerText);
                        unsubscribePage.append(page[1]);
                        unsubscribePage.find('.campaign-category-list [type="checkbox"].chkSelectAll').on('change', function () {
                            if ($(this).prop('checked'))
                                unsubscribePage.find('.campaign-category-list [type="checkbox"]').prop('checked', true);
                            else
                                unsubscribePage.find('.campaign-category-list [type="checkbox"]').prop('checked', false);
                        });
                        unsubscribePage.find('.campaign-category-list [type="checkbox"].chkSelect').on('change', function () {
                            if (unsubscribePage.find('.campaign-category-list [type="checkbox"].chkSelect:not(:checked)').length == 0)
                                unsubscribePage.find('.campaign-category-list input[data-id="-1"]').prop('checked', true);
                            else
                                unsubscribePage.find('.campaign-category-list input[data-id="-1"]').prop('checked', false);
                        });
                        unsubscribePage.find('.btnSaveUnsubscribePage').on('click', function () {
                            let obj = {};
                            obj.EmailID = unsubscribePage.find('.emailid').text().trim();
                            obj.CampaignSubsCategory = [];
                            obj.CampaignUnsubsCategory = [];
                            unsubscribePage.find('.campaign-category-list [type="checkbox"]').each(function (i, v) {
                                if ($(this).attr('data-id') == -1) return;
                                if ($(this).prop('checked'))
                                    obj.CampaignUnsubsCategory.push($(this).attr('data-id'));
                                else
                                    obj.CampaignSubsCategory.push($(this).attr('data-id'));
                            });
                            // if (obj.CampaignCategory.length == 0) {
                            //     alert('please select subscription category.');
                            //     return false;
                            // }
                            obj.Comment = unsubscribePage.find('.txtComment').val().trim();
                            // obj.CompanyID = unsubscribePage.find('.hdncid').val();
                            obj.LastModifiedDate = new Date();
                            obj.token = key.token
                            // obj.CampaignID = key.campaignid
                            insertUnsubscribeData(obj);
                        });
                    }
                }
            });
        }

        function insertUnsubscribeData(data) {
            unsubscribePage.html("<center><img src='/images/loader.gif' /></center>");
            let urlins = url + '' + 'noauth/api/mail/insertunsubscribedata';
            $.ajax({
                url: urlins,
                data: { data: data },
                success: function (result) {
                    if (result.message == "success") {
                        unsubscribePage.html('').append(`<div class="container text-center">
                                <div class="sub-updated pt-5 mt-4 text-success">
                                    <i class="far fa-check-circle display-2"></i>
                                <div>
                                <div>
                                    <h2 class="text-secondary">Your subscription has been updated successfully</h2>
                                </div>
                            </div>`);
                    } else {
                        bindError()
                    }

                }
                , error: function (err) {
                    console.log(err);
                    bindError()
                }
            })
        }

        function getQueryParams() {
            let obj = {}
            window.location.search.replace("?", '').split("&").forEach(e => {
                let splittedE = e.split("=")
                if (splittedE[0].indexOf('amp;') !== -1) {
                    splittedE[0] = splittedE[0].replace(new RegExp('amp;', 'g'), '');
                }
                if (splittedE[0]) {
                    obj[splittedE[0]] = splittedE[1];
                }
            })
            return obj;
        }
    }
})



function bindError(){
    $('.container-fluid .unsubscribe-body').html('');
    $('.container-fluid .unsubscribe-body').append(`
       <div class="container text-center">
         <div class="sub-updated pt-5 mt-4 text-danger">
            <i class="fas fa-exclamation-circle display-2"></i>
         <div>
        <div>
             <h2 class="text-secondary">Something went wrong</h2>
        </div>
       </div>
    `)
    
}