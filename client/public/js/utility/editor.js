var editor = {};

editor.load = function (event) {
    let eventValue = $(event).attr('value');
    if (eventValue == 'createLink') {
        _controls.OpenLinkDialouge();
    } else if (eventValue == 'fontSize') {
        var spanString = $('<span/>', {
            'text': document.getSelection()
        }).css('font-size', $(event).val() + 'px').prop('outerHTML');
        document.execCommand('insertHTML', false, spanString);
    }
    else if (eventValue == 'fontFamily') {
        // var spanString = $('<span/>', {
        //     'text': document.getSelection()
        // }).css('font-family', $(event).val()).prop('outerHTML');
        document.execCommand('fontName', false, $(event).val());
    }
    else {
        document.execCommand(eventValue);
    }
}


editor.FormattingTool = function (selectionWords, value, styleValue) {
    let startOffset = selectionWords.startOffset;
    let endOffset = selectionWords.endOffset;
    let selection = selectionWords.cloneContents();
    let len = selection.childNodes.length;
    let fragment = document.createDocumentFragment();
    if ($(selection).find('*').length == 0) {
        let span = document.createElement('span');
        span.appendChild(selection);
        span.style[styleValue] = value;
        fragment.appendChild(span);
    } else {
        for (let i = 0; i < len; i++) {
            if (selection.childNodes.length == 0)
                break;
            if (selection.childNodes[i] && selection.childNodes[i].nodeName === '#text') {
                if (selection.childNodes[i].textContent.trim() != "") {
                    let sp = document.createElement('span');
                    sp.appendChild(document.createTextNode(selection.childNodes[i].textContent))
                    sp.style[styleValue] = value;
                    fragment.appendChild(sp);
                }
            } else if (selection.childNodes[i] && selection.childNodes[i].nodeName === 'SPAN'
                || selection.childNodes[i] && selection.childNodes[i].nodeName === 'FONT'
            ) {
                if (selection.childNodes[i].textContent != "") {
                    selection.childNodes[i].style[styleValue] = value;
                    fragment.appendChild(selection.childNodes[i]);
                    i = i - 1;
                }
            }
            else if (selection.childNodes[i] && selection.childNodes[i].nodeName === 'H1'
                || selection.childNodes[i] && selection.childNodes[i].nodeName === 'H2'
                || selection.childNodes[i] && selection.childNodes[i].nodeName === 'H3'
                || selection.childNodes[i] && selection.childNodes[i].nodeName === 'H4'
                || selection.childNodes[i] && selection.childNodes[i].nodeName === 'H5'
                || selection.childNodes[i] && selection.childNodes[i].nodeName === 'H6'
            ) {
                if (selection.childNodes[i].textContent != "") {
                    selection.childNodes[i].style[styleValue] = value;
                    fragment.appendChild(selection.childNodes[i]);
                    i = i - 1;
                }
            }
            else if (selection.childNodes[i] && (selection.childNodes[i].nodeName === 'P'
                || selection.childNodes[i].nodeName === 'SMALL' || selection.childNodes[i].nodeName === 'LI'
                || selection.childNodes[i].nodeName === 'A' || selection.childNodes[i].nodeName === 'Q'
                || selection.childNodes[i].nodeName === 'STRONG' || selection.childNodes[i].nodeName === 'TABLE')
            ) {
                if (selection.childNodes[i].textContent != "") {
                    selection.childNodes[i].style[styleValue] = value;
                    fragment.appendChild(selection.childNodes[i]);
                    i = i - 1;
                }
            }
            // else if (selection.childNodes[i] && selection.childNodes[i].nodeName === 'TABLE'
            // ) {
            //     if (selection.childNodes[i].textContent != "") {
            //         selection.childNodes[i].style[styleValue] = value;
            //         fragment.appendChild(selection.childNodes[i]);
            //         i = i - 1;
            //     }
            // }
            else if (selection.childNodes[i] &&
                (selection.childNodes[i].nodeName === 'B' || selection.childNodes[i].nodeName === 'U' ||
                    selection.childNodes[i].nodeName === 'I' || selection.childNodes[i].nodeName === 'SUB' ||
                    selection.childNodes[i].nodeName === 'SUP')
            ) {
                let sp = document.createElement('span');
                sp.appendChild(selection.childNodes[i]);
                sp.style[styleValue] = value;
                fragment.appendChild(sp);
                i = i - 1;
            }
        }
    }
    if (fragment.childNodes.length > 0) {
       var isContentDeleted=false;
        if(selectionWords.commonAncestorContainer.tagName=='UL'){
            if($(selectionWords.commonAncestorContainer).find('li').length==$(selectionWords.cloneContents()).find('li').length)
            {
                $(selectionWords.commonAncestorContainer).find('li').remove();
                isContentDeleted=true;
            }
        }
        if(selectionWords.commonAncestorContainer.nodeName=="#text"){
           if(selectionWords.commonAncestorContainer.parentNode.tagName=="Q"){
             if($(selectionWords.commonAncestorContainer.parentNode).text()==$(selectionWords.cloneContents()).text()){
                $(selectionWords.commonAncestorContainer.parentNode).html('');
                isContentDeleted=true;
             }
           } 
        }
if(isContentDeleted==false){
        selectionWords.deleteContents();
}

        //  $(selectionWords.commonAncestorContainer.children).filter(function() {return $(this).text()== '';}).remove()
        selectionWords.insertNode(fragment);
        selectionWords.endOffset = endOffset;
        selectionWords.startOffset = startOffset;
    } else {
        let span = document.createElement('span');
        span.appendChild(selection);
        span.style[styleValue] = value;
        fragment.appendChild(span);
        selectionWords.deleteContents();
        selectionWords.insertNode(fragment);
        selectionWords.endOffset = endOffset;
        selectionWords.startOffset = startOffset;
    }
}
editor.EditorKeyUp = function (element, e, data) {
    e.stopPropagation();
    var self = $(element);
    if (e.key == "@") {
        if (self.find('.editor.msp-popup').length > 0) {
            self.find('.editor.msp-popup').parent().remove();
        }
        var popup = $('<div class="border bg-white editor msp-popup shadow-lg rounded-bottom db-tool-tip" style="height:150px;overflow:auto; position:absolute;z-index:99999999; bottom:-60px;" data-allowed="true" data-pos="0"></div>');
        self.popup = popup;
        for (let i = 0; i < data.length; i++) {

            let itm = $('<div class="item" data-fieldName="' + data[i].key + '">' + data[i].key + '</div>');
            popup.append(itm);
        }

        setTimeout(function () {
            let left = 0;
            let top = '21px';
            popup.css({ 'left': left, 'top': top });
            var pSpan = $('<span style="position:relative; width:1px; height:1px" class="popup-container" contenteditable="false"></span>');
            var sel = window.getSelection();
            var range = sel.getRangeAt(0);
            if (range) {
                popup.attr('data-pos', range.startOffset);
                range.insertNode(pSpan[0]);
                popup.appendTo(pSpan);
                range.collapse(false);
                popup[0].range = range;
            }

            popup.on('mousedown', '.item', function (e) {
                if (e.button == 0) {
                    e.preventDefault();
                    if ($(this).attr('data-fieldName')) {
                        var textNode = document.createTextNode($(this).attr('data-fieldName'));
                        let rangeTemp = $(this).parent()[0].range
                        let clearSearchValueFlag = $(this).parent()[0].clearSearchValue;
                        let searchRange = $(this).parent()[0].srange;
                        let searchRangeTextLength = ($(this).parent()[0].SearchValueLength || 0);

                        if (rangeTemp) {
                            if (clearSearchValueFlag == true) {
                                if (searchRange) {
                                    if (searchRange.startContainer) {
                                        searchRange.startContainer.replaceData(0, searchRangeTextLength, "");
                                    }
                                }
                            }
                            rangeTemp.insertNode(textNode);
                        }
                        self.find('.editor.msp-popup').parent().remove();
                    } else {
                        self.find('.editor.msp-popup').parent().remove();
                    }
                }
            });
        }, 100);
    } else {
        if (self.find('.editor.msp-popup').attr('data-allowed') == "true"
            && (e.key != "ArrowUp" && e.key != "ArrowDown" && e.key != "Enter")) {
            var sel = window.getSelection();
            var range = sel.getRangeAt(0);
            if (range) {
                self.find('.editor.msp-popup').html('');
                let searchVal = "";
                let clearSearchVal = true;
                if (range.startContainer.substringData) {
                    searchVal = range.startContainer.substringData(0, range.startOffset);
                } else {
                    try {
                        searchVal = range.startContainer.substringData(self.find('.editor.msp-popup').attr('data-pos'), range.startOffset);
                    }
                    catch (ex) {
                        clearSearchVal = false;
                        searchVal = "";
                    }
                }
                searchVal = (searchVal || "").toString().toLowerCase().trim();
                for (let i = 0; i < data.length; i++) {
                    let key = data[i].key
                    if (key.toLowerCase().indexOf(searchVal) >= 0) {
                        let itm = $('<div class="item" data-fieldName="' + key + '">' + key + '</div>');
                        self.find('.editor.msp-popup').append(itm);
                    }
                }
                if (self.find('.editor.msp-popup .item').length == 0) {
                    self.find('.editor.msp-popup').attr('data-allowed', "false");
                    self.find('.editor.msp-popup').closest('.popup-container').remove();
                } else {
                    self.find('.editor.msp-popup')[0].srange = range;
                    self.find('.editor.msp-popup')[0].clearSearchValue = clearSearchVal;
                    self.find('.editor.msp-popup')[0].SearchValueLength = searchVal.length;
                }
            }
        }
    }
}

editor.EditorKeyDown = function (element, e, data) {
    e.stopPropagation();
    let self = $(element);
    if (self.find('.editor.msp-popup').length > 0) {
        let cIndex = 0;
        if (self.find('.editor.msp-popup')[0].currentIndex) {
            cIndex = self.find('.editor.msp-popup')[0].currentIndex;
        }
        self.find('.editor.msp-popup .item').removeClass('selected');
        if (e.key == "ArrowUp" || e.key == "ArrowDown" || e.key == "Enter") {
            e.preventDefault();
            if (e.key == "ArrowUp") {
                if (cIndex > 0) {
                    cIndex -= 1;
                }
                self.find('.editor.msp-popup')[0].currentIndex = cIndex;
            } else if (e.key == "ArrowDown") {
                if (cIndex < self.find('.editor.msp-popup .item').length - 1) {
                    cIndex += 1;
                }
                self.find('.editor.msp-popup')[0].currentIndex = cIndex;
            }
            self.find('.editor.msp-popup .item').eq(cIndex).addClass('selected');
            if (e.key == "Enter") {
                let field = self.find('.editor.msp-popup .item.selected');
                if (field && field.attr('data-fieldName')) {
                    var textNode = document.createTextNode(field.attr('data-fieldName'));
                    let rangeTemp = self.find('.editor.msp-popup')[0].range;
                    let clearSearchValueFlag = self.find('.editor.msp-popup')[0].clearSearchValue;
                    let searchRange = self.find('.editor.msp-popup')[0].srange;
                    let searchRangeTextLength = (self.find('.editor.msp-popup')[0].SearchValueLength || 0);
                    self.find('.editor.msp-popup').parent().remove();
                    if (rangeTemp) {
                        if (clearSearchValueFlag == true) {
                            if (searchRange) {
                                if (searchRange.startContainer) {
                                    searchRange.startContainer.replaceData(0, searchRangeTextLength, "");
                                }
                            }
                        }
                        rangeTemp.insertNode(textNode);
                    }
                }
                self.find('.editor.msp-popup').parent().remove();
            }
        }
        else {
            if (e.key == " ") {
                self.find('.editor.msp-popup').parent().remove();
            }
        }
    }
}


// editor.EditorKeyUp = function (e, data) {
//     e.stopPropagation();
//     var self = $(this);
//     if (e.key == "@") {
//         if (self.find('.msp-popup').length > 0) {
//             self.find('.msp-popup').parent().remove();
//         }
//         var popup = $('<div class="border bg-white msp-popup shadow-lg rounded-bottom" style="position:absolute;z-index:1060"></div>');
//         self.popup = popup;
//         for (let item in data) {
//             let key = item.replace(" ", "");
//             let itm = $('<div class="item" data-fieldName="' + key + '">' + key + '</div>');
//             popup.append(itm);
//         }
//         setTimeout(function () {
//             let left = 0;
//             let top = 0;
//             popup.css({ 'left': left, 'top': top });
//             var pSpan = $('<span style="position:relative;width:1px;height:1px" contenteditable="false"></span>');
//             var sel = window.getSelection();
//             var range = sel.getRangeAt(0);
//             range.insertNode(pSpan[0]);
//             popup.appendTo(pSpan);
//             range.collapse(false);
//             popup[0].range = range;
//             popup.on('mousedown', '.item', function (e) {
//                 if (e.button == 0) {
//                     let field = $(this).attr('data-fieldName');
//                     popup.parent().before(field);
//                     popup.parent().remove();
//                     e.preventDefault();
//                 }
//             });
//         }, 100);
//     }
// }

// editor.EditorKeyDown = function (e, data) {
//     e.stopPropagation();
//     var self = $(this);
//     if (self.find('.msp-popup').length > 0) {
//         let cIndex = 0;
//         if (self.find('.msp-popup')[0].currentIndex) {
//             cIndex = self.find('.msp-popup')[0].currentIndex;
//         }
//         self.find('.msp-popup .item').removeClass('selected');
//         if (e.key == "ArrowUp") {
//             if (cIndex > 0) {
//                 cIndex -= 1;
//             }
//             self.find('.msp-popup')[0].currentIndex = cIndex;
//         } else if (e.key == "ArrowDown") {
//             if (cIndex < self.find('.msp-popup .item').length - 1) {
//                 cIndex += 1;
//             }
//             self.find('.msp-popup')[0].currentIndex = cIndex;
//         }
//         self.find('.msp-popup .item').eq(cIndex).addClass('selected');
//         if (e.key == "Enter") {
//             let field = self.find('.msp-popup .item.selected');
//             if (field && field.attr('data-fieldName')) {
//                 self.find('.msp-popup').parent().before(field.attr('data-fieldName'));
//             }
//             self.find('.msp-popup').parent().remove();
//             e.preventDefault();
//         }
//     }
// }


