const express = require('express');
const json = require('body-parser');

const app = express();
app.use(json());

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res, next) {
    // _common.GetStaticPage('/public/page/login.html',function(result){
    //     res.sendFile(result)
    // })
    res.sendFile(__dirname + '/public/page/login1.html');
});

// app.get('/', function (req, res, next) {
//     if (req.cookies.msp_d === undefined)
//         res.sendFile(__dirname + '/public/page/login.html');
//     else {
//         redisClient.hgetall(req.cookies.msp_d.token, function (err, data) {
//             if (err) {
//                 next(err)
//             }
//             if (data === undefined || data === null)
//                 res.sendFile(__dirname + '/public/page/login.html');
//             else
//                 res.sendFile(__dirname + '/public/page/index.html');
//         })
//     }

// });

app.listen(3000, () => {
    console.log('Listening on port 3000!!!!!!!!');
});