const express = require('express');
const {json} = require('body-parser');
const cors = require('cors');

const app = express();
app.use(json());
app.use(cors());

app.get('/',(req,res)=>{
    res.send("Hi There")
})

app.get('/api/users/currentuser',(req,res)=>{
    res.send("Hamza ")
})

app.listen(4000,()=>{
    console.log("Successfully running on port 4000 !! ");
})