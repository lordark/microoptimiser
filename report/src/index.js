const express = require('express');
const {json} = require('body-parser');
const cors = require('cors');

const app = express();
app.use(json());
app.use(cors());

app.get('/',(req,res)=>{
    res.send("Hi There Report")
})


app.listen(4003,()=>{
    console.log("Successfully running on port 4003 !! ");
})